FROM node:17.6.0

WORKDIR /app

COPY package.json ./

RUN npm install cross-env --legacy-peer-deps

RUN npm install --legacy-peer-deps

COPY . .

EXPOSE 3000

CMD ["bash"]