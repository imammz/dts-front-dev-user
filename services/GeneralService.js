import http from "../src/http-common";
import moment from "moment/moment";

const akademi = () => {
  return http.get("/general/akademi");
};

const tema = (akademiSlug) => {
  return http.get("/general/tema", {
    params: {
      akademi: akademiSlug,
    },
  });
};

const mitra = (limit = 1000) => {
  return http.get("/general/mitra", {
    params: {
      limit: limit,
      order: "Alphabet",
    },
  });
};

const filterMitra = (akademi = "", tema = "", limit = 1000) => {
  return http.get("/general/mitra-filter", {
    params: {
      limit: limit,
      akademi: akademi,
      tema: tema,
      order: "Alphabet",
    },
  });
};

const penyelenggara = () => {
  return http.get("/general/penyelenggara");
};

const pendidikan = () => {
  return http.get("/general/pendidikan");
};

const provinsi = () => {
  return http.get("/general/provinsi");
};

const negara = () => {
  return http.get("/general/country");
};

const kota = (provinsiId) => {
  return http.get("/general/kota", {
    params: {
      provinsi: provinsiId,
    },
  });
};

const kecamatan = (kotaId) => {
  return http.get("/general/kecamatan", {
    params: {
      kota: kotaId,
    },
  });
};

const kelurahan = (kecamatanId) => {
  return http.get("/general/kelurahan", {
    params: {
      kecamatan: kecamatanId,
    },
  });
};

const statusPekerjaan = () => {
  return http.get("/general/status-pekerjaan");
};

const bidangPekerjaan = () => {
  return http.get("/general/bidang-pekerjaan");
};

const generalSearch = (keyword = "", limit = 6) => {
  return http.get("/general/search", {
    params: { keyword: keyword, limit: parseInt(limit) || 6 },
  });
};

const checkNeedForgetPass = (email) => {
  return http.post("/general/required_reset_password", { email: email });
};

const getServerTime = async (diff = false) => {
  try {
    const time = await http.get("/general/servertime");
    const serverTime = time.data.result[0].server_time;
    return Promise.resolve({
      diff: moment().diff(moment(serverTime, "YYYY-MM-DD HH:mm:ss"), "minutes"),
      serverTime: serverTime,
    });
  } catch (err) {
    return Promise.reject(err);
  }
};

const GeneralService = {
  negara,
  akademi,
  tema,
  mitra,
  penyelenggara,
  pendidikan,
  provinsi,
  kota,
  kecamatan,
  kelurahan,
  statusPekerjaan,
  bidangPekerjaan,
  generalSearch,
  checkNeedForgetPass,
  getServerTime,
  filterMitra,
};

export default GeneralService;
