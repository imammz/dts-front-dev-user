import http, { httpDummy } from "../src/http-common";
import httpDukcapil from "../src/http-dukcapil";
import PortalHelper from "../src/helper/PortalHelper";
import CacheHelper from "../src/helper/CacheHelper";
import EncryptionHelper from "../src/helper/EncryptionHelper";
import RouteSegment from "./../src/route-segment";

const headers = {
  "Content-Type": "application/json",
  Authorization: "Bearer 11842|XXvJaNJGdIWX195d7ktoRNjk3BeGq7yLU7JRnITd",
};

// User Auth
const lupaPassword = (payload) => {
  return http.post("/auth/reset-password", payload, {
    headers: headers,
  });
};

const resetPassword = (payload) => {
  return http.post("/auth/change-password", payload);
};

const login = (payload, ress = "") => {
  return http.post("/auth/login", payload, {
    headers: {
      "Content-Type": "application/json",
      Authorization: ress,
    },
  });
};

const kodeNegaraList = () => {
  return http.post(
    "/kodenegara",
    {},
    {
      headers: headers,
    },
  );
};

const register = (payload) => {
  return http.post("/auth/register", payload);
};
// const register = (payload) => {
//   return http.post("/auth/register_pdp", payload);
// };
const registerMitra = (payload) => {
  return http.post("/auth/register_mitra", payload);
};

const emailVerif = (payload) => {
  const header = PortalHelper.tokenGetFromCookie().includes("undefined")
    ? headers
    : {
        "Content-Type": "application/json",
        Authorization: PortalHelper.tokenGetFromCookie(),
      };
  return http.post("/auth/verify-otp", payload, {
    headers: header,
  });
};

const emailPinVerif = (payload) => {
  return http.post("/auth/reset-password/verify-otp", payload);
};

const userFetchData = (userId) => {
  return http.post(
    "/cari-user",
    {
      id: userId,
    },
    {
      headers: {
        "Content-Type": "application/json",
        Authorization: PortalHelper.tokenGetFromCookie(),
      },
    },
  );
};

const bannerList = () => {
  return http.get("/home/load-banner");
};

const sliderList = () => {
  return http.post("/web/get-imagetron-slider");
  // return http.post("/portal/get-imagetron-slider");
};

const sidebarList = () => {
  return http.get("/home/load-sidebar");
};

const akademiList = () => {
  return http.get("/home/akademi");
};

const akademiDetail = (slug) => {
  return http.get(`/home/akademi/${slug}`);
};

const pelatihanList = (
  akademiSlug,
  page,
  tanggal_pendaftaran,
  tanggal_pelatihan,
  temaID,
  mitraID,
  penyelenggaraID,
  lokasiID,
  metodePelatihan,
  statusPendaftaran,
) => {
  return http.get(`/home/pelatihan/${akademiSlug}`, {
    params: {
      page: page,
      tanggal_pendaftaran: tanggal_pendaftaran,
      tanggal_pelatihan: tanggal_pelatihan,
      tema_id: temaID,
      mitra_id: mitraID,
      penyelenggara_id: penyelenggaraID,
      lokasi_id: lokasiID,
      metode_pelatihan: metodePelatihan,
      status_pendaftaran: statusPendaftaran,
    },
  });
};

const pelatihanListJadwal = (
  akademiSlug,
  page,
  tanggal_pendaftaran,
  tanggal_pelatihan,
  temaID,
  mitraID,
  penyelenggaraID,
  lokasiID,
  metodePelatihan,
  statusPendaftaran,
) => {
  return http.get(`/home/pelatihan/jadwal/list`, {
    params: {
      page: page,
      tanggal_pendaftaran: tanggal_pendaftaran,
      tanggal_pelatihan: tanggal_pelatihan,
      tema_id: temaID,
      mitra_id: mitraID,
      penyelenggara_id: penyelenggaraID,
      lokasi_id: lokasiID,
      metode_pelatihan: metodePelatihan,
      status_pendaftaran: statusPendaftaran,
    },
  });
};

const pelatihanJadwalHeader = (month) => {
  return http.get(`/home/pelatihan/jadwal/header`, {
    params: {
      month: month,
    },
  });
};

const pelatihanRelated = (akademiSlug) => {
  return http.get(`/home/pelatihan/${akademiSlug}/related`);
};

const pelatihanFilter = (
  length,
  page,
  mitraId = null,
  temaId = null,
  penyelenggaraId = null,
  lokasiId = null,
) => {
  return http.get("/home/pelatihan", {
    params: {
      limit: length,
      page: page,
      mitra_id: mitraId,
      tema_id: temaId,
      penyelenggara_id: penyelenggaraId,
      lokasi_id: lokasiId,
    },
  });
};

const pelatihanDetail = (pelatihanID) => {
  return http.get(`/home/pelatihan/${pelatihanID}/detail`);
};

const pelatihanByMitra = (mitraId, length, start = 0) => {
  return http.post(
    `/home/pelatihan`,
    {
      mitraId: mitraId,
      mulai: start,
      limit: length,
    },
    {
      headers,
    },
  );
};

// tema by akademi

const temaPopolerList = () => {
  return http.get("/home/tema/popular");
};

const temaList = (slugAkademi, order_by, page, limit = 20) => {
  return http.get("/tema/with-relasi", {
    params: {
      akademi: slugAkademi,
      order_by: order_by,
      page: page,
      limit: limit,
    },
  });
};

// Publikasi

const addView = (jenis, slug, id = null) => {
  return http.post("/portal/update-counter", {
    tabel: jenis,
    slug: slug,
    id: id,
  });
};

const gallerySetting = (kategori) => {
  return http.post(
    "/publikasi/list-setting-galeri",
    {
      kategori: kategori,
      nama: "ss",
    },
    {
      headers: headers,
    },
  );
};

const galleryList = async (token, limit) => {
  return http.get(
    process.env.INSTAGRAM_MEDIA_URL ?? RouteSegment.INSTAGRAM_MEDIA_URL,
    {
      params: {
        limit: limit,
        access_token: token,
        fields:
          "id, thumbnail_url, media_type, media_url, username, timestamp, caption",
      },
    },
  );
};

const galleryCount = async (token) => {
  return http.get(
    process.env.INSTAGRAM_MEDIA_URL ?? RouteSegment.INSTAGRAM_MEDIA_URL,
    {
      params: {
        fields: "id",
        access_token: token,
      },
    },
  );
};

const videoList = (kategoriId, length, page = 1) => {
  return http.get("/home/video", {
    params: {
      page: page,
      limit: length,
      kategori_id: kategoriId,
    },
  });
};

const eventList = (start, length, sorting) => {
  return http.get(`/home/event`, {
    params: {
      page: start,
      limit: length,
      order: sorting,
    },
  });
};

const eventById = (eventId) => {
  return http.post(`/portal/get-event-by-id`, {
    id: eventId,
  });
};
const imagetronList = (pinned = 1) => {
  return http.post("/web/get-imagetron-square", {
    // return http.post("/portal/get-imagetron-square", {
    pinned: pinned,
  });
};

const artikelCarifull = (length, keyword, start = 0) => {
  return http.post("/portal/get-artikel-search", {
    start: start,
    length: length,
    keyword: keyword,
  });
};

const artikelFilterByKategori = (category, length, start = 0) => {
  return http.post(
    "/portal/get-artikel-by-kategori",
    {
      start: start,
      length: length,
      kategori_id: category,
    },
    {
      headers,
    },
  );
};

const artikelFilterByTags = (tag, length, start = 0) => {
  return http.post("/portal/get-artikel-by-tags", {
    start: start,
    length: length,
    tags: tag,
  });
};

const artikelList = (
  limit,
  start = 0,
  kategori_id = null,
  tags = null,
  keyword = null,
  slug = null,
) => {
  return http.get("/home/artikel", {
    params: {
      page: start,
      limit: limit,
      kategori_id: kategori_id,
      tags: tags,
      keyword: keyword,
      slug: slug,
    },
  });
};

const artikelFindBySlug = (slug) => {
  return http.post("/portal/get-artikel-by-slug", {
    slug: slug,
  });
};

const artikelFindById = (id) => {
  return http.post("/portal/get-artikel-by-id", {
    id: id,
  });
};

const artikelAllJudul = () => {
  return http.post("portal/get-artikel-judul");
};

const kategoriList = (jenis) => {
  return http.post(
    "/portal/get-kategori",
    {
      jenis: jenis,
    },
    {
      headers,
    },
  );
};

const informasiList = (
  limit,
  page = 1,
  kategori_id = null,
  tags = null,
  keyword = null,
  slug = null,
) => {
  return http.get("/home/informasi", {
    params: {
      page: page,
      limit: limit,
      kategori_id: kategori_id,
      tags: tags,
      keyword: keyword,
      slug: slug,
    },
  });
};

const informasiFilterByKategori = (kategori, length, start = 0) => {
  return http.post("/portal/get-informasi-by-kategori", {
    start: start,
    length: length,
    kategori_id: kategori,
  });
};

const informasiAllJudul = () => {
  return http.post("portal/get-informasi-judul");
};

const informasiFilterByTags = (tag, length, start = 0) => {
  return http.post("/portal/get-informasi-by-tags", {
    start: start,
    length: length,
    tags: tag,
  });
};

const rilisMediaList = (length, type = 1, start = 0) => {
  return http.post("/portal/get-rilis-media", {
    page: start,
    limit: length,
    type: type,
  });
};

const rilisMediaListDashboard = (length) => {
  return http.post("/portal/get-rilis-media_2");
};

const informasiFindBySlug = (slug) => {
  return http.post("/portal/get-informasi-by-slug", {
    slug: slug,
  });
};

const informasiFindById = (id) => {
  return http.post("/portal/get-informasi-by-id", {
    id: id,
  });
};

const informasiSearch = (keyword, length, start = 0) => {
  return http.post("/portal/get-informasi-search", {
    start: start,
    length: length,
    keyword: keyword,
  });
};

const tagsList = (jenis) => {
  return http.post("/portal/get-tags", {
    jenis: jenis,
  });
};

const faqList = (length, start = 0) => {
  return http.post("/portal/get-faq", {
    start: start,
    length: length,
  });
};

const faqById = (faqId) => {
  return http.post("/portal/get-faq-by-id", {
    id: faqId,
  });
};

const faqByKategori = (kategori, length, start = 0) => {
  return http.post("/portal/get-faq-by-kategori", {
    start: start,
    length: length,
    kategori_id: kategori,
  });
};

// mitra
const mitraList = (akademiSlug = null, sortBy, perPage, page = 0) => {
  return http.get(
    "/mitra/with-relasi",
    {
      params: {
        akademi: akademiSlug,
        order_by: sortBy,
        limit: perPage,
        page: page,
      },
    },
    {
      headers: headers,
    },
  );
};

const widgetList = () => {
  return http.get("/home/widget");
};

const hitNikCapil = (nik, nama) => {
  return httpDukcapil.post("/hitnik", {
    NIK: nik,
    NAMA_LGKP: nama,
    NO_KK: "",
    JENIS_KLMIN: "",
    TMPT_LHR: "",
    TGL_LHR: "",
    STATUS_KAWIN: "",
    JENIS_PKRJN: "",
    NAMA_LGKP_IBU: "",
    ALAMAT: "",
    NO_PROP: "",
    NO_KAB: "",
    NO_KEC: "",
    NO_KEL: "",
    PROP_NAME: "",
    KAB_NAME: "",
    KEC_NAME: "",
    KEL_NAME: "",
    NO_RT: "",
    NO_RW: "",
    TRESHOLD: "100",
  });
};

const pinResendRegister = () => {
  const userEmail = JSON.parse(
    EncryptionHelper.decrypt(PortalHelper.getUserEmailRegister()),
  )?.email;
  return http.post(
    "/auth/resend-otp",
    {
      email: userEmail,
      verifikasi_otp: "email",
    },
    {
      headers: {
        "Content-Type": "application/json",
        Authorization: PortalHelper.tokenGetFromCookie(),
      },
    },
  );
};

const pinResend = () => {
  const userData = PortalHelper.getUserData();
  return http.post(
    "/auth/resend-otp",
    {
      verifikasi_otp:
        userData &&
        (userData.verifikasi_otp == "email" || userData.verifikasi_otp == "sms")
          ? userData.verifikasi_otp
          : "email",
    },
    {
      headers: {
        "Content-Type": "application/json",
        Authorization: PortalHelper.tokenGetFromCookie(),
      },
    },
  );
};

const sendPinRegister = (email) => {
  return http.post("/auth/reg-send-otp", {
    email: email,
  });
};

const sendEmailOtpAdmin = () => {
  return http.post("/auth/admin-email/send-otp");
};

const verifyEmailOtpAdmin = (pin) => {
  return http.post("/auth/admin-email/verify-otp", {
    pin: pin,
  });
};

// captcha validate
const captchaValidate = (captchaToken) => {
  return http.post("home/captcha-verify", {
    response: captchaToken,
  });
};

// web content
const contentGet = (id, useNetwork = false) => {
  if (useNetwork) {
    return http.get("/home/content", {
      params: {
        id: id,
      },
    });
  }
  let index = "/home/content?id=" + id;
  let func = () =>
    http.get("/home/content", {
      params: {
        id: id,
      },
    });
  // 3 jam
  let duration = 3;
  let transform = (data) => {
    // console.log(data)
    let tmp = data.result[0];
    if (!tmp.text_content) {
      return data;
    }
    let str = `${tmp.text_content}`;
    const newStr = str.replace(/\"assets/, '"/assets');
    tmp.text_content = newStr;
    data.result[0] = tmp;
    return data;
  };
  return CacheHelper.HttpCacheWrapper({
    func: func,
    index: index,
    duration: duration,
    axiosResultDataTransform: transform,
  });
};

const kontakGet = () => {
  return http.get("/home/kontak");
};

const footerCopyRight = () => {
  return contentGet(5);
};

const footerAlamat = () => {
  return contentGet(2);
};

const footerPranala = () => {
  return contentGet(3);
};

const footerInformasi = () => {
  return contentGet(4);
};

const footerPhone = () => {
  return contentGet(7);
};

const footerJamKerja = () => {
  return contentGet(8);
};

const footerEmail = () => {
  return contentGet(9);
};

const footerSocMed = () => {
  return contentGet(10);
};

// profil informasi lainnya masih menggunakan data dummy
const getBeasiswa = () => {
  // return httpDummy("beasiswa");
  return http.get("/beasiswa");
};

const getLowongan = () => {
  // return httpDummy("lowongan");
  return http.get("/lowongan-pekerjaan", {
    params: {
      limit: 4,
    },
  });
};

// waktu prod set ke false
const generalDataSiteManajamenen = (useNetwork = true) => {
  let func = () => http.get("/home/setting"); // ubah lagi 20 Juni 2023 -- returnya ga mau berubah yang sebelumnya
  let duration = 6;
  return useNetwork
    ? func()
    : CacheHelper.HttpCacheWrapper({
        duration: duration,
        func: func,
        index: "setting",
      });
};

const generalMenuSiteManajamenen = (useNetwork = true) => {
  let func = () => http.get("/home/setting-menu");
  let duration = 6;
  return useNetwork
    ? func()
    : CacheHelper.HttpCacheWrapper({
        duration: duration,
        func: func,
        index: "setting",
      });
};

const getDetailPageMenu = (slug) => {
  return http.get("/home/page", { params: { url: slug } });
};

const mitraSentOtp = (email) => {
  return http.post("/auth/reg-resend-otp", { email: email });
};

const sendPhoneOTP = (phoneNumber) => {
  return http.post("/send-otp-phone", {
    nomor_handphone: phoneNumber,
  });
};

const verifyPhoneOTP = (phoneNumber, pin) => {
  return http.post(
    "/verify-otp-phone",
    {
      nomor_handphone: phoneNumber,
      pin: pin,
    },
    {
      headers: {
        "Content-Type": "application/json",
        Authorization: PortalHelper.tokenGetFromCookie(),
      },
    },
  );
};

const checkNomorHpDuplicate = (phoneNumber) => {
  return http.post(
    "/profil/profil-cek-bynotelp",
    {
      notelp: phoneNumber,
    },
    {
      headers: {
        "Content-Type": "application/json",
        Authorization: PortalHelper.tokenGetFromCookie(),
      },
    },
  );
};

const checkEmailDuplicate = (email) => {
  return http.post(
    "/profil/profil-cek-byemail",
    {
      email: email,
    },
    {
      headers: {
        "Content-Type": "application/json",
        Authorization: PortalHelper.tokenGetFromCookie(),
      },
    },
  );
};

const mitraSave = async (payload) => {
  const res = await http.post("/mitra/portal-tambah-mitra-s3", payload);
  if (!res?.data?.result?.Status) throw new Error(res?.data?.result?.Message);

  const [Data] = res?.data?.result?.Data || [];
  return Data || Object.fromEntries(payload.entries());
};

const refreshUserData = () => {
  return http.post("/auth/login_refresh");
};

const aktivasiUser = (tkn) => {
  return http.post("/aktivasi", {
    token: tkn,
  });
};

const checkUserEligible = (idPelatihan) => {
  return http.post("/auth/eligible", {
    pelatihan_id: idPelatihan,
  });
};

const mitraById = (id) => {
  return http.get("/mitra/byidmitra/" + id);
};

const validasiSertifikat = (idSertifikat) => {
  return http.get("/cek-sertifikat/cek", {
    params: { registrasi: idSertifikat },
  });
};
const publikasiAOS = () => {
  return http.get("/publikasi/view_aos");
};

const checkHistoryPelatihan = () => {
  return http.post("/profil/profil-survey-evaluasi/history");
};

const padiUMKMRegisterUser = () => {
  return http.post("/exchange/padi/register-user");
};

const captchaV2load = () => {
  return http.get("/auth/reload-captcha");
};

const PortalService = {
  checkHistoryPelatihan,
  verifyEmailOtpAdmin,
  sendEmailOtpAdmin,
  aktivasiUser,
  refreshUserData,
  mitraSave,
  verifyPhoneOTP,
  sendPhoneOTP,
  addView,
  kontakGet,
  contentGet,
  emailPinVerif,
  kodeNegaraList,
  pinResend,
  captchaValidate,
  eventById,
  login,
  bannerList,
  sidebarList,
  akademiList,
  akademiDetail,
  pelatihanList,
  pelatihanJadwalHeader,
  pelatihanRelated,
  temaList,
  temaPopolerList,
  eventList,
  imagetronList,
  pelatihanDetail,
  artikelCarifull,
  artikelFilterByKategori,
  artikelList,
  kategoriList,
  informasiFindBySlug,
  informasiSearch,
  informasiList,
  informasiFilterByKategori,
  tagsList,
  artikelFindBySlug,
  artikelFilterByTags,
  informasiFilterByTags,
  informasiAllJudul,
  artikelAllJudul,
  faqList,
  faqById,
  faqByKategori,
  mitraList,
  rilisMediaList,
  sliderList,
  userFetchData,
  register,
  emailVerif,
  informasiFindById,
  artikelFindById,
  videoList,
  pelatihanFilter,
  galleryList,
  gallerySetting,
  galleryCount,
  resetPassword,
  lupaPassword,
  widgetList,
  footerCopyRight,
  footerAlamat,
  footerPranala,
  footerEmail,
  footerInformasi,
  footerJamKerja,
  footerPhone,
  footerSocMed,
  getBeasiswa,
  getLowongan,
  generalDataSiteManajamenen,
  generalMenuSiteManajamenen,
  sendPinRegister,
  getDetailPageMenu,
  pinResendRegister,
  registerMitra,
  hitNikCapil,
  mitraSentOtp,
  checkNomorHpDuplicate,
  checkEmailDuplicate,
  pelatihanListJadwal,
  rilisMediaListDashboard,
  checkUserEligible,
  mitraById,
  validasiSertifikat,
  publikasiAOS,
  padiUMKMRegisterUser,
  captchaV2load,
};

export default PortalService;
