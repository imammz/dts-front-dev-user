import http from "../src/http-common";
import PortalHelper from "../src/helper/PortalHelper";
import PortalService from "./PortalService";

const authenticatedHeader = {
  "Content-Type": "application/json",
  Authorization: PortalHelper.tokenGetFromCookie(),
};

const jadwalHeader = (bulan, tipe) => {
  return http.get(`/home/pelatihan/jadwal/header`, {
    params: {
      bulan: bulan,
      tipe: tipe,
    },
  });
};

const pendaftaran = (pelatihanID) => {
  return http.get(`/pelatihan/${pelatihanID}/pendaftaran`);
};

const pendaftaranSubmit = (pelatihanID, payload) => {
  return http.post(`/pelatihan/${pelatihanID}/pendaftaran`, payload, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
};

const bookmarkByPelatihan = (pelatihanID) => {
  return http.get(`/pelatihan/${pelatihanID}/bookmark`, {
    headers: authenticatedHeader,
  });
};

const bookmarkByPelatihanUpdate = (pelatihanID) => {
  return http.post(
    `/pelatihan/${pelatihanID}/bookmark`,
    {},
    {
      headers: authenticatedHeader,
    },
  );
};

const batalPendaftaran = async (pelatihanID, alasanId, password) => {
  return http
    .post("/profil/profil-batal-pelatihan", {
      pelatihan_id: pelatihanID,
      alasan_id: alasanId,
      password: password,
    })
    .then(async (result) => {
      try {
        let resp = await PortalService.refreshUserData();
        if (resp.data?.success) {
          PortalHelper.refreshUserCookies(resp.data.result?.data?.user);
          return Promise.resolve(result);
        } else {
          throw Error(resp.data.message);
        }
      } catch (err) {
        return Promise.reject(err);
      }
    });
};

const fetchListAlasanBatal = () => {
  return http.post("/umum/list-alasan-batal", {});
};

const PelatihanService = {
  jadwalHeader,
  pendaftaran,
  pendaftaranSubmit,
  bookmarkByPelatihan,
  bookmarkByPelatihanUpdate,
  batalPendaftaran,
  fetchListAlasanBatal,
};

export default PelatihanService;
