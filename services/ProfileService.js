import http, { httpDummy } from "../src/http-common";
import PortalHelper from "../src/helper/PortalHelper";
import PortalService from "./PortalService";

const VIEWED_BROADCAST_KEY = "bcs";

const profil = (version = 1) => {
  return version == 2
    ? http.get("/profil/profil-detail2")
    : http.get(`/profil`);
};

const update = (payload) => {
  return http.post("/profil", payload, {
    headers: { "Content-Type": "multipart/form-data" },
  });
};

const updateFotoProfil = (payload) => {
  return http.post("/profil/foto", payload, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
};

const profilDashboard = () => {
  return http.get("/profil/profil-dashboard");
};

const profilPelatihan = (page = 1, limit = 10, order = "terbaru") => {
  return http.get("/profil/profil-pelatihan", {
    params: { page: page, order: order, limit: limit },
  });
};

const profilSurvey = (page = 1, limit = 10, order = "terbaru") => {
  return http.get("/profil/profil-survey", {
    params: { page: page, order: order, limit: limit },
  });
};

const profilSubstansi = (page = 1, limit = 10, order = "terbaru") => {
  return http.get("/profil/profil-substansi", {
    params: { page: page, order: order, limit: limit },
  });
};

const profilTrivia = (page = 1, limit = 10, order = "terbaru") => {
  return http.get("/profil/profil-trivia", {
    params: { page: page, order: order, limit: limit },
  });
};

// pengerjaan substansi
const profilStatSubstansi = (id_pelatihan, id) => {
  return http.post("/profil/profil-sta-substansi", {
    substansi_id: id,
    training_id: id_pelatihan,
  });
};

const profilSubstansiStart = (id_pelatihan, id) => {
  return http.post("/profil/profil-substansi-start", {
    substansi_id: id,
    training_id: id_pelatihan,
  });
};

const getPertanyaanTestSubstansi = (id = "") => {
  // return httpDummy("pertanyaan-test-substansi")
  return http.post("/profil/profil-soal-substans", { substansi_id: id });
};

const profilSubmitSubstansi = async (training_id, answer_json, category) => {
  return http
    .post("/profil/profil-subtansisubmit", {
      training_id: training_id,
      answer_json: answer_json,
      type: category,
    })
    .then(async (result) => {
      try {
        let resp = await PortalService.refreshUserData();
        if (resp.data?.success) {
          PortalHelper.refreshUserCookies(resp.data.result?.data?.user);
          return Promise.resolve(result);
        } else {
          throw Error(resp.data.message);
        }
      } catch (err) {
        console.log(err);
        return Promise.reject(err);
      }
    });
};

const getPertanyaanSurvey = async (id) => {
  const resp = await http.post("/list_soal_survey_byid", {
    id_survey: id,
    start: 0,
    rows: 10000,
  });
  return {
    total: resp?.data?.result?.JumlahData || 0,
    data: resp?.data?.result?.Data || [],
  };
};

const profilAktivitas = () => {
  return http.get("/profil-aktivitas");
};

//pengerjaan trivia
const profilStatTrivia = (id_pelatihan, id) => {
  return http.post("/profil/profil-status-trivia", {
    trivia_id: id,
    training_id: id_pelatihan,
  });
};

const profilTriviaStart = (id_pelatihan, id) => {
  return http.post("/profil/profil-trivia-start", {
    trivia_id: id,
    training_id: id_pelatihan,
  });
};

const getPertanyaanTrivia = (id = "") => {
  return http.post("/profil/profil-soal-trivia", { trivia_id: id });
};

const profilSubmitTrivia = (training_id, question_bank_id, answer_json) => {
  return http.post("/profil/profil-trivia-submit", {
    question_bank_id: question_bank_id,
    training_id: training_id,
    answer: answer_json,
  });
};

const pelatihan = (pelatihanID = null) => {
  return http.get("/profil/pelatihan", {
    params: {
      pelatihan_id: pelatihanID,
    },
  });
};

const bookmark = () => {
  return http.get("/profil/pelatihan/bookmark");
};

const aktivitas = () => {
  return http.get("/profil/aktivitas");
};

const notification = (page, limit, status) => {
  return http.post("/profil/profil-notification", {
    page: page,
    limit: limit,
    status_baca: status,
  });
};

const notificationGetDetail = (id, status = 0) => {
  if (status == 0) {
    notificationSetRead(id);
  }
  return http.post("/profil/profil-notification-detail", { id: id });
};

const notificationSetRead = (id) => {
  return http.post("/profil/profil-update-notification", { id: id });
};

const unreadNotificationBadge = () => {
  return http.get("/profil/profil-notification-badge");
};

// setting menu
const ubahPassword = (p_active, p_baru, p_konfirm) => {
  return http.post("/profil-ubahpass", {
    password_active: p_active,
    password: p_baru,
    password_confirmation: p_konfirm,
  });
};

// ubah email
const reqOtpEmail = (email_lama, email_baru) => {
  return http.post("/auth/change-email/send-otp", {
    email: email_lama,
    newemail: email_baru,
  });
};

const verifyOtpEmail = (pin, email_baru) => {
  return http.post("/auth/change-email/verify-otp", {
    pin: pin,
    newemail: email_baru,
  });
};

// Ubah no HP
const reqOtpEmailHp = (hp, newhp) => {
  return http.post("/auth/change-hp/send-otp", { hp: hp, newhp: newhp });
};

const verifyOtpEmailHp = (pin, newhp) => {
  return http.post("/auth/change-hp/verify-otp", { pin: pin, newhp: newhp });
};

const cekIsUserMitra = () => {
  return http.get("/profil/profil-user-mitra");
};

const cekStatusPelatihanPeserta = (idPelatihan) => {
  return http.post("/profil/status-pelatihan-peserta", {
    pelatian_id: idPelatihan,
  });
};

const updateMandatoryData = (payload) => {
  return http.post("/profil/profil-update-bynik", payload);
};

const checkStatusSurveiEvaluasiPelatihan = (id_pelatihan) => {
  return http.post("/profil/profil-survey-evaluasi", {
    pelatihan_id: id_pelatihan,
  });
};

// get broadcast
const getBroadcast = (isLoggedin = false) => {
  let url = isLoggedin ? "/broadcast/user" : "/broadcast/public";
  return http.get(url, {});
};

const logout = () => {
  return http.post("/logout");
};

const ProfilService = {
  profil,
  update,
  updateFotoProfil,
  updateMandatoryData,
  profilDashboard,
  profilPelatihan,
  profilSurvey,
  profilSubstansi,
  profilTrivia,
  profilAktivitas,
  pelatihan,
  bookmark,
  aktivitas,
  profilSurvey,
  notification,
  unreadNotificationBadge,
  notificationGetDetail,
  notificationSetRead,
  cekStatusPelatihanPeserta,
  // subvit
  profilStatSubstansi,
  getPertanyaanTestSubstansi,
  profilSubstansiStart,
  profilSubmitSubstansi,
  profilStatTrivia,
  getPertanyaanSurvey,
  profilTriviaStart,
  getPertanyaanTrivia,
  profilSubmitTrivia,
  checkStatusSurveiEvaluasiPelatihan,
  getBroadcast,
  // setting
  ubahPassword,
  reqOtpEmail,
  verifyOtpEmail,
  reqOtpEmailHp,
  verifyOtpEmailHp,
  cekIsUserMitra,

  logout,

  // Constant
  VIEWED_BROADCAST_KEY,
};

export default ProfilService;
