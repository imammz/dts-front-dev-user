import http, { httpDummy } from "../src/http-common";
import PortalHelper from "../src/helper/PortalHelper";
import PortalService from "./PortalService";

const start = (akademi_id, pelatihan_id, sqb_id) => {
  return http.post(`/survey/start`, {
    akademi_id: akademi_id,
    pelatihan_id: pelatihan_id,
    sqb_id: sqb_id,
  });
};

const base = (akademi_id, pelatihan_id, sqb_id) => {
  return http.get(`/survey/base`, {
    params: {
      akademi_id: akademi_id,
      pelatihan_id: pelatihan_id,
      sqb_id: sqb_id,
    },
  });
};

const detail = (akademi_id, pelatihan_id, sqb_id) => {
  return http.get(`/survey/detail`, {
    params: {
      akademi_id: akademi_id,
      pelatihan_id: pelatihan_id,
      sqb_id: sqb_id,
    },
  });
};

const submit = async (akademi_id, pelatihan_id, sqb_id, answer_json) => {
  return http
    .post(`/survey/submit`, {
      akademi_id: akademi_id,
      pelatihan_id: pelatihan_id,
      sqb_id: sqb_id,
      participant_answers: answer_json,
    })
    .then(async (result) => {
      try {
        let resp = await PortalService.refreshUserData();
        if (resp.data?.success) {
          PortalHelper.refreshUserCookies(resp.data.result?.data?.user);
          return Promise.resolve(result);
        } else {
          throw Error(resp.data.message);
        }
      } catch (err) {
        console.log(err);
        return Promise.reject(err);
      }
    });
};

const SurveyService = {
  start,
  base,
  detail,
  submit,
};

export default SurveyService;
