/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  output: 'standalone',
};

const {
  PHASE_DEVELOPMENT_SERVER,
  PHASE_PRODUCTION_BUILD,
} = require("next/constants");

module.exports = (phase) => {
  // when started in development mode `next dev` or `npm run dev` regardless of the value of STAGING environmental variable
  const isDev = phase === PHASE_DEVELOPMENT_SERVER;
  // when `next build` or `npm run build` is used
  const isProd =
    phase === PHASE_PRODUCTION_BUILD && process.env.STAGING !== "1";
  // when `next build` or `npm run build` is used
  const isStaging =
    phase === PHASE_PRODUCTION_BUILD && process.env.STAGING === "1";
  output: 'standalone';
  const env = {
    poweredByHeader: (() => {
      return false;
    })(),
    RESTURL_SESSIONS: (() => {
      if (isDev) return "https://api.dev.sdmdigital.id/";
      if (isProd) return "https://api.dev.sdmdigital.id/";
      if (isStaging) return "https://api.dev.sdmdigital.id/";
      return "RESTURL_SESSIONS:not (isDev,isProd && !isStaging,isProd && isStaging)";
    })(),
    URL_IMG: (() => {
      if (isDev) return "https://bucket.dev.sdmdigital.id/";
      if (isProd)
        return "https://bucket.dev.sdmdigital.id/";
      if (isStaging)
        return "https://bucket.dev.sdmdigital.id/";
      return "RESTURL_SESSIONS:not (isDev,isProd && !isStaging,isProd && isStaging)";
    })(),
    STORAGE_URL: (() => {
      if (isDev) return "https://bucket.dev.sdmdigital.id/";
      if (isProd)
        return "https://bucket.dev.sdmdigital.id/";
      if (isStaging)
        return "https://bucket.dev.sdmdigital.id/";
      return "RESTURL_SESSIONS:not (isDev,isProd && !isStaging,isProd && isStaging)";
    })(),
    BASE_URL: (() => {
      if (isStaging) return "https://digitalent.dev.sdmdigital.id/";
      // if (isDev) return 'https://localhost:3000/'
      if (isDev) return "https://digitalent.dev.sdmdigital.id/";
      if (isProd) return "https://digitalent.dev.sdmdigital.id/"; //ganti jadi domain server production
      return "RESTURL_SESSIONS:not (isDev,isProd && !isStaging,isProd && isStaging)";
    })(),
    ADMIN_BASE_URL: (() => {
      if (isStaging) return "https://dtsadmin.dev.sdmdigital.id/";
      // if (isDev) return 'https://localhost:3000/'
      if (isDev) return "https://dtsadmin.dev.sdmdigital.id/";
      if (isProd) return "https://dtsadmin.dev.sdmdigital.id/"; //ganti jadi domain server production
      return "RESTURL_SESSIONS:not (isDev,isProd && !isStaging,isProd && isStaging)";
    })(),
    SENTRY_DSN: (() => {
      if (isStaging) return "https://361b4b66ad83367307770cc4723eb8bb@sentry-dev.sdmdigital.id/3";
      if (isDev) return "https://361b4b66ad83367307770cc4723eb8bb@sentry-dev.sdmdigital.id/3";
      if (isProd) return "https://361b4b66ad83367307770cc4723eb8bb@sentry-dev.sdmdigital.id/3";
      return "RESTURL_SESSIONS:not (isDev,isProd && !isStaging,isProd && isStaging)";
    })(),
    PADIUMKM_BASE_URL: (() => {
      if (isStaging) return "https://seller-stg.berasumkm.id/";
      if (isDev) return "https://seller-stg.berasumkm.id/";
      if (isProd) return "https://seller.padiumkm.id/";
      return "RESTURL_SESSIONS:not (isDev,isProd && !isStaging,isProd && isStaging)";
    })(),
    USER_TOKEN_KEY: (() => {
      return "token";
    })(),
    USER_DATA_KEY: (() => {
      return "user_data";
    })(),
    USER_REGISTRATION_EMAIL_KEY: (() => {
      return "user_regist_email";
    })(),
    CAPTCHA_SITEKEY: (() => {
      return "6Ldjd3whAAAAAKG-i-bP0mraKjUbabBEJrQxBGdd";
    })(), //udah punya kominfo
    USER_EMAIL_RESET_PASSWORD: (() => {
      return "user_forgot_pass_email";
    })(),
    USER_EMAIL_REGISTER: (() => {
      return "user_email_register";
    })(),
    VERIFY: (() => {
      return "verify";
    })(),
    PELATIHAN_KEY: (() => {
      return "pelatihan_temp";
    })(),
    // mulai daru sini ada kondisi
    AVATAR_NAME: (() => {
      return "https://ui-avatars.com/api/";
    })(),
    WA_URL: (() => {
      return "https://wa.me/";
    })(),
    INSTAGRAM_MEDIA_URL: (() => {
      return "https://graph.instagram.com/me/media";
    })(),
    MAJAPAHIT_BEASISWA: (() => {
      return "https://api.dts.sdmdigital.id/beasiswa/api/get-scholarship-data";
    })(),
    CHECKTIMEZONE: (() => {
      return "http://worldtimeapi.org/api/timezone/";
    })(),
    SSO_FORGOT_PASSWORD: (() => {
      return "https://sso.dev.sdmdigital.id/realms/dignas.space/login-actions/reset-credentials?client_id=api.dev.sdmdigital.id&tab_id=a2kRySv00qw";
    })(),
    LMS_BASE_URL: (() => {
      if (isStaging) return "https://lms.dev.sdmdigital.id/";
      // if (isDev) return 'https://localhost:3000/'
      if (isDev) return "https://lms.dev.sdmdigital.id/";
      if (isProd) return "https://lms.dev.sdmdigital.id/"; //ganti jadi domain server production
      return "LMS_BASE_URL:not (isDev,isProd && !isStaging,isProd && isStaging)";
    })(),
    PRESENSI_BASE_URL: (() => {
      if (isStaging) return "https://presensi.dev.sdmdigital.id/";
      // if (isDev) return 'https://localhost:3000/'
      if (isDev) return "https://presensi.dev.sdmdigital.id/";
      if (isProd) return "https://presensi.dev.sdmdigital.id/"; //ganti jadi domain server production
      return "PRESENSI_BASE_URL:not (isDev,isProd && !isStaging,isProd && isStaging)";
    })(),
  };

  // next.config.js object
  return {
    env,
    eslint: {
      // Warning: This allows production builds to successfully complete even if
      // your project has ESLint errors.
      ignoreDuringBuilds: true,
    },
  };
};
