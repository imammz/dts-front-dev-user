# Base Image
FROM node:17.6.0-alpine3.15 AS base
LABEL maintainer="danigeeks <danigeeks@gmail.com>"
WORKDIR /app

# Dependencies
FROM base AS deps
COPY package* /app/
RUN apk add --no-cache libc6-compat \
    && npm ci --legacy-peer-deps

# Builder
FROM base AS builder
COPY . /app
# Update URLs and keys in all files
RUN find /app/ -type f -exec sed -i -e 's/front.dev.dtsnet.net/dtsadmin.dev.sdmdigital.id/g' \
                               -e 's/front-user.dev.dtsnet.net/digitalent.dev.sdmdigital.id/g' \
                               -e 's/dtsapi.dtsnet.net/api.dev.sdmdigital.id/g' \
                               -e 's/back.dev.dtsnet.net/api.dev.sdmdigital.id/g' \
                               -e 's/6LfiDbogAAAAAIg9C6ywnUTbOYbcwiFQKU5bYU2z/6Ldjd3whAAAAAKG-i-bP0mraKjUbabBEJrQxBGdd/g' \
                               -e 's/\/signin-mitra/signin-mitra/g' \
                               -e 's/sso.sdm.dev.sdmdigital.id/sso.dev.sdmdigital.id/g' \
                               -e 's/sdm.sdmdigital.id/sdm.dev.sdmdigital.id/g' {} \;
# Copy the node_modules from the deps stage
COPY --from=deps /app/node_modules /app/node_modules
# Copy configuration files
COPY ./elastic-apm-node.js-dev /app/elastic-apm-node.js
COPY ./next.config.js-dev /app/next.config.js

ENV NODE_ENV=production \
    NEXT_PRIVATE_STANDALONE=true
RUN export NEXT_PRIVATE_STANDALONE=true

# Install necessary packages
RUN npm install next moment-timezone --legacy-peer-deps \
    && npx browserslist@latest --update-db \
    && npm run build

# Runner
FROM base AS runner

RUN addgroup --system --gid 1001 nodejs \
    && adduser --system --uid 1001 nextjs

COPY --from=builder /app/public /app/public
COPY --from=builder --chown=nextjs:nodejs /app/.next/standalone /app/
COPY --from=builder --chown=nextjs:nodejs /app/.next/static /app/.next/static

USER nextjs
ENV NODE_ENV=production \
    PORT=3000
EXPOSE 3000
CMD ["node", "server.js"]
