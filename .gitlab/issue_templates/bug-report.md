## What Happened?
<!-- Jelaskan detail issue (dalam bentuk point) !-->

## Steps to reproduce
<!-- Langkah-langkah untuk melakukan reproduce (dalam bentuk point) !-->

## Console / Inspect Report
<!-- Cantumkan hasil inspect disini !-->

## Link / End-Point
<!-- Cantumkan link/end-point disini !-->

## Screenshots
<!-- lampirkan screenshoot atau screen record !-->

## Expected result
<!-- Jelaskan apa yang seharusnya terjadi !-->

## Reported by
<!-- cantumkan laporan dari siapa !-->

## Priority/Severity
<!-- Pilih prioritas !-->
- [ ] High
- [ ] Medium
- [ ] Low
