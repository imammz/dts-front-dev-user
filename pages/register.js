// wizard resgitrasi
import React, { useState, useEffect, useRef } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import Link from "next/link";
import { useRouter } from "next/router";
import PortalService from "../services/PortalService";
import ValidationMessage from "../src/validation-message";
import SSOInfo from "../components/SSOSInfo";
import Failed from "../components/Modal/Failed";
import Success from "../components/Modal/Success";
import PortalHelper from "../src/helper/PortalHelper";
import FlipMove from "react-flip-move";
import EncryptionHelper from "../src/helper/EncryptionHelper";
import VerifikasiEmail from "../components/Modal/VerifikasiEmailModal";
import ReCAPTCHA from "react-google-recaptcha";
import _ from "lodash";
import ButtonWithLoading from "../components/ButtonWithLoading";
import Modal from "react-bootstrap/Modal";
import eventBus from "../components/EventBus";
// import eventBus

export default function register({ showLoadingModal }) {
  const [loading, setLoading] = useState(false);
  const [isLoadingGetLinks, setIsLoadingGetLinks] = useState(true);
  const [showFailModal, setShowFailModal] = useState(false);
  const [showSuccessModal, setShowSuccessModal] = useState(false);
  const [showVerifikasiModal, setShowVerifikasiModal] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showPasswordConf, setShowPasswordConf] = useState(false);
  const [emailChecked, setEmailChecked] = useState(false);
  const [emailCheckResult, setEmailCheckResult] = useState(null);
  const [emailCheckLoading, setEmailCheckLoading] = useState(false);
  const [serverMessage, setServerMessage] = useState("");
  const [otpErrorMessage, setOtpErrorMessage] = useState("");
  const [captchaErrorMessage, setCaptchaErrorMessage] = useState();
  const [userCaptchaResponse, setUserCaptchaResponse] = useState(null);
  const [agreement, setAgreement] = useState(false);
  const registerForm = useRef(null);
  const submitRef = useRef(null);
  const [siteManajemenData, setSiteManajemenData] = useState({
    alamat: "",
    color: [],
    created_at: "",
    created_by: "",
    email: "",
    footer_logo: "",
    header_logo: "",
    koordinat: [],
    link: [],
    logo_description: "",
    notelp: [],
    social_media: [],
  });

  const closeFailModal = () => {
    showLoadingModal(false);
    setShowFailModal(false);
  };
  const closeSuccessModal = () => {
    showLoadingModal(false);
    setShowSuccessModal(false);
    showLoadingModal(true);
    router.replace({
      pathname: "/verify-user/verifikasi",
    });
  };

  const makeid = (length) => {
    var result = "";
    var characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  };

  const resendEmail = async () => {
    try {
      const email = JSON.parse(
        EncryptionHelper.decrypt(PortalHelper.getUserEmailRegister()),
      )?.email;
      const resp = await PortalService.sendPinRegister(email);
      if (!resp.data.success) {
        throw Error(resp.data.result.message);
      }
    } catch (err) {
      const message =
        typeof err == "string" ? err : err?.response?.data?.message;
      setServerMessage(message ?? "Terjadi kesalahan.");
      setShowFailModal(true);
    }
  };

  // ambil data link
  useEffect(async () => {
    let settingData = await PortalService.generalDataSiteManajamenen();
    // console.log(settingData)
    if (settingData.data?.success) {
      // console.log(settingData.data.result.Data)
      const {
        alamat,
        color,
        created_at,
        created_by,
        email,
        footer_logo,
        header_logo,
        koordinat,
        link,
        logo_description,
        notelp,
        social_media,
      } = settingData.data.result[0];
      setSiteManajemenData({
        alamat: alamat,
        color: JSON.parse(color),
        created_at: created_at,
        created_by: created_by,
        email: email,
        footer_logo: footer_logo,
        header_logo: header_logo,
        koordinat: koordinat?.split(","),
        logo_description: logo_description,
        notelp: JSON.parse(notelp),
        link: JSON.parse(link),
        social_media: JSON.parse(social_media),
      });
      // console.log(siteManajemenData)
    } else {
      settingData = await PortalService.generalDataSiteManajamenen(true);
      const {
        alamat,
        color,
        created_at,
        created_by,
        email,
        footer_logo,
        header_logo,
        koordinat,
        link,
        logo_description,
        notelp,
        social_media,
      } = settingData.data.result[0];
      setSiteManajemenData({
        alamat: alamat,
        color: JSON.parse(color),
        created_at: created_at,
        created_by: created_by,
        email: email,
        footer_logo: footer_logo,
        header_logo: header_logo,
        koordinat: koordinat?.split(","),
        logo_description: logo_description,
        notelp: JSON.parse(notelp),
        link: JSON.parse(link),
        social_media: JSON.parse(social_media),
      });
    }
    setIsLoadingGetLinks(false);
  }, []);

  const validationSchema = Yup.object({
    email: Yup.string()
      .required(ValidationMessage.required.replace("__field", "Email"))
      .email(ValidationMessage.invalid.replace("__field", "Email"))
      .test(
        "unique",
        ValidationMessage.unique.replace("__field", "Email"),
        async (val) => {
          if (!emailChecked) {
            try {
              setEmailChecked(true);
              setEmailCheckLoading(true);
              const resp = await PortalService.checkEmailDuplicate(val);
              setEmailCheckLoading(false);
              const res = resp.data;
              if (res.success) {
                if (res.result.data.length == 0) {
                  setEmailCheckResult(true);
                  return true;
                } else {
                  setEmailCheckResult(false);
                  return false;
                }
                // const isVerified = res.result.data.filter(
                //   (elem) => !elem.email_verifikasi
                // );
                // if (isVerified.length > 0) {
                //   setEmailCheckResult(true);
                //   return true;
                // }
                // setEmailCheckResult(false);
                // return false;

                // setEmailCheckResult(
                //   res.result.data.length == 0 ||
                //     !res.result.data[0].email_verifikasi
                // );
                // return (
                //   res.result.data.length == 0 ||
                //   !res.result.data[0].email_verifikasi
                // );
              } else {
                throw Error("Belum bisa memeriksa email.");
              }
            } catch (error) {
              setEmailCheckLoading(false);
              console.log(error);
              setEmailCheckResult(false);
              return false;
            }
          } else return emailCheckResult ?? false;
        },
      ),
    password: Yup.string().matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[-._!"`'#%&,:;<>=@{}~\$\(\)\*\+\/\\\?\[\]\^\|])[A-Za-z\d-._!"`'#%&,:;<>=@{}~\$\(\)\*\+\/\\\?\[\]\^\|]{8,}$/,
      ValidationMessage.password.replace("__field", "Password"),
    ),
    password_confirmation: Yup.string().oneOf(
      [Yup.ref("password"), null],
      ValidationMessage.match
        .replace("__field", "Konfirmasi Password")
        .replace("__ref", "Password"),
    ),
  });

  const {
    register,
    watch,
    handleSubmit,
    getValues,
    trigger,
    formState: { errors },
  } = useForm({
    mode: "onBlur",
    resolver: yupResolver(validationSchema),
  });

  const router = useRouter();

  const pinResend = async () => {
    try {
      showLoadingModal(true);
      const resp = await PortalService.pinResend();
      if (resp.data.success) {
        setServerMessage(resp.data.message);
        // setShowSuccessModal(true);
        // showLoadingModal(false);
      } else {
        throw Error(resp.data.message);
      }
    } catch (err) {
      console.log(err);
      const message = err.response?.data?.result?.Message
        ? err.response.data.result.Message
        : err.message;
      // showLoadingModal(false);
      setServerMessage(
        message || "Terjadi kesalahan saat mencoba terhubung dengan server!",
      );
      // setShowFailModal(true);
    }
  };

  class CaptchaError extends Error {
    constructor(message) {
      super(message); // (1)
      this.name = "CaptchaError"; // (2)
    }
  }
  class CapilError extends Error {
    constructor(message) {
      super(message); // (1)
      this.name = "CapilError"; // (2)
    }
  }

  async function onSubmit(data) {
    data.email = data.email.toLowerCase();
    try {
      setLoading(true);
      if (userCaptchaResponse == null) {
        throw new CaptchaError("Captcha tidak boleh kosong");
      }
      showLoadingModal(true);
      const res = await PortalService.sendPinRegister(data.email);
      showLoadingModal(false);

      if (res.data.success) {
        let dataSimpan = EncryptionHelper.encrypt(JSON.stringify(data));

        PortalHelper.saveUserEmailRegister(dataSimpan);

        // PortalHelper.saveUserData(userData);

        setServerMessage(
          res.data.message ||
            "Periksa email " +
              data.email +
              " untuk mendapatkan kode verifikasi.",
        );
        setShowVerifikasiModal(true);
        // setShowSuccessModal(true);
      } else {
        if (res.data?.result) {
          throw Error(PortalHelper.handleErrorResponse(res.data.result));
        } else {
          throw Error(res.data.message);
        }
      }
      showLoadingModal(false);
    } catch (err) {
      console.log(err);
      if (err.name == "CaptchaError") {
        setCaptchaErrorMessage(err.message);
      } else {
        const message = err.response?.data?.result?.Message
          ? err.response.data.result.Message
          : err.message;
        showLoadingModal(false);
        setServerMessage(
          message || "Terjadi kesalahan saat mencoba terhubung dengan server!",
        );
        setShowFailModal(true);
      }
    }
    setLoading(false);
  }

  async function submitUserData(pin) {
    const userData = JSON.parse(
      EncryptionHelper.decrypt(PortalHelper.getUserEmailRegister()),
    );
    userData["otp"] = pin;
    userData["flag_nik"] = 0;
    try {
      const sendData = await PortalService.register(userData);
      if (sendData.data.success) {
        PortalHelper.saveUserToken(sendData.data.result.data.token);
        const userData = sendData.data.result.data.user;
        PortalHelper.saveUserData(userData);
        setShowVerifikasiModal(false);
        eventBus.dispatch("updateMenuLink");
        router.push("/auth/user-profile");
      } else {
        let message = sendData.data.result;
        let errorMessage = "";
        if (message) {
          Object.keys(message).map((key) => {
            // console.log(key);
            errorMessage += message[key].join("\n");
            errorMessage += "\n";
          });
        } else {
          errorMessage = sendData.data.message;
        }
        throw Error(errorMessage);
      }
    } catch (err) {
      console.log("Error:", err);
      let message = err.response?.data?.result
        ? err.response.data.result
        : err.message;
      // showLoadingModal(false);
      setOtpErrorMessage(
        message || "Terjadi kesalahan saat mencoba terhubung dengan server!",
      );
      // setShowFailModal(true);
    }
  }

  return (
    <>
      <div className="row vh-100">
        <div className="col-lg-5 bg-info d-none d-lg-block">
          <div className="container py-8">
            <SSOInfo
              links={siteManajemenData.link}
              isLoading={isLoadingGetLinks}
            />
          </div>
        </div>
        <div className="col-lg-7 mb-0 position-relative">
          <div className="container py-8">
            <div>
              <div className="row mb-2 align-items-end">
                <div className="col-md p-0 mb-md-0">
                  <div className="card-body pt-0 mb-0 ps-3">
                    <div className="mb-5">
                      <a
                        href="/"
                        className="text-blue font-size-lg"
                        data-kt-menu-dismiss="true"
                      >
                        <i className="fa fa-chevron-left me-2"></i>Beranda{" "}
                      </a>
                    </div>
                    <h2 className="fw-bolder mb-1">Registrasi Akun Baru</h2>
                    <p className="font-size-lg mb-0 text-capitalize">
                      Hi, Ayo bergabung menjadi{" "}
                      <span className="text-blue font-size-lg font-weight-medium">
                        #JagoanDigital
                      </span>
                    </p>
                  </div>
                </div>
              </div>
              <form
                className="col-lg-8 col-sm-12 ps-0"
                onSubmit={handleSubmit(onSubmit)}
                ref={registerForm}
              >
                <div className="row px-0">
                  <div className="form-group mb-5 ps-3 pe-0">
                    <div className="row align-items-end">
                      <label for="email">
                        Email
                        <span
                          className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                          title="required"
                        >
                          *
                        </span>
                        {emailCheckLoading && (
                          <span
                            className="icon spinner-border spinner-border-sm"
                            role="status"
                            aria-hidden="true"
                          ></span>
                        )}
                      </label>
                    </div>
                    <input
                      type="text"
                      className={`form-control rounded-3 form-control-sm ${
                        errors.email ? "is-invalid" : ""
                      }`}
                      id="email"
                      placeholder=""
                      name="email"
                      {...register("email")}
                      onChange={() => {
                        setEmailChecked(false);
                      }}
                    />
                    <span className="form-caption font-size-sm text-italic my-2">
                      Masukkan email yang valid
                    </span>{" "}
                    <br />
                    <span className="text-red">{errors.email?.message}</span>
                  </div>
                  <div className="col-12 col-md-6 px-0">
                    <div className="form-group mb-5 ps-3 password-input">
                      <div className="row align-items-end">
                        <label for="pass">
                          Password
                          <span
                            className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                            title="required"
                          >
                            *
                          </span>
                        </label>
                      </div>
                      <div className="input inner-addon right-addon">
                        <span
                          className="icon"
                          onClick={() => {
                            setShowPassword(!showPassword);
                          }}
                        >
                          <FlipMove>
                            {!showPassword && (
                              <i className="fa fa-eye grey"></i>
                            )}
                            {showPassword && (
                              <i className="fa fa-eye-slash "></i>
                            )}
                          </FlipMove>
                        </span>
                        <input
                          placeholder="***********"
                          type={showPassword ? "text" : "password"}
                          className={`form-control rounded-3 form-control-sm ${
                            errors.password ? "is-invalid" : ""
                          }`}
                          id="pass"
                          name="password"
                          {...register("password")}
                        />
                      </div>
                      <span className="form-caption font-size-sm text-italic my-2">
                        {!errors.password
                          ? "Minimal 8 Karakter kombinasi huruf kapital, huruf kecil, angka dan simbol."
                          : ""}
                      </span>
                      <span className="text-red">
                        {errors.password?.message}
                      </span>
                    </div>
                  </div>
                  <div className="col-12 col-md-6 px-0">
                    <div className="form-group ps-3 mb-5">
                      <div className="row align-items-end">
                        <label for="pass2">
                          Konfirmasi Password
                          <span
                            className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                            title="required"
                          >
                            *
                          </span>
                        </label>
                      </div>
                      <div className="input inner-addon right-addon">
                        <span
                          className="icon"
                          onClick={() => {
                            setShowPasswordConf(!showPasswordConf);
                          }}
                        >
                          <FlipMove>
                            {!showPasswordConf && (
                              <i className="fa fa-eye grey"></i>
                            )}
                            {showPasswordConf && (
                              <i className="fa fa-eye-slash "></i>
                            )}
                          </FlipMove>
                        </span>
                        <input
                          placeholder="***********"
                          type={showPasswordConf ? "text" : "password"}
                          className={`form-control rounded-3 form-control-sm ${
                            errors.password_confirmation ? "is-invalid" : ""
                          }`}
                          id="pass2"
                          name="password_confirmation"
                          {...register("password_confirmation")}
                        />
                      </div>
                      <span className="text-red">
                        {errors.password_confirmation?.message}
                      </span>
                    </div>
                  </div>

                  <div className="d-flex align-items-center mb-5 font-size-sm">
                    <div>
                      <ReCAPTCHA
                        sitekey={process.env.CAPTCHA_SITEKEY}
                        onChange={(e) => {
                          setUserCaptchaResponse(e);
                        }}
                      ></ReCAPTCHA>
                    </div>
                    {!userCaptchaResponse && captchaErrorMessage && (
                      <span className="text-red">{captchaErrorMessage}</span>
                    )}
                  </div>
                  <div className="form-group mb-1 ps-3">
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        value=""
                        id="agreementCheckbox"
                        onChange={() => {
                          setAgreement(!agreement);
                        }}
                      />
                      <label
                        className="form-check-label"
                        for="agreementCheckbox"
                      >
                        Saya setuju dengan{" "}
                        <Link href="/syarat-ketentuan">
                          <a className="text-blue font-size-14">
                            Syarat dan Ketentuan
                          </a>
                        </Link>{" "}
                        dan{" "}
                        <Link href="/pemberitahuan-privasi">
                          <a className="text-blue font-size-14">
                            Pemberitahuan Privasi
                          </a>
                        </Link>{" "}
                        yang berlaku
                      </label>
                    </div>
                  </div>

                  <div className="form-row place-order">
                    {/* verified.html                    */}
                    <ButtonWithLoading
                      ref={submitRef}
                      className={
                        "btn btn-block btn-blue rounded-pill" +
                        " " +
                        (!agreement ? "disabled" : "")
                      }
                      type="submit"
                      disabled={emailCheckLoading}
                      loading={loading}
                    >
                      DAFTAR
                    </ButtonWithLoading>
                    {/* Text */}
                    <p className="my-5 text-center">
                      Sudah Punya Akun? &nbsp;
                      <Link href="/login">
                        <a
                          className="text-blue font-size-14 text-underline"
                          onClick={() => {
                            // showLoadingModal(true);
                          }}
                        >
                          Login
                        </a>
                      </Link>
                    </p>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <Failed
        show={showFailModal}
        handleClose={closeFailModal}
        children={serverMessage}
      />
      <Success
        show={showSuccessModal}
        handleClose={closeSuccessModal}
        children={serverMessage}
      />
      <VerifikasiEmail
        show={showVerifikasiModal}
        handleClose={() => {
          setShowVerifikasiModal(false);
          setOtpErrorMessage("");
        }}
        children={serverMessage}
        onVerify={({ pin }) => {
          setOtpErrorMessage("");
          submitUserData(pin);
        }}
        onResend={() => {
          setOtpErrorMessage("");
          resendEmail();
        }}
        errMessage={otpErrorMessage}
      />
    </>
  );
}
