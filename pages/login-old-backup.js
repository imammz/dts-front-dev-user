import React, { useCallback, useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import Link from "next/link";
import { useRouter } from "next/router";
import PortalService from "../services/PortalService";
import ValidationMessage from "../src/validation-message";
import SSOInfo from "../components/SSOSInfo";
import PortalHelper from "../src/helper/PortalHelper";
import Failed from "../components/Modal/Failed";
import FlipMove from "react-flip-move";
import ReCAPTCHA from "react-google-recaptcha";
import Moment from "react-moment";
import NotificationHelper from "../src/helper/NotificationHelper";
import _ from "lodash";
import GeneralService from "../services/GeneralService";
import ButtonWithLoading from "../components/ButtonWithLoading";

export default function Login({ showLoadingModal }) {
  const [loading, setLoading] = useState(false);
  const [showFailModal, setShowFailModal] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [captchaErrorMessage, setCaptchaErrorMessage] = useState();
  const [userCaptchaResponse, setUserCaptchaResponse] = useState(null);

  const [showPassword, setShowPassword] = useState(false);
  const [needReset, setNeedReset] = useState(true);

  // data links disamping
  const [isLoadingGetLinks, setIsLoadingGetLinks] = useState(true);
  const [siteManajemenData, setSiteManajemenData] = useState({
    alamat: "",
    color: [],
    created_at: "",
    created_by: "",
    email: "",
    footer_logo: "",
    header_logo: "",
    koordinat: [],
    link: [],
    logo_description: "",
    notelp: [],
    social_media: [],
  });

  const validationSchema = Yup.object({
    email: Yup.string()
      .required(ValidationMessage.required.replace("__field", "Email"))
      .email(ValidationMessage.invalid.replace("__field", "Email")),
    password: Yup.string()
      .min(
        8,
        ValidationMessage.minLength
          .replace("__field", "Password")
          .replace("__length", 8),
      )
      .required(ValidationMessage.required.replace("__field", "Password")),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
    getValues,
  } = useForm({
    mode: "onChange",
    resolver: yupResolver(validationSchema),
  });

  // watch email change
  // const watchEmail = watch("email");

  // useEffect(() => {
  //   // console.log(getValues("email"),errors)
  //   if (!errors.email) {
  //     checkEmailResetState(getValues("email"))
  //   }
  // }, [watchEmail])

  // const checkEmailResetState = useCallback(
  //   _.debounce(async (email) => {
  //     // nunggu BE dari pak sakho
  //     try {
  //       // console.log(email)
  //       const resp = await GeneralService.checkNeedForgetPass(email)
  //       // console.log(resp)
  //       // setNeedReset(false)
  //       if (resp.data.success) {
  //         const { result } = resp.data
  //         if (result && result.length > 0) {
  //           // console.log("masuk")
  //           if (result[0]) {
  //             // console.log("masuk")
  //             if (result[0]?.reset_password_required == 1) {
  //               setNeedReset(result[0]?.reset_password_required == 1)
  //             } else {

  //             }

  //           }
  //         } else {
  //           setNeedReset(false)
  //         }
  //       }
  //     } catch (error) {
  //       setNeedReset(false)
  //     }

  //   }, 700),
  //   [],
  // )

  const router = useRouter();

  const closeFailModal = () => {
    setShowFailModal(false);
  };

  class CaptchaError extends Error {
    constructor(message) {
      super(message); // (1)
      this.name = "CaptchaError"; // (2)
    }
  }

  const checkNeedReset = async (email) => {
    const resp = await GeneralService.checkNeedForgetPass(email);
    // console.log(resp)
    // setNeedReset(false)
    if (resp.data.success) {
      const { result } = resp.data;
      if (result && result.length > 0) {
        // console.log("masuk")
        if (result[0]) {
          // console.log("masuk")
          if (result[0]?.reset_password_required == 1) {
            return result[0]?.reset_password_required == 1;
          } else {
            return false;
          }
        }
      } else {
        return false;
      }
    }
  };

  async function onSubmit(data) {
    // if (needReset) {
    //   return
    // }
    try {
      if (userCaptchaResponse == null) {
        throw new CaptchaError("Captcha tidak boleh kosong");
      }
      setLoading(true);
      showLoadingModal(true);
      // check need reset
      if (await checkNeedReset(data.email)) {
        NotificationHelper.Failed({
          message: "Anda harus melakukan reset password terlebih dahulu",
          onClose: () => {
            router.push("/forgot-pass");
          },
        });
        return;
      }
      // return
      const capcthaValidate =
        await PortalService.captchaValidate(userCaptchaResponse);
      const remember = data.remember_user;
      delete data["remember_user"];
      data.email = data.email.toLowerCase();

      const res = await PortalService.login(data);

      if (res.status === 200 && res.data.success) {
        /** Condiftion After Login Success */
        // if (res.data.result.data.user.verifikasi_otp === null) {
        //   var targetAfterLogin = "/verify-user/verifikasi";
        // } else
        if (res.data.result.data.user.wizard == 0) {
          var targetAfterLogin = "/verify-user/complete-user-data";
        } else {
          var targetAfterLogin = localStorage.getItem("targetAfterLogin")
            ? localStorage.getItem("targetAfterLogin")
            : "/auth/user-profile";
        }
        PortalHelper.saveUserToken(res.data.result.data.token, remember == 1);
        PortalHelper.saveUserData(res.data.result.data.user, remember == 1);
        // check user
        const { wizard, lengkapi_pendaftaran, handphone_verifikasi } =
          res.data.result.data.user;

        if (wizard != null && handphone_verifikasi) {
          if (lengkapi_pendaftaran.length > 0) {
            setLoading(false);
            NotificationHelper.Success({
              title: "Perhatian",
              message: "Selanjutnya Mohon Lengkapi Form Pendaftaran.",
              onClose: () => {
                router.push(
                  `/pelatihan/${lengkapi_pendaftaran[0].pelatian_id}/daftar`,
                );
              },
              okText: "Lengkapi Pendaftaran",
            });
            return;
          }
        }
        router.push(targetAfterLogin, undefined, {});
        localStorage.removeItem("targetAfterLogin");
      } else {
        if (res.data?.result) {
          throw Error(PortalHelper.handleErrorResponse(res.data.result));
        } else {
          throw Error(res.data.message);
        }
      }
      // showLoadingModal(false);
    } catch (err) {
      setLoading(false);
      if (err.name == "CaptchaError") {
        setCaptchaErrorMessage(err.message);
      } else {
        const message = err.message;
        showLoadingModal(false);
        setErrorMessage(message);
        setShowFailModal(true);
      }
    }
  }

  // ambil data link
  useEffect(async () => {
    let settingData = await PortalService.generalDataSiteManajamenen();
    // console.log(settingData)
    if (settingData.data?.success) {
      // console.log(settingData.data.result.Data)
      const {
        alamat,
        color,
        created_at,
        created_by,
        email,
        footer_logo,
        header_logo,
        koordinat,
        link,
        logo_description,
        notelp,
        social_media,
      } = settingData.data.result[0];
      setSiteManajemenData({
        alamat: alamat,
        color: JSON.parse(color),
        created_at: created_at,
        created_by: created_by,
        email: email,
        footer_logo: footer_logo,
        header_logo: header_logo,
        koordinat: koordinat?.split(","),
        logo_description: logo_description,
        notelp: JSON.parse(notelp),
        link: JSON.parse(link),
        social_media: JSON.parse(social_media),
      });
      // console.log(siteManajemenData)
    } else {
      settingData = await PortalService.generalDataSiteManajamenen(true);
      const {
        alamat,
        color,
        created_at,
        created_by,
        email,
        footer_logo,
        header_logo,
        koordinat,
        link,
        logo_description,
        notelp,
        social_media,
      } = settingData.data.result[0];
      setSiteManajemenData({
        alamat: alamat,
        color: JSON.parse(color),
        created_at: created_at,
        created_by: created_by,
        email: email,
        footer_logo: footer_logo,
        header_logo: header_logo,
        koordinat: koordinat?.split(","),
        logo_description: logo_description,
        notelp: JSON.parse(notelp),
        link: JSON.parse(link),
        social_media: JSON.parse(social_media),
      });
    }
    setIsLoadingGetLinks(false);
  }, []);

  return (
    <>
      <div className="row vh-100">
        <div className="col-lg-5 mb-0 bg-info d-none d-lg-block">
          <div className="container py-8">
            <SSOInfo
              links={siteManajemenData.link}
              isLoading={isLoadingGetLinks}
            />
          </div>
        </div>
        <div className="col-lg-7 mb-0 position-relative">
          <div className="container py-8">
            {/* FORM LOGIN ================================================== */}
            <div>
              <div className="row mb-2 align-items-end">
                <div className="col-md p-0 mb-md-0">
                  <div className="card-body pt-0 mb-0 ps-3">
                    <div className="mb-5">
                      <a
                        href="/"
                        className="text-blue font-size-lg"
                        data-kt-menu-dismiss="true"
                      >
                        <i className="fa fa-chevron-left me-2"></i>Beranda{" "}
                      </a>
                    </div>
                    <h2 className="fw-bolder mb-1">Log In Akun</h2>
                    <p className="font-size-lg mb-0 text-capitalize">
                      Hi, Selamat Datang{" "}
                      <span className="text-blue font-size-lg font-weight-medium">
                        #JagoanDigital
                      </span>
                    </p>
                  </div>
                </div>
              </div>

              <form
                className="col-lg-8 col-sm-12 ps-0"
                onSubmit={handleSubmit(onSubmit)}
              >
                {/* Form Signin */}
                {/* Email */}
                <div className="form-group mb-5">
                  <label htmlFor="modalSigninEmail">
                    Email
                    <span
                      className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                      title="required"
                    >
                      {" "}
                      *
                    </span>
                  </label>
                  <input
                    type="text"
                    className={`form-control rounded-3 form-control-sm ${
                      errors.email ? "is-invalid" : ""
                    }`}
                    name="email"
                    {...register("email")}
                    placeholder="Email"
                  />
                  <span className="text-red">{errors.email?.message}</span>
                </div>
                {/* Password */}
                <div className="form-group mb-5 password-input">
                  <label htmlFor="modalSigninPassword">
                    Password
                    <span
                      className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                      title="required"
                    >
                      *
                    </span>
                  </label>
                  <div className="input inner-addon right-addon">
                    <span
                      className="icon"
                      onClick={() => {
                        setShowPassword(!showPassword);
                      }}
                    >
                      <FlipMove>
                        {!showPassword && <i className="fa fa-eye "></i>}
                        {showPassword && <i className="fa fa-eye-slash "></i>}
                      </FlipMove>
                    </span>
                    <input
                      type={showPassword ? "text" : "password"}
                      className={`form-control rounded-3 form-control-sm ${
                        errors.password ? "is-invalid" : ""
                      }`}
                      name="password"
                      {...register("password")}
                      placeholder="**********"
                    />
                  </div>
                  <span className="text-red">{errors.password?.message}</span>
                </div>
                <div className="d-flex align-items-center mb-5 font-size-sm">
                  <div className="form-check">
                    <input
                      className="form-check-input text-gray-800"
                      type="checkbox"
                      value={1}
                      id="autoSizingCheck"
                      {...register("remember_user")}
                    />
                    <label
                      className="form-check-label font-size-base text-gray-800"
                      htmlFor="autoSizingCheck"
                    >
                      Ingat Saya
                    </label>
                  </div>
                  <div className="ms-auto">
                    <Link href="/forgot-pass">
                      <a className="font-size-base text-blue">Lupa Password</a>
                    </Link>
                  </div>
                </div>
                <div className="d-flex align-items-center mb-5 font-size-sm">
                  <div>
                    <ReCAPTCHA
                      sitekey={process.env.CAPTCHA_SITEKEY}
                      onChange={(e) => {
                        setUserCaptchaResponse(e);
                      }}
                    ></ReCAPTCHA>
                  </div>
                  {!userCaptchaResponse && captchaErrorMessage && (
                    <span className="text-red">{captchaErrorMessage}</span>
                  )}
                </div>
                <ButtonWithLoading
                  className={`btn btn-block btn-blue rounded-pill`}
                  type="submit"
                  loading={loading}
                >
                  LOGIN
                </ButtonWithLoading>
                <p className="my-5 text-center">
                  Belum Punya Akun? &nbsp;
                  <Link href="/register">
                    <a
                      className="text-blue font-size-14 text-underline"
                      onClick={() => {
                        // showLoadingModal(true);
                      }}
                    >
                      Daftar
                    </a>
                  </Link>
                </p>
              </form>
            </div>
          </div>
        </div>
      </div>
      <Failed
        show={showFailModal}
        handleClose={closeFailModal}
        children={errorMessage}
      />
    </>
  );
}
