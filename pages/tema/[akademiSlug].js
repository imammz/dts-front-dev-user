import React, { useState, useEffect } from "react";
import Link from "next/link";
import axios from "axios";
import Moment from "react-moment";
import "moment/locale/id";
import PortalService from "../../services/PortalService";
import Select from "react-select";

import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

import FlipMove from "react-flip-move";
import { useRouter } from "next/router";
import Pagination from "../../components/Pagination";
import SideWidget from "../../components/SideWidget";
import ListTema from "../../components/ListTema";
import SideImage from "../../components/SideImage";
import PortalHelper from "../../src/helper/PortalHelper";

function TemaAkademi() {
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [perPage, setPerPage] = useState(20);
  const [dataTema, setDataTema] = useState([]);
  const [akademi, setAkademi] = useState(null);
  const [skeletons, setSkeletons] = useState([...Array(10).keys()]);
  const [metaData, setMetaData] = useState(null);
  const [sortBy, setSortBy] = useState("Newest");
  const page = router.query.page ? router.query.page : 1;

  useEffect(async () => {
    try {
      if (router.isReady) {
        setLoading(true);
        const akademi = PortalService.akademiDetail(router.query.akademiSlug);
        const tema = PortalService.temaList(
          router.query.akademiSlug,
          sortBy,
          page,
          perPage,
        );
        const resps = await axios.all([akademi, tema]);
        if (resps[0].data.success) {
          setAkademi(resps[0].data.result.data);
        } else {
          throw Error(resps[0].data.message);
        }
        if (resps[1].data.success) {
          setDataTema(resps[1].data.result.data);
          setMetaData(resps[1].data.result.meta);
        } else {
          throw Error(resps[1].data.message);
        }
        setLoading(false);
      }
    } catch (err) {
      console.error(err);
    }
  }, [sortBy, page, perPage, router]);

  return (
    <>
      <header
        className="py-6 bg-blue mb-8 z-index-0"
        style={{ backgroundImage: "none" }}
      >
        <div className="container">
          <div className="row align-items-end" data-aos="fade-up">
            {/*<nav aria-label="breadcrumb">
                        <ol className="breadcrumb breadcrumb-scroll">
                            <li className="breadcrumb-item">
                                <Link  href="/">
                                    <a className="text-gray-700">
                                        Home
                                    </a>
                                </Link>
                            </li>
                            <li className="breadcrumb-item">
                                <a className="text-gray-700">
                                    Tentang Kami
                                </a>
                            </li>
                            <li className="breadcrumb-item text-blue active" aria-current="page">
                                Tema
                            </li>
                        </ol>
                    </nav>*/}
            <div className="col-md mb-md-0">
              <div className="row align-items-center mb-0 py-3 ps-4">
                <div className="col-auto px-0">
                  <div className="icon-h-p secondary">
                    <img
                      className="h-100p"
                      src={PortalHelper.urlAkademiIcon(akademi?.logo)}
                      alt={akademi?.nama}
                    />
                  </div>
                </div>

                <div className="col px-4">
                  <div className="card-body mb-0 p-0">
                    <p className="mb-0 text-white line-clamp-1">
                      Tema Pelatihan
                    </p>
                    <h3 className="fw-bolder text-white mb-0">
                      {akademi?.sub_nama || "Belum ada data"}
                    </h3>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-auto col-sm-12">
              <div className="d-lg-flex rounded mb-3">
                <div className="d-lg-flex flex-wrap">
                  <div>
                    <div className="d-flex align-items-center h-50p">
                      <div className="form-floating col-12">
                        <select
                          value={sortBy}
                          onChange={(e) => {
                            setSortBy(e.target.value);
                          }}
                          className="form-select form-select-sm form-control-sm ps-5 shadow-none text-primary fw-medium rounded-xl"
                        >
                          <option value="Alphabet">Alphabet</option>
                          <option value="Pelatihan">Jml. Pelatihan</option>
                          <option value="Pendaftar">Jml. Pendaftar</option>
                        </select>
                        <label className="ps-5">Urutkan</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>

      <div className="container z-index-0">
        <div className="row mb-9">
          <div className="col-lg-9">
            <ListTema
              loading={loading}
              data={dataTema}
              namaAkademi={akademi?.nama}
            ></ListTema>
            <div className="d-flex justify-content-center align-items-center my-9">
              <Pagination
                pageSize={metaData?.last_page}
                currentPage={page}
              ></Pagination>
            </div>
          </div>
          <div className="col-lg-3">
            <SideWidget loading={loading} />
            <SideImage loading={loading} />
          </div>
        </div>
      </div>
    </>
  );
}
export default TemaAkademi;
