import React, { useState, useEffect } from "react";
import Link from "next/link";
import axios from "axios";
import Moment from "react-moment";
import "moment/locale/id";
import PortalService from "./../../services/PortalService";
import Select from "react-select";

import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

import FlipMove from "react-flip-move";
import { useRouter } from "next/router";
import Pagination from "../../components/Pagination";
import SideWidget from "../../components/SideWidget";
import SideImageWidget from "../../components/SideImageWidget";
import ListTema from "../../components/ListTema";
const BelumAdaData = ({ message = "Belum Ada Data" }) => {
  return (
    <>
      <div className="col-12 mx-auto text-center">
        <h2 className="fw-bolder mb-0">{message}</h2>
        {/* <p className="font-size-lg mb-6">Pelatihan pada akademi <span className="text-blue font-weight-semi-bold">{AkademiDetail.nama}</span> belum tersedia</p> */}
        <img
          src="/assets/img/empty-state/no-data.png"
          alt="..."
          className="img-fluid align-self-center"
          style={{ width: "500px" }}
        />
      </div>
    </>
  );
};
function TemaPopuler() {
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [perPage, setPerPage] = useState(20);
  const [dataTema, setDataTema] = useState([]);
  const [skeletons, setSkeletons] = useState([...Array(10).keys()]);
  const [metaData, setMetaData] = useState(null);
  const [sortBy, setSortBy] = useState("Pendaftar");
  const page = router.query.page ? router.query.page : 1;

  useEffect(async () => {
    try {
      setLoading(true);
      const resp = await PortalService.temaList("", sortBy, page, perPage);
      if (resp.data.success) {
        setDataTema(resp.data.result.data);
        setMetaData(resp.data.result.meta ?? {});
      } else {
        throw Error(resp.data.message);
      }
      setLoading(false);
    } catch (err) {
      setLoading(false);
      setDataTema([]);
      console.error(err);
    }
  }, [sortBy, page, perPage]);

  return (
    <>
      <header
        className="py-6 bg-blue mb-8 z-index-0"
        style={{ backgroundImage: "none" }}
      >
        <div className="container">
          <div className="row align-items-end" data-aos="fade-up">
            <div className="col-md mb-md-0">
              <div className="row align-items-center mb-0 py-3 ps-4">
                <div className="col-auto px-0">
                  <div className="icon-h-p secondary">
                    <img
                      className="img-fluid h-60p shadow-light-lg"
                      src="/assets/img/logo-dts.svg"
                      alt="DTS"
                    />
                  </div>
                </div>

                <div className="col px-4">
                  <div className="card-body mb-0 p-0">
                    <p className="mb-0 text-white line-clamp-1">
                      Tema Pelatihan
                    </p>
                    {/* request nambah be ke buat judul ke admin */}
                    <h3 className="fw-bolder text-white mb-0">
                      Digital Talent Scholarship
                    </h3>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-auto col-sm-12">
              <div className="d-lg-flex rounded mb-3">
                <div className="d-lg-flex flex-wrap">
                  <div>
                    <div className="d-flex align-items-center h-50p">
                      <div className="form-floating col-12">
                        <select
                          value={sortBy}
                          onChange={(e) => {
                            setSortBy(e.target.value);
                          }}
                          className="form-select form-select-sm form-control-sm ps-5 shadow-none text-primary fw-medium rounded-xl"
                        >
                          <option value="Alphabet">Alphabet</option>
                          <option value="Pelatihan">Jml. Pelatihan</option>
                          <option value="Pendaftar">Jml. Pendaftar</option>
                        </select>
                        <label className="ps-5">Urutkan</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>

      <div className="container z-index-0">
        <div className="row mb-9">
          <div className="col-xl-9">
            {/* {dataTema.length != 0 ? <> */}
            <ListTema loading={loading} data={dataTema}></ListTema>

            <div className="d-flex justify-content-center align-items-center my-9">
              <Pagination
                pageSize={metaData?.last_page}
                currentPage={page}
              ></Pagination>
            </div>
            {/* </> : <>
              <BelumAdaData></BelumAdaData>
            </>} */}
          </div>
          <div className="col-lg-3">
            <SideWidget loading={loading} />
            <SideImageWidget loading={loading} />
          </div>
        </div>
      </div>
    </>
  );
}
export default TemaPopuler;
