import react, { useState, useEffect, useRef } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import Link from "next/link";
import PortalHelper from "../../src/helper/PortalHelper";
import PortalService from "../../services/PortalService";
import GeneralService from "../../services/GeneralService";
import Configuration from "/config";

/**
 * Components
 */
import Pagination from "../../components/Pagination";
import CardPelatihan from "../../components/Card/Pelatihan";
import SideWidget from "../../components/SideWidget";
import SideImageWidget from "../../components/SideImageWidget";

/** Libraries */
import Moment from "react-moment";
import "moment/locale/id";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

export default function akademiDetailPage() {
  const router = useRouter();
  const [AkademiDetail, setAkademiDetail] = useState([]);
  const [ListPelatihan, setListPelatihan] = useState([]);
  const toggleCloseFilterModal = useRef(null);

  /** Filter */
  const [FListAkademi, setFilterListAkademi] = useState([]);
  const [FListTema, setFilterListTema] = useState([]);
  const [FListMitra, setFilterListMitra] = useState([]);
  const [FListPenyelenggara, setFilterListPenyelenggara] = useState([]);
  const [FListLokasi, setFilterListLokasi] = useState([]);
  const [timeDiff, setTimeDiff] = useState(0);
  const [FListMetode, setFilterListMetode] = useState([
    "Online",
    "Offline",
    "Online & Offline",
  ]);

  // Create FListStatusPelatihan with key 1 => Buka, 2 => tutup
  const [FListStatusPelatihan, setFilterStatusPelatihan] = useState([
    { key: 1, value: "Buka" },
    { key: 2, value: "Tutup" },
  ]);

  const [loading, setLoading] = useState(false);
  const [TemaID, setTemaID] = useState("");
  const [MitraID, setMitraID] = useState("");
  const [PenyelenggaraID, setPenyelenggaraID] = useState("");
  const [LokasiID, setLokasiID] = useState("");
  const [Metode, setMetode] = useState("");
  const [FAkademiID, setFAkademiID] = useState("");
  const [FStatusPendaftaran, setFStatusPendaftaran] = useState("");
  const akademiID = router.query.akademiID;
  const page = router.query.page ? router.query.page : 1;

  const onChangeAkademi = async (e) => {
    setFAkademiID(e.target.value);
    setTemaID(null);
    setMitraID(null);
    const resFilterTema = await GeneralService.tema(e.target.value);
    const resFilterMitra = await GeneralService.filterMitra(
      e.target.value,
      null,
    );
    setFilterListTema(resFilterTema.data.result);
    setFilterListMitra(resFilterMitra.data.result);
  };

  const getServerTime = async () => {
    const resp = await GeneralService.getServerTime();
    setTimeDiff(resp);
  };

  const onChangeTema = async (e) => {
    setTemaID(e.target.value);
    const resFilterMitra = await GeneralService.filterMitra(
      FAkademiID,
      e.target.value,
    );
    setFilterListMitra(resFilterMitra.data.result);
  };

  const onChangeMitra = async (e) => {
    setMitraID(e.target.value);
  };

  const onChangePenyelenggara = async (e) => {
    setPenyelenggaraID(e.target.value);
  };

  const onChangeLokasi = async (e) => {
    setLokasiID(e.target.value);
  };

  const onChangeMetode = async (e) => {
    setMetode(e.target.value);
  };

  const onChangeStatusPendaftaran = async (e) => {
    setFStatusPendaftaran(e.target.value);
  };

  const onSubmitFilter = async (e) => {
    e.preventDefault();
    toggleCloseFilterModal.current.click();
    router.push({
      pathname: "",
      query: {
        akademiID: FAkademiID ? FAkademiID : akademiID,
        tema: TemaID,
        mitra: MitraID,
        penyelenggara: PenyelenggaraID,
        lokasi: LokasiID,
        metode: Metode,
        status_pendaftaran: FStatusPendaftaran,
      },
    });
  };

  useEffect(async () => {
    try {
      setLoading(true);
      if (router.isReady) {
        const resAkademiDetail = await PortalService.akademiDetail(akademiID);
        const resPelatihanList = await PortalService.pelatihanList(
          akademiID,
          page,
          undefined,
          undefined,
          router.query.tema,
          router.query.mitra,
          router.query.penyelenggara,
          router.query.lokasi,
          router.query.metode,
          router.query.status_pendaftaran,
        );

        /** Filter */
        const resFilterAkademi = await GeneralService.akademi();
        const resFilterTema = await GeneralService.tema(akademiID);
        const resFilterMitra = await GeneralService.filterMitra(
          akademiID,
          TemaID,
        );
        const resFilterPenyelenggara = await GeneralService.penyelenggara();
        const resFilterLokasi = await GeneralService.provinsi();

        // console.log(resAkademiDetail.data.result);

        setAkademiDetail(resAkademiDetail.data.result.data);
        setListPelatihan(resPelatihanList.data.result);

        /** Filter */
        setFilterListAkademi(resFilterAkademi.data.result);
        setFilterListTema(resFilterTema.data.result);
        setFilterListMitra(resFilterMitra.data.result);
        setFilterListPenyelenggara(resFilterPenyelenggara.data.result);
        setFilterListLokasi(resFilterLokasi.data.result);

        setFAkademiID(router.query.akademiID);
        setTemaID(router.query.tema);
        setMitraID(router.query.mitra);
        setPenyelenggaraID(router.query.penyelenggara);
        setLokasiID(router.query.lokasi);
        setMetode(router.query.metode);
        setFStatusPendaftaran(router.query.status_pendaftaran);
        getServerTime();
        setLoading(false);
      }
    } catch (error) {
      setLoading(false);
      console.error(error);
    }
  }, [router, akademiID, page]);

  return (
    <div>
      {loading && (
        <div>
          <header
            className="py-6 bg-blue mb-6 z-index-0"
            style={{ backgroundImage: "none" }}
          >
            <div className="container">
              <div className="row align-items-center" data-aos="fade-up">
                {/* <nav aria-label="breadcrumb">
                  <ol className="breadcrumb breadcrumb-scroll">
                    <li className="breadcrumb-item">
                      <Skeleton />
                    </li>
                    <li className="breadcrumb-item">
                      <Skeleton />
                    </li>
                    <li
                      className="breadcrumb-item text-blue active"
                      aria-current="page"
                    >
                      <Skeleton />
                    </li>
                  </ol>
                </nav>*/}

                <div className="col-md mb-md-0">
                  <div className="row align-items-center mb-0 py-2 ps-4">
                    <div className="px-0" style={{ width: "50px" }}>
                      <Skeleton className="h-50p"></Skeleton>
                    </div>

                    <div className="col-3 px-4">
                      <div className="card-body mb-0 p-0">
                        <Skeleton></Skeleton>
                        <Skeleton></Skeleton>
                      </div>
                    </div>
                    <div className="col-auto d-block d-md-none d-lg-none d-xl-none">
                      <Skeleton className="h-50p"></Skeleton>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </header>

          <div className="container z-index-0">
            <div className="d-lg-flex flex-wrap">
              <div className="mb-5">
                <p className="mb-lg-0">
                  <Skeleton />
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-xl-9 mb-9">
                <div className="card border shadow p-3 rounded-5 mb-5 lift">
                  <div className="row gx-0">
                    <div style={{ maxWidth: "80px" }}>
                      <Skeleton inline={true} height={80} />
                    </div>
                    <div className="col-12 colmd-9 col-lg-9 col-xl-9 d-md-block">
                      <div className="card-body p-2">
                        <Skeleton className="mb-0" width={200} />

                        <Skeleton className="mb-4" width={250} />

                        <ul className="nav mx-n3 d-block d-md-flex">
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                              {/* <div className="font-size-sm">{item.speaker}</div> */}
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                              {/* <div className="font-size-sm">{item.speaker}</div> */}
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="col-auto d-none pe-xl-2 d-lg-flex place-center">
                      <Skeleton />
                    </div>
                  </div>
                </div>
                <div className="card border shadow p-3 rounded-5 mb-5 lift">
                  <div className="row gx-0">
                    <div style={{ maxWidth: "80px" }}>
                      <Skeleton inline={true} height={80} />
                    </div>
                    <div className="col-12 colmd-9 col-lg-9 col-xl-9 d-md-block">
                      <div className="card-body p-2">
                        <Skeleton className="mb-0" width={200} />

                        <Skeleton className="mb-4" width={250} />

                        <ul className="nav mx-n3 d-block d-md-flex">
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                              {/* <div className="font-size-sm">{item.speaker}</div> */}
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                              {/* <div className="font-size-sm">{item.speaker}</div> */}
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="col-auto d-none pe-xl-2 d-lg-flex place-center">
                      <Skeleton />
                    </div>
                  </div>
                </div>
                <div className="card border shadow p-3 rounded-5 mb-5 lift">
                  <div className="row gx-0">
                    <div style={{ maxWidth: "80px" }}>
                      <Skeleton inline={true} height={80} />
                    </div>
                    <div className="col-12 colmd-9 col-lg-9 col-xl-9 d-md-block">
                      <div className="card-body p-2">
                        <Skeleton className="mb-0" width={200} />

                        <Skeleton className="mb-4" width={250} />

                        <ul className="nav mx-n3 d-block d-md-flex">
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                              {/* <div className="font-size-sm">{item.speaker}</div> */}
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                              {/* <div className="font-size-sm">{item.speaker}</div> */}
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="col-auto d-none pe-xl-2 d-lg-flex place-center">
                      <Skeleton />
                    </div>
                  </div>
                </div>
                <div className="card border shadow p-3 rounded-5 mb-5 lift">
                  <div className="row gx-0">
                    <div style={{ maxWidth: "80px" }}>
                      <Skeleton inline={true} height={80} />
                    </div>
                    <div className="col-12 colmd-9 col-lg-9 col-xl-9 d-md-block">
                      <div className="card-body p-2">
                        <Skeleton className="mb-0" width={200} />

                        <Skeleton className="mb-4" width={250} />

                        <ul className="nav mx-n3 d-block d-md-flex">
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                              {/* <div className="font-size-sm">{item.speaker}</div> */}
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                              {/* <div className="font-size-sm">{item.speaker}</div> */}
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="col-auto d-none pe-xl-2 d-lg-flex place-center">
                      <Skeleton />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xl-3 px-4 mb-5 mb-xl-0">
                <Skeleton className="h-300p mb-5" count={2}></Skeleton>
              </div>
            </div>
          </div>
        </div>
      )}

      {!loading && (
        <div>
          <form onSubmit={onSubmitFilter}>
            <div
              className="modal modal-sidebar col-5 left fade-left fade"
              id="filterModal"
            >
              <div className="modal-dialog modal-dialog-scrollable">
                <div className="modal-content pb-4">
                  {/* <form onSubmit={onSubmitFilter}> */}
                  <div className="modal-header mb-2">
                    <div className="row align-items-end">
                      <h4 className="fw-bolder">Filter</h4>
                    </div>
                    <button
                      type="button"
                      className="close text-primary mb-2"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                      ref={toggleCloseFilterModal}
                    >
                      {" "}
                      <svg
                        width="16"
                        height="17"
                        viewBox="0 0 16 17"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M0.142135 2.00015L1.55635 0.585938L15.6985 14.7281L14.2843 16.1423L0.142135 2.00015Z"
                          fill="currentColor"
                        ></path>
                        <path
                          d="M14.1421 1.0001L15.5563 2.41431L1.41421 16.5564L0 15.1422L14.1421 1.0001Z"
                          fill="currentColor"
                        ></path>
                      </svg>
                    </button>
                  </div>
                  <div className="modal-body border-top ">
                    <div className="row my-5">
                      <div className="form-group col-xl-12">
                        <div className="form-floating col-12 my-3">
                          <select
                            class="form-select form-select-sm form-control-sm ps-5 shadow-none text-primary fw-medium rounded-xl"
                            value={FAkademiID ? FAkademiID : akademiID}
                            onChange={(e) => {
                              onChangeAkademi(e);
                            }}
                          >
                            {FListAkademi.map((Akademi) => (
                              <option key={Akademi.slug} value={Akademi.slug}>
                                {Akademi.nama}
                              </option>
                            ))}
                          </select>
                          <label className="ps-5">Akademi</label>
                        </div>
                      </div>
                      <div className="form-group col-xl-12">
                        <div className="form-floating col-12 my-3">
                          <select
                            class="form-select form-select-sm form-control-sm ps-5 shadow-none text-primary fw-medium rounded-xl"
                            value={TemaID}
                            onChange={onChangeTema}
                          >
                            <option value="">Semua Tema</option>
                            {FListTema.map((Tema) => (
                              <option key={Tema.id} value={Tema.id}>
                                {Tema.nama_tema}
                              </option>
                            ))}
                          </select>
                          <label className="ps-5">Tema Pelatihan</label>
                        </div>
                      </div>
                      <div className="form-group col-xl-12">
                        <div className="form-floating col-12 my-3">
                          <select
                            class="form-select form-select-sm form-control-sm ps-5 shadow-none text-primary fw-medium rounded-xl"
                            value={MitraID}
                            onChange={onChangeMitra}
                          >
                            <option value="">Semua Mitra</option>
                            {FListMitra.map((Mitra) => (
                              <option key={Mitra.id} value={Mitra.id}>
                                {Mitra.nama_mitra}
                              </option>
                            ))}
                          </select>
                          <label className="ps-5">Mitra Pelatihan</label>
                        </div>
                      </div>
                      <div className="form-group col-xl-12">
                        <div className="form-floating col-12 my-3">
                          <select
                            class="form-select form-select-sm form-control-sm ps-5 shadow-none text-primary fw-medium rounded-xl"
                            value={PenyelenggaraID}
                            onChange={onChangePenyelenggara}
                          >
                            <option value="">Semua Penyelenggara</option>
                            {FListPenyelenggara.map((Penyelenggara) => (
                              <option
                                key={Penyelenggara.id}
                                value={Penyelenggara.id}
                              >
                                {Penyelenggara.nama_penyelenggara}
                              </option>
                            ))}
                          </select>
                          <label className="ps-5">Penyelenggara</label>
                        </div>
                      </div>
                      <div className="form-group col-xl-12">
                        <div className="form-floating col-12 my-3">
                          <select
                            class="form-select form-select-sm form-control-sm ps-5 shadow-none text-primary fw-medium rounded-xl"
                            value={LokasiID}
                            onChange={onChangeLokasi}
                          >
                            <option value="">Semua Lokasi</option>
                            {FListLokasi.map((Lokasi) => (
                              <option key={Lokasi.id} value={Lokasi.id}>
                                {Lokasi.name}
                              </option>
                            ))}
                          </select>
                          <label className="ps-5">Lokasi Pelatihan</label>
                        </div>
                      </div>
                      <div className="form-group col-xl-12">
                        <div className="form-floating col-12 my-3">
                          <select
                            class="form-select form-select-sm form-control-sm ps-5 shadow-none text-primary fw-medium rounded-xl"
                            value={Metode}
                            onChange={onChangeMetode}
                          >
                            <option value="">Semua Metode Pelatihan</option>
                            {FListMetode.map((value) => (
                              <option key={value} value={value}>
                                {value}
                              </option>
                            ))}
                          </select>
                          <label className="ps-5">Metode Pelatihan</label>
                        </div>
                      </div>
                      <div className="form-group col-xl-12">
                        <div className="form-floating col-12 my-3">
                          <select
                            className="form-select form-select-sm form-control-sm ps-5 shadow-none text-primary fw-medium rounded-xl"
                            value={FStatusPendaftaran}
                            onChange={onChangeStatusPendaftaran}
                          >
                            <option value="">Semua Status Pendaftaran</option>
                            {FListStatusPelatihan.map((value) => (
                              <option key={value.key} value={value.key}>
                                {value.value}
                              </option>
                            ))}
                          </select>
                          <label className="ps-5">Status Pendaftaran</label>
                        </div>
                      </div>
                      {/* <div className="col-12 my-5">
                          <button
                            type="submit"
                            className="col-12 btn btn-sm btn-blue btn-block"
                          >
                            TERAPKAN FILTER
                          </button>
                        </div> */}
                    </div>
                  </div>
                  <div className="modal-footer pb-0 px-0">
                    <button
                      type="submit"
                      className="col-12 btn btn-sm btn-blue btn-block"
                    >
                      TERAPKAN FILTER
                    </button>
                  </div>
                  {/* </form> */}
                </div>
              </div>
            </div>
          </form>
          <header className="py-6 bg-blue mb-6 z-index-0">
            <div className="container">
              <div className="row align-items-end" data-aos="fade-up">
                {/*
              <nav aria-label="breadcrumb">
                <ol className="breadcrumb breadcrumb-scroll">
                  <li className="breadcrumb-item">
                    <a className="text-gray-700" href="#">
                      Home
                    </a>
                  </li>
                  <li className="breadcrumb-item">
                    <a className="text-gray-700" href="#">
                      Pelatihan
                    </a>
                  </li>
                  <li
                    className="breadcrumb-item text-blue active"
                    aria-current="page"
                  >
                    {AkademiDetail.nama}
                  </li>
                </ol>
              </nav>*/}
                <div className="col-md mb-md-0">
                  <div className="col-md mb-md-0">
                    <div className="row align-items-center mb-0 py-2 ps-4">
                      <div className="col-auto px-0">
                        <div className="icon-h-p secondary">
                          <img
                            src={PortalHelper.urlAkademiIcon(
                              AkademiDetail.logo,
                            )}
                            className="h-100p"
                          />
                        </div>
                      </div>

                      <div className="col px-4">
                        <div className="card-body mb-0 p-0">
                          <h3 className="fw-bolder text-white mb-0">
                            {AkademiDetail.sub_nama}
                          </h3>
                          <p className="mb-0 text-white">
                            {AkademiDetail.nama}
                          </p>
                        </div>
                      </div>
                      <div className="col-auto d-block d-md-none d-lg-none d-xl-none">
                        <Link
                          href={{
                            pathname: `/program`,
                            query: {
                              akademi: AkademiDetail.slug,
                            },
                          }}
                        >
                          <i className="bi bi-info-circle text-white font-size-18"></i>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-auto">
                  <div className="d-flex rounded mb-sm-0 mb-lg-5">
                    <div className="d-flex flex-wrap d-none d-md-block d-lg-block d-xl-block">
                      <Link
                        href={{
                          pathname: `/program`,
                          query: {
                            akademi: AkademiDetail.slug,
                          },
                        }}
                      >
                        <a className="btn btn-outline-white-ice btn-sm btn-pill mb-1">
                          Tentang Akademi
                        </a>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </header>
          <div className="container z-index-0">
            <div className="row mb-9">
              <div className="col-xl-9 pt-lg-6">
                <div className="row mb-3">
                  <div className="col-md-6 col-sm-12 float-start">
                    <p className="float-start">
                      Ditemukan{" "}
                      <span className="font-size-14 text-blue">
                        {ListPelatihan.meta?.total} Pelatihan
                      </span>
                    </p>
                  </div>
                  <div className="col-md-6 col-sm-12">
                    <a
                      href="#"
                      className="badge badge-lg rounded-pill border border-gray-800 text-gray-800 float-start float-md-end float-lg-end me-2"
                      data-bs-toggle="modal"
                      data-bs-target="#filterModal"
                    >
                      <i className="bi bi-sliders me-1"></i>Filter
                    </a>
                    <Link
                      href={{
                        pathname: `/tema/${AkademiDetail.slug}`,
                      }}
                    >
                      <a className="badge badge-lg rounded-pill border border-gray-800 text-gray-800 float-start float-md-end float-lg-end me-2">
                        <i className="bi bi-pass me-1"></i>Pilihan Tema
                      </a>
                    </Link>
                  </div>
                </div>
                {ListPelatihan.data?.length > 0 && (
                  <div>
                    {ListPelatihan.data?.map((Pelatihan, i) => {
                      Pelatihan = {
                        ...Pelatihan,
                        current_server_time: timeDiff.serverTime,
                        diff: timeDiff.diff,
                      };
                      return (
                        <div className="col-lg mb-5" key={i}>
                          <CardPelatihan
                            cardClassName="rounded-5 border p-3 lift"
                            {...Pelatihan}
                          ></CardPelatihan>
                        </div>
                      );
                    })}
                    <div className="d-flex justify-content-center align-items-center my-9">
                      <Pagination
                        pageSize={ListPelatihan.meta?.last_page}
                        currentPage={page}
                      ></Pagination>
                    </div>
                  </div>
                )}
                {ListPelatihan.data?.length == 0 && (
                  <div>
                    <div className="row">
                      {/* 
                      <div className="row my-3">
                        <div className="col-6 float-start">
                          <p className="float-start">
                            Ditemukan{" "}
                            <span className="text-dark">
                              {ListPelatihan.meta?.total} Pelatihan
                            </span>
                          </p>
                        </div>
                        <div className="col-6 float-end">
                          <a
                            href="#"
                            className="badge badge-lg border border-gray-800 text-gray-800 float-end"
                            data-bs-toggle="modal"
                            data-bs-target="#filterModal"
                          >
                            <svg
                              width="16"
                              height="16"
                              viewBox="0 0 16 16"
                              xmlns="http://www.w3.org/2000/svg"
                              class="mr-10"
                            >
                              <path
                                d="M15.4286 7.42841H6.20457C5.89469 6.21086 4.65646 5.47506 3.43888 5.78494C2.63143 5.99045 2.00093 6.62095 1.79541 7.42841H0.571439C0.255837 7.42841 0 7.68424 0 7.99985C0 8.31545 0.255837 8.57125 0.571439 8.57125H1.79545C2.10532 9.7888 3.34356 10.5246 4.56114 10.2147C5.36859 10.0092 5.99909 9.37871 6.20461 8.57125H15.4286C15.7442 8.57125 16 8.31542 16 7.99981C16 7.68421 15.7442 7.42841 15.4286 7.42841ZM4.00001 9.14269C3.36884 9.14269 2.85716 8.63102 2.85716 7.99985C2.85716 7.36868 3.36884 6.857 4.00001 6.857C4.63118 6.857 5.14285 7.36868 5.14285 7.99985C5.14285 8.63102 4.63118 9.14269 4.00001 9.14269Z"
                                fill="currentColor"
                              ></path>
                              <path
                                d="M15.4286 1.71405H13.6331C13.3233 0.496508 12.085 -0.239295 10.8675 0.0705817C10.06 0.276095 9.4295 0.906597 9.22399 1.71405H0.571439C0.255837 1.71405 0 1.96989 0 2.28549C0 2.60109 0.255837 2.85693 0.571439 2.85693H9.22399C9.53387 4.07447 10.7721 4.81028 11.9897 4.5004C12.7971 4.29489 13.4276 3.66438 13.6331 2.85693H15.4286C15.7442 2.85693 16 2.60109 16 2.28549C16 1.96989 15.7442 1.71405 15.4286 1.71405ZM11.4286 3.42834C10.7974 3.42834 10.2857 2.91666 10.2857 2.28549C10.2857 1.65432 10.7974 1.14265 11.4286 1.14265C12.0598 1.14265 12.5714 1.65432 12.5714 2.28549C12.5714 2.91666 12.0598 3.42834 11.4286 3.42834Z"
                                fill="currentColor"
                              ></path>
                              <path
                                d="M15.4286 13.1428H12.4903C12.1804 11.9252 10.9422 11.1894 9.72458 11.4993C8.91713 11.7048 8.28662 12.3353 8.08111 13.1428H0.571439C0.255837 13.1428 0 13.3986 0 13.7142C0 14.0297 0.255837 14.2856 0.571439 14.2856H8.08114C8.39102 15.5032 9.62926 16.239 10.8468 15.9291C11.6543 15.7236 12.2848 15.0931 12.4903 14.2856H15.4286C15.7442 14.2856 16 14.0298 16 13.7142C16 13.3986 15.7442 13.1428 15.4286 13.1428ZM10.2857 14.8571C9.65454 14.8571 9.14286 14.3454 9.14286 13.7142C9.14286 13.083 9.65454 12.5714 10.2857 12.5714C10.9169 12.5714 11.4286 13.083 11.4286 13.7142C11.4286 14.3454 10.9169 14.8571 10.2857 14.8571Z"
                                fill="currentColor"
                              ></path>
                            </svg>{" "}
                            Filter
                          </a>
                        </div> 
                      </div>*/}
                      <div className="col-12 mx-auto text-center pt-8 pb-80">
                        <h2 className="fw-bolder mb-0">Belum Ada Pelatihan</h2>
                        <p className="font-size-14 mb-6">
                          Pelatihan pada akademi{" "}
                          <span className="text-blue font-size-14 fw-bolder">
                            {AkademiDetail.nama}
                          </span>{" "}
                          belum tersedia
                        </p>
                        <img
                          src="/assets/img/empty-state/no-data.png"
                          alt="..."
                          className="img-fluid align-self-center"
                          style={{ width: "500px" }}
                        />
                      </div>
                    </div>
                  </div>
                )}
              </div>
              <div className="col-xl-3 pt-lg-6 px-4">
                <div className="card rounded-5 border px-5 pt-6 pb-6 mb-5">
                  <div className="d-inline-block rounded-circle mb-4">
                    <div
                      className="icon-circle"
                      style={{ backgroundColor: "#c2dffb", color: "#196ECD" }}
                    >
                      <i className="fa fa-book fa-2x"></i>
                    </div>
                  </div>
                  <h5 className="fw-semi-bold mb-0">{AkademiDetail.nama}</h5>
                  <p className="font-size-12 mb-5">
                    Cari tahu mengenai program dan benefit mengikuti pelatihan
                    disini{" "}
                  </p>
                  <Link
                    href={{
                      pathname: `/program`,
                      query: {
                        akademi: AkademiDetail.slug,
                      },
                    }}
                  >
                    <a className="btn btn-outline-primary btn-xs btn-pill mb-1">
                      Pelajari
                    </a>
                  </Link>
                </div>

                <SideWidget loading={loading} />
                <SideImageWidget loading={loading} />
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
