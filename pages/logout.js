import react, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import Cookies from "js-cookie";
import CacheHelper from "../src/helper/CacheHelper";
import * as Sentry from "@sentry/browser";
import eventBus from "../components/EventBus";

function Logout() {
  const router = useRouter();

  useEffect(() => {
    Cookies.remove("user");
    CacheHelper.ClearAllCache();
    Sentry.setUser(null);
    eventBus.dispatch("updateMenuLink");
    router.push("/login");
  }, []);

  return <div></div>;
}

export default Logout;
