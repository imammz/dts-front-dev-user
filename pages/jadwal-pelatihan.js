import react, { useState, useEffect, useRef } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import PortalHelper from "../src/helper/PortalHelper";
import PortalService from "../services/PortalService";

/**
 * Components
 */
import Pagination from "../components/Pagination";
import CardPelatihan from "../components/Card/Pelatihan";
import SideWidget from "../components/SideWidget";
import SideImageWidget from "../components/SideImageWidget";

/** Libraries */
import moment from "moment";
import Moment from "react-moment";
import "moment/locale/id";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import Flickity from "react-flickity-component";
import PelatihanService from "../services/PelatihanService";
import GeneralService from "../services/GeneralService";

export default function jadwalPelatihan() {
  const router = useRouter();
  const [Loading, setLoading] = useState(false);
  const [LoadingPelatihan, setLoadingPelatihan] = useState(false);
  const [timeDiff, setTimeDiff] = useState(0);

  // ref
  const FlickityRef = useRef();

  const [wasMounted, setWasMounted] = useState(false);

  /** Filter */
  // const [Page, setPage] = useState("1");
  const Page = router.query.page ? router.query.page : 1;

  const [Month, setMonth] = useState();
  const [Date, setDate] = useState();

  /** Filter Display */
  const [ListMonths, setListMonths] = useState([]);
  const [ListHeaderPelatihan, setListHeaderPelatihan] = useState([]);
  const [ListPelatihan, setListPelatihan] = useState([]);

  const flickityOptions = {
    pageDots: true,
    prevNextButtons: false,
    cellAlign: "left",
    groupCells: true,
  };

  const { month, date } = router.query;

  const filterMonth = async (e) => {
    let date = moment(e.target.value + "-" + "01").format("YYYY-MM-DD");
    if (e.target.value == moment().format("YYYY-MM")) {
      date = moment().format("YYYY-MM-DD");
    }

    router.push({
      pathname: "",
      query: {
        month: e.target.value,
        date: date,
      },
    });
  };

  const filterDate = async (e) => {
    setDate(e.target.value);
    router.push(
      {
        pathname: "",
        query: {
          month: Month,
          date: e.target.value,
        },
      },
      undefined,
      { shallow: true },
    );
  };

  const executeLoading = (isLoading = false) => {
    if (
      router.query.hasOwnProperty("month") &&
      !router.query.hasOwnProperty("date")
    ) {
      setLoading(isLoading);
    } else if (router.query.hasOwnProperty("date")) {
      setLoadingPelatihan(isLoading);
    } else {
      setLoading(isLoading);
    }
  };

  const refreshData = async () => {
    try {
      executeLoading(true);
      /** Filter Display */
      setListMonths(PortalHelper.makeMonthFromYear(moment().year()));
      // console.log(Month)
      const resListHeaderPelatihan = await PelatihanService.jadwalHeader(
        Month,
        2,
      );
      const resPelatihanList = await PortalService.pelatihanListJadwal(
        "",
        Page || 1,
        "",
        Date,
      );

      setListHeaderPelatihan(resListHeaderPelatihan.data.result);
      setListPelatihan(resPelatihanList.data.result);

      executeLoading(false);

      const lenPerSlide = FlickityRef.current.slides[0].cells.length;
      const pos = parseInt(moment(Date).format("D"));
      const slidePos = Math.floor((pos - 1) / lenPerSlide);

      FlickityRef.current.on("ready", function () {
        if (FlickityRef.current.selectedIndex != slidePos) {
          do {
            if (FlickityRef.current.selectedIndex > slidePos) {
              FlickityRef.current.previous();
            } else {
              FlickityRef.current.next();
            }
          } while (FlickityRef.current.selectedIndex != slidePos);
        }
      });
      if (wasMounted) {
        if (FlickityRef.current.selectedIndex != slidePos) {
          do {
            if (FlickityRef.current.selectedIndex > slidePos) {
              FlickityRef.current.previous();
            } else {
              FlickityRef.current.next();
            }
          } while (FlickityRef.current.selectedIndex != slidePos);
        }
      }
    } catch (error) {
      setLoading(false);
    }
  };

  const getTimeDiff = async () => {
    const resp = await GeneralService.getServerTime();
    setTimeDiff(resp);
  };

  useEffect(() => {
    if (Date && Month) {
      refreshData();
    }
    getTimeDiff();
  }, [Date, Month, Page]);

  useEffect(() => {
    if (router.isReady) {
      setWasMounted(true);
      // mengatur URL
      const mm = moment(month, "YYYY-MM");
      const md = moment(date, "YYYY-MM-DD");
      // check format regex
      const reM = /[0-9]{4}-[0-9]{2}/;
      const reD = /[0-9]{4}-[0-9]{2}-[0-9]{2}/;
      // validasi format dan validasi tahun
      if (
        mm.year() == md.year() &&
        (mm.month() == md.month()) == mm.isValid() &&
        md.isValid() &&
        reM.test(month) &&
        reD.test(date)
      ) {
        setDate(date);
        setMonth(month);
      } else {
        const m = moment().format("YYYY-MM");
        const d = moment().format("YYYY-MM-DD");
        router.push(
          {
            pathname: router.pathname,
            query: { month: m, date: d },
          },
          undefined,
          { shallow: true },
        );
        setDate(d);
        setMonth(m);
      }
    }
  }, [month, date]);

  return (
    <>
      {Loading && (
        <div>
          <header
            className="py-8 bg-blue mb-8 z-index-0"
            style={{ backgroundImage: "none" }}
          >
            <div className="container">
              <div className="row align-items-end">
                {/* <nav aria-label="breadcrumb">
                  <ol className="breadcrumb breadcrumb-scroll">
                    <li className="breadcrumb-item">
                      <a className="text-gray-700" href="#">
                        <Skeleton></Skeleton>
                      </a>
                    </li>
                    <li
                      className="breadcrumb-item text-blue active"
                      aria-current="page"
                    >
                      <Skeleton></Skeleton>
                    </li>
                  </ol>
                </nav>*/}
                <div style={{ overflowX: "auto", display: "flex" }}>
                  {[...Array(10)].map((v, i) => (
                    <Skeleton key={i} className="h-100p w-100p mx-2"></Skeleton>
                  ))}
                </div>
              </div>
            </div>
          </header>

          <div className="container z-index-0">
            <div className="d-lg-flex flex-wrap">
              <div className="mb-5">
                <p className="mb-lg-0">
                  <Skeleton />
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col-xl-9 mb-9">
                <div className="card border shadow p-3 rounded-5 mb-5 lift">
                  <div className="row gx-0">
                    <div style={{ maxWidth: "80px" }}>
                      <Skeleton inline={true} height={80} />
                    </div>
                    <div className="col-12 colmd-9 col-lg-9 col-xl-9 d-md-block">
                      <div className="card-body p-2">
                        <Skeleton className="mb-0" width={200} />

                        <Skeleton className="mb-4" width={250} />

                        <ul className="nav mx-n3 d-block d-md-flex">
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                              {/* <div className="font-size-sm">{item.speaker}</div> */}
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                              {/* <div className="font-size-sm">{item.speaker}</div> */}
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="col-auto d-none pe-xl-2 d-lg-flex place-center">
                      <Skeleton />
                    </div>
                  </div>
                </div>
                <div className="card border shadow p-3 rounded-5 mb-5 lift">
                  <div className="row gx-0">
                    <div style={{ maxWidth: "80px" }}>
                      <Skeleton inline={true} height={80} />
                    </div>
                    <div className="col-12 colmd-9 col-lg-9 col-xl-9 d-md-block">
                      <div className="card-body p-2">
                        <Skeleton className="mb-0" width={200} />

                        <Skeleton className="mb-4" width={250} />

                        <ul className="nav mx-n3 d-block d-md-flex">
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                              {/* <div className="font-size-sm">{item.speaker}</div> */}
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                              {/* <div className="font-size-sm">{item.speaker}</div> */}
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="col-auto d-none pe-xl-2 d-lg-flex place-center">
                      <Skeleton />
                    </div>
                  </div>
                </div>
                <div className="card border shadow p-3 rounded-5 mb-5 lift">
                  <div className="row gx-0">
                    <div style={{ maxWidth: "80px" }}>
                      <Skeleton inline={true} height={80} />
                    </div>
                    <div className="col-12 colmd-9 col-lg-9 col-xl-9 d-md-block">
                      <div className="card-body p-2">
                        <Skeleton className="mb-0" width={200} />

                        <Skeleton className="mb-4" width={250} />

                        <ul className="nav mx-n3 d-block d-md-flex">
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                              {/* <div className="font-size-sm">{item.speaker}</div> */}
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                              {/* <div className="font-size-sm">{item.speaker}</div> */}
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="col-auto d-none pe-xl-2 d-lg-flex place-center">
                      <Skeleton />
                    </div>
                  </div>
                </div>
                <div className="card border shadow p-3 rounded-5 mb-5 lift">
                  <div className="row gx-0">
                    <div style={{ maxWidth: "80px" }}>
                      <Skeleton inline={true} height={80} />
                    </div>
                    <div className="col-12 colmd-9 col-lg-9 col-xl-9 d-md-block">
                      <div className="card-body p-2">
                        <Skeleton className="mb-0" width={200} />

                        <Skeleton className="mb-4" width={250} />

                        <ul className="nav mx-n3 d-block d-md-flex">
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                              {/* <div className="font-size-sm">{item.speaker}</div> */}
                            </div>
                          </li>
                          <li className="nav-item px-3 mb-3 mb-md-0">
                            <div className="d-flex align-items-center">
                              <Skeleton
                                className="me-1 d-flex text-secondary icon-uxs"
                                width={80}
                              />
                              {/* <div className="font-size-sm">{item.speaker}</div> */}
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="col-auto d-none pe-xl-2 d-lg-flex place-center">
                      <Skeleton />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xl-3 px-4 mb-5 mb-xl-0">
                <Skeleton className="h-300p mb-5" count={2}></Skeleton>
              </div>
            </div>
          </div>
        </div>
      )}

      {!Loading && (
        <>
          <header
            className="py-8 bg-blue mb-6 z-index-0"
            style={{ backgroundImage: "none" }}
          >
            <div className="container">
              <div className="row align-items-end">
                {/* <nav aria-label="breadcrumb">
                  <ol className="breadcrumb breadcrumb-scroll">
                    <li className="breadcrumb-item">
                      <a className="text-gray-700" href="#">
                        Home
                      </a>
                    </li>
                    <li
                      className="breadcrumb-item text-blue active"
                      aria-current="page"
                    >
                      Jadwal Pelatihan
                    </li>
                  </ol>
                </nav>*/}
                <div className="col-md mb-md-0">
                  <h3 className="fw-bolder text-white mb-0">
                    Jadwal Pelatihan
                  </h3>
                  <p className="mb-lg-0 text-white">
                    Rencanakan dan temukan jadwal pelatihan sesuai dengan agenda
                    mu
                  </p>
                </div>
                <div className="col-md-auto col-sm-12">
                  <div className="d-lg-flex rounded">
                    <div className="d-lg-flex flex-wrap">
                      <div>
                        <div className="d-flex align-items-center h-50p">
                          <div className="form-floating col-12">
                            <select
                              className="form-select form-select-sm form-control-sm ps-5 shadow-none text-primary fw-medium rounded-xl"
                              value={Month}
                              onChange={filterMonth}
                            >
                              {ListMonths.map((Month) => (
                                <option value={Month}>
                                  {moment(Month)
                                    .locale("id")
                                    .format("MMMM YYYY")}
                                </option>
                              ))}
                            </select>
                            <label className="ps-5">Pilih Bulan</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="mt-8 z-index-2">
                <section>
                  <div className="container container-wd">
                    {LoadingPelatihan || Loading ? (
                      <>
                        <div style={{ overflowX: "auto", display: "flex" }}>
                          {[...Array(10)].map((v, i) => (
                            // <div className="col-3">
                            <Skeleton
                              key={i}
                              className="h-100p w-100p mx-2"
                            ></Skeleton>
                            // {/* </div> */}
                          ))}
                        </div>
                      </>
                    ) : (
                      <>
                        <Flickity
                          flickityRef={(c) => {
                            FlickityRef.current = c;
                          }}
                          className={"mx-n3"}
                          elementType={"div"}
                          elementpageDotsType={"div"}
                          options={flickityOptions}
                          disableImagesLoaded={false}
                        >
                          {ListHeaderPelatihan.data?.map((Header) => (
                            <div className="col-auto mb-3 mx-2">
                              {Header.jumlah_pelatihan >= 1 && (
                                <label className="date-label">
                                  <div
                                    className={
                                      moment(Header.tanggal).format(
                                        "YYYY-MM-DD",
                                      ) == Date
                                        ? "rounded-lg d-flex place-center bg-gray-300"
                                        : "rounded-lg d-flex place-center"
                                    }
                                  >
                                    <input
                                      checked={
                                        moment(Header.tanggal).format(
                                          "YYYY-MM-DD",
                                        ) == Date
                                      }
                                      type="radio"
                                      name="date[]"
                                      className="card-input-element d-none"
                                      value={moment(Header.tanggal).format(
                                        "YYYY-MM-DD",
                                      )}
                                      onChange={filterDate}
                                    />
                                    <div className="card-date align-items-center text-center rounded-lg place-center w-100p h-100p py-4">
                                      <p className="mb-0 fw-bolder font-size-xxl">
                                        <Moment locale="id" format="DD">
                                          {Header.tanggal}
                                        </Moment>
                                      </p>
                                      <span className="mb-0 fw-normal">
                                        <Moment locale="id" format="MMMM">
                                          {Header.tanggal}
                                        </Moment>
                                      </span>
                                    </div>
                                  </div>
                                </label>
                              )}
                              {Header.jumlah_pelatihan <= 0 && (
                                <label className="date-label">
                                  <div className="rounded-lg d-flex place-center">
                                    <input
                                      type="radio"
                                      name="demo"
                                      className="card-input-element d-none"
                                      id="demo1"
                                      disabled
                                    />
                                    <div className="card-date align-items-center text-center rounded-lg place-center w-100p h-100p py-4">
                                      <p className="mb-0 fw-bolder font-size-xxl">
                                        <Moment locale="id" format="DD">
                                          {Header.tanggal}
                                        </Moment>
                                      </p>
                                      <span className="mb-0 fw-normal">
                                        <Moment locale="id" format="MMMM">
                                          {Header.tanggal}
                                        </Moment>
                                      </span>
                                    </div>
                                  </div>
                                </label>
                              )}
                            </div>
                          ))}
                        </Flickity>
                      </>
                    )}
                  </div>
                </section>
              </div>
            </div>
          </header>
          <div className="container z-index-0">
            <div className="row mb-9">
              {ListPelatihan.data?.length > 0 && !LoadingPelatihan && (
                <div className="col-xl-9 pt-lg-6">
                  {ListPelatihan.data?.map((Pelatihan) => {
                    Pelatihan = {
                      ...Pelatihan,
                      current_server_time: timeDiff.serverTime,
                      diff: timeDiff.diff,
                    };
                    return (
                      <div className="col-lg mb-5">
                        <CardPelatihan
                          useTanggalPelatihan={true}
                          cardClassName="rounded-5 border p-3 lift"
                          {...Pelatihan}
                        ></CardPelatihan>
                      </div>
                    );
                  })}
                  <div className="d-flex justify-content-center align-items-center my-9">
                    <Pagination
                      pageSize={ListPelatihan.meta?.last_page}
                      currentPage={Page}
                    ></Pagination>
                  </div>
                </div>
              )}
              {ListPelatihan.data?.length == 0 && !LoadingPelatihan && (
                <div className="col-xl-9 pt-lg-6">
                  <div className="row">
                    <div className="col-12 mx-auto text-center">
                      <h2 className="fw-bolder mb-0">Belum Ada Pelatihan</h2>
                      <p className="font-size-14 mb-6">
                        Pelatihan pada tanggal&nbsp;
                        <span className="text-blue font-size-14 fw-bolder">
                          <Moment locale="id" format="DD-MM-YYYY">
                            {Date}
                          </Moment>
                        </span>
                        &nbsp;belum tersedia
                      </p>
                      <img
                        src="/assets/img/empty-state/no-data.png"
                        alt="..."
                        className="img-fluid align-self-center"
                        style={{ width: "500px" }}
                      />
                    </div>
                  </div>
                </div>
              )}
              {LoadingPelatihan && (
                <div className="col-xl-9 pt-lg-6">
                  <div className="card border shadow p-3 rounded-5 mb-5 lift">
                    <div className="row gx-0">
                      <div style={{ maxWidth: "80px" }}>
                        <Skeleton inline={true} height={80} />
                      </div>
                      <div className="col-12 colmd-9 col-lg-9 col-xl-9 d-md-block">
                        <div className="card-body p-2">
                          <Skeleton className="mb-0" width={200} />

                          <Skeleton className="mb-4" width={250} />

                          <ul className="nav mx-n3 d-block d-md-flex">
                            <li className="nav-item px-3 mb-3 mb-md-0">
                              <div className="d-flex align-items-center">
                                <Skeleton
                                  className="me-1 d-flex text-secondary icon-uxs"
                                  width={80}
                                />
                              </div>
                            </li>
                            <li className="nav-item px-3 mb-3 mb-md-0">
                              <div className="d-flex align-items-center">
                                <Skeleton
                                  className="me-1 d-flex text-secondary icon-uxs"
                                  width={80}
                                />
                              </div>
                            </li>
                            <li className="nav-item px-3 mb-3 mb-md-0">
                              <div className="d-flex align-items-center">
                                <Skeleton
                                  className="me-1 d-flex text-secondary icon-uxs"
                                  width={80}
                                />
                                {/* <div className="font-size-sm">{item.speaker}</div> */}
                              </div>
                            </li>
                            <li className="nav-item px-3 mb-3 mb-md-0">
                              <div className="d-flex align-items-center">
                                <Skeleton
                                  className="me-1 d-flex text-secondary icon-uxs"
                                  width={80}
                                />
                                {/* <div className="font-size-sm">{item.speaker}</div> */}
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="col-auto d-none pe-xl-2 d-lg-flex place-center">
                        <Skeleton />
                      </div>
                    </div>
                  </div>
                  <div className="card border shadow p-3 rounded-5 mb-5 lift">
                    <div className="row gx-0">
                      <div style={{ maxWidth: "80px" }}>
                        <Skeleton inline={true} height={80} />
                      </div>
                      <div className="col-12 colmd-9 col-lg-9 col-xl-9 d-md-block">
                        <div className="card-body p-2">
                          <Skeleton className="mb-0" width={200} />

                          <Skeleton className="mb-4" width={250} />

                          <ul className="nav mx-n3 d-block d-md-flex">
                            <li className="nav-item px-3 mb-3 mb-md-0">
                              <div className="d-flex align-items-center">
                                <Skeleton
                                  className="me-1 d-flex text-secondary icon-uxs"
                                  width={80}
                                />
                              </div>
                            </li>
                            <li className="nav-item px-3 mb-3 mb-md-0">
                              <div className="d-flex align-items-center">
                                <Skeleton
                                  className="me-1 d-flex text-secondary icon-uxs"
                                  width={80}
                                />
                              </div>
                            </li>
                            <li className="nav-item px-3 mb-3 mb-md-0">
                              <div className="d-flex align-items-center">
                                <Skeleton
                                  className="me-1 d-flex text-secondary icon-uxs"
                                  width={80}
                                />
                                {/* <div className="font-size-sm">{item.speaker}</div> */}
                              </div>
                            </li>
                            <li className="nav-item px-3 mb-3 mb-md-0">
                              <div className="d-flex align-items-center">
                                <Skeleton
                                  className="me-1 d-flex text-secondary icon-uxs"
                                  width={80}
                                />
                                {/* <div className="font-size-sm">{item.speaker}</div> */}
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="col-auto d-none pe-xl-2 d-lg-flex place-center">
                        <Skeleton />
                      </div>
                    </div>
                  </div>
                  <div className="card border shadow p-3 rounded-5 mb-5 lift">
                    <div className="row gx-0">
                      <div style={{ maxWidth: "80px" }}>
                        <Skeleton inline={true} height={80} />
                      </div>
                      <div className="col-12 colmd-9 col-lg-9 col-xl-9 d-md-block">
                        <div className="card-body p-2">
                          <Skeleton className="mb-0" width={200} />

                          <Skeleton className="mb-4" width={250} />

                          <ul className="nav mx-n3 d-block d-md-flex">
                            <li className="nav-item px-3 mb-3 mb-md-0">
                              <div className="d-flex align-items-center">
                                <Skeleton
                                  className="me-1 d-flex text-secondary icon-uxs"
                                  width={80}
                                />
                              </div>
                            </li>
                            <li className="nav-item px-3 mb-3 mb-md-0">
                              <div className="d-flex align-items-center">
                                <Skeleton
                                  className="me-1 d-flex text-secondary icon-uxs"
                                  width={80}
                                />
                              </div>
                            </li>
                            <li className="nav-item px-3 mb-3 mb-md-0">
                              <div className="d-flex align-items-center">
                                <Skeleton
                                  className="me-1 d-flex text-secondary icon-uxs"
                                  width={80}
                                />
                                {/* <div className="font-size-sm">{item.speaker}</div> */}
                              </div>
                            </li>
                            <li className="nav-item px-3 mb-3 mb-md-0">
                              <div className="d-flex align-items-center">
                                <Skeleton
                                  className="me-1 d-flex text-secondary icon-uxs"
                                  width={80}
                                />
                                {/* <div className="font-size-sm">{item.speaker}</div> */}
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="col-auto d-none pe-xl-2 d-lg-flex place-center">
                        <Skeleton />
                      </div>
                    </div>
                  </div>
                  <div className="card border shadow p-3 rounded-5 mb-5 lift">
                    <div className="row gx-0">
                      <div style={{ maxWidth: "80px" }}>
                        <Skeleton inline={true} height={80} />
                      </div>
                      <div className="col-12 colmd-9 col-lg-9 col-xl-9 d-md-block">
                        <div className="card-body p-2">
                          <Skeleton className="mb-0" width={200} />

                          <Skeleton className="mb-4" width={250} />

                          <ul className="nav mx-n3 d-block d-md-flex">
                            <li className="nav-item px-3 mb-3 mb-md-0">
                              <div className="d-flex align-items-center">
                                <Skeleton
                                  className="me-1 d-flex text-secondary icon-uxs"
                                  width={80}
                                />
                              </div>
                            </li>
                            <li className="nav-item px-3 mb-3 mb-md-0">
                              <div className="d-flex align-items-center">
                                <Skeleton
                                  className="me-1 d-flex text-secondary icon-uxs"
                                  width={80}
                                />
                              </div>
                            </li>
                            <li className="nav-item px-3 mb-3 mb-md-0">
                              <div className="d-flex align-items-center">
                                <Skeleton
                                  className="me-1 d-flex text-secondary icon-uxs"
                                  width={80}
                                />
                                {/* <div className="font-size-sm">{item.speaker}</div> */}
                              </div>
                            </li>
                            <li className="nav-item px-3 mb-3 mb-md-0">
                              <div className="d-flex align-items-center">
                                <Skeleton
                                  className="me-1 d-flex text-secondary icon-uxs"
                                  width={80}
                                />
                                {/* <div className="font-size-sm">{item.speaker}</div> */}
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="col-auto d-none pe-xl-2 d-lg-flex place-center">
                        <Skeleton />
                      </div>
                    </div>
                  </div>
                </div>
              )}
              <div className="col-xl-3 pt-lg-6 px-4 mb-5 mb-xl-0">
                <SideWidget loading={Loading} />
                <SideImageWidget loading={Loading} />
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
}
