import React, { useState, useEffect } from "react";
import Link from "next/link";
import "moment/locale/id";
import PortalService from "./../../services/PortalService";

import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
//
import { useRouter } from "next/router";

function FaqDetail() {
  const router = useRouter();

  const [dataFaq, setdataFaq] = useState(null);
  const [loading, setLoading] = useState(false);
  const [faqCategory, setFaqCategory] = useState(null);
  const [currentFaq, setCurrentFaq] = useState(null);
  const [skeletons, setSkeletons] = useState([...Array(3).keys()]);
  const [footerskeletons, setFooterSkeletons] = useState([...Array(6).keys()]);

  useEffect(async () => {
    try {
      if (router.isReady) {
        setLoading(true);
        const dataFaq = await PortalService.faqByKategori(
          router.query.faqId,
          99999,
        );
        if (router.query.hasOwnProperty("category")) {
          const faqCategory = await PortalService.kategoriList("FAQ");
          setFaqCategory(faqCategory.data.result.Data);
          setCurrentFaq(
            JSON.parse(router.query.category).filter(
              (elem) => elem.id == router.query.faqId,
            )[0],
          );
        } else {
          const faqCategory = await PortalService.kategoriList("FAQ");
          // console.log(faqCategory);
          setFaqCategory(faqCategory.data.result.Data);
          setCurrentFaq(
            faqCategory.data.result.Data?.filter(
              (elem) => elem.id == router.query.faqId,
            )[0],
          );
        }
        setdataFaq(dataFaq.data.result.Data);
        setLoading(false);
        if (dataFaq.data.result.Data.length > 0) {
          setSkeletons([...Array(dataFaq.data.Data.length).keys()]);
        }
      }
    } catch (err) {
      console.error(err);
    }
  }, [router]);

  return (
    <>
      <header
        className="py-6 bg-blue mb-6 z-index-0"
        style={{ backgroundImage: "none" }}
      >
        <div className="container">
          <div className="row align-items-end" data-aos="fade-up">
            <div className="col-md mb-md-0">
              <h3 className="fw-bolder text-white mb-0">
                Frequently Asked Questions
              </h3>
              <p className="text-white mb-0">
                Hi Digiers, Telusuri informasi yang kamu butuhkan
              </p>
            </div>
          </div>
        </div>
      </header>

      <div className="container pt-lg-6 mb-9">
        <div className="">
          {/* Skeleton */}
          {loading &&
            skeletons.map((index) => (
              <Skeleton
                height={80}
                className="border rounded-5 shadow mb-3 overflow-hidden"
              />
            ))}
          <h3 className="fw-bolder">{currentFaq?.nama}</h3>
          {!loading && (
            <div className="mb-8" id="accordionCurriculum">
              {dataFaq != null && dataFaq[0] == null && (
                <div className="row justify-content-center">
                  <div className="col-3">
                    <img
                      src="/assets/img/empty-state/no-data.png"
                      className="w-100p img-responsive"
                    />
                    <h2 className="mb-0" style={{ textAlign: "center" }}>
                      Tidak ada data!
                    </h2>
                  </div>
                </div>
              )}

              {dataFaq &&
                dataFaq[0] !== null &&
                dataFaq.map((faq, index) => (
                  <div className="border rounded-5 mb-3 overflow-hidden">
                    <div
                      className="d-flex align-items-center"
                      id={`collapse-heading-${faq.id}`}
                    >
                      <h5 className="mb-0 w-100 text-left">
                        <button
                          className="d-flex align-items-center p-5 min-height-60 text-dark fw-medium collapse-accordion-toggle line-clamp-2"
                          type="button"
                          data-bs-toggle="collapse"
                          data-bs-target={`#collapse-item-${faq.id}`}
                          aria-expanded={index == 0 ? "false" : "false"}
                          aria-controls={`collapse-item-${faq.id}`}
                        >
                          {faq.judul}

                          <span className="ms-auto text-blue d-flex">
                            <svg
                              width="15"
                              height="2"
                              viewBox="0 0 15 2"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <rect width="15" height="2" fill="currentColor" />
                            </svg>

                            <svg
                              width="15"
                              height="16"
                              viewBox="0 0 15 16"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path d="M0 7H15V9H0V7Z" fill="currentColor" />
                              <path
                                d="M6 16L6 8.74228e-08L8 0L8 16H6Z"
                                fill="currentColor"
                              />
                            </svg>
                          </span>
                        </button>
                      </h5>
                    </div>

                    <div
                      id={`collapse-item-${faq.id}`}
                      className={`collapse ${index == 0 ? "" : ""}`}
                      aria-labelledby={`collapse-heading-${faq.id}`}
                      data-parent="#accordionCurriculum"
                    >
                      <div className="p-6 border-top">
                        <div
                          className="faq_jawaban text-justify"
                          dangerouslySetInnerHTML={{ __html: faq.jawaban }}
                        ></div>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
          )}
        </div>
      </div>

      <section className="bg-gray py-8">
        <div className="container">
          <div className="row align-items-end mb-4 mb-md-7">
            <div className="col-md mb-4 mb-md-0">
              <h3 className="fw-bolder mb-0">Kategori FAQ Lainnya</h3>
              <p className="mb-0">Temukan informasi pada kategori lainnya</p>
            </div>
            <div className="col-md-auto">
              <Link href="/faq">
                <a className="d-flex align-items-center fw-medium">
                  Lihat Semua
                  <div className="ms-2 d-flex">
                    <svg
                      width="10"
                      height="10"
                      viewBox="0 0 10 10"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M7.7779 4.6098L3.32777 0.159755C3.22485 0.0567475 3.08745 0 2.94095 0C2.79445 0 2.65705 0.0567475 2.55412 0.159755L2.2264 0.487394C2.01315 0.700889 2.01315 1.04788 2.2264 1.26105L5.96328 4.99793L2.22225 8.73895C2.11933 8.84196 2.0625 8.97928 2.0625 9.1257C2.0625 9.27228 2.11933 9.4096 2.22225 9.51269L2.54998 9.84025C2.65298 9.94325 2.7903 10 2.9368 10C3.0833 10 3.2207 9.94325 3.32363 9.84025L7.7779 5.38614C7.88107 5.2828 7.93774 5.14484 7.93741 4.99817C7.93774 4.85094 7.88107 4.71305 7.7779 4.6098Z"
                        fill="currentColor"
                      />
                    </svg>
                  </div>
                </a>
              </Link>
            </div>
          </div>
          {loading && (
            <div className="row row-cols-md-3 row-cols-lg-3 row-cols-xl-3">
              {footerskeletons.map((index) => (
                <Skeleton
                  //   baseColor="#009EF7"
                  height={90}
                  className="col-md mb-md-6 mb-4 px-2"
                ></Skeleton>
              ))}
            </div>
          )}
          {!loading && (
            <div className="row row-cols-md-3 row-cols-lg-3 row-cols-xl-3">
              {faqCategory &&
                faqCategory[0] !== null &&
                faqCategory.map((category) => {
                  if (category.id != router.query.faqId) {
                    return (
                      <div className="col-md mb-md-6 mb-4 px-2">
                        <Link
                          href={{
                            pathname: `/faq/${[category.id]}`,
                            query: {
                              category: JSON.stringify(faqCategory),
                            },
                          }}
                          as={`/faq/${[category.id]}`}
                        >
                          <a className="card rounded-5 border icon-category icon-category-sm p-5 lift shadow-dark-hover">
                            <div className="row align-items-center mx-n3">
                              <div className="col-auto px-3">
                                <div className="icon-h-p secondary">
                                  <i className="bi-info-circle"></i>
                                </div>
                              </div>

                              <div className="col px-3">
                                <div className="card-body p-0">
                                  <h6 className="mb-0 line-clamp-1">
                                    {category.nama}
                                  </h6>
                                </div>
                              </div>
                            </div>
                          </a>
                        </Link>
                      </div>
                    );
                  }
                })}
            </div>
          )}
        </div>
      </section>
    </>
  );
}
FaqDetail.bodyBackground = "bg-white";
export default FaqDetail;
