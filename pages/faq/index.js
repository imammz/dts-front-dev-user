import React, { useState, useEffect } from "react";
import Link from "next/link";
import axios from "axios";
import Moment from "react-moment";
import "moment/locale/id";
import Select from "react-select";
import PortalService from "./../../services/PortalService";

import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
//
import FlipMove from "react-flip-move";
import { useRouter } from "next/router";

function ListFaq() {
  const [dataFaq, setdataFaq] = useState(null);
  const [loading, setLoading] = useState(false);
  const [totalFaq, setTotalFaq] = useState(0);
  const [faqCategory, setFaqCategory] = useState(null);
  const [skeletons, setSkeletons] = useState([...Array(6).keys()]);
  const router = useRouter();

  useEffect(async () => {
    try {
      setLoading(true);
      const listFaqCatgory = await PortalService.kategoriList("FAQ");
      setTotalFaq(listFaqCatgory.data.result.Total);
      setFaqCategory(listFaqCatgory.data.result.Data);

      setLoading(false);
    } catch (err) {
      console.error(err);
    }
  }, []);

  return (
    <>
      <header className="py-6 bg-blue mb-6 z-index-0">
        <div className="container">
          <div className="row align-items-end" data-aos="fade-up">
            {/* <nav aria-label="breadcrumb">
                        <ol className="breadcrumb breadcrumb-scroll">
                            <li className="breadcrumb-item">
                                <Link href="/">
                                    <a className="text-gray-700">
                                        Home
                                    </a>
                                </Link>
                            </li>
                            <li className="breadcrumb-item text-blue active" aria-current="page">
                                FAQ
                            </li>
                        </ol>
                    </nav>*/}
            <div className="col-md mb-md-0">
              <h3 className="fw-bolder text-white mb-0">
                Frequently Asked Questions
              </h3>
              <p className="text-white mb-0">
                Hi Digiers, Telusuri informasi yang kamu butuhkan
              </p>
            </div>
          </div>
        </div>
      </header>

      <section className="pt-lg-6">
        <div className="container">
          <div className="row row-cols-md-3 row-cols-lg-3 row-cols-xl-3 mb-9">
            {loading &&
              skeletons.map((index) => (
                <div className="col-md mb-md-6 mb-4 px-2">
                  <Skeleton
                    height={90}
                    className="card card-border-hover border icon-category icon-category-sm p-5 lift shadow-dark-hover"
                  />
                </div>
              ))}
            {!loading &&
              faqCategory &&
              faqCategory.map((category) => (
                <div className="col-md mb-md-6 mb-4 px-2" key={category.id}>
                  <Link
                    href={{
                      pathname: `/faq/${[category.id]}`,
                      query: {
                        category: JSON.stringify(faqCategory),
                      },
                    }}
                    as={`/faq/${[category.id]}`}
                  >
                    <a className="card rounded-5 border icon-category icon-category-sm p-5 lift shadow-dark-hover">
                      <div className="row align-items-center mx-n3">
                        <div className="col-auto px-3">
                          <div className="icon-h-p secondary">
                            <i className="bi bi-info-circle"></i>
                          </div>
                        </div>

                        <div className="col px-3">
                          <div className="card-body p-0">
                            <h6 className="mb-0 line-clamp-1">
                              {category.nama}
                            </h6>
                            <p className="mb-0 line-clamp-1">
                              {category.jml_data} Pertanyaan
                            </p>
                          </div>
                        </div>
                      </div>
                    </a>
                  </Link>
                </div>
              ))}
          </div>
        </div>
      </section>
    </>
  );
}
export default ListFaq;
