import React, { useEffect, useState } from "react";
import Link from "next/link";
import Lottie, { useLottie } from "lottie-react";
import PortalService from "../services/PortalService";
import * as animation from "./../public/assets/json/file-not-found-animation.json";
import { useRouter } from "next/router";
import EncryptionHelper from "../src/helper/EncryptionHelper";
import SideWidget from "../components/SideWidget";
import SideImage from "../components/SideImage";

export default function Verified() {
  const options = {
    animationData: animation,
    loop: true,
  };
  const lottie = useLottie(options);
  lottie.setSpeed(2);
  const { View } = lottie;
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const [message, setMessage] = useState("");

  useEffect(() => {
    if (router.isReady) {
      setMessage(router.query.message);
    }
  }, [router]);

  return (
    <>
      <div className="container mb-8">
        <div className="row py-8">
          <div className="col-lg-7 border rounded-5 border-0 border-lg border-xl mx-auto">
            <div className="d-block rounded-5">
              <div className="pt-6 px-5 px-lg-3 px-xl-5 text-center">
                <h3 className="fw-bold text-red mb-1 text-center">
                  Pendaftaran Pelatihan Gagal
                </h3>
                <div className="row">
                  {/* Illustrator */}

                  <div className="text-center mb-0">
                    <div dangerouslySetInnerHTML={{ __html: message }}></div>
                  </div>
                  <div className="w-50 m-auto">{View}</div>
                  <div className="text-center my-7">
                    <a
                      href="#"
                      className="fw-semi-bold"
                      onClick={() => router.back()}
                    >
                      Kembali ke halaman sebelumnya
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
