import React, { useState } from "react";
import * as Yup from "yup";
import Link from "next/link";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import ValidationMessage from "../../src/validation-message";
import Success from "../../components/Modal/Success";
import Failed from "../../components/Modal/Failed";
import PortalService from "../../services/PortalService";
import { useRouter } from "next/router";
import PortalHelper from "../../src/helper/PortalHelper";
import ReCAPTCHA from "react-google-recaptcha";

export default function Verified({ showLoadingModal }) {
  const [serverMessage, setServerMessage] = useState("");
  const [showFailModal, setShowFailModal] = useState(false);
  const [showSuccessModal, setShowSuccessModal] = useState(false);
  const [captchaErrorMessage, setCaptchaErrorMessage] = useState();
  const [userCaptchaResponse, setUserCaptchaResponse] = useState(null);
  const router = useRouter();

  const validationSchema = Yup.object({
    email: Yup.string()
      .required(ValidationMessage.required.replace("__field", "Email"))
      .email(ValidationMessage.invalid.replace("__field", "Email")),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: "onBlur",
    resolver: yupResolver(validationSchema),
  });

  const closeFailModal = () => {
    showLoadingModal(false);
    setShowFailModal(false);
  };
  const closeSuccessModal = () => {
    showLoadingModal(false);
    setShowSuccessModal(false);
    showLoadingModal(true);
    router.replace({
      pathname: "/forgot-pass/verifikasi-email",
    });
  };
  class CaptchaError extends Error {
    constructor(message) {
      super(message); // (1)
      this.name = "CaptchaError"; // (2)
    }
  }

  async function onSubmit(data) {
    showLoadingModal(true);
    try {
      if (userCaptchaResponse == null) {
        throw new CaptchaError("Captcha tidak boleh kosong");
      }
      const res = await PortalService.lupaPassword(data);
      showLoadingModal(false);
      if (res.data.success) {
        const result = res.data.result;
        setServerMessage(res.data.message || "Verifikasi email berhasil!");
        setShowSuccessModal(true);

        PortalHelper.saveUserEmailForgotPass(data.email);
      } else {
        if (res.data?.result) {
          throw Error(PortalHelper.handleErrorResponse(res.data.result));
        } else {
          throw Error(res.data.message);
        }
      }
      showLoadingModal(false);
    } catch (err) {
      showLoadingModal(false);
      if (err.name == "CaptchaError") {
        setCaptchaErrorMessage(err.message);
      } else {
        const message = err.message;
        setServerMessage(
          message || "Terjadi kesalahan saat mencoba terhubung dengan server!",
        );
        setShowFailModal(true);
      }
    }
  }

  return (
    <>
      <header
        className="py-8 mb-6 bg-blue z-index-0"
        style={{ backgroundImage: "none" }}
      >
        <div className="container">
          <div className="row align-items-center" data-aos="fade-up">
            <h3 className="fw-bolder text-white mb-0">Pemulihan Akun</h3>
          </div>
        </div>
      </header>
      <div className="container pt-6 mb-11">
        <div className="card border rounded-5 px-6 py-8">
          <div className="row align-items-center">
            <div className="col-lg-5">
              {/* Illustrator */}
              <img
                src="/assets/img/empty-state/user-laptop.png"
                className="img-fluid align-self-center"
                style={{ width: "500px" }}
              />
            </div>
            <div className="col-lg-7">
              <div className="d-block rounded bg-light rounded-5 mb-6">
                <form onSubmit={handleSubmit(onSubmit)}>
                  <div className="pt-6 pb-5 px-5 px-lg-3 px-xl-5">
                    <div className="d-flex align-items-center">
                      <div className="mb-4">
                        <h4 className="fw-semi-bold mb-4">Reset Password</h4>
                        <p className="mb-0 text-capitalize">
                          Hi Digiers, Silahkan masukkan email yang kamu gunakan
                          saat mendaftar. Kode OTP akan dikirimkan melalui
                          Email/Inbox mu. Jangan bagikan kode OTP kamu kepada
                          siapapun.
                        </p>
                      </div>
                    </div>
                    <div className="my-2">
                      <div className="form-group mb-2">
                        <div className="row align-items-end" data-aos="fade-up">
                          <div className="col-md mb-4 mb-md-0">
                            <label
                              className="font-size-14 fw-semi-bold"
                              for="email"
                            >
                              Email
                              <span
                                className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                                title="required"
                              >
                                *
                              </span>
                            </label>
                          </div>
                        </div>
                        <input
                          type="text"
                          className={`form-control form-control-sm ${
                            errors.email ? "is-invalid" : ""
                          }`}
                          id="email"
                          placeholder=""
                          name="email"
                          {...register("email")}
                        />
                        <span className="text-red">
                          {errors.email?.message}
                        </span>
                      </div>
                    </div>
                    <div>
                      <div>
                        <ReCAPTCHA
                          sitekey={process.env.CAPTCHA_SITEKEY}
                          onChange={(e) => {
                            setUserCaptchaResponse(e);
                          }}
                        ></ReCAPTCHA>
                      </div>
                      {!userCaptchaResponse && captchaErrorMessage && (
                        <span className="text-red">{captchaErrorMessage}</span>
                      )}
                    </div>
                  </div>
                  <div className="form-row place-order p-5">
                    <button
                      className="btn btn-blue rounded-pill font-size-14 btn-block"
                      type="submit"
                    >
                      KIRIM OTP
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Failed
        show={showFailModal}
        handleClose={closeFailModal}
        children={serverMessage}
      />
      <Success
        show={showSuccessModal}
        handleClose={closeSuccessModal}
        children={serverMessage}
      />
    </>
  );
}
