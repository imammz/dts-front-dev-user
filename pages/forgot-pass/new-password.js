import React, { useState } from "react";
import * as Yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import ValidationMessage from "../../src/validation-message";
import Success from "../../components/Modal/Success";
import Failed from "../../components/Modal/Failed";
import FlipMove from "react-flip-move";
import PortalService from "../../services/PortalService";
import PortalHelper from "../../src/helper/PortalHelper";
import { useRouter } from "next/router";

export default function NewPassword({ showLoadingModal }) {
  const [serverMessage, setServerMessage] = useState("");
  const [showFailModal, setShowFailModal] = useState(false);
  const [showSuccessModal, setShowSuccessModal] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showPasswordConf, setShowPasswordConf] = useState(false);
  const router = useRouter();

  const validationSchema = Yup.object({
    password: Yup.string().matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[-._!"`'#%&,:;<>=@{}~\$\(\)\*\+\/\\\?\[\]\^\|])[A-Za-z\d-._!"`'#%&,:;<>=@{}~\$\(\)\*\+\/\\\?\[\]\^\|]{8,}$/,
      ValidationMessage.password.replace("__field", "Password"),
    ),
    password_confirmation: Yup.string().oneOf(
      [Yup.ref("password"), null],
      ValidationMessage.match
        .replace("__field", "Konfirmasi Password")
        .replace("__ref", "Password"),
    ),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: "onBlur",
    resolver: yupResolver(validationSchema),
  });

  const closeFailModal = () => {
    showLoadingModal(false);
    setShowFailModal(false);
  };
  const closeSuccessModal = () => {
    showLoadingModal(false);
    setShowSuccessModal(false);
    showLoadingModal(true);
    router.replace({
      pathname: "/login",
    });
  };

  async function onSubmit(data) {
    showLoadingModal(true);
    try {
      data["email"] = PortalHelper.getUserEmailForgotPass().email;
      data["pin"] = PortalHelper.getUserEmailForgotPass().pin;
      const res = await PortalService.resetPassword(data);
      showLoadingModal(false);

      if (res.data.success) {
        setServerMessage(res.data.message || "Password berhasil di reset!");
        setShowSuccessModal(true);
      } else {
        if (res.data?.result) {
          throw Error(PortalHelper.handleErrorResponse(res.data.result));
        } else {
          throw Error(res.data.message);
        }
      }
      showLoadingModal(false);
    } catch (err) {
      console.log(err);
      const message = err.message;
      showLoadingModal(false);
      setServerMessage(
        message || "Terjadi kesalahan saat mencoba terhubung dengan server!",
      );
      setShowFailModal(true);
    }
  }

  return (
    <>
      <header
        className="py-8 mb-6 bg-blue z-index-0"
        style={{ backgroundImage: "none" }}
      >
        <div className="container">
          <div className="row align-items-center" data-aos="fade-up">
            <h3 className="fw-bolder text-white mb-0">Pemulihan Akun</h3>
          </div>
        </div>
      </header>
      <div className="container pt-6 mb-11">
        <div className="card border rounded-5 px-6 py-8">
          <div className="row align-items-center">
            <div className="col-lg-5">
              {/* Illustrator */}
              <img
                src="/assets/img/empty-state/otp.png"
                className="img-fluid align-self-center"
                style={{ width: "500px" }}
              />
            </div>
            <div className="col-lg-7">
              <div className="d-block rounded bg-light rounded-5 mb-6">
                <form onSubmit={handleSubmit(onSubmit)}>
                  <div className="pt-6 pb-5 px-5 px-lg-3 px-xl-5">
                    <div className="d-flex align-items-center">
                      <div className="mb-4">
                        <h4 className="fw-semi-bold mb-4">Password Baru</h4>
                        <p className="mb-0 text-capitalize">
                          Hi Digiers, silahkan masukkan password baru untuk
                          akunmu. Jaga kerahasiaan password dan jangan bagikan
                          kepada siapapun.
                        </p>
                      </div>
                    </div>
                    <div className="my-3">
                      <div className="col-12">
                        <div className="form-group mb-2 password-input">
                          <div
                            className="row align-items-end"
                            data-aos="fade-up"
                          >
                            <div className="col-md mb-4 mb-md-0">
                              <label
                                className="font-size-14 fw-semi-bold"
                                for="pass"
                              >
                                Password
                                <span
                                  className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                                  title="required"
                                >
                                  *
                                </span>
                              </label>
                            </div>
                          </div>
                          <div className="input inner-addon right-addon">
                            <span
                              className="icon"
                              onClick={() => {
                                setShowPassword(!showPassword);
                              }}
                            >
                              <FlipMove>
                                {!showPassword && (
                                  <i className="fa fa-eye grey"></i>
                                )}
                                {showPassword && (
                                  <i className="fa fa-eye-slash "></i>
                                )}
                              </FlipMove>
                            </span>
                            <input
                              placeholder="***********"
                              type={showPassword ? "text" : "password"}
                              className={`form-control form-control-sm ${
                                errors.password ? "is-invalid" : ""
                              }`}
                              id="pass"
                              name="password"
                              {...register("password")}
                            />
                          </div>
                          <span className="form-caption font-size-sm text-italic my-2">
                            {!errors.password
                              ? "Minimal 8 Karakter kombinasi huruf kapital, huruf kecil, angka dan simbol."
                              : ""}
                          </span>
                          <span className="text-red">
                            {errors.password?.message}
                          </span>
                        </div>
                      </div>
                      <div className="col-12">
                        <div className="form-group mb-5">
                          <div
                            className="row align-items-end"
                            data-aos="fade-up"
                          >
                            <div className="col-md mb-4 mb-md-0">
                              <label
                                className="font-size-14 fw-semi-bold"
                                for="pass2"
                              >
                                Konfirmasi Password
                                <span
                                  className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                                  title="required"
                                >
                                  *
                                </span>
                              </label>
                            </div>
                          </div>
                          <div className="input inner-addon right-addon">
                            <span
                              className="icon"
                              onClick={() => {
                                setShowPasswordConf(!showPasswordConf);
                              }}
                            >
                              <FlipMove>
                                {!showPasswordConf && (
                                  <i className="fa fa-eye grey"></i>
                                )}
                                {showPasswordConf && (
                                  <i className="fa fa-eye-slash "></i>
                                )}
                              </FlipMove>
                            </span>
                            <input
                              placeholder="***********"
                              type={showPasswordConf ? "text" : "password"}
                              className={`form-control form-control-sm ${
                                errors.password_confirmation ? "is-invalid" : ""
                              }`}
                              id="pass2"
                              name="password_confirmation"
                              {...register("password_confirmation")}
                            />
                          </div>
                          <span className="text-red">
                            {errors.password_confirmation?.message}
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="form-row place-order p-5">
                    <button
                      className="btn btn-blue rounded-pill font-size-14 btn-block"
                      type="submit"
                    >
                      UBAH PASSWORD
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Failed
        show={showFailModal}
        handleClose={closeFailModal}
        children={serverMessage}
      />
      <Success
        show={showSuccessModal}
        handleClose={closeSuccessModal}
        children={serverMessage}
      />
    </>
  );
}
