import react, { useState, useEffect, useRef } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import PortalService from "../../services/PortalService";
import GeneralService from "../../services/GeneralService";
import ProfilService from "../../services/ProfileService";
import CountryCode from "../../data/phone_code.json";

/** Libraries */
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import InputMask from "react-input-mask";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import ValidationMessage from "../../src/validation-message";
import PortalHelper from "../../src/helper/PortalHelper";
import _ from "lodash";
import MasterDataHelper from "../../src/helper/MasterDataHelper";
import Failed from "./../../components/Modal/Failed";
import moment from "moment";

import DatePicker from "react-datepicker";
import eventBus from "../../components/EventBus";
import { CropperHelper } from "../../components/Modal/ImageCropper";
import NotificationHelper from "../../src/helper/NotificationHelper";

function CompleteUserData({ showLoadingModal }) {
  const router = useRouter();
  const formUpdateDataRef = useRef(null);
  const [LoadingSkeleton, setLoadingSkeleton] = useState(false);
  const toggleCloseKonfirmasiModal = useRef(null);
  const [saved, setSaved] = useState(false);
  const [phoneChecked, setPhoneChecked] = useState(false);
  const [Cropper, setCropper] = useState(null);

  const [ListAkademi, setListAkademi] = useState([]);
  const [showPilihanPekerjaan, setShowPilihanPekerjaan] = useState(false);
  const [StatusButtonSubmit, setStatusButtonSubmit] = useState(false);

  const [FListJenisKelamin, setFListJenisKelamin] = useState({
    label: Object.keys(MasterDataHelper.jenisKelamin).map(
      (key) => MasterDataHelper.jenisKelamin[key],
    ),
    value: Object.keys(MasterDataHelper.jenisKelamin).map((key) => key),
  });
  const [FListHubunganKontakDarurat, setFListHubunganKontakDarurat] = useState([
    ...MasterDataHelper.hubunganKontakDarurat,
  ]);
  const [phoneChanged, setPhoneChanged] = useState(false);

  const [FNomorHandphoneDarurat, setFNomorHandphoneDarurat] = useState({
    name: "Indonesia",
    code: "ID",
    emoji: "🇮🇩",
    unicode: "U+1F1EE U+1F1E9",
    image:
      "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ID.svg",
    phone: "62",
  });

  // acceptImageProfile = async () => {

  // }
  const onCropComplete = (file) => {
    try {
      setUrl(URL.createObjectURL(file));
      // console.log(file);
      setValue("upload_foto", file);
      setError("upload_foto", "");
    } catch (error) {
      console.log(error);
    }
  };

  const [FNomorHandphone, setFNomorHandphone] = useState({
    name: "Indonesia",
    code: "ID",
    emoji: "🇮🇩",
    unicode: "U+1F1EE U+1F1E9",
    image:
      "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ID.svg",
    phone: "62",
  });
  const [FListPendidikan, setFListPendidikan] = useState([]);
  const [FListProvinsi, setFListProvinsi] = useState([]);
  const [FListKota, setFListKota] = useState([]);
  const [isNikEligible, setIsNikEligible] = useState(false);
  const [FListKecamatan, setFListKecamatan] = useState([]);
  const [FListKelurahan, setFListKelurahan] = useState([]);
  const [FListStatusPekerjaan, setFListStatusPekerjaan] = useState([]);
  const [FListBidangPekerjaan, setFListBidangPekerjaan] = useState([]);
  const [FReadOnlyKota, setFReadOnlyKota] = useState(true);
  const [FReadOnlyKecamatan, setFReadOnlyKecamatan] = useState(true);
  const [FReadOnlyKelurahan, setFReadOnlyKelurahan] = useState(true);
  const [FormProfil, setFormProfil] = useState([]);
  const [FormProfilError, setFormProfilError] = useState([]);
  const [capilLoading, setCapilLoading] = useState(false);
  const [inputNIK, setinputNIK] = useState(0);
  const [nikValidate, setNikValidate] = useState({});
  const [showVerifikasiModal, setShowVerifikasiModal] = useState(false);
  const [serverMessage, setServerMessage] = useState("");
  const [showFailModal, setShowFailModal] = useState("");
  const [url, setUrl] = useState(null);

  const [tanggalLahir, setTanggalLahir] = useState("");
  const [nomorHandphone, setNomorHandphone] = useState("");
  const [nomorHandphoneDarurat, setNomorHandphoneDarurat] = useState("");

  const getListAkademi = async () => {
    const resAkademi = await PortalService.akademiList();
    setListAkademi(resAkademi.data.result);
  };

  const maxDate = new Date();
  maxDate.setFullYear(maxDate.getFullYear() - 5); // max tanggal umur 5 tahuns
  const onChangeStatusPekerjaan = async (e) => {
    // setFormProfil({ status_pekerjaan_id: e.target.value });
    setShowPilihanPekerjaan(true);
    setValue("status_pekerjaan_id", e.target.value);
  };
  const onChangeProvinsi = async (e) => {
    setFListKota([]);
    setFListKecamatan([]);
    setFListKelurahan([]);
    setFReadOnlyKota(true);
    setFReadOnlyKecamatan(true);
    setFReadOnlyKelurahan(true);
    setValue("kota_id", "");
    setValue("kecamatan_id", "");
    setValue("kelurahan_id", "");

    const resFListKota = await GeneralService.kota(e.target.value);
    if (resFListKota.data.success) {
      setFListKota(_.sortBy(resFListKota.data.result, "name"));
      setFReadOnlyKota(false);
    }
  };

  const onChangeKota = async (e) => {
    setFListKecamatan([]);
    setFListKelurahan([]);
    setFReadOnlyKecamatan(true);
    setFReadOnlyKelurahan(true);
    setValue("kecamatan_id", "");
    setValue("kelurahan_id", "");

    const resFListKecamatan = await GeneralService.kecamatan(e.target.value);
    if (resFListKecamatan.data.success) {
      setFListKecamatan(_.sortBy(resFListKecamatan.data.result, "name"));
      setFReadOnlyKecamatan(false);
    }
  };

  const onChangeKecamatan = async (e) => {
    setFListKelurahan([]);
    setFReadOnlyKelurahan(true);
    // setValue("kecamatan_id", "")
    setValue("kelurahan_id", "");

    const resFListKelurahan = await GeneralService.kelurahan(e.target.value);
    if (resFListKelurahan.data.success) {
      setFListKelurahan(_.sortBy(resFListKelurahan.data.result, "name"));
      setFReadOnlyKelurahan(false);
    }
  };

  const onChangeSetujuDataBenar = async (e) => {
    if (e.target.checked) {
      setStatusButtonSubmit(true);
    } else {
      setStatusButtonSubmit(false);
    }
  };

  /** Validation Form Wizard */
  const schemaValidation = Yup.object({
    nama: Yup.string().required(
      ValidationMessage.required.replace("__field", "Nama Lengkap"),
    ),
    nik: Yup.number()
      .typeError(ValidationMessage.required.replace("__field", "NIK"))
      .required(ValidationMessage.required.replace("__field", "NIK"))
      .test(
        "nik",
        ValidationMessage.fixLength
          .replace("__field", "Nomor KTP")
          .replace("__length", 16),
        (val) => String(val).length === 16,
      )
      .test(
        "nik",
        ValidationMessage.invalid.replace("__field", "NIK"),
        (val) => !String(val).match(/^(\d)\1{6}/),
      ),
    // email: Yup.string()
    //   .required(ValidationMessage.required.replace("__field", "Email"))
    //   .email(ValidationMessage.invalid.replace("__field", "Email")),
    // tempat_lahir: Yup.string()
    //   .required(ValidationMessage.required.replace("__field", "Tempat Lahir"))
    //   .label("Tempat Lahir"),
    tanggal_lahir: Yup.string()
      // .matches(
      //   /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/,
      //   "Tanggal Lahir harus mengikuti format DD/MM/YYYY"
      // )
      .required(ValidationMessage.required.replace("__field", "Tanggal Lahir"))
      .test(
        "Valid Date",
        ValidationMessage.validDate.replace("__field", "Tanggal Lahir"),
        (val) => {
          return moment(val, "DD/MM/YYYY", true).isValid();
        },
      )
      .test(
        "maxDate",
        ValidationMessage.maxDate
          .replace("__field", "Tanggal Lahir")
          .replace("__max", moment(maxDate).format("DD MMMM YYYY")),
        (val) => {
          return moment(val, "DD/MM/YYYY").isBefore(maxDate, "day");
        },
      )
      .label("Tanggal Lahir"),
    jenis_kelamin: Yup.string()
      .nullable()
      .oneOf(FListJenisKelamin.value)
      .required(ValidationMessage.required.replace("__field", "Jenis Kelamin"))
      .label("Jenis Kelamin"),
    // jenjang_id: Yup.string()
    //   .nullable()
    //   .required(
    //     ValidationMessage.required.replace("__field", "Pendidikan Terakhir")
    //   )
    //   .label("Pendidikan Terakhir"),
    // address: Yup.string()
    //   .required(ValidationMessage.required.replace("__field", "Alamat Lengkap"))
    //   .label("Alamat Lengkap"),
    provinsi_id: Yup.string()
      .nullable()
      .required(ValidationMessage.required.replace("__field", "Provinsi"))
      .label("Provinsi"),
    kota_id: Yup.string()
      .nullable()
      .required(ValidationMessage.required.replace("__field", "Kota/Kabupaten"))
      .label("Kota/Kabupaten"),
    kecamatan_id: Yup.string()
      .nullable()
      .required(ValidationMessage.required.replace("__field", "Kecamatan"))
      .label("Kecamatan"),
    kelurahan_id: Yup.string()
      .nullable()
      .required(ValidationMessage.required.replace("__field", "Desa/Kelurahan"))
      .label("Desa/Kelurahan"),
    status_pekerjaan_id: Yup.string()
      .nullable()
      .required(
        ValidationMessage.required.replace("__field", "Status Pekerjaan"),
      )
      .label("Status Pekerjaan"),
    pekerjaan_id: Yup.string()
      .when("status_pekerjaan_id", {
        is: (value) => value == "16",
        then: Yup.string()
          .nullable()
          .required(ValidationMessage.required.replace("__field", "Pekerjaan")),
      })
      .nullable()
      .label("Pekerjaan"),
    perusahaan: Yup.string()
      .when("status_pekerjaan_id", {
        is: (value) => value == "16",
        then: Yup.string().required(
          ValidationMessage.required.replace("__field", "Nama Intitusi"),
        ),
      })
      .nullable()
      .label("Nama Intitusi"),
    // nama_kontak_darurat: Yup.string()
    //   .required(
    //     ValidationMessage.required.replace("__field", "Nama Kontak Darurat")
    //   )
    //   .label("Nama Kontak Darurat"),
    // nomor_handphone_darurat: Yup.number()
    //   .typeError(
    //     ValidationMessage.required.replace("__field", "Nomor Handphone Darurat")
    //   )
    //   .required(
    //     ValidationMessage.required.replace("__field", "Nomor Kontak Darurat")
    //   )
    //   .test(
    //     "minLength",
    //     ValidationMessage.minLength
    //       .replace("__field", "Nomor Handphone")
    //       .replace("__length", 7 + FNomorHandphoneDarurat.phone.length),
    //     (val) => String(val).length >= 8
    //   )
    //   .test(
    //     "maxLength",
    //     ValidationMessage.maxLength
    //       .replace("__field", "Nomor Handphone")
    //       .replace("__length", 16 - FNomorHandphoneDarurat.phone.length + 1),
    //     (val) => String(val).length <= 16 - FNomorHandphoneDarurat.phone.length
    //   )
    //   .test(
    //     "different_from_form",
    //     ValidationMessage.different
    //       .replace("__field", "Nomor Handphone Darurat")
    //       .replace("__ref", "Nomor Handphone"),
    //     (val) =>
    //       String(FNomorHandphoneDarurat.phone) + String(val) !=
    //       String(FNomorHandphone.phone) + String(getValues("nomor_handphone"))
    //   )
    //   .label("Nomor Handphone Darurat"),
    nomor_handphone: Yup.number()
      .typeError(
        ValidationMessage.required.replace("__field", "Nomor Handphone"),
      )
      .required(ValidationMessage.required.replace("__field", "Nomor Kontak"))
      .test(
        "minLength",
        ValidationMessage.minLength
          .replace("__field", "Nomor Handphone")
          .replace("__length", 7),
        (val) => String(val).length >= 8,
      )
      .test(
        "maxLength",
        ValidationMessage.maxLength
          .replace("__field", "Nomor Handphone")
          .replace("__length", 16 - FNomorHandphone.phone.length + 1),
        (val) => String(val).length <= 16 - FNomorHandphone.phone.length,
      )
      .test(
        "unique",
        ValidationMessage.unique.replace("__field", "Nomor Handphone"),
        async (val) => {
          if (!phoneChecked) {
            try {
              setPhoneChecked(true);
              const resp = await PortalService.checkNomorHpDuplicate(
                `${FNomorHandphone.phone}${val}`,
              );
              const res = resp.data;
              if (res.success) {
                return (
                  res.result.data.length == 0 || !res.result.data[0].stat_notelp
                );
              } else {
                throw Error("Belum bisa memeriksa nomor hp.");
              }
            } catch (error) {
              console.log(error);
              return false;
            }
          } else return true;
        },
      )
      .label("Nomor Handphone"),
    // hubungan: Yup.string()
    //   .required(ValidationMessage.required.replace("__field", "Hubungan"))
    //   .label("Hubungan"),
    upload_foto: Yup.mixed()
      .test(
        "required",
        ValidationMessage.required.replace("__field", "Foto Profil"),
        (file) => {
          // return file && file.size <-- u can use this if you don't want to allow empty files to be uploaded;
          if (file instanceof File) return true;
          return false;
        },
      )
      .label("Foto Profil"),
  }).required();

  /** Handler Form Wizard */
  const {
    register,
    watch,
    trigger,
    handleSubmit,
    setError,
    getValues,
    setValue,
    formState: { errors },
    reset,
    setFocus,
  } = useForm({
    mode: "onChange",
    defaultValues: FormProfil,
    resolver: yupResolver(schemaValidation),
  });

  const onSubmitWizardFormCallbackError = (err, e) => {
    // console.log(err)
    let idxsError = Object.keys(err);
    if (idxsError.length > 0 && setFocus) {
      NotificationHelper.Failed({ message: "Periksa kembali isian anda" });
      // console.log(idxsError[0])
    }
  };

  const onSubmitWizardForm = async (data) => {
    setLoadingSkeleton(true);
    // toggleCloseKonfirmasiModal.current.click();
    try {
      data.nomor_handphone_darurat = `${FNomorHandphoneDarurat.phone}${data.nomor_handphone_darurat}`;
      data.nomor_handphone = `${FNomorHandphone.phone}${data.nomor_handphone}`;
      // convert jadi form data
      const formData = new FormData();
      formData.append("upload_foto", data.upload_foto);
      formData.append("nama", data.nama);
      formData.append("nik", data.nik);
      formData.append("tanggal_lahir", data.tanggal_lahir);
      formData.append("tempat_lahir", "-");
      formData.append("email", data.email);
      formData.append("nomor_handphone", data.nomor_handphone);
      formData.append("jenis_kelamin", data.jenis_kelamin);
      formData.append("jenjang_id", "");
      formData.append("address", "-");
      formData.append("provinsi_id", data.provinsi_id);
      formData.append("kota_id", data.kota_id);
      formData.append("kecamatan_id", data.kecamatan_id);
      formData.append("kelurahan_id", data.kelurahan_id);
      formData.append("status_pekerjaan_id", data.status_pekerjaan_id);
      if (showPilihanPekerjaan) {
        formData.append("pekerjaan_id", data.pekerjaan_id);
        formData.append("perusahaan", data.perusahaan);
      }
      formData.append("nama_kontak_darurat", "-");
      formData.append("nomor_handphone_darurat", "-");
      formData.append("hubungan", "-");
      formData.append("flag_nik", nikValidate.status ? 1 : 0);
      const resUpdateProfil = await ProfilService.update(formData);
      setLoadingSkeleton(false);
      if (resUpdateProfil.data.success == true) {
        setSaved(true);
        const userData = resUpdateProfil.data.result.data;
        let kodeNegaraHandphone = CountryCode.filter((item) =>
          userData?.nomor_handphone?.startsWith(item.phone),
        );
        if (kodeNegaraHandphone.length > 0) {
          userData.nomor_handphone = userData?.nomor_handphone.slice(
            kodeNegaraHandphone[0].phone.length,
          );
        } else {
          userData.nomor_handphone = userData.nomor_handphone;
        }

        kodeNegaraHandphone = CountryCode.filter((item) =>
          userData?.nomor_handphone_darurat?.startsWith(item.phone),
        );
        if (kodeNegaraHandphone.length > 0) {
          userData.nomor_handphone_darurat =
            userData?.nomor_handphone_darurat.slice(
              kodeNegaraHandphone[0].phone.length,
            );
        } else {
          userData.nomor_handphone_darurat = userData.nomor_handphone_darurat;
        }
        /** Update Cookies */
        PortalHelper.saveUserData(userData);
        /** Update Form Profil */
        setFormProfil(userData);
        /** Get List Akademi */
        getListAkademi();
        /** Loading Turn Off */
        setLoadingSkeleton(false);
        eventBus.dispatch("profil-updated");
        if (
          userData.lengkapi_pendaftaran &&
          userData.lengkapi_pendaftaran.length > 0
        ) {
          NotificationHelper.Success({
            title: "Perhatian",
            message: "Selanjutnya Mohon Lengkapi Form Pendaftaran.",
            onClose: () => {
              router.push(
                `/pelatihan/${userData?.lengkapi_pendaftaran[0]?.pelatian_id}`,
              );
            },
            okText: "Lengkapi Pendaftaran",
          });
        }
      } else if (PortalHelper.getLastViewPelatihanCookie()) {
        router.push(`/pelatihan/${PortalHelper.getLastViewPelatihanCookie()}`);
      } else if (
        resUpdateProfil.data.success == false &&
        resUpdateProfil.data.message == "Validation Error." &&
        Array.isArray(resUpdateProfil.data.result)
      ) {
        const err = resUpdateProfil.data.result;
        const error = {};
        Object.keys(err).forEach((key) => {
          error[key] = err[key].join("\n");
          setError(
            key,
            { type: "focus", message: err[key].join("\n") },
            { shouldFocus: true },
          );
        });
        setFormProfilError(error);
      } else if (resUpdateProfil.data.success == false) {
        const message = resUpdateProfil.data.message;
        setNikValidate(false);
        throw Error(message);
      }
    } catch (err) {
      setLoadingSkeleton(false);
      const message = err.message ? err.message : err.response?.data?.message;
      setServerMessage(
        message ?? "Terjadi kesalahan pada saat melakukan verifikasi.",
      );
      setShowFailModal(true);
    }
  };

  const verifyPhoneNumber = async () => {
    trigger()
      .then((valid) => {
        if (valid) {
          if (
            `${FNomorHandphone.phone}${getValues("nomor_handphone")}` ==
            `${FNomorHandphoneDarurat.phone}${getValues(
              "nomor_handphone_darurat",
            )}`
          ) {
            setError("nomor_handphone_darurat", {
              message:
                "Nomor kontak darurat tidak boleh sama dengan nomor handphone akun",
            });
            setFocus("nomor_handphone_darurat", { shouldSelect: true });
            return;
          }
          showLoadingModal(true);
          return Promise.resolve({
            data: {
              success: true,
            },
          });
        }
        return Promise.reject("validation fail");
      })
      .then((resp) => {
        showLoadingModal(false);

        if (resp.data.success) {
          //   showLoadingModal(false);
          setShowVerifikasiModal(true);
        } else if (resp.data.success == false) {
          const error = resp.data.result;
          setFormProfilError(error);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(async () => {
    try {
      setLoadingSkeleton(true);
      if (router.isReady) {
        const resProfil = await ProfilService.profil();
        setCropper(CropperHelper(onCropComplete));
        let dataUser = null;
        if (resProfil.data.success) {
          dataUser = resProfil.data.result.data;
          Object.keys(dataUser).map((key, index) => {
            if (dataUser[key] == null) {
              dataUser[key] = "";
            }
          });
          // console.log(dataUser)
          // dataUser = _.pick(dataUser,["email","id"])
          // Object.keys(dataUser).forEach((k) => {
          //   dataUser[k] = !["email", ""].includes(k) ? dataUser[k] : ""
          // })

          if (dataUser.nomor_handphone_darurat) {
            let idCountry = CountryCode.filter(({ phone }) => {
              return dataUser.nomor_handphone_darurat?.startsWith(phone);
            });
            if (idCountry.length > 0) {
              dataUser.nomor_handphone_darurat =
                dataUser.nomor_handphone_darurat.substring(
                  idCountry[0].phone.length,
                );
            }
            setNomorHandphoneDarurat(dataUser.nomor_handphone_darurat);
          }
          if (dataUser.nomor_handphone) {
            let idCountry = CountryCode.filter(({ phone }) => {
              return dataUser.nomor_handphone?.startsWith(phone);
            });
            if (idCountry.length > 0) {
              dataUser.nomor_handphone = dataUser.nomor_handphone.substring(
                idCountry[0].phone.length,
              );
            }
            setNomorHandphone(dataUser.nomor_handphone);
          }
        }
        if (dataUser["tanggal_lahir"] == null) {
          dataUser = { ...dataUser, tanggal_lahir: "" };
          setTanggalLahir("");
        } else {
          setTanggalLahir(dataUser.tanggal_lahir);
        }
        setNikValidate({ status: dataUser["flag_nik"] == 1, message: "" });
        setIsNikEligible(dataUser["flag_nik"] == 1);
        setFormProfil(dataUser);

        setinputNIK(dataUser.nik?.length ?? 0);
        /** IF Wizard is Done */
        if (resProfil.data.result.data.wizard == 1) {
          getListAkademi();
        }

        /** Form */
        // const reqFListPendidikan = GeneralService.pendidikan();
        const reqFListProvinsi = GeneralService.provinsi();
        const reqFListStatusPekerjaan = GeneralService.statusPekerjaan();
        const reqFListBidangPekerjaan = GeneralService.bidangPekerjaan();

        const response = await Promise.all([
          // reqFListPendidikan,
          reqFListProvinsi,
          reqFListStatusPekerjaan,
          reqFListBidangPekerjaan,
        ]);
        // console.log(response)
        const [
          resFListProvinsi,
          resFListStatusPekerjaan,
          resFListBidangPekerjaan,
        ] = response;

        if (resProfil.data.result.data.provinsi_id) {
          const resFListKota = await GeneralService.kota(
            resProfil.data.result.data.provinsi_id,
          );
          setFListKota(resFListKota.data.result);
          setFReadOnlyKota(false);
        }
        if (resProfil.data.result.data.kota_id) {
          const resFListKecamatan = await GeneralService.kecamatan(
            resProfil.data.result.data.kota_id,
          );
          setFListKecamatan(resFListKecamatan.data.result);
          setFReadOnlyKecamatan(false);
        }
        if (resProfil.data.result.data.kelurahan_id) {
          const resFListKelurahan = await GeneralService.kelurahan(
            resProfil.data.result.data.kecamatan_id,
          );
          setFListKelurahan(resFListKelurahan.data.result);
          setFReadOnlyKelurahan(false);
        }
        // console.log(resFListProvinsi.data.result)
        // setFListPendidikan(resFListPendidikan.data.result);
        setFListProvinsi(_.sortBy(resFListProvinsi.data.result, "name"));
        setFListStatusPekerjaan(resFListStatusPekerjaan.data.result);
        setFListBidangPekerjaan(resFListBidangPekerjaan.data.result);
        // console.log(dataUser)
        reset(dataUser);
        setLoadingSkeleton(false);
      }
    } catch (error) {
      setLoadingSkeleton(false);
      console.error(error);
    }
  }, [router.isReady]);

  const checkNik = async (nik, nama) => {
    try {
      const resp = await PortalService.hitNikCapil(nik, nama);
      setCapilLoading(false);
      if (Object.keys(resp.data).length == 0) {
        throw new CapilError(
          "Service periksa NIK Dukcapil sedang offline, silahkan lanjutkan pengisian data dan lakukan verifikasi NIK kemudian.",
        );
      }
      if (resp.data.content[0].RESPONSE_CODE) {
        if (resp.data.content[0].RESPONSE_CODE == "05") {
          setIsNikEligible(true);
        }
        throw new Error(resp.data.content[0].RESPONSE);
      } else if (resp.data.content[0].NAMA_LGKP.includes("Tidak Sesuai")) {
        throw new Error("Nama dan NIK Tidak Sesuai.");
      }
      setNikValidate({ message: "Validasi NIK Berhasil", status: true });
      setIsNikEligible(true);
      window.document?.getElementById("nik")?.classList.remove("is-invalid");
    } catch (err) {
      setCapilLoading(false);
      //   window.document?.getElementById("nik")?.classList.add("is-invalid");
      setIsNikEligible(false);
      setNikValidate({
        status: false,
        message:
          err?.message ??
          "Terjadi kesalahan saat terhubung dengan server CAPIL",
      });
    }
  };

  class CapilError extends Error {
    constructor(message) {
      super(message); // (1)
      this.name = "CapilError"; // (2)
    }
  }

  const handleHitNikDelay = _.debounce((q) => {
    if (q.nama.length == 0 || errors["nik"] || errors["nama"]) {
      return;
    }
    checkNik(q.nik, q.nama);
  }, 700);

  useEffect(async () => {
    const subscription = watch((value, { name, type }) => {
      if (name == "nik") {
        if (
          value.nama != null &&
          value.nama != "" &&
          value.nik?.length == 16 &&
          !value.nik?.match(/^(\d)\1{6}/)
        ) {
          checkNik(value.nik, value.nama);
        }
      } else if (
        name == "nama" &&
        value.nik?.length == 16 &&
        !value.nik?.match(/^(\d)\1{6}/)
      ) {
        setCapilLoading(true);
        handleHitNikDelay(value);
      } else if (name == "nomor_handphone") {
        setPhoneChanged(true);
        setPhoneChecked(false);
        setNomorHandphone(value.nomor_handphone);
      } else if (name == "nomor_handphone_darurat") {
        setNomorHandphoneDarurat(value.nomor_handphone_darurat);
      }
    });
    return () => subscription.unsubscribe();
  }, [watch]);

  useEffect(() => {
    reset(FormProfil);
  }, [FormProfil]);

  const inputNIKChange = (e) => {
    setinputNIK(e.target.value.length);
  };

  return (
    <div>
      {LoadingSkeleton && (
        <div>
          <header
            className="py-8 mb-6 bg-blue z-index-0"
            style={{ backgroundImage: "none" }}
          >
            <div className="container">
              <Skeleton count={3} />
            </div>
          </header>

          <section className="py-6 mb-11">
            <div className="container py-9">
              <div className="row mb-8">
                <div className="col-9 mx-auto">
                  <div className="d-block rounded mb-6">
                    <div className="pt-5 pb-5">
                      <ul className="wizard-skeleton mt-5 mx-lg-5 mx-xl-5 mx-xxl-5">
                        <li>
                          <Skeleton height={10} width={100}></Skeleton>
                        </li>
                        <li>
                          <Skeleton height={10} width={100}></Skeleton>
                        </li>
                        <li>
                          <Skeleton height={10} width={100}></Skeleton>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <Skeleton height={30} className="mb-5" width={100}></Skeleton>
                  <div className="row">
                    {[...Array(6).keys()].map((elem) => (
                      <div className="col-md-6 mb-5">
                        <div className="form-group">
                          <Skeleton height={20} width={150}></Skeleton>
                          <Skeleton height={40}></Skeleton>
                        </div>
                      </div>
                    ))}
                  </div>
                  <Skeleton
                    height={30}
                    className="mb-5 my-7"
                    width={100}
                  ></Skeleton>
                  <Skeleton height={20} width={150}></Skeleton>
                  <div className="row">
                    {[...Array(4).keys()].map((elem) => (
                      <div className="col-md-6 mb-5">
                        <div className="form-group">
                          <Skeleton height={20} width={150}></Skeleton>
                          <Skeleton height={40}></Skeleton>
                        </div>
                      </div>
                    ))}
                  </div>

                  <Skeleton
                    height={30}
                    className="mb-5 my-7"
                    width={100}
                  ></Skeleton>
                  <Skeleton height={20} width={150}></Skeleton>
                  <Skeleton height={40} className="mb-5"></Skeleton>

                  <Skeleton
                    height={30}
                    className="mb-5 my-7"
                    width={100}
                  ></Skeleton>

                  <Skeleton height={15} className="mb-5" width={500}></Skeleton>
                  <Skeleton height={50} className="mb-5"></Skeleton>
                </div>
              </div>
            </div>
          </section>
        </div>
      )}
      {!LoadingSkeleton && saved && (
        <div>
          <header
            className="py-8 mb-6 bg-blue z-index-0"
            style={{ backgroundImage: "none" }}
          >
            <div className="container">
              <div className="row align-items-center" data-aos="fade-up">
                <h1 className="fw-bolder text-white text-center mb-0">
                  Formulir Data Pokok
                </h1>
                <p className="text-center text-white">
                  Terima kasih telah melengkapi data pokok
                </p>
              </div>
            </div>
          </header>

          <div className="container rounded-5 border-0 border-lg border-xl mb-80">
            <div className="row">
              <div className="d-block rounded p-2">
                <div className="pb-8">
                  <ul className="wizard pt-6 mx-lg-5 mx-xl-5 mx-xxl-5">
                    <li className="passed">Buat Akun</li>
                    <li className="passed">Data Pokok</li>
                    <li className="done">Selesai</li>
                  </ul>
                </div>
              </div>

              <div className="col-lg-5 mb-8">
                <div className="row pt-8 pb-1">
                  {FormProfil.flag_nik == 1 ? (
                    <h3 className="fw-bold text-green text-center mb-5">
                      Selamat, Data anda telah lengkap
                    </h3>
                  ) : (
                    <h5 className="fw-bold text-red text-center mb-5">
                      Data anda telah lengkap, namun NIK belum terverifikasi
                      silahkan lakukan verifikasi ulang saat mendaftar
                      pelatihan.
                    </h5>
                  )}
                </div>
                <div className="text-center">
                  <img
                    src="/assets/img/empty-state/success-data.png"
                    className="img-fluid align-self-center"
                    style={{ width: "500px" }}
                  />
                </div>
              </div>

              <div className="col-lg-7">
                <div className="d-block rounded-5 bg-light mb-6">
                  <div className="pb-5 px-5 px-lg-3 px-xl-5">
                    <div className="pt-8 mb-3 d-flex align-items-center">
                      <div className="mb-4">
                        <h4 className="fw-bold mb-0">
                          Pilih Akademi Pelatihan
                        </h4>
                        <p className="mb-0 font-size-16">
                          Hi Digiers, ayo pilih dan temukan berbagai pelatihan
                          menarik yang sesuai dengan minat dan dirimu.
                        </p>
                      </div>
                    </div>
                    <div className="row row-cols-1 row-cols-md-2 row-cols-lg-2 row-cols-xl-2">
                      {ListAkademi.map((Akademi, idx) => (
                        <div key={idx} className="col-md mb-2">
                          <Link href={`/akademi/${Akademi.slug}`}>
                            <a className="card bg-transparent border rounded-5 icon-category icon-category-sm  p-3 mb-2">
                              <div className="row align-items-center">
                                <div className="col-auto px-3">
                                  <div className="icon-h-p secondary">
                                    <img
                                      src={PortalHelper.urlAkademiIcon(
                                        Akademi.logo,
                                      )}
                                      className="avatar-img avatar-md img-fluid"
                                    />
                                  </div>
                                </div>
                                <div className="col px-0">
                                  <div className="card-body p-0">
                                    <h6 className="text-primary fw-bold mb-n1">
                                      {Akademi.sub_nama}
                                    </h6>
                                    <p className="font-size-11 text-muted mb-0">
                                      {Akademi.nama}
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </a>
                          </Link>
                        </div>
                      ))}
                    </div>
                  </div>
                  <div className="form-row place-order p-5">
                    {/* <Link href="/auth/user-profile" passHref> */}
                    <a
                      href="/auth/user-profile"
                      // onClick={() => {
                      //   router.replace("/auth/user-profile");
                      // }}
                      className="btn rounded-pill font-size-14 btn-outline-blue btn-block cursor-pointer"
                    >
                      LEWATI
                    </a>
                    {/* </Link> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}

      {!LoadingSkeleton && !saved && (
        <div>
          <div
            className="modal fade"
            id="modalKonfirmasi"
            tabIndex="-1"
            role="dialog"
            aria-labelledby="modalKonfirmasiTitle"
            aria-hidden="true"
          >
            <div className="modal-dialog modal-dialog-centered" role="document">
              <div className="modal-content">
                <div className="modal-body">
                  <button
                    type="button"
                    className="close float-end"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                    ref={toggleCloseKonfirmasiModal}
                  >
                    <span aria-hidden="true">×</span>
                  </button>
                  <h2
                    className="fw-bold text-center mb-1"
                    id="modalExampleTitle"
                  >
                    Konfirmasi
                  </h2>
                  <p className="font-size-lg text-center text-muted mb-5">
                    Apakah data yang anda isi telah benar?
                  </p>
                  <div className="text-center">
                    <button
                      type="button"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                      ref={toggleCloseKonfirmasiModal}
                      className="btn rounded-pill font-size-14 btn-outline-blue mt-3 me-3 lift"
                    >
                      BATAL
                    </button>
                    <button
                      type="submit"
                      className="btn rounded-pill font-size-14 btn-blue mt-3 lift"
                      form="formWizard"
                    >
                      YA, LANJUTKAN
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <header
            className="py-8 mb-6 bg-blue z-index-0"
            style={{ backgroundImage: "none" }}
          >
            <div className="container">
              <div className="row align-items-center" data-aos="fade-up">
                <h1 className="fw-bolder text-white text-center mb-0">
                  Formulir Data Pokok
                </h1>
                <p className="text-center text-white">
                  Mohon lengkapi semua isian dengan benar
                </p>
              </div>
            </div>
          </header>

          <section>
            <div className="container pb-80">
              <div className="row mb-8">
                <div className="col-lg-9 mx-auto mb-6 mb-lg-0">
                  <div className="d-block rounded p-2 mb-6">
                    <div className="pt-6 pb-5">
                      <ul className="wizard mt-5 mx-lg-5 mx-xl-5 mx-xxl-5">
                        <li className="passed">Buat Akun</li>
                        <li className="active">Data Pokok</li>
                        <li className="">Selesai</li>
                      </ul>
                    </div>
                  </div>
                  <div className="my-5">
                    <form
                      className="mb-5"
                      id="formWizard"
                      onSubmit={() => {
                        if (
                          getValues("tanggal_lahir") == "" ||
                          getValues("tanggal_lahir") == null
                        ) {
                          if (tanggalLahir != "") {
                            setValue("tanggal_lahir", tanggalLahir);
                          }
                        }
                        handleSubmit(
                          onSubmitWizardForm,
                          onSubmitWizardFormCallbackError,
                        )();
                      }}
                      ref={formUpdateDataRef}
                    >
                      <div className="row">
                        <div className="col-12 d-flex justify-content-center mb-6">
                          <div className="row">
                            <div className="col-12 text-center">
                              <img
                                src={
                                  url ??
                                  "/assets/img/avatars/no-profile-picture-icon.webp"
                                }
                                alt="Foto user"
                                className="avatar-img rounded-circle user"
                                style={{ width: 110 }}
                              />
                            </div>
                            <div className="col-12">
                              <div className="form-group text-center">
                                <input
                                  id="inputFoto"
                                  type="file"
                                  className="form-control form-control-sm rounded-3 d-none"
                                  {...register("upload_foto")}
                                />
                                <span className="text-red">
                                  {errors.upload_foto?.message}
                                </span>
                              </div>
                            </div>
                            <div className="col-12 text-center">
                              <button
                                type="button"
                                className="btn rounded-pill btn-sm btn-info mt-2"
                                onClick={() => {
                                  // atur max file size ke 2000KB
                                  Cropper.setKeteranganForm(
                                    "Foto Formal (Pas Foto)",
                                  );
                                  Cropper.setMaxFileSize(2000 * 1024);
                                  Cropper.show();
                                }}
                              >
                                Upload Foto Formal
                                <span className="text-red">*</span>
                              </button>
                            </div>
                          </div>
                        </div>

                        <div className="col-md-6 mb-5">
                          <div className="form-group">
                            {/* <label>Nama Lengkap</label>
                            <p>{FormProfil.nama}</p> */}
                            <label className="font-size-14 fw-bold">
                              Nama Lengkap (Sesuai KTP)&nbsp;
                              <span className="text-red">*</span>
                            </label>
                            <input
                              type="text"
                              className={
                                FormProfilError["nama"]
                                  ? "form-control form-control-sm is-invalid"
                                  : "form-control form-control-sm"
                              }
                              placeholder="Nama Lengkap"
                              id="name-uppercase"
                              disabled={
                                FormProfil.flag_nik == 1 ||
                                nikValidate?.status == 1
                              }
                              {...register("nama")}
                              {...PortalHelper.addNameValidation()}
                              onChange={(e) => {
                                setValue("nama", e.target.value?.toUpperCase());
                              }}
                            />
                            <span className="text-red">
                              {errors.nama?.message}
                            </span>
                            {FormProfil.flag_nik == 1 && (
                              <span className="text-green">Verified</span>
                            )}
                          </div>
                        </div>
                        <div className="col-md-6 mb-0">
                          <div className="form-group">
                            <div className="row align-items-end">
                              <div className="col-md mb-4 mb-md-0">
                                <label
                                  className="font-size-14 fw-bold"
                                  for="nik"
                                >
                                  NIK <span className="text-red">*</span>
                                  {capilLoading && (
                                    <span
                                      className="icon spinner-border spinner-border-sm"
                                      role="status"
                                      aria-hidden="true"
                                    ></span>
                                  )}
                                </label>
                              </div>
                            </div>
                            <div className="input inner-addon right-addon">
                              <span className="icon">{inputNIK}</span>
                              <input
                                type="numeric"
                                onKeyDown={(e) => {
                                  PortalHelper.inputNumberOnly(e);
                                }}
                                onKeyUpCapture={(e) => {
                                  inputNIKChange(e);
                                  PortalHelper.keyup(e);
                                }}
                                className="form-control form-control-sm"
                                id="nik"
                                placeholder="Masukkan Nomor KTP"
                                name="nik"
                                {...register("nik")}
                                maxLength={16}
                                disabled={
                                  FormProfil.flag_nik == 1 ||
                                  nikValidate?.status == 1
                                }
                              />
                            </div>
                            {nikValidate?.message == "" &&
                              FormProfil.flag_nik == 0 &&
                              !nikValidate.status && (
                                <span className="form-caption font-size-sm text-italic my-2 text-red">
                                  Belum Verifikasi
                                </span>
                              )}
                            {nikValidate?.message == "" &&
                              FormProfil.flag_nik == 1 &&
                              !nikValidate.status && (
                                <span className="form-caption font-size-sm text-italic my-2 text-green">
                                  Verified
                                </span>
                              )}
                            {/* <br /> */}
                            {nikValidate.message && (
                              <>
                                <span
                                  className={
                                    nikValidate.status
                                      ? "text-green"
                                      : "text-red"
                                  }
                                >
                                  {nikValidate?.message}
                                </span>
                                <br />
                              </>
                            )}
                            <span className="text-red">
                              {errors.nik?.message}
                            </span>{" "}
                          </div>
                        </div>
                        <div className="col-12 mb-5">
                          {!nikValidate?.status && (
                            <a
                              href="#"
                              className="btn btn-outline-blue btn-block btn-sm"
                              onClick={async (e) => {
                                const validationResult = await trigger([
                                  "nama",
                                  "nik",
                                ]);
                                if (validationResult) {
                                  checkNik(getValues("nik"), getValues("nama"));
                                }
                              }}
                            >
                              Validasi Data Dukcapil
                            </a>
                          )}
                          {nikValidate?.status == 1 && (
                            <a
                              href="#"
                              className="btn btn-outline-orange btn-block btn-sm"
                              onClick={async (e) => {
                                setNikValidate({});
                              }}
                            >
                              Ubah Nama dan NIK
                            </a>
                          )}
                        </div>
                        <div className="col-md-6 mb-5">
                          <div className="form-group">
                            <div className="row align-items-end">
                              <div className="col-md mb-4 mb-md-0">
                                <label
                                  className="font-size-14 fw-bold"
                                  for="email"
                                >
                                  Email <span className="text-red">*</span>
                                </label>
                              </div>
                            </div>
                            <input
                              type="text"
                              className={`form-control form-control-sm ${
                                errors.email ? "is-invalid" : ""
                              }`}
                              id="email"
                              placeholder=""
                              disabled
                              name="email"
                              {...register("email")}
                            />

                            {FormProfil.email_verifikasi == true ? (
                              <span className="form-caption font-size-sm text-italic my-2 text-green">
                                Terverifikasi
                              </span>
                            ) : (
                              <span className="form-caption font-size-sm text-italic my-2 text-red">
                                Belum Verifikasi
                              </span>
                            )}
                            <br />
                            <span className="text-red">
                              {errors.email?.message}
                            </span>
                          </div>
                        </div>
                        <div className="col-md-6 mb-5">
                          <div className="form-group">
                            <div className="form-group">
                              <div className="d-flex justify-content-between">
                                <label className="font-size-14 fw-bold">
                                  Nomor Handphone&nbsp;
                                  <span className="text-red">*</span>
                                </label>
                                {nomorHandphone.length > 0 && (
                                  <a
                                    href="#"
                                    onClick={(e) => {
                                      e.preventDefault();
                                      setValue("nomor_handphone", "");
                                    }}
                                  >
                                    Clear
                                  </a>
                                )}
                              </div>
                              <div className="input-group">
                                <span
                                  style={{
                                    position: "absolute",
                                    top: 14,
                                    right: 10,
                                    zIndex: 1,
                                    fontSize: 12,
                                  }}
                                >
                                  {FNomorHandphone?.phone?.length +
                                    nomorHandphone.length}
                                </span>
                                <button
                                  type="button"
                                  className="btn btn-outline-secondary btn-sm rounded dropdown-toggle"
                                  data-bs-toggle="dropdown"
                                  aria-haspopup="true"
                                  aria-expanded="false"
                                  style={{ zIndex: 1 }}
                                >
                                  <span style={{ fontWeight: "lighter" }}>
                                    {FNomorHandphone.emoji}{" "}
                                    {FNomorHandphone.phone}
                                  </span>
                                </button>
                                <div
                                  className="dropdown-menu h-200p"
                                  style={{ overflowY: "auto" }}
                                >
                                  {CountryCode &&
                                    CountryCode.map((Code, idx) => (
                                      <a
                                        key={idx}
                                        className="dropdown-item"
                                        style={{ cursor: "pointer" }}
                                        onClick={() => {
                                          setValue("nomor_handphone", "");
                                          setFNomorHandphone(Code);
                                          trigger(["nomor_handphone"]);
                                          setNomorHandphone("");
                                        }}
                                      >
                                        {Code.emoji} {Code.name}{" "}
                                        {`+${Code.phone}`}
                                      </a>
                                    ))}
                                </div>
                                <input
                                  type="numeric"
                                  className={
                                    FormProfilError["nomor_handphone"]
                                      ? "zero-input-focus form-control form-control-sm is-invalid"
                                      : "zero-input-focus form-control form-control-sm"
                                  }
                                  placeholder="Nomor Handphone"
                                  {...register("nomor_handphone")}
                                  maxLength={
                                    15 - FNomorHandphone?.phone?.length
                                  }
                                  onChange={(e) => {
                                    setPhoneChanged(true);
                                    setPhoneChecked(false);
                                    setNomorHandphone(e.target.value);
                                    PortalHelper.phoneNumberPreventZeroOnFirstInput(
                                      e,
                                    );
                                    PortalHelper.keyup(e);
                                    if (FNomorHandphone?.code == "ID") {
                                      e.target.value = e.target.value.replace(
                                        /^62/,
                                        "",
                                      );
                                      setNomorHandphone(e.target.value);
                                    }
                                  }}
                                />
                              </div>
                              <span className="form-caption font-size-sm text-italic my-2">
                                Format penulisan tanpa kode negara, wajib
                                menggunakan Nomor Handphone Aktif/WhatsApp
                              </span>
                              {/* error message no hp di hilangkan */}
                              {/* {!FormProfil.handphone_verifikasi ||
                                phoneChanged ? (
                                <span className="form-caption font-size-sm text-italic my-2 text-red">
                                  Belum Verifikasi
                                </span>
                              ) : (
                                <span className="form-caption font-size-sm text-italic my-2 text-green">
                                  Terverifikasi
                                </span>
                              )}{" "} */}
                              <span className="text-red">
                                {errors.nomor_handphone?.message}
                              </span>
                            </div>
                          </div>
                        </div>
                        <div className="col-md-6 mb-5">
                          <div className="form-group">
                            <label className="font-size-14 fw-bold">
                              Tanggal Lahir (DD/MM/YYYY)&nbsp;
                              <span className="text-red">*</span>
                            </label>
                            <DatePicker
                              selected={
                                tanggalLahir
                                  ? PortalHelper.stringToDate(tanggalLahir)
                                  : null
                              }
                              dateFormat="dd/MM/yyyy"
                              className={
                                errors.tanggal_lahir
                                  ? "form-control form-control-sm is-invalid"
                                  : "form-control form-control-sm"
                              }
                              onChange={(v) => {
                                let d = moment(v);
                                if (d.isValid()) {
                                  let ds = d.format("DD/MM/YYYY");
                                  setValue("tanggal_lahir", ds);
                                  setTanggalLahir(ds);
                                }
                              }}
                              minDate={new Date(1950, 1, 1)}
                              maxDate={maxDate}
                              placeholderText="Tanggal Lahir"
                              showYearDropdown
                              locale={"id-ID"}
                              yearDropdownItemNumber={100}
                              scrollableYearDropdown
                              popperContainer={({ className, children }) => (
                                <div style={{ zIndex: 2000 }}>{children}</div>
                              )}
                            ></DatePicker>
                            <span className="text-red">
                              {errors.tanggal_lahir?.message}
                            </span>
                          </div>
                        </div>
                        <div className="col-md-6 mb-5">
                          <div className="form-group">
                            <label className="font-size-14 fw-bold">
                              Jenis Kelamin&nbsp;
                              <span className="text-red">*</span>
                            </label>
                            <select
                              className={
                                FormProfilError["jenis_kelamin"]
                                  ? "form-select form-select-sm is-invalid"
                                  : "form-select form-select-sm"
                              }
                              {...register("jenis_kelamin")}
                            >
                              <option selected disabled value="">
                                Pilih Jenis Kelamin
                              </option>
                              {FListJenisKelamin.label.map((elem, index) => (
                                <option
                                  key={FListJenisKelamin.value[index]}
                                  value={FListJenisKelamin.value[index]}
                                >
                                  {elem}
                                </option>
                              ))}
                            </select>
                            <span className="text-red">
                              {errors.jenis_kelamin?.message}
                            </span>
                          </div>
                        </div>
                        {/* <div className="col-md-12 mb-5">
                          <div className="form-group">
                            <label className="font-size-14 fw-bold">
                              Pendidikan Terakhir&nbsp;
                              <span className="text-red">*</span>
                            </label>
                            <select
                              className={
                                FormProfilError["jenjang_id"]
                                  ? "form-select form-select-sm is-invalid"
                                  : "form-select form-select-sm"
                              }
                              {...register("jenjang_id")}
                            >
                              <option selected disabled value="">
                                Pilih Pendidikan Terakhir
                              </option>
                              {FListPendidikan.map((Pendidikan) => (
                                <option
                                  key={Pendidikan.id}
                                  value={Pendidikan.id}
                                >
                                  {Pendidikan.nama}
                                </option>
                              ))}
                            </select>
                            <span className="text-red">
                              {errors.jenjang_id?.message}
                            </span>
                          </div>
                        </div> */}
                        <div className="col-md-12 mt-6">
                          <div className="form-row">
                            <h4 className="fw-bolder text-muted mb-3">
                              Domisili
                            </h4>
                          </div>
                        </div>
                        {/* <div className="col-md-12 mb-5">
                          <div className="form-group">
                            <label className="font-size-14 fw-bold">
                              Alamat Lengkap&nbsp;
                              <span className="text-red">*</span>
                            </label>
                            <input
                              type="text"
                              className={
                                FormProfilError["address"]
                                  ? "form-control form-control-sm is-invalid"
                                  : "form-control form-control-sm"
                              }
                              placeholder="Alamat Lengkap"
                              {...register("address")}
                            />
                            <span className="text-red">
                              {errors.address?.message}
                            </span>
                          </div>
                        </div> */}
                        <div className="col-md-6 mb-5">
                          <div className="form-group">
                            <label className="font-size-14 fw-bold">
                              Provinsi&nbsp;<span className="text-red">*</span>
                            </label>
                            <select
                              className={
                                FormProfilError["provinsi_id"]
                                  ? "form-select form-select-sm is-invalid"
                                  : "form-select form-select-sm"
                              }
                              {...register("provinsi_id")}
                              onChange={onChangeProvinsi}
                            >
                              <option selected disabled value="">
                                Pilih Provinsi Domisili
                              </option>
                              {FListProvinsi.map((Provinsi) => (
                                <option key={Provinsi.id} value={Provinsi.id}>
                                  {Provinsi.name}
                                </option>
                              ))}
                            </select>
                            <span className="text-red">
                              {errors.provinsi_id?.message}
                            </span>
                          </div>
                        </div>
                        <div className="col-md-6 mb-5">
                          <div className="form-group">
                            <label className="font-size-14 fw-bold">
                              Kota/Kabupaten&nbsp;
                              <span className="text-red">*</span>
                            </label>
                            <select
                              className={
                                FormProfilError["kota_id"]
                                  ? "form-select form-select-sm is-invalid"
                                  : "form-select form-select-sm"
                              }
                              {...register("kota_id")}
                              onChange={onChangeKota}
                              disabled={FReadOnlyKota}
                            >
                              <option selected disabled value="">
                                Pilih Kota/Kabupaten
                              </option>
                              {FListKota.map((Kota, idx) => (
                                <option key={idx} value={Kota.id}>
                                  {Kota.name}
                                </option>
                              ))}
                            </select>
                            <span className="text-red">
                              {errors.kota_id?.message}
                            </span>
                          </div>
                        </div>
                        <div className="col-md-6 mb-5">
                          <div className="form-group">
                            <label className="font-size-14 fw-bold">
                              Kecamatan&nbsp;<span className="text-red">*</span>
                            </label>
                            <select
                              className={
                                FormProfilError["kecamatan_id"]
                                  ? "form-select form-select-sm is-invalid"
                                  : "form-select form-select-sm"
                              }
                              {...register("kecamatan_id")}
                              onChange={onChangeKecamatan}
                              disabled={FReadOnlyKecamatan}
                            >
                              <option selected disabled value="">
                                Pilih Kecamatan
                              </option>
                              {FListKecamatan.map((Kecamatan, idx) => (
                                <option key={idx} value={Kecamatan.id}>
                                  {Kecamatan.name}
                                </option>
                              ))}
                            </select>
                            <span className="text-red">
                              {errors.kecamatan_id?.message}
                            </span>
                          </div>
                        </div>
                        <div className="col-md-6 mb-5">
                          <div className="form-group">
                            <label className="font-size-14 fw-bold">
                              Desa/Kelurahan&nbsp;
                              <span className="text-red">*</span>
                            </label>
                            <select
                              className={
                                FormProfilError["kelurahan_id"]
                                  ? "form-select form-select-sm is-invalid"
                                  : "form-select form-select-sm"
                              }
                              {...register("kelurahan_id")}
                              disabled={FReadOnlyKelurahan}
                            >
                              <option selected disabled value="">
                                Pilih Desa/Kelurahan
                              </option>
                              {FListKelurahan.map((Kelurahan, idx) => (
                                <option key={idx} value={Kelurahan.id}>
                                  {Kelurahan.name}
                                </option>
                              ))}
                            </select>
                            <span className="text-red">
                              {errors.kelurahan_id?.message}
                            </span>
                          </div>
                        </div>
                        <div className="col-md-12 mt-6">
                          <div className="form-row">
                            <h4 className="fw-bolder text-muted mb-3">
                              Pekerjaan Saat ini
                            </h4>
                          </div>
                        </div>
                        <div className="col-md-12 mb-5">
                          <div className="form-group">
                            <label className="font-size-14 fw-bold">
                              Status Pekerjaan&nbsp;
                              <span className="text-red">*</span>
                            </label>
                            <select
                              className={
                                FormProfilError["status_pekerjaan_id"]
                                  ? "form-select form-select-sm is-invalid"
                                  : "form-select form-select-sm"
                              }
                              {...register("status_pekerjaan_id")}
                              onChange={(e) => {
                                if (e.currentTarget.value == 16) {
                                  setShowPilihanPekerjaan(true);
                                } else {
                                  setShowPilihanPekerjaan(false);
                                }
                              }}
                            >
                              <option selected disabled value="">
                                Pilih Status Pekerjaan
                              </option>
                              {FListStatusPekerjaan.map(
                                (StatusPekerjaan, idx) => (
                                  <option key={idx} value={StatusPekerjaan.id}>
                                    {StatusPekerjaan.nama}
                                  </option>
                                ),
                              )}
                            </select>
                            <span className="text-red">
                              {errors.status_pekerjaan_id?.message}
                            </span>
                          </div>
                        </div>
                        {showPilihanPekerjaan && (
                          <>
                            <div className="col-md-6 mb-5">
                              <div className="form-group">
                                <label className="font-size-14 fw-bold">
                                  Pekerjaan&nbsp;
                                  <span className="text-red">*</span>
                                </label>
                                <select
                                  className={
                                    FormProfilError["pekerjaan_id"]
                                      ? "form-select form-select-sm is-invalid"
                                      : "form-select form-select-sm"
                                  }
                                  {...register("pekerjaan_id")}
                                >
                                  <option selected disabled value="">
                                    Pilih Pekerjaan
                                  </option>
                                  {FListBidangPekerjaan.map(
                                    (BidangPekerjaan, idx) => (
                                      <option
                                        key={idx}
                                        value={BidangPekerjaan.id}
                                      >
                                        {BidangPekerjaan.nama}
                                      </option>
                                    ),
                                  )}
                                </select>
                                <span className="text-red">
                                  {errors.pekerjaan_id?.message}
                                </span>
                              </div>
                            </div>
                            <div className="col-md-6 mb-5">
                              <div className="form-group">
                                <label className="font-size-14 fw-bold">
                                  Institusi Tempat Bekerja&nbsp;
                                  <span className="text-red">*</span>
                                </label>
                                <input
                                  type="text"
                                  className={
                                    FormProfilError["perusahaan"]
                                      ? "form-control form-control-sm is-invalid"
                                      : "form-control form-control-sm"
                                  }
                                  placeholder="Institusi Tempat Bekerja"
                                  {...register("perusahaan")}
                                />
                                <span className="text-red">
                                  {errors.perusahaan?.message}
                                </span>
                              </div>
                            </div>
                          </>
                        )}
                        {/* <div className="col-md-12 mt-6">
                          <div className="form-row">
                            <h4 className="fw-bolder text-muted mb-3">
                              Kontak Darurat
                            </h4>
                          </div>
                        </div>
                        <div className="col-md-4 mb-5">
                          <div className="form-group">
                            <label className="font-size-14 fw-bold">
                              Nama Kontak Darurat&nbsp;
                              <span className="text-red">*</span>
                            </label>
                            <input
                              type="text"
                              className={
                                FormProfilError["nama_kontak_darurat"]
                                  ? "form-control form-control-sm is-invalid"
                                  : "form-control form-control-sm"
                              }
                              placeholder="Nama Kontak Darurat"
                              {...register("nama_kontak_darurat")}
                            />
                            <span className="text-red">
                              {errors.nama_kontak_darurat?.message}
                            </span>
                          </div>
                        </div>
                        <div className="col-md-4 mb-5">
                          <div className="form-group">
                            <div className="d-flex justify-content-between">
                              <label className="font-size-14 fw-bold">
                                Nomor Handphone Darurat&nbsp;
                                <span className="text-red">*</span>
                              </label>
                              {nomorHandphoneDarurat?.length > 0 && (
                                <a
                                  href="#"
                                  onClick={(e) => {
                                    e.preventDefault();
                                    setValue("nomor_handphone_darurat", "");
                                  }}
                                >
                                  Clear
                                </a>
                              )}
                            </div>
                            <div className="input-group">
                              <button
                                type="button"
                                className="btn btn-outline-secondary btn-sm dropdown-toggle"
                                data-bs-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                              >
                                <span style={{ fontWeight: "lighter" }}>
                                  {FNomorHandphoneDarurat.emoji}{" "}
                                  {FNomorHandphoneDarurat.phone}
                                </span>
                              </button>
                              <div
                                className="dropdown-menu h-200p"
                                style={{ overflowY: "auto" }}
                              >
                                {CountryCode &&
                                  CountryCode.map((Code, idx) => (
                                    <a
                                      key={idx}
                                      className="dropdown-item"
                                      style={{ cursor: "pointer" }}
                                      onClick={() => {
                                        setFNomorHandphoneDarurat(Code);
                                        trigger(["nomor_handphone_darurat"]);
                                      }}
                                    >
                                      {Code.emoji} {Code.name}{" "}
                                      {`+${Code.phone}`}
                                    </a>
                                  ))}
                              </div>
                              <span
                                style={{
                                  position: "absolute",
                                  top: 14,
                                  right: 10,
                                  zIndex: 1,
                                  fontSize: 12,
                                }}
                              >
                                {FNomorHandphoneDarurat?.phone?.length +
                                  nomorHandphoneDarurat.length}
                              </span>
                              <input
                                type="numeric"
                                {...PortalHelper.addPhoneNumberValidation()}
                                className={
                                  FormProfilError["nomor_handphone_darurat"]
                                    ? "zero-input-focus form-control form-control-sm is-invalid"
                                    : "zero-input-focus form-control form-control-sm"
                                }
                                placeholder="Nomor Handphone"
                                {...register("nomor_handphone_darurat")}
                                maxLength={
                                  15 - FNomorHandphoneDarurat.phone.length
                                }
                              />
                            </div>

                            <span className="text-red">
                              {errors.nomor_handphone_darurat?.message}
                            </span>
                          </div>
                        </div>
                        <div className="col-md-4 mb-5">
                          <div className="form-group">
                            <label className="font-size-14 fw-bold">
                              Hubungan&nbsp;<span className="text-red">*</span>
                            </label>
                            <select
                              className={
                                FormProfilError["hubungan"]
                                  ? "form-select form-select-sm is-invalid"
                                  : "form-select form-select-sm"
                              }
                              {...register("hubungan")}
                            >
                              <option selected disabled value="">
                                Pilih Hubungan
                              </option>
                              {FListHubunganKontakDarurat.map((Hubungan) => (
                                <option key={Hubungan} value={Hubungan}>
                                  {Hubungan}
                                </option>
                              ))}
                            </select>
                            <span className="text-red">
                              {errors.hubungan?.message}
                            </span>
                          </div>
                        </div> */}
                        <div className="col-md-12 mb-5">
                          <div className="form-group">
                            <div className="form-check">
                              <input
                                className="form-check-input text-gray-800"
                                type="checkbox"
                                id="setujuDataBenar"
                                value={1}
                                onChange={onChangeSetujuDataBenar}
                              />
                              <label
                                className="form-check-label font-size-14 text-gray-800"
                                htmlFor="setujuDataBenar"
                              >
                                Saya menyatakan bahwa seluruh data benar
                              </label>
                            </div>
                          </div>
                        </div>
                        <div className="col-md-12">
                          <div className="form-row">
                            <button
                              type="button"
                              //   data-bs-toggle="modal"
                              //   data-bs-target="#modalKonfirmasi"
                              className={
                                StatusButtonSubmit &&
                                isNikEligible &&
                                !capilLoading
                                  ? "btn btn-blue rounded-pill font-size-14 btn-block"
                                  : "btn btn-blue rounded-pill font-size-14 btn-block disabled"
                              }
                              onClick={() => {
                                // if (
                                //   FormProfil.handphone_verifikasi == true &&
                                //   !phoneChanged
                                // ) {
                                // formUpdateDataRef.current?.dispatchEvent(
                                //   new Event("submit", {
                                //     cancelable: true,
                                //     bubbles: true,
                                //   })
                                // );
                                handleSubmit(
                                  onSubmitWizardForm,
                                  onSubmitWizardFormCallbackError,
                                )();
                                //   return;
                                // } else {
                                //   verifyPhoneNumber();
                                // }
                              }}
                            >
                              SIMPAN
                            </button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      )}
      {/* <VerifikasiHandphone
        show={showVerifikasiModal}
        handleClose={() => {
          showLoadingModal(false);
          setShowVerifikasiModal(false);
        }}
        children={serverMessage}
        onVerify={() => {
          setShowVerifikasiModal(false);
          // console.log(formUpdateDataRef.current)
          // formUpdateDataRef.current?.dispatchEvent(
          //   new Event("submit", {
          //     cancelable: true,
          //     bubbles: true,
          //   })
          // );
          handleSubmit(onSubmitWizardForm)();
          // formUpdateDataRef.current?.submit()
        }}
        onResend={async () => {
          //   resendEmail;
          PortalService.sendPhoneOTP(
            `${FNomorHandphone.phone}${getValues("nomor_handphone")}`
          );
        }}
        phoneNumber={`0${getValues("nomor_handphone")}`}
      /> */}

      <Failed
        show={showFailModal}
        handleClose={() => {
          setShowFailModal(false);
        }}
        children={serverMessage}
      />
    </div>
  );
}

// CompleteUserData

export default CompleteUserData;
