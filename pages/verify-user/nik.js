import react, { useState, useEffect, useRef } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import PortalService from "../../services/PortalService";
import GeneralService from "../../services/GeneralService";
import ProfilService from "../../services/ProfileService";
import CountryCode from "../../data/phone_code.json";

/** Libraries */
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import InputMask from "react-input-mask";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import ValidationMessage from "../../src/validation-message";
import PortalHelper from "../../src/helper/PortalHelper";
import _ from "lodash";
import MasterDataHelper from "../../src/helper/MasterDataHelper";
import VerifikasiHandphone from "../../components/Modal/VerifikasiHandphoneModal";
import Failed from "../../components/Modal/Failed";
import moment from "moment";
import DatePicker from "react-datepicker";
import EncryptionHelper from "../../src/helper/EncryptionHelper";

function Nik({ showLoadingModal }) {
  const router = useRouter();
  const formUpdateDataRef = useRef(null);
  const [LoadingSkeleton, setLoadingSkeleton] = useState(false);
  const toggleCloseKonfirmasiModal = useRef(null);

  const [ListAkademi, setListAkademi] = useState([]);
  const [StatusButtonSubmit, setStatusButtonSubmit] = useState(false);

  const [FListJenisKelamin, setFListJenisKelamin] = useState({
    label: Object.keys(MasterDataHelper.jenisKelamin).map(
      (key) => MasterDataHelper.jenisKelamin[key],
    ),
    value: Object.keys(MasterDataHelper.jenisKelamin).map((key) => key),
  });
  const [FListPendidikan, setFListPendidikan] = useState([]);
  const [FormProfil, setFormProfil] = useState([]);
  const [FormProfilError, setFormProfilError] = useState([]);
  const [capilLoading, setCapilLoading] = useState(false);
  const [inputNIK, setinputNIK] = useState(0);
  const [nikValidate, setNikValidate] = useState({});
  const [serverMessage, setServerMessage] = useState("");
  const [showFailModal, setShowFailModal] = useState("");
  const [tanggalLahir, setTanggalLahir] = useState("");

  const getListAkademi = async () => {
    const resAkademi = await PortalService.akademiList();
    setListAkademi(resAkademi.data.result);
  };

  const maxDate = new Date();
  maxDate.setFullYear(maxDate.getFullYear() - 5); // max tanggal umur 5 tahun

  const onChangeSetujuDataBenar = async (e) => {
    if (e.target.checked) {
      setStatusButtonSubmit(true);
    } else {
      setStatusButtonSubmit(false);
    }
  };

  /** Validation Form Wizard */
  const schemaValidation = Yup.object({
    nama: Yup.string().required(
      ValidationMessage.required.replace("__field", "Nama Lengkap"),
    ),
    nik: Yup.number()
      .typeError(ValidationMessage.required.replace("__field", "NIK"))
      .required(ValidationMessage.required.replace("__field", "NIK"))
      .test(
        "nik",
        ValidationMessage.fixLength
          .replace("__field", "Nomor KTP")
          .replace("__length", 16),
        (val) => String(val).length === 16,
      )
      .test(
        "nik",
        ValidationMessage.invalid.replace("__field", "NIK"),
        (val) => !String(val)?.match(/(\d)(\1){6,}/),
      ),
    tempat_lahir: Yup.string()
      .required(ValidationMessage.required.replace("__field", "Tempat Lahir"))
      .label("Tempat Lahir"),
    tanggal_lahir: Yup.string()
      .required(ValidationMessage.required.replace("__field", "Tanggal Lahir"))
      .test(
        "Valid Date",
        ValidationMessage.validDate.replace("__field", "Tanggal Lahir"),
        (val) => {
          return moment(val, "DD/MM/YYYY", true).isValid();
        },
      )
      .test(
        "maxDate",
        ValidationMessage.maxDate
          .replace("__field", "Tanggal Lahir")
          .replace("__max", moment(maxDate).format("DD MMMM YYYY")),
        (val) => {
          return moment(val, "DD/MM/YYYY").isBefore(maxDate, "day");
        },
      )
      .label("Tanggal Lahir"),
    jenis_kelamin: Yup.string()
      .nullable()
      .oneOf(FListJenisKelamin.value)
      .required(ValidationMessage.required.replace("__field", "Jenis Kelamin"))
      .label("Jenis Kelamin"),
    jenjang_id: Yup.string()
      .nullable()
      .required(
        ValidationMessage.required.replace("__field", "Pendidikan Terakhir"),
      )
      .label("Pendidikan Terakhir"),
  }).required();

  /** Handler Form Wizard */
  const {
    register,
    watch,
    trigger,
    handleSubmit,
    setError,
    setValue,
    getValues,
    formState: { errors },
    reset,
  } = useForm({
    mode: "onBlur",
    defaultValues: FormProfil,
    resolver: yupResolver(schemaValidation),
  });

  const onSubmitWizardForm = async (data) => {
    setLoadingSkeleton(true);
    toggleCloseKonfirmasiModal.current?.click();
    try {
      data["flag_nik"] = nikValidate.status ? 1 : 0;
      const resUpdateProfil = await ProfilService.updateMandatoryData(data);
      setLoadingSkeleton(false);
      if (resUpdateProfil.data.success == true) {
        const userData = resUpdateProfil.data.result.data;

        /** Update Cookies */
        PortalHelper.saveUserData(userData);
        /** Update Form Profil */
        setFormProfil(userData);
        /** Get List Akademi */
        getListAkademi();
        /** Loading Turn Off */
        setLoadingSkeleton(false);

        const nextPage = `/pelatihan/${EncryptionHelper.decrypt(
          router.query.data,
        )}/daftar`;
        if (nextPage) {
          router.push(nextPage);
        } else {
          router.back();
        }
      } else if (
        resUpdateProfil.data.success == false &&
        resUpdateProfil.data.message == "Validation Error."
      ) {
        const err = resUpdateProfil.data.result;
        const error = {};
        Object.keys(err).forEach((key) => {
          error[key] = err[key].join("\n");
          setError(
            key,
            { type: "focus", message: err[key].join("\n") },
            { shouldFocus: true },
          );
        });
        toggleCloseKonfirmasiModal.current.click();
        setFormProfilError(error);
      } else if (resUpdateProfil.data.success == false) {
        const message = resUpdateProfil.data.message;
        throw Error(message);
      }
    } catch (err) {
      setLoadingSkeleton(false);
      toggleCloseKonfirmasiModal.current.click();
      const message = err.message ? err.message : err.response?.data?.message;
      setServerMessage(
        message ?? "Terjadi kesalahan pada saat melakukan verifikasi.",
      );
      setShowFailModal(true);
    }
  };

  useEffect(async () => {
    try {
      setLoadingSkeleton(true);
      if (router.isReady) {
        const resProfil = await ProfilService.profil();
        let dataUser = null;
        if (resProfil.data.success) {
          dataUser = resProfil.data.result.data;
        }
        if (dataUser["tanggal_lahir"] == null) {
          dataUser = { ...dataUser, tanggal_lahir: "" };
          setTanggalLahir("");
        } else {
          setTanggalLahir(dataUser.tanggal_lahir);
        }
        setNikValidate({ status: dataUser["flag_nik"] == 1, message: "" });
        setFormProfil(dataUser);

        setinputNIK(dataUser.nik?.length ?? 0);
        /** IF Wizard is Done */
        if (resProfil.data.result.data.wizard == 1) {
          getListAkademi();
        }

        /** Form */
        const resFListPendidikan = await GeneralService.pendidikan();

        setFListPendidikan(resFListPendidikan.data.result);

        reset(dataUser);
        setLoadingSkeleton(false);
      }
    } catch (error) {
      setLoadingSkeleton(false);
      console.error(error);
    }
  }, [router.isReady]);

  const checkNik = async (nik, nama) => {
    try {
      setCapilLoading(true);
      const resp = await PortalService.hitNikCapil(nik, nama);
      setCapilLoading(false);
      if (Object.keys(resp.data).length == 0) {
        throw new CapilError(
          "Service periksa NIK Dukcapil sedang offline, silahkan lanjutkan pengisian data dan lakukan verifikasi NIK kemudian.",
        );
      }
      if (resp.data.content[0].RESPONSE_CODE) {
        throw new Error(resp.data.content[0].RESPONSE);
      } else if (resp.data.content[0].NAMA_LGKP.includes("Tidak Sesuai")) {
        throw new Error("Nama dan NIK Tidak Sesuai.");
      }
      setNikValidate({ message: "Verifikasi NIK Berhasil", status: true });
      window.document?.getElementById("nik")?.classList.remove("is-invalid");
    } catch (err) {
      setCapilLoading(false);
      //   window.document?.getElementById("nik")?.classList.add("is-invalid");
      setNikValidate({
        status: false,
        message:
          err?.message ??
          "Terjadi kesalahan saat terhubung dengan server CAPIL",
      });
    }
  };

  class CapilError extends Error {
    constructor(message) {
      super(message); // (1)
      this.name = "CapilError"; // (2)
    }
  }

  const handleHitNikDelay = _.debounce((q) => {
    // console.log("nik:", q.nik);
    if (
      q.nama.length == 0 ||
      errors["nik"] ||
      errors["nama"] ||
      q.nik == null ||
      q.nik.length == 0
    ) {
      return;
    }
    checkNik(q.nik, q.nama);
  }, 700);

  useEffect(async () => {
    const subscription = watch((value, { name, type }) => {
      // console.log(value);
      if (value.nama != null && value.nik != null) {
        if (name == "nik") {
          if (
            value.nama != "" &&
            value.nik?.length == 16 &&
            !value.nik?.match(/(\d)(\1){6,}/)
          ) {
            checkNik(value.nik, value.nama);
          }
        } else if (
          name == "nama" &&
          value.nik.length == 16 &&
          !value?.nik?.match(/(\d)(\1){6,}/)
        ) {
          handleHitNikDelay(value);
        }
      }
    });
    return () => subscription.unsubscribe();
  }, [watch]);

  useEffect(() => {
    reset(FormProfil);
  }, [FormProfil]);

  const inputNIKChange = (e) => {
    setinputNIK(e.target.value.length);
  };

  return (
    <div>
      <div
        className="modal fade"
        id="modalKonfirmasi"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="modalKonfirmasiTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <h2 className="fw-bold text-center mb-1" id="modalExampleTitle">
                Konfirmasi
              </h2>
              <p className="font-size-lg text-center text-muted mb-5">
                Apakah data yang anda isi telah benar?
              </p>
              <div className="text-center">
                <button
                  type="button"
                  className="btn btn-sm rounded-pill font-size-14 btn-outline-blue me-3 mt-3"
                  data-bs-dismiss="modal"
                  data-bs-target="#modalKonfirmasi"
                  aria-label="Close"
                  ref={toggleCloseKonfirmasiModal}
                >
                  BATAL
                </button>
                <button
                  type="submit"
                  className="btn btn-sm rounded-pill font-size-14 btn-blue mt-3"
                  form="formWizard"
                >
                  YA, LANJUTKAN
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      {LoadingSkeleton && (
        <div>
          <header
            className="py-8 mb-6 bg-white z-index-0"
            style={{ backgroundImage: "none" }}
          >
            <div className="container">
              <Skeleton count={3} />
            </div>
          </header>

          <section className="mb-11">
            <div className="container ">
              <div className="row mb-8">
                <div className="col-lg-12 mb-6 mb-lg-0 position-relative">
                  <Skeleton height={30} className="mb-5" width={100}></Skeleton>
                  <div className="row">
                    {[...Array(8).keys()].map((elem) => (
                      <div className="col-md-6 mb-5">
                        <div className="form-group">
                          <Skeleton height={20} width={150}></Skeleton>
                          <Skeleton height={40}></Skeleton>
                        </div>
                      </div>
                    ))}
                  </div>
                  <Skeleton
                    height={30}
                    className="mb-5 my-7"
                    width={100}
                  ></Skeleton>
                  <Skeleton height={20} width={150}></Skeleton>
                  <Skeleton height={40} className="mb-5"></Skeleton>
                  <div className="row">
                    {[...Array(4).keys()].map((elem) => (
                      <div className="col-md-6 mb-5">
                        <div className="form-group">
                          <Skeleton height={20} width={150}></Skeleton>
                          <Skeleton height={40}></Skeleton>
                        </div>
                      </div>
                    ))}
                  </div>

                  <Skeleton
                    height={30}
                    className="mb-5 my-7"
                    width={100}
                  ></Skeleton>
                  <Skeleton height={20} width={150}></Skeleton>
                  <Skeleton height={40} className="mb-5"></Skeleton>

                  <Skeleton
                    height={30}
                    className="mb-5 my-7"
                    width={100}
                  ></Skeleton>

                  <div className="row">
                    {[...Array(3).keys()].map((elem) => (
                      <div className="col-md-4 mb-5">
                        <div className="form-group">
                          <Skeleton height={20} width={150}></Skeleton>
                          <Skeleton height={40}></Skeleton>
                        </div>
                      </div>
                    ))}
                  </div>

                  <Skeleton height={15} className="mb-5" width={500}></Skeleton>
                  <Skeleton height={50} className="mb-5"></Skeleton>
                </div>
              </div>
            </div>
          </section>
        </div>
      )}

      {!LoadingSkeleton && (
        <div>
          <header
            className="py-8 mb-6 bg-blue z-index-0"
            style={{ backgroundImage: "none" }}
          >
            <div className="container">
              <div className="row align-items-center" data-aos="fade-up">
                <h1 className="fw-bolder text-white text-center mb-0">
                  Registrasi Akun Baru
                </h1>
                <p className="text-center font-size-16 text-white">
                  Mohon lengkapi seluruh data dengan benar
                </p>
              </div>
            </div>
          </header>

          <section className="pb-80">
            <div className="container ">
              <div className="row mb-8">
                <div className="col-lg-9 mx-auto mb-6 mb-lg-0">
                  <div className="d-block rounded p-2">
                    <div className="pb-8">
                      <ul className="wizard mt-5 mx-lg-5 mx-xl-5 mx-xxl-5">
                        <li className="active">Buat Akun</li>
                        <li className="">Data Pokok</li>
                        <li className="">Selesai</li>
                      </ul>
                    </div>
                  </div>
                  <div className="">
                    <form
                      className="mb-5"
                      id="formWizard"
                      onSubmit={handleSubmit(onSubmitWizardForm)}
                      ref={formUpdateDataRef}
                    >
                      <div className="row">
                        <div className="col-md-6 mb-5">
                          <div className="form-group">
                            {/* <label>Nama Lengkap</label>
                            <p>{FormProfil.nama}</p> */}
                            <label className="fw-semi-bold font-size-14">
                              Nama Lengkap (Sesuai KTP)&nbsp;
                              <span className="text-red">*</span>
                            </label>
                            <input
                              type="text"
                              className={
                                FormProfilError["nama"]
                                  ? "form-control form-control-sm is-invalid"
                                  : "form-control form-control-sm"
                              }
                              placeholder="Nama Lengkap"
                              style={{ textTransform: "uppercase" }}
                              disabled={FormProfil.flag_nik == 1}
                              {...register("nama")}
                            />
                            <span className="text-red">
                              {errors.nama?.message}
                            </span>
                            {FormProfil.flag_nik == 1 && (
                              <span className="text-green">Verified</span>
                            )}
                          </div>
                        </div>
                        <div className="col-md-6 mb-0">
                          <div className="form-group">
                            <div className="row align-items-end">
                              <div className="col-md mb-4 mb-md-0">
                                <label
                                  className="fw-semi-bold font-size-14"
                                  for="nik"
                                >
                                  NIK <span className="text-red">*</span>
                                  {capilLoading && (
                                    <span
                                      className="icon spinner-border spinner-border-sm"
                                      role="status"
                                      aria-hidden="true"
                                    ></span>
                                  )}
                                </label>
                              </div>
                            </div>
                            <div className="input inner-addon right-addon">
                              <span className="icon">{inputNIK}</span>

                              <input
                                type="text"
                                onKeyDown={(e) => {
                                  PortalHelper.inputNumberOnly(e);
                                }}
                                onKeyUpCapture={(e) => {
                                  inputNIKChange(e);
                                  PortalHelper.keyup(e);
                                }}
                                className="form-control form-control-sm"
                                id="nik"
                                placeholder=""
                                name="nik"
                                {...register("nik")}
                                maxLength={16}
                                disabled={FormProfil.flag_nik == 1}
                              />
                            </div>
                            {nikValidate?.message == "" &&
                              FormProfil.flag_nik == 0 &&
                              !nikValidate.status && (
                                <span className="form-caption font-size-sm text-italic my-2 text-red">
                                  Belum Verifikasi
                                </span>
                              )}
                            {nikValidate?.message == "" &&
                              FormProfil.flag_nik == 1 &&
                              !nikValidate.status && (
                                <span className="form-caption font-size-sm text-italic my-2 text-green">
                                  Validated
                                </span>
                              )}
                            {/* <br /> */}
                            <span
                              className={
                                nikValidate.status ? "text-green" : "text-red"
                              }
                            >
                              {nikValidate?.message}
                            </span>
                            <br />
                            <span className="text-red">
                              {errors.nik?.message}
                            </span>{" "}
                          </div>
                        </div>
                        <div className="col-12 mb-5">
                          {!nikValidate?.status && (
                            <a
                              href="#"
                              className="btn btn-outline-blue btn-block btn-sm"
                              onClick={async (e) => {
                                const validationResult = await trigger([
                                  "nama",
                                  "nik",
                                ]);
                                if (validationResult) {
                                  checkNik(getValues("nik"), getValues("nama"));
                                }
                              }}
                            >
                              Validasi Data Dukcapil
                            </a>
                          )}
                        </div>
                        <div className="col-md-6 mb-5">
                          <div className="form-group">
                            <label className="fw-semi-bold font-size-14">
                              Tempat Lahir&nbsp;
                              <span className="text-red">*</span>
                            </label>
                            <input
                              type="text"
                              className={
                                FormProfilError["tempat_lahir"]
                                  ? "form-control form-control-sm is-invalid"
                                  : "form-control form-control-sm"
                              }
                              placeholder="Tempat Lahir"
                              {...register("tempat_lahir")}
                            />
                            <span className="text-red">
                              {errors.tempat_lahir?.message}
                            </span>
                          </div>
                        </div>
                        <div className="col-md-6 mb-5">
                          <div className="form-group">
                            <label className="fw-semi-bold font-size-14">
                              Tanggal Lahir (DD/MM/YYYY)&nbsp;
                              <span className="text-red">*</span>
                            </label>
                            <DatePicker
                              selected={
                                tanggalLahir
                                  ? PortalHelper.stringToDate(tanggalLahir)
                                  : null
                              }
                              dateFormat="dd/MM/yyyy"
                              className={
                                errors.tanggal_lahir
                                  ? "form-control form-control-sm is-invalid"
                                  : "form-control form-control-sm"
                              }
                              onChange={(v) => {
                                let d = moment(v);
                                if (d.isValid()) {
                                  let ds = d.format("DD/MM/YYYY");
                                  // console.log(ds)
                                  setValue("tanggal_lahir", ds);
                                  // console.log("gv", getValues("tanggal_lahir"))
                                  setTanggalLahir(ds);
                                }

                                // const { onChange } = register("tanggal_lahir")
                                // onChange()
                                // setFormProfilByIndex("tanggal_lahir", ds)
                                // console.log(v)
                              }}
                              minDate={new Date(1950, 1, 1)}
                              maxDate={maxDate}
                              placeholderText="Tanggal Lahir"
                              showYearDropdown
                              yearDropdownItemNumber={100}
                              scrollableYearDropdown
                              popperContainer={({ className, children }) => (
                                <div style={{ zIndex: 2000 }}>{children}</div>
                              )}
                            ></DatePicker>
                            {/* <InputMask
                              mask="99/99/9999"
                              className={
                                FormProfilError["tanggal_lahir"]
                                  ? "form-control form-control-sm is-invalid"
                                  : "form-control form-control-sm"
                              }
                              // defaultValue={FormProfil.tanggal_lahir}
                              placeholder="DD/MM/YYYY"
                              {...register("tanggal_lahir")}
                            /> */}
                            <span className="text-red">
                              {errors.tanggal_lahir?.message}
                            </span>
                          </div>
                        </div>
                        <div className="col-md-6 mb-5">
                          <div className="form-group">
                            <label className="fw-semi-bold font-size-14">
                              Jenis Kelamin&nbsp;
                              <span className="text-red">*</span>
                            </label>
                            <select
                              className={
                                FormProfilError["jenis_kelamin"]
                                  ? "form-select form-select-sm is-invalid"
                                  : "form-select form-select-sm"
                              }
                              {...register("jenis_kelamin")}
                            >
                              <option selected disabled value="">
                                Pilih Jenis Kelamin
                              </option>
                              {FListJenisKelamin.label.map((elem, index) => (
                                <option
                                  key={FListJenisKelamin.value[index]}
                                  value={FListJenisKelamin.value[index]}
                                >
                                  {elem}
                                </option>
                              ))}
                            </select>
                            <span className="text-red">
                              {errors.jenis_kelamin?.message}
                            </span>
                          </div>
                        </div>
                        <div className="col-md-6 mb-5">
                          <div className="form-group">
                            <label className="fw-semi-bold font-size-14">
                              Pendidikan Terakhir&nbsp;
                              <span className="text-red">*</span>
                            </label>
                            <select
                              className={
                                FormProfilError["jenjang_id"]
                                  ? "form-select form-select-sm is-invalid"
                                  : "form-select form-select-sm"
                              }
                              {...register("jenjang_id")}
                            >
                              <option selected disabled value="">
                                Pilih Pendidikan Terakhir
                              </option>
                              {FListPendidikan.map((Pendidikan) => (
                                <option
                                  key={Pendidikan.id}
                                  value={Pendidikan.id}
                                >
                                  {Pendidikan.nama}
                                </option>
                              ))}
                            </select>
                            <span className="text-red">
                              {errors.jenjang_id?.message}
                            </span>
                          </div>
                        </div>
                        <div className="col-md-12 mb-5">
                          <div className="form-group">
                            <div className="form-check">
                              <input
                                className="form-check-input text-gray-800"
                                type="checkbox"
                                id="setujuDataBenar"
                                value={1}
                                onChange={onChangeSetujuDataBenar}
                              />
                              <label
                                className="form-check-label text-muted font-size-14"
                                htmlFor="setujuDataBenar"
                              >
                                Saya menyatakan bahwa seluruh data benar
                              </label>
                            </div>
                          </div>
                        </div>
                        <div className="col-md-12">
                          <div className="form-row">
                            <button
                              type="button"
                              data-bs-toggle="modal"
                              data-bs-target="#modalKonfirmasi"
                              className={
                                StatusButtonSubmit &&
                                nikValidate.status &&
                                !capilLoading
                                  ? "btn btn-blue rounded-pill font-size-14 btn-block"
                                  : "btn btn-blue rounded-pill font-size-14 btn-block disabled"
                              }
                            >
                              SIMPAN
                            </button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      )}

      <Failed
        show={showFailModal}
        handleClose={() => {
          setShowFailModal(false);
        }}
        children={serverMessage}
      />
    </div>
  );
}
export default Nik;
