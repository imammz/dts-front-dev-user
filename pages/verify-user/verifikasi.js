// wizard resgitrasi
import React, { useEffect, useRef, useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import Link from "next/link";
import { useRouter } from "next/router";
import PortalService from "../../services/PortalService";
import ValidationMessage from "../../src/validation-message";
import BeatLoader from "react-spinners/BeatLoader";
import PortalHelper from "../../src/helper/PortalHelper";
import Failed from "../../components/Modal/Failed";
import Success from "../../components/Modal/Success";
import moment from "moment";

function Verifikasi({ showLoadingModal }) {
  const [loading, setLoading] = useState(false);
  const submitButton = useRef(null);
  const [showFailModal, setShowFailModal] = useState(false);
  const [showSuccessModal, setShowSuccessModal] = useState(false);
  const [serverMessage, setServerMessage] = useState("");

  const validationSchema = Yup.object({
    "digit-1": Yup.string().required(ValidationMessage.otp),
    "digit-2": Yup.string().required(ValidationMessage.otp),
    "digit-3": Yup.string().required(ValidationMessage.otp),
    "digit-4": Yup.string().required(ValidationMessage.otp),
    "digit-5": Yup.string().required(ValidationMessage.otp),
    "digit-6": Yup.string().required(ValidationMessage.otp),
  });
  const router = useRouter();

  const pinResend = async () => {
    try {
      showLoadingModal(true);
      const resp = await PortalService.pinResend();
      if (resp.data.success) {
        setServerMessage(resp.data.message);
        setShowSuccessModal(true);
        showLoadingModal(false);
      } else {
        throw Error(resp.data.message);
      }
    } catch (err) {
      console.log(err);
      const message = err.response?.data?.result?.Message
        ? err.response.data.result.Message
        : err.message;
      showLoadingModal(false);
      setServerMessage(
        message || "Terjadi kesalahan saat mencoba terhubung dengan server!",
      );
      setShowFailModal(true);
    }
  };

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    mode: "onBlur",
    resolver: yupResolver(validationSchema),
  });
  const closeSuccessModal = () => {
    setShowSuccessModal(false);
    reset(
      {
        "digit-1": "",
        "digit-2": "",
        "digit-3": "",
        "digit-4": "",
        "digit-5": "",
        "digit-6": "",
      },
      {
        keepErrors: false,
        keepDirty: false,
        keepIsSubmitted: false,
        keepTouched: false,
        keepIsValid: false,
        keepSubmitCount: false,
      },
    );
  };

  const closeFailModal = () => {
    setShowFailModal(false);
    reset(
      {
        "digit-1": "",
        "digit-2": "",
        "digit-3": "",
        "digit-4": "",
        "digit-5": "",
        "digit-6": "",
      },
      {
        keepErrors: false,
        keepDirty: false,
        keepIsSubmitted: false,
        keepTouched: false,
        keepIsValid: false,
        keepSubmitCount: false,
      },
    );
  };

  const changeElementfocus = (idElement, e) => {
    if (e.code.includes("Digit") || e.code.includes("Key")) {
      document.getElementById(idElement).focus();
    }
    return;
  };

  useEffect(() => {
    showLoadingModal(false);
  }, []);

  async function onSubmit(data, event) {
    showLoadingModal(true);
    let pin = "";
    Object.keys(data).forEach((key) => {
      pin += data[key];
      delete data[key];
    });
    const userData = PortalHelper.getUserData();
    // userdata == null -> lupa password
    data["email"] = userData?.email;
    data["pin"] = pin;
    try {
      const res = await PortalService.emailVerif(data);
      showLoadingModal(false);

      if (res.data?.success) {
        PortalHelper.updateUserData(
          "verifikasi_otp",
          res.data.result.data.verifikasi_otp ?? null,
        );
        // PortalHelper.updateUserData('handphone_verifikasi', res.data.result.data.handphone_verifikasi ?? false);

        router.replace({
          pathname: "/auth/verified",
        });
      } else {
        if (res.data?.result) {
          throw Error(PortalHelper.handleErrorResponse(res.data.result));
        } else {
          throw Error(res.data.message);
        }
      }
      setLoading(false);
    } catch (err) {
      showLoadingModal(false);
      const message = err.response?.data?.result?.Message
        ? err.response.data.result.Message
        : err.message;
      showLoadingModal(false);
      setServerMessage(
        message || "Terjadi kesalahan saat mencoba terhubung dengan server!",
      );
      setShowFailModal(true);
    }
  }

  return (
    <>
      <div className="container my-11">
        <div className="row">
          <div className="col-lg-5 col-xl-6 mb-6 mb-lg-0">
            <img
              src="/assets/img/illustrations/file-mail.png"
              alt="..."
              className="img-fluid"
            />
          </div>

          <div className="col-lg-7 col-xl-6">
            <div className="d-block rounded bg-light mb-6">
              <form
                onSubmit={handleSubmit(onSubmit)}
                className="digit-group"
                data-group-name="digits"
                autoComplete="off"
              >
                <div className="pt-6 pb-5 px-5 px-lg-3 px-xl-5">
                  <div className="d-flex align-items-center">
                    <div className="mb-4">
                      <h4 className="mb-4">Kode Verifikasi</h4>
                      <p className="mb-0 text-capitalize">
                        Hi Digiers, Silahkan masukkan kode verifikasi yang
                        dikirim ke Email/Inbox mu. Jangan bagikan kode
                        verifikasi kamu kepada siapapun.
                      </p>
                    </div>
                  </div>
                  <div id="otp" className="my-3">
                    <div
                      className="digit-group"
                      data-group-name="digits"
                      autoComplete="off"
                    >
                      <input
                        type="text"
                        onKeyUp={(e) => {
                          changeElementfocus("digit-2", e);
                        }}
                        className="otp-input text-center m-1"
                        id="digit-1"
                        name="digit-1"
                        {...register("digit-1")}
                        maxLength="1"
                      />
                      <input
                        type="text"
                        onKeyUp={(e) => {
                          changeElementfocus("digit-3", e);
                        }}
                        className="otp-input text-center m-1"
                        id="digit-2"
                        name="digit-2"
                        {...register("digit-2")}
                        maxLength="1"
                      />
                      <input
                        type="text"
                        onKeyUp={(e) => {
                          changeElementfocus("digit-4", e);
                        }}
                        className="otp-input text-center m-1"
                        id="digit-3"
                        name="digit-3"
                        {...register("digit-3")}
                        maxLength="1"
                      />
                      <input
                        type="text"
                        onKeyUp={(e) => {
                          changeElementfocus("digit-5", e);
                        }}
                        className="otp-input text-center m-1"
                        id="digit-4"
                        name="digit-4"
                        {...register("digit-4")}
                        maxLength="1"
                      />
                      <input
                        type="text"
                        onKeyUp={(e) => {
                          changeElementfocus("digit-6", e);
                        }}
                        className="otp-input text-center m-1"
                        id="digit-5"
                        name="digit-5"
                        {...register("digit-5")}
                        maxLength="1"
                      />
                      <input
                        type="text"
                        onKeyUp={() => {
                          if (!loading) {
                            submitButton.current.click();
                          }
                        }}
                        className="otp-input text-center m-1"
                        id="digit-6"
                        name="digit-6"
                        {...register("digit-6")}
                        maxLength="1"
                      />

                      <div className="text-danger">
                        {errors["digit-1"]
                          ? errors["digit-1"].message
                          : errors["digit-2"]
                            ? errors["digit-2"].message
                            : errors["digit-3"]
                              ? errors["digit-3"].message
                              : errors["digit-4"]
                                ? errors["digit-4"].message
                                : errors["digit-5"]
                                  ? errors["digit-5"].message
                                  : errors["digit-6"]
                                    ? errors["digit-6"].message
                                    : ""}
                      </div>

                      <p className="font-12 color-silver mt-3">
                        Tidak mendapatkan Kode Verifikasi?
                        <a
                          href="#"
                          onClick={() => {
                            pinResend();
                          }}
                          className="font-500 color-theme"
                        >
                          &nbsp;Kirim Ulang
                        </a>
                      </p>
                    </div>
                  </div>
                </div>
                <div className="form-row place-order p-5">
                  {!loading && (
                    <button
                      className="btn btn-blue btn-block"
                      type="submit"
                      ref={submitButton}
                    >
                      SUBMIT
                    </button>
                  )}
                  {loading && (
                    <div className="btn btn-block btn-blue">
                      <BeatLoader
                        color="#fff"
                        loading={loading}
                        size={7}
                        margin={2}
                      />
                    </div>
                  )}
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <Failed show={showFailModal} handleClose={closeFailModal}>
        {serverMessage}
      </Failed>
      <Success show={showSuccessModal} handleClose={closeSuccessModal}>
        {serverMessage}
      </Success>
    </>
  );
}

Verifikasi.bodyBackground = "bg-white";

export default Verifikasi;
