export default function Custom404() {
  return (
    <>
      <div className="container my-11">
        <div className="text-center">
          <img
            src="/assets/img/empty-state/404.png"
            className="img-fluid align-self-center mb-5"
            style={{ width: "450px" }}
          />
        </div>
      </div>
    </>
  );
}
