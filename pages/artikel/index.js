import React, { useState, useEffect, useRef } from "react";
import Link from "next/link";
import axios from "axios";
import Moment from "react-moment";
import "moment/locale/id";
import Select from "react-select";
import PortalService from "./../../services/PortalService";

import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
//
import PortalHelper from "../../src/helper/PortalHelper";
import FlipMove from "react-flip-move";
import { useRouter } from "next/router";
import { Typeahead } from "react-bootstrap-typeahead";
import "react-bootstrap-typeahead/css/Typeahead.css";
import Pagination from "../../components/Pagination";

function TidakAdaData({ filter }) {
  return (
    <>
      <div className="row">
        <div className="col-12 mx-auto text-center">
          <h2>Belum Ada Data Artikel</h2>

          {filter && (
            <p className="font-size-lg mb-6">
              Artikel dengan {filter?.label} &nbsp;
              <span className="text-blue font-weight-semi-bold">
                {filter?.value}
              </span>{" "}
              belum tersedia, <br />
              silahkan coba pada &nbsp;
              <span>
                <Link
                  href={{
                    pathname: "/artikel",
                  }}
                >
                  <a>{filter.label} lain.</a>
                </Link>
              </span>
            </p>
          )}
          {!filter && (
            <p className="font-size-lg mb-6">
              Artikel belum tersedia, temukan hal menarik lain di &nbsp;
              <span className="text-blue font-weight-semi-bold">
                <Link
                  href={{
                    pathname: "/",
                  }}
                >
                  <a>Halaman utama.</a>
                </Link>
              </span>
            </p>
          )}
          <img
            src="assets/img/empty-state/not-found.png"
            alt="..."
            className="img-fluid align-self-center"
            style={{ width: "500px" }}
          />
        </div>
      </div>
    </>
  );
}

function ListArtikel() {
  const [dataArticles, setdataArticles] = useState(null);
  const [categoryArticles, setCategoryArticles] = useState(null);
  const [loading, setLoading] = useState(false);
  const [length, setLength] = useState(5);
  const [totalArticle, setTotalArticle] = useState(0);
  const [skeletons, setSkeletons] = useState([1, 2, 3, 4]);
  const [recentPost, setRecentPost] = useState(null);
  const [filterLoading, setFilterLoading] = useState(false);
  const [filter, setFilter] = useState(null);
  const [metaData, setMetaData] = useState(null);
  const [search, setSearch] = useState("");
  const [activeIndex, setActiveIndex] = useState(-1);
  const [tags, setTags] = useState(null);
  const router = useRouter();
  const [autoCompleteOptions, setAutoCompleteOptions] = useState(null);
  //   Auto complete
  const typeaheadArtikelListRef = useRef(null);
  const [selected, setSelected] = useState([]);

  const page = router.query.page ? router.query.page : 1;

  const filterArtikelByTags = async (tag) => {
    setActiveIndex(-1);
    // setFilterLoading(true);
    router.replace({
      pathname: "",
      query: {
        ...router.query,
        page: 1,
        keyword: null,
        categoryId: null,
        tags: tag,
      },
    });

    setFilter({
      label: "Tag",
      value: tag,
    });
  };

  const artikelList = async (length, start = 0) => {
    setActiveIndex(-1);
    // setFilterLoading(true);
    router.replace({
      pathname: "",
      query: {
        ...router.query,
        page: 1,
        keyword: null,
        categoryId: null,
        tags: null,
      },
    });
  };

  const filterArticleByCategory = async (category, activeIndex) => {
    setActiveIndex(activeIndex);
    // setFilterLoading(true);
    router.replace({
      pathname: "",
      query: {
        ...router.query,
        page: 1,
        categoryId: category.id,
        keyword: null,
        activeIndex: activeIndex,
      },
    });
    setFilter({
      label: "Kategori",
      value: category.nama,
    });
  };

  const carifullArticle = async (keyword) => {
    setSearch(keyword);
    setActiveIndex(-1);
    // setFilterLoading(true);
    router.replace({
      pathname: "",
      query: {
        ...router.query,
        page: 1,
        keyword: keyword,
        categoryId: null,
      },
    });
    setFilter({
      label: "Pencarian",
      value: keyword,
    });
  };

  const executeloading = (isloading = false) => {
    if (loading && !isloading) {
      setLoading(false);
    }
    if (
      router.query.hasOwnProperty("tags") ||
      router.query.hasOwnProperty("keyword") ||
      router.query.hasOwnProperty("kategori_id")
    ) {
      setFilterLoading(isloading);
    } else {
      setLoading(isloading);
    }
  };

  useEffect(async () => {
    try {
      if (router.isReady) {
        executeloading(true);
        const reqArray = [];
        reqArray.push(PortalService.kategoriList("Artikel"));
        reqArray.push(PortalService.tagsList("Artikel"));
        reqArray.push(PortalService.artikelAllJudul());

        if (Object.keys(router.query).length === 0) {
          reqArray.push(PortalService.artikelList(length, page));
        } else {
          reqArray.push(
            PortalService.artikelList(
              length,
              page,
              router.query.categoryId ? router.query.categoryId : null,
              router.query.tags ? router.query.tags : null,
              router.query.keyword ? router.query.keyword : null,
              null,
            ),
          );
          if (router.query.hasOwnProperty("activeIndex")) {
            setActiveIndex(router.query.activeIndex);
          }
        }
        reqArray.push(PortalService.artikelList(3));

        const resps = await axios.all(reqArray);

        setdataArticles(resps[3].data.result.data);
        setMetaData(resps[3].data.result.meta);

        if (router.query.hasOwnProperty("keyword")) {
          const slctd = resps[2].data.result.Data.filter((elem) => {
            return elem.judul_artikel == router.query.keyword;
          });
          if (slctd.length > 0) {
            setSelected(slctd);
          } else {
            setSelected([{ judul_artikel: router.query.keyword }]);
          }
        }

        setCategoryArticles(resps[0].data.result.Data);
        setTags(resps[1].data.result.Data);
        setAutoCompleteOptions(resps[2].data.result.Data);
        setRecentPost(resps[4].data.result.data);

        let totalCategory = 0;
        resps[0].data.result.Data.forEach((element) => {
          totalCategory += element.jml_data;
        });

        setTotalArticle(totalCategory);

        executeloading(false);
      }
    } catch (err) {
      executeloading(false);
      console.error(err);
    }
  }, [router]);

  return (
    <div>
      <header className="py-6 bg-blue mb-6 z-index-0">
        <div className="container">
          <div className="row align-items-end" data-aos="fade-up">
            {/* <nav aria-label="breadcrumb">
              <ol className="breadcrumb breadcrumb-scroll">
                <li className="breadcrumb-item">
                  <Link href="/">
                    <a className="text-gray-700">Home</a>
                  </Link>
                </li>
                <li className="breadcrumb-item">
                  <Link href="/rilis-media">
                    <a className="text-gray-700">Rilis Media</a>
                  </Link>
                </li>
                <li
                  className="breadcrumb-item text-blue active"
                  aria-current="page"
                >
                  Artikel
                </li>
              </ol>
            </nav>*/}
            <div className="col-md mb-md-0">
              <h3 className="fw-bolder text-white mb-0">Artikel</h3>
              <p className="text-white mb-0">
                Ragam Artikel terkini seputar #JagoanDigital
              </p>
            </div>
          </div>
        </div>
      </header>

      <div className="container z-index-0">
        <div className="row mb-9">
          <div className="col-md-7 pt-lg-6 col-lg-8 col-xl-9">
            {loading &&
              skeletons.map((skeleton) => (
                <div
                  className="row mb-6 align-items-center"
                  key={`skeleton1_${skeleton}`}
                >
                  <div className="col-lg-6 col-xl-5 mb-lg-0 mb-2">
                    <Skeleton className="sk-thumbnail img-fluid rounded-5" />
                  </div>

                  <div className="col-lg-6 col-xl-7">
                    <div className="d-block mb-lg-0 mb-xl-0">
                      <Skeleton className="h-50p" />
                    </div>

                    <div className="d-block mb-lg-0 mb-xl-0">
                      <Skeleton className="h-50p" />
                    </div>

                    <Skeleton className="h-100p" />
                  </div>
                </div>
              ))}

            {!loading && (
              <FlipMove>
                {dataArticles &&
                  dataArticles[0] !== null &&
                  dataArticles.map((article, index) => (
                    <div
                      className="row mb-7 align-items-center"
                      key={article.slug}
                    >
                      <div className="col-lg-6 col-xl-5 mb-3 mb-lg-0">
                        {filterLoading && (
                          <Skeleton className="sk-thumbnail img-fluid rounded-5" />
                        )}
                        {!filterLoading && (
                          <Link href={`/artikel/${[article.slug]}`}>
                            <a className="d-block sk-thumbnail border img-fluid rounded-5">
                              <img
                                src={PortalHelper.urlPublikasiLogo(
                                  article.gambar,
                                )}
                                alt="..."
                                className="rounded img-fluid"
                              />
                            </a>
                          </Link>
                        )}
                      </div>

                      {filterLoading && (
                        <div className="col-lg-6 col-xl-7">
                          <div className="d-block mb-lg-0 mb-xl-0">
                            <Skeleton className="h-50p" />
                          </div>

                          <div className="d-block mb-lg-0 mb-xl-0">
                            <Skeleton className="h-50p" />
                          </div>

                          <Skeleton className="h-100p" />
                        </div>
                      )}

                      {!filterLoading && (
                        <div className="col-lg-6 col-xl-7">
                          <Link href={`/informasi/${[article.slug]}`}>
                            <a className="d-inline-block">
                              <span className="badge badge-blue font-size-sm fw-normal mb-1 rounded-5">
                                Artikel
                              </span>
                            </a>
                          </Link>
                          <Link href={`/artikel/${[article.slug]}`}>
                            <a className="d-block me-xl-12">
                              <h4 className="fw-bolder mb-1">
                                {article.judul_artikel}
                              </h4>
                            </a>
                          </Link>
                          <ul className="nav mx-n3 mb-4 d-flex">
                            <li className="nav-item px-3 mb-md-0">
                              <span className="font-size-sm text-muted">
                                <Moment format="DD MMMM YYYY" locale="id">
                                  {article.tanggal_publish}
                                </Moment>
                              </span>
                            </li>
                            {/*<li className="nav-item px-3 mb-md-0">
                              <span className="font-size-sm text-muted">{article.user_name}</span>
                            </li>*/}
                          </ul>
                          <p className="line-clamp-3 mb-3 mb-xl-3 me-xl-6">
                            {PortalHelper.truncateString(
                              PortalHelper.stripHtml(article.isi_artikel),
                            )}
                          </p>
                        </div>
                      )}
                    </div>
                  ))}

                {dataArticles && (
                  <div className="d-flex justify-content-center align-items-center my-9">
                    <Pagination
                      pageSize={metaData?.last_page}
                      currentPage={page}
                    />
                  </div>
                )}

                {(!dataArticles || !dataArticles[0]) && (
                  <div>
                    {filterLoading &&
                      skeletons.map((skeleton) => (
                        <div
                          className="row mb-6 align-items-center"
                          key={`skeleton1_${skeleton}`}
                        >
                          <div className="col-lg-6 col-xl-5 mb-lg-0 mb-2">
                            <Skeleton className="sk-thumbnail img-fluid rounded-5" />
                          </div>

                          <div className="col-lg-6 col-xl-7">
                            <div className="d-block mb-lg-0 mb-xl-0">
                              <Skeleton className="h-50p" />
                            </div>

                            <div className="d-block mb-lg-0 mb-xl-0">
                              <Skeleton className="h-50p" />
                            </div>

                            <Skeleton className="h-100p" />
                          </div>
                        </div>
                      ))}

                    {!filterLoading && <TidakAdaData filter={filter} />}
                  </div>
                )}
              </FlipMove>
            )}

            {/* Load More ================================================== */}
          </div>
          <div className="col-md-5 pt-lg-6 col-lg-4 col-xl-3 border-left-lg border-left-md border-left-sm">
            <div className="ps-lg-3 ps-sm-0">
              <h4 className="fw-bolder text-muted mb-5 mt-4">Pencarian</h4>
              <div className="border rounded mb-8">
                <div className="input-group">
                  {/* <input className="form-control form-control-sm border-0 pe-0" onKeyDown={(e) => {
                                    if (e.key == 'Enter') {
                                        carifullArticle(search);
                                    }
                                }} type="search" value={search} onInput={(e) => {setSearch(e.target.value)}} placeholder="Search" aria-label="Search" /> */}

                  <Typeahead
                    id="auto-complete-search-artikel"
                    onChange={(select) => {
                      setSelected(select);
                    }}
                    onInputChange={(e) => {
                      if (e.length == 0) {
                        carifullArticle(e);
                      }
                      setSearch(e);
                    }}
                    options={autoCompleteOptions}
                    placeholder="Cari artikel..."
                    labelKey="judul_artikel"
                    emptyLabel="Tidak ada rekomendasi."
                    ref={typeaheadArtikelListRef}
                    minLength={1}
                    onKeyDown={(e) => {
                      if (e.key == "Tab") {
                        return;
                      }
                      if (e.key == "Enter") {
                        const q =
                          selected.length > 0
                            ? selected[0].judul_artikel
                            : e.target.value;
                        carifullArticle(q);
                      }
                    }}
                    selected={selected}
                  />
                  <div className="input-group-append">
                    <button
                      className="btn btn-sm my-2 my-sm-0 text-secondary icon-uxs"
                      type="submit"
                      onClick={() => {
                        const q =
                          selected.length > 0
                            ? selected[0].judul_artikel
                            : search;
                        carifullArticle(q);
                      }}
                    >
                      <svg
                        width="20"
                        height="20"
                        viewBox="0 0 20 20"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M8.80758 0C3.95121 0 0 3.95121 0 8.80758C0 13.6642 3.95121 17.6152 8.80758 17.6152C13.6642 17.6152 17.6152 13.6642 17.6152 8.80758C17.6152 3.95121 13.6642 0 8.80758 0ZM8.80758 15.9892C4.8477 15.9892 1.62602 12.7675 1.62602 8.80762C1.62602 4.84773 4.8477 1.62602 8.80758 1.62602C12.7675 1.62602 15.9891 4.8477 15.9891 8.80758C15.9891 12.7675 12.7675 15.9892 8.80758 15.9892Z"
                          fill="currentColor"
                        />
                        <path
                          d="M19.762 18.6121L15.1007 13.9509C14.7831 13.6332 14.2687 13.6332 13.9511 13.9509C13.6335 14.2682 13.6335 14.7831 13.9511 15.1005L18.6124 19.7617C18.7712 19.9205 18.9791 19.9999 19.1872 19.9999C19.395 19.9999 19.6032 19.9205 19.762 19.7617C20.0796 19.4444 20.0796 18.9295 19.762 18.6121Z"
                          fill="currentColor"
                        />
                      </svg>
                    </button>
                  </div>
                </div>
              </div>

              {loading && (
                <div className="mb-8">
                  <h4 className="fw-bolder text-muted mt-4">Kategori</h4>
                  {skeletons.map((skeleton, index) => (
                    <Skeleton className="my-2" key={`skeleton2_${skeleton}`} />
                  ))}
                </div>
              )}

              {!loading && categoryArticles && categoryArticles[0] !== null && (
                <div className="mb-8">
                  <h4 className="fw-bolder text-muted mt-4">Kategori</h4>
                  <div className="nav flex-column nav-vertical">
                    <div
                      onClick={() => {
                        filterArticleByCategory("", -1);
                      }}
                      className={`nav-link py-2 px-0 ${
                        activeIndex == -1 ? "active" : ""
                      }`}
                      style={{ cursor: "pointer" }}
                    >{`Semua Kategori (${totalArticle})`}</div>
                    {categoryArticles.map((category, index) => (
                      <div
                        onClick={(el) => {
                          filterArticleByCategory(category, index);
                        }}
                        className={`nav-link py-2 px-0 ${
                          activeIndex == index ? "active" : ""
                        } pointer`}
                        style={{ cursor: "pointer" }}
                      >
                        {`${category.nama} (${category.jml_data})`}
                      </div>
                    ))}
                  </div>
                </div>
              )}

              <div className="mb-8">
                <h4 className="fw-bolder text-muted mb-5">Terbaru</h4>
                <ul className="list-unstyled mb-0">
                  {loading &&
                    !recentPost &&
                    skeletons.map((skeleton) => (
                      <li className="media mb-5" key={`skeleton3_${skeleton}`}>
                        <div className="row mt-3">
                          <div className="col-4">
                            <Skeleton
                              style={{ height: "24px", width: "24px" }}
                              circle
                            />
                          </div>
                          <div className="col-6 p-0">
                            <Skeleton className="" />
                            <Skeleton className="font-size-sm mt-0" />
                          </div>
                        </div>
                      </li>
                    ))}

                  {recentPost &&
                    recentPost.map((article, index) => (
                      <li className="media mb-5 d-flex" key={article.slug}>
                        <Link href={`/artikel/${[article.slug]}`}>
                          <a className="mw-70p d-block me-2">
                            <img
                              src={PortalHelper.urlPublikasiLogo(
                                article.gambar,
                              )}
                              alt="..."
                              className="avatar-md rounded-3"
                            />
                          </a>
                        </Link>
                        <div className="media-body flex-shrink-1">
                          <Link href={`/artikel/${[article.slug]}`}>
                            <a className="d-block">
                              <h6 className="fw-bolder line-clamp-2 mb-0">
                                {PortalHelper.truncateString(
                                  article.judul_artikel,
                                  20,
                                )}
                              </h6>
                            </a>
                          </Link>
                          <span className="font-size-11">
                            <Moment format="DD MMMM YYYY" locale="id">
                              {article.tanggal_publish}
                            </Moment>
                          </span>
                        </div>
                      </li>
                    ))}
                </ul>
              </div>

              {loading && (
                <div className="mb-8">
                  {/* Soon tobe */}
                  <h4 className="fw-bolder text-muted mb-5">Tags</h4>

                  {skeletons.map((skeleton) => (
                    <Skeleton
                      className="btn btn-sm btn-light mx-2 mb-2 w-25"
                      inline="true"
                    />
                  ))}
                </div>
              )}

              {!loading && (
                <div className="mb-8">
                  {/* Belum ada filter by tag */}

                  <h4 className="fw-bolder text-muted mb-5">Tags</h4>
                  {/* <button className="btn btn-sm btn-light text-gray-800 px-5 fw-normal me-1 mb-2 d-block" onClick={() => {
                                artikelList(length);
                                setActiveIndex(-1);

                            }}>
                                Tampilkan semua
                            </button> */}
                  {tags &&
                    tags[0] !== null &&
                    tags.map((tag) => (
                      <button
                        className="btn btn-sm btn-outline-gray-800 rounded-pill px-5 me-1 mb-2"
                        onClick={() => {
                          filterArtikelByTags(tag);
                        }}
                      >
                        {decodeURIComponent(tag)}
                      </button>
                    ))}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
ListArtikel.bodyBackground = "bg-white";
export default ListArtikel;
