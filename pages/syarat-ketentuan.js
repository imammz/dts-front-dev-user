import React, { useState, useEffect, useRef } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import axios from "axios";
import Moment from "react-moment";
import "moment/locale/id";
import FlipMove from "react-flip-move";
import PortalService from "../services/PortalService";
import Failed from "../components/Modal/Failed";

//skeleton
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import PortalHelper from "../src/helper/PortalHelper";

function Program() {
  const router = useRouter();
  const [listAkademi, setListAkademi] = useState(null);
  const [program, setProgram] = useState(null);
  const [akademi, setAkademi] = useState(null);
  const [akademiDetailCount, setAkademiDetailCount] = useState(null);
  const [length, setLength] = useState(6);
  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [showFailModal, setShowFailModal] = useState("");

  useEffect(async () => {
    try {
      setLoading(true);
      if (router.isReady) {
        const program = PortalService.generalDataSiteManajamenen();
        const listAkademiRequest = PortalService.akademiList();
        const resps = await axios.all([program, listAkademiRequest]);
        if (router.query.akademi) {
          const akademi = await PortalService.akademiDetail(
            router.query.akademi,
          );
          if (akademi.data.success) {
            setAkademi(akademi.data.result.data);
            setAkademiDetailCount(akademi.data.result.total);
          } else {
            if (res.data?.result) {
              throw Error(PortalHelper.handleErrorResponse(res.data.result));
            } else {
              if (res.data?.result) {
                throw Error(PortalHelper.handleErrorResponse(res.data.result));
              } else {
                throw Error(res.data.message);
              }
            }
          }
          setLoading(false);
        } else {
          // console.log(resps[0].data)
          // setProgram(resps[0].data.result[0].logo_description);
          setProgram({
            id: 1,
            judul_content: "Syarat dan Ketentuan",
            text_content: resps[0].data.result[0].syarat_ketentuan,
          });
        }

        if (resps[1].data.success) {
          if (resps[1].data.result && resps[1].data.result[0] != null) {
            setListAkademi(resps[1].data.result.slice(0, length));
          }

          if (resps[1].data.success) {
            if (resps[1].data.result && resps[1].data.result[0] != null) {
              setListAkademi(resps[1].data.result.slice(0, length));
            }
          } else {
            throw resps[1].data.message;
          }
          setLoading(false);
        }
      }
    } catch (err) {
      console.log(err);
    }
  }, [router]);

  const getAkademiBySlug = async (slug) => {
    try {
      setLoading(true);
      const akademi = await PortalService.akademiDetail(slug);
      if (akademi.data.success) {
        setAkademi(akademi.data.result.data);
        setAkademiDetailCount(akademi.data.result.total);
      } else {
        if (res.data?.result) {
          throw Error(PortalHelper.handleErrorResponse(res.data.result));
        } else {
          throw Error(res.data.message);
        }
      }
      setLoading(false);
    } catch (err) {
      console.log(err);
      const message = err.message;
      setLoading(false);
      setErrorMessage(message);
      setShowFailModal(true);
    }
  };

  return (
    <>
      <header
        className="py-6 bg-blue mb-6 z-index-0"
        style={{ backgroundImage: "none" }}
      >
        <div className="container">
          <div className="row align-items-end" data-aos="fade-up">
            {loading && <Skeleton height={20} />}
            {loading && <Skeleton height={40} />}
            <div className="col-md mb-md-0">
              {!loading && akademi && (
                <div className="col-md mb-md-0">
                  <div className="row align-items-center mb-0 py-3 ps-4">
                    <div className="col-auto px-0">
                      <div className="icon-h-p secondary">
                        <img
                          src={PortalHelper.urlAkademiIcon(akademi.logo)}
                          className="h-60p"
                        />
                      </div>
                    </div>

                    <div className="col px-4">
                      <div className="card-body mb-0 p-0">
                        <h3 className="fw-bolder text-white mb-0">
                          {akademi.sub_nama}
                        </h3>
                        <p className="mb-0 text-white">{akademi.nama}</p>
                      </div>
                    </div>
                  </div>
                </div>
              )}
              {!loading && !akademi && program && (
                <>
                  <div className="row align-items-center mb-0 py-3 ps-4">
                    <div className="col-auto px-0">
                      <div className="icon-h-p secondary">
                        <img
                          className="img-fluid h-60p shadow-light-lg"
                          src={PortalHelper.getUrlLogoDts("white")}
                          alt="DTS"
                        />
                      </div>
                    </div>

                    <div className="col px-4">
                      <div className="card-body mb-0 p-0">
                        <p className="mb-0 text-white line-clamp-1">
                          {program.judul_content}
                          {/* {"Tentang Program"} */}
                        </p>
                        {/* request nambah be ke buat judul ke admin */}
                        <h3 className="fw-bolder text-white mb-0">
                          Digital Talent Scholarship
                        </h3>
                      </div>
                    </div>
                  </div>
                </>
              )}
            </div>
            {akademi && (
              <div className="col-md-auto">
                {loading && <Skeleton height={40} />}
                <div className="d-lg-flex rounded mb-3">
                  <div className="d-lg-flex flex-wrap">
                    <div className="d-lg-flex flex-wrap">
                      <a
                        href={PortalHelper.urlBrosur(akademi.brosur)}
                        target="_blank"
                        className="btn btn-outline-white text-white btn-sm btn-pill mb-1"
                      >
                        <i className="fas fa-file me-2"></i>
                        Download Brosur
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </header>
      <div className="container">
        <div className="row mb-9">
          <div className="col-lg-8 pt-lg-6 position-relative">
            {loading && (
              <div>
                <Skeleton height={40} style={{ marginBottom: "0.5rem" }} />
                <Skeleton
                  height={120}
                  count={3}
                  style={{ marginBottom: "1rem" }}
                />

                <Skeleton height={40} style={{ marginBottom: "0.5rem" }} />
                <div className="row row-cols-lg-2 mb-8">
                  <div className="col-md">
                    <Skeleton count={6} />
                  </div>

                  <div className="col-md">
                    <Skeleton count={6} />
                  </div>
                </div>
                <div className="row g-3" style={{ textAlign: "center" }}>
                  <Skeleton
                    height={70}
                    inline={true}
                    width={240}
                    count={3}
                    className="me-3"
                  />
                </div>
              </div>
            )}
            {!loading && !akademi && program && (
              <div
                className="paragraph-wrapper text-justify"
                dangerouslySetInnerHTML={{ __html: program.text_content }}
              ></div>
            )}
            {!loading && akademi && (
              <>
                <div
                  className="post-body mb-8"
                  dangerouslySetInnerHTML={{ __html: akademi.deskripsi }}
                ></div>
                <div className="row g-3 mt-8">
                  <h3 className="fw-bolder">Eksplor lebih lanjut</h3>
                  <div className="col-12 col-md-6 col-lg-4 col-xl-4 mb-5">
                    <Link href={`/tema/${akademi.slug}`}>
                      <a className="card border rounded-5 icon-category icon-category-sm p-3">
                        <div className="row align-items-center mx-n3">
                          <div className="col-auto px-3">
                            <div
                              className="icon-circle"
                              style={{ color: "#196ECD" }}
                            >
                              <i className="fa fa-tag"></i>
                            </div>
                          </div>
                          <div className="col px-1">
                            <div className="card-body p-0">
                              <h3 className="text-primary fw-bolder mb-n1">
                                {akademiDetailCount.jml_tema}
                              </h3>
                              <p className="text-muted mb-0">Tema Pelatihan</p>
                            </div>
                          </div>
                        </div>
                      </a>
                    </Link>
                  </div>
                  <div className="col-12 col-md-6 col-lg-4 col-xl-4 mb-5">
                    <Link href="/mitra">
                      <a className="card border rounded-5 icon-category icon-category-sm p-3">
                        <div className="row align-items-center mx-n3">
                          <div className="col-auto px-3">
                            <div
                              className="icon-circle"
                              style={{ color: "#196ECD" }}
                            >
                              <i className="fa fa-building"></i>
                            </div>
                          </div>
                          <div className="col px-1">
                            <div className="card-body p-0">
                              <h3 className="text-primary fw-bolder mb-n1">
                                {akademiDetailCount.jml_mitra}
                              </h3>
                              <p className="text-muted mb-0">Mitra Pelatihan</p>
                            </div>
                          </div>
                        </div>
                      </a>
                    </Link>
                  </div>
                  <div className="col-12 col-md-6 col-lg-4 col-xl-4 mb-5">
                    <Link href={`akademi/${akademi.slug}`}>
                      <a className="card border rounded-5 icon-category icon-category-sm p-3">
                        <div className="row align-items-center mx-n3">
                          <div className="col-auto px-3">
                            <div
                              className="icon-circle"
                              style={{ color: "#196ECD" }}
                            >
                              <i className="fa fa-book"></i>
                            </div>
                          </div>

                          <div className="col px-1">
                            <div className="card-body p-0">
                              <h3 className="text-primary fw-bolder mb-n1">
                                {akademiDetailCount.jml_pelatihan}
                              </h3>
                              <p className="text-muted mb-0">Pelatihan</p>
                            </div>
                          </div>
                        </div>
                      </a>
                    </Link>
                  </div>
                </div>
              </>
            )}
          </div>
          <div className="col-lg-4 pt-lg-6  pt-6">
            <div className="d-block rounded mb-6">
              <div className="pb-5 px-xl-5">
                <div className="d-flex align-items-center">
                  <div className="mb-4">
                    <h4 className="fw-bolder mb-4">Akademi Pelatihan</h4>
                  </div>
                </div>
                <div className="row">
                  {loading && (
                    <div>
                      <Skeleton
                        color="white"
                        count={4}
                        height={70}
                        className="col-12 px-2 mb-5"
                      />
                    </div>
                  )}

                  {!loading && (
                    <FlipMove>
                      {listAkademi &&
                        listAkademi[0] != null &&
                        listAkademi.map((akademi, i) => (
                          <div className="col-12 mb-5" key={i}>
                            <a
                              className="card icon-category border rounded-5 icon-category-sm lift"
                              href="#"
                              onClick={(e) => {
                                getAkademiBySlug(akademi.slug);
                              }}
                            >
                              <div className="row align-items-center mx-n3 p-3">
                                <div className="col-auto px-3">
                                  <div className="icon-h-p secondary">
                                    <img
                                      src={PortalHelper.urlAkademiIcon(
                                        akademi.logo,
                                      )}
                                      className="h-60p"
                                    />
                                  </div>
                                </div>

                                <div className="col px-3">
                                  <div className="card-body p-0">
                                    <h6 className="fw-bolder mb-0 line-clamp-1">
                                      {akademi.sub_nama}
                                    </h6>
                                    <p className="mb-0 font-size-12 line-clamp-1">
                                      {akademi.nama}
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </a>
                          </div>
                        ))}
                    </FlipMove>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Failed
        show={showFailModal}
        handleClose={() => {
          setShowFailModal(false);
          if (window !== undefined) {
            window.location.reload();
          }
        }}
        children={errorMessage}
      />
    </>
  );
}

export default Program;
