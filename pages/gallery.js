import React, { useState, useEffect } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import axios from "axios";
import Moment from "react-moment";
import "moment/locale/id";
import PortalService from "../services/PortalService";
import PrevNextPagination from "../components/PrevNextPaging";
import FlipMove from "react-flip-move";
import Lottie, { useLottie } from "lottie-react";
import * as animation from "./../public/assets/json/lottie-button-loading.json";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

function Gallery() {
  const [dataGalleries, setdataGalleries] = useState(null);
  const [loading, setLoading] = useState(false);
  const [loadingLoadMore, setloadingLoadMore] = useState(false);
  const [length, setLength] = useState(9);
  const [totalLength, setTotalLength] = useState(null);
  const [cursor, setCursor] = useState(null);
  const [igToken, setIgToken] = useState(null);
  const [skeletons, setSkeletons] = useState([0, 1, 2, 3, 4, 5, 6, 7, 8]);

  useEffect(async () => {
    try {
      //   setLoading(true);
      if (igToken != null) {
        setloadingLoadMore(true);
        const listGallery = await PortalService.galleryList(igToken, length);
        setdataGalleries(listGallery.data.data);
        setCursor(listGallery.data.paging);

        // setLoading(false);
        setloadingLoadMore(false);
      }
    } catch (err) {
      console.log(err);
    }
  }, [length]);

  //   load lottie resource
  const options = {
    animationData: animation,
    loop: true,
  };
  const lottie = useLottie(options);
  lottie.setSpeed(1);
  let { View } = lottie;

  useEffect(async () => {
    try {
      setLoading(true);
      const setting = await PortalService.gallerySetting("publikasi");
      const listGallery = PortalService.galleryList(
        setting.data.result.Data.value,
        length,
      );
      const galleryCount = PortalService.galleryCount(
        setting.data.result.Data.value,
      );
      const resps = await axios.all([listGallery, galleryCount]);
      setIgToken(setting.data.result.Data.value);
      setdataGalleries(resps[0].data.data);
      setCursor(resps[0].data.paging);
      setTotalLength(resps[1].data.data.length ?? 0);

      setLoading(false);
    } catch (err) {
      console.log(err);
    }
  }, []);

  return (
    <div className="mb-6">
      {/* Header galeri */}
      <header className="py-6 bg-blue mb-6 z-index-0">
        <div className="container">
          <div className="row align-items-end" data-aos="fade-up">
            {/* <nav aria-label="breadcrumb">
              <ol className="breadcrumb breadcrumb-scroll">
                <li className="breadcrumb-item">
                  <Link href="/">
                    <a className="text-gray-700">Home</a>
                  </Link>
                </li>
                <li className="breadcrumb-item">
                  <Link href="/rilis-media">
                    <a className="text-gray-700">Rilis Media</a>
                  </Link>
                </li>
                <li
                  className="breadcrumb-item text-blue active"
                  aria-current="page"
                >
                  <Link href="/galeri">
                    <a className="dropdown-link">Galeri</a>
                  </Link>
                </li>
              </ol>
            </nav> */}
            <div className="col-md mb-md-0">
              <h3 className="fw-bolder text-white mb-0">Galeri</h3>
              <p className="text-white mb-lg-0">
                Momen-momen menarik dalam foto
              </p>
            </div>
          </div>
        </div>
      </header>

      {/* Body galeri */}
      <div
        className={`container mt-8 pb-80 ${
          dataGalleries != null && dataGalleries[0] == null
            ? "justify-content-center"
            : ""
        }`}
      >
        {loading && (
          <div className="row">
            {skeletons.map((id, index) => (
              <div key={{ id }} className="col-md-4 col-xs-12 mb-5">
                <Skeleton className="h-200p w-100" />
                <Skeleton className="mt-3 w-100" />
              </div>
            ))}
          </div>
        )}

        {!loading && dataGalleries != null && dataGalleries[0] == null && (
          <div className="col-lg-4">
            <img
              src="/assets/img/empty-state/no-data.png"
              className="w-100p img-responsive"
            />
            <h2 className="mb-0" style={{ textAlign: "center" }}>
              Tidak ada data!
            </h2>
          </div>
        )}

        {!loading && dataGalleries && (
          <FlipMove className="grid-container pb-8">
            {dataGalleries.map((galeri, index) => (
              <div style={{ display: "block" }} key={galeri.id} className="">
                <a
                  className="card-hover-overlay rounded overflow-hidden d-block position-relative pb-2"
                  href={galeri.media_url}
                  data-fancybox
                  data-height="1110"
                  data-caption={galeri.caption}
                >
                  {galeri.media_type == "VIDEO" && (
                    <>
                      <img
                        src={galeri.thumbnail_url}
                        className="img-fluid w-100 h-210p rounded-5"
                        disabled
                        alt="..."
                      />
                    </>
                  )}
                  {galeri.media_type != "VIDEO" && (
                    <>
                      <img
                        src={galeri.media_url}
                        className="img-fluid rounded-5 w-100 h-210p"
                        disabled
                        alt="..."
                      />
                    </>
                  )}
                  <div className="center position-absolute hover-visible z-index-1">
                    <div className="mb-4 text-white">
                      <a className="btn rounded-circle bg-blue text-white">
                        <i className="fa fa-plus-circle"></i>
                      </a>
                    </div>
                  </div>
                  {/*<div className="card-footer ps-2 pt-4 pb-1">
                    <div className="row align-items-center">
                      <div
                        className="col-auto pl-1 m-0"
                        style={{ paddingRight: 0, paddingLeft: "7px" }}
                      >
                        <i className="fa fa-play-circle text-blue fa-2x"></i>
                      </div>
                      <div className="col">
                        <div className="p-0 m-0">
                          {/* <h6 className="mb-0 line-clamp-1">{galeri.judul_video}</h6> */}
                  {/*<span className="font-size-sm text-gray-00">
                            <Moment format="DD MMMM YYYY" locale="id">
                              {galeri.timestamp}
                            </Moment>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>*/}
                </a>
              </div>
            ))}
          </FlipMove>
        )}

        {/* Load More ================================================== */}
        {!loadingLoadMore && (
          <div className="text-center">
            <a
              className="btn btn-outline-white mw-300p d-flex mx-auto read-more"
              data-bs-toggle="collapse"
              href="#loadcollapseExample"
              onClick={() => {
                if (length >= totalLength) {
                  setLength(9);
                } else {
                  setLength(length + 9);
                }
              }}
            >
              {length < totalLength && (
                <span className="d-inline-flex mx-auto font-size-14 align-items-center more">
                  <i className="fa fa-chevron-down" />
                  <span className="ms-2">MUAT LEBIH BANYAK</span>
                </span>
              )}

              {length >= totalLength && (
                <span className="d-inline-flex mx-auto font-size-14 align-items-center more">
                  <i className="fa fa-chevron-up" />
                  <span className="ms-2">LEBIH SEDIKIT</span>
                </span>
              )}
            </a>
          </div>
        )}

        <div
          className="text-center"
          style={{ display: loadingLoadMore ? "block" : "none" }}
        >
          <a
            className="btn btn-outline-white mw-300p d-flex mx-auto p-0"
            data-bs-toggle="collapse"
            href="#loadcollapseExample"
            role="button"
          >
            <span className="d-inline-flex mx-auto align-items-center more">
              {/* Icon */}
              <span className="ms-2">{View}</span>
            </span>
          </a>
        </div>

        {/* {dataGalleries && cursor?.previous && <div className="btn p-0 left-arrow-btn">
                    <div className='page-link' style={{borderRadius: "50%"}} onClick={() => {
                        handlePageChange(cursor.previous);
                    }}>
                        <i className="fas fa-arrow-left"></i>
                    </div>
                </div>}
                {dataGalleries && cursor?.next && <div className="btn p-0 right-arrow-btn" onClick={() => {
                        handlePageChange(cursor.next);
                    }}>
                    <div className='page-link' style={{borderRadius: "50%"}}>
                        <i className="fas fa-arrow-right"></i>
                    </div>
                </div>} */}

        {/* <div className="d-flex justify-content-center align-items-center mt-6">
                     <PrevNextPagination handlePageChange={handlePageChange} before={cursor?.previous} next={cursor?.next} />
                 </div> */}
      </div>
    </div>
  );
}

Gallery.bodyBackground = "bg-white";

export default Gallery;
