import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import PortalService from "../services/PortalService";
import ListApp from "../components/ListApp";

export default function AppPortal({ showLoadingModal }) {
  // data links disamping
  const [isLoadingGetLinks, setIsLoadingGetLinks] = useState(true);
  const [siteManajemenData, setSiteManajemenData] = useState({
    alamat: "",
    color: [],
    created_at: "",
    created_by: "",
    email: "",
    footer_logo: "",
    header_logo: "",
    koordinat: [],
    link: [],
    logo_description: "",
    notelp: [],
    social_media: [],
  });

  // ambil data link
  useEffect(async () => {
    let settingData = await PortalService.generalDataSiteManajamenen();
    // console.log(settingData)
    if (settingData.data?.success) {
      // console.log(settingData.data.result.Data)
      const {
        alamat,
        color,
        created_at,
        created_by,
        email,
        footer_logo,
        header_logo,
        koordinat,
        link,
        logo_description,
        notelp,
        social_media,
      } = settingData.data.result[0];
      setSiteManajemenData({
        alamat: alamat,
        color: JSON.parse(color),
        created_at: created_at,
        created_by: created_by,
        email: email,
        footer_logo: footer_logo,
        header_logo: header_logo,
        koordinat: koordinat?.split(","),
        logo_description: logo_description,
        notelp: JSON.parse(notelp),
        link: JSON.parse(link),
        social_media: JSON.parse(social_media),
      });
      // console.log(siteManajemenData)
    } else {
      settingData = await PortalService.generalDataSiteManajamenen(true);
      const {
        alamat,
        color,
        created_at,
        created_by,
        email,
        footer_logo,
        header_logo,
        koordinat,
        link,
        logo_description,
        notelp,
        social_media,
      } = settingData.data.result[0];
      setSiteManajemenData({
        alamat: alamat,
        color: JSON.parse(color),
        created_at: created_at,
        created_by: created_by,
        email: email,
        footer_logo: footer_logo,
        header_logo: header_logo,
        koordinat: koordinat?.split(","),
        logo_description: logo_description,
        notelp: JSON.parse(notelp),
        link: JSON.parse(link),
        social_media: JSON.parse(social_media),
      });
    }
    setIsLoadingGetLinks(false);
  }, []);

  return (
    <>
      <section className="bg-white pb-8 pt-md-7">
        <ListApp links={siteManajemenData.link} isLoading={isLoadingGetLinks} />
      </section>
    </>
  );
}
