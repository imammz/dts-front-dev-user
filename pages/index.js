import react, { useState, useEffect } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import Link from "next/link";
import PortalService from "/services/PortalService";
import Configuration from "/config";
import { setCookies, getCookie, removeCookies } from "cookies-next";

/** Libraries */
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
//
import Moment from "react-moment";
import "moment/locale/id";
import PortalHelper from "../src/helper/PortalHelper";
import SideImage from "../components/SideImage";
import Modal from "react-bootstrap/Modal";
import BroadcastModal from "../components/Modal/BroadcastModal";
import React from "react";

function homePage() {
  const router = useRouter();
  const [ListBanner, setListBanner] = useState([]);
  const [ListAkademi, setAkademi] = useState([]);
  const [ListTemaPopular, setListTemaPopuler] = useState([]);
  const [ListRilisMedia, setListMedia] = useState([]);
  const [ListSidebar, setListSidebar] = useState([]);
  const [loading, setLoading] = useState(false);

  // Separate loading states for each content section
  const [loadingBanner, setLoadingBanner] = useState(true);
  const [loadingAkademi, setLoadingAkademi] = useState(true);
  const [loadingTemaPopuler, setLoadingTemaPopuler] = useState(true);
  const [loadingRilisMedia, setLoadingRilisMedia] = useState(true);
  const [loadingSidebar, setLoadingSidebar] = useState(true);

  const [showModalDM, setShowModalDM] = useState(false);
  const [platform, setPlatform] = useState("");

  const onMouseOverLineClamp = async (event) => {
    event.target.classList.remove("line-clamp-1");
  };

  const onMouseLeaveLineClamp = async (event) => {
    event.target.classList.add("line-clamp-1");
  };

  useEffect(() => {
    if (router.isReady) {
      // Fetch Banner Data
      PortalService.sliderList()
        .then((response) => {
          setListBanner(response.data.result);
          setLoadingBanner(false); // Set loading to false once data is received
        })
        .catch((error) => console.error(error));

      // Fetch Akademi Data
      PortalService.akademiList()
        .then((response) => {
          setAkademi(response.data.result);
          setLoadingAkademi(false); // Set loading to false once data is received
        })
        .catch((error) => console.error(error));

      // Fetch Tema Populer Data
      PortalService.temaList("", "Pendaftar", 1, 6)
        .then((response) => {
          setListTemaPopuler(response.data.result.data);
          setLoadingTemaPopuler(false); // Set loading to false once data is received
        })
        .catch((error) => console.error(error));

      // Fetch Rilis Media Data
      PortalService.rilisMediaList(6)
        .then((response) => {
          setListMedia(response.data.result.data);
          setLoadingRilisMedia(false); // Set loading to false once data is received
        })
        .catch((error) => console.error(error));

      // Fetch Sidebar Data
      PortalService.imagetronList()
        .then((response) => {
          setListSidebar([response.data.result[0], response.data.result[1]]);
          setLoadingSidebar(false); // Set loading to false once data is received
        })
        .catch((error) => console.error(error));
    }
  }, [router.isReady]);

  useEffect(() => {
    const isDesktop = window.innerWidth > 992; // Adjust the breakpoint as needed
    const hasModalClosed = localStorage.getItem("modalClosed");

    if (!isDesktop && !hasModalClosed) {
      setShowModalDM(true);
    }
  }, []);

  useEffect(() => {
    const userAgent = navigator.userAgent;
    const isIOSDevice = /iPad|iPhone|iPod/.test(userAgent) && !window.MSStream;
    const isAndroidDevice = /Android/.test(userAgent);

    if (isIOSDevice) {
      setPlatform("iOS");
    } else if (isAndroidDevice) {
      setPlatform("Android");
    } else {
      setPlatform("Other");
    }
  }, []);

  const getPlatformName = () => {
    if (platform === "iOS") {
      return (
        <div className="text-center pt-3">
          <img
            src="/assets/img/digitalent-mobile.png"
            alt="Apple Store"
            className="text-center border rounded-5 p-2"
            style={{ width: "80px" }}
          />
          <h3 className="fw-bold mb-0 mt-3">Digitalent Mobile</h3>
          <p>Download sekarang di App Store</p>
          <div className="row gx-2 mt-6">
            <div className="col-6">
              <a
                href="#"
                className="btn btn-block rounded-3 btn-outline-blue font-size-14 lift"
                onClick={() => {
                  handleCloseModalDM();
                }}
              >
                Nanti Saja
              </a>
            </div>
            <div className="col-6">
              <a
                href="https://apps.apple.com/id/app/digital-talent-scholarship/id1669947124?l=id"
                target="_blank"
                className="btn btn-blue rounded-3 font-size-14 btn-block"
                onClick={() => {
                  handleCloseModalDM();
                }}
              >
                <i className="bi bi-apple"></i> App Store
              </a>
            </div>
          </div>
        </div>
      );
    } else if (platform === "Android") {
      return (
        <div className="text-center pt-3">
          <img
            src="/assets/img/digitalent-mobile.png"
            alt="Apple Store"
            className="text-center border rounded-5 p-2"
            style={{ width: "80px" }}
          />
          <h3 className="fw-bold mb-0 mt-3">Digitalent Mobile</h3>
          <p>Download sekarang di Google Play</p>
          <div className="row gx-2 mt-6">
            <div className="col-6">
              <a
                href="#"
                className="btn btn-block rounded-3 btn-outline-blue font-size-14 lift"
                onClick={() => {
                  handleCloseModalDM();
                }}
              >
                Nanti Saja
              </a>
            </div>
            <div className="col-6">
              <a
                href="https://play.google.com/store/apps/details?id=id.go.kominfo.digitalent"
                target="_blank"
                onClick={() => {
                  handleCloseModalDM();
                }}
                className="btn btn-blue rounded-3 font-size-14 btn-block"
              >
                <i className="bi bi-google-play"></i> Google Play
              </a>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <>
          <div className="text-center pt-3">
            <img
              src="/assets/img/digitalent-mobile.png"
              alt="Apple Store"
              className="text-center border rounded-5 p-2"
              style={{ width: "80px" }}
            />
            <h3 className="fw-bold mb-0 mt-3">Digitalent Mobile</h3>
            <p>Download sekarang di App Store dan Google Play</p>
            <p>
              <a
                href="https://apps.apple.com/id/app/digital-talent-scholarship/id1669947124?l=id"
                target="_blank"
                onClick={() => {
                  handleCloseModalDM();
                }}
              >
                <img
                  src="/assets/img/icon-appstore.webp"
                  alt="App Store"
                  className="img-fluid align-self-center me-3 mb-3"
                  style={{ width: "140px" }}
                />
              </a>
              <a
                href="https://play.google.com/store/apps/details?id=id.go.kominfo.digitalent"
                target="_blank"
                onClick={() => {
                  handleCloseModalDM();
                }}
              >
                <img
                  src="/assets/img/icon-gplay.webp"
                  alt="Google Play"
                  className="img-fluid align-self-center mb-3"
                  style={{ width: "140px" }}
                />
              </a>
            </p>
            <div className="row gx-2 mt-6">
              <div className="col-12">
                <a
                  href="#"
                  className="btn btn-block rounded-3 btn-outline-blue font-size-14 lift"
                  onClick={() => {
                    handleCloseModalDM();
                  }}
                >
                  Nanti Saja
                </a>
              </div>
            </div>
          </div>
        </>
      );
    }
  };

  const handleCloseModalDM = () => {
    localStorage.setItem("modalClosed", "true");
    setShowModalDM(false);
  };

  return (
    <div>
      {/* Banner Section */}
      {loadingBanner ? (
        <section className="half-bg pt-1 pb-5">
          <div className="container  mt-0 pt-0">
            <div className="row gx-2 p-0">
              <div className="col-12 col-lg-12 col-xl-9 col-md-12 mb-2">
                <div id="" className="carousel slide" data-bs-ride="carousel">
                  <Skeleton height={430}></Skeleton>
                </div>
              </div>
              <div className="col-12 col-lg-12 col-md-12 col-xl-3">
                <div className="row gx-2">
                  <div className="col-6 col-lg-6 col-xl-12 mb-2">
                    <Skeleton height={207} />
                  </div>
                  <div className="col-6 col-lg-6 col-xl-12 mb-2">
                    <Skeleton height={207} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      ) : (
        <section className="half-bg pt-1 pb-5">
          <div className="container">
            <div className="rounded" style={{ overflow: "hidden" }}>
              <div className="row gx-0">
                <div className="col-12 col-lg-12 col-xl-9 col-md-12 p-0">
                  <div
                    id="hero"
                    className="carousel slide"
                    data-bs-ride="carousel"
                  >
                    <div class="carousel-inner rounded-0">
                      {ListBanner.map((Banner, index) => (
                        <div
                          className={
                            index == 0
                              ? "carousel-item active"
                              : "carousel-item"
                          }
                        >
                          <a target={"_blank"} href={Banner.link_url ?? "#"}>
                            <img
                              src={PortalHelper.urlPublikasiLogo(Banner.gambar)}
                              alt={Banner.judul_imagetron}
                              className="d-block"
                              style={{ width: "100%" }}
                            />
                          </a>
                        </div>
                      ))}
                    </div>
                    <button
                      class="carousel-control-prev"
                      type="button"
                      data-bs-target="#hero"
                      data-bs-slide="prev"
                    >
                      <span
                        class="carousel-control-prev-icon"
                        aria-hidden="true"
                      ></span>
                      <span class="visually-hidden">Previous</span>
                    </button>
                    <button
                      class="carousel-control-next"
                      type="button"
                      data-bs-target="#hero"
                      data-bs-slide="next"
                    >
                      <span
                        class="carousel-control-next-icon"
                        aria-hidden="true"
                      ></span>
                      <span class="visually-hidden">Next</span>
                    </button>
                  </div>
                </div>
                {ListSidebar.length > 0 && (
                  <div className="col-12 col-lg-12 p-0 col-md-12 col-xl-3">
                    <SideImage
                      loading={loading}
                      className="img-responsive rounded-0"
                    ></SideImage>
                  </div>
                )}
              </div>
            </div>
          </div>
        </section>
      )}

      {/* Akademi Section */}
      {loadingAkademi ? (
        <section className="bg-white py-5 pb-8 pt-md-7">
          <div className="container">
            <div className="row align-items-end mb-md-7 mb-4">
              <div className="col-md mb-4 mb-md-0">
                <h2 className="fw-bolder mb-1">
                  {" "}
                  <Skeleton width={200} height={30}></Skeleton>
                </h2>
                <h5 className="mb-0 text-muted">
                  <Skeleton width={300} height={20}></Skeleton>
                </h5>
              </div>
            </div>

            <div className="row row-cols-2 row-cols-lg-3 row-cols-xl-4">
              <div className="col mb-md-6 mb-4 px-2 px-md-4 ">
                <Skeleton height={207} />
              </div>
              <div className="col mb-md-6 mb-4 px-2 px-md-4 ">
                <Skeleton height={207} />
              </div>
              <div className="col mb-md-6 mb-4 px-2 px-md-4 ">
                <Skeleton height={207} />
              </div>
              <div className="col mb-md-6 mb-4 px-2 px-md-4 ">
                <Skeleton height={207} />
              </div>
              <div className="col mb-md-6 mb-4 px-2 px-md-4 ">
                <Skeleton height={207} />
              </div>
              <div className="col mb-md-6 mb-4 px-2 px-md-4 ">
                <Skeleton height={207} />
              </div>
              <div className="col mb-md-6 mb-4 px-2 px-md-4 ">
                <Skeleton height={207} />
              </div>
              <div className="col mb-md-6 mb-4 px-2 px-md-4 ">
                <Skeleton height={207} />
              </div>
            </div>
          </div>
        </section>
      ) : (
        <section className="bg-white py-5 pb-8 pt-md-7">
          <div className="container">
            <div className="row align-items-end mb-md-7 mb-4">
              <div className="col-md mb-4 mb-md-0">
                <h2 className="fw-bolder mb-1">Pilih Akademi Pelatihan</h2>
                <h5 className="mb-0 text-muted">
                  Hi Digiers, temukan berbagai pelatihan menarik yang sesuai
                  dengan dirimu
                </h5>
              </div>
            </div>

            <div className="row row-cols-2 row-cols-lg-3 row-cols-xl-4">
              {ListAkademi.map((Akademi) => (
                <div className="col mb-md-6 mb-4 px-2 px-md-4 ">
                  <Link href={`/akademi/${Akademi.slug}`}>
                    <a class="card icon-category border-1 rounded-5 p-md-5 p-3 text-center lift">
                      <div class="position-relative text-light">
                        <img
                          src={PortalHelper.urlAkademiIcon(Akademi.logo)}
                          className="h-152"
                        />
                      </div>

                      <div class="card-footer px-0 py-3">
                        <h5 class="mb-0 line-clamp-1"> {Akademi.sub_nama}</h5>
                        <p
                          className="mb-0 font-size-sm fw-500 text-caption"
                          onMouseOver={onMouseOverLineClamp}
                          onMouseLeave={onMouseLeaveLineClamp}
                        >
                          {Akademi.nama}
                        </p>
                      </div>
                    </a>
                  </Link>
                </div>
              ))}
            </div>
          </div>
        </section>
      )}

      {/* Tema Populer Section */}
      {loadingTemaPopuler ? (
        <section className="bg-gray pt-8">
          <div className="container">
            <div className="row align-items-end mb-4 mb-md-7">
              <div className="col-md mb-4 mb-md-0">
                <h2 className="fw-bolder mb-1">
                  <Skeleton width={200} height={30}></Skeleton>
                </h2>
                <h5 className="mb-0 text-muted">
                  <Skeleton width={300} height={20}></Skeleton>
                </h5>
              </div>
              <div className="col-md-auto">
                <Skeleton width={100} height={20}></Skeleton>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-lg-5 col-12 p-0 d-flex mb-0">
                <Skeleton height={207}></Skeleton>
              </div>
              <div className="col-12 col-lg-7 my-8">
                <div className="row">
                  <div className="col-lg-6 mb-5">
                    <Skeleton height={80} />
                  </div>
                  <div className="col-lg-6 mb-5">
                    <Skeleton height={80} />
                  </div>
                  <div className="col-lg-6 mb-5">
                    <Skeleton height={80} />
                  </div>
                  <div className="col-lg-6 mb-5">
                    <Skeleton height={80} />
                  </div>
                  <div className="col-lg-6 mb-5">
                    <Skeleton height={80} />
                  </div>
                  <div className="col-lg-6 mb-5">
                    <Skeleton height={80} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      ) : (
        <section className="bg-gray pt-8">
          <div className="container">
            <div className="row align-items-end mb-4 mb-md-7">
              <div className="col-md mb-4 mb-md-0">
                <h2 className="fw-bolder mb-1">Tema Pelatihan Populer</h2>
                <h5 className="mb-0 text-muted">
                  Hi Digiers, berikut {ListTemaPopular.length} tema populer saat
                  ini
                </h5>
              </div>
              <div className="col-md-auto">
                <Link href="/tema/populer">
                  <a className="d-flex align-items-center fw-medium">
                    Selengkapnya
                    <div className="ms-2 d-flex">
                      <svg
                        width="10"
                        height="10"
                        viewBox="0 0 10 10"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M7.7779 4.6098L3.32777 0.159755C3.22485 0.0567475 3.08745 0 2.94095 0C2.79445 0 2.65705 0.0567475 2.55412 0.159755L2.2264 0.487394C2.01315 0.700889 2.01315 1.04788 2.2264 1.26105L5.96328 4.99793L2.22225 8.73895C2.11933 8.84196 2.0625 8.97928 2.0625 9.1257C2.0625 9.27228 2.11933 9.4096 2.22225 9.51269L2.54998 9.84025C2.65298 9.94325 2.7903 10 2.9368 10C3.0833 10 3.2207 9.94325 3.32363 9.84025L7.7779 5.38614C7.88107 5.2828 7.93774 5.14484 7.93741 4.99817C7.93774 4.85094 7.88107 4.71305 7.7779 4.6098Z"
                          fill="currentColor"
                        />
                      </svg>
                    </div>
                  </a>
                </Link>
              </div>
            </div>
            <div className="row">
              <div className="col-12 col-lg-5 col-12 p-0 d-flex mb-0">
                <img
                  src="assets/img/tema-race.png"
                  className="img-responsive p-0 aos-init aos-animate"
                  style={{ alignSelf: "self-end" }}
                  alt="diploy"
                  data-aos="fade-up"
                />
              </div>
              <div className="col-12 col-lg-7 my-8">
                <div className="row">
                  {ListTemaPopular.map((TemaPopuler) => {
                    if (TemaPopuler.id != null)
                      return (
                        <div className="col-lg-6 mb-5">
                          <Link
                            href={{
                              pathname: `/akademi/[slug]`,
                              query: {
                                slug: TemaPopuler.slug_akademi,
                                tema: TemaPopuler.id,
                              },
                            }}
                          >
                            <a className="p-0">
                              <div className="card p-2 lift rounded-3">
                                <div className="row gx-0 align-items-center">
                                  <div className="col">
                                    <div className="card-body p-0 mx-3">
                                      <h6
                                        className="fw-semi-bold line-clamp-1 mb-n1"
                                        onMouseOver={onMouseOverLineClamp}
                                        onMouseLeave={onMouseLeaveLineClamp}
                                      >
                                        {TemaPopuler.nama_tema}
                                      </h6>
                                      <span className="fw-semi-bold font-size-12 text-muted">
                                        {TemaPopuler.nama_akademi}
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </a>
                          </Link>
                        </div>
                      );
                  })}
                </div>
              </div>
            </div>
          </div>
        </section>
      )}

      {/* Rilis Media Section */}
      {loadingRilisMedia ? (
        <section className="bg-white pt-8 pb-80">
          <div className="container">
            <div className="row align-items-end mb-4 mb-md-7">
              <div className="col-md mb-4 mb-md-0">
                <h2 className="fw-bolder mb-1">
                  <Skeleton width={200} height={30}></Skeleton>
                </h2>
                <h5 className="mb-0 text-muted">
                  <Skeleton width={300} height={20}></Skeleton>
                </h5>
              </div>
            </div>
            <div className="row gx-3 row-cols-lg-2">
              <div className="col-6 col-lg-4 mb-5 mb-md-5">
                <Skeleton height={207} />
              </div>

              <div className="col-6 col-lg-4 mb-5 mb-md-5">
                <Skeleton height={207} />
              </div>

              <div className="col-6 col-lg-4 mb-5 mb-md-5">
                <Skeleton height={207} />
              </div>

              <div className="col-6 col-lg-4 mb-5 mb-md-5">
                <Skeleton height={207} />
              </div>

              <div className="col-6 col-lg-4 mb-5 mb-md-5">
                <Skeleton height={207} />
              </div>

              <div className="col-6 col-lg-4 mb-5 mb-md-5">
                <Skeleton height={207} />
              </div>
            </div>
          </div>
        </section>
      ) : (
        <section className="bg-white pt-8 pb-80">
          <div className="container">
            <div className="row align-items-end mb-4 mb-md-7">
              <div className="col-md mb-4 mb-md-0">
                <h2 className="fw-bolder mb-1">Rilis Media</h2>
                <h5 className="mb-0 text-muted">
                  Berita, Artikel, dan Pengumuman Terbaru
                </h5>
              </div>
            </div>
            <div className="row gx-3 row-cols-lg-2">
              {/* artikel */}
              {!loading &&
                ListRilisMedia &&
                ListRilisMedia[0] !== null &&
                ListRilisMedia.slice(0, 6).map((media, i) => {
                  if (media.tabel == "artikel")
                    return (
                      <div className="col-6 col-lg-4 mb-5 mb-md-5">
                        <div className="card border rounded-3 sk-fade  h-100">
                          <div className="card-zoom position-relative rounded-bottom-0">
                            <Link href={`/${media.tabel}/${[media.slug]}`}>
                              <a className="card-img d-block sk-thumbnail img-ratio-3 rounded-bottom-0">
                                <img
                                  className="shadow-light-lg img-fluid"
                                  src={PortalHelper.urlPublikasiLogo(
                                    media.gambar,
                                  )}
                                  alt={PortalHelper.truncateString(
                                    media.judul,
                                    75,
                                  )}
                                />
                              </a>
                            </Link>
                          </div>

                          <div className="card-footer p-3">
                            <Link href={`/${media.tabel}`}>
                              <a>
                                <span className="fw-semi-bold text-blue">
                                  {PortalHelper.capitalizeFirstLetter(
                                    media.tabel,
                                  )}
                                </span>
                              </a>
                            </Link>

                            <Link href={`/${media.tabel}/${[media.slug]}`}>
                              <a>
                                <h6 className="fw-bolder line-clamp-2 mb-0">
                                  {PortalHelper.truncateString(media.judul, 75)}
                                </h6>
                              </a>
                            </Link>

                            <Link href={`/${media.tabel}/${media.slug}`}>
                              <span>
                                <Moment format="DD MMMM YYYY" locale="id">
                                  {media.tanggal_publish}
                                </Moment>
                              </span>
                            </Link>
                          </div>
                        </div>
                      </div>
                    );
                })}

              {/* informasi */}
              {!loading &&
                ListRilisMedia &&
                ListRilisMedia[0] !== null &&
                ListRilisMedia.slice(0, 6).map((media) => {
                  if (media.tabel == "informasi")
                    return (
                      <div className="col-6 col-lg-4 mb-5 mb-md-5">
                        <div className="card border rounded-3 sk-fade  h-100">
                          <div className="card-zoom position-relative rounded-bottom-0">
                            <Link href={`/${media.tabel}/${[media.slug]}`}>
                              <a className="card-img d-block sk-thumbnail img-ratio-3 rounded-bottom-0">
                                <img
                                  className="shadow-light-lg img-fluid"
                                  src={PortalHelper.urlPublikasiLogo(
                                    media.gambar,
                                  )}
                                  alt={PortalHelper.truncateString(
                                    media.judul,
                                    75,
                                  )}
                                />
                              </a>
                            </Link>
                          </div>

                          <div className="card-footer p-3">
                            <Link href={`/${media.tabel}`}>
                              <a>
                                <span className="fw-semi-bold text-blue">
                                  {PortalHelper.capitalizeFirstLetter(
                                    media.tabel,
                                  )}
                                </span>
                              </a>
                            </Link>

                            <Link href={`/${media.tabel}/${[media.slug]}`}>
                              <a>
                                <h6 className="fw-bolder line-clamp-2 mb-0">
                                  {PortalHelper.truncateString(media.judul, 75)}
                                </h6>
                              </a>
                            </Link>

                            <Link href={`/${media.tabel}/${media.slug}`}>
                              <span>
                                <Moment format="DD MMMM YYYY" locale="id">
                                  {media.tanggal_publish}
                                </Moment>
                              </span>
                            </Link>
                          </div>
                        </div>
                      </div>
                    );
                })}
            </div>
          </div>
        </section>
      )}

      <section className="bg-diploy pt-8">
        <div className="container">
          <div className="row align-items-end mb-4 mb-md-7">
            <div className="col-md mb-4 mb-md-0">
              <h2 class="fw-bolder text-white mb-1">
                Pasca Pelatihan
                <br />
                Digital Talent Scholarship
              </h2>
              <h5 class="mb-0 text-muted">
                Bagi kamu yang telah lulus pelatihan, kini saatnya untuk
                menyambut masa depan
              </h5>
            </div>
            <div className="col-md-auto">
              <a
                href="https://diploy.id/tentang-talenta"
                target="_blank"
                className="d-flex align-items-center fw-medium"
              >
                Pelajari lebih lanjut
                <div className="ms-2 d-flex">
                  <svg
                    width="10"
                    height="10"
                    viewBox="0 0 10 10"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M7.7779 4.6098L3.32777 0.159755C3.22485 0.0567475 3.08745 0 2.94095 0C2.79445 0 2.65705 0.0567475 2.55412 0.159755L2.2264 0.487394C2.01315 0.700889 2.01315 1.04788 2.2264 1.26105L5.96328 4.99793L2.22225 8.73895C2.11933 8.84196 2.0625 8.97928 2.0625 9.1257C2.0625 9.27228 2.11933 9.4096 2.22225 9.51269L2.54998 9.84025C2.65298 9.94325 2.7903 10 2.9368 10C3.0833 10 3.2207 9.94325 3.32363 9.84025L7.7779 5.38614C7.88107 5.2828 7.93774 5.14484 7.93741 4.99817C7.93774 4.85094 7.88107 4.71305 7.7779 4.6098Z"
                      fill="currentColor"
                    />
                  </svg>
                </div>
              </a>
            </div>
          </div>

          <div className="row mt-5">
            <div className="col-12 col-lg-6 p-0 d-flex mb-0">
              <img
                src="assets/img/talent-page-pasca.png"
                className="img-responsive p-0 aos-init aos-animate"
                style={{ alignSelf: "self-end" }}
                alt="diploy"
                data-aos="fade-up"
              />
            </div>
            <div className="col-12 col-lg-6 pb-50">
              <div class="card bg-biscay rounded-lg shadow p-2 lift mb-4">
                <div class="row gx-0">
                  <div class="col-auto">
                    <img
                      src="https://diploy.id/images/vector/kurasi.png"
                      className="h-100p mb-2"
                    />
                  </div>

                  <div class="col">
                    <div class="ms-5 py-2">
                      <h5 class="mb-0 text-white fw-bolder">
                        Perluasan Kesempatan Kerja
                      </h5>
                      <p
                        className="mb-0 text-light fw-500"
                        style={{ fontSize: "12px !important" }}
                      >
                        Fasilitasi perluasan kesempatan kerja untuk
                        mempertemukan Alumni Digital Talent Scholarship dengan
                        mitra perusahaan atau Industri yang membutuhkan talenta
                        melalui platform Diploy.
                      </p>
                    </div>
                  </div>
                </div>
              </div>

              <div class="card bg-biscay rounded-lg shadow p-2 lift mb-4">
                <div class="row gx-0">
                  <div class="col-auto">
                    <img
                      src="https://diploy.id/images/vector/vix.png"
                      className="h-100p mb-2"
                    />
                  </div>

                  <div class="col">
                    <div class="ms-5 py-2">
                      <h5 class="mb-0 text-white fw-bolder">Mentor Class</h5>
                      <p
                        className="mb-0 text-light fw-500"
                        style={{ fontSize: "12px !important" }}
                      >
                        Materi video on-demand terkait persiapan kerja dan
                        pengenalan profesi/industri melalui platform Diploy
                        untuk membekali Alumni Digital Talent Scholarship
                        memasuki dunia kerja.
                      </p>
                    </div>
                  </div>
                </div>
              </div>

              <div class="card bg-biscay rounded-lg shadow p-2 lift mb-4 mb-xl-6">
                <div class="row gx-0">
                  <div class="col-auto">
                    <img
                      src="https://diploy.id/images/vector/profiling.png"
                      className="h-100p mb-2"
                    />
                  </div>

                  <div class="col">
                    <div class="ms-5 py-2">
                      <h5 class="mb-0 text-white fw-bolder">
                        Digitalent After Class
                      </h5>
                      <p
                        className="mb-0 text-light fw-500"
                        style={{ fontSize: "12px !important" }}
                      >
                        Workshop untuk meningkatkan soft skill Alumni Digital
                        Talent Scholarship yang bekerjasama dengan mitra
                        perusahaan maupun expert.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <Modal
        className="action-sheet"
        show={showModalDM}
        onHide={handleCloseModalDM}
      >
        <Modal.Body className="p-5" closeButton>
          <p>{getPlatformName()}</p>
        </Modal.Body>
      </Modal>

      <BroadcastModal />
    </div>
  );
}

export default homePage;
