// wizard resgitrasi
import React, { useState, useEffect, useRef, useMemo } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import Link from "next/link";
import { useRouter } from "next/router";
import ValidationMessage from "../../src/validation-message";
import SSOInfo from "../../components/SSOSInfo";
import Failed from "../../components/Modal/Failed";
import Success from "../../components/Modal/Success";
import PortalHelper from "../../src/helper/PortalHelper";
import FlipMove from "react-flip-move";
import CountryCode from "../../public/assets/json/phone_code.js";
import EncryptionHelper from "../../src/helper/EncryptionHelper";
import VerifikasiEmail from "../../components/Modal/VerifikasiEmailModal";
import PortalService from "../../services/PortalService";
import { CacheHelperV2 } from "../../src/helper/CacheHelper";
import SurveyService from "../../services/SurveyService";
// import eventBus

export default function SurveyIndex({ showLoadingModal }) {
  const router = useRouter();
  const { akademi_id, pelatihan_id, sqb_id } = router.query;
  const [DataSurveyDetail, setDataSurveyDetail] = useState([]);
  const [Loading, setLoading] = useState(false);

  const onClickStartButton = async () => {
    try {
      showLoadingModal(true);
      const resSurveyDetail = await SurveyService.start(
        akademi_id,
        pelatihan_id,
        sqb_id,
      );
      if (resSurveyDetail.status === 200 && resSurveyDetail.data.result.data) {
        setDataSurveyDetail(resSurveyDetail.data.result.data);
        router.push(
          `/survey/base?akademi_id=${akademi_id}&pelatihan_id=${pelatihan_id}&sqb_id=${sqb_id}`,
        );
      }
    } catch (error) {
      showLoadingModal(false);
    }
  };

  useEffect(async () => {
    setLoading(true);
    try {
      if (router.isReady) {
        const resSurveyDetail = await SurveyService.detail(
          akademi_id,
          pelatihan_id,
          sqb_id,
        );
        setDataSurveyDetail(resSurveyDetail.data.result.data);
        setLoading(false);
      }
    } catch (error) {
      //
    }
  }, [router.isReady]);

  return (
    <>
      <div>
        <div className="container">
          <div className="row my-8">
            <div className="col-lg-12">
              <div className="card px-6 py-8">
                <div className="row">
                  <div className="col-lg-5 mb-lg-0">
                    <h2 className="text-center mb-8">Panduan Survey</h2>
                    <img
                      src="/assets/img/illustrations/illustration-11.svg"
                      className="img-fluid pt-5"
                    />
                  </div>
                  <div className="col-lg-7">
                    <div className="d-block rounded bg-info">
                      <div className="pt-6 px-5">
                        <div className="d-flex align-items-center">
                          <div>
                            <h4 className="mb-4">Harap dibaca :</h4>
                            <p>
                              Sebelum mengerjakan tes, harap perhatikan dan
                              lakukan hal-hal berikut
                            </p>
                            <ul
                              className="list-style-v2 list-unstyled"
                              style={{ textAlign: "left" }}
                            >
                              <li>
                                Alokasi waktu yang diberikan untuk mengerjakan
                                Tes Substansi sesuai dengan masing-masing tema
                                pelatihan. Informasi tersebut dapat di akses
                                pada dashboard Tes Substansi.
                              </li>
                              <li>
                                Peserta wajib menjawab seluruh soal Tes
                                Substansi dan jumlah soal sesuai dengan
                                masing-masing tema pelatihan. Tidak ada nilai
                                negatif untuk jawaban yang salah.
                              </li>
                              <li>
                                Setelah Tes Substansi dimulai, waktu tes tidak
                                dapat diberhentikan dan tes tidak dapat diulang.
                                Setelah waktu habis, halaman soal akan tertutup
                                secara otomatis.
                              </li>
                              <li>
                                Skor untuk soal yang sudah dijawab tetap
                                terhitung walaupun peserta belum menekan tombol
                                submit atau peserta mengalami force majeure.
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div className="form-row place-order p-5">
                        <button
                          type="button"
                          className="btn btn-blue btn-block"
                          onClick={onClickStartButton}
                        >
                          <i className="fa fa-play me-2"></i>MULAI SURVEY
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
