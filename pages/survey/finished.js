// wizard resgitrasi
import React, { useState, useEffect, useRef, useMemo } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import Link from "next/link";
import { useRouter } from "next/router";
import ValidationMessage from "../../src/validation-message";
import SSOInfo from "../../components/SSOSInfo";
import Failed from "../../components/Modal/Failed";
import Success from "../../components/Modal/Success";
import PortalHelper from "../../src/helper/PortalHelper";
import FlipMove from "react-flip-move";
import CountryCode from "../../public/assets/json/phone_code.js";
import EncryptionHelper from "../../src/helper/EncryptionHelper";
import VerifikasiEmail from "../../components/Modal/VerifikasiEmailModal";
import PortalService from "../../services/PortalService";
import { CacheHelperV2 } from "../../src/helper/CacheHelper";
import SurveyService from "../../services/SurveyService";
// import eventBus

export default function SurveyIndex({ showLoadingModal }) {
  const router = useRouter();
  const { akademi_id, pelatihan_id, sqb_id } = router.query;
  const [DataSurveyDetail, setDataSurveyDetail] = useState([]);
  const [Loading, setLoading] = useState(false);

  useEffect(async () => {
    setLoading(true);
    try {
      if (router.isReady) {
        const resSurveyDetail = await SurveyService.detail(
          akademi_id,
          pelatihan_id,
          sqb_id,
        );
        setDataSurveyDetail(resSurveyDetail.data.result.data);
        setLoading(false);
      }
    } catch (error) {
      //
    }
  }, [router.isReady]);

  return (
    <>
      <div className="container bg-white my-8">
        <div className="row py-8">
          <div className="col-lg-7 col-xl-6 order-md-2 order-lg-2 order-sm-2 order-xl-2 d-flex align-items-center">
            <div className="d-block rounded bg-light w-100">
              <div className="pt-6 px-5 px-lg-3 px-xl-5">
                <div className="row">
                  <div className="col-12 px-4">
                    <div className="alert col-12 bg-green" role="alert">
                      <div className="row mb-md-0">
                        <div className="row align-items-center">
                          <div className="col-auto px-3">
                            <i className="fa fa-check-circle text-white fa-2x"></i>
                          </div>
                          <div className="col px-3">
                            <h5 className="text-white mb-0">
                              Anda telah mengerjakan{" "}
                              <strong>{DataSurveyDetail.survey_nama}</strong>{" "}
                              ini.
                            </h5>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="mb-5">
                      <table className="mb-2" style={{ fontSize: 14 }}>
                        <tbody>
                          <tr>
                            <td className="align-top" width={120}>
                              Nama Pelatihan
                            </td>
                            <td
                              className="align-top"
                              width={10}
                              valign="center"
                            >
                              :
                            </td>
                            <td className="align-top">ASDOK</td>
                          </tr>
                          <tr>
                            <td className="align-top">Nama Akademi</td>
                            <td className="align-top">:</td>
                            <td className="align-top">ASDOK</td>
                          </tr>
                        </tbody>
                      </table>
                      <p className="mb-0">
                        Silakan lihat detail dihalaman{" "}
                        <Link
                          href={{
                            pathname: "/auth/user-profile",
                            query: { menu: "survey" },
                          }}
                        >
                          Profil
                        </Link>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-5 col-xl-6 mb-6 order-sm-1 order-md-1 order-lg-1 order-xl-1 d-flex align-items-center">
            <img
              src="/assets/img/illustrations/illustration-10.jpg"
              className="img-fluid"
            />
          </div>
        </div>
      </div>
    </>
  );
}
