// wizard resgitrasi
import React, { useState, useEffect, useRef, useMemo } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import Link from "next/link";
import { useRouter } from "next/router";
import ValidationMessage from "../../src/validation-message";
import SSOInfo from "../../components/SSOSInfo";
import Failed from "../../components/Modal/Failed";
import Success from "../../components/Modal/Success";
import PortalHelper from "../../src/helper/PortalHelper";
import FlipMove from "react-flip-move";
import CountryCode from "../../public/assets/json/phone_code.js";
import EncryptionHelper from "../../src/helper/EncryptionHelper";
import VerifikasiEmail from "../../components/Modal/VerifikasiEmailModal";
import PortalService from "../../services/PortalService";
import { CacheHelperV2 } from "../../src/helper/CacheHelper";
import SurveyService from "../../services/SurveyService";
// import eventBus

export default function SurveyIndex({ showLoadingModal }) {
  const router = useRouter();
  const { akademi_id, pelatihan_id, sqb_id } = router.query;
  const [Loading, setLoading] = useState(false);

  const [DataSurveyDetail, setDataSurveyDetail] = useState([]);
  const [DataSurveyBase, setDataSurveyBase] = useState([]);
  const [DataSurveyQuestion, setDataSurveyQuestion] = useState([]);
  const [DataSurveyAnswer, setDataSurveyAnswer] = useState([]);
  const [Soal, setSoal] = useState([]);
  const [NomorSoal, setNomorSoal] = useState(1);

  const getSurveyBase = async () => {
    const resSurveyDetail = await SurveyService.detail(
      akademi_id,
      pelatihan_id,
      sqb_id,
    );
    setDataSurveyDetail(resSurveyDetail.data.result.data);
    const resSurveyBase = await SurveyService.base(
      akademi_id,
      pelatihan_id,
      sqb_id,
    );
    if (resSurveyBase.status === 200 && resSurveyBase.data.result.data) {
      setDataSurveyBase(resSurveyBase.data.result.data);
      setDataSurveyQuestion(
        JSON.parse(resSurveyBase.data.result.data.participant_answers),
      );
      setSoal(
        JSON.parse(resSurveyBase.data.result.data.participant_answers)[0],
      );
    }
  };

  const prevSoal = () => {
    if (NomorSoal > 1) {
      document.getElementsByName("answer").forEach((item) => {
        if (item.type == "radio") {
          item.checked = false;
        }
      });
      setNomorSoal(NomorSoal - 1);
    }
  };

  const nextSoal = () => {
    if (NomorSoal < DataSurveyQuestion.length) {
      document.getElementsByName("answer").forEach((item) => {
        if (item.type == "radio") {
          item.checked = false;
        }
      });
      setNomorSoal(NomorSoal + 1);
    }
  };

  const updateAnswer = (e) => {
    const data = DataSurveyQuestion;
    data[NomorSoal - 1].participant_answer = e.target.value;
    setDataSurveyQuestion(data);

    // console.log(data)
  };

  const submitAnswer = async () => {
    showLoadingModal(true);
    const resSubmit = await SurveyService.submit(
      akademi_id,
      pelatihan_id,
      sqb_id,
      DataSurveyQuestion,
    );
    showLoadingModal(false);
    router.push(
      `/survey/finished?akademi_id=${akademi_id}&pelatihan_id=${pelatihan_id}&sqb_id=${sqb_id}`,
    );
  };

  useEffect(async () => {
    try {
      if (router.isReady) {
        await getSurveyBase();
      }
    } catch (error) {
      //
    }
  }, [router.isReady]);
  return (
    <>
      <div>
        <div className="container">
          <div className="row my-8">
            <div className="col-lg-12">
              <div className="row">
                <div className="col-lg-9 mb-5">
                  <div className="card border px-6 py-5">
                    <div className="d-block">
                      <div className="pb-5">
                        <div className="row align-items-end pb-5 mb-3">
                          <div className="col-md mb-md-0">
                            <h5 className="fw-semi-bold text-muted mb-0">
                              Survey
                            </h5>
                            <span className="text-dark font-size-sm fw-normal">
                              {DataSurveyDetail.pelatihan}
                            </span>
                          </div>
                          <div className="col-md-auto">
                            <span className="text-dark font-size-sm fw-normal">
                              {DataSurveyDetail.akademi}
                            </span>
                          </div>
                        </div>
                        <hr />
                        <div className="d-flex align-items-center">
                          <div className="col-12 pt-5">
                            <h6 className="mb-0 text-muted">
                              Pertanyaan nomor {NomorSoal} dari{" "}
                              {DataSurveyQuestion.length}
                            </h6>
                            {DataSurveyQuestion[NomorSoal - 1] && (
                              <div className="my-5">
                                <h4>
                                  {DataSurveyQuestion[NomorSoal - 1].question}
                                </h4>
                                {DataSurveyQuestion[NomorSoal - 1].type &&
                                  DataSurveyQuestion[NomorSoal - 1].type ==
                                    "pertanyaan_terbuka" && (
                                    <>
                                      <div className="form-group">
                                        <input
                                          type="text"
                                          className="form-control"
                                          name="answer"
                                          placeholder="Jawaban"
                                          defaultValue={
                                            DataSurveyQuestion[NomorSoal - 1]
                                              .participant_answer
                                          }
                                          onChange={updateAnswer}
                                        />
                                      </div>
                                    </>
                                  )}
                                {DataSurveyQuestion[NomorSoal - 1].type &&
                                  DataSurveyQuestion[NomorSoal - 1].type ==
                                    "objective" && (
                                    <>
                                      {JSON.parse(
                                        DataSurveyQuestion[NomorSoal - 1]
                                          .answer,
                                      ).map((Choice) => (
                                        <>
                                          <div className="form-check">
                                            <input
                                              type="hidden"
                                              name="question_id"
                                              value={
                                                DataSurveyQuestion[
                                                  NomorSoal - 1
                                                ].id
                                              }
                                            />
                                            <input
                                              className="form-check-input"
                                              type="radio"
                                              name="answer"
                                              id={Choice.key}
                                              value={Choice.key}
                                              defaultChecked={
                                                Choice.key ==
                                                DataSurveyQuestion[
                                                  NomorSoal - 1
                                                ].participant_answer
                                                  ? true
                                                  : false
                                              }
                                              onChange={updateAnswer}
                                            />
                                            <label
                                              className="form-check-label"
                                              htmlFor={Choice.key}
                                            >
                                              {Choice.option}
                                            </label>
                                          </div>
                                        </>
                                      ))}
                                    </>
                                  )}
                                {DataSurveyQuestion[NomorSoal - 1].type &&
                                  DataSurveyQuestion[NomorSoal - 1].type ==
                                    "triggered_question" && (
                                    <>
                                      {JSON.parse(
                                        DataSurveyQuestion[NomorSoal - 1]
                                          .answer,
                                      ).map((Choice) => (
                                        <>
                                          <div className="form-check">
                                            <input
                                              type="hidden"
                                              name="question_id"
                                              value={
                                                DataSurveyQuestion[
                                                  NomorSoal - 1
                                                ].id
                                              }
                                            />
                                            <input
                                              className="form-check-input"
                                              type="radio"
                                              name="answer"
                                              id={Choice.key}
                                              value={Choice.key}
                                              defaultChecked={
                                                Choice.key ==
                                                DataSurveyQuestion[
                                                  NomorSoal - 1
                                                ].participant_answer
                                                  ? true
                                                  : false
                                              }
                                              onChange={updateAnswer}
                                            />
                                            <label
                                              className="form-check-label"
                                              htmlFor={Choice.key}
                                            >
                                              {Choice.option}
                                            </label>
                                          </div>
                                        </>
                                      ))}
                                    </>
                                  )}
                              </div>
                            )}
                          </div>
                        </div>
                      </div>
                      <div className="row gx-2">
                        <div className="col-auto align-self-start">
                          <button
                            type="button"
                            className="btn btn-outline-secondary btn-sm px-3 mb-3 font-size-14"
                            onClick={prevSoal}
                          >
                            <i className="fa fa-chevron-left"></i>
                            <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                              Sebelumnya
                            </span>
                          </button>
                        </div>
                        {NomorSoal != DataSurveyQuestion.length ? (
                          <div className="col-auto align-self-start">
                            <button
                              type="button"
                              className="btn btn-outline-secondary btn-sm px-3 mb-3 font-size-14"
                              onClick={nextSoal}
                            >
                              <i className="fa fa-chevron-right"></i>
                              <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                                Selanjutnya
                              </span>
                            </button>
                          </div>
                        ) : (
                          <div className="col-auto align-self-start">
                            <button
                              type="button"
                              className="btn btn-outline-secondary btn-sm px-3 mb-3 font-size-14"
                              onClick={submitAnswer}
                            >
                              Submit Jawaban
                            </button>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3 border-left">
                  <div className="card border px-4 py-5 mb-5">
                    <h5>Pertanyaan</h5>
                    <div className="col">
                      {DataSurveyQuestion.map((soal, i) => {
                        let n = i + 1;
                        return (
                          <a key={i} href="#">
                            <span
                              className={`badge ${
                                i == NomorSoal - 1
                                  ? "bg-green"
                                  : soal.participant_answer
                                    ? "bg-blue"
                                    : "bg-red"
                              } w-40p p-2 mb-2 mx-1`}
                            >
                              {n}
                            </span>
                          </a>
                        );
                      })}
                    </div>
                    <hr />
                    <ul className="list mb-0">
                      <li className="list-item">
                        <span className="badge bg-blue">Sudah dijawab</span>
                      </li>
                      <li className="list-item">
                        <span className="badge bg-red">Belum dijawab</span>
                      </li>
                      <li className="list-item">
                        <span className="badge bg-green">Soal Saat ini</span>
                      </li>
                      <li className="list-item">
                        <span className="badge badge-primary-soft">
                          Belum dikerjakan
                        </span>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
