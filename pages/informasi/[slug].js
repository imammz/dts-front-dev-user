import React, { useState, useEffect, useRef } from "react";
import Link from "next/link";
import axios from "axios";
import Moment from "react-moment";
import "moment/locale/id";
import Select from "react-select";
import { useRouter } from "next/router";

import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import PortalHelper from "../../src/helper/PortalHelper";
import PortalService from "../../services/PortalService";

import { Typeahead } from "react-bootstrap-typeahead";
import "react-bootstrap-typeahead/css/Typeahead.css";
import ShareModal from "./../../components/Modal/ShareModal";

function Informasi() {
  const [dataInformasi, setdataInformasi] = useState(null);
  const [jmlInformasi, setjmlInformasi] = useState("0");
  const [categoryInformasi, setCategoryInformasi] = useState(null);
  const [thresHold, setThesHold] = useState(10);
  const [loading, setLoading] = useState(false);
  const [length, setLength] = useState(thresHold);
  const [totalInformasi, setTotalInformasi] = useState(0);
  const [skeletons, setSkeletons] = useState([1, 2, 3, 4]);
  const [recentPost, setRecentPost] = useState(null);
  const [search, setSearch] = useState("");
  const [activeIndex, setActiveIndex] = useState();
  const router = useRouter();
  const [tags, setTags] = useState(null);
  //   const[textCopied, setTextCopied] = useState(false);
  const [allTags, setAllTags] = useState(null);
  const [autoCompleteOptions, setAutoCompleteOptions] = useState(null);
  const [showShareModal, setShowShareModal] = useState(false);

  //   Auto complete
  const typeaheadInformasiRef = useRef(null);
  const [selected, setSelected] = useState([]);

  const searchInformasi = (searchQuery) => {
    setLoading(true);
    router.push({
      pathname: "/informasi",
      query: {
        page: 1,
        keyword: searchQuery,
        tags: null,
        categoryId: null,
      },
    });
  };

  const informasiFindBySlug = async (slug) => {
    setLoading(true);
    setdataInformasi(null);
    const informasi = await PortalService.informasiFindBySlug(slug);
    setdataInformasi(informasi.data.result.Data);
    setLoading(false);
  };

  const informasiList = async () => {
    setLoading(true);
    router.push({
      pathname: "/informasi",
      query: {
        page: 1,
      },
    });
  };

  const filterInformasiByTags = async (tag) => {
    setLoading(true);
    router.push({
      pathname: "/informasi",
      query: {
        page: 1,
        keyword: null,
        tags: tag,
        categoryId: null,
      },
    });
  };

  const filterInformasiByCategory = (categoryId, index) => {
    setLoading(true);
    router.push({
      pathname: "/informasi",
      query: {
        page: 1,
        keyword: null,
        tags: null,
        categoryId: categoryId,
        activeIndex: index,
      },
    });
  };

  const countTotalInformasi = (pKategoriList) => {
    let sum = 0;
    pKategoriList.forEach((element) => {
      sum += Number(element.jml_data);
    });
    return sum;
  };

  useEffect(async () => {
    try {
      setLoading(true);
      if (router.isReady) {
        const addView = PortalService.addView("informasi", router.query.slug);
        const informasi = PortalService.informasiFindBySlug(router.query.slug);
        const informasiCategories = PortalService.kategoriList("Informasi");
        const recentPosts = PortalService.informasiList(3);
        const allTags = PortalService.tagsList("Informasi");
        const autoCompleteOptions = PortalService.informasiAllJudul();

        const resps = await axios.all([
          informasi,
          informasiCategories,
          recentPosts,
          allTags,
          autoCompleteOptions,
          addView,
        ]);
        setdataInformasi(resps[0].data.result.Data);
        setCategoryInformasi(resps[1].data.result.Data);
        setRecentPost(resps[2].data.result.Data);
        setAllTags(resps[3].data.result.Data);
        setAutoCompleteOptions(resps[4].data.result.Data);
        const tag =
          resps[0].data.result.Data.tag != null
            ? resps[0].data.result.Data.tag.split(",")
            : null;
        setTags(tag);

        setTotalInformasi(countTotalInformasi(resps[1].data.result.Data));

        setLoading(false);
      }
    } catch (err) {
      console.error(err);
    }
  }, [router.isReady]);

  const handleShareClose = () => {
    setShowShareModal(false);
  };

  return (
    <>
      <ShareModal handleClose={handleShareClose} show={showShareModal} />

      <header className="py-6 bg-blue mb-6 z-index-0">
        <div className="container">
          <div className="row align-items-end" data-aos="fade-up">
            <div className="col-md mb-md-0">
              <h3 className="fw-bolder text-white mb-0">Informasi</h3>
              <p className="text-white mb-0">
                Ragam Informasi terkini seputar Digital Talent Schoalrship
              </p>
            </div>
          </div>
        </div>
      </header>

      <div className="container">
        <div className="row mb-9">
          {loading && (
            <div className="col-md-7 pt-lg-6 col-lg-8 col-xl-9">
              <Skeleton height={320} className="mb-6" />
              <div className="row mb-6 mb-md-10 align-items-center">
                <div className="col-12 mb-5 mb-md-2">
                  <Skeleton height={20} />
                  <Skeleton height={20} />
                  <Skeleton height={20} />
                  <Skeleton height={20} />
                  <Skeleton height={20} />
                  <Skeleton height={20} />
                  <Skeleton height={20} />
                  <Skeleton height={20} />
                  <Skeleton height={20} />
                  <Skeleton height={20} />
                </div>
              </div>
            </div>
          )}

          {!loading && dataInformasi && (
            <div className="col-md-7 pt-lg-6 col-lg-8 col-xl-9">
              <div className="pe-3">
                <div className="mb-5">
                  <img
                    src={PortalHelper.urlPublikasiLogo(dataInformasi.gambar)}
                    alt={dataInformasi.judul_informasi}
                    className="img-responsive border rounded-5"
                  />
                </div>

                <span className="badge badge-blue font-size-sm fw-normal mb-1 rounded-5">
                  Informasi
                </span>
                <h2 className="fw-bolder text-capitalize w-96 mb-2">
                  {dataInformasi.judul_informasi}
                </h2>
                <ul className="nav mx-n3 mb-4 d-flex">
                  <li className="nav-item px-3 mb-2">
                    <span className="font-size-14 text-muted">
                      <Moment format="DD MMMM YYYY" locale="id">
                        {dataInformasi.tanggal_publish}
                      </Moment>
                    </span>
                  </li>
                  <li className="nav-item px-3 mb-2">
                    <span className="font-size-14 text-muted">
                      {" "}
                      {`${dataInformasi.total_views} kali dibaca`}
                    </span>
                  </li>
                  <li className="nav-item px-3 mb-2">
                    <a
                      href="#"
                      onClick={() => {
                        setShowShareModal(true);
                      }}
                      className="text-blue font-size-14 fw-medium d-flex align-items-center"
                    >
                      <i className="fa fa-share-alt"></i>

                      <span className="text-blue font-size-14 ms-2">Share</span>
                    </a>
                  </li>
                </ul>

                <div
                  className="post-body font-size-16 text-justify"
                  dangerouslySetInnerHTML={{
                    __html: dataInformasi.isi_informasi,
                  }}
                ></div>
                <div className="mb-9">
                  <p className="text-dark fw-bolder">
                    Kontributor -
                    <span className="font-size-14 text-blue ms-1">
                      Media Publikasi & Pasca Pelatihan{" "}
                      {/* {dataInformasi.user_name}*/}
                    </span>
                  </p>
                </div>
              </div>
            </div>
          )}
          <div className="col-md-5 pt-lg-6 col-lg-4 col-xl-3 border-left-lg border-left-md border-left-sm">
            <div className="ps-lg-3 ps-sm-0">
              <h4 className="fw-bolder text-muted mb-5 mt-4">Pencarian</h4>
              {loading && <Skeleton height={50} className="mb-6" />}
              {!loading && (
                <div className="border rounded mb-8">
                  <div className="input-group">
                    <Typeahead
                      id="auto-complete-search-informasi"
                      onChange={(select) => {
                        setSelected(select);
                      }}
                      onInputChange={(e) => {
                        setSearch(e);
                      }}
                      options={autoCompleteOptions}
                      placeholder="Cari informasi..."
                      labelKey="judul_informasi"
                      emptyLabel="Tidak ada rekomendasi."
                      ref={typeaheadInformasiRef}
                      minLength={1}
                      onKeyDown={(e) => {
                        if (e.key == "Tab") {
                          return;
                        }
                        if (e.key == "Enter") {
                          const q =
                            selected.length > 0
                              ? selected[0].judul_informasi
                              : e.target.value;
                          searchInformasi(q);
                        }
                      }}
                    />
                    <div className="input-group-append">
                      <button
                        className="btn btn-sm border-0 text-secondary icon-uxs"
                        type="submit"
                        onClick={() => {
                          const q =
                            selected.length > 0
                              ? selected[0].judul_informasi
                              : search;
                          searchInformasi(q);
                        }}
                      >
                        <svg
                          width="20"
                          height="20"
                          viewBox="0 0 20 20"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M8.80758 0C3.95121 0 0 3.95121 0 8.80758C0 13.6642 3.95121 17.6152 8.80758 17.6152C13.6642 17.6152 17.6152 13.6642 17.6152 8.80758C17.6152 3.95121 13.6642 0 8.80758 0ZM8.80758 15.9892C4.8477 15.9892 1.62602 12.7675 1.62602 8.80762C1.62602 4.84773 4.8477 1.62602 8.80758 1.62602C12.7675 1.62602 15.9891 4.8477 15.9891 8.80758C15.9891 12.7675 12.7675 15.9892 8.80758 15.9892Z"
                            fill="currentColor"
                          />
                          <path
                            d="M19.762 18.6121L15.1007 13.9509C14.7831 13.6332 14.2687 13.6332 13.9511 13.9509C13.6335 14.2682 13.6335 14.7831 13.9511 15.1005L18.6124 19.7617C18.7712 19.9205 18.9791 19.9999 19.1872 19.9999C19.395 19.9999 19.6032 19.9205 19.762 19.7617C20.0796 19.4444 20.0796 18.9295 19.762 18.6121Z"
                            fill="currentColor"
                          />
                        </svg>
                      </button>
                    </div>
                  </div>
                </div>
              )}

              {loading && !categoryInformasi && (
                <div className="mb-8">
                  <h4 className="fw-bolder text-muted mb-5 mt-4">Kategori</h4>
                  {skeletons.map((skeleton, index) => (
                    <Skeleton className="my-2" key={`skeleton2_${skeleton}`} />
                  ))}
                </div>
              )}

              {categoryInformasi && (
                <div className="mb-8">
                  <h4 className="fw-bolder text-muted mb-5 mt-4">Kategori</h4>
                  <div className="nav flex-column nav-vertical">
                    <div
                      onClick={() => {
                        setActiveIndex(-1);
                        filterInformasiByCategory("", -1);
                      }}
                      className={`nav-link py-2 px-0 ${
                        activeIndex == -1 ? "active" : ""
                      }`}
                      style={{ cursor: "pointer" }}
                    >{`Semua Kategori (${totalInformasi})`}</div>
                    {categoryInformasi.map((category, index) => (
                      <div
                        style={{ cursor: "pointer" }}
                        onClick={(el) => {
                          setActiveIndex(index);
                          filterInformasiByCategory(category.id, index);
                        }}
                        className={`nav-link py-2 px-0 ${
                          activeIndex == index ? "active" : ""
                        }`}
                      >
                        {`${category.nama} (${category.jml_data})`}
                      </div>
                    ))}
                  </div>
                </div>
              )}

              <div className="mb-8">
                <h4 className="fw-bolder text-muted mb-5">Terbaru</h4>
                <ul className="list-unstyled mb-0">
                  {loading &&
                    !recentPost &&
                    skeletons.map((skeleton) => (
                      <li className="media mb-5" key={`skeleton3_${skeleton}`}>
                        <div className="row mt-3">
                          <div className="col-4">
                            <Skeleton
                              style={{ height: "24px", width: "24px" }}
                              circle
                            />
                          </div>
                          <div className="col-6 p-0">
                            <Skeleton className="" />
                            <Skeleton className="font-size-sm mt-0" />
                          </div>
                        </div>
                      </li>
                    ))}

                  {recentPost &&
                    recentPost.map((informasi, index) => (
                      <li className="media mb-5 d-flex" key={informasi.id}>
                        <Link
                          href="/informasi/[slug]"
                          as={`/informasi/${[informasi.slug]}`}
                        >
                          <a
                            href="#"
                            className="mw-70p d-block me-5"
                            onClick={() => {
                              informasiFindBySlug(informasi.slug);
                            }}
                          >
                            <img
                              src={PortalHelper.urlPublikasiLogo(
                                informasi.gambar,
                              )}
                              alt="..."
                              className="avatar-md rounded-3"
                            />
                          </a>
                        </Link>
                        <div className="media-body flex-shrink-1">
                          <Link
                            href="/informasi/[slug]"
                            as={`/informasi/${[informasi.slug]}`}
                          >
                            <a className="d-block">
                              <h6 className="fw-bolder line-clamp-2 mb-0">
                                {PortalHelper.truncateString(
                                  informasi.judul_informasi,
                                  20,
                                )}
                              </h6>
                            </a>
                          </Link>
                          <span className="font-size-11">
                            <Moment format="DD MMMM YYYY" locale="id">
                              {informasi.tanggal_publish}
                            </Moment>
                          </span>
                        </div>
                      </li>
                    ))}
                </ul>
              </div>

              {loading && (
                <div className="border rounded mb-6 p-5 py-md-6 ps-md-6 pe-md-4">
                  {/* Belum ada filter by tag */}
                  <h4 className="mb-5">Tags</h4>

                  {skeletons.map((skeleton) => (
                    <Skeleton
                      className="btn btn-sm btn-light mx-2 mb-2 w-25"
                      inline="true"
                    />
                  ))}
                </div>
              )}

              {!loading && (
                <div className="mb-8">
                  {/* Belum ada filter by tag */}
                  <h4 className="fw-bolder text-muted mb-5">Tags</h4>
                  {allTags &&
                    allTags[0] !== null &&
                    allTags.map((tag) => (
                      <a
                        href="#"
                        className="btn btn-sm btn-outline-gray-800 rounded-pill px-5 me-1 mb-2"
                        onClick={() => {
                          filterInformasiByTags(tag);
                        }}
                      >
                        {decodeURIComponent(tag)}
                      </a>
                    ))}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

Informasi.bodyBackground = "bg-white";
export default Informasi;
