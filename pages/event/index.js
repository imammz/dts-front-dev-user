import React, { useState, useEffect } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import axios from "axios";
import Moment from "react-moment";
import moment from "moment/moment";
import "moment/locale/id";
import Select from "react-select";

import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
//
import PortalHelper from "../../src/helper/PortalHelper";
import PortalService from "./../../services/PortalService";
import Pagination from "../../components/Pagination";
import SideImageWidget from "../../components/SideImageWidget";
import SideWidget from "../../components/SideWidget";
import SimpleDateRange from "../../components/SimpleDateRange";
import GeneralService from "../../services/GeneralService";

export default function ListEvent() {
  const router = useRouter();
  const [dataEvents, setdataEvents] = useState([]);
  const [loading, setLoading] = useState(false);
  const [length, setLength] = useState(10);
  const [skeletons, setSkeletons] = useState([1, 2, 3, 4]);
  const [metaData, setMetaData] = useState(null);
  const [sortBy, setSortBy] = useState("Newest");
  const [timeDiff, setTimeDiff] = useState({});
  const page = router.query.page ? router.query.page : 1;
  const serverTime = moment().format("YYYY-MM-DD HH:mm:ss");

  const sortOptions = [
    {
      value: "Newest",
      label: "Terbaru",
    },
    {
      value: "Kategori",
      label: "Kategori",
    },
  ];

  useEffect(async () => {
    try {
      setLoading(true);
      const listEvent = PortalService.eventList(page, length, sortBy);
      // const sidebars = PortalService.imagetronList();

      const resps = await axios.all([listEvent]);
      setMetaData(resps[0].data.result.meta);

      setdataEvents(resps[0].data.result.data);

      setLoading(false);
      getTimeDiff();
    } catch (err) {
      console.error(err);
    }
  }, [length, sortBy]);

  const getTimeDiff = async () => {
    const resp = await GeneralService.getServerTime();
    setTimeDiff(resp);
  };

  useEffect(async () => {
    if (
      Object.keys(router.query).length >= 0 &&
      router.query.hasOwnProperty("page")
    ) {
      setLoading(true);
      const resps = await PortalService.eventList(page, length, sortBy);
      setdataEvents(resps.data.result.data);
      setMetaData(resps.data.result.meta);
      setLoading(false);
      getTimeDiff();
    }
  }, [page]);

  return (
    <div>
      <header className="py-6 bg-blue mb-8 z-index-0">
        <div className="container">
          <div className="row align-items-end" data-aos="fade-up">
            <div className="col-md mb-md-0">
              <h3 className="fw-bolder text-white mb-0">Events</h3>
              <p className="mb-5 mb-md-0 mb-lg-0 text-white">
                Ayo ikuti berbagai Event dan Agenda menarik
              </p>
            </div>
            <div className="col-md-auto col-sm-12">
              <div className="d-lg-flex rounded">
                <div className="d-lg-flex flex-wrap">
                  <div>
                    <div className="d-flex align-items-center h-50p ">
                      <div className="form-floating col-12">
                        <select
                          value={sortBy}
                          onChange={(e) => {
                            setSortBy(e.target.value);
                          }}
                          className="form-select form-select-sm form-control-sm ps-5 shadow-none text-primary fw-medium rounded-xl"
                        >
                          <option value="Terbaru">Terbaru</option>
                          <option value="Terlama">Terlama</option>
                          <option value="Buka">Buka</option>
                          <option value="Tutup">Tutup</option>
                          <option value="Kategori">Kategori</option>
                        </select>
                        <label className="ps-5">Urutkan</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
      <div className="container z-index-0">
        <div
          className={`row mb-9 ${
            dataEvents[0] == null ? "justify-content-center" : ""
          }`}
        >
          <div className="col-lg-9">
            {!loading && dataEvents[0] == null && (
              <div className="col-12 mx-auto text-center pt-8">
                <h2 className="fw-bolder mb-0">Belum Ada Data</h2>
                <p className="font-size-14 mb-6">
                  Hi Digiers, saat ini belum ada jadwal event atau agenda
                </p>
                <img
                  src="/assets/img/empty-state/no-data.png"
                  className="img-fluid align-self-center"
                  style={{ width: "500px" }}
                />
              </div>
            )}

            {/* SKELETON LOADING DATA EVENTS */}

            <div className="row">
              {loading &&
                skeletons.map((key) => (
                  <div className="col-lg-6 mb-5" key={key}>
                    <div className="card border rounded-5 sk-fade">
                      <div className="card-zoom position-relative rounded-bottom-0">
                        <Skeleton className="rounded-lg h-200p bg-grey" />
                      </div>

                      <div className="card-footer p-4">
                        <Skeleton className="mb-0" count={2} />

                        <Skeleton className="mt-4" />
                      </div>
                    </div>
                  </div>
                ))}
            </div>

            {/* START LIST EVENT */}
            <div className="row">
              {!loading &&
                dataEvents[0] != null &&
                dataEvents.map((event) => (
                  <div className="col-lg-6 mb-5" key={event.id}>
                    <div className="card border rounded-5 sk-fade">
                      <div className="card-zoom position-relative rounded-bottom-0">
                        <Link href={`/event/${[event.id]}`}>
                          <a className="card-img d-block sk-thumbnail img-ratio-3 rounded-bottom-0">
                            <img
                              className="shadow-light-lg img-fluid"
                              src={PortalHelper.urlPublikasiLogo(event.gambar)}
                            />
                          </a>
                        </Link>
                      </div>

                      <div className="card-footer p-4">
                        <Link href={`/event/${[event.id]}`}>
                          <a className="d-block mb-2">
                            <span className="text-dark">{event.kategori}</span>
                            <span className="float-end">
                              <div className="d-flex align-items-center">
                                <div
                                  className={`badge badge-sm badge-pill bg-${
                                    PortalHelper.isEventOpen(
                                      event.tanggal_selesai,
                                      serverTime,
                                    )
                                      ? "green"
                                      : "red"
                                  }`}
                                >
                                  <span className="text-white font-size-sm fw-normal">
                                    {PortalHelper.isEventOpen(
                                      event.tanggal_selesai,
                                      serverTime,
                                    )
                                      ? "Buka"
                                      : "Tutup"}
                                  </span>
                                </div>
                              </div>
                            </span>
                            <h5 className="fw-bolder line-clamp-2 mb-0">
                              {event.judul_event}
                            </h5>
                            <span>
                              <SimpleDateRange
                                startDate={event.tanggal_mulai}
                                endDate={event.tanggal_selesai}
                                diff={timeDiff.diff}
                              ></SimpleDateRange>
                            </span>
                          </a>
                        </Link>
                        <ul className="nav mx-n3 mb-0">
                          <li
                            className="nav-item px-3 mb-2"
                            key={`${event.id}_3`}
                          >
                            <div className="d-flex align-items-center">
                              <div className="me-1 d-flex text-secondary icon-uxs">
                                <i className="bi bi-mic"></i>
                              </div>
                              <div className="font-size-sm">
                                {event.jenis == 1
                                  ? "Online"
                                  : event.jenis == 2
                                    ? "Online dan Offline"
                                    : "Offline"}
                              </div>
                            </div>
                          </li>
                          {event.lokasi && event.lokasi != "null" && (
                            <li className="nav-item px-3" key={`${event.id}_2`}>
                              <div className="d-flex align-items-center">
                                <div className="me-1 d-flex text-secondary icon-uxs">
                                  <i className="bi bi-geo-alt"></i>
                                </div>
                                <div className="font-size-sm">
                                  {event.lokasi}
                                </div>
                              </div>
                            </li>
                          )}
                          <li
                            className="nav-item px-3 mb-2"
                            key={`${event.id}_4`}
                          ></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                ))}
              {/* END LIST PELATIHAN */}
            </div>

            {/* Pagination ================================================== */}

            {dataEvents && (
              <div className="d-flex justify-content-center align-items-center my-9">
                <Pagination pageSize={metaData?.last_page} currentPage={page} />
              </div>
            )}
          </div>

          <div className="col-xl-3 px-4 mb-xl-0">
            {/* SIDE WIDGET ================================================= */}
            <SideWidget loading={loading} />
            {/* SIDEBAR IMAGE
                ================================================== */}
            <SideImageWidget loading={loading} />
          </div>
        </div>
      </div>
    </div>
  );
}
