import React, { useState, useEffect } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import axios from "axios";
import Moment from "react-moment";
import moment from "moment/moment";
import Select from "react-select";
import Flickity from "react-flickity-component";
import SimpleDateRange from "../../components/SimpleDateRange";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
//
import PortalHelper from "../../src/helper/PortalHelper";
import PortalService from "../../services/PortalService";
import GeneralService from "../../services/GeneralService";

function DetailEvent() {
  const [detailEvent, setDetailEvent] = useState(null);
  const [daftarEvent, setDaftarEvent] = useState(null);
  const [loading, setLoading] = useState(false);
  const [textCopied, setTextCopied] = useState(false);
  const [timeDiff, setTimeDiff] = useState({});
  const router = useRouter();
  const _today = new Date();
  const serverTime = moment().format("YYYY-MM-DD HH:mm:ss");

  const flickityOptions = {
    initialIndex: 1,
    pageDots: true,
    prevNextButtons: false,
    cellAlign: "left",
    wrapAround: true,
    imagesLoaded: true,
  };

  const getTimeDiff = async () => {
    const resp = await GeneralService.getServerTime();
    setTimeDiff(resp);
  };
  useEffect(async () => {
    try {
      setLoading(true);
      if (router.isReady) {
        const detailEvent = PortalService.eventById(router.query.id);
        const addView = PortalService.addView("event", "t", router.query.id);
        const events = PortalService.eventList(0, 10, "newest");
        const responses = await axios.all([detailEvent, events, addView]);

        setDetailEvent(responses[0].data.result.Data);
        setDaftarEvent(responses[1].data.result.data);
        getTimeDiff();
        setLoading(false);
      }
    } catch (err) {
      setLoading(false);

      console.error(err);
    }
  }, [router.isReady, router.asPath]);

  return (
    <div>
      {detailEvent && (
        <div
          className="modal fade"
          tabIndex="-1"
          id="shareEventModal"
          style={{
            position: "absolute!important",
          }}
        >
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
              <div className="modal-header py-4">
                <div className="row align-items-end">
                  <h4 className="fw-bolder fs-4 m-auto">Bagikan event ini!</h4>
                </div>
                <button
                  type="button"
                  className="close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                >
                  <i className="fa fa-times-circle text-muted"></i>
                </button>
              </div>
              <div className="modal-body py-5 border-top">
                <div className="input-group flex-nowrap">
                  <input
                    type="text"
                    className="form-control py-2"
                    placeholder="URL page"
                    aria-describedby="button-copy"
                    value={window.location.href}
                    readOnly
                  />
                  <button
                    className="btn btn-secondary py-2"
                    type="button"
                    id="button-copy"
                    onClick={() => {
                      setTextCopied(true);
                      navigator.clipboard.writeText(window.location.href);
                    }}
                  >
                    <span className="svg-icon svg-icon-primary svg-icon-2x">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24px"
                        height="24px"
                        viewBox="0 0 24 24"
                        version="1.1"
                      >
                        <g
                          stroke="none"
                          strokeWidth="1"
                          fill="none"
                          fillRule="evenodd"
                        >
                          <rect x="0" y="0" width="24" height="24" />
                          <path
                            d="M14,16 L12,16 L12,12.5 C12,11.6715729 11.3284271,11 10.5,11 C9.67157288,11 9,11.6715729 9,12.5 L9,17.5 C9,19.4329966 10.5670034,21 12.5,21 C14.4329966,21 16,19.4329966 16,17.5 L16,7.5 C16,5.56700338 14.4329966,4 12.5,4 L12,4 C10.3431458,4 9,5.34314575 9,7 L7,7 C7,4.23857625 9.23857625,2 12,2 L12.5,2 C15.5375661,2 18,4.46243388 18,7.5 L18,17.5 C18,20.5375661 15.5375661,23 12.5,23 C9.46243388,23 7,20.5375661 7,17.5 L7,12.5 C7,10.5670034 8.56700338,9 10.5,9 C12.4329966,9 14,10.5670034 14,12.5 L14,16 Z"
                            fill="#fff"
                            fillRule="nonzero"
                            transform="translate(12.500000, 12.500000) rotate(-315.000000) translate(-12.500000, -12.500000) "
                          />
                        </g>
                      </svg>
                    </span>
                  </button>
                </div>
                {textCopied && (
                  <span style={{ color: "success" }}>URL copied!</span>
                )}
              </div>
            </div>
          </div>
        </div>
      )}

      {/* Skeleton */}
      {loading && (
        <div>
          <header
            className="py-6 bg-blue mb-6 z-index-0"
            style={{ backgroundImage: "none" }}
          >
            <div className="container">
              <div className="row gx-0">
                <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                  <div className="card-body py-sm-0 px-0">
                    <Skeleton className="line-clamp-2 mb-4" />
                    <div className="row mt-1">
                      <Skeleton className="w-50" count={2}></Skeleton>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </header>
          <div className="container py-9">
            <div className="row mb-8">
              <div className="col-lg-8 mb-6 mb-lg-0 position-relative">
                <div className="row gx-0">
                  <div className="col-auto">
                    <Skeleton className="rounded-lg me-5 d-flex w-100p h-100p place-center" />
                  </div>

                  <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                    <div className="card-body py-sm-0 px-0">
                      <Skeleton className="line-clamp-2 mb-4" />
                      <div className="row mt-1">
                        <Skeleton className="w-50" count={2}></Skeleton>
                      </div>
                    </div>
                  </div>
                </div>
                <hr />

                <div className="my-5">
                  <Skeleton className="h-40p" />

                  <Skeleton count={3} className="h-120p mt-3"></Skeleton>
                </div>
                <div className="mb-8">
                  <Skeleton className="h-50p w-50" />
                  <Skeleton count={2} className="w-50" />
                </div>
              </div>

              <div className="col-lg-4">
                <Skeleton className="d-block rounded border p-2 shadow mb-6 h-300p">
                  <div className="pt-6 pb-5 px-5 px-lg-3 px-xl-5"></div>
                </Skeleton>
              </div>
            </div>
          </div>

          <div className="floating-help z-index-2 m-3">
            <Link href="/helpdesk">
              <a className="text-reset">Helpdesk</a>
            </Link>
          </div>
        </div>
      )}

      {!loading && detailEvent && (
        <div>
          <header className="py-6 bg-blue mb-6 z-index-0">
            <div className="container">
              <div className="row gx-0 align-items-center">
                <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                  <div className="card-body p-0">
                    <div className="mb-0">
                      <span className="text-dark font-size-sm fw-normal"></span>
                    </div>
                    <a href="#">
                      <h5 className="text-white mb-0">Event</h5>
                      <h2 className="text-white fw-bolder me-xl-14">
                        {detailEvent.judul_event}
                      </h2>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </header>
          <div className="container">
            <div className="row mb-9">
              <div className="col-xl-8 pt-lg-5">
                <img
                  className="w-100 rounded-5"
                  src={PortalHelper.urlPublikasiLogo(detailEvent.gambar)}
                />
                <h4 className="pt-6 fw-bolder">Deskripsi Event</h4>
                <div
                  className="post-body"
                  dangerouslySetInnerHTML={{
                    __html: `${detailEvent.deskripsi_event}`,
                  }}
                ></div>
                {detailEvent.pembicara && (
                  <div className="row pt-6 mb-8">
                    <h4 className="fw-bolder">Pembicara</h4>
                    <div className="col-lg">
                      <ul className="list-style-v1 list-unstyled">
                        {detailEvent.pembicara.split(";").map((pembicara) => (
                          <li>{pembicara}</li>
                        ))}
                      </ul>
                    </div>
                  </div>
                )}
              </div>
              <div className="col-xl-4 px-4 mt-lg-n12">
                <div className="d-block rounded-5 border p-2 shadow mb-6 bg-white">
                  <div className="pt-6 pb-5 px-5 px-lg-3 px-xl-5">
                    <div className="d-flex align-items-center text-alizarin mb-6">
                      {PortalHelper.dateDifference(detailEvent.tanggal_event) <
                        0 && (
                        <div>
                          <i className="fa fa-bell"></i>

                          <span className="font-size-lg ms-2">
                            {-1 *
                              PortalHelper.dateDifference(
                                detailEvent.tanggal_event,
                              )}{" "}
                            hari lagi!
                          </span>
                        </div>
                      )}

                      {PortalHelper.dateIsEqualToday(
                        detailEvent.tanggal_event,
                      ) && (
                        <div>
                          <span className="font-size-lg ms-2">
                            {/* {PortalHelper.dateDifference(detailEvent.tanggal_event)} hari yang lalu */}
                            Event dilaksanakan hari ini jam &nbsp;
                            <Moment
                              format="HH:mm"
                              add={{ minutes: timeDiff.diff }}
                            >
                              {detailEvent.tanggal_event}
                            </Moment>
                            !
                          </span>
                        </div>
                      )}
                    </div>

                    <ul className="list-group list-group-flush mb-5">
                      <li className="list-group-item d-flex align-items-center py-3">
                        <div
                          className="text-secondary d-flex icon-uxs"
                          style={{ minWidth: "16px" }}
                        >
                          <i className="bi bi-calendar2-week"></i>
                        </div>
                        <h6 className="mb-0 ms-3 me-auto">Tgl. Event</h6>
                        <span>
                          <SimpleDateRange
                            startDate={detailEvent.tanggal_mulai}
                            endDate={detailEvent.tanggal_selesai}
                            diff={timeDiff.diff}
                          ></SimpleDateRange>
                        </span>
                      </li>
                      <li className="list-group-item d-flex align-items-center py-3">
                        <div
                          className="text-secondary d-flex icon-uxs"
                          style={{ minWidth: "16px" }}
                        >
                          <i className="bi bi-mic"></i>
                        </div>
                        <h6 className="mb-0 ms-3 me-auto">Pelaksanaan</h6>
                        <span>
                          {detailEvent.jenis == 1
                            ? "Online"
                            : detailEvent.jenis == 2
                              ? "Online dan Offline"
                              : "Offline"}
                        </span>
                      </li>
                      {(detailEvent.jenis == 1 || detailEvent.jenis == 2) && (
                        <li className="list-group-item d-flex align-items-center py-3">
                          <div
                            className="text-secondary d-flex icon-uxs"
                            style={{ minWidth: "16px" }}
                          >
                            <i className="bi bi-people"></i>
                          </div>
                          <h6 className="mb-0 ms-3 me-auto">Kuota Online</h6>
                          <span>{detailEvent.quota_online} orang</span>
                        </li>
                      )}
                      {(detailEvent.jenis == 0 || detailEvent.jenis == 2) && (
                        <li className="list-group-item d-flex align-items-center py-3">
                          <div
                            className="text-secondary d-flex icon-uxs"
                            style={{ minWidth: "16px" }}
                          >
                            <i className="bi bi-people"></i>
                          </div>
                          <h6 className="mb-0 ms-3 me-auto">Kuota Offline</h6>
                          <span> {detailEvent.quota_offline} orang</span>
                        </li>
                      )}
                      <li className="list-group-item d-flex align-items-center py-3">
                        <div
                          className="text-secondary d-flex icon-uxs"
                          style={{ minWidth: "16px" }}
                        >
                          <i className="bi bi-geo-alt"></i>
                        </div>
                        <h6 className="mb-0 ms-3 me-auto">Lokasi</h6>
                        <span>
                          {!detailEvent.lokasi || detailEvent.lokasi == "null"
                            ? "Online"
                            : PortalHelper.truncateString(
                                detailEvent.lokasi,
                                25,
                              )}
                        </span>
                      </li>
                    </ul>
                    {detailEvent.link &&
                      detailEvent.link != "null" &&
                      PortalHelper.isEventOpen(
                        detailEvent.tanggal_selesai,
                        serverTime,
                      ) && (
                        <div className="mb-0">
                          <a
                            href={
                              detailEvent.link.includes("http://") ||
                              detailEvent.link.includes("https://")
                                ? detailEvent.link
                                : "https://" + detailEvent.link
                            }
                            target="_blank"
                            className="btn btn-blue rounded-pill btn-block font-size-14 btn-block mb-5"
                            type="button"
                            name="button"
                          >
                            LINK EVENT
                          </a>
                        </div>
                      )}
                    {!PortalHelper.isEventOpen(
                      detailEvent.tanggal_selesai,
                      serverTime,
                    ) && (
                      <div className="mb-0">
                        <a
                          href="#"
                          className="btn bg-red text-white rounded-pill btn-block font-size-14 btn-block mb-5"
                          type="button"
                          name="button"
                          onClick={(e) => {
                            e.preventDefault();
                          }}
                        >
                          Event Telah Selesai
                        </a>
                      </div>
                    )}

                    <p className="text-center">
                      <a
                        href="#"
                        onClick={() => {
                          setTextCopied(false);
                        }}
                        data-bs-toggle="modal"
                        data-bs-target="#shareEventModal"
                        className="font-size-14 fw-semi-bold text-blue"
                      >
                        <svg
                          width="20"
                          height="20"
                          viewBox="0 0 20 20"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M16.0283 6.25C14.3059 6.25 12.9033 4.84833 12.9033 3.125C12.9033 1.40167 14.3059 0 16.0283 0C17.7509 0 19.1533 1.40167 19.1533 3.125C19.1533 4.84833 17.7509 6.25 16.0283 6.25ZM16.0283 1.25C14.995 1.25 14.1533 2.09076 14.1533 3.125C14.1533 4.15924 14.995 5 16.0283 5C17.0616 5 17.9033 4.15924 17.9033 3.125C17.9033 2.09076 17.0616 1.25 16.0283 1.25Z"
                            fill="currentColor"
                          />
                          <path
                            d="M16.0283 20C14.3059 20 12.9033 18.5983 12.9033 16.875C12.9033 15.1517 14.3059 13.75 16.0283 13.75C17.7509 13.75 19.1533 15.1517 19.1533 16.875C19.1533 18.5983 17.7509 20 16.0283 20ZM16.0283 15C14.995 15 14.1533 15.8408 14.1533 16.875C14.1533 17.9092 14.995 18.75 16.0283 18.75C17.0616 18.75 17.9033 17.9092 17.9033 16.875C17.9033 15.8408 17.0616 15 16.0283 15Z"
                            fill="currentColor"
                          />
                          <path
                            d="M3.94531 13.125C2.22275 13.125 0.820312 11.7233 0.820312 10C0.820312 8.27667 2.22275 6.875 3.94531 6.875C5.66788 6.875 7.07031 8.27667 7.07031 10C7.07031 11.7233 5.66788 13.125 3.94531 13.125ZM3.94531 8.125C2.91199 8.125 2.07031 8.96576 2.07031 10C2.07031 11.0342 2.91199 11.875 3.94531 11.875C4.97864 11.875 5.82031 11.0342 5.82031 10C5.82031 8.96576 4.97864 8.125 3.94531 8.125Z"
                            fill="currentColor"
                          />
                          <path
                            d="M6.12066 9.39154C5.90307 9.39154 5.69143 9.27817 5.57729 9.0766C5.40639 8.77661 5.51061 8.39484 5.8106 8.22409L13.5431 3.81568C13.8422 3.64325 14.2247 3.74823 14.3947 4.04914C14.5656 4.34912 14.4614 4.73075 14.1614 4.90164L6.42888 9.30991C6.33138 9.36484 6.22564 9.39154 6.12066 9.39154Z"
                            fill="currentColor"
                          />
                          <path
                            d="M13.8524 16.2665C13.7475 16.2665 13.6416 16.2398 13.5441 16.1841L5.81151 11.7757C5.51152 11.6049 5.40745 11.2231 5.5782 10.9232C5.74818 10.6224 6.12996 10.5182 6.42994 10.6899L14.1623 15.0981C14.4623 15.269 14.5665 15.6506 14.3958 15.9506C14.2807 16.1531 14.0691 16.2665 13.8524 16.2665Z"
                            fill="currentColor"
                          />
                        </svg>
                        <span className="text-blue ms-3">Share event ini</span>
                      </a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
DetailEvent.bodyBackground = "bg-white";
export default DetailEvent;
