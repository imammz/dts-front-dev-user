import React, { useState, useEffect, useRef } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import axios from "axios";
import Moment from "react-moment";
import "moment/locale/id";
import FlipMove from "react-flip-move";
import Pagination from "../components/Pagination";
//skeleton
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

import PortalService from "../services/PortalService";
import PortalHelper from "../src/helper/PortalHelper";
// import ReactFancyBox from "react-fancybox";
// import "react-fancybox/lib/fancybox.css";:

function TidakAdaData({ filter }) {
  return (
    <>
      <div className="row">
        <div className="col-12 mx-auto text-center">
          <h2>Belum Ada Video</h2>

          {filter && (
            <p className="font-size-lg mb-6">
              Video dengan {filter?.label} &nbsp;
              <span className="text-blue font-weight-semi-bold">
                {filter?.value}
              </span>{" "}
              belum tersedia, <br />
              silahkan coba pada &nbsp;
              <span>
                <Link
                  href={{
                    pathname: "/video",
                  }}
                >
                  <a>{filter.label} lain.</a>
                </Link>
              </span>
            </p>
          )}
          {!filter && (
            <p className="font-size-lg mb-6">
              Video belum tersedia, temukan hal menarik lain di &nbsp;
              <span className="text-blue font-weight-semi-bold">
                <Link
                  href={{
                    pathname: "/",
                  }}
                >
                  <a>Halaman utama.</a>
                </Link>
              </span>
            </p>
          )}
          <img
            src="assets/img/empty-state/not-data.png"
            alt="thumbnail"
            className="img-fluid align-self-center"
            style={{ width: "500px" }}
          />
        </div>
      </div>
    </>
  );
}

function Video() {
  const router = useRouter();
  const [dataVideos, setdataVideos] = useState(null);
  const [allVideos, setAllVideos] = useState(null);
  const [loading, setLoading] = useState(false);
  const [length, setLength] = useState(9);
  const [kategori, setKategori] = useState(null);
  const [filter, setFilter] = useState(null);
  const [skeletons, setSkeletons] = useState([0, 1, 2, 3, 4, 5, 6, 7, 8]);
  const [metaData, setMetaData] = useState(null);
  const [currentVideo, setCurrentVideo] = useState(null);
  const [openIndex, setOpenIndex] = useState(-1);
  const page = router.query.page ? router.query.page : 1;

  const changeFilter = async (kategori = null) => {
    // setLoading(true);
    // const listVideo = await PortalService.videoList(kategori_id, length, page);
    router.replace({
      pathname: "",
      query: {
        page: 1,
        kategori_id: kategori?.id,
      },
    });
    if (kategori) {
      setFilter({
        value: kategori.nama,
        label: "Kategori",
      });
    }
    // setdataVideos(listVideo.data.result.data);
    // setMetaData(listVideo.data.result.data);
    // setLoading(false);
  };

  const addVideoView = async (id) => {
    const addView = await PortalService.addView("video", "slug-video", id);
  };

  useEffect(() => {
    // setTimeout(() => {
    //   if (document && dataVideos && dataVideos[0] != null) {
    //     const slides = document.querySelector("div.fancybox-slide");
    //     if (!slides) {
    //       return;
    //     }
    //     const video = dataVideos[window.location.hash.slice(-1) - 1];
    //     const elms = document.querySelectorAll(
    //       "div.fancybox-navigation button.fancybox-button"
    //     );
    //     if (elms.length > 0) {
    //       elms[0].addEventListener("click", () => {
    //         addVideoView(video.id);
    //       });
    //       elms[1].addEventListener("click", () => {
    //         addVideoView(video.id);
    //       });
    //     }
    //     return () => {
    //       if (elms.length > 0) {
    //         elms[0].removeEventListener("click");
    //         elms[1].removeEventListener("click");
    //       }
    //     };
    //   }
    // }, 500);
  }, [currentVideo]);

  useEffect(async () => {
    const reqArray = [];
    setLoading(true);
    reqArray.push(PortalService.kategoriList("Video"));
    reqArray.push(PortalService.videoList(null, length, page));
    const resps = await axios.all(reqArray);
    setKategori(resps[0].data.result.Data);
    setMetaData(resps[1].data.result.meta);
    setdataVideos(resps[1].data.result.data);
    setLoading(false);
  }, []);

  useEffect(async () => {
    try {
      if (router.isReady) {
        if (router?.query.hasOwnProperty("page")) {
          const res = await PortalService.videoList(
            router?.query?.kategori_id,
            length,
            page,
          );

          setMetaData(res.data.result.meta);
          setdataVideos(res.data.result.data);
        }
      }
    } catch (err) {
      console.error(err);
    }
  }, [router]);

  return (
    <div className="mb-6">
      {/* Header video */}
      <header
        className="py-6 mb-6 bg-blue z-index-0"
        style={{ backgroundImage: "none" }}
      >
        <div className="container">
          <div className="row align-items-end" data-aos="fade-up">
            {/*<nav aria-label="breadcrumb">
              <ol className="breadcrumb breadcrumb-scroll">
                <li className="breadcrumb-item">
                  <a className="text-gray-700" href="#">
                    Home
                  </a>
                </li>
                <li className="breadcrumb-item">
                  <Link href="/rilis-media">
                    <a className="text-gray-700">Rilis Media</a>
                  </Link>
                </li>
                <li
                  className="breadcrumb-item text-blue active"
                  aria-current="page"
                >
                  <Link href="/video">
                    <a className="dropdown-link">Video</a>
                  </Link>
                </li>
              </ol>
            </nav>*/}
            <div className="col-md mb-md-0">
              <h3 className="fw-bolder text-white mb-0">Video</h3>
              <p className="text-white mb-0">
                Rekaman momen dan kegiatan dalam video
              </p>
            </div>
          </div>
        </div>
      </header>

      {/* Body video */}
      <div
        className={`container mt-8 mb-9 ${
          dataVideos != null && dataVideos[0] == null
            ? "justify-content-center"
            : ""
        }`}
      >
        {loading && (
          <div>
            <div className="w-100 border-bottom pb-4  mb-8">
              <div className="row">
                <div className="col-2">
                  <Skeleton className="h-40p" />
                </div>
                <div className="col-2">
                  <Skeleton className="h-40p" />
                </div>
                <div className="col-2">
                  <Skeleton className="h-40p" />
                </div>
                <div className="col-2">
                  <Skeleton className="h-40p" />
                </div>
              </div>
            </div>

            <div className="row">
              {skeletons.map((id, index) => (
                <div key={{ id }} className="col-md-4 col-xs-12 mb-5">
                  <Skeleton className="h-200p w-100" />
                  <div className="row mt-3 ">
                    <div className="col-3">
                      <Skeleton className="w-100 h-40p" />
                    </div>
                    <div className="col-9">
                      <div className="col-12">
                        <Skeleton count={2} />
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        )}

        {!loading && dataVideos && (
          <div>
            <ul
              id="pills-tab"
              className="nav nav-pills border-bottom pb-4 course-tab-v2 h5 mb-8 flex-nowrap overflow-auto"
              role="tablist"
              data-aos="fade-up"
            >
              <li className="nav-item filter">
                <button
                  key="no-filter"
                  className="nav-link active"
                  data-bs-toggle="pill"
                  onClick={() => {
                    changeFilter();
                    // setFilter(null);
                  }}
                >
                  All
                </button>
              </li>
              {kategori &&
                kategori.map((filterKategori, index) => (
                  <li key={filterKategori.id} className="nav-item filter">
                    <button
                      href="#"
                      key={filterKategori.id}
                      className="nav-link"
                      data-bs-toggle="pill"
                      onClick={() => {
                        changeFilter(filterKategori);
                        // setFilter(filterKategori.id);
                      }}
                    >
                      {filterKategori.nama}
                    </button>
                  </li>
                ))}
            </ul>

            <FlipMove className="video-container row row-cols-md-3">
              {dataVideos.map((video, index) => (
                <div
                  style={{ display: "block" }}
                  key={video.id}
                  className="col-md mb-6"
                  onClick={() => {
                    addVideoView(video.id);
                    setCurrentVideo(video);
                    setOpenIndex(index);
                  }}
                >
                  <a
                    className="card-hover-overlay rounded overflow-hidden d-block position-relative fancybox.iframe"
                    href={video.url_video}
                    data-fancybox
                    data-height="1110"
                  >
                    <div layout>
                      <img
                        src={PortalHelper.urlPublikasiLogo(video.gambar)}
                        className="img-fluid rounded-5 border w-100 h-210p"
                        disabled
                        alt="..."
                      />
                    </div>

                    <div className="center position-absolute hover-visible z-index-1">
                      <div className="mb-9 text-white">
                        <a className="btn rounded-circle bg-blue text-white">
                          <i className="fa fa-play"></i>
                        </a>
                      </div>
                    </div>
                    <div className="card-footer px-0 pt-3 pb-1">
                      <h5 className="fw-bolder line-clamp-2 mb-0">
                        {video.judul_video}
                      </h5>
                      <ul className="nav mx-n3 d-flex">
                        <li className="nav-item px-3 mb-md-0">
                          <span className="font-size-sm text-muted">
                            <i className="fa bi bi-calendar2-week me-1"></i>
                            <Moment format="DD MMMM YYYY" locale="id">
                              {video.tanggal_publish}
                            </Moment>
                          </span>
                        </li>
                        <li className="nav-item px-3 mb-md-0">
                          <span className="font-size-sm text-muted">
                            <i className="bi bi-eye me-1"></i>
                            {video.total_views} views
                          </span>
                        </li>
                      </ul>
                    </div>
                  </a>
                </div>
              ))}
            </FlipMove>
            {!loading && (dataVideos == null || dataVideos[0] == null) && (
              <TidakAdaData filter={filter} />
            )}
          </div>
        )}

        {/* Load More ================================================== */}
        {dataVideos && (
          <div className="d-flex justify-content-center align-items-center mt-9">
            <Pagination pageSize={metaData?.last_page} currentPage={page} />
          </div>
        )}
      </div>
    </div>
  );
}

Video.bodyBackground = "bg-white";

export default Video;
