import React, { useState, useEffect, useRef } from "react";
import Link from "next/link";
import PortalHelper from "../../src/helper/PortalHelper";
import PortalService from "../../services/PortalService";
import * as yup from "yup";
import { useForm, FormProvider } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import validationMessage from "../../src/validation-message";

/** Components */
import NeededLogin from "../../components/Modal/NeededLogin";
import ShareModal from "./../../components/Modal/ShareModal";
import ZonasiModal from "./../../components/Modal/ZonasiModal";
import { useRouter } from "next/router";

/** Libraries */
import Moment from "react-moment";
import "moment/locale/id";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import PelatihanService from "../../services/PelatihanService";
import ProfileService from "../../services/ProfileService";
import VerifikasiHandphone from "../../components/Modal/VerifikasiHandphonePelatihanModal";
import VerifikasiEmail from "../../components/Modal/VerifikasiEmailModal";
import eventBus from "../../components/EventBus";
import SimpleDateRange from "../../components/SimpleDateRange";
import NotificationHelper from "../../src/helper/NotificationHelper";
import ButtonWithLoading from "../../components/ButtonWithLoading";
import GeneralService from "../../services/GeneralService";
import SurveyMandatoryModalDownloadSertifikat from "../../components/Modal/SurveyMandatoryModalDs";
import FlipMove from "react-flip-move";
import moment from "moment/moment";
import { removeCookies } from "cookies-next";

const ModalBatalPelatihan = ({ idModal = "", onClose = () => {} }) => {
  // const [idModal] = useState(");
  const toggleOpenModal = useRef(null);
  const toggleCloseModal = useRef(null);
  const showSuccessModal = useRef(null);
  const [alasanPembatalanSel, setAlasanPembatalanSel] = useState(null);
  const [alasanBatalOpt, setAlasanBatalOpt] = useState(null);
  const [showPassword, setShowPassword] = useState(false);

  const yupSchemaValidation = yup.object({
    alasan: yup
      .string()
      .required(validationMessage.required.replace("__field", "Alasan")),
    password: yup
      .string()
      .required(validationMessage.required.replace("__field", "Password"))
      .min(
        8,
        validationMessage.minLength
          .replace("__field", "Password")
          .replace("__length", 8),
      ),
  });

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm({
    mode: "onBlur",
    resolver: yupResolver(yupSchemaValidation),
  });

  const [pelatihan, setPelatihan] = useState({
    nama_pelatihan: "",
    id: "",
    callBack: () => {},
  });

  const [show, setShow] = useState(false);

  useEffect(async () => {
    eventBus.on("setModalBatalPendaftaran", async function (res) {
      // console.log(res)
      setPelatihan(res);

      setValue("password", "");
      setValue("alasan", "");
      // fetch alasan
      try {
        const resp = await PelatihanService.fetchListAlasanBatal();
        if (resp.data.result.Status && resp.data.result.Data[0] != null) {
          setAlasanBatalOpt(
            resp.data.result.Data.map((elem) => {
              return {
                label: elem.name,
                value: elem.id,
              };
            }),
          );
        } else {
          throw Error(resp.data.result.Message);
        }
      } catch (err) {
        const message =
          typeof err == "string" ? err : err?.response?.data?.Message;
        NotificationHelper.Failed({
          message:
            message ?? "Terjadi kesalahan saat mengambil alasan pembatalan...",
        });
      }

      setShow(true);
    });

    // refresh userdata jika login

    if (PortalHelper.isLoggedin()) {
      try {
        let resp = await PortalService.refreshUserData();
        if (resp.data?.success) {
          PortalHelper.refreshUserCookies(resp.data.result?.data?.user);
        }
      } catch (err) {
        console.log(err);
      }
    }
  }, []);

  useEffect(() => {
    showSuccessModal.current.addEventListener("hidden.bs.modal", () => {
      onClose();
      setShow(false);
    });
    if (show && idModal) {
      toggleOpenModal.current?.click();
    } else {
      toggleCloseModal.current?.click();
    }
  }, [show]);

  const batalPelatihan = async (data) => {
    try {
      const resp = await PelatihanService.batalPendaftaran(
        pelatihan.id,
        data.alasan,
        data.password,
      );
      // console.log(resp)
      if (typeof pelatihan.callBack == "function") {
        pelatihan.callBack(resp);
      }
    } catch (err) {
      NotificationHelper.Failed({
        message:
          err.response.data.result ??
          "Terjadi kesalahaan, silahkan coba lagi nanti...",
      });
    }
  };

  return (
    <>
      <a
        href="#"
        ref={toggleOpenModal}
        data-bs-toggle="modal"
        data-bs-target={`#${idModal}`}
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseModal}
        data-bs-dismiss="modal"
        data-bs-target={`#${idModal}`}
        hidden
      ></a>

      <div
        className="modal fade"
        id={`${idModal}`}
        ref={showSuccessModal}
        tabIndex={-1}
        role="dialog"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <button
                type="button"
                className="close float-end"
                data-bs-dismiss="modal"
                aria-label="Close"
                onClick={() => {
                  onClose();
                }}
              >
                <span aria-hidden="true">
                  <i className="fa fa-times-circle text-muted fa-2x"></i>
                </span>
              </button>

              <h2
                className="fw-bold text-center mb-1 text-dark"
                id={`${idModal}Title`}
              >
                PERINGATAN
              </h2>

              <p className="font-size-lg text-center text-red fw-bold mb-0">
                Jika melakukan pembatalan, kamu tidak dapat lagi diproses untuk
                mengikuti pelatihan maupun mendaftar ulang pada pelatihan ini.
              </p>
              <p className="font-size-lg text-center text-muted mb-0">
                Apakah kamu yakin membatalkan pendaftaran pelatihan{" "}
                <span className="fw-bold font-size-lg">
                  {pelatihan.nama_pelatihan}
                </span>{" "}
                ?
              </p>
              <div className="mb-md-4 text-center">
                <img
                  src="/assets/img/illustrations/failed-illustration.jpg"
                  className="w-50 image-responsive"
                />
              </div>
              <div className="mb-md-4">
                <form onSubmit={handleSubmit(batalPelatihan)} id="alasan-batal">
                  <div className="mb-md-4 col-12">
                    <select
                      onChange={(e) => {
                        setAlasanPembatalanSel(e.target.value);
                      }}
                      disabled={!alasanBatalOpt}
                      className="form-select form-select-sm shadow-none text-primary rounded"
                      {...register("alasan")}
                    >
                      <option value="" hidden>
                        Pilih alasan
                      </option>
                      {alasanBatalOpt?.map((elem) => (
                        <option value={elem.value}>{elem.label}</option>
                      ))}
                    </select>
                    <span className="text-red">{errors.alasan?.message}</span>
                  </div>
                  <div className="form-group col-12 password-input">
                    <div className="input inner-addon right-addon">
                      <span
                        className="icon"
                        onClick={() => {
                          setShowPassword(!showPassword);
                        }}
                      >
                        <FlipMove>
                          {!showPassword && <i className="fa fa-eye "></i>}
                          {showPassword && <i className="fa fa-eye-slash "></i>}
                        </FlipMove>
                      </span>
                      <input
                        type={showPassword ? "text" : "password"}
                        className={`form-control rounded-3 form-control-sm ${
                          errors.password ? "is-invalid" : ""
                        }`}
                        name="password"
                        {...register("password")}
                        placeholder="Masukkan Password"
                      />
                    </div>
                    <span className="text-red">{errors.password?.message}</span>
                  </div>
                </form>
              </div>

              <div className="row">
                <div className="col-12">
                  <button
                    type="submit"
                    form="alasan-batal"
                    className="btn btn-block bg-red rounded-pill mt-3 lift text-white"
                    onClick={() => {
                      // batalPelatihan();
                    }}
                  >
                    BATALKAN PENDAFTARAN
                  </button>
                </div>
                <div className="col-12">
                  <button
                    className="btn btn-block mt-2"
                    onClick={() => {
                      setShow(false);
                    }}
                  >
                    TIDAK
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
function pelatihanDetailPage({ showLoadingModal }) {
  const router = useRouter();

  /** Data */
  const [PelatihanDetail, setPelatihanDetail] = useState([]);
  const [PelatihanBookmark, setPelatihanBookmark] = useState([]);
  const [PelatihanRelated, setPelatihanRelated] = useState([]);
  const [PelatihanByUser, setPelatihanByUser] = useState(null);
  const [loading, setLoading] = useState(false);
  const [unverfiedMessage, setUnverifiedMessage] = useState([]);
  const [isAutoRegist, setIsAutoRegist] = useState(false);
  const [timeDiff, setTimeDiff] = useState(0);
  const pelatihanID = router.query.pelatihanID;

  const [showNeededLoginModal, setShowNeededLoginModal] = useState(false);
  const [showShareModal, setShowShareModal] = useState(false);
  const [serverMessage, setServerMessage] = useState("");
  const [showVerifikasiModal, setShowVerifikasiModal] = useState(false);
  const [showEmailVerifikasiModal, setShowEmailVerifikasiModal] =
    useState(false);
  const [otpErrorMessage, setOtpErrorMessage] = useState("");
  const [showZonasiModal, setShowZonasiModal] = useState(false);

  const [user, setUser] = useState(PortalHelper.getUserData());

  const [loadingButton, setLoadingButton] = useState(false);
  const [pelatihanByUserLoading, setPelatihanByUserLoading] = useState(false);

  const closeNeededLoginModal = () => {
    setLoading(false);
    setShowNeededLoginModal(false);
    setLoadingButton(false);
  };

  const handleShareClose = () => {
    setShowShareModal(false);
  };

  const checkPelatihanByUser = async () => {
    /** Cek apakah user ini sudah mendaftar atau belum */
    setPelatihanByUserLoading(true);
    const resPelatihanByUser = await ProfileService.pelatihan(pelatihanID);
    setPelatihanByUserLoading(false);
    if (resPelatihanByUser.data.success) {
      setPelatihanByUser(resPelatihanByUser.data.result.data);
    }
  };

  const checkPelatihanSurvey = async () => {
    try {
      setLoadingButton(true);
      if (!PortalHelper.isLoggedin()) {
        PortalHelper.saveLastViewPelatihanCookie(pelatihanID);
        setServerMessage("Anda belum login");
        setShowNeededLoginModal(true);
        return;
      }
      const resp = await PortalService.checkHistoryPelatihan();
      // console.log(resp)
      if (resp.data.success) {
        if (resp.data.result.data?.length > 0) {
          const curDate = moment(timeDiff.serverTime);
          const uniqueId = [];
          // cek list survey yang belum dikerjakan
          // const listSurvey = resp.data.result.data;
          const surveyBelumDikerjakan = _.filter(
            resp.data.result.data,
            function (survey) {
              if (
                (survey.status_mengerjakan_id == null ||
                  survey.status_mengerjakan_id == 0) &&
                survey.training_id != null &&
                !uniqueId.includes(survey.training_id)
              ) {
                uniqueId.push(survey.training_id);
                return true;
              } else {
                return false;
              }
            },
          );

          if (surveyBelumDikerjakan.length > 0) {
            const surveysPelatihan = _.filter(
              surveyBelumDikerjakan,
              function (survey) {
                return curDate.isSameOrBefore(moment(survey.survey_selesai));
              },
            );

            if (surveysPelatihan.length > 0) {
              eventBus.dispatch("showMandatoryDS", {
                show: true,
                surveys: surveysPelatihan.map((survey) => ({
                  nama: survey.pelatihan_nama + " - " + survey.survey_name,
                  id: survey.survey_id,
                  type: "survey",
                  pelatian_id: survey.training_id,
                  question_bank_id: survey.survey_id,
                })),
              });
            } else {
              applyPelatihan();
            }
          } else {
            applyPelatihan();
          }
        } else {
          applyPelatihan();
        }
      }

      // return
    } catch (error) {
      setLoadingButton(false);
      NotificationHelper.Failed({ message: error.toString() });
    }
  };

  const applyPelatihan = async () => {
    const userData = PortalHelper.getUserData();
    setLoadingButton(true);
    if (!userData?.email_verifikasi) {
      try {
        const sendResponse = await PortalService.sendEmailOtpAdmin();
        if (sendResponse.data.success) {
          setShowEmailVerifikasiModal(true);
        } else {
          NotificationHelper.Failed({
            message:
              sendResponse.data.message ??
              "Terjadi kesalahan saat mengirim email!",
          });
        }
      } catch (err) {
        NotificationHelper.Failed({
          message: err.message ?? "Terjadi kesalahan saat mengirim email!",
        });
      }
    } else if (!userData?.handphone_verifikasi) {
      setShowVerifikasiModal(true);
      setIsAutoRegist(true);
    } else {
      router.push(`/pelatihan/${pelatihanID}/daftar`);
    }
    setLoadingButton(false);
  };

  const getTimeDiff = async () => {
    const resp = await GeneralService.getServerTime();
    setTimeDiff(resp);
  };

  const clickBookmark = async (e) => {
    if (!PortalHelper.isLoggedin()) {
      setServerMessage("Anda belum login");
      setShowNeededLoginModal(true);
    } else {
      showLoadingModal(true);
      const resUpdate =
        await PelatihanService.bookmarkByPelatihanUpdate(pelatihanID);
      const resPelatihanBookmark =
        await PelatihanService.bookmarkByPelatihan(pelatihanID);
      setPelatihanBookmark(resPelatihanBookmark.data.result.data);
      showLoadingModal(false);
    }
  };

  const resendEmail = async () => {
    try {
      const resp = await PortalService.sendEmailOtpAdmin(email);
      if (!resp.data.success) {
        throw Error(resp.data.result.message);
      }
    } catch (err) {
      const message =
        typeof err == "string" ? err : err?.response?.data?.message;
      NotificationHelper.Failed({
        message: message ?? "Terjadi kesalahan saat mengirim email!",
      });
      setServerMessage(message ?? "Terjadi kesalahan.");
      // setShowFailModal(true);
    }
  };

  useEffect(async () => {
    try {
      setLoading(true);
      if (router.isReady) {
        const resPelatihanDetail =
          await PortalService.pelatihanDetail(pelatihanID);
        const resPelatihanBookmark =
          await PelatihanService.bookmarkByPelatihan(pelatihanID);
        const resPelatihanRelated = await PortalService.pelatihanRelated(
          resPelatihanDetail.data.result.data?.slug_akademi,
        );
        const resp = await GeneralService.getServerTime(true);

        if (!resPelatihanDetail.data.result.data) {
          throw Error("Pelatihan tidak ditemukan!");
        }
        const pelatihanDetail = {
          ...resPelatihanDetail.data.result.data,
          diff: resp.diff,
          current_server_time: resp.serverTime,
        };
        setPelatihanDetail(pelatihanDetail);
        setTimeDiff(resp);
        setPelatihanBookmark(resPelatihanBookmark.data.result.data);
        const related = resPelatihanRelated.data.result.data?.filter(
          (pelatihan) => resPelatihanDetail.data.result.data.id != pelatihan.id,
        );
        setPelatihanRelated(related);

        PortalHelper.savePelatihanToCookie(resPelatihanDetail.data.result.data);

        if (PortalHelper.isLoggedin()) {
          checkPelatihanByUser();
        }
        setLoading(false);
        // setIsloggedIn(PortalHelper.isLoggedin());
      }
    } catch (error) {
      setLoading(false);
      console.error(error);
      NotificationHelper.Failed({
        message: error.message,
        onClose: () => {
          router.back();
        },
      });
    }
  }, [router.isReady, pelatihanID]);

  useEffect(() => {
    if (PortalHelper.isLoggedin()) {
      removeCookies("LSPlID", { path: "/" });
    }
  }, []);

  const totalJPSilabus = () => {
    let jp = PelatihanDetail?.silabus_list?.[0].jml_jp;
    jp = jp ? parseFloat(jp) : 0;
    // remove zero decimal
    return 1 * jp.toFixed(1);
  };

  return (
    <div>
      <ModalBatalPelatihan idModal="modalBatalPelatihanPendaftaran"></ModalBatalPelatihan>
      <SurveyMandatoryModalDownloadSertifikat
        body="Hai Digiers, sebelum mendaftar pelatihan kamu diwajibkan untuk
                mengisi survei evaluasi berikut."
        onClose={() => {
          setLoadingButton(false);
        }}
      />
      <ShareModal
        handleClose={handleShareClose}
        show={showShareModal}
        title="Bagikan pelatihan ini!"
      />
      <ZonasiModal
        show={showZonasiModal}
        handleClose={() => setShowZonasiModal(false)}
        zonasi={PelatihanDetail.pelatihan_provinsi}
      />
      {loading && (
        <div>
          <header
            className="py-6 bg-blue mb-6 z-index-0"
            style={{ backgroundImage: "none" }}
          >
            <div className="container">
              <div className="row align-items-end" data-aos="fade-up">
                {/* <nav aria-label="breadcrumb">
                  <ol className="breadcrumb breadcrumb-scroll">
                    <li className="breadcrumb-item">
                      <Skeleton />
                    </li>
                    <li className="breadcrumb-item">
                      <Skeleton />
                    </li>
                    <li
                      className="breadcrumb-item text-blue active"
                      aria-current="page"
                    >
                      <Skeleton />
                    </li>
                  </ol>
                </nav>*/}
                <div className="col-md mb-md-0">
                  <Skeleton count={2} />
                </div>
              </div>

              <div className="mt-8">
                <Skeleton className="h-50p"></Skeleton>
              </div>
            </div>
          </header>
          <div className="container">
            <div className="row mb-8">
              <div className="col-lg-8 mb-6 mb-lg-0 position-relative">
                <div className="mb-4 mb-md-0">
                  <Skeleton />
                </div>
                <h2 className="me-xl-14">
                  <Skeleton />
                </h2>
                <div className="row mt-6">
                  <div className="col-6 col-lg-6">
                    <h6 className="mb-0">
                      <Skeleton />
                    </h6>
                    <h6 className="text-primary">
                      <Skeleton />
                    </h6>
                  </div>
                  <div className="col-6 col-lg-6">
                    <h6 className="mb-0">
                      <Skeleton />
                    </h6>
                    <h6 className="text-primary">
                      <Skeleton />
                    </h6>
                  </div>
                </div>
                <hr />

                <div className="row mt-6">
                  <div className="col-6 col-lg-6 mb-6 px-5">
                    <div className="card icon-category icon-category-sm">
                      <div className="row align-items-center">
                        <div className="col px-3">
                          <div className="card-body p-0">
                            <h6 className="mb-0 line-clamp-1">
                              <Skeleton />
                            </h6>
                            <p className="mb-0 line-clamp-1">
                              <Skeleton />
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-6 col-lg-6 mb-6 px-5">
                    <div className="card icon-category icon-category-sm">
                      <div className="row align-items-center">
                        <div className="col px-3">
                          <div className="card-body p-0">
                            <h6 className="mb-0 line-clamp-1">
                              <Skeleton />
                            </h6>
                            <p className="mb-0 line-clamp-1">
                              <Skeleton />
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="my-5">
                  <h3>
                    <Skeleton />
                  </h3>
                  <div>
                    <Skeleton count={3} />
                  </div>
                </div>
              </div>

              <div className="col-lg-4">
                <Skeleton className="h-500p"></Skeleton>
              </div>
            </div>
          </div>

          <section className="bg-white py-11">
            <div className="container">
              <div
                className="row align-items-end mb-4 mb-md-7"
                data-aos="fade-up"
              >
                <div className="col-md mb-4 mb-md-0">
                  <h1 className="mb-1">
                    <Skeleton />
                  </h1>
                  <p className="font-size-lg mb-0 text-capitalize">
                    <Skeleton />
                  </p>
                </div>
              </div>

              <div className="row row-cols-lg-2">
                <div className="col-lg mb-5 mb-md-6">
                  <Skeleton className="h-100p"></Skeleton>
                </div>
                <div className="col-lg mb-5 mb-md-6">
                  <Skeleton className="h-100p"></Skeleton>
                </div>
                <div className="col-lg mb-5 mb-md-6">
                  <Skeleton className="h-100p"></Skeleton>
                </div>
                <div className="col-lg mb-5 mb-md-6">
                  <Skeleton className="h-100p"></Skeleton>
                </div>
              </div>
            </div>
          </section>
        </div>
      )}

      {!loading && (
        <div>
          <NeededLogin
            show={showNeededLoginModal}
            handleClose={closeNeededLoginModal}
            children={serverMessage}
          />

          {/* {unverfiedMessage.length > 0 && (
            <AlertWarning
              redirect={"/pelatihan/" + PelatihanDetail.id + "/daftar"}
              unverfiedMessage={unverfiedMessage}
              onPhoneClick={() => {
                setShowVerifikasiModal(true);
                setIsAutoRegist(false);
              }}
            />
          )} */}
          {/* {unverfiedMessage.length > 0 && (
            <div className="alert bg-red rounded-0 mb-0" role="alert">
              <div className="container">
                <div className="row text-white mb-md-0">
                  <div className="row align-items-center">
                    <div className="col-auto px-3">
                      <i className="fa fa-exclamation-circle img-fluid fa-3x"></i>
                    </div>
                    <div className="col px-3">
                      <h5 className="text-white mb-0">
                        Hai Digiers, untuk dapat melakukan pendaftaran pelatihan
                        silahakan lakukan verifikasi data berikut:
                        <br />{" "}
                        {unverfiedMessage.map((msg) => (
                          <>
                            {msg == "NIK" ? (
                              <Link
                                href={`/verify-user/nik?data=${EncryptionHelper.encrypt(
                                  "/pelatihan/" + PelatihanDetail.id + "/daftar"
                                )}`}
                              >
                                <a className="btn btn-sm btn-outline-light rounded-pill font-size-14 my-1 py-0 px-2">
                                  <i className="fa fa-id-card me-1"></i>
                                  Verifikasi {msg}
                                </a>
                              </Link>
                            ) : (
                              <>
                                {unverfiedMessage.length > 1 && (
                                  <span>{<br />}</span>
                                )}
                                <a
                                  href="#"
                                  className="btn btn-sm btn-outline-light rounded-pill font-size-14 my-1 py-0 px-2"
                                  onClick={(e) => {
                                    setShowVerifikasiModal(true);
                                    setIsAutoRegist(false);
                                  }}
                                >
                                  <i className="fa fa-phone-alt me-1"></i>
                                  Verifikasi {msg}
                                </a>
                              </>
                            )}
                          </>
                        ))}{" "}
                      </h5>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )} */}

          {/* <div className="modal fade" id="zonasi" tabIndex="-1" role="dialog">
            <div className="modal-dialog modal-dialog-centered" role="document">
              <div className="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">
                    Zonasi
                  </h5>
                  <button
                    type="button"
                    class="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                  ></button>
                </div>
                <div className="modal-body mt-0">
                  <h5 className="text-center">
                    {PelatihanDetail.pelatihan_provinsi?.map((Provinsi) => (
                      <span class="mx-1 badge bg-blue rounded-pill">
                        {Provinsi.nama_provinsi}
                      </span>
                    ))}
                  </h5>
                </div>
              </div>
            </div>
          </div> */}

          <header className="py-6 bg-blue mb-6 z-index-0">
            <div className="container">
              <div className="row">
                <div className="col-lg-9 mb-0 position-relative">
                  <div className="mb-0 pt-5 me-6">
                    <h6 className="text-white mb-0">
                      {PelatihanDetail.namasub_akademi} (
                      {PelatihanDetail.nama_akademi}) -{" "}
                      {PelatihanDetail.nama_tema}
                    </h6>
                  </div>
                  <h2 className="text-white fw-bolder me-xl-14">
                    {PelatihanDetail.nama_pelatihan} - Batch{" "}
                    {PelatihanDetail.batch}
                  </h2>
                  {!PelatihanBookmark && (
                    <>
                      <a
                        className="badge badge-lg badge-rounded-circle border bg-white font-size-base badge-float badge-float-inside top-0"
                        style={{ color: "#de4d6e" }}
                        onClick={clickBookmark}
                      >
                        <i className="far fa-heart"></i>
                      </a>
                    </>
                  )}
                  {PelatihanBookmark && (
                    <>
                      <a
                        className="badge badge-lg badge-rounded-circle badge-danger font-size-base badge-float badge-float-inside top-0"
                        style={{ color: "#de4d6e" }}
                        onClick={clickBookmark}
                      >
                        <i className="fa fa-heart"></i>
                      </a>
                    </>
                  )}
                  <hr className="border" />
                  <div className="row mt-6">
                    <div className="col-md-4">
                      <h5 className="fw-bolder mb-0 text-white">Lokasi</h5>
                      <h6 className="fw-bolder text-white">
                        <i className="bi bi-geo-alt me-1"></i>
                        {PelatihanDetail.metode_pelatihan == "Online"
                          ? "Dilaksanakan secara Online"
                          : "" + PelatihanDetail.nama_kabupaten}
                      </h6>
                    </div>
                    <div className="col-md-4">
                      <h5 className="fw-bolder mb-0 text-white">
                        Tgl. Pendaftaran
                      </h5>
                      <h6 className="fw-bolder text-white">
                        <i className="bi bi-calendar2-week me-2"></i>
                        <SimpleDateRange
                          startDate={PelatihanDetail.pendaftaran_mulai}
                          endDate={PelatihanDetail.pendaftaran_selesai}
                          diff={timeDiff.diff}
                        ></SimpleDateRange>
                      </h6>
                    </div>
                    <div className="col-md-4">
                      <h5 className="fw-bolder text-white mb-0">
                        Tgl. Pelatihan
                      </h5>
                      <h6 className="fw-bolder text-white">
                        <i className="bi bi-journal-bookmark-fill me-2"></i>
                        <SimpleDateRange
                          startDate={PelatihanDetail.pelatihan_mulai}
                          endDate={PelatihanDetail.pelatihan_selesai}
                          diff={timeDiff.diff}
                        ></SimpleDateRange>
                      </h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </header>

          <div className="container">
            <div className="row mb-9">
              <div className="col-lg-8 mb-6 mb-lg-0 position-relative">
                <div className="row mt-6">
                  {PelatihanDetail.metode_pelaksanaan == "Mitra" && (
                    <div className="col-md-6 mb-6 px-5">
                      <div className="card icon-category icon-category-sm">
                        <div className="row align-items-center">
                          <div className="col-auto px-3">
                            <div className="avatar avatar-custom d-inline">
                              <img
                                src={
                                  PelatihanDetail.metode_pelaksanaan ==
                                  "Swakelola"
                                    ? "/assets/img/kominfo.png"
                                    : PortalHelper.urlMitraLogo(
                                        PelatihanDetail.logo,
                                      )
                                }
                                alt="..."
                                class="avatar-img avatar-md img-fluid"
                                style={{ objectFit: "contain" }}
                              />
                            </div>
                          </div>

                          <div className="col px-3">
                            <div className="card-body p-0">
                              <h6 className="mb-0 line-clamp-1">Mitra</h6>
                              <p className="mb-0 line-clamp-1">
                                {PelatihanDetail.nama_mitra}
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                  <div className="col-md-6 mb-6 px-5">
                    <div className="card icon-category icon-category-sm">
                      <div className="row align-items-center">
                        <div className="col-auto px-3">
                          <div className="avatar avatar-custom d-inline">
                            <img
                              src="/assets/img/kominfo.png"
                              alt="{ PelatihanDetail.nama_penyelenggara }"
                              className="avatar-img avatar-md img-fluid"
                            />
                          </div>
                        </div>

                        <div className="col px-3">
                          <div className="card-body p-0">
                            <h6 className="mb-0 line-clamp-1">Penyelenggara</h6>
                            <p className="mb-0 line-clamp-1">
                              {PelatihanDetail.nama_penyelenggara}
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="my-5">
                  <h4 className="fw-bold mb-0">Deskripsi Pelatihan</h4>
                  <div
                    className="post-body"
                    dangerouslySetInnerHTML={{
                      __html: PelatihanDetail.deskripsi,
                    }}
                  ></div>
                  {PelatihanDetail?.silabus_list?.length != null && (
                    <div className="my-5">
                      <h4 className="fw-bold mb-0">
                        Silabus
                        {totalJPSilabus() != 0
                          ? " (" + totalJPSilabus() + " JP)"
                          : ""}
                      </h4>
                      <ul className="course-timeline">
                        {PelatihanDetail?.silabus_list?.map((list) => {
                          let jp = parseFloat(list.jam_pelajaran);
                          // remove zero decimal
                          jp = 1 * jp.toFixed(1);

                          return (
                            <li>
                              <div>{list.name}</div>
                              <div
                                className="duration"
                                title={jp + " Jam Pelajaran"}
                              >
                                <i className="bi bi-stopwatch"></i> {jp} JP
                              </div>
                            </li>
                          );
                        })}
                      </ul>
                    </div>
                  )}
                  <div className="my-5">
                    <h4 className="fw-bold mb-0">Alur Seleksi</h4>
                    <p
                      className="post-body"
                      dangerouslySetInnerHTML={{
                        __html: PelatihanDetail.alur_pendaftaran,
                      }}
                    ></p>
                  </div>
                </div>
                {PelatihanDetail?.metode_pelatihan?.includes("Offline") && (
                  <div className="mb-5">
                    <h4 className="fw-bold mb-0">Lokasi Pelatihan</h4>
                    <div className="">
                      <p>
                        <span
                          className="alamat-m0"
                          dangerouslySetInnerHTML={{
                            __html: PelatihanDetail.alamat,
                          }}
                        ></span>
                        <p className="mb-0 pb-0">
                          {`${PortalHelper.titleCase(
                            PelatihanDetail.nama_kabupaten,
                          )} - ${PortalHelper.titleCase(
                            PelatihanDetail.lokasi_pelatihan,
                          )}`}
                        </p>
                      </p>
                    </div>
                  </div>
                )}
              </div>

              <div className="col-lg-4 mt-lg-n12">
                <div className="d-block rounded-5 border p-2 bg-white mb-6">
                  <div className="pt-6 pb-5 px-4">
                    <div className="d-flex align-items-center">
                      <div className="mb-4 mb-md-0 me-md-8 me-lg-4 me-xl-8">
                        {(PortalHelper.isPelatihanOpen(
                          PelatihanDetail.pendaftaran_mulai,
                          PelatihanDetail.pendaftaran_selesai,
                          timeDiff.serverTime,
                        ) == 1 ||
                          PortalHelper.isPelatihanOpen(
                            PelatihanDetail.pendaftaran_mulai,
                            PelatihanDetail.pendaftaran_selesai,
                            timeDiff.serverTime,
                          ) == 2) && (
                          <div>
                            <h6 className="mb-0">Batas Pendaftaran</h6>
                            <h4 className="text-primary fw-bolder">
                              <Moment
                                locale="id"
                                format="DD MMMM YYYY"
                                add={{ minutes: timeDiff.diff }}
                              >
                                {PelatihanDetail.pendaftaran_selesai}
                              </Moment>
                            </h4>
                          </div>
                        )}
                        {PortalHelper.isPelatihanOpen(
                          PelatihanDetail.pendaftaran_mulai,
                          PelatihanDetail.pendaftaran_selesai,
                          timeDiff.serverTime,
                        ) == 0 && (
                          <div>
                            <h6 className="mb-0">Pendaftaran Dimulai</h6>
                            <h4 className="text-primary">
                              <Moment
                                locale="id"
                                format="DD MMMM YYYY"
                                add={{ minutes: timeDiff.diff }}
                              >
                                {PelatihanDetail.pendaftaran_mulai}
                              </Moment>
                            </h4>
                          </div>
                        )}
                      </div>

                      {PortalHelper.isPelatihanOpen(
                        PelatihanDetail.pendaftaran_mulai,
                        PelatihanDetail.pendaftaran_selesai,
                        timeDiff.serverTime,
                      ) == 1 && (
                        <div className="badge badge-pill badge-lg bg-green text-white ms-auto fw-normal">
                          Buka
                        </div>
                      )}

                      {PortalHelper.isPelatihanOpen(
                        PelatihanDetail.pendaftaran_mulai,
                        PelatihanDetail.pendaftaran_selesai,
                        timeDiff.serverTime,
                      ) == 2 && (
                        <div className="badge badge-pill badge-lg bg-red text-white ms-auto fw-normal">
                          Tutup
                        </div>
                      )}
                    </div>

                    {PortalHelper.oneWeekNoticeBefore(
                      PelatihanDetail.pendaftaran_selesai,
                      timeDiff.serverTime,
                    ) &&
                      PortalHelper.isPelatihanOpen(
                        PelatihanDetail.pendaftaran_mulai,
                        PelatihanDetail.pendaftaran_selesai,
                        timeDiff.serverTime,
                      ) == 1 && (
                        <div className="d-flex align-items-center text-alizarin mb-6">
                          <i className="fa fa-bell"></i>

                          <span className="ms-2">
                            Tutup{" "}
                            {-1 *
                              PortalHelper.dateDifference(
                                PelatihanDetail.pendaftaran_selesai,
                                timeDiff.serverTime,
                              )}{" "}
                            hari lagi!
                          </span>
                        </div>
                      )}
                    {pelatihanByUserLoading && (
                      <>
                        <Skeleton
                          className="w-100 rounded-pill"
                          height="50px"
                        ></Skeleton>
                      </>
                    )}

                    {!loading && PelatihanDetail.sisa_kuota <= 0 ? (
                      <button
                        className="btn btn-blue rounded-pill btn-block my-3"
                        style={{ backgroundColor: "#db3e3e" }}
                      >
                        KUOTA PENUH
                      </button>
                    ) : PelatihanDetail.status_kuota == 0 ||
                      PortalHelper.isPelatihanOpen(
                        PelatihanDetail.pendaftaran_mulai,
                        PelatihanDetail.pendaftaran_selesai,
                        timeDiff.serverTime,
                      ) == 0 ? (
                      <button
                        className="btn btn-blue rounded-pill btn-block my-3"
                        style={{ backgroundColor: "#db3e3e" }}
                      >
                        TUTUP
                      </button>
                    ) : !pelatihanByUserLoading &&
                      PortalHelper.isPelatihanOpen(
                        PelatihanDetail.pendaftaran_mulai,
                        PelatihanDetail.pendaftaran_selesai,
                        timeDiff.serverTime,
                      ) == 1 &&
                      !PelatihanByUser ? (
                      <ButtonWithLoading
                        className="btn btn-blue rounded-pill btn-block my-3"
                        type="button"
                        name="button"
                        onClick={() => {
                          checkPelatihanSurvey();
                        }}
                        loading={loadingButton}
                      >
                        DAFTAR SEKARANG
                      </ButtonWithLoading>
                    ) : (
                      ""
                    )}

                    {!loading &&
                      !pelatihanByUserLoading &&
                      PelatihanByUser &&
                      PortalHelper.getUserData()?.lengkapi_pendaftaran?.length >
                        0 &&
                      (PelatihanByUser.status_peserta.toLowerCase() !=
                        "pembatalan" ||
                        PelatihanByUser.status_peserta?.toLowerCase() !=
                          "mengundurkan diri") &&
                      user?.wizard == 1 && (
                        <>
                          {PortalHelper.getUserData()?.handphone_verifikasi &&
                          PortalHelper.getUserData()?.email_verifikasi ? (
                            <>
                              <a
                                href="javascript:void(0)"
                                // target="_blank"
                                className="btn btn-green rounded-pill btn-block my-3"
                                onClick={() => {
                                  if (!PelatihanByUser.file_path) {
                                    router.push({
                                      pathname: `/pelatihan/${PelatihanByUser.pelatian_id}/daftar`,
                                    });
                                  } else {
                                    PortalHelper.axiosDownloadFile(
                                      process.env.STORAGE_URL +
                                        "dts-dev/" +
                                        PelatihanByUser.file_path,
                                      `Bukti Pendaftaran_${user.nama}_${PelatihanDetail.nama_pelatihan}`,
                                    );
                                  }
                                }}
                              >
                                <i className="bi bi-receipt me-1"></i>DOWNLOAD
                                BUKTI PENDAFTARAN
                              </a>
                              {PortalHelper.isCancelPelatihanAvailabel(
                                PelatihanByUser.pendaftaran_mulai,
                                PelatihanByUser.pendaftaran_selesai,
                                PelatihanByUser.status_peserta,
                              ) && (
                                <a
                                  href="javascript:void(0)"
                                  // target="_blank"
                                  className="btn bg-red rounded-pill text-white btn-block my-3"
                                  onClick={() => {
                                    eventBus.dispatch(
                                      "setModalBatalPendaftaran",
                                      {
                                        nama_pelatihan:
                                          PelatihanDetail.nama_pelatihan,
                                        id: PelatihanDetail.id,
                                        callBack: function () {
                                          router.reload();
                                        },
                                      },
                                    );
                                  }}
                                >
                                  <i className="bi bi-exclamation-octagon me-1"></i>
                                  BATALKAN PENDAFTARAN
                                </a>
                              )}
                            </>
                          ) : (
                            <>
                              {!PortalHelper.getUserData()
                                ?.handphone_verifikasi && (
                                <a
                                  href="#"
                                  // target="_blank"
                                  className="btn bg-primary rounded-pill text-white btn-block my-3"
                                  onClick={(e) => {
                                    checkPelatihanSurvey();
                                  }}
                                >
                                  <i className="bi bi-exclamation-octagon me-1"></i>
                                  LENGKAPI PENDAFTARAN
                                </a>
                              )}

                              {!PortalHelper.getUserData()
                                ?.handphone_verifikasi && (
                                <a
                                  href="#"
                                  // target="_blank"
                                  className="btn bg-orange rounded-pill text-white btn-block my-3"
                                  onClick={(e) => {
                                    setShowVerifikasiModal(true);
                                    setIsAutoRegist(false);
                                  }}
                                >
                                  <i className="bi bi-exclamation-octagon me-1"></i>
                                  VERIFIKASI NOMOR HANDPHONE
                                </a>
                              )}

                              {!PortalHelper.getUserData()
                                ?.email_verifikasi && (
                                <ButtonWithLoading
                                  className="btn bg-orange rounded-pill text-white btn-block my-3"
                                  loading={loadingButton}
                                  onClick={async (e) => {
                                    setLoadingButton(true);
                                    try {
                                      setIsAutoRegist(false);
                                      const sendResponse =
                                        await PortalService.sendEmailOtpAdmin();
                                      setLoadingButton(false);
                                      if (sendResponse.data.success) {
                                        setShowEmailVerifikasiModal(true);
                                      } else {
                                        NotificationHelper.Failed({
                                          message:
                                            sendResponse.data.message ??
                                            "Terjadi kesalahan saat mengirim email!",
                                        });
                                      }
                                    } catch (err) {
                                      NotificationHelper.Failed({
                                        message:
                                          err.message ??
                                          "Terjadi kesalahan saat mengirim email!",
                                      });
                                    }
                                  }}
                                >
                                  <i className="bi bi-exclamation-octagon me-1"></i>
                                  VERIFIKASI EMAIL
                                </ButtonWithLoading>
                              )}
                            </>
                          )}
                        </>
                      )}
                    {!loading &&
                      PelatihanByUser &&
                      (PelatihanByUser.status_peserta.toLowerCase() !=
                        "pembatalan" ||
                        PelatihanByUser.status_peserta?.toLowerCase() !=
                          "mengundurkan diri") &&
                      (!user?.wizard || user?.wizard == 0) && (
                        <Link href="/verify-user/complete-user-data">
                          <a
                            // target="_blank"
                            className="btn btn-primary btn-block my-3"
                          >
                            LENGKAPI WIZARD
                          </a>
                        </Link>
                      )}

                    <ul className="list-group list-group-flush mt-6">
                      <li className="list-group-item d-flex align-items-center py-3">
                        <div className="text-secondary d-flex icon-uxs">
                          <i className="bi bi-book"></i>
                        </div>
                        <h6 className="mb-0 ms-3 me-auto">Metode</h6>
                        <span>{PelatihanDetail.metode_pelatihan}</span>
                      </li>
                      <li className="list-group-item d-flex align-items-center py-3">
                        <div className="text-secondary d-flex icon-uxs">
                          <i className="fa fa-recycle"></i>
                        </div>
                        <h6 className="mb-0 ms-3 me-auto">Alur Seleksi</h6>
                        <span>{PelatihanDetail.alur_pendaftaran}</span>
                      </li>
                      <li className="list-group-item d-flex align-items-center py-3">
                        <div className="text-secondary d-flex icon-uxs">
                          <i className="bi bi-shield-check"></i>
                        </div>
                        <h6 className="mb-0 ms-3 me-auto">Level</h6>
                        <span>{PelatihanDetail.level_pelatihan}</span>
                      </li>
                      <li className="list-group-item d-flex align-items-center py-3">
                        <div className="text-secondary d-flex icon-uxs">
                          <i className="bi bi-people"></i>
                        </div>
                        <h6 className="mb-0 ms-3 me-auto">Kuota Peserta</h6>
                        <span>{PelatihanDetail.kuota} orang</span>
                      </li>
                      <li className="list-group-item d-flex align-items-center py-3">
                        <div className="text-secondary d-flex icon-uxs">
                          <i className="bi bi-patch-check"></i>
                        </div>
                        <h6 className="mb-0 ms-3 me-auto">Sertifikasi</h6>
                        <span>{PelatihanDetail.sertifikasi}</span>
                      </li>
                      <li className="list-group-item d-flex align-items-center py-3">
                        <div className="text-secondary d-flex icon-uxs">
                          <i className="bi bi-globe-central-south-asia"></i>
                        </div>
                        <h6 className="mb-0 ms-3 me-auto">Zonasi</h6>
                        <a
                          className="nav-link py-0"
                          href="#"
                          onClick={(e) => {
                            e.preventDefault();
                            setShowZonasiModal(true);
                          }}
                        >
                          Lihat
                        </a>
                      </li>
                      <li className="list-group-item d-flex align-items-center py-3">
                        <div className="text-secondary d-flex icon-uxs">
                          <i className="bi bi-geo-alt"></i>
                        </div>
                        <h6 className="mb-0 ms-3 me-auto">Lokasi Pelatihan</h6>
                        <span>{PelatihanDetail.lokasi_pelatihan}</span>
                      </li>
                      {PelatihanDetail.substansi_mulai ? (
                        <>
                          <li className="list-group-item d-flex align-items-center py-3">
                            <div className="text-secondary d-flex icon-uxs">
                              <i className="bi bi-calendar-check"></i>
                            </div>
                            <h6 className="mb-0 ms-3 me-auto">
                              Waktu Tes Substansi
                            </h6>
                            <span>
                              <Moment
                                locale="id"
                                format="DD MMMM YYYY"
                                add={{ minutes: timeDiff.diff }}
                              >
                                {PelatihanDetail.substansi_mulai}
                              </Moment>
                            </span>
                          </li>
                        </>
                      ) : (
                        <></>
                      )}
                      <li className="list-group-item d-flex align-items-center py-3">
                        <div className="text-secondary d-flex icon-uxs">
                          <i className="bi bi-journal-bookmark-fill"></i>
                        </div>
                        <h6 className="mb-0 ms-3 me-auto">
                          Tgl. Mulai Pelatihan
                        </h6>
                        <span>
                          <Moment
                            locale="id"
                            format="DD MMMM YYYY"
                            add={{ minutes: timeDiff.diff }}
                          >
                            {PelatihanDetail.pelatihan_mulai}
                          </Moment>
                        </span>
                      </li>
                      <li className="list-group-item d-flex align-items-center py-3">
                        <div className="text-secondary d-flex icon-uxs">
                          <i className="bi bi-journal-check"></i>
                        </div>
                        <h6 className="mb-0 ms-3 me-auto">
                          Tgl. Selesai Pelatihan
                        </h6>
                        <span>
                          <Moment
                            locale="id"
                            format="DD MMMM YYYY"
                            add={{ minutes: timeDiff.diff }}
                          >
                            {PelatihanDetail.pelatihan_selesai}
                          </Moment>
                        </span>
                      </li>
                      <li className="list-group-item d-flex align-items-center py-3">
                        <div className="text-secondary d-flex icon-uxs">
                          <i className="bi bi-cloud-download"></i>
                        </div>
                        <h6 className="mb-0 ms-3 me-auto">Silabus</h6>
                        <span>
                          <a
                            href="#"
                            onClick={(e) => {
                              e.preventDefault();
                              PortalHelper.axiosDownloadFile(
                                PortalHelper.urlBrosur(
                                  PelatihanDetail.silabus?.replace(
                                    "/uploads",
                                    "",
                                  ),
                                ),
                                "Silabus_" + PelatihanDetail.nama_pelatihan,
                              );
                            }}
                          >
                            Download
                          </a>
                        </span>
                      </li>
                      <li className="list-group-item d-flex align-items-center py-3">
                        <div className="text-secondary d-flex icon-uxs">
                          <i className="bi bi-eye"></i>
                        </div>
                        <h6 className="mb-0 ms-3 me-auto">Dilihat sebanyak</h6>
                        <span>{PelatihanDetail.jumlah_pengunjung}</span>
                      </li>
                      <li className="list-group-item d-flex align-items-center pt-2 mb-2">
                        <a
                          href="#"
                          onClick={(e) => {
                            setShowShareModal(true);
                            e.preventDefault();
                          }}
                          className="mx-auto text-teal fw-medium d-flex align-items-center mt-2"
                        >
                          <svg
                            width="20"
                            height="20"
                            viewBox="0 0 20 20"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M16.0283 6.25C14.3059 6.25 12.9033 4.84833 12.9033 3.125C12.9033 1.40167 14.3059 0 16.0283 0C17.7509 0 19.1533 1.40167 19.1533 3.125C19.1533 4.84833 17.7509 6.25 16.0283 6.25ZM16.0283 1.25C14.995 1.25 14.1533 2.09076 14.1533 3.125C14.1533 4.15924 14.995 5 16.0283 5C17.0616 5 17.9033 4.15924 17.9033 3.125C17.9033 2.09076 17.0616 1.25 16.0283 1.25Z"
                              fill="currentColor"
                            />
                            <path
                              d="M16.0283 20C14.3059 20 12.9033 18.5983 12.9033 16.875C12.9033 15.1517 14.3059 13.75 16.0283 13.75C17.7509 13.75 19.1533 15.1517 19.1533 16.875C19.1533 18.5983 17.7509 20 16.0283 20ZM16.0283 15C14.995 15 14.1533 15.8408 14.1533 16.875C14.1533 17.9092 14.995 18.75 16.0283 18.75C17.0616 18.75 17.9033 17.9092 17.9033 16.875C17.9033 15.8408 17.0616 15 16.0283 15Z"
                              fill="currentColor"
                            />
                            <path
                              d="M3.94531 13.125C2.22275 13.125 0.820312 11.7233 0.820312 10C0.820312 8.27667 2.22275 6.875 3.94531 6.875C5.66788 6.875 7.07031 8.27667 7.07031 10C7.07031 11.7233 5.66788 13.125 3.94531 13.125ZM3.94531 8.125C2.91199 8.125 2.07031 8.96576 2.07031 10C2.07031 11.0342 2.91199 11.875 3.94531 11.875C4.97864 11.875 5.82031 11.0342 5.82031 10C5.82031 8.96576 4.97864 8.125 3.94531 8.125Z"
                              fill="currentColor"
                            />
                            <path
                              d="M6.12066 9.39154C5.90307 9.39154 5.69143 9.27817 5.57729 9.0766C5.40639 8.77661 5.51061 8.39484 5.8106 8.22409L13.5431 3.81568C13.8422 3.64325 14.2247 3.74823 14.3947 4.04914C14.5656 4.34912 14.4614 4.73075 14.1614 4.90164L6.42888 9.30991C6.33138 9.36484 6.22564 9.39154 6.12066 9.39154Z"
                              fill="currentColor"
                            />
                            <path
                              d="M13.8524 16.2665C13.7475 16.2665 13.6416 16.2398 13.5441 16.1841L5.81151 11.7757C5.51152 11.6049 5.40745 11.2231 5.5782 10.9232C5.74818 10.6224 6.12996 10.5182 6.42994 10.6899L14.1623 15.0981C14.4623 15.269 14.5665 15.6506 14.3958 15.9506C14.2807 16.1531 14.0691 16.2665 13.8524 16.2665Z"
                              fill="currentColor"
                            />
                          </svg>

                          <span className="text-blue ms-3">
                            Bagikan pelatihan ini
                          </span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {PelatihanRelated.length > 0 && (
            <section className="bg-gray py-11">
              <div className="container">
                <div
                  className="row align-items-end mb-4 mb-md-7"
                  data-aos="fade-up"
                >
                  <div className="col-md mb-4 mb-md-0">
                    <h2 className="fw-bolder mb-1">Pelatihan Lainnya</h2>
                    <h5 className="mb-0 text-muted">
                      Temukan Pelatihan Menarik {PelatihanDetail.nama_akademi}{" "}
                      Lainnya
                    </h5>
                  </div>
                  <div className="col-md-auto">
                    <Link href={`/akademi/${PelatihanDetail.slug_akademi}`}>
                      <a className="d-flex align-items-center fw-medium">
                        Lihat Semua
                        <div className="ms-2 d-flex">
                          <svg
                            width="10"
                            height="10"
                            viewBox="0 0 10 10"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M7.7779 4.6098L3.32777 0.159755C3.22485 0.0567475 3.08745 0 2.94095 0C2.79445 0 2.65705 0.0567475 2.55412 0.159755L2.2264 0.487394C2.01315 0.700889 2.01315 1.04788 2.2264 1.26105L5.96328 4.99793L2.22225 8.73895C2.11933 8.84196 2.0625 8.97928 2.0625 9.1257C2.0625 9.27228 2.11933 9.4096 2.22225 9.51269L2.54998 9.84025C2.65298 9.94325 2.7903 10 2.9368 10C3.0833 10 3.2207 9.94325 3.32363 9.84025L7.7779 5.38614C7.88107 5.2828 7.93774 5.14484 7.93741 4.99817C7.93774 4.85094 7.88107 4.71305 7.7779 4.6098Z"
                              fill="currentColor"
                            />
                          </svg>
                        </div>
                      </a>
                    </Link>
                  </div>
                </div>

                <div className="row row-cols-lg-2">
                  {PelatihanRelated?.map((Pelatihan) => (
                    <div className="col-lg mb-5">
                      <div className="card rounded-5 p-3 lift">
                        <div className="row gx-0">
                          <Link href={`/pelatihan/${Pelatihan.id}`}>
                            <a
                              className="col-auto p-3 d-block"
                              style={{ maxWidth: "80px" }}
                            >
                              <img
                                className="img-fluid shadow-light-lg"
                                src={
                                  Pelatihan.metode_pelaksanaan == "Swakelola"
                                    ? "/assets/img/kominfo.png"
                                    : PortalHelper.urlMitraLogo(Pelatihan.logo)
                                }
                                alt="Mitra"
                              />
                            </a>
                          </Link>
                          <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                            <div className="card-body p-2">
                              {PortalHelper.isPelatihanOpen(
                                Pelatihan.pendaftaran_mulai,
                                Pelatihan.pendaftaran_selesai,
                                timeDiff.serverTime,
                              ) == 1 && (
                                <>
                                  <span
                                    className="badge badge-success font-size-sm fw-normal mb-1 rounded-5"
                                    style={{ backgroundColor: "#11BB82" }}
                                  >
                                    Buka
                                  </span>
                                </>
                              )}
                              {PortalHelper.isPelatihanOpen(
                                Pelatihan.pendaftaran_mulai,
                                Pelatihan.pendaftaran_selesai,
                                timeDiff.serverTime,
                              ) == 2 && (
                                <>
                                  <span
                                    className="badge badge-danger font-size-sm fw-normal mb-1 rounded-5"
                                    style={{ backgroundColor: "#de4d6e" }}
                                  >
                                    Tutup
                                  </span>
                                </>
                              )}
                              <div className="mt-1 mb-0">
                                <h6 className="line-clamp-2 fw-bolder text-muted mb-n1">
                                  Batch {Pelatihan.batch}
                                </h6>
                              </div>

                              <Link href={`/pelatihan/${Pelatihan.id}`}>
                                <a className="d-block">
                                  <h6 className="line-clamp-2 fw-bolder mb-n1">
                                    {Pelatihan.nama_pelatihan}
                                  </h6>
                                  <span className="text-dark font-size-sm fw-normal">
                                    {Pelatihan.namasub_akademi} (
                                    {Pelatihan.nama_akademi})
                                  </span>
                                </a>
                              </Link>

                              <ul className="nav d-block mt-2">
                                <li className="nav-item w-100 mb-1">
                                  <div className="d-flex align-items-center">
                                    <div className="me-1 d-flex text-secondary icon-uxs">
                                      <i className="bi bi-calendar2-week"></i>
                                    </div>
                                    <div className="font-size-sm">
                                      <SimpleDateRange
                                        startDate={Pelatihan.pendaftaran_mulai}
                                        endDate={Pelatihan.pendaftaran_selesai}
                                        diff={timeDiff.diff}
                                      ></SimpleDateRange>
                                    </div>
                                  </div>
                                </li>
                                <li className="nav-item w-100 mb-1">
                                  <div className="d-flex align-items-center">
                                    <div className="me-1 d-flex text-secondary icon-uxs">
                                      <i className="bi bi-pass"></i>
                                    </div>
                                    <div className="font-size-sm">
                                      {Pelatihan.nama_tema}
                                    </div>
                                  </div>
                                </li>
                                <li className="nav-item w-100 mb-1">
                                  <div className="d-flex align-items-center">
                                    <div className="me-1 d-flex text-secondary icon-uxs">
                                      <i className="bi bi-geo-alt"></i>
                                    </div>
                                    <div className="font-size-sm">
                                      {Pelatihan.metode_pelatihan == "Online"
                                        ? "Online"
                                        : "" + Pelatihan.lokasi_pelatihan}
                                    </div>
                                  </div>
                                </li>
                                <li className="nav-item w-100 mb-1">
                                  <div className="d-flex align-items-center">
                                    <div className="me-1 d-flex text-secondary icon-uxs">
                                      <i className="bi bi-mic"></i>
                                    </div>
                                    <div className="font-size-sm">
                                      {Pelatihan.nama_penyelenggara}
                                    </div>
                                  </div>
                                </li>
                                {Pelatihan.metode_pelaksanaan == "Mitra" && (
                                  <li className="nav-item w-100 mb-1">
                                    <div className="d-flex align-items-center">
                                      <div className="me-1 d-flex text-secondary icon-uxs">
                                        {/* Icon */}
                                        <i className="bi bi-person-video3" />
                                      </div>
                                      <div className="font-size-sm">
                                        {Pelatihan.nama_mitra}
                                      </div>
                                    </div>
                                  </li>
                                )}
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </section>
          )}
        </div>
      )}

      <VerifikasiHandphone
        show={showVerifikasiModal}
        handleClose={() => {
          setShowVerifikasiModal(false);
        }}
        children={serverMessage}
        nomorHandphone={PortalHelper.getUserData()?.nomor_handphone ?? ""}
        onVerify={() => {
          setShowVerifikasiModal(false);

          const user = PortalHelper.getUserData();
          PortalHelper.saveUserData({ ...user, handphone_verifikasi: true });
          eventBus.dispatch("profil-updated");

          const { lengkapi_pendaftaran } = user;
          if (lengkapi_pendaftaran && lengkapi_pendaftaran.length > 0) {
            NotificationHelper.Success({
              title: "Perhatian",
              message: "Selanjutnya Mohon Lengkapi Form Pendaftaran.",
              onClose: () => {
                router.push(
                  `/pelatihan/${lengkapi_pendaftaran[0].pelatian_id}/daftar`,
                );
              },
              okText: "Lengkapi Pendaftaran",
            });
          } else {
            router.push(`/pelatihan/${pelatihanID}/daftar`);
          }
        }}
      />

      <VerifikasiEmail
        show={showEmailVerifikasiModal}
        handleClose={() => {
          setShowEmailVerifikasiModal(false);
          setOtpErrorMessage("");
        }}
        children={serverMessage}
        onVerify={async ({ pin }) => {
          setOtpErrorMessage("");
          try {
            const verifyResult = await PortalService.verifyEmailOtpAdmin(pin);
            if (verifyResult.data.success) {
              NotificationHelper.Success({
                message:
                  verifyResult.data.message ??
                  "Berhasil melakukan verifikasi email.",
              });
              PortalHelper.updateUserData("email_verifikasi", true);
            } else {
              setShowEmailVerifikasiModal(false);
            }
          } catch (err) {
            setShowEmailVerifikasiModal(false);
            const message =
              typeof err == "string" ? err : err?.response?.data?.message;
            setOtpErrorMessage(
              message ?? "Tidak dapat melakukan verifikasi email.",
            );
          }
        }}
        onResend={() => {
          setOtpErrorMessage("");
          resendEmail();
        }}
        errMessage={otpErrorMessage}
      />
    </div>
  );
}

pelatihanDetailPage.bodyBackground = "bg-white";

export default pelatihanDetailPage;
