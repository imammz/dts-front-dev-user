const ValidasiSertifikat = () => {
  return (
    <>
      <div className="container py-9">
        <div className="row mb-8">
          <div className="col-lg-7 mb-6 mb-lg-0 position-relative">
            <div>
              <h3 className="mb-1">Validasi e-Sertifikat</h3>
              <p className="font-size-lg mb-0 text-capitalize mb-5">
                Hi, silahkan cek validitas e-sertifikat (hanya berlaku untuk
                e-sertifikat)
              </p>
              <div className="row g-3">
                <div className="col-9">
                  <input
                    type="text"
                    className="form-control form-control-sm"
                    placeholder="Nomor Sertifikat"
                    aria-label="Nomor Sertifikat"
                  />
                </div>
                <div className="col">
                  <a
                    href="daftar-pelatihan.html"
                    className="btn btn-blue btn-sm btn-block mb-6"
                    type="button"
                    name="button"
                  >
                    Validasi
                  </a>
                </div>
              </div>
              <div className="card border rounded mb-5 p-5">
                <div className="alert bg-green text-white p-2" role="alert">
                  Sertifikat Valid
                </div>
                <div className="col-lg mb-5">
                  <div className="card p-3">
                    <div className="row gx-0">
                      <div className="col-9 d-md-block">
                        <div className="card-body p-0">
                          <div className="mb-0">
                            <span className="text-dark font-size-sm fw-normal">
                              Fresh Graduate Academy
                            </span>
                          </div>
                          <a href="#" className="d-block mb-2">
                            <h6 className="line-clamp-2 mb-3">
                              Database Design and Programming With SQL{" "}
                            </h6>
                          </a>
                          <ul className="nav mx-n3 d-block d-md-flex">
                            <li className="nav-item px-3 mb-3 mb-md-0">
                              <div className="d-flex align-items-center">
                                <div className="me-2 d-flex text-secondary icon-uxs">
                                  <i className="fa fa-credit-card"></i>
                                </div>
                                <div className="font-size-base">
                                  xxxx/xxxxx/xxxxx/xxxxxx-x
                                </div>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-5">
            <div className="d-block rounded bg-light p-2 mb-6">
              <div className="pt-6 pb-5 px-5 px-lg-3 px-xl-5">
                <div className="d-flex align-items-center">
                  <div className="mb-4">
                    <h4 className="mb-4">Panduan</h4>
                    <p className="font-size-sm mb-3 text-capitalize">
                      Cek keaslian sertifikat dengan menggunakan nomor
                      registrasi pelatihan yang tertera di sertifikat atau
                      dengan kode QR. Nomor registrasi dan kode QR dapat dilihat
                      pada bagian yang ditandai merah.
                    </p>
                    <img
                      src="/assets/img/dummy-sertif.jpg"
                      className="mt-5 img-responsive"
                    />
                  </div>
                </div>
              </div>
            </div>

            <div className="d-none">
              <div className="border rounded px-6 px-lg-5 px-xl-6 pt-5 shadow">
                <h3 className="mb-5">Latest Courses</h3>
                <ul className="list-unstyled mb-0">
                  <li className="media mb-6 d-flex">
                    <a
                      href="course-single-v5.html"
                      className="w-100p d-block me-5"
                    >
                      <img
                        src="/assets/img/photos/photo-1.jpg"
                        alt="..."
                        className="avatar-img rounded-lg h-90p w-100p"
                      />
                    </a>
                    <div className="media-body flex-grow-1">
                      <a href="course-single-v5.html" className="d-block">
                        <h6 className="line-clamp-2 mb-3">
                          Web Developtment and Design
                        </h6>
                      </a>
                      <del className="font-size-sm me-2">$959</del>
                      <ins className="h6 mb-0 ">$415.99</ins>
                    </div>
                  </li>

                  <li className="media mb-6 d-flex">
                    <a
                      href="course-single-v5.html"
                      className="w-100p d-block me-5"
                    >
                      <img
                        src="/assets/img/photos/photo-2.jpg"
                        alt="..."
                        className="avatar-img rounded-lg h-90p w-100p"
                      />
                    </a>
                    <div className="media-body flex-grow-1">
                      <a href="course-single-v5.html" className="d-block">
                        <h6 className="line-clamp-2 mb-3">
                          The Complete Cyber Security Course : Hackers{" "}
                        </h6>
                      </a>
                      <del className="font-size-sm me-2">$959</del>
                      <ins className="h6 mb-0 ">$415.99</ins>
                    </div>
                  </li>

                  <li className="media mb-6 d-flex">
                    <a
                      href="course-single-v5.html"
                      className="w-100p d-block me-5"
                    >
                      <img
                        src="/assets/img/photos/photo-14.jpg"
                        alt="..."
                        className="avatar-img rounded-lg h-90p w-100p"
                      />
                    </a>
                    <div className="media-body flex-grow-1">
                      <a href="course-single-v5.html" className="d-block">
                        <h6 className="line-clamp-2 mb-3">
                          Fashion Photography From Professional
                        </h6>
                      </a>
                      <del className="font-size-sm me-2">$959</del>
                      <ins className="h6 mb-0 ">$415.99</ins>
                    </div>
                  </li>

                  <li className="media mb-6 d-flex">
                    <a
                      href="course-single-v5.html"
                      className="w-100p d-block me-5"
                    >
                      <img
                        src="/assets/img/photos/photo-16.jpg"
                        alt="..."
                        className="avatar-img rounded-lg h-90p w-100p"
                      />
                    </a>
                    <div className="media-body flex-grow-1">
                      <a href="course-single-v5.html" className="d-block">
                        <h6 className="line-clamp-2 mb-3">
                          The Complete Financial Analyst Course 2020
                        </h6>
                      </a>
                      <del className="font-size-sm me-2">$959</del>
                      <ins className="h6 mb-0 ">$415.99</ins>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default ValidasiSertifikat;
