import data from "../../../data/db.json";

// untuk penggunaan data dummy dari file data/db.json
export default async function handler(req, res) {
  try {
    const result = data[req.query.fileName];
    // console.log(result)
    if (result == undefined) throw "Data tidak ditemukan";
    res.status(200).json({ success: true, result: { data: result } });
  } catch (err) {
    res.status(500).json({ success: false, message: err });
  }
}
