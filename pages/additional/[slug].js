import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import HtmlParser from "react-html-parser";
import Skeleton from "react-loading-skeleton";
import PortalService from "../../services/PortalService";
import NotificationHelper from "../../src/helper/NotificationHelper";

const options = {
  transform: function transform(node, index) {
    // console.log(node);
    if (node.type === "script") {
      let urlSplitted = window.location.href.split("/");
      let id = urlSplitted[urlSplitted.length - 1] + "-" + index;
      if (window.document.getElementById(id) == null) {
        var script = document.createElement("script");
        script.onload = function () {
          //do stuff with the script
        };
        script.src = node?.attribs?.src;
        script.id = id;
        script.type = node?.attribs?.type;

        window.document.head.appendChild(script);
      }
      // return <script key={index} src={node?.attribs?.src}>{node?.children[0]?.data}</script>;
      return "";
    }
  },
};

export default function AdditionalPage() {
  const router = useRouter();
  const {
    query: { slug },
  } = router;

  const [isLoading, setIsLoading] = useState(true);
  const [page, setPage] = useState({ content: "", image: "", title: "" });
  const [templateType, setTemplateType] = useState(1);

  useEffect(async () => {
    if (router.isReady) {
      try {
        const resp = await PortalService.getDetailPageMenu(slug);
        if (resp.data.success) {
          if (resp.data.result.length != 0) {
            const page = JSON.parse(resp.data.result[0].property_template);
            // console.log(page)
            setPage({ ...page[0] });
            setTemplateType(resp.data.result[0]?.template_type || 1);
          } else {
            NotificationHelper.Failed({
              message: "Halaman tidak ditemukan",
              onClose: () => {
                router.push("/");
              },
              okText: "Kembali ke Beranda",
            });
          }
        }
      } catch (error) {
        console.log(error);
      }
      setIsLoading(false);
    }
  }, [router.isReady, slug]);

  return (
    <div>
      <header className="py-6 bg-blue mb-6 z-index-0">
        <div className="container">
          <div className="row align-items-end">
            <div className="col-md mb-md-0">
              <h3 className="fw-bolder text-white mb-0">{page.title}</h3>
            </div>
          </div>
        </div>
      </header>
      <div className="container z-index-0 pb-80">
        {isLoading ? (
          <Skeleton className="h-400p w-100"></Skeleton>
        ) : (
          <>
            <div className="row py-lg-6">
              <div
                className={`col-md-${templateType == 3 ? "4" : "12"} col-12`}
              >
                {page.image && (
                  <div>
                    <img
                      src={page.image}
                      alt={page.title}
                      className="img-fluid mb-6"
                      style={{ width: "100%" }}
                    />
                  </div>
                )}
              </div>
              <div
                className={`col-md-${templateType == 3 ? "8" : "12"} col-12`}
              >
                {HtmlParser(page.content, options)}
              </div>
            </div>
          </>
        )}
      </div>
    </div>
  );
}
