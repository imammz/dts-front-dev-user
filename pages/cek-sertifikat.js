import Lottie, { useLottie } from "lottie-react";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Skeleton from "react-loading-skeleton";
import ButtonWithLoading from "../components/ButtonWithLoading";
import PortalService from "../services/PortalService";
import PelatihanService from "../services/PelatihanService";

import * as animation from "./../public/assets/json/file-not-found-animation.json";
import SimpleDateRange from "../components/SimpleDateRange";
import PortalHelper from "../src/helper/PortalHelper";
import ReCAPTCHA from "react-google-recaptcha";

const LoadingSertifikat = () => (
  <>
    <div className="card border rounded-5 border-0 border-lg border-xl  py-8">
      <div className="col-lg-12 mx-auto">
        <div className="d-block rounded-5 mb-6">
          <div className="text-center">
            <div className="pt-6 px-5 px-lg-3 px-xl-5">
              <div className="mx-auto mb-5 " style={{ width: 400 }}>
                <Skeleton className="h-200p"></Skeleton>
              </div>

              <div className="row">
                <div className="col-12 px-2">
                  <Skeleton
                    className="w-200p mb-3"
                    style={{ height: 25 }}
                  ></Skeleton>
                  <div className="mb-5">
                    <Skeleton style={{ height: 20 }}></Skeleton>
                    <Skeleton
                      style={{ height: 25, width: 200 }}
                      className="mx-auto my-4"
                    ></Skeleton>
                    <Skeleton
                      style={{ height: 20, width: 250 }}
                      className="mx-auto"
                    ></Skeleton>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg mb-5">
            <Skeleton className="h-140p rounded"></Skeleton>
          </div>
        </div>
      </div>
    </div>
  </>
);

const ValidasiSertifikat = () => {
  const [idSertifikat, setIdSertifikat] = useState("");
  const [loading, setLoading] = useState(false);
  const [status, setStatus] = useState(null);
  const [result, setResult] = useState();
  const [PelatihanDetail, setPelatihanDetail] = useState();
  const [userCaptchaResponse, setUserCaptchaResponse] = useState(null);

  const router = useRouter();
  const { registrasi } = router.query;

  useEffect(() => {
    if (!registrasi) {
      setLoading(false);
      setStatus(null);
    } else {
      setIdSertifikat(registrasi);
    }
  }, [registrasi]);

  const getDetailSertifikat = async (id = idSertifikat) => {
    try {
      setLoading(true);
      const resp = await PortalService.validasiSertifikat(id);
      // console.log(resp);
      // setStatus(false)
      setResult(resp);
      if (resp.data?.success) {
        setStatus(true);
        const data = resp.data.result[0];
        // console.log(data)
        setResult(data);
        // if (registrasi) {
        const pelatihan = await PortalService.pelatihanDetail(
          data.pelatihan_id,
        );
        // console.log(pelatihan)
        setPelatihanDetail(pelatihan.data?.result?.data);
        // }
      } else {
        setStatus(false);
      }
    } catch (error) {
      console.log(error);
      setStatus(false);
    }
    setLoading(false);
  };

  return (
    <>
      <div className="container pt-9">
        <div className="row mb-9">
          <div className="col-lg-7 mb-6 mb-lg-0 position-relative">
            <div>
              <h3 className="fw-bolder mb-1">
                Validasi e-Sertifikat Kelulusan
              </h3>
              <p className="font-size-lg mb-0 text-capitalize mb-5">
                Silahkan periksa validitas e-sertifikat (hanya berlaku untuk
                e-sertifikat yang terbit mulai tahun 2023)
              </p>
              <div className="row g-3">
                <div className="col-lg-10">
                  <input
                    type="text"
                    className="form-control form-control-sm"
                    value={idSertifikat}
                    onChange={(e) => {
                      setIdSertifikat(e.target.value);
                    }}
                    placeholder="Nomor Sertifikat"
                    aria-label="Nomor Sertifikat"
                  />
                </div>
              </div>
              <div className="row g-3 mt-2 mb-6">
                <div className="col-lg-10">
                  <ReCAPTCHA
                    sitekey={process.env.CAPTCHA_SITEKEY}
                    onChange={(e) => {
                      setUserCaptchaResponse(e);
                    }}
                    onExpired={() => {
                      setUserCaptchaResponse(null);
                    }}
                    onError={() => {
                      setUserCaptchaResponse(null);
                    }}
                  ></ReCAPTCHA>
                </div>
                <div className="col-lg-10">
                  <ButtonWithLoading
                    onClick={() => {
                      getDetailSertifikat();
                    }}
                    className={`btn btn-block btn-blue rounded-pill mb-6 `}
                    disabled={
                      userCaptchaResponse == null || idSertifikat.length == 0
                    }
                    loading={loading}
                    type="button"
                    name="button"
                  >
                    VALIDASI SERTIFIKAT
                  </ButtonWithLoading>
                  {/* <a  onClick={getDetailSertifikat} className="btn btn-blue btn-sm btn-block mb-6" type="button" name="button">Validasi</a> */}
                </div>
              </div>

              {loading && <LoadingSertifikat></LoadingSertifikat>}

              {!loading && (
                <>
                  {status == true && (
                    <>
                      <div className="card border rounded-5 border-0 border-lg border-xl p-5">
                        <div className="col-lg-12 mx-auto">
                          <div className="d-block rounded-5 mb-6">
                            <div className="text-center">
                              <div className="pt-6 px-5 px-lg-3 px-xl-5">
                                <img
                                  src="/assets/img/empty-state/success-data.png"
                                  className="img-fluid align-self-center mb-5"
                                  style={{ width: 300 }}
                                ></img>

                                <div className="row">
                                  <div className="col-12 px-2">
                                    <h2 className="fw-bold text-green mb-2">
                                      Sertifikat Valid
                                    </h2>
                                    <div className="mb-5">
                                      <p className="mb-0">
                                        Sertifikat dengan nomor{" "}
                                        <span className="text-black fw-bold">
                                          {result.nomor_sertifikat}
                                        </span>{" "}
                                        adalah sertifikat valid yang dikeluarkan
                                        oleh Digitalent Scholarship kepada
                                      </p>
                                      <p className="my-3 fs-4 fw-bold text-black">
                                        {result.nama_peserta}
                                      </p>
                                      <p>atas kelulusannya pada pelatihan</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-lg mb-5">
                              <div className="card rounded-5 border p-3">
                                <div className="row gx-0">
                                  <div
                                    className="col-auto p-3 d-block"
                                    style={{ maxWidth: "80px" }}
                                  >
                                    <img
                                      className="img-fluid shadow-light-lg"
                                      src={
                                        PelatihanDetail?.metode_pelaksanaan ==
                                        "Swakelola"
                                          ? "/assets/img/kominfo.png"
                                          : PortalHelper.urlMitraLogo(
                                              PelatihanDetail?.logo,
                                            )
                                      }
                                      alt="Mitra"
                                    />
                                  </div>
                                  <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                                    <div className="card-body p-3">
                                      <Link
                                        href={
                                          "/akademi/" +
                                          PelatihanDetail.slug_akademi
                                        }
                                      >
                                        <div className="mb-0">
                                          <span className="text-dark font-size-sm cursor-pointer">
                                            {PelatihanDetail?.nama_akademi}
                                          </span>
                                        </div>
                                      </Link>
                                      <Link
                                        href={
                                          "/pelatihan/" + PelatihanDetail.id
                                        }
                                      >
                                        <h5 className="fw-bolder mb-3 cursor-pointer">
                                          {PelatihanDetail?.nama_pelatihan}
                                        </h5>
                                      </Link>
                                      <ul className="nav mx-n3 d-block d-md-flex">
                                        <li className="nav-item px-3 mb-2">
                                          <div className="d-flex align-items-center">
                                            <div className="me-1 d-flex text-secondary icon-uxs">
                                              <i className="fa fa-calendar"></i>
                                            </div>
                                            <div className="font-size-sm">
                                              <SimpleDateRange
                                                startDate={
                                                  PelatihanDetail?.pendaftaran_mulai
                                                }
                                                endDate={
                                                  PelatihanDetail?.pendaftaran_selesai
                                                }
                                              ></SimpleDateRange>
                                            </div>
                                          </div>
                                        </li>
                                        <li className="nav-item px-3 mb-2">
                                          <div className="d-flex align-items-center">
                                            <div className="me-1 d-flex text-secondary icon-uxs">
                                              <i className="fa fa-map-marker-alt"></i>
                                            </div>
                                            <div className="font-size-sm">
                                              {PelatihanDetail?.metode_pelatihan ==
                                              "Online"
                                                ? "Online"
                                                : "" +
                                                  PelatihanDetail?.lokasi_pelatihan}
                                            </div>
                                          </div>
                                        </li>
                                        <li className="nav-item px-3 mb-2">
                                          <div className="d-flex align-items-center">
                                            <div className="me-1 d-flex text-secondary icon-uxs">
                                              <i className="fa fa-microphone-alt"></i>
                                            </div>
                                            <div className="font-size-sm">
                                              {PelatihanDetail?.metode_pelaksanaan ==
                                              "Mitra"
                                                ? PelatihanDetail?.nama_mitra
                                                : PelatihanDetail?.nama_penyelenggara
                                                  ? PelatihanDetail?.nama_penyelenggara
                                                  : "Kominfo"}
                                            </div>
                                          </div>
                                        </li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </>
                  )}
                  {status == false && (
                    <div className="row py-4">
                      <div className="col-lg-12 border rounded-5 border-0 border-lg border-xl mx-auto">
                        <div className="d-block rounded-5 mb-6">
                          <div className="text-center">
                            <div className="pt-6 px-5 px-lg-3 px-xl-5">
                              <div className="w-50 m-auto">
                                <Lottie
                                  animationData={animation}
                                  loop={true}
                                ></Lottie>
                              </div>

                              <div className="row">
                                <div className="col-12 px-2">
                                  <h2 className="fw-bold text-red mb-0">
                                    Sertifikat Tidak Ditemukan
                                  </h2>
                                  <div className="mb-5">
                                    <p className="mb-0">
                                      Mohon maaf, sertifikat yang anda cari
                                      tidak ditemukan
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                </>
              )}
            </div>
          </div>

          <div className="col-lg-5">
            <div className="d-block rounded-5 bg-light p-2 mb-6">
              <div className="pt-6 pb-5 px-5 px-lg-3 px-xl-5">
                <div className="d-flex align-items-center">
                  <div className="mb-4">
                    <h4 className="fw-bolder mb-4">Panduan</h4>
                    <p className="font-size-sm mb-3 text-capitalize">
                      Cek keaslian sertifikat dengan menggunakan nomor
                      registrasi pelatihan yang tertera di sertifikat atau
                      dengan kode QR. Nomor registrasi dan kode QR dapat dilihat
                      pada bagian yang ditandai merah.
                    </p>
                    <img
                      src="/assets/img/dummy-sertif.jpeg"
                      className="mt-5 img-responsive"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default ValidasiSertifikat;
