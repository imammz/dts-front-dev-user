import React, { useState, useEffect, useRef } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import axios from "axios";
import Moment from "react-moment";
import "moment/locale/id";
import PortalService from "../services/PortalService";
import PortalHelper from "../src/helper/PortalHelper";
import ReactHtmlParser from "react-html-parser";

const parsingNoTel = (tel) => {
  return `(021) ` + `${tel}`.substr(3);
};

//skeleton
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

export default function DataCapaian() {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(false);
  });

  return (
    <div>
      <header className="py-6 bg-blue mb-6 z-index-0">
        <div className="container">
          <div className="row align-items-end" data-aos="fade-up">
            <div className="col-md mb-md-0">
              <h3 className="fw-bolder text-white mb-0">Data Capaian</h3>
            </div>
          </div>
        </div>
      </header>

      <div className="container">
        {loading && (
          <div className="row my-8">
            <div className="col-12">
              <Skeleton className="w-100" height={500} />
            </div>
          </div>
        )}
        {!loading && (
          <div className="row my-8">
            <div className="col-12">
              <iframe
                src="https://data.sdmdigital.id/t/Publish/views/DTS2023web_blsdm/DashboardNew1080?%3Aembed=y&:isGuestRedirectFromVizportal=y&:display_count=n&:showAppBanner=false&:origin=viz_share_link&:showVizHome=n"
                style={{
                  width: "100%",
                  height: "1800px",
                  padding: "0",
                  margin: "0",
                }}
                allowFullScreen
              ></iframe>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
