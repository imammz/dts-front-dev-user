// all library style placed here
import "react-loading-skeleton/dist/skeleton.css";
import "react-datepicker/dist/react-datepicker.css";
import "react-phone-input-2/lib/style.css";
import "../styles/croppie.css";
import "./../styles/custom.css";

import Layout from "../layouts/Layout.jsx";
import Loading from "../components/Modal/Loading.js";
import Survey from "../components/Modal/SurveyMandatoryModal";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import PortalHelper from "../src/helper/PortalHelper";
import Script from "next/script";
import Middleware from "../src/helper/MiddlewareHelper";

function MyApp({ Component, pageProps, pageData, ...appProps }) {
  const router = useRouter();
  const [showLoading, setShowLoading] = useState(false);
  const [showSurveyModal, setShowSurveyModal] = useState(false);
  const [listSurvey, setListSurvey] = useState([]);
  const [isMounted, setIsMounted] = useState(false);
  const [currentRoute, setCurrentRoute] = useState("/");

  const handleShowLoading = (show) => {
    setShowLoading(show);
  };

  useEffect(async () => {
    if (router.isReady) {
      setIsMounted(true);
      const userData = PortalHelper.getUserData();
      if (
        !router.pathname.includes("/survey") &&
        !router.pathname.includes("/test-substansi") &&
        !router.pathname.includes("error") &&
        !router.pathname.includes("/login") &&
        userData?.mandatory_survey?.length > 0
      ) {
        setShowSurveyModal(true);
        setListSurvey(userData.mandatory_survey);
      } else if (userData == null) {
        setListSurvey([]);
        setShowSurveyModal(false);
      }
      // firebaseApp.messaging.requestPermission()
      router.events?.on("routeChangeError", () => {
        setShowLoading(false);
      });
      router.events?.on("routeChangeStart", async (url, { shallow }) => {
        // console.log("router change")
        const btnNavbarTogglerPc =
          window.document.getElementById("navbar-toggler-pc");
        const btnNavbarTogglerMobile = window.document.getElementById(
          "navbar-toggler-mobile",
        );
        const btnSearchMobile = window.document.getElementById(
          "btnCollapseSearchMobile",
        );

        if (btnNavbarTogglerPc != null) {
          const stateT1 = btnNavbarTogglerPc.getAttribute("aria-expanded");
          if (stateT1 == "true") {
            btnNavbarTogglerPc.click();
          }
        }
        if (btnNavbarTogglerMobile != null) {
          const stateT2 = btnNavbarTogglerMobile.getAttribute("aria-expanded");
          if (stateT2 == "true") {
            btnNavbarTogglerMobile.click();
          }
        }
        if (btnSearchMobile != null) {
          const stateT3 = btnNavbarTogglerMobile.getAttribute("aria-expanded");
          if (stateT3 == "true") {
            btnSearchMobile.click();
          }
        }
        setShowLoading(true);
        if (!shallow && currentRoute != url.split("?")[0]) {
          await Middleware(router, url);
          setCurrentRoute(url.split("?")[0]);
        }
      });
      router.events?.on("routeChangeComplete", () => {
        setShowLoading(false);
        let modals = document.getElementsByClassName("modal-backdrop show");
        if (modals && modals.length > 0) {
          modals = Array.from(modals).filter((elem) => !elem.hasChildNodes());
          modals[0]?.remove();
        }
      });
    }
  }, [router]);

  useEffect(async () => {
    if (router) {
      await Middleware(router, null);
    }
  }, []);

  return (
    <Layout
      pageData={pageData}
      bodyBackground={
        Component.bodyBackground ? Component.bodyBackground : "bg-white"
      }
      pathname={appProps.router.pathname}
    >
      <Component {...pageProps} showLoadingModal={handleShowLoading} />
      <Loading show={showLoading} handleClose={() => {}} backdropOnly={true} />
      <Survey
        show={showSurveyModal}
        handleClose={() => {
          setShowSurveyModal(false);
        }}
        surveys={listSurvey}
      />
      <Script src="https://www.googletagmanager.com/gtag/js?id=G-XK9JGQFQQR"></Script>
      <Script id="google-analytics">
        {`
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag("js", new Date());

            gtag("config", "G-XK9JGQFQQR");
            `}
      </Script>

      <Script>
        {`
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '3476059545965334');
            fbq('track', 'PageView');
          `}
      </Script>
    </Layout>
  );
}
export default MyApp;
