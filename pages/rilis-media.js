import React, { useState, useEffect } from "react";
import Link from "next/link";
import "moment/locale/id";
import PortalService from "./../services/PortalService";

import { useRouter } from "next/router";
import DaftarPelatihanView from "../components/DaftarPelatihanView";
import PortalHelper from "./../src/helper/PortalHelper";

function ListMedia(props) {
  const [thresHold, setThesHold] = useState(5);
  const [loading, setLoading] = useState(false);
  const [length, setLength] = useState(thresHold);
  const [start, setStart] = useState(1);
  const [totalLength, setTotalLength] = useState(0);
  const [dataRilisMedia, setDataRilisMedia] = useState([]);
  const [skeletons, setSkeletons] = useState([1, 2, 3, 4]);
  const router = useRouter();

  useEffect(async () => {
    if (router.isReady) {
      getRilisMedia();
    }
  }, [start]);

  const getRilisMedia = async () => {
    try {
      setLoading(true);
      // console.log(start);
      const mediaResp = await PortalService.rilisMediaList(length, 0, start);
      const listRilisMedia = mediaResp.data.result.data.map((rm) => ({
        ...rm,
        gambar: PortalHelper.isValidHttpUrl(rm.gambar)
          ? rm.gambar
          : PortalHelper.urlPublikasiLogo(rm.gambar),
      }));
      setTotalLength(mediaResp.data.result.meta.total);
      setDataRilisMedia((rilismedia) => [...rilismedia, ...listRilisMedia]);
      // console.log(dataRilisMedia)
      // setDataRilisMedia((rilismedia) => (rilismedia))
      setLoading(false);
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <>
      <header
        className="py-8 bg-white mb-8 z-index-0"
        style={{ backgroundImage: "none" }}
      >
        <div className="container">
          <div className="row align-items-end" data-aos="fade-up">
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb breadcrumb-scroll">
                <li className="breadcrumb-item">
                  <Link href="/">
                    <a className="text-gray-700">Home</a>
                  </Link>
                </li>
                <li
                  className="breadcrumb-item text-blue active"
                  aria-current="page"
                >
                  Rilis Media
                </li>
              </ol>
            </nav>
            <div className="col-md mb-md-0">
              <h1 className="fw-semi-bold mb-0">Rilis Media</h1>
              <p className="font-size-lg mb-lg-0">Informasi dan Artikel</p>
            </div>
            <div className="col-md-auto col-sm-12">
              <div className="d-lg-flex rounded mb-5">
                <div className="d-lg-flex flex-wrap">
                  <div>
                    <div className="d-flex align-items-center h-50p">
                      <div className="form-floating col-12"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>

      <div className="container z-index-0">
        <div className="row">
          <div className="col-xl-9 mb-9">
            {/* { JSON.stringify(dataRilisMedia)} */}
            {dataRilisMedia && (
              <DaftarPelatihanView
                loading={loading}
                data={PortalHelper.generateListMediaDisplay(dataRilisMedia, {
                  category: "kategori",
                  title: "judul",
                  speaker: "user_name",
                  gambar: "gambar",
                })}
              />
            )}

            {dataRilisMedia.length < totalLength && (
              <div className="text-center mt-8">
                <a
                  className="btn btn-outline-white mw-300p d-flex mx-auto read-more"
                  role="button"
                  aria-expanded="false"
                  onClick={() => {
                    setStart((s) => s + length);
                  }}
                >
                  <span className="d-inline-flex mx-auto align-items-center more">
                    <i className="fa fa-chevron-down"></i>

                    <span className="ms-2">Muat lebih banyak</span>
                  </span>

                  <span className="less mx-auto">LOAD LESS</span>
                </a>
              </div>
            )}
          </div>
          <div className="col-xl-3 px-4 mb-5 mb-xl-0">
            <div className="col-lg-12 mb-5">
              <div className="card border shadow lift px-5 pt-6 pb-8">
                <div className="d-inline-block rounded-circle mb-4">
                  <div
                    className="icon-circle"
                    style={{ backgroundColor: "#c2dffb", color: "#196ECD" }}
                  >
                    <i className="fa fa-book fa-2x"></i>
                  </div>
                </div>
                <h5>Tentang Program</h5>
                <p className="mb-5">
                  Pelajari mengenai program Digital Talent Scholarship.
                </p>
                <Link href="/program">
                  <a className="btn btn-outline-primary btn-xs btn-pill mb-1">
                    Pelajari
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
export default ListMedia;
