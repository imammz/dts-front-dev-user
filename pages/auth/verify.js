import React, { useEffect, useState } from "react";
import Link from "next/link";
import SSOInfo from "../../components/SSOSInfo";
import PortalService from "../../services/PortalService";
import { useRouter } from "next/router";
import NotificationHelper from "../../src/helper/NotificationHelper";
import Skeleton from "react-loading-skeleton";

export default function Verify({ showLoadingModal }) {
  const [isLoadingGetLinks, setIsLoadingGetLinks] = useState(true);
  const [siteManajemenData, setSiteManajemenData] = useState(true);
  const [isLoading, setIsLoading] = useState(true);
  const [verify, setVerify] = useState(null);

  useState(async () => {
    try {
      const params = new Proxy(new URLSearchParams(window.location.search), {
        get: (searchParams, prop) => searchParams.get(prop),
      });
      let token = params.token;
      setIsLoading(true);
      const resp = await PortalService.aktivasiUser(token);
      setIsLoading(false);

      if (resp.data.result.Status) {
        setVerify(resp.data.result.Data[0]);
      } else {
        setVerify({ flag_verify: 0 });
        throw Error(resp.data.result.Message);
      }
    } catch (err) {
      console.log(err);
      setIsLoading(false);
    }
  }, []);
  // ambil data link
  useEffect(async () => {
    let settingData = await PortalService.generalDataSiteManajamenen();
    // console.log(settingData)
    if (settingData.data?.success) {
      // console.log(settingData.data.result.Data)
      const {
        alamat,
        color,
        created_at,
        created_by,
        email,
        footer_logo,
        header_logo,
        koordinat,
        link,
        logo_description,
        notelp,
        social_media,
      } = settingData.data.result[0];
      setSiteManajemenData({
        alamat: alamat,
        color: JSON.parse(color),
        created_at: created_at,
        created_by: created_by,
        email: email,
        footer_logo: footer_logo,
        header_logo: header_logo,
        koordinat: koordinat?.split(","),
        logo_description: logo_description,
        notelp: JSON.parse(notelp),
        link: JSON.parse(link),
        social_media: JSON.parse(social_media),
      });
      // console.log(siteManajemenData)
    } else {
      settingData = await PortalService.generalDataSiteManajamenen(true);
      const {
        alamat,
        color,
        created_at,
        created_by,
        email,
        footer_logo,
        header_logo,
        koordinat,
        link,
        logo_description,
        notelp,
        social_media,
      } = settingData.data.result[0];
      setSiteManajemenData({
        alamat: alamat,
        color: JSON.parse(color),
        created_at: created_at,
        created_by: created_by,
        email: email,
        footer_logo: footer_logo,
        header_logo: header_logo,
        koordinat: koordinat?.split(","),
        logo_description: logo_description,
        notelp: JSON.parse(notelp),
        link: JSON.parse(link),
        social_media: JSON.parse(social_media),
      });
    }
    setIsLoadingGetLinks(false);
  }, []);
  return (
    <>
      <div className="container my-8">
        <div className="row">
          {isLoading && (
            <div className="col-lg-5 col-xl-6 mb-6 mb-lg-0">
              <Skeleton
                className="text-red text-center mb-2"
                style={{ height: "30px" }}
              />
              <Skeleton className="img-fluid h-50" />
            </div>
          )}
          {!isLoading && (
            <>
              {verify && verify.flag_verify == 1 ? (
                <>
                  <div className="card border rounded-5 border-0 border-lg border-xl px-6 py-8">
                    <div className="row">
                      <div className="col-lg-6 text-center">
                        <h2 className="fw-bold text-center text-green mb-0">
                          Selamat, Verifikasi Akun Berhasil
                        </h2>
                        <img
                          src={`/assets/img/empty-state/success-data.png?v=${Date.now()}`}
                          className="img-fluid align-self-center"
                          style={{ width: "300px" }}
                        />
                      </div>
                      <div className="col-lg-6">
                        <div className="d-block rounded-5 bg-light mb-6">
                          <div className="pb-5 px-5 px-lg-3 px-xl-5">
                            <div className="pt-8 mb-3 d-flex align-items-center">
                              <div className="mb-4">
                                <h4 className="fw-bold mb-0">Hi Digiers,</h4>
                                <p className="mb-0 font-size-16">
                                  Silahkan login menggunakan email dan password
                                  yang tertera pada email
                                </p>
                              </div>
                            </div>
                            <a
                              href="/login"
                              // onClick={() => {
                              //   router.replace("/auth/user-profile");
                              // }}
                              className="btn rounded-pill font-size-14 btn-blue btn-block cursor-pointer mb-5"
                            >
                              LOGIN SEKARANG
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </>
              ) : (
                <>
                  <div className="col-lg-7 border rounded-5 border-0 border-lg border-xl mx-auto">
                    <div className="d-block rounded-5 w-100">
                      <div className="pt-6 px-5 px-lg-3 px-xl-5 text-center">
                        <h2 className="fw-bolder text-red text-center mb-8">
                          Verifikasi Gagal, Token Invalid
                        </h2>
                        <img
                          src={`/assets/img/illustrations/failed-illustration.jpg?v=${Date.now()}`}
                          className="img-fluid align-self-center pb-8"
                          style={{ width: "450px" }}
                        />
                      </div>
                    </div>
                  </div>
                </>
              )}
            </>
          )}
        </div>
      </div>
    </>
  );
}
