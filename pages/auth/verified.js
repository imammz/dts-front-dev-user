import React, { useEffect, useState } from "react";
import Link from "next/link";
import SSOInfo from "../../components/SSOSInfo";
import PortalService from "../../services/PortalService";

export default function Verified() {
  const [isLoadingGetLinks, setIsLoadingGetLinks] = useState(true);
  const [siteManajemenData, setSiteManajemenData] = useState(true);

  // ambil data link
  useEffect(async () => {
    let settingData = await PortalService.generalDataSiteManajamenen();
    // console.log(settingData)
    if (settingData.data?.success) {
      // console.log(settingData.data.result.Data)
      const {
        alamat,
        color,
        created_at,
        created_by,
        email,
        footer_logo,
        header_logo,
        koordinat,
        link,
        logo_description,
        notelp,
        social_media,
      } = settingData.data.result[0];
      setSiteManajemenData({
        alamat: alamat,
        color: JSON.parse(color),
        created_at: created_at,
        created_by: created_by,
        email: email,
        footer_logo: footer_logo,
        header_logo: header_logo,
        koordinat: koordinat?.split(","),
        logo_description: logo_description,
        notelp: JSON.parse(notelp),
        link: JSON.parse(link),
        social_media: JSON.parse(social_media),
      });
      // console.log(siteManajemenData)
    } else {
      settingData = await PortalService.generalDataSiteManajamenen(true);
      const {
        alamat,
        color,
        created_at,
        created_by,
        email,
        footer_logo,
        header_logo,
        koordinat,
        link,
        logo_description,
        notelp,
        social_media,
      } = settingData.data.result[0];
      setSiteManajemenData({
        alamat: alamat,
        color: JSON.parse(color),
        created_at: created_at,
        created_by: created_by,
        email: email,
        footer_logo: footer_logo,
        header_logo: header_logo,
        koordinat: koordinat?.split(","),
        logo_description: logo_description,
        notelp: JSON.parse(notelp),
        link: JSON.parse(link),
        social_media: JSON.parse(social_media),
      });
    }
    setIsLoadingGetLinks(false);
  }, []);
  return (
    <>
      <div className="container my-11">
        <div className="row">
          <div className="col-lg-5 col-xl-6 mb-6 mb-lg-0">
            {/* Illustrator */}
            <h2 className="text-green text-center mb-8">
              Selamat, Verifikasi Berhasil
            </h2>
            <img
              src="/assets/img/illustrations/illustration-11.svg"
              alt="..."
              className="img-fluid"
            />
          </div>
          <div className="col-lg-7 col-xl-6">
            <SSOInfo
              background="bg-info"
              links={siteManajemenData.link}
              isLoading={isLoadingGetLinks}
              CustomFoot={() => (
                <div className="form-row place-order p-5">
                  <Link href="/verify-user/complete-user-data">
                    <a className="btn btn-blue btn-block">
                      LENGKAPI PENDAFTARAN
                    </a>
                  </Link>
                </div>
              )}
            />
          </div>
        </div>
      </div>
    </>
  );
}
