import Flatpickr from "react-flatpickr";
import "flatpickr/dist/flatpickr.css";
import { Component } from "react";

export default class DatePicker extends Component {
  constructor() {
    super();

    this.state = {
      date: new Date(),
    };
  }

  render() {
    const { date } = this.state;
    return (
      <Flatpickr
        options={{ dateFormat: "d-m-Y" }}
        value={""}
        //onChange={date => { this.setState({date}) }}
        onChange={(selectedDates, dateStr, instance) => {
          const firstDate = selectedDates[0];
          console.log({ firstDate, dateStr });
        }}
      />
    );
  }
}
