import moment from "moment";
import { useEffect, useState } from "react";
import FormTestSubstansi from "../../../../components/Form/TestSubstansi";
import ProfilService from "../../../../services/ProfileService";
import NotificationHelper from "../../../../src/helper/NotificationHelper";

const Guidline = ({ setShowRenderer }) => {
  return (
    <>
      <div className="row pb-8">
        <div className="col-lg-12">
          <div className="card border px-6 py-8">
            <div className="row">
              <div className="col-lg-5 mb-lg-0">
                <h2 className="text-center mb-8">Panduan Pengerjaan Survey</h2>

                <img
                  src="/assets/img/illustrations/illustration-11.svg"
                  className="img-fluid pt-5"
                />
              </div>
              <div className="col-lg-7">
                <div className="d-block rounded bg-info">
                  <div className="pt-6 px-5">
                    <div className="d-flex align-items-center">
                      <div>
                        <h4 className="mb-4">Harap dibaca :</h4>
                        <p>
                          Sebelum mengerjakan tes, harap perhatikan dan lakukan
                          hal-hal berikut
                        </p>
                        <ul
                          className="list-style-v2 list-unstyled"
                          style={{ textAlign: "left" }}
                        >
                          <li>
                            Alokasi waktu yang diberikan untuk mengerjakan Tes
                            Substansi sesuai dengan masing-masing tema
                            pelatihan. Informasi tersebut dapat di akses pada
                            dashboard Tes Substansi.
                          </li>
                          <li>
                            Peserta wajib menjawab seluruh soal Tes Substansi
                            dan jumlah soal sesuai dengan masing-masing tema
                            pelatihan. Tidak ada nilai negatif untuk jawaban
                            yang salah.
                          </li>
                          <li>
                            Setelah Tes Substansi dimulai, waktu tes tidak dapat
                            diberhentikan dan tes tidak dapat diulang. Setelah
                            waktu habis, halaman soal akan tertutup secara
                            otomatis.
                          </li>
                          <li>
                            Skor untuk soal yang sudah dijawab tetap terhitung
                            walaupun peserta belum menekan tombol submit atau
                            peserta mengalami force majeure.
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div className="form-row place-order p-5">
                    <a
                      onClick={() => {
                        setShowRenderer(true);
                      }}
                      className="btn btn-blue btn-block"
                    >
                      <i className="fa fa-play me-2"></i>MULAI SURVEI
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const Timer = ({ waktu_mulai, waktu_sekarang, waktu_selesai }) => {
  const [totalDuration, setTotalDuration] = useState();
  const [currentDuration, setCurrentDuration] = useState(0);

  useEffect(() => {
    let interval;
    if (waktu_mulai && waktu_sekarang && waktu_selesai) {
      const wc = moment(waktu_sekarang);
      const ws = moment(waktu_mulai);
      const we = moment(waktu_selesai);

      setTotalDuration(we.diff(ws, "minutes"));
      setCurrentDuration(we.diff(wc));
      interval = setInterval(() => {
        // console.log(currentDuration)
        if (currentDuration != 0) {
          // console.log("tidak sama dengan no")
          setCurrentDuration((c) => {
            return c - 1000;
          });
        } else {
          window.alert("waktu habis");
        }
      }, 1000);
    }

    return () => {
      clearInterval(interval);
    };
  }, [waktu_mulai, waktu_sekarang, waktu_selesai]);

  return (
    <div className="card border px-5 py-5 mb-5">
      <h5 className="mb-0">
        Waktu <small className="font-size-sm">({totalDuration} menit)</small>
      </h5>
      <div className="col">
        <h4 className="text-green my-0">
          {moment.utc(currentDuration).format("HH:mm:ss")}
        </h4>
      </div>
    </div>
  );
};

const FormRenderer = ({ showRenderer }) => {
  const [namaSurvey, setNamaSurvey] = useState("");
  const [listSoal, setListSoal] = useState([]);
  const [currentNumber, setCurrentNumber] = useState(1);
  const [namaAkademi, setNamaAkademi] = useState("");
  const [namaPelatihan, setNamaPelatihan] = useState("");
  const [waktu, setWaktu] = useState({
    waktu_mulai: "2022-08-01 07:00:00",
    waktu_sekarang: "2022-08-01 07:30:00",
    waktu_selesai: "2022-08-01 09:00:00",
  });

  const [isLoading, setIsLoading] = useState(true);

  useEffect(async () => {
    // console.log("itsshonw")
    try {
      if (showRenderer) {
        const resp = await ProfilService.getPertanyaanSurvey();

        if (resp.data.success) {
          const {
            akademi_nama,
            pertanyaan,
            pelatihan_nama,
            waktu_mulai,
            waktu_selesai,
            waktu_sekarang,
            survey_nama,
          } = resp.data.result.data;
          setNamaSurvey(survey_nama);
          setNamaAkademi(akademi_nama);
          setNamaPelatihan(pelatihan_nama);
          pertanyaan.map((v, i) => {
            v.jawaban = "";
            v.seen = i == 0 ? true : false;
          });
          // console.log(pertanyaan)
          setListSoal(pertanyaan);
          setWaktu({
            waktu_mulai: waktu_mulai,
            waktu_sekarang: waktu_sekarang,
            waktu_selesai: waktu_selesai,
          });
        }
      }
    } catch (error) {}
  }, [showRenderer]);

  useEffect(() => {
    let soals = [...listSoal];
    soals[currentNumber - 1] = { ...soals[currentNumber - 1], seen: true };
    setListSoal(soals);
  }, [currentNumber]);

  const onJawab = (v) => {
    // console.log(v)
    let soals = [...listSoal];
    soals[currentNumber - 1] = { ...soals[currentNumber - 1], jawaban: v };
    setListSoal(soals);
  };

  const submitJawban = () => {
    NotificationHelper.Success({ message: "Berhasil submit" });
  };

  return (
    <>
      <div className="container pb-8">
        <div className="row">
          <div className="col-lg-12">
            <div className="row">
              <div className="col-lg-9 mb-5">
                <div className="card border px-6 py-5">
                  <div className="d-block">
                    <div className="pb-5">
                      <div className="row align-items-end pb-5 mb-3">
                        <div className="col-md mb-md-0">
                          <h5 className="fw-semi-bold text-muted mb-0">
                            Survey
                          </h5>
                          <span className="text-dark font-size-sm fw-normal">
                            {namaSurvey}
                          </span>
                        </div>
                        <div className="col-md-auto">
                          <span className="text-dark font-size-sm fw-normal">
                            {namaPelatihan}
                          </span>{" "}
                        </div>
                      </div>
                      <hr />
                      <div className="d-flex align-items-center">
                        <div className="col-12 pt-5">
                          <h6 className="mb-0 text-muted">
                            Soal {currentNumber} dari {listSoal.length}
                          </h6>
                          <FormTestSubstansi
                            {...listSoal[currentNumber - 1]}
                            onChange={onJawab}
                          ></FormTestSubstansi>
                        </div>
                      </div>
                    </div>
                    <div className="row gx-2 mt-8">
                      <div className="col-md mb-md-0">
                        {listSoal[currentNumber - 1]?.jawaban != "" ? (
                          <>
                            {" "}
                            <a className="btn btn-green btn-sm px-8 mb-5 font-size-12">
                              <i className="fa fa-check"></i>
                              <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                                JAWAB
                              </span>
                            </a>
                          </>
                        ) : (
                          <></>
                        )}
                      </div>
                      <div className="col-auto align-self-start">
                        {currentNumber != 1 && (
                          <a
                            onClick={() => {
                              setCurrentNumber((c) => {
                                return c - 1;
                              });
                            }}
                            className="btn btn-outline-secondary btn-sm px-3 mb-3 font-size-12"
                          >
                            <i className="fa fa-chevron-left"></i>
                            <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                              Sebelumnya
                            </span>
                          </a>
                        )}
                      </div>
                      <div className="col-auto align-self-start">
                        {currentNumber != listSoal.length ? (
                          <a
                            onClick={() => {
                              setCurrentNumber((c) => {
                                return c + 1;
                              });
                            }}
                            className="btn btn-outline-secondary btn-sm px-3 mb-3 font-size-12"
                          >
                            <i className="fa fa-chevron-right"></i>
                            <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                              Selanjutnya
                            </span>
                          </a>
                        ) : (
                          <>
                            <a
                              onClick={submitJawban}
                              className="btn btn-success btn-sm px-3 mb-3 font-size-12"
                            >
                              <i className="fa fa-check-double"></i>
                              <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                                Selesai
                              </span>
                            </a>
                          </>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 border-left">
                {/* <Timer {...waktu}></Timer> */}
                <div className="card border px-4 py-5 mb-5">
                  <h5>Pertanyaan</h5>
                  <div className="col">
                    {listSoal.map((soal, i) => {
                      let n = i + 1;
                      return (
                        <a
                          className="cursor-pointer"
                          onClick={() => {
                            setCurrentNumber(n);
                          }}
                        >
                          <span
                            className={`badge ${
                              n == currentNumber
                                ? "bg-green"
                                : soal.seen
                                  ? soal.jawaban
                                    ? "bg-blue"
                                    : "bg-red"
                                  : "badge-primary-soft"
                            } w-40p p-2 mb-2 mx-1`}
                          >
                            {n}
                          </span>
                        </a>
                      );
                    })}
                  </div>
                  <hr />
                  <ul className="list mb-0">
                    <li className="list-item">
                      <span className="badge bg-blue">Sudah dijawab</span>
                    </li>
                    <li className="list-item">
                      <span className="badge bg-red">Belum dijawab</span>
                    </li>
                    <li className="list-item">
                      <span className="badge bg-green">Soal Saat ini</span>
                    </li>
                    <li className="list-item">
                      <span className="badge badge-primary-soft">
                        Belum dikerjakan
                      </span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default function Survey({}) {
  const [showRenderer, setShowRenderer] = useState(false);

  return (
    <div className="container pt-8">
      {showRenderer ? (
        <FormRenderer showRenderer={showRenderer}></FormRenderer>
      ) : (
        <Guidline setShowRenderer={setShowRenderer}></Guidline>
      )}
    </div>
  );
}
