import Link from "next/link";
import { useRouter } from "next/router";
import React, { memo, useContext, useEffect, useRef, useState } from "react";
import PortalService from "../../../../services/PortalService";
import NotificationHelper from "../../../../src/helper/NotificationHelper";

import { useLottie } from "lottie-react";
import * as animation from "../../../../public/assets/json/loading-animation.json";
import SurveyService from "../../../../services/SurveyService";
import { CacheHelperV2 as CacheHelper } from "../../../../src/helper/CacheHelper";
import EncryptionHelper from "../../../../src/helper/EncryptionHelper";
import PortalHelper from "../../../../src/helper/PortalHelper";
import Skeleton from "react-loading-skeleton";

const Context = React.createContext();

const BelumAdaData = ({ message = "Belum Ada Data" }) => {
  return (
    <>
      <div className="container py-8">
        <div className="row">
          <div className="col-12 mx-auto text-center">
            <h2>{message}</h2>
            {/* <p className="font-size-lg mb-6">Pelatihan pada akademi <span className="text-blue font-weight-semi-bold">{AkademiDetail.nama}</span> belum tersedia</p> */}
            <img
              src="/assets/img/empty-state/not-found.png"
              alt="..."
              className="img-fluid align-self-center"
              style={{ width: "500px" }}
            />
          </div>
        </div>
      </div>
    </>
  );
};

const Selesai = ({
  namaTest = "",
  namaPelatihan = "",
  namaAkademi = "",
  idPelatihan,
  idTest,
  category = "",
  score = 70,
}) => {
  // const [skor, setSkor] = useState()

  // useEffect(async () => {
  //     const resp = await ProfilService.profilSubstansiStart(idPelatihan, idTest)
  //     console.log(resp)
  // }, [])

  return (
    <>
      <div className="container mb-8">
        <div className="row py-8">
          <div className="col-lg-7 border rounded-5 border-lg border-xl mx-auto">
            <div className="d-block rounded-5 w-100">
              <div className="pt-6 px-5 px-lg-3 px-xl-5 text-center">
                <img
                  src="/assets/img/empty-state/finished-survey.png"
                  className="img-fluid align-self-center mb-5"
                  style={{ width: "400px" }}
                />
                <div className="row">
                  <div className="col-12 px-4">
                    <h2 className="fw-bold text-green mb-0">Survey selesai</h2>
                    <p>
                      Terima kasih atas partisipasi kamu dalam mengikuti
                      <br />
                      <strong>{namaTest}</strong>
                    </p>
                    <div className="mb-5">
                      <div className="my-7">
                        <Link
                          href={{
                            pathname: "/auth/user-profile",
                            query: { menu: "survey" },
                          }}
                        >
                          <a className="btn btn-md py-2 rounded-pill font-size-14 btn-primary">
                            TUTUP
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const FormRendererHeaderMobile = () => {
  const headerMobileSubstansi = useRef();
  const {
    namaTest,
    namaPelatihan,
    namaAkademi,
    isHeaderSticky,
    setIsHeaderSticky,
  } = useContext(Context);

  useEffect(() => {
    window.onscroll = function () {
      var sticky = headerMobileSubstansi.current?.getBoundingClientRect().top;

      if (window.pageYOffset > sticky) {
        setIsHeaderSticky(true);
      } else {
        setIsHeaderSticky(false);
      }
    };

    return () => {
      window.onscroll = true;
    };
  }, []);

  return (
    <div
      id="headerMobileSubstansi"
      ref={headerMobileSubstansi}
      style={
        isHeaderSticky
          ? {
              position: "fixed",
              top: 0,
              width: "100%",
              zIndex: 1044,
            }
          : {}
      }
      className="bg-white shadow "
    >
      <div className="row align-items-end px-4 py-4 d-none">
        <div className="col-md mb-md-0 ">
          <h5 className="fw-semi-bold text-muted mb-0">{namaTest}</h5>
          <span className="text-dark font-size-sm fw-normal line-clamp-1">
            {namaPelatihan}
          </span>
        </div>
        <div className="col-md-auto">
          <span className="text-dark font-size-sm fw-normal line-clamp-1">
            {namaAkademi}
          </span>
        </div>
        {/* <div className="col-12 text-end">
        <Timer {...waktu} textOnly={true}></Timer>
      </div> */}
      </div>
    </div>
  );
};

const letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];

const ObjectiveOptions = ({ data = [] }) => {
  const { selectedPertanyaanIdx, jawaban, setJawaban } = useContext(Context);

  return data.map((opt, i) => (
    <div className="form-check" key={i}>
      <input
        className="form-check-input"
        onChange={() => setJawaban(selectedPertanyaanIdx, opt.key)}
        value={opt.key}
        type="radio"
        name={"soal-" + selectedPertanyaanIdx}
        id={"opt-" + selectedPertanyaanIdx + "-" + i}
        checked={opt.key == jawaban[selectedPertanyaanIdx]}
      />
      <label
        className="form-check-label mb-2 cursor-pointer"
        htmlFor={"opt-" + selectedPertanyaanIdx + "-" + i}
      >
        {opt.option && (
          <span className="mb-4">{`${opt.key}. ${opt.option}`}</span>
        )}
        <br />
        <br />
        {opt.image && PortalHelper.isBase64Data(opt.image) && (
          <img
            src={opt.image}
            alt={"Gambar " + opt.key}
            className="img-thumbnail mb-4"
          />
        )}
      </label>
    </div>
  ));
};

const SkalaOptions = ({ data = [] }) => {
  const { selectedPertanyaanIdx, jawaban, setJawaban } = useContext(Context);
  return data.map((opt, i) => (
    <>
      <button
        className={
          opt.key == jawaban[selectedPertanyaanIdx]
            ? "jawaban active btn btn-primary form-control-sm m-1 text-white"
            : "jawaban btn btn-primary form-control-sm m-1 text-white"
        }
        name={"soal-" + selectedPertanyaanIdx}
        id={"opt-" + selectedPertanyaanIdx + "-" + i}
        type="button"
        onClick={() => {
          setJawaban(selectedPertanyaanIdx, opt.key);
        }}
      >
        {opt.option}
      </button>
    </>
  ));
};

const MultipleChoiceOption = ({ data = [] }) => {
  const { selectedPertanyaanIdx, jawaban, setJawaban } = useContext(Context);
  let currentJawaban = jawaban[selectedPertanyaanIdx] || [];

  return data.map((opt, i) => (
    <div className="form-check" key={i}>
      <input
        className="form-check-input"
        value={opt.key}
        type="checkbox"
        name={"soal-" + selectedPertanyaanIdx}
        id={"opt-" + selectedPertanyaanIdx + "-" + i}
        checked={currentJawaban.includes(opt.key)}
        onChange={(e) => {
          if (e.target.checked) currentJawaban = [...currentJawaban, opt.key];
          else currentJawaban = currentJawaban.filter((key) => key != opt.key);
          setJawaban(selectedPertanyaanIdx, currentJawaban);
        }}
      />
      <label
        className="form-check-label mb-2 cursor-pointer"
        htmlFor={"opt-" + selectedPertanyaanIdx + "-" + i}
      >
        {opt.option && (
          <span className="mb-4">{`${opt.key}. ${opt.option}`}</span>
        )}
        <br />
        <br />
        {opt.image && PortalHelper.isBase64Data(opt.image) && (
          <img
            src={opt.image}
            alt={"Gambar " + opt.key}
            className="img-thumbnail mb-4"
          />
        )}
      </label>
    </div>
  ));
};

const TerbukaField = () => {
  const { selectedPertanyaanIdx, jawaban, setJawaban, ...rest } =
    useContext(Context);
  // console.log({selectedPertanyaanIdx, jawaban, setJawaban, ...rest})

  return (
    <input
      type="textarea"
      className="form-control"
      rows="3"
      name={"soal-" + selectedPertanyaanIdx}
      id={"opt-" + selectedPertanyaanIdx}
      value={jawaban[selectedPertanyaanIdx] || ""}
      onChange={(e) => setJawaban(selectedPertanyaanIdx, e.currentTarget.value)}
    />
  );
};

const TriggeredOption = ({ data, level = 0, index = 0 }) => {
  data = (data || []).map((d, i) => ({ ...d, key: d?.key || letters[i] }));

  let { selectedPertanyaanIdx, jawaban, setJawaban } = useContext(Context);
  let currentJawaban = jawaban[selectedPertanyaanIdx] || {};
  currentJawaban[level] = currentJawaban[level] || {};

  let currentOption = data.find(
    (opt) => opt.key == currentJawaban[level][index],
  );

  return (
    <>
      {data.map((opt, i) => {
        return (
          <div className="form-check" key={i}>
            <input
              className="form-check-input"
              value={opt.key}
              type="radio"
              name={"soal-" + selectedPertanyaanIdx + "-" + level + "-" + index}
              id={
                "opt-" +
                selectedPertanyaanIdx +
                "-" +
                level +
                "-" +
                index +
                "-" +
                i
              }
              checked={opt.key == currentJawaban[level][index]}
              onChange={() => {
                currentJawaban[level][index] = opt.key;

                // Object.keys(currentJawaban).forEach((ilevel) => {
                //   if (parseInt(ilevel) > level)
                //     delete currentJawaban[parseInt(ilevel)];
                // });
                setJawaban(selectedPertanyaanIdx, currentJawaban);
              }}
            />
            <label
              className="form-check-label mb-2 cursor-pointer"
              htmlFor={
                "opt-" +
                selectedPertanyaanIdx +
                "-" +
                level +
                "-" +
                index +
                "-" +
                i
              }
            >
              {opt.key && (
                <span className="mb-4">{`${opt.key}. ${opt.option}`}</span>
              )}
              <br />
              <br />
              {opt.image && PortalHelper.isBase64Data(opt.image) && (
                <img
                  src={opt.image}
                  alt={"Gambar " + opt.key}
                  className="img-thumbnail mb-4"
                />
              )}
            </label>
          </div>
        );
      })}
      {currentOption?.is_next &&
        (currentOption?.sub || []).map((s, idx) => {
          s = {
            ...s,
            pertanyaan: s?.question,
            pertanyaan_gambar: s?.image,
            jawaban: JSON.stringify(s?.answer),
            type: "triggered_question",
            level: level + 1,
          };

          return <FormRendererFormSurvei data={s} index={idx} />;
        })}
    </>
  );
};

const FormRendererFormSurvei = memo(({ data, index }) => {
  let jawaban = [];
  try {
    jawaban = JSON.parse(`${data?.answer}`);
  } catch (err) {
    try {
      jawaban = data?.answer;
    } catch (err2) {}
  }

  return (
    <>
      <div className="mb-5">
        {data?.question && <h4 className="mb-0">{data?.question}</h4>}
        {PortalHelper.isBase64Data(data?.question_image) && (
          <img
            src={data?.question_image}
            alt="..."
            className="img-thumbnail my-5"
          />
        )}
      </div>

      {data?.type == "objective" && <ObjectiveOptions data={jawaban} />}
      {data?.type == "skala" && (
        <div className="btn-group">
          <div>
            <SkalaOptions data={jawaban} />
            <div className="col-lg-12 row">
              <div className="col-lg-6 text-start">Tidak Merekomendasikan</div>
              <div className="col-lg-6 text-end pe-0">
                Sangat Merekomendasikan
              </div>
            </div>
          </div>
        </div>
      )}
      {data?.type == "multiple_choice" && (
        <MultipleChoiceOption data={jawaban} />
      )}
      {data?.type == "pertanyaan_terbuka" && <TerbukaField />}
      {data?.type == "triggered_question" && (
        <TriggeredOption
          data={jawaban}
          level={data?.level || 0}
          index={index}
        />
      )}
    </>
  );
});

const validateTriggeredHasAnswer = (options, answers) => {
  for (let opt of options) {
    if (!answers) {
      return false;
    }

    if (!answers[0]) {
      return false;
    }

    if (!answers[0][0]) {
      return false;
    }

    if (opt.key == answers[0][0]) {
      if (!opt.is_next) return true;

      let subQuestionsAnswered = (opt?.sub || [])
        .map((sq, idx) => ({ ...sq, idx }))
        .map((sq) => !!answers[1][sq.idx]);

      return subQuestionsAnswered.every((v) => v);
    }
  }

  return false;
};

const FormRendererFormNav = ({
  dataPertanyaan,
  jawaban,
  validateTriggeredHasAnswer,
  selectedPertanyaanIdx,
  setSelectedPertanyaanIdx,
}) => {
  dataPertanyaan = dataPertanyaan.map((soal, i) => {
    let currentJawaban = jawaban[i];
    var isAnswered = true;
    if (soal.type == "triggered_question") {
      let soalPertanyaan = JSON.parse(soal.answer);
      isAnswered = validateTriggeredHasAnswer(soalPertanyaan, currentJawaban);
      if (!isAnswered) {
        var isAnswered = false;
      }
    } else if (soal.type == "multiple_choice") {
      if (
        !currentJawaban ||
        !Array.isArray(currentJawaban) ||
        currentJawaban.length == 0 ||
        currentJawaban == undefined
      ) {
        isAnswered = false;
      }
    } else {
      if (!currentJawaban) {
        isAnswered = false;
      }
    }

    return isAnswered;
  });

  return (
    <div className="col-lg-3 border-left">
      <div className="card border rounded-5 px-4 py-5 mb-5">
        <h5 class="fw-bold text-muted">Soal</h5>
        <div className="col">
          {dataPertanyaan.map((isAnswered, i) => {
            let disabled = true;
            if (i == 0) disabled = false;
            else disabled = !dataPertanyaan[i - 1];

            return (
              <a
                key={i}
                className={`${disabled ? "disabled" : "cursor-pointer"}`}
                disabled={disabled}
                onClick={() => {
                  if (!disabled) setSelectedPertanyaanIdx(i);
                }}
              >
                <span
                  className={`badge font-size-14 ${
                    i == selectedPertanyaanIdx
                      ? "bg-green"
                      : isAnswered
                        ? "bg-blue"
                        : "bg-red"
                  } w-40p p-2 mb-2 mx-1`}
                >
                  {i + 1}
                </span>
              </a>
            );
          })}
        </div>
        <hr />
        <ul className="list mb-0">
          <li className="list-item">
            <span className="badge bg-blue">Sudah dijawab</span>
          </li>
          <li className="list-item">
            <span className="badge bg-red">Belum dijawab</span>
          </li>
          <li className="list-item">
            <span className="badge bg-green">Soal Saat ini</span>
          </li>
          <li className="list-item">
            <span className="badge badge-primary-soft">Belum dikerjakan</span>
          </li>
        </ul>
      </div>
    </div>
  );
};

const FormRendererFormNext = ({
  dataPertanyaan,
  jawaban,
  validateTriggeredHasAnswer,
  selectedPertanyaanIdx,
  setSelectedPertanyaanIdx,
}) => {
  dataPertanyaan = dataPertanyaan.map((soal, i) => {
    let currentJawaban = jawaban[i];
    var isAnswered = true;
    if (soal.type == "triggered_question") {
      let soalPertanyaan = JSON.parse(soal.answer);
      isAnswered = validateTriggeredHasAnswer(soalPertanyaan, currentJawaban);
      if (!isAnswered) {
        var isAnswered = false;
      }
    } else if (soal.type == "multiple_choice") {
      if (
        !currentJawaban ||
        !Array.isArray(currentJawaban) ||
        currentJawaban.length == 0 ||
        currentJawaban == undefined
      ) {
        isAnswered = false;
      }
    } else {
      if (!currentJawaban) {
        isAnswered = false;
      }
    }

    return isAnswered;
  });

  let disabled = !dataPertanyaan[selectedPertanyaanIdx];

  return (
    <a
      onClick={() => {
        if (!disabled) setSelectedPertanyaanIdx(selectedPertanyaanIdx + 1);
      }}
      className={`btn btn-primary rounded-pill btn-sm px-3 mb-3 font-size-12 ${
        disabled ? "disabled" : ""
      }`}
    >
      <i className="fa fa-chevron-right"></i>
      <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
        Selanjutnya
      </span>
    </a>
  );
};

const FormRendererFormSubmit = ({
  dataPertanyaan,
  jawaban,
  validateTriggeredHasAnswer,
  updateStatPengerjaan,
  setShowModalResumePengerjaan,
}) => {
  // check if all question answered
  let isAllAnswered = true;
  let soalBelumDijawab = [];
  dataPertanyaan.forEach((soal, i) => {
    if (soal.type == "triggered_question") {
      let soalPertanyaan = JSON.parse(soal.answer);

      // Using function validateAnswer
      let isAnswered = validateTriggeredHasAnswer(soalPertanyaan, jawaban[i]);
      if (!isAnswered) {
        isAllAnswered = false;
        soalBelumDijawab.push(i + 1);
      }
    } else if (soal.type == "multiple_choice") {
      if (
        !jawaban[i] ||
        !Array.isArray(jawaban[i]) ||
        jawaban[i].length == 0 ||
        jawaban[i] == undefined
      ) {
        isAllAnswered = false;
        soalBelumDijawab.push(i + 1);
      }
    } else {
      if (!jawaban[i]) {
        isAllAnswered = false;
        soalBelumDijawab.push(i + 1);
      }
    }
  });

  return (
    <a
      onClick={() => {
        if (!isAllAnswered) {
          alert(
            `Anda belum menjawab soal nomor ${soalBelumDijawab.join(", ")}`,
          );
        } else {
          updateStatPengerjaan();
          setShowModalResumePengerjaan(true);
        }
      }}
      className={`btn btn-primary rounded-pill btn-sm px-3 mb-3 font-size-12 ${
        !isAllAnswered ? "disabled" : ""
      }`}
    >
      <i className="fa fa-paper-plane me-1"></i>
      <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
        Submit Survey
      </span>
    </a>
  );
};

const FormRendererForm = () => {
  const {
    isHeaderSticky,
    namaPelatihan,
    namaAkademi,
    namaTest,
    dataPertanyaan,
    totalPertanyaan,
    selectedPertanyaanIdx,
    setSelectedPertanyaanIdx,
    jawaban,
    updateStatPengerjaan,
    setShowModalResumePengerjaan,
  } = useContext(Context);

  return (
    <div
      id="fieldPengerjaanSubstansi"
      className={`container my-8 ${isHeaderSticky ? "py-8" : ""} py-md-0 `}
    >
      <div className="row">
        <div className="col-lg-12">
          <div className="row">
            <div className="col-lg-9 mb-5">
              <div className="card border rounded-5 border-lg border-xl px-6 py-5">
                <div className="d-block">
                  <div className="pb-5">
                    <div className="row align-items-end pb-5 mb-3">
                      <div className="col-md mb-md-0 ">
                        <h3 className="fw-bold text-dark mb-0">
                          Survey {namaTest}
                        </h3>
                        <p className="text-muted font-size-14 fw-bold mb-0">
                          {namaPelatihan}
                        </p>
                        <p className="text-muted font-size-14">{namaAkademi}</p>
                      </div>
                    </div>
                    <hr className="d-none d-md-block" />
                    <div className="d-flex align-items-center">
                      <div className="col-12 pt-5">
                        <h6 className="mb-0 fw-semi-bold text-muted">
                          Soal {selectedPertanyaanIdx + 1} dari{" "}
                          {totalPertanyaan}
                        </h6>
                        <FormRendererFormSurvei
                          data={dataPertanyaan[selectedPertanyaanIdx]}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="row gx-2 mt-8">
                    {/* <div className="col-md mb-md-0">
                    {listSoal[currentNumber - 1]?.answer_key != "" ? (
                      <>
                        <a className="btn btn-green btn-sm px-8 mb-5 font-size-14 pe-none">
                          <i className="fa fa-check"></i>
                          <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                            SUDAH DIJAWAB
                          </span>
                        </a>
                      </>
                    ) : (
                      <></>
                    )}
                  </div> */}
                    <div className="col-auto align-self-start">
                      {selectedPertanyaanIdx > 0 && (
                        <a
                          onClick={() => {
                            setSelectedPertanyaanIdx(selectedPertanyaanIdx - 1);
                          }}
                          className="btn btn-outline-secondary rounded-pill btn-sm px-3 mb-3 font-size-12"
                        >
                          <i className="fa fa-chevron-left"></i>
                          <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                            Sebelumnya
                          </span>
                        </a>
                      )}
                    </div>
                    <div className="col-auto align-self-start">
                      {selectedPertanyaanIdx < totalPertanyaan - 1 ? (
                        <FormRendererFormNext
                          {...{
                            dataPertanyaan,
                            jawaban,
                            validateTriggeredHasAnswer,
                            selectedPertanyaanIdx,
                            setSelectedPertanyaanIdx,
                          }}
                        />
                      ) : (
                        <FormRendererFormSubmit
                          {...{
                            dataPertanyaan,
                            jawaban,
                            validateTriggeredHasAnswer,
                            updateStatPengerjaan,
                            setShowModalResumePengerjaan,
                          }}
                        />
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <FormRendererFormNav
              {...{
                dataPertanyaan,
                jawaban,
                validateTriggeredHasAnswer,
                selectedPertanyaanIdx,
                setSelectedPertanyaanIdx,
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

const ModalLoading = ({
  show,
  handleClose,
  loadingMessage = "Loading ...",
  backdropOnly = true,
}) => {
  const toggleOpenLoadignModal = useRef(null);
  const toggleCloseLoadingModal = useRef(null);
  const showLoadingModal = useRef(null);

  const options = {
    animationData: animation,
    loop: true,
  };
  const lottie = useLottie(options);
  lottie.setSpeed(2);
  const { View } = lottie;

  useEffect(() => {
    // console.log(show)
    showLoadingModal.current?.addEventListener("hidden.bs.modal", () => {
      handleClose();
    });
    if (show) {
      toggleOpenLoadignModal.current?.click();
    } else {
      // console.log("handle show loading: ", show);
      toggleCloseLoadingModal.current.click();
      // showLoadingModal.current.addEventListener('shown.bs.modal', () => {
      //     if (!show) {
      //     }
      // })
    }
  }, [show]);

  return (
    <>
      <a
        href="#"
        ref={toggleOpenLoadignModal}
        data-bs-toggle="modal"
        data-bs-target="#loadingModalTest"
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseLoadingModal}
        data-bs-dismiss="modal"
        data-bs-target="#loadingModalTest"
        hidden
      ></a>

      <div
        className="modal"
        id="loadingModalTest"
        ref={showLoadingModal}
        data-bs-backdrop="static"
        data-bs-keyboard="false"
        role="dialog"
      >
        {!backdropOnly && (
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">
              <div className="modal-body" style={{ textAlign: "center" }}>
                <div className="w-100">
                  {/* {View} */}
                  <img
                    src="/assets/img/GIF-DTS.gif"
                    alt="loading gif"
                    className="w-50"
                  />
                  <h2
                    className="fw-bold text-center mb-1 text-primary"
                    id="title"
                  >
                    {loadingMessage}
                  </h2>
                </div>
              </div>
            </div>
          </div>
        )}
        {backdropOnly && (
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-body" style={{ background: "transparent" }}>
              <div className="w-100" style={{ textAlign: "center" }}>
                {/* {View} */}

                <img
                  src="/assets/img/GIF-DTS.gif"
                  alt="loading gif"
                  className="w-50"
                />
                <h2 className="fw-bold text-center mb-1 text-white" id="title">
                  {loadingMessage}
                </h2>
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

const ModalResumePengerjaan = () => {
  const state = useContext(Context);
  const {
    showModalResumePengerjaan: show,
    setShowModalResumePengerjaan,
    submitJawaban,
    namaPelatihan,
    namaAkademi,
    statPengerjaan,
  } = state;
  const [user] = useState(PortalHelper.getUserData());
  const [idModal] = useState("modalResumePengerjaan");

  const toggleOpenModal = useRef(null);
  const toggleCloseModal = useRef(null);
  const showSuccessModal = useRef(null);

  useEffect(() => {
    showSuccessModal.current.addEventListener("hidden.bs.modal", () => {
      setShowModalResumePengerjaan(false);
    });
    if (show && idModal) {
      toggleOpenModal.current?.click();
    } else {
      toggleCloseModal.current?.click();
    }
  }, [show]);

  return (
    <>
      <a
        href="#"
        ref={toggleOpenModal}
        data-bs-toggle="modal"
        data-bs-target={`#${idModal}`}
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseModal}
        data-bs-dismiss="modal"
        data-bs-target={`#${idModal}`}
        hidden
      ></a>

      <div
        className="modal fade"
        id={`${idModal}`}
        ref={showSuccessModal}
        tabIndex={-1}
        role="dialog"
      >
        <div
          className="modal-dialog modal-dialog-centered modal-fullscreen-sm-down"
          role="document"
        >
          <div className="modal-content">
            <div className="modal-header py-3 px-4">
              <h5 className="fw-bold modal-title" id="exampleModalLabel">
                Rekap Survey
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body py-3 px-4">
              <h3 className="fw-bold mt-5 mb-6 text-center">
                {state.namaTest}
              </h3>
              <div className="w-100">
                <div className="mx-auto d-flex justify-content-around">
                  <div>
                    <div className="rounded d-flex align-items-center justify-content-center bg-secondary w-100p h-100p">
                      <div>
                        <span className="stat-pengerjaan-jumlah text-white">
                          {statPengerjaan.totalSoal}
                        </span>
                      </div>
                    </div>
                    <p className="text-center fw-bold">Total</p>
                  </div>
                  <div>
                    <div className="rounded d-flex align-items-center justify-content-center bg-green w-100p h-100p">
                      <div>
                        <span className="stat-pengerjaan-jumlah text-white">
                          {statPengerjaan.terjawab}
                        </span>
                      </div>
                    </div>
                    <p className="text-center fw-bold">Dijawab</p>
                  </div>
                  <div>
                    <div className="rounded d-flex align-items-center justify-content-center bg-red w-100p h-100p">
                      <div>
                        <span className="stat-pengerjaan-jumlah text-white">
                          {statPengerjaan.belumDijawab}
                        </span>
                      </div>
                    </div>
                    <p className="text-center fw-bold">Belum Dijawab</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="modal-footer py-3 px-4 justify-content-center">
              <button
                type="button"
                className="btn btn-sm rounded-pill btn-outline-secondary"
                data-bs-dismiss="modal"
              >
                Batal
              </button>
              <button
                type="button"
                className={`btn btn-sm rounded-pill btn-green ${
                  statPengerjaan.belumDijawab > 0 ? "disabled" : ""
                }`}
                onClick={() => {
                  setShowModalResumePengerjaan(false);
                  submitJawaban(state);
                }}
              >
                <i className="fa fa-paper-plane me-1"></i>Kirim Survey
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const FormRenderer = () => {
  const router = useRouter();
  const {
    query: { idTest, idPelatihan },
  } = router;
  const state = useContext(Context);
  const { showRenderer, fetchPertanyaanSurvei, startSurvei } = state;

  React.useEffect(() => {
    if (router.isReady && showRenderer && idTest && idPelatihan) {
      startSurvei(state.idAkademi, idPelatihan, idTest);
    }
  }, [router.isReady, showRenderer, idTest, idPelatihan]);

  return (
    <>
      <ModalResumePengerjaan />
      <FormRendererHeaderMobile />
      <FormRendererForm />
    </>
  );
};

const Guidline = () => {
  const state = useContext(Context);

  return (
    <>
      <div className="container my-8">
        <div className="row">
          <div className="col-lg-12">
            <div className="card border rounded-5 border-lg border-xl px-6 py-8">
              <div className="row">
                <div className="col-lg-5 mb-6">
                  <h3 className="fw-bold text-center mb-4">
                    Survey <br></br>
                    {state.namaTest}
                  </h3>
                  <img
                    src="/assets/img/empty-state/panduan-survey.png"
                    className="img-fluid pt-5"
                  />
                </div>
                <div className="col-lg-7">
                  <div className="d-block rounded-5 bg-info">
                    <div className="pt-6 px-5">
                      <div className="d-flex align-items-center">
                        <div>
                          <h4 className="fw-semi-bold mb-4">Panduan:</h4>
                          <p>
                            Sebelum mengerjakan, harap membaca panduan berikut :
                          </p>
                          <ul
                            className="list-style-v2 list-unstyled"
                            style={{ textAlign: "left" }}
                          >
                            <li>
                              Peserta wajib mengisi seluruh pertanyaan pada
                              survey
                            </li>
                            <li>
                              Jawablah survey dengan sebenarnya, hal ini berguna
                              untuk evaluasi program pelatihan kedepannya
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div className="form-row place-order p-5">
                      <a
                        onClick={() => {
                          state?.setShowRenderer(true);
                        }}
                        className="btn btn-blue rounded-pill btn-block"
                      >
                        <i className="fa fa-play me-2"></i>MULAI SURVEI
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const InnerSurvey = () => {
  const state = useContext(Context);

  if (state.isError) return <BelumAdaData />;
  if (state.isLoading)
    return (
      <div className="container py-8">
        <Skeleton className="h-400p"></Skeleton>
      </div>
    );
  if (state.isSelesai)
    return (
      <Selesai
        score={state.score}
        namaPelatihan={state.namaPelatihan}
        namaTest={state.namaTest}
        namaAkademi={state.namaAkademi}
        idTest={state.idTest}
        idPelatihan={state.idPelatihan}
        category={state.category}
      />
    );
  if (state.showRenderer)
    return (
      <FormRenderer
        namaTest={state.namaTest}
        namaAkademi={state.namaAkademi}
        namaPelatihan={state.namaPelatihan}
        showRenderer={state.showRenderer}
        idTest={state.idTest}
        idPelatihan={state.idPelatihan}
        category={state.category}
      />
    );
  return <Guidline setShowRenderer={state.setShowRenderer}></Guidline>;
};

const Survey = () => {
  return (
    <>
      <Context.Consumer>
        {(state) => {
          let loadingMessage = "";
          if (state?.readingInfo)
            loadingMessage = "Sedang mempersiapkan survei";
          else if (state?.fetchingPertanyaan)
            loadingMessage = "Sedang memuat pertanyaan";
          else if (state?.submittingJawaban)
            loadingMessage = "Sedang menyimpan jawaban";

          return (
            <>
              <ModalLoading
                show={
                  state?.readingInfo ||
                  state?.fetchingPertanyaan ||
                  state?.submittingJawaban
                }
                handleClose={() => {
                  // NotificationHelper.Success({ message: "berhasil submit" })
                  // setIsLoading(false);
                }}
                loadingMessage={loadingMessage}
              ></ModalLoading>
              <InnerSurvey />
            </>
          );
        }}
      </Context.Consumer>
    </>
  );
};

const ContextSurvey = () => {
  const router = useRouter();
  const {
    query: { idTest, idPelatihan },
  } = router;
  const cacheId = "survey-" + idPelatihan + "-" + idTest;

  const [state, setState] = React.useState({
    showRenderer: false,
    setShowRenderer: (v) =>
      setState((state) => ({ ...state, showRenderer: v })),
    isSelesai: false,
    setIsSelesai: (v) => setState((state) => ({ ...state, isSelesai: v })),
    isLoading: true,
    setIsLoading: (v) => setState((state) => ({ ...state, isLoading: v })),
    isError: false,

    isHeaderSticky: false,
    setIsHeaderSticky: (v) =>
      setState((state) => ({ ...state, isHeaderSticky: v })),

    readingInfo: false,
    setReadingInfo: (v) => setState((state) => ({ ...state, readingInfo: v })),
    fetchingPertanyaan: false,
    dataPertanyaan: [],
    totalPertanyaan: 0,
    selectedPertanyaanIdx: 0,
    setSelectedPertanyaanIdx: (v) =>
      setState((state) => ({ ...state, selectedPertanyaanIdx: v })),
    jawaban: [],
    setJawaban: (idx, v) =>
      setState((state) => {
        let jawaban = state.jawaban;
        jawaban[idx] = v;
        return { ...state, jawaban };
      }),

    namaTest: "",
    namaPelatihan: "",
    setNamaPelatihan: (v) =>
      setState((state) => ({ ...state, namaPelatihan: v })),
    idAkademi: null,
    setIdAkademi: (v) => setState((state) => ({ ...state, idAkademi: v })),
    namaAkademi: "",
    setNamaAkademi: (v) => setState((state) => ({ ...state, namaAkademi: v })),
    category: "",
    score: 0,

    fetchPertanyaanSurvei: async (idTest) => {
      setState((state) => ({
        ...state,
        dataPertanyaan: [],
        totalPertanyaan: 0,
        fetchingPertanyaan: true,
      }));
      // const { total, data } = await ProfilService.getPertanyaanSurvey(idTest);

      setState((state) => {
        (async () => {
          const resSurveyBase = await SurveyService.base(
            state.idAkademi,
            idPelatihan,
            idTest,
          );
          if (resSurveyBase.status === 200 && resSurveyBase.data.result.data) {
            let data = [];
            try {
              data = JSON.parse(
                `${resSurveyBase?.data?.result?.data?.participant_answers}`,
              );
            } catch (err) {}

            // console.log("data", data);

            setState((state) => ({
              ...state,
              dataPertanyaan: data,
              totalPertanyaan: data?.length || 0,
              fetchingPertanyaan: false,
            }));
          }
        })();

        return state;
      });

      // setState((state) => ({ ...state, dataPertanyaan: data, totalPertanyaan: total, fetchingPertanyaan: false }));
    },
    startSurvei: (idAkademi, idPelatihan, idTest) => {
      (async () => {
        const resp = await SurveyService.start(idAkademi, idPelatihan, idTest);
        let data = [];
        if (resp.status === 200 && resp.data.result.data) {
          try {
            data = JSON.parse(
              `${resp?.data?.result?.data?.rparticipant_answers}`,
            );
            // console.log(data)
          } catch (err) {}

          setState((state) => ({
            ...state,
            dataPertanyaan: data,
            totalPertanyaan: data?.length || 0,
            fetchingPertanyaan: false,
          }));
        }

        if (data.length == 0) state.fetchPertanyaanSurvei(idTest);
      })();
    },

    statPengerjaan: {
      terjawab: 0,
      totalSoal: 0,
      belumDijawab: 0,
    },
    updateStatPengerjaan: () => {
      setState((state) => {
        let jawab = 0;
        let totalSoal = state.totalPertanyaan;
        for (let i = 0; i < totalSoal; i++) {
          let currentJawaban = state.jawaban[i];
          let isAnswered =
            currentJawaban != null ||
            (Array.isArray(currentJawaban) && currentJawaban.length == 0) ||
            (typeof currentJawaban == "object" &&
              Object.keys(currentJawaban || {}).length == 0);

          if (isAnswered) {
            jawab++;
          }
        }

        return {
          ...state,
          statPengerjaan: {
            terjawab: jawab,
            totalSoal: totalSoal,
            belumDijawab: totalSoal - jawab,
          },
        };
      });
    },
    showModalResumePengerjaan: false,
    setShowModalResumePengerjaan: (v) =>
      setState((state) => ({ ...state, showModalResumePengerjaan: v })),
    submittingJawaban: false,
  });

  useEffect(async () => {
    try {
      if (router.isReady) {
        state.setReadingInfo(true);

        let dt = await CacheHelper.GetFromCache({
          index: cacheId,
          force: true,
        });
        if (dt != undefined) {
          const {
            showRenderer,
            fetchingPertanyaan,
            submittingJawaban,
            ...savedState
          } = JSON.parse(EncryptionHelper.decrypt(dt));
          setState({ ...state, ...savedState });
        }

        const resp = await PortalService.pelatihanDetail(idPelatihan);
        // console.log(resp, idPelatihan);
        if (resp.data.success) {
          if (!resp.data.result.data)
            throw new Error("Pelatihan tidak ditemukan");

          const { akademi_id, nama_pelatihan, nama_akademi } =
            resp.data.result.data;

          const respd = await SurveyService.detail(
            akademi_id,
            idPelatihan,
            idTest,
          );
          if (respd?.data?.success) {
            if (!respd.data.result.data)
              throw new Error("Survei tidak ditemukan");

            // cek available pengerjaan
            if (
              respd?.data?.result?.data.status_peserta_survey.toLowerCase() ==
                "false" ||
              respd?.data?.result?.data.status_peserta_survey?.toLowerCase() ==
                false
            ) {
              NotificationHelper.Failed({
                message: "Anda belum bisa mengerjakan survei ini",
                onClose: () => {
                  router.back();
                },
                okText: "Kembali",
              });
              return;
            }
            if (respd?.data?.result?.data?.status == "sudah mengerjakan") {
              state.setIsSelesai(true);
            } else if (
              respd?.data?.result?.data?.status == "sedang mengerjakan"
            ) {
              state.setShowRenderer(true);
            } else {
              state.setIsSelesai(false);
            }
          } else {
            throw new Error("Survei tidak ditemukan");
          }

          setState((s) => ({
            ...s,
            namaTest: respd?.data?.result?.data.survey_nama,
          }));
          state.setIdAkademi(akademi_id);
          state.setNamaPelatihan(nama_pelatihan);
          state.setNamaAkademi(nama_akademi);
        } else {
          if (!resp.data.result.data)
            throw new Error("Gagal mengambil data pelatihan");
        }
      }
    } catch (err) {
      NotificationHelper.Failed({
        message: err.message,
        onClose: () => {
          router.back();
        },
        okText: "Kembali",
      });
    } finally {
      state.setIsLoading(false);
    }

    state.setReadingInfo(false);
  }, [router.isReady, idPelatihan, idTest, cacheId]);

  React.useEffect(() => {
    let dataSimpan = EncryptionHelper.encrypt(JSON.stringify(state));
    CacheHelper.CreateCache({ index: cacheId, data: dataSimpan });
  }, [JSON.stringify(state)]);

  return (
    <Context.Provider
      value={{
        ...state,
        submitJawaban: async () => {
          setState({ submittingJawaban: true });

          let dataPertanyaan = state.dataPertanyaan.map(
            ({ answer: opts, ...rest }, qIdx) => {
              let jawaban = state.jawaban[qIdx];

              try {
                opts = JSON.parse(`${opts}`);
              } catch (err) {
                opts = [];
              }

              if (rest?.type == "objective") rest.participant_answer = jawaban;
              if (rest?.type == "multiple_choice")
                rest.participant_answer = jawaban;
              if (rest?.type == "pertanyaan_terbuka")
                rest.participant_answer = jawaban;
              if (rest?.type == "triggered_question") {
                // if (!jawaban) jawaban = [];
                rest.participant_answer = jawaban[0][0];

                for (let optIdx = 0; optIdx < opts.length; optIdx++) {
                  if (opts[optIdx].key == jawaban[0][0]) {
                    for (
                      let subIdx = 0;
                      subIdx < (opts[optIdx].sub || []).length;
                      subIdx++
                    ) {
                      opts[optIdx].sub[subIdx].participant_answer =
                        jawaban[1][subIdx];
                    }
                  }
                }
              }

              return { answer: JSON.stringify(opts, null, 2), ...rest };
            },
          );

          await SurveyService.submit(
            state.idAkademi,
            idPelatihan,
            idTest,
            JSON.stringify(dataPertanyaan),
          );

          const userData = PortalHelper.getUserData();
          const surveys = (userData?.mandatory_survey || [])?.filter(
            (elem) => elem.question_bank_id != idTest && elem.type == "survey",
          );
          PortalHelper.updateUserData("mandatory_survey", surveys);

          await CacheHelper.ClearCache({ index: cacheId });
          NotificationHelper.Success({
            message: "Jawaban berhasil disubmit",
            onClose: () => {
              window.location.reload();
            },
          });

          setState({ submittingJawaban: false });
        },
      }}
    >
      <Survey />
    </Context.Provider>
  );
};

export default ContextSurvey;
