import React, { useState, useEffect, useRef, forwardRef } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import PortalHelper from "./../../src/helper/PortalHelper";
import ProfilService from "../../services/ProfileService";
import Skeleton from "react-loading-skeleton";
import FlipMove from "react-flip-move";

//
import { useForm, FormProvider } from "react-hook-form";
import GeneralService from "../../services/GeneralService";
import * as yup from "yup";
import Modal from "react-bootstrap/Modal";
import { yupResolver } from "@hookform/resolvers/yup";
import validationMessage from "../../src/validation-message";
import PortalService from "../../services/PortalService";

import CountryCode from "../../public/assets/json/phone_code";

import PelatihanService from "../../services/PelatihanService";
import MasterDataHelper from "../../src/helper/MasterDataHelper";
import AvatarUser from "../../components/Layout/AvatarUser";
import { CropperHelper } from "../../components/Modal/ImageCropper";
import NotificationHelper from "../../src/helper/NotificationHelper";

import CardPelatihan from "../../components/Card/Pelatihan";
import CardSurvey from "../../components/Card/Survey";
import CardTesSubstansi from "../../components/Card/TesSubstansi";
import CardTrivia from "../../components/Card/Trivia";

import InputPassword from "../../components/Form/InputPassword";

import NotificationCounter from "../../components/NotificationCounter";
import eventBus from "../../components/EventBus";
import NotificationDate from "../../components/NotificationDate";
import HtmlParser from "react-html-parser";
import Moment from "react-moment";
import { removeCookies } from "cookies-next";
import moment from "moment";
import AlertWarning from "../../components/AlertWarning";
import VerifikasiHandphone from "../../components/Modal/VerifikasiHandphonePelatihanModal";
import ReCAPTCHA from "react-google-recaptcha";
import SurveyMandatoryModalDownloadSertifikat from "../../components/Modal/SurveyMandatoryModalDs";
import TanggalUpdateDomisili from "../../components/TanggalUpdateDomisili";
import ButtonLMS from "../../components/ButtonLMS";
import ButtonPresensi from "../../components/ButtonPresensi";
import routeSegment from "../../src/route-segment";
import http from "../../src/http-dukcapil";
import axios from "axios";

class CaptchaError extends Error {
  constructor(message) {
    super(message); // (1)
    this.name = "CaptchaError"; // (2)
  }
}

const ButtonDownloadSertifikat = ({
  pelatihanSertifikat,
  pelatihanId,
  userNama,
  pelatihanTema,
  pelatihanNama,
  pelatihanAkademi,
  pelatihanTahun,
  pelatihanBulan,
  userNoreg,
  timeDiff,
  pelatihanSelesai,
}) => {
  const [loading, setLoading] = useState(false);
  const [avalailable, setAvailabel] = useState(true);

  const download = () => {
    console.log(
      "Pelatihan Sertifikat: ",
      pelatihanSertifikat,
      PortalHelper.urlSertifikat(pelatihanSertifikat),
    );
    PortalHelper.axiosDownloadFile(
      PortalHelper.urlSertifikat(pelatihanSertifikat),
      `Sertifikat_${userNama}_${pelatihanNama}.pdf`,
      {
        exportData: "application/pdf",
        responseType: "blob",
      },
    );
  };

  useEffect(() => {
    // cek sertifikat
    try {
      setLoading(true);
      const authToken = PortalHelper.tokenGetFromCookie();
      // cek header
      axios.get(PortalHelper.urlSertifikat(pelatihanSertifikat), {
        headers: {
          Authorization: authToken,
        },
      });
    } catch (error) {
      setAvailabel(false);
    } finally {
      setLoading(false);
    }
  }, []);

  return (
    <div className="col-auto align-self-end">
      <a
        disabled={loading || !avalailable}
        href="javascript:void(0)"
        onClick={async () => {
          try {
            setLoading(true);
            const resp =
              await ProfilService.checkStatusSurveiEvaluasiPelatihan(
                pelatihanId,
              );
            // console.log(resp)
            if (resp.data.success) {
              const curDate = moment(timeDiff.serverTime);
              if (
                resp.data.result.data?.length > 0 ||
                moment(pelatihanSelesai).year() < curDate.year()
              ) {
                setLoading(false);

                // cek list survey yang belum dikerjakan
                const surveyBelumDikerjakan = _.filter(
                  resp.data.result.data,
                  function (survey) {
                    return (
                      curDate.isSameOrBefore(moment(survey.survey_selesai)) &&
                      (survey.status_mengerjakan_id == null ||
                        survey.status_mengerjakan_id == 0)
                    );
                  },
                );

                if (surveyBelumDikerjakan.length > 0) {
                  eventBus.dispatch("showMandatoryDS", {
                    show: true,
                    surveys: surveyBelumDikerjakan.map((survey) => ({
                      nama: survey.survey_name,
                      id: survey.survey_id,
                      type: "survey",
                      pelatian_id: survey.training_id,
                      question_bank_id: survey.survey_id,
                    })),
                  });
                } else {
                  download();
                }
              } else {
                NotificationHelper.Failed({
                  message:
                    "Tidak dapat mengunduh sertifikat karena belum ada survey evaluasi",
                  okText: "Kembali",
                  onClose: () => {
                    setLoading(false);
                  },
                });
              }
            }

            // return
          } catch (error) {
            setLoading(false);
            NotificationHelper.Failed({ message: error.toString() });
          }
        }}
        className={`btn btn-green rounded-pill btn-xs px-3 mb-3 font-size-14 me-2 ${
          loading ? "disabled" : ""
        }`}
      >
        {loading ? (
          <>
            <div className="spinner-border spinner-border-sm" role="status">
              <span className="visually-hidden">Loading...</span>
            </div>
          </>
        ) : (
          <i className="bi bi-cloud-download"></i>
        )}
        <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
          Download Sertifikat
        </span>
      </a>
      <a
        className="btn btn-tropaz rounded-pill btn-xs px-3 mb-3 font-size-14 me-2"
        href={`https://www.linkedin.com/profile/add?startTask=CERTIFICATION_NAME&name=${pelatihanTema}%20(${pelatihanAkademi})&organizationId=14595172&issueYear=${pelatihanTahun}&issueMonth=${pelatihanBulan}&certUrl=https://digitalent.kominfo.go.id/cek-sertifikat&certId=${userNoreg}`}
        target="_blank"
      >
        <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
          <i className="bi bi-linkedin me-1"></i>Add to LinkedIn
        </span>
      </a>
    </div>
  );
};

const BelumAdaData = ({ message = "Belum Ada Data" }) => {
  return (
    <>
      <div className="col-12 mx-auto text-center">
        <h2 className="fw-bolder mb-0">{message}</h2>
        {/* <p className="font-size-lg mb-6">Pelatihan pada akademi <span className="text-blue font-weight-semi-bold">{AkademiDetail.nama}</span> belum tersedia</p> */}
        <img
          src="/assets/img/empty-state/no-data.png"
          alt="..."
          className="img-fluid align-self-center"
          style={{ width: "500px" }}
        />
      </div>
    </>
  );
};

function List({ lowongan }) {
  return (
    <>
      {lowongan.map((lwg, i) => {
        const current = moment();
        const isBefore = !current.isSameOrAfter(
          moment(lwg.registration_starts_at, "DD-MM-YYYY"),
        );
        const isAfter = !current.isSameOrBefore(
          moment(lwg.registration_ends_at, "DD-MM-YYYY"),
        );
        const isOpen = !isBefore && !isAfter;
        return (
          <div key={i} className="card border-bottom p-3 mb-1">
            <div className="row gx-0">
              {/* Image */}
              <a
                href={`https://diploy.id/jobs/${lwg.id}`}
                className="col-auto p-3 d-block"
                target={"_blank"}
                style={{ maxWidth: 90 }}
              >
                <img
                  className="img-fluid shadow-light-lg"
                  src={`https://diploy.id/get-file?path=${lwg.company.logo}`}
                  alt="Mitra"
                />
              </a>
              {/* Body */}
              <div className="col d-md-block">
                <div className="card-body p-2">
                  <div className="mb-0">
                    <span className="text-dark font-size-sm fw-normal">
                      {lwg.company.name}
                    </span>
                  </div>
                  <a
                    href={`https://diploy.id/jobs/${lwg.id}`}
                    target={"_blank"}
                    className="d-block mb-2"
                  >
                    <h5 className="line-clamp-2 fw-bolder hover-clamp-off mb-3">
                      {lwg.title}
                    </h5>
                  </a>

                  <ul className="nav mx-n3 d-block d-md-flex">
                    <li className="nav-item px-3 mb-3 mb-md-0">
                      <div className="d-flex align-items-center">
                        <div className="me-2 d-flex text-secondary icon-uxs">
                          {/* Icon */}
                          <i className="bi bi-geo-alt" />
                        </div>
                        <div className="font-size-sm">{lwg.city?.name}</div>
                      </div>
                    </li>
                    <li className="nav-item px-3 mb-3 mb-md-0">
                      <div className="d-flex align-items-center">
                        <div className="me-2 d-flex text-secondary icon-uxs">
                          {/* Icon */}
                          <i className="bi bi-briefcase" />
                        </div>
                        <div className="font-size-sm">{lwg.job_type.name}</div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        );
      })}
    </>
  );
}

const LoadingOrData = ({ isLoading, valueData }) => {
  return (
    <>
      {isLoading ? <Skeleton className="h-10p"></Skeleton> : <>{valueData}</>}
    </>
  );
};

const TabDashboard = ({
  userProfile,
  timeDiff,
  displayPadiUMKM,
  successLoadDiploy,
  urlDiploy,
  loadingBeasiswa,
  urlBeasiswa,
}) => {
  const [dashboarData, setDashboarData] = useState({
    jml_pelatihan: 0,
    jml_lulus: 0,
    jml_tidak_lulus: 0,
  });
  const [isLoading, setIsLoading] = useState(true);
  const router = useRouter();

  const [loadingLowongan, setLoadingLowongan] = useState(true);
  const [lowongan, setLowongan] = useState([]);

  // aktivitas
  const [loadingAktivitas, setLoadingAktivitas] = useState(true);
  const [listPelatihan, setListPelatihan] = useState([]);
  const [listSurvey, setListSurvey] = useState([]);
  const [listTestSubstansi, setListTestSubstansi] = useState([]);
  const [listTrivia, setListTrivia] = useState([]);

  useEffect(async () => {
    // on succes pembatalan
    eventBus.on("success-batal-pendaftaran", (resp) => {
      // console.log(d)
      // callBackBatalDaftar(d)
      setListPelatihan((lp) => {
        const { result, message, success } = resp.data;
        if (success) {
          let tmp = [...lp];
          const { user_id, pelatihan_id, status_peserta } = result.data[0];
          let idx = _.findIndex(lp, ["pelatian_id", pelatihan_id]);
          if (idx >= 0) {
            tmp[idx] = { ...lp[idx], status_peserta: status_peserta };
          }
          NotificationHelper.Success({ message: message });

          return tmp;
        } else {
          NotificationHelper.Failed({ message: message });
          return lp;
        }
      });
    });
    await getDataDashboard();
    await getDataInformasi();
    // await getAkitvitas()
  }, []);

  const getDataDashboard = async () => {
    let initData = {
      jml_pelatihan: 0,
      jml_lulus: 0,
      jml_tidak_lulus: 0,
    };
    try {
      const resp = await ProfilService.profilDashboard();
      if (resp.data.success) {
        const statData = resp.data.result.data.statistik[0];
        Object.keys(initData).forEach((idx) => {
          initData[idx] = statData[idx];
        });
        // console.log(initData)
        setDashboarData(initData);
        setIsLoading(false);
        const curdate = moment();
        // set aktivitas
        let testSubstansi = _.filter(
          resp.data.result.data.test_substansi,
          function (o) {
            return true;
            // return o.ptgl_pengerjaan == null
          },
        );
        let survey = _.filter(resp.data.result.data.survey, function (o) {
          return true;
          // return o.ptgl_pengerjaan == null
        });
        let trivia = _.filter(resp.data.result.data.trivia, function (o) {
          return true;
          // return o.ptgl_pengerjaan == null
        });
        // let survey = _.filter(resp.data.result.data.survey, function (o) { return o.ptgl_pengerjaan == null })

        setListPelatihan(resp.data.result.data.pelatihan);
        setListSurvey(survey);
        setListTestSubstansi(testSubstansi);
        setListTrivia(trivia);
        setLoadingAktivitas(false);
      } else {
        throw resp;
      }
    } catch (error) {
      setIsLoading(false);
      setDashboarData(initData);
    }
  };

  const getDataInformasi = async () => {
    try {
      const resp2 = await PortalService.getLowongan();
      // console.log(resp2);
      if (resp2.status == 200) {
        // console.log(resp2.data?.data);
        setLowongan(resp2.data.result?.data || []);
      }
      // console.log(resp)
      setLoadingLowongan(false);
    } catch (error) {
      console.log(error);
      setLoadingLowongan(false);
    }
  };

  const getAkitvitas = async () => {
    try {
      const resp = await ProfilService.profilAktivitas();
      // console.log("aktivitas", resp)
      if (resp.data?.success) {
        const { pelatihan, survey, test_substansi, trivia } =
          resp.data.result.data;
        // console.log(pelatihan)
        setListPelatihan(pelatihan);
        setListSurvey(survey);
        setListTestSubstansi(test_substansi);
        setListTrivia(trivia);
      }
      setLoadingAktivitas(false);
    } catch (error) {
      setLoadingAktivitas(false);
    }
  };

  const callBackBatalDaftar = (resp) => {
    // console.log(resp)
    const { result, message, success } = resp.data;
    if (success) {
      const { user_id, pelatihan_id, status_peserta } = result.data[0];
      let idx = _.findIndex(listPelatihan, ["pelatian_id", pelatihan_id]);
      if (idx >= 0) {
        let tmp = [...listPelatihan];
        tmp[idx] = { ...listPelatihan[idx], status_peserta: status_peserta };
        setListPelatihan(tmp);
      }
      NotificationHelper.Success({ message: message });
      // "result": {
      //     "data": [
      //         {
      //             "user_id": 156327,
      //             "pelatihan_id": 5071,
      //             "status_peserta": "Pembatalan"
      //         }
      //     ]
      // },
    } else {
      NotificationHelper.Failed({ message: message });
    }
  };

  const texts = [
    "250+ Alumni DEA sudah gabung & ikut berjualan di PaDi UMKM. Sekarang giliran kamu!",
    "Selesai ikut pelatihan, saatnya banyakin transaksi. Ayo segera daftar!",
  ];
  const [currentTextIndex, setCurrentTextIndex] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentTextIndex((prevIndex) => (prevIndex + 1) % texts.length);
    }, 4000);

    return () => clearInterval(interval);
  }, []); // Dependensi kosong agar hanya berjalan sekali saat komponen dimuat

  const currentText = texts[currentTextIndex];

  return (
    <>
      <div className="row gx-2">
        {!isLoading ? (
          <>
            <div className="col-6 col-sm-4  mb-md-6 mb-4">
              <div className="card border rounded-5 icon-category icon-category-sm p-5">
                <div className="row align-items-center mx-n3">
                  <div className="col-auto px-3">
                    <div className="icon-h-p secondary">
                      <i className="bi bi-mortarboard-fill text-blue" />
                    </div>
                  </div>
                  <div className="col px-3">
                    <div className="card-body p-0">
                      <h6 className="mb-0 text-muted line-clamp-1">
                        Pelatihan
                      </h6>
                      <h6 className="mb-0 line-clamp-1 hover-clamp-off">
                        {dashboarData.jml_pelatihan} Pelatihan
                      </h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-6 col-sm-4 mb-md-6 mb-4">
              <div className="card border rounded-5 icon-category icon-category-sm p-5">
                <div className="row align-items-center mx-n3">
                  <div className="col-auto px-3">
                    <div className="icon-h-p secondary">
                      <i className="fbi bi-patch-check-fill text-green" />
                    </div>
                  </div>
                  <div className="col px-3">
                    {/* Body */}
                    <div className="card-body p-0">
                      <h6 className="mb-0 text-muted line-clamp-1">Lulus</h6>
                      <h6 className="mb-0 line-clamp-1 hover-clamp-off">
                        {dashboarData.jml_lulus} Pelatihan
                      </h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-12 col-sm-4 mb-md-6 mb-4">
              <div className="card border rounded-5 icon-category icon-category-sm p-5">
                <div className="row align-items-center mx-n3">
                  <div className="col-auto px-3">
                    <div className="icon-h-p">
                      <i className="bi bi-clipboard-x-fill text-red" />
                    </div>
                  </div>
                  <div className="col px-3">
                    <div className="card-body p-0">
                      <h6 className="mb-0 text-muted line-clamp-1">
                        Tidak Lulus
                      </h6>
                      <h6 className="mb-0 line-clamp-1 hover-clamp-off">
                        {dashboarData.jml_tidak_lulus} Pelatihan
                      </h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        ) : (
          <>
            <div className="col-6 col-sm-4  mb-md-6 mb-4">
              <Skeleton className="h-120p"></Skeleton>
            </div>
            <div className="col-6 col-sm-4 mb-md-6 mb-4">
              <Skeleton className="h-120p"></Skeleton>
            </div>
            <div className="col-12 col-sm-4 mb-md-6 mb-4">
              <Skeleton className="h-120p"></Skeleton>
            </div>
          </>
        )}
      </div>
      <h4 className="fw-bold text-muted mt-5">Aktivitas</h4>
      {loadingAktivitas ? (
        <>
          <Skeleton className="h-150p mb-4" count={3}></Skeleton>
        </>
      ) : listPelatihan.length > 0 ||
        listSurvey.length > 0 ||
        listTestSubstansi.length > 0 ||
        listTrivia.length > 0 ? (
        <>
          {listPelatihan.map((pelatihan, i) => {
            const isCancelAvailabel = () => {
              if (
                pelatihan.status_peserta != "Submit" &&
                pelatihan.status_peserta?.toLowerCase() != "belum melengkapi"
              ) {
                return false;
              }
              return (
                moment().isSameOrAfter(moment(pelatihan.pendaftaran_mulai)) &&
                moment().isBefore(moment(pelatihan.pendaftaran_selesai))
              );
            };

            return (
              <div>
                {displayPadiUMKM && (
                  <>
                    <div class="alert bg-info border rounded-5 px-5 mb-5">
                      <div class="card bg-transparent pt-3 mb-3">
                        <div class="row gx-0">
                          <div
                            className="col-auto p-3 d-block"
                            style={{ maxWidth: "100px" }}
                          >
                            <div class="d-flex justify-content-center align-items-center h-100">
                              <img
                                class="img-fluid shadow-light-lg"
                                src="/padi.svg"
                              />
                            </div>
                          </div>
                          <div class="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                            <div class="card-body bg-transparent p-3">
                              <div className="mb-0">
                                <span className="text-blue font-size-sm">
                                  Pemberdayaan dan Pasca Pelatihan
                                </span>
                              </div>
                              <div class="d-block mb-2">
                                <h5 class="fw-bolder mb-0">
                                  Bergabung Menjadi Penjual di PadiUMKM
                                </h5>
                                <p class="font-size-14 text-dark mb-0">
                                  {currentText}
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <hr className="my-2" />
                      </div>
                      <div class="row gx-2 mb-3">
                        <div class="col-md mb-md-0">
                          <button
                            class="btn rounded-pill btn-sm btn-blue me-3"
                            type="button"
                            style={{ width: "150px" }}
                            data-bs-toggle="modal"
                            data-bs-target="#modalPadiUMKM"
                          >
                            Mulai Berjualan
                          </button>
                          <Link
                            href="https://info.padiumkm.id/kominfo?utm_source=digitalent&utm_medium=cta&utm_campaign=kominfo"
                            target="_blank"
                          >
                            <a
                              class="btn btn-sm rounded-pill btn-outline-primary"
                              target="_blank"
                              rel="noopener noreferrer"
                              style={{ width: "150px" }}
                            >
                              Pelajari
                            </a>
                          </Link>
                        </div>
                      </div>
                    </div>
                  </>
                )}
                {successLoadDiploy && (
                  <>
                    <div class="alert bg-info border rounded-5 px-5 mb-5">
                      <div class="card bg-transparent pt-3 mb-3">
                        <div class="row gx-0">
                          <div
                            className="col-auto p-3 d-block"
                            style={{ maxWidth: "100px" }}
                          >
                            <div class="d-flex justify-content-center align-items-center h-100">
                              <img
                                class="img-fluid shadow-light-lg"
                                src="/diploy.png"
                              />
                            </div>
                          </div>
                          <div class="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                            <div class="card-body bg-transparent p-3">
                              <div className="mb-0">
                                <span className="text-blue font-size-sm">
                                  Pemberdayaan dan Pasca Pelatihan
                                </span>
                              </div>
                              <div class="d-block mb-2">
                                <h5 class="fw-bolder mb-0">
                                  Yuk, mulai karir dan masa depan bersama Diploy
                                </h5>
                                <p class="font-size-14 text-dark mb-0">
                                  Temukan berbagai lowongan pekerjaan dari
                                  Perusahaan terkemuka
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <hr className="my-2" />
                      </div>
                      <div class="row gx-2 mb-3">
                        <div class="col-md mb-md-0">
                          <Link href={urlDiploy}>
                            <a
                              class="btn rounded-pill btn-sm btn-blue me-3"
                              target="_blank"
                              style={{ width: "150px" }}
                            >
                              Mulai
                            </a>
                          </Link>
                          <Link href={"https://diploy.id/tentang-talenta"}>
                            <a
                              class="btn btn-sm rounded-pill btn-outline-primary"
                              target="_blank"
                              rel="noopener noreferrer"
                              style={{ width: "150px" }}
                            >
                              Pelajari
                            </a>
                          </Link>
                        </div>
                      </div>
                    </div>
                  </>
                )}
                <div
                  className="bg-white border rounded-5 px-5 mb-5"
                  key={"pel-" + i}
                >
                  <CardPelatihan
                    key={"pel-" + i}
                    cardClassName="border-bottom pt-3 mb-3"
                    {...pelatihan}
                    id={pelatihan.pelatian_id}
                    customAction={() => <></>}
                    showStatusPelatihan={false}
                  ></CardPelatihan>
                  <div className="row gx-2 mb-3">
                    <div className="col-md mb-md-0">
                      <div
                        className={`badge badge-sm mt-3 ${PortalHelper.getColorClassStatusPendaftaran(
                          pelatihan.status_peserta,
                        )} mb-3 badge-pill`}
                      >
                        <span className="text-white font-size-sm fw-normal">
                          {pelatihan.status_peserta}
                        </span>
                      </div>
                    </div>
                    <div className="col-auto align-self-end">
                      {pelatihan.status_peserta != "Pembatalan" && (
                        <a
                          href="javascript:void(0)"
                          className="btn btn-outline-primary p-2 btn-rounded-circle btn-xs mb-3 font-size-15 me-2"
                          onClick={() => {
                            if (!pelatihan.file_path) {
                              router.push({
                                pathname: `/pelatihan/${pelatihan.pelatian_id}/daftar`,
                              });
                            } else {
                              PortalHelper.axiosDownloadFile(
                                process.env.STORAGE_URL +
                                  "dts-dev/" +
                                  pelatihan.file_path,
                                `Bukti Pendaftaran_${userProfile.nama}_${pelatihan.nama_pelatihan}.pdf`,
                              );

                              // let a = document.createElement("a");
                              // a.target = "_blank";
                              // a.href =
                              //   process.env.STORAGE_URL + pelatihan.file_path;
                              // a.click();
                              // a.remove();
                            }
                          }}
                        >
                          <i className="bi bi-person-vcard" />
                        </a>
                      )}
                      {isCancelAvailabel() && (
                        <a
                          className="btn bg-red text-white rounded-pill btn-xs px-3 mb-3 font-size-15 me-2"
                          onClick={() => {
                            eventBus.dispatch("setModalBatalPendaftaran", {
                              nama_pelatihan: pelatihan.nama_pelatihan,
                              id: pelatihan.pelatian_id,
                              // callBack: callBackBatalDaftar,
                            });
                          }}
                        >
                          <i className="bi bi-exclamation-octagon" />
                          <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                            Batal Pendaftaran
                          </span>
                        </a>
                      )}
                      <ButtonLMS
                        status_peserta={pelatihan.status_peserta}
                        id_pelatihan={pelatihan.pelatian_id}
                      ></ButtonLMS>
                      {/* <ButtonPresensi
                      status_peserta={pelatihan.status_peserta}
                      id_pelatihan={pelatihan.pelatian_id}
                    ></ButtonPresensi> */}
                    </div>
                    {
                      // status peserta lulus
                      ["7", "8", "19", "20", "21"].includes(
                        pelatihan.status,
                      ) ? (
                        <>
                          {pelatihan.file_sertifikat != null ? (
                            <ButtonDownloadSertifikat
                              pelatihanSertifikat={pelatihan.file_sertifikat}
                              pelatihanId={pelatihan.pelatian_id}
                              userNama={userProfile.nama}
                              userNoreg={pelatihan.nomor_registrasi}
                              pelatihanNama={pelatihan.nama_pelatihan}
                              timeDiff={timeDiff}
                              pelatihanTema={pelatihan.nama_tema}
                              pelatihanAkademi={pelatihan.nama_akademi}
                              pelatihanTahun={new Date(
                                pelatihan.pelatihan_selesai,
                              ).getFullYear()}
                              pelatihanBulan={
                                new Date(
                                  pelatihan.pelatihan_selesai,
                                ).getMonth() + 1
                              }
                              pelatihanSelesai={pelatihan.pelatihan_selesai}
                            ></ButtonDownloadSertifikat>
                          ) : (
                            <>
                              <div className="col-auto align-self-end">
                                <a className="btn bg-yellow rounded-pill btn-xs px-3 mb-3 font-size-15 text-dark">
                                  <i className="bi bi-exclamation-triangle"></i>
                                  <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                                    Sertifikat Belum Tersedia
                                  </span>
                                </a>
                              </div>
                            </>
                          )}
                        </>
                      ) : (
                        <></>
                      )
                    }
                  </div>
                </div>
              </div>
            );
          })}

          {listTestSubstansi.map((testSubstansi, i) => {
            return (
              <CardTesSubstansi
                key={"sub-" + i}
                subtance_nama={testSubstansi.sqb_title}
                subtansi_mulai={testSubstansi.subtansi_mulai}
                subtansi_selesai={testSubstansi.subtansi_selesai}
                tgl_pengerjaan={testSubstansi.ptgl_pengerjaan}
                status={testSubstansi.pstatus}
                akademi_nama={testSubstansi.pakademi}
                pelatihan_nama={testSubstansi.ppelatihan}
                subtance_id={testSubstansi.subtansi_id}
                duration={testSubstansi.duration}
                questions_to_share={testSubstansi.questions_to_share}
                pelatihan_id={testSubstansi.ptraining_id}
                nilai={testSubstansi.pnilai}
                finish_datetime={testSubstansi.finish_datetime}
                midtest_mulai={testSubstansi.midtest_mulai}
                midtest_selesai={testSubstansi.midtest_selesai}
                alur_pendaftaran={testSubstansi.alur_pendaftaran}
              ></CardTesSubstansi>
            );
          })}

          {listSurvey.map((survey, i) => {
            return (
              <CardSurvey
                key={"sur-" + i}
                survey_nama={survey.sqb_title}
                survey_mulai={survey.survey_mulai}
                survey_selesai={survey.survey_selesai}
                nama_level={survey.nama_level}
                jenis_survey={survey.jenis_survey}
                tgl_pengerjaan={survey.ptgl_pengerjaan}
                status={survey.pstatus}
                akademi_nama={survey.pakademi}
                akademi_id={survey.pakademi_id}
                pelatihan_nama={survey.ppelatihan}
                pelatihan_id={survey.ptraining_id}
                survey_id={survey.sqb_id}
              ></CardSurvey>
            );
          })}

          {listTrivia.map((trivia, i) => {
            return (
              <CardTrivia
                key={"triv-" + i}
                {...trivia}
                trivia_nama={trivia.sqb_title}
                trivia_mulai={trivia.trivia_mulai}
                trivia_selesai={trivia.trivia_selesai}
                nama_level={trivia.nama_level}
                pelatihan_nama={trivia.ppelatihan}
                status={trivia.pstatus}
                akademi_nama={trivia.pakademi}
                akademi_id={trivia.pakademi_id}
                finish_datetime={trivia.finish_datetime}
                trivia_id={trivia.sqb_id}
                pelatihan_id={trivia.ptraining_id}
                tgl_pengerjaan={trivia.ptgl_pengerjaan}
                // trivia_mulai
              ></CardTrivia>
            );
          })}
        </>
      ) : (
        <>
          <div className="bg-white border rounded-5 px-5 pt-6 pb-6 mb-5">
            <div className="row mb-3">
              <div className="col-12 mb-md-0 flex-column justify-content-center d-flex">
                <p className="text-center">
                  Belum ada aktivitas yang sedang berjalan
                </p>
                <img
                  src="/assets/img/empty-state/no-data.png"
                  className="w-200p mx-auto"
                ></img>
              </div>
            </div>
          </div>
        </>
      )}
      <h4 className="fw-bold text-muted mt-5">Informasi</h4>
      <div className="bg-white border rounded-5 px-5 pt-6 pb-6 mb-5">
        <div className="row mb-3">
          <div className="col-md mb-md-0">
            <img
              className="img-fluid d-block shadow-light-lg"
              src={`https://diploy.id/images/diploy.png`}
              alt="diploy"
              width="120"
            />
            <span>
              Peluang kerja dan magang terbaru untuk alumni pelatihan Digital
              Talent Scholarship
            </span>
          </div>
          <div className="col-md-auto">
            <a
              href="https://diploy.id/jobs"
              target={"_blank"}
              className="d-flex font-size-14 align-items-center fw-medium"
            >
              Lihat Semua
              <div className="ms-2 d-flex">
                {/* Icon */}
                <svg
                  width={10}
                  height={10}
                  viewBox="0 0 10 10"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M7.7779 4.6098L3.32777 0.159755C3.22485 0.0567475 3.08745 0 2.94095 0C2.79445 0 2.65705 0.0567475 2.55412 0.159755L2.2264 0.487394C2.01315 0.700889 2.01315 1.04788 2.2264 1.26105L5.96328 4.99793L2.22225 8.73895C2.11933 8.84196 2.0625 8.97928 2.0625 9.1257C2.0625 9.27228 2.11933 9.4096 2.22225 9.51269L2.54998 9.84025C2.65298 9.94325 2.7903 10 2.9368 10C3.0833 10 3.2207 9.94325 3.32363 9.84025L7.7779 5.38614C7.88107 5.2828 7.93774 5.14484 7.93741 4.99817C7.93774 4.85094 7.88107 4.71305 7.7779 4.6098Z"
                    fill="currentColor"
                  />
                </svg>
              </div>
            </a>
          </div>
        </div>

        {loadingLowongan && <div>Loading Lowongan...</div>}
        {lowongan.length != 0 ? (
          <List lowongan={lowongan} />
        ) : (
          <>Tidak ada data</>
        )}
      </div>
      {loadingBeasiswa && (
        <>
          <div class="alert border rounded-5 px-5 mb-5">
            <div class="card bg-transparent pt-3 mb-3">
              <div class="row gx-0">
                <div
                  className="col-auto p-3 d-block"
                  style={{ maxWidth: "100px" }}
                >
                  <div class="d-flex justify-content-center align-items-center h-100">
                    <img
                      class="img-fluid shadow-light-lg"
                      src="/beasiswa.png"
                    />
                  </div>
                </div>
                <div class="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                  <div class="card-body bg-transparent p-3">
                    <div className="mb-0">
                      <span className="text-blue font-size-sm">
                        Beasiswa Kominfo
                      </span>
                    </div>
                    <div class="d-block mb-2">
                      <h5 class="fw-bolder mb-0">
                        Beasiswa S2 Dalam dan Luar Negeri Kominfo
                      </h5>
                      <p class="font-size-14 text-dark mb-0">
                        Raih kesempatan melanjutkan studi di Perguruang Tinggi
                        terkemuka
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <hr className="my-2" />
            </div>
            <div class="row gx-2 mb-3">
              <div class="col-md mb-md-0">
                <Link href={urlBeasiswa}>
                  <a
                    class="btn rounded-pill btn-sm btn-blue me-3"
                    target="_blank"
                    style={{ width: "150px" }}
                  >
                    Mulai
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
};

const TabProfile = ({ setMainLoading, userProfile, refreshProfile }) => {
  const router = useRouter();
  const {
    query: { rand },
  } = router;

  const [isLoading, setIsLoading] = useState(false);
  const [isEdit, setIsEdit] = useState(false);

  const [FListProvinsi, setFListProvinsi] = useState([]);

  const [FListStatusPekerjaan, setFListStatusPekerjaan] = useState([]);
  const [FListBidangPekerjaan, setFListBidangPekerjaan] = useState([]);
  const [FListPendidikan, setFListPendidikan] = useState([]);

  const [FListJenisKelamin, setFListJenisKelamin] = useState(
    Object.keys(MasterDataHelper.jenisKelamin),
  );
  const [FListHubunganKontakDarurat, setFListHubunganKontakDarurat] = useState([
    ...MasterDataHelper.hubunganKontakDarurat,
  ]);

  const [codes, setCodes] = useState([]);

  useEffect(() => {
    if (typeof userProfile.id != "number") {
      setIsLoading(false);
    }
    // atur edit domisili
  }, [userProfile.id]);

  useEffect(async () => {
    // karena datanya static biar gak fetch ulang
    if (FListPendidikan.length == 0) {
      const resFListPendidikan = await GeneralService.pendidikan();
      setFListPendidikan(resFListPendidikan?.data?.result);
    }
    if (FListProvinsi.length == 0) {
      const resFListProvinsi = await GeneralService.provinsi();
      setFListProvinsi(_.sortBy(resFListProvinsi?.data?.result, "name"));
    }
    if (FListStatusPekerjaan.length == 0) {
      const resFListStatusPekerjaan = await GeneralService.statusPekerjaan();
      setFListStatusPekerjaan(resFListStatusPekerjaan.data.result);
    }
    if (FListBidangPekerjaan.length == 0) {
      const resFListBidangPekerjaan = await GeneralService.bidangPekerjaan();
      setFListBidangPekerjaan(resFListBidangPekerjaan.data.result);
    }
    if (codes.length == 0) {
      const resp = await PortalService.kodeNegaraList();
      const codes = resp.data.result.Data.map((element, index) => {
        CountryCode.forEach((country, idx) => {
          if (country.phone == element.phone_code) {
            element["emoji"] = country.emoji;
          }
        });
        return element;
      });
      setCodes(codes);
    }
    // setIsEdit(true)
    setTimeout(() => {
      const params = router.query;
      if (params?.menu == "profile" && params?.action == "edit") {
        setIsEdit(true);
      }
    }, 1000);
  }, []);

  useEffect(() => {
    const params = router.query;
    if (params?.menu == "profile" && params?.action == "edit") {
      setIsEdit(true);
    }
  }, [rand]);

  useEffect(() => {
    if (isEdit) {
      PortalHelper.scrollSpy("#pills-tab");
    }
  }, [isEdit]);

  const EditData = ({ setMainLoading, userProfile }) => {
    const [FListKota, setFListKota] = useState([]);
    const [FListKecamatan, setFListKecamatan] = useState([]);
    const [FListKelurahan, setFListKelurahan] = useState([]);

    const [FReadOnlyKota, setFReadOnlyKota] = useState(true);
    const [FReadOnlyKecamatan, setFReadOnlyKecamatan] = useState(true);
    const [FReadOnlyKelurahan, setFReadOnlyKelurahan] = useState(true);

    const [FormProfil, setFormProfil] = useState({});
    const [FormProfilError, setFormProfilError] = useState([]);

    const [useServerValidation, setuseServerValidation] = useState(false);

    const [codesE, setCodesE] = useState(codes);
    const [phoneCodeE, setPhoneCodeE] = useState({
      country_name: "Indonesia",
      country_slug: "ID",
      emoji: "🇮🇩",
      phone_code: "62",
    });

    const [Cropper, setCropper] = useState(null);
    const [lenKontakDarurat, setLenKontakDarurat] = useState(0);

    const [enableEditDomisili, setEnableEditDomisili] = useState(false);

    const onCropComplete = (file) => {
      try {
        uploadFotoProfil(file);
      } catch (error) {
        console.log(error);
      }
    };

    useEffect(async () => {
      // untuk modal Cropper
      setCropper(CropperHelper(onCropComplete));

      let uf = { ...userProfile };
      if (typeof uf?.nomor_handphone_darurat == "string") {
        let idCountry = codesE.filter(({ phone_code }) => {
          return uf.nomor_handphone_darurat?.startsWith(phone_code);
        });
        if (idCountry.length > 0) {
          setPhoneCodeE(idCountry[0]);
          uf.nomor_handphone_darurat = uf.nomor_handphone_darurat.substring(
            idCountry[0].phone_code.length,
          );
        }
      }

      if (!FListHubunganKontakDarurat.includes(uf.hubungan)) {
        uf.hubungan = "";
      }

      setFormProfil(uf);
      // console.log(userProfile)
      if (isEdit) {
        let e = {};
        e["target"] = {};
        e["target"]["value"] = userProfile["provinsi_id"];
        await onChangeProvinsi(e, true);
        e["target"]["value"] = userProfile["kota_id"];
        await onChangeKota(e, true);
        e["target"]["value"] = userProfile["kecamatan_id"];
        await onChangeKecamatan(e, true);
      }
      // return () => {
      //     e = {}
      //     uf = { ...userProfile }
      // }
      let { update_at_domisili, count_update_domisili } = userProfile;
      count_update_domisili = parseInt(count_update_domisili) || 0;
      // console.log({...userProfile})
      if (count_update_domisili < 2) {
        setEnableEditDomisili(true);
      } else {
        if (update_at_domisili.includes(".")) {
          update_at_domisili = update_at_domisili.split(".")[0];
        }
        const d = moment(update_at_domisili);
        if (d.isValid()) {
          const curdate = moment();
          if (curdate.diff(d, "months") > 1) {
            setEnableEditDomisili(true);
          }
        }
      }
    }, [userProfile]);

    useEffect(() => {
      // console.log(FormProfil)
      reset(FormProfil);
    }, [FormProfil]);

    const errMessageField = (idx) => {
      return useServerValidation ? FormProfilError[idx] : errors[idx]?.message;
    };

    const objYup = {
      nama: yup
        .string()
        .required(validationMessage.required.replace("__field", "Nama")),
      nik: yup
        .string()
        .length(
          16,
          validationMessage.fixLength
            .replace("__field", "Nama")
            .replace("__length", 16),
        )
        .required(validationMessage.required.replace("__field", "nik")),
      email: yup.string().email("format masukan harus email").required(),
      nomor_handphone: yup
        .string()
        .required(
          validationMessage.required.replace("__field", "Nomor Handphone"),
        )
        // Jika nomor handphone diawali 62 maka selanjutnya bukan 62 atau 0
        .test("nomor_handphone", "Nomor Handphone tidak valid", (value) => {
          console.log(value);
          if (value) {
            if (value.startsWith("62")) {
              return !value.startsWith("620") || !value.startsWith("6262");
            }
          }
          return true;
        }),
      tempat_lahir: yup
        .string()
        .required(
          validationMessage.required.replace("__field", "Tempat Lahir"),
        ),
      tanggal_lahir: yup
        .string()
        .required(
          validationMessage.required.replace("__field", "Tanggal Lahir"),
        ),
      jenis_kelamin: yup
        .string()
        .required(
          validationMessage.required.replace("__field", "Jenis Kelamin"),
        ),
      jenjang_id: yup
        .string()
        .required(
          validationMessage.required.replace("__field", "Jenjang Pendidikan"),
        ),
      // "jenjang_nama": "--",
      address: yup
        .string()
        .required(validationMessage.required.replace("__field", "Alamat")),
      provinsi_id: yup
        .string()
        .length(
          2,
          validationMessage.required.replace("__field", "Alamat Provinsi"),
        ),
      // "provinsi_nama": "--",
      kota_id: yup
        .string()
        .length(
          4,
          validationMessage.required.replace("__field", "Alamat Kab/Kota"),
        ),
      // "kota_nama": "--",
      kecamatan_id: yup
        .string()
        .length(
          7,
          validationMessage.required.replace("__field", "Alamat Kecamatan"),
        ),
      // "kecamatan_nama": "--",
      kelurahan_id: yup
        .string()
        .length(
          10,
          validationMessage.required.replace(
            "__field",
            "Alamat Kelurahan/Desa",
          ),
        ),
      // "kelurahan_nama": "--",
      status_pekerjaan_id: yup
        .string()
        .required(
          validationMessage.required.replace("__field", "Status Pekerjaan"),
        ),
      // "status_pekerjaan_nama": "--",
      pekerjaan_id: yup.string().when("status_pekerjaan_id", {
        is: (value) => value == "16",
        then: yup
          .string()
          .required(validationMessage.required.replace("__field", "Pekerjaan")),
      }),
      // "pekerjaan_nama": "--",
      perusahaan: yup.string().when("status_pekerjaan_id", {
        is: (value) => value == "16",
        then: yup
          .string()
          .required(
            validationMessage.required.replace("__field", "Nama Intitusi"),
          ),
      }),
      // nama_kontak_darurat: yup
      //   .string()
      //   .required(
      //     validationMessage.required.replace("__field", "Nama kontak Darurat")
      //   ),
      // hubungan: yup
      //   .string()
      //   .required(validationMessage.required.replace("__field", "Hubungan")),
      // // "nomor_handphone_darurat": yup.string().required(validationMessage.required.replace('__field', 'Nomor Handphone Darurat')),

      // nomor_handphone_darurat: yup
      //   .number()
      //   .typeError(
      //     validationMessage.required.replace("__field", "Nomor Handphone")
      //   )
      //   .test(
      //     "minLength",
      //     validationMessage.minLength
      //       .replace("__field", "Nomor Handphone")
      //       .replace("__length", 7),
      //     (val) => String(val).length >= 8
      //   )
      //   .test(
      //     "maxLength",
      //     validationMessage.maxLength
      //       .replace("__field", "Nomor Handphone")
      //       .replace("__length", 16 - phoneCodeE.phone_code.length + 1),
      //     (val) => String(val).length <= 16 - phoneCodeE.phone_code.length
      //   )
      //   .required(
      //     validationMessage.required.replace("__field", "Nomor Handphone")
      //   ),
      // "wizard": "--"
    };

    const schemaValidation = yup.object().shape(objYup);

    const {
      register,
      handleSubmit,
      reset,
      formState: { errors },
      getValues,
      setError,
      setFocus,
      watch,
      setValue,
    } = useForm({
      mode: "onBlur",
      defaultValues: FormProfil,
      resolver: yupResolver(schemaValidation),
    });

    // const watchNHD =
    useEffect(() => {
      const subscription = watch((value, { name, type }) => {
        setLenKontakDarurat(
          phoneCodeE?.phone_code?.length +
            value?.nomor_handphone_darurat?.length,
        );
      });
      return () => subscription.unsubscribe();
    }, [watch("nomor_handphone_darurat")]);

    const onChangeProvinsi = async (e, v = undefined) => {
      // console.log(v)
      if (v == undefined) {
        // alert("change kota i")
        // setFormProfilByIndex("kota_id", "")
        setFormProfilByIndex("kecamatan_id", null);
        setFormProfilByIndex("kelurahan_id", null);
      }
      setFListKota([]);
      setFListKecamatan([]);
      setFListKelurahan([]);
      setFReadOnlyKota(true);
      setFReadOnlyKecamatan(true);
      setFReadOnlyKelurahan(true);

      const resFListKota = await GeneralService.kota(e.target.value);
      if (resFListKota.data.success) {
        setFListKota(_.sortBy(resFListKota.data.result, "name"));
        setFReadOnlyKota(false);
      }
    };

    const onChangeKota = async (e, v = undefined) => {
      if (v == undefined) {
        setFormProfilByIndex("kecamatan_id", null);
        setFormProfilByIndex("kelurahan_id", null);
      }
      setFListKecamatan([]);
      setFListKelurahan([]);
      setFReadOnlyKecamatan(true);
      setFReadOnlyKelurahan(true);

      const resFListKecamatan = await GeneralService.kecamatan(e.target.value);
      if (resFListKecamatan.data.success) {
        setFListKecamatan(_.sortBy(resFListKecamatan.data.result, "name"));
        setFReadOnlyKecamatan(false);
      }
    };

    const onChangeKecamatan = async (e, v = undefined) => {
      if (v == undefined) {
        setFormProfilByIndex("kelurahan_id", null);
      }
      setFListKelurahan([]);
      setFReadOnlyKelurahan(true);

      const resFListKelurahan = await GeneralService.kelurahan(e.target.value);
      if (resFListKelurahan.data.success) {
        setFListKelurahan(_.sortBy(resFListKelurahan.data.result, "name"));
        setFReadOnlyKelurahan(false);
      }
      // console.log(FormProfil)
    };

    const onSubmitForm = async (data) => {
      const userData = PortalHelper.getUserData();
      let newFormData = new FormData();
      const nomor_handphone_darurat = getValues("nomor_handphone_darurat");
      Object.keys(objYup).forEach((k) => {
        if (k == "nomor_handphone_darurat") {
          newFormData.append(
            "nomor_handphone_darurat",
            `${phoneCodeE.phone_code}${nomor_handphone_darurat}`,
          );
        } else if (k != "foto") {
          // console.log("append",k)
          newFormData.append(k, data[k]);
          // console.log(k, data[k], newFormData)
        }
      });
      newFormData.append("flag_nik", userData.flag_nik);

      setMainLoading(true);
      const resUpdateProfil = await ProfilService.update(newFormData);
      setuseServerValidation(true);
      // return
      if (resUpdateProfil.data.success == true) {
        NotificationHelper.Success({
          message: "Berhasil Memperbarui Data",
          timeout: 3,
        });
        setMainLoading(false);
        refreshProfile();
        PortalHelper.scrollSpy("#pills-tab");
        setIsEdit(false);
      } else if (
        resUpdateProfil.data.success == false &&
        resUpdateProfil.data.message == "Validation Error."
      ) {
        const error = resUpdateProfil.data.result;
        NotificationHelper.Failed({
          message: error || "Terjadi Kesalahan",
          timeout: 3,
        });
        setuseServerValidation(true);
        setFormProfilError(error);
        setMainLoading(false);
      }
    };

    const setFormProfilByIndex = (idx, v) => {
      const f = getValues();
      // console.log(f)
      f[idx] = v;
      switch (idx) {
        case "provinsi_id":
          f["kota_id"] = "";
        case "kota_id":
          f["kecamatan_id"] = "";
        case "kecamatan_id":
          f["kelurahan_id"] = "";
          break;
        case "status_pekerjaan_id":
          // console.log(v)
          if (["17", "18"].includes(`${v}`)) {
            f["pekerjaan_id"] = "";
            f["perusahaan"] = "";
          } else if (v == 16) {
            // console.log(f["pekerjaan_id"])
            if (f["pekerjaan_id"] == "--") f["pekerjaan_id"] = "";
            f["perusahaan"] = "";
          }
          break;

        default:
          break;
      }
      setFormProfil(f);
      // console.log("editd", FormProfil)
    };

    const uploadFotoProfil = async (file) => {
      try {
        setMainLoading(true);
        const formData = new FormData();
        formData.append("foto_profil", file);
        const resUploadFotoProfil =
          await ProfilService.updateFotoProfil(formData);
        if (resUploadFotoProfil.data.success === false) {
          if (resUploadFotoProfil.data?.result) {
            throw Error(
              PortalHelper.handleErrorResponse(resUploadFotoProfil.data.result),
            );
          } else {
            throw Error(resUploadFotoProfil.data.message);
          }
        }
        PortalHelper.updateUserData(
          "foto",
          resUploadFotoProfil.data.result.data.foto,
        );
        setMainLoading(false);
      } catch (error) {
        setMainLoading(false);
        const message = error.response?.data?.result?.Message
          ? error.response.data.result.Message
          : error.message;
        NotificationHelper.Failed({
          message:
            message ||
            "Terjadi kesalahan saat mencoba terhubung dengan server !",
        });
      }
    };

    return (
      <>
        <FormProvider>
          <form
            className="my-5"
            id="form"
            onSubmit={handleSubmit(onSubmitForm)}
          >
            <div className="row">
              <div className="col-12 d-flex justify-content-center mb-6">
                <table>
                  <tbody>
                    <tr>
                      <td>
                        <AvatarUser
                          width={120}
                          isLoggedin={true}
                          isLoading={isLoading}
                        ></AvatarUser>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className="w-100 d-flex justify-content-center">
                          <button
                            type="button"
                            className="btn btn-sm rounded-pill btn-info mt-2"
                            onClick={() => {
                              // atur max file size ke 2000KB
                              Cropper.setKeteranganForm(
                                "Foto Formal (Pas Foto)",
                              );
                              Cropper.setMaxFileSize(2000 * 1024);
                              Cropper.show();
                            }}
                          >
                            Ubah Foto Formal
                          </button>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="col-md-6 mb-5">
                <div className="form-group">
                  <label className="font-size-14 fw-bold">Nama Lengkap</label>
                  <p style={{ textTransform: "capitalize" }}>
                    {FormProfil["nama"]}
                  </p>
                  {/* <input type="text" className={errMessageField('nama') ? 'form-control form-control-sm is-invalid' : 'form-control form-control-sm'} placeholder="Masukan nama lengkap"  {...register("nama")} />
                                {errMessageField('nama') && <span className="text-red">{errMessageField('nama')}</span>} */}
                </div>
              </div>
              <div className="col-md-6 mb-5">
                <div className="form-group">
                  <label className="font-size-14 fw-bold">NIK</label>
                  <p>{PortalHelper.mask(FormProfil["nik"], 7, 14)}</p>
                  {/* <input type="text" className={errMessageField('nik') ? 'form-control form-control-sm is-invalid' : 'form-control form-control-sm'} placeholder="Masukan NIK"  {...register("nik")} />
                                {errMessageField('nik') && <span className="text-red">{errMessageField('nik')}</span>} */}
                </div>
              </div>
              <div className="col-md-6 mb-5">
                <div className="form-group">
                  <label className="font-size-14 fw-bold">Email </label>
                  <p>
                    {PortalHelper.mask(FormProfil["email"], null, null, "@")}
                    {/* <a className='ms-1' data-bs-toggle="tooltip" data-bs-placement="right" title="Ajukan perubahan data"><i className='fa fa-pencil-alt'></i></a> */}
                  </p>
                  {/* <input type="text" className={errMessageField('email') ? 'form-control form-control-sm is-invalid' : 'form-control form-control-sm'} placeholder="Masukan alamat email"  {...register("email")} />
                                {errMessageField('email') && <span className="text-red">{errMessageField('email')}</span>} */}
                </div>
              </div>
              <div className="col-md-6 mb-5">
                <div className="form-group">
                  <label className="font-size-14 fw-bold">
                    Nomor Handphone
                  </label>
                  <p>
                    {PortalHelper.mask(FormProfil["nomor_handphone"], 7, 14)}
                    {/* <a className='ms-1' data-bs-toggle="tooltip" data-bs-placement="right" title="Ajukan perubahan data"><i className='fa fa-pencil-alt'></i></a> */}
                  </p>
                  {/* <input type="text" className={errMessageField('nomor_handphone') ? 'form-control form-control-sm is-invalid' : 'form-control form-control-sm'} placeholder="Masukan nomor handphone"  {...register("nomor_handphone")} />
                                {errMessageField('nomor_handphone') && <span className="text-red">{errMessageField('nomor_handphone')}</span>} */}
                </div>
              </div>
              {/* <div className="col-md-6 mb-5">
                <div className="form-group">
                  <label className="font-size-14 fw-bold">
                    Tempat Lahir&nbsp;<span className="text-red">*</span>
                  </label>
                  <p>{FormProfil["tempat_lahir"]}</p>
                  <input type="text" className={errMessageField('tempat_lahir') ? 'form-control form-control-sm is-invalid' : 'form-control form-control-sm'} placeholder="Tempat Lahir" onChange={(v) => { console.log(v) }} {...register("tempat_lahir")} />
                                {errMessageField('tempat_lahir') && <span className="text-red">{errMessageField('tempat_lahir')}</span>}
                </div>
              </div> */}
              <div className="col-md-6 mb-5">
                <div className="form-group">
                  <label className="font-size-14 fw-bold">
                    Tanggal Lahir&nbsp;<span className="text-red">*</span>
                  </label>
                  <p>{FormProfil["tanggal_lahir"]}</p>
                  {/* <DatePicker selected={PortalHelper.stringToDate(FormProfil['tanggal_lahir'] || "01/01/1998")} dateFormat="dd/MM/yyyy" className={errMessageField('tanggal_lahir') ? 'form-control form-control-sm is-invalid' : 'form-control form-control-sm'}
                                    onChange={(v) => {
                                        let d = moment(v)
                                        let ds = d.format("DD/MM/YYYY")
                                        // console.log(ds)
                                        // const { onChange } = register("tanggal_lahir")
                                        // onChange()
                                        setFormProfilByIndex("tanggal_lahir", ds)
                                        // console.log(v)
                                    }}></DatePicker>
                                  {errMessageField('tanggal_lahir') && <span className="text-red">{errMessageField('tanggal_lahir')}</span>} */}
                </div>
              </div>
              <div className="col-md-6 mb-5">
                <div className="form-group">
                  <label className="font-size-14 fw-bold">
                    Jenis Kelamin&nbsp;<span className="text-red">*</span>
                  </label>
                  <p>
                    {isNaN(FormProfil["jenis_kelamin"])
                      ? FormProfil["jenis_kelamin"]
                      : MasterDataHelper.jenisKelamin[
                          FormProfil["jenis_kelamin"]
                        ]}
                  </p>
                  {/* <select className={errMessageField('jenis_kelamin') ? 'form-select form-select-sm is-invalid' : 'form-select form-select-sm'} {...register('jenis_kelamin')}>
                                    <option disabled>Pilih Jenis Kelamin</option>
                                    {FListJenisKelamin.map((JenisKelamin) => (
                                        <option key={JenisKelamin} value={JenisKelamin}>{JenisKelamin}</option>
                                    ))}
                                </select>
                                {errMessageField('jenis_kelamin') && <span className="text-red">{errMessageField('jenis_kelamin')}</span>} */}
                </div>
              </div>
              {/* <div className="col-md-6 mb-5">
                <div className="form-group">
                  <label className="font-size-14 fw-bold">
                    Pendidikan Terakhir&nbsp;<span className="text-red">*</span>
                  </label>
                  <select
                    className={
                      errMessageField("jenjang_id")
                        ? "form-select form-select-sm is-invalid"
                        : "form-select form-select-sm"
                    }
                    {...register("jenjang_id")}
                  >
                    <option disabled value="">
                      Pilih Pendidikan Terakhir
                    </option>
                    {FListPendidikan.map((Pendidikan) => (
                      <option key={Pendidikan.id} value={Pendidikan.id}>
                        {Pendidikan.nama}
                      </option>
                    ))}
                  </select>
                  {errMessageField("jenjang_id") && (
                    <span className="text-red">
                      {errMessageField("jenjang_id")}
                    </span>
                  )}
                </div>
              </div> */}
              <div className="col-md-12 mt-6">
                <div className="form-row  mb-4">
                  <h4 className="fw-bold text-muted mt-5 mb-0">
                    Domisili
                    <span className="form-caption font-size-sm text-italic my-2 ms-2">
                      <TanggalUpdateDomisili
                        tanggal={userProfile?.update_at_domisili}
                        jumlahUpdate={userProfile?.count_update_domisili || 0}
                      ></TanggalUpdateDomisili>
                    </span>
                  </h4>
                  <small className="font-size-sm">
                    * Anda hanya dapat mengubah alamat domisili (provinsi)
                    sebanyak 2 kali dalam satu bulan
                  </small>
                </div>
              </div>
              {/* <div className="col-md-12 mb-5">
                <div className="form-group">
                  <label className="font-size-14 fw-bold">
                    Alamat Lengkap&nbsp;<span className="text-red">*</span>
                  </label>
                  <input
                    disabled={!enableEditDomisili}
                    type="text"
                    className={`form-control form-control-sm ${
                      errMessageField("address") ? "is-invalid" : ""
                    } ${enableEditDomisili ? "" : "disabled"}`}
                    placeholder="Alamat Lengkap"
                    {...register("address")}
                  />
                  {errMessageField("address") && (
                    <span className="text-red">
                      {errMessageField("address")}
                    </span>
                  )}
                </div>
              </div> */}
              <div className="col-md-6 mb-5">
                <div className="form-group">
                  <label className="font-size-14 fw-bold">
                    Provinsi&nbsp;<span className="text-red">*</span>
                  </label>
                  <select
                    disabled={!enableEditDomisili}
                    className={`form-control form-control-sm ${
                      errMessageField("provinsi_id") ? "is-invalid" : ""
                    } ${enableEditDomisili ? "" : "disabled"}`}
                    onChange={(v) => {
                      // console.log(v)
                      onChangeProvinsi(v);
                      setFormProfilByIndex("provinsi_id", v.target.value);
                    }}
                    value={FormProfil["provinsi_id"] || ""}
                  >
                    <option disabled value="">
                      Pilih Provinsi Domisili
                    </option>
                    {FListProvinsi.map((Provinsi) => (
                      <option
                        key={Provinsi.id}
                        // selected={Provinsi.id == FormProfil["provinsi_id"]}
                        value={Provinsi.id}
                      >
                        {Provinsi.name}
                      </option>
                    ))}
                  </select>
                  {errMessageField("provinsi_id") && (
                    <span className="text-red">
                      {errMessageField("provinsi_id")}
                    </span>
                  )}
                </div>
              </div>
              <div className="col-md-6 mb-5">
                <div className="form-group">
                  <label className="font-size-14 fw-bold">
                    Kota/Kabupaten&nbsp;<span className="text-red">*</span>
                  </label>
                  <select
                    disabled={!enableEditDomisili || FReadOnlyKota}
                    className={`form-control form-control-sm ${
                      errMessageField("kota_id") ? "is-invalid" : ""
                    } ${enableEditDomisili ? "" : "disabled"}`}
                    onChange={(v) => {
                      onChangeKota(v);
                      setFormProfilByIndex("kota_id", v.target.value);
                    }}
                    // disabled={FReadOnlyKota}
                    value={FormProfil["kota_id"] || ""}
                  >
                    <option
                      disabled
                      // selected={!FormProfil["kota_id"]}
                      value=""
                    >
                      Pilih Kota/Kabupaten
                    </option>
                    {FListKota.map((Kota) => (
                      <option
                        key={Kota.id}
                        value={Kota.id}
                        // selected={FormProfil["kota_id"] == Kota.id}
                      >
                        {Kota.name}
                      </option>
                    ))}
                  </select>
                  {errMessageField("kota_id") && (
                    <span className="text-red">
                      {errMessageField("kota_id")}
                    </span>
                  )}
                </div>
              </div>
              <div className="col-md-6 mb-5">
                <div className="form-group">
                  <label className="font-size-14 fw-bold">
                    Kecamatan&nbsp;<span className="text-red">*</span>
                  </label>
                  <select
                    disabled={!enableEditDomisili || FReadOnlyKecamatan}
                    className={`form-control form-control-sm ${
                      errMessageField("kecamatan_id") ? "is-invalid" : ""
                    } ${enableEditDomisili ? "" : "disabled"}`}
                    onChange={(v) => {
                      onChangeKecamatan(v);
                      setFormProfilByIndex("kecamatan_id", v.target.value);
                    }}
                    // disabled={FReadOnlyKecamatan}
                    value={FormProfil["kecamatan_id"] || ""}
                  >
                    <option
                      disabled
                      // selected={!FormProfil["kecamatan_id"]}
                      value=""
                    >
                      Pilih Kecamatan
                    </option>
                    {FListKecamatan.map((Kecamatan) => (
                      <option
                        key={Kecamatan.id}
                        value={Kecamatan.id}
                        // selected={FormProfil["kecamatan_id"] == Kecamatan.id}
                      >
                        {Kecamatan.name}
                      </option>
                    ))}
                  </select>
                  {errMessageField("kecamatan_id") && (
                    <span className="text-red">
                      {errMessageField("kecamatan_id")}
                    </span>
                  )}
                </div>
              </div>
              <div className="col-md-6 mb-5">
                <div className="form-group">
                  <label className="font-size-14 fw-bold">
                    Desa/Kelurahan&nbsp;<span className="text-red">*</span>
                  </label>
                  <select
                    disabled={!enableEditDomisili || FReadOnlyKelurahan}
                    className={`form-select form-select-sm ${
                      errMessageField("kelurahan_id") ? "is-invalid" : ""
                    } ${enableEditDomisili ? "" : "disabled"}`}
                    onChange={(v) => {
                      // onChangeKecamatan(v)
                      setFormProfilByIndex("kelurahan_id", v.target.value);
                    }}
                    // disabled={FReadOnlyKelurahan}
                    value={FormProfil["kelurahan_id"] || ""}
                  >
                    <option
                      disabled
                      // selected={!FormProfil['kelurahan_id']}
                      value=""
                    >
                      Pilih Desa/Kelurahan
                    </option>
                    {FListKelurahan.map((Kelurahan) => (
                      <option
                        key={Kelurahan.id}
                        value={Kelurahan.id}
                        // selected={FormProfil['kelurahan_id'] == Kelurahan.id}
                      >
                        {Kelurahan.name}
                      </option>
                    ))}
                  </select>
                  {errMessageField("kelurahan_id") && (
                    <span className="text-red">
                      {errMessageField("kelurahan_id")}
                    </span>
                  )}
                </div>
              </div>
              <div className="col-md-12 mt-6">
                <div className="form-row">
                  <h4 className="fw-bold text-muted mt-5">
                    Pekerjaan Saat ini
                  </h4>
                </div>
              </div>
              <div className="col-md-12 mb-5">
                <div className="form-group">
                  <label className="font-size-14 fw-bold">
                    Status Pekerjaan&nbsp;<span className="text-red">*</span>
                  </label>
                  <select
                    className={
                      errMessageField("status_pekerjaan_id")
                        ? "form-select form-select-sm is-invalid"
                        : "form-select form-select-sm"
                    }
                    value={FormProfil["status_pekerjaan_id"]}
                    onChange={(e) => {
                      setFormProfilByIndex(
                        "status_pekerjaan_id",
                        e.target.value,
                      );
                    }}
                  >
                    <option disabled value="">
                      Pilih Status Pekerjaan
                    </option>
                    {FListStatusPekerjaan.map((StatusPekerjaan) => (
                      <option
                        key={StatusPekerjaan.id}
                        value={StatusPekerjaan.id}
                      >
                        {StatusPekerjaan.nama}
                      </option>
                    ))}
                  </select>
                  {errMessageField("status_pekerjaan_id") && (
                    <span className="text-red">
                      {errMessageField("status_pekerjaan_id")}
                    </span>
                  )}
                </div>
              </div>
              {FormProfil["status_pekerjaan_id"] &&
              !["17", "18"].includes(`${FormProfil["status_pekerjaan_id"]}`) ? (
                <>
                  <div className="col-md-6 mb-5">
                    <div className="form-group">
                      <label className="font-size-14 fw-bold">
                        Pekerjaan&nbsp;<span className="text-red">*</span>
                      </label>
                      <select
                        className={
                          errMessageField("pekerjaan_id")
                            ? "form-select form-select-sm is-invalid"
                            : "form-select form-select-sm"
                        }
                        {...register("pekerjaan_id")}
                      >
                        <option disabled value="">
                          Pilih Pekerjaan
                        </option>
                        {FListBidangPekerjaan.map((BidangPekerjaan) => (
                          <option
                            key={BidangPekerjaan.id}
                            value={BidangPekerjaan.id}
                          >
                            {BidangPekerjaan.nama}
                          </option>
                        ))}
                      </select>
                      {errMessageField("pekerjaan_id") && (
                        <span className="text-red">
                          {errMessageField("pekerjaan_id")}
                        </span>
                      )}
                    </div>
                  </div>

                  <div className="col-md-6 mb-5">
                    <div className="form-group">
                      <label className="font-size-14 fw-bold">
                        Institusi Tempat Bekerja&nbsp;
                        <span className="text-red">*</span>
                      </label>
                      <input
                        onKeyUpCapture={(e) => {
                          PortalHelper.keyup(e);
                        }}
                        type="text"
                        className={
                          errMessageField("perusahaan")
                            ? "form-control form-control-sm is-invalid"
                            : "form-control form-control-sm"
                        }
                        placeholder="Institusi Tempat Bekerja"
                        {...register("perusahaan")}
                      />
                      {errMessageField("perusahaan") && (
                        <span className="text-red">
                          {errMessageField("perusahaan")}
                        </span>
                      )}
                    </div>
                  </div>
                </>
              ) : (
                <></>
              )}

              {/* <div className="col-md-12 mt-6">
                <div className="form-row">
                  <h4 className="fw-bold text-muted mt-5">Kontak Darurat</h4>
                </div>
              </div>
              <div className="col-md-4 mb-5">
                <div className="form-group">
                  <label className="font-size-14 fw-bold">
                    Nama Kontak Darurat&nbsp;<span className="text-red">*</span>
                  </label>
                  <input
                    {...PortalHelper.addNameValidation()}
                    type="text"
                    className={
                      errMessageField("nama_kontak_darurat")
                        ? "form-control form-control-sm is-invalid"
                        : "form-control form-control-sm"
                    }
                    placeholder="Nama Kontak Darurat"
                    {...register("nama_kontak_darurat")}
                  />
                  {errMessageField("nama_kontak_darurat") && (
                    <span className="text-red">
                      {errMessageField("nama_kontak_darurat")}
                    </span>
                  )}
                </div>
              </div>
              <div className="col-md-4 mb-5">
                <div className="form-group">
                  <div className="d-flex justify-content-between">
                    <label className="font-size-14 fw-bold">
                      Nomor Handphone&nbsp;<span className="text-red">*</span>
                    </label>

                    {lenKontakDarurat > phoneCodeE?.phone_code.length && (
                      <a
                        href="#"
                        onClick={(e) => {
                          e.preventDefault();
                          setValue("nomor_handphone", "");
                        }}
                      >
                        Clear
                      </a>
                    )}
                  </div>
                  <div className="input-group">
                    <button
                      type="button"
                      className="btn btn-outline-secondary btn-sm dropdown-toggle"
                      data-bs-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      <span style={{ fontWeight: "lighter" }}>
                        {phoneCodeE?.emoji || ""} {phoneCodeE?.phone_code || ""}
                      </span>
                    </button>
                    {codesE && (
                      <div
                        className="dropdown-menu h-200p"
                        style={{ overflowY: "auto" }}
                      >
                        {codesE &&
                          codesE.map((code, i) => (
                            <a
                              key={i}
                              className="dropdown-item"
                              onClick={() => {
                                setPhoneCodeE(code);
                              }}
                            >
                              {code.emoji} {code.country_name}{" "}
                              {`+${code.phone_code}`}
                            </a>
                          ))}
                      </div>
                    )}
                    <span
                      style={{
                        position: "absolute",
                        top: 14,
                        right: 10,
                        zIndex: 1,
                        fontSize: 12,
                      }}
                    >
                      {lenKontakDarurat}
                    </span>

                    <input
                      type="tel"
                      {...PortalHelper.addPhoneNumberValidation()}
                      className={`zero-input-focus form-control form-control-sm ${
                        errMessageField("nomor_handphone_darurat")
                          ? "is-invalid"
                          : ""
                      }`}
                      placeholder=""
                      name="nomor_hp"
                      {...register("nomor_handphone_darurat", {
                        maxLength: 15,
                      })}
                      maxLength={15 - phoneCodeE.phone_code.length}
                    />
                  </div>
                  {errMessageField("nomor_handphone_darurat") && (
                    <span className="text-red">
                      {errMessageField("nomor_handphone_darurat")}
                    </span>
                  )}
                </div>
              </div>
              <div className="col-md-4 mb-5">
                <div className="form-group">
                  <label className="font-size-14 fw-bold">
                    Hubungan&nbsp;<span className="text-red">*</span>
                  </label>
                  <select
                    value={FormProfil["hubungan"] || ""}
                    className={
                      FormProfilError["hubungan"]
                        ? "form-select form-select-sm is-invalid"
                        : "form-select form-select-sm"
                    }
                    {...register("hubungan")}
                    onChange={(v) => {
                      // console.log(v.target.value);
                      setFormProfilByIndex("hubungan", v.target.value);
                    }}
                  >
                    <option selected disabled value="">
                      Pilih Hubungan
                    </option>
                    {FListHubunganKontakDarurat.map((Hubungan) => (
                      <option key={Hubungan} value={Hubungan}>
                        {Hubungan}
                      </option>
                    ))}
                  </select>
                  {errMessageField("hubungan") && (
                    <span className="text-red">
                      {errMessageField("hubungan")}
                    </span>
                  )}
                </div>
              </div> */}
              <div className="row mt-6">
                <div className="col-6">
                  <button
                    type="button"
                    onClick={() => {
                      setIsEdit(false);
                    }}
                    className="btn btn-outline-blue btn-block rounded-pill"
                  >
                    BATAL
                  </button>
                </div>
                <div className="col-6">
                  <button
                    type="button"
                    onClick={handleSubmit(onSubmitForm)}
                    className="btn btn-blue rounded-pill rounded-pill btn-block rounded-pill"
                  >
                    SIMPAN
                  </button>
                </div>
              </div>
            </div>
          </form>
        </FormProvider>
      </>
    );
  };

  const ViewData = () => {
    const CLoadingOrData = ({ value }) => (
      <LoadingOrData isLoading={isLoading} valueData={value}></LoadingOrData>
    );

    return (
      <>
        <div className="row my-5">
          <div className="col-12 pl-0 col-md-6">
            <div className="form-group mb-3">
              <div className="row align-items-end">
                <div className="col-md mb-md-0">
                  <label className="font-size-14 fw-bold">Nama Lengkap</label>
                  <p style={{ textTransform: "capitalize" }}>
                    <CLoadingOrData value={userProfile.nama}></CLoadingOrData>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="form-group mb-3">
              <div className="row align-items-end">
                <div className="col-md mb-md-0">
                  <label className="font-size-14 fw-bold">
                    NIK &nbsp;
                    {userProfile.flag_nik == 1 ? (
                      <span className="form-caption font-size-sm text-italic my-2 text-green">
                        <i className="fa fa-check-circle"></i>
                      </span>
                    ) : (
                      <span className="form-caption font-size-sm text-italic my-2 text-red">
                        (Belum Verifikasi)
                      </span>
                    )}
                  </label>
                  <p>
                    <CLoadingOrData
                      value={PortalHelper.mask(userProfile.nik, 7, 14)}
                    ></CLoadingOrData>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="form-group mb-3">
              <div className="row align-items-end">
                <div className="col-md mb-md-0">
                  <label className="font-size-14 fw-bold">
                    Email &nbsp;
                    {userProfile.email_verifikasi == true ? (
                      <span className="form-caption font-size-sm text-italic my-2 text-green">
                        <i className="fa fa- check-circle"></i>
                      </span>
                    ) : (
                      <span className="form-caption font-size-sm text-italic my-2 text-red">
                        (Belum Verifikasi)
                      </span>
                    )}
                  </label>
                  <p>
                    <CLoadingOrData
                      value={PortalHelper.mask(
                        userProfile.email,
                        null,
                        null,
                        "@",
                      )}
                    ></CLoadingOrData>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="form-group mb-3">
              <div className="row align-items-end">
                <div className="col-md mb-md-0">
                  <label className="font-size-14 fw-bold">
                    No. Handphone &nbsp;
                    {userProfile.handphone_verifikasi == true ? (
                      <span className="form-caption font-size-sm text-italic my-2 text-green">
                        <i className="fa fa-check-circle"></i>
                      </span>
                    ) : (
                      <span className="form-caption font-size-sm text-italic my-2 text-red">
                        (Belum Verifikasi)
                      </span>
                    )}
                  </label>
                  <p>
                    <CLoadingOrData
                      value={PortalHelper.mask(
                        userProfile.nomor_handphone,
                        7,
                        14,
                      )}
                    ></CLoadingOrData>
                  </p>
                </div>
              </div>
            </div>
          </div>
          {/* <div className="col-12 col-md-6">
            <div className="form-group mb-3">
              <div className="row align-items-end">
                <div className="col-md mb-md-0">
                  <label className="font-size-14 fw-bold">Tempat Lahir</label>
                  <p style={{ textTransform: "capitalize" }}>
                    <CLoadingOrData
                      value={userProfile.tempat_lahir}
                    ></CLoadingOrData>
                  </p>
                </div>
              </div>
            </div>
          </div> */}
          <div className="col-12 col-md-6">
            <div className="form-group mb-3">
              <div className="row align-items-end">
                <div className="col-md mb-md-0">
                  <label className="font-size-14 fw-bold">Tgl. Lahir</label>
                  <p>
                    <CLoadingOrData
                      value={userProfile.tanggal_lahir}
                    ></CLoadingOrData>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="form-group mb-3">
              <div className="row align-items-end">
                <div className="col-md mb-md-0">
                  <label className="font-size-14 fw-bold">Jenis Kelamin</label>
                  <p>
                    <CLoadingOrData
                      value={
                        isNaN(userProfile?.jenis_kelamin)
                          ? userProfile?.jenis_kelamin
                          : MasterDataHelper.jenisKelamin[
                              userProfile.jenis_kelamin
                            ]
                      }
                    ></CLoadingOrData>
                  </p>
                </div>
              </div>
            </div>
          </div>
          {/* <div className="col-12 col-md-6">
            <div className="row align-items-end">
              <div className="col-md mb-md-0">
                <label className="font-size-14 fw-bold">
                  Pendidikan Terakhir
                </label>
                <p>
                  <CLoadingOrData
                    value={userProfile.jenjang_nama}
                  ></CLoadingOrData>
                </p>
              </div>
            </div>
          </div> */}
        </div>
        <hr />
        <div className="row mb-5">
          <div className="form-row mb-4">
            <h4 className="fw-bold text-muted mt-5 mb-0">
              Domisili
              <span className="form-caption font-size-sm text-italic my-2 ms-2">
                <TanggalUpdateDomisili
                  tanggal={userProfile?.update_at_domisili}
                  jumlahUpdate={userProfile?.count_update_domisili || 0}
                ></TanggalUpdateDomisili>
              </span>
            </h4>
            <small className="font-size-sm">
              * Anda hanya dapat mengubah alamat domisili (provinsi) sebanyak 2
              kali dalam satu bulan
            </small>
          </div>
          {/* <div className="col-12">
            <div className="form-group mb-3">
              <div className="row align-items-end">
                <div className="col-md mb-md-0">
                  <label className="font-size-14 fw-bold">Alamat Lengkap</label>
                  <p>
                    <CLoadingOrData
                      value={userProfile.address}
                    ></CLoadingOrData>
                  </p>
                </div>
              </div>
            </div>
          </div> */}
          <div className="col-12 col-md-6">
            <div className="form-group mb-3">
              <div className="row align-items-end">
                <div className="col-md mb-md-0">
                  <label className="font-size-14 fw-bold">Provinsi</label>
                  <p>
                    <CLoadingOrData
                      value={userProfile.provinsi_nama}
                    ></CLoadingOrData>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="form-group mb-3">
              <div className="row align-items-end">
                <div className="col-md mb-md-0">
                  <label className="font-size-14 fw-bold">Kota/Kab</label>
                  <p>
                    <CLoadingOrData
                      value={userProfile.kota_nama}
                    ></CLoadingOrData>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="form-group mb-3">
              <div className="row align-items-end">
                <div className="col-md mb-md-0">
                  <label className="font-size-14 fw-bold">Kecamatan</label>
                  <p>
                    <CLoadingOrData
                      value={userProfile.kecamatan_nama}
                    ></CLoadingOrData>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="form-group mb-3">
              <div className="row align-items-end">
                <div className="col-md mb-md-0">
                  <label className="font-size-14 fw-bold">Desa/Kelurahan</label>
                  <p>
                    <CLoadingOrData
                      value={userProfile.kelurahan_nama}
                    ></CLoadingOrData>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr />
        <div className="row mb-5">
          <div className="form-row">
            <h4 className="fw-bold text-muted mt-5">Pekerjaan</h4>
          </div>
          <div className="col-12 col-md-12">
            <div className="form-group mb-3">
              <div className="row align-items-end">
                <div className="col-md mb-md-0">
                  <label className="font-size-14 fw-bold">Status</label>
                  <p>
                    <CLoadingOrData
                      value={userProfile.status_pekerjaan_nama}
                    ></CLoadingOrData>
                  </p>
                </div>
              </div>
            </div>
          </div>
          {!["17", "18"].includes(`${userProfile.status_pekerjaan_id}`) ? (
            <>
              <div className="col-12 col-md-6">
                <div className="form-group mb-3">
                  <div className="row align-items-end">
                    <div className="col-md mb-md-0">
                      <label className="font-size-14 fw-bold">Pekerjaan</label>
                      <p>
                        <CLoadingOrData
                          value={userProfile.pekerjaan_nama}
                        ></CLoadingOrData>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-12 col-md-6">
                <div className="form-group mb-3">
                  <div className="row align-items-end">
                    <div className="col-md mb-md-0">
                      <label className="font-size-14 fw-bold">
                        Institusi Tempat Bekerja
                      </label>
                      <p>
                        <CLoadingOrData
                          value={userProfile.perusahaan}
                        ></CLoadingOrData>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </>
          ) : (
            <></>
          )}
        </div>
        <hr />
        {/* <div className="row mb-5">
          <div className="form-row">
            <h4 className="fw-bold text-muted mt-5">Kontak Darurat</h4>
          </div>
          <div className="col-6 col-md-4">
            <div className="form-group mb-3">
              <div className="row align-items-end">
                <div className="col-md mb-md-0">
                  <label className="font-size-14 fw-bold">
                    Nama Kontak Darurat
                  </label>
                  <p style={{ textTransform: "capitalize" }}>
                    <CLoadingOrData
                      value={userProfile.nama_kontak_darurat}
                    ></CLoadingOrData>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-4">
            <div className="form-group mb-3">
              <div className="row align-items-end">
                <div className="col-md mb-md-0">
                  <label className="font-size-14 fw-bold">
                    Nomor Handphone
                  </label>
                  <p>
                    <CLoadingOrData
                      value={userProfile.nomor_handphone_darurat}
                    ></CLoadingOrData>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-4">
            <div className="form-group mb-3">
              <div className="row align-items-end">
                <div className="col-md mb-md-0">
                  <label className="font-size-14 fw-bold">Hubungan</label>
                  <p style={{ textTransform: "capitalize" }}>
                    <CLoadingOrData
                      value={userProfile.hubungan}
                    ></CLoadingOrData>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div> */}
        <div className="row mb-5">
          <div className="form-row place-order">
            <a
              onClick={() => {
                setIsEdit(true);
              }}
              className="btn btn-outline-blue rounded-pill btn-block"
            >
              UBAH DATA DIRI
            </a>
          </div>
        </div>
      </>
    );
  };

  return (
    <>
      <div className="bg-white border rounded-5 p-5 mb-5">
        <div className="card mb-1">
          <div>
            <div className="d-flex justify-content-between w-100 align-content-center">
              <div className="col-md-12">
                <div className="form-row">
                  <h4 className="fw-bold text-muted ps-1 mt-6 mb-0">
                    Data Diri
                  </h4>
                </div>
              </div>
              {isEdit ? (
                <button
                  onClick={() => {
                    setIsEdit(false);
                  }}
                  className="btn bg-white shadow-0 text-muted btn-sm"
                  style={{ marginTop: -10 }}
                >
                  <i className="fa fa-times-circle text-muted fa-2x"></i>
                </button>
              ) : (
                <></>
              )}
            </div>
            {isEdit ? (
              <EditData
                setMainLoading={setMainLoading}
                userProfile={userProfile}
              ></EditData>
            ) : (
              <ViewData></ViewData>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

// tab setting
const Otp = ({
  email = "",
  type = "email",
  onChange = () => {},
  setHideOtp = () => {},
  isLoading = false,
  onComplete = () => {},
  reqAgain = () => {},
  duration = "",
  otpSended = false,
}) => {
  const validationSchema = yup.object({
    "digit-1": yup.string().required(validationMessage.otp),
    "digit-2": yup.string().required(validationMessage.otp),
    "digit-3": yup.string().required(validationMessage.otp),
    "digit-4": yup.string().required(validationMessage.otp),
    "digit-5": yup.string().required(validationMessage.otp),
    "digit-6": yup.string().required(validationMessage.otp),
  });

  const [timestampOtp, settimestampOtp] = useState(
    moment().format("DD/MM/YYYY, HH:mm:ss"),
  );
  useEffect(() => {
    reset();
  }, []);

  const {
    register,
    handleSubmit,
    watch,
    reset,
    formState: { errors },
  } = useForm({
    mode: "onBlur",
    resolver: yupResolver(validationSchema),
  });

  useEffect(() => {
    const subscription = watch((value, { name, type }) => {
      // console.log(value, name, type)
      let v = "";
      Object.keys(value).forEach((k) => {
        v += `${value[k]}`;
      });
      // console.log(v)
      onChange(v);
      if (v.length == "6") {
        onComplete({ pin: v, email: email });
      }
    });
    return () => subscription.unsubscribe();
  }, [watch]);

  const changeElementfocus = (idElement, e) => {
    // console.log(e.code)
    if (e.code.includes("Digit") || e.code.includes("Key")) {
      const el = document.getElementById(idElement);
      el?.focus();
      el?.setSelectionRange(0, 0);
    } else if (e.code.includes("Backspace")) {
      const arr = idElement.split("-");
      const num = parseInt(arr[arr.length - 1]) - 2;
      if (num > 0) {
        let id = arr[0] + "-" + arr[1] + "-" + num;
        // console.log(id)
        document.getElementById(id)?.focus();
      }
    }
    return;
  };

  return (
    <>
      <div id="otp" className="my-3">
        {type == "email" ? (
          <p>
            Masukan Kode OTP yg dikirim ke{" "}
            <strong>email baru anda ({email})</strong>
          </p>
        ) : (
          <p>
            Masukan Kode OTP yg dikirim ke{" "}
            <strong>nomor handphone anda ({email})</strong>
          </p>
        )}

        <div
          className="digit-group"
          data-group-name="digits"
          autoComplete="off"
        >
          <input
            type="text"
            disabled={isLoading}
            onKeyUp={(e) => {
              changeElementfocus(type + "-digit-2", e);
            }}
            className="otp-input text-center m-1"
            id={type + "-digit-1"}
            name="digit-1"
            {...register("digit-1")}
            maxLength="1"
          />
          <input
            type="text"
            disabled={isLoading}
            onKeyUp={(e) => {
              changeElementfocus(type + "-digit-3", e);
            }}
            className="otp-input text-center m-1"
            id={type + "-digit-2"}
            name="digit-2"
            {...register("digit-2")}
            maxLength="1"
          />
          <input
            type="text"
            disabled={isLoading}
            onKeyUp={(e) => {
              changeElementfocus(type + "-digit-4", e);
            }}
            className="otp-input text-center m-1"
            id={type + "-digit-3"}
            name="digit-3"
            {...register("digit-3")}
            maxLength="1"
          />
          <input
            type="text"
            disabled={isLoading}
            onKeyUp={(e) => {
              changeElementfocus(type + "-digit-5", e);
            }}
            className="otp-input text-center m-1"
            id={type + "-digit-4"}
            name="digit-4"
            {...register("digit-4")}
            maxLength="1"
          />
          <input
            type="text"
            disabled={isLoading}
            onKeyUp={(e) => {
              changeElementfocus(type + "-digit-6", e);
            }}
            className="otp-input text-center m-1"
            id={type + "-digit-5"}
            name="digit-5"
            {...register("digit-5")}
            maxLength="1"
          />
          <input
            type="text"
            disabled={isLoading}
            className="otp-input text-center m-1"
            id={type + "-digit-6"}
            name="digit-6"
            {...register("digit-6")}
            maxLength="1"
          />

          <div className="text-red">
            {errors["digit-1"]
              ? errors["digit-1"].message
              : errors["digit-2"]
                ? errors["digit-2"].message
                : errors["digit-3"]
                  ? errors["digit-3"].message
                  : errors["digit-4"]
                    ? errors["digit-4"].message
                    : errors["digit-5"]
                      ? errors["digit-5"].message
                      : errors["digit-6"]
                        ? errors["digit-6"].message
                        : ""}
          </div>

          {otpSended ? (
            <p
              className={`font-12 color-silver mt-3 mb-0 ${
                isLoading ? "pe-none" : ""
              }`}
            >
              Tidak mendapatkan Kode Verifikasi ?
              <a
                onClick={() => {
                  reqAgain();
                  settimestampOtp(moment().format("DD/MM/YYYY, HH:mm:ss"));
                }}
                className="font-500 color-theme cursor-pointer"
              >
                &nbsp;Kirim Ulang
              </a>
            </p>
          ) : (
            <p className="color-silver mt-3 mb-0 ">
              <span className="fw-bolder italic">
                {`Kirim ulang setelah ${duration}`}{" "}
              </span>{" "}
              <br />
              <div className="text-grey small">
                (OTP Terakhir {timestampOtp})
              </div>
            </p>
          )}
          <p>
            atau <br />
            <a
              onClick={() => {
                setHideOtp();
              }}
              className="font-500 text-red cursor-pointer"
            >
              Batalkan Pengajuan Perubahan
            </a>
          </p>
        </div>
      </div>
    </>
  );
};

const UbahPassword = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");

  const yupSchemaValidation = yup.object({
    password_active: yup
      .string()
      .required(
        validationMessage.required.replace("__field", "Password Aktif"),
      ),
    //   .matches(
    //     /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!#%*?&]{8,}$/,
    //     validationMessage.password.replace("__field", "Password")
    //   ),
    password: yup
      .string()
      .test(
        "notEqual",
        validationMessage.notEqualTo
          .replace("__field", "Password baru")
          .replace("__target", "Password Aktif"),
        (value) => {
          return String(value) !== getValues("password_active");
        },
      )
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[-._!"`'#%&,:;<>=@{}~\$\(\)\*\+\/\\\?\[\]\^\|])[A-Za-z\d-._!"`'#%&,:;<>=@{}~\$\(\)\*\+\/\\\?\[\]\^\|]{8,}$/,
        validationMessage.password.replace("__field", "Password"),
      ),
    password_confirmation: yup
      .string()
      .oneOf(
        [yup.ref("password"), null],
        validationMessage.match
          .replace("__field", "Konfirmasi Password")
          .replace("__ref", "Password"),
      ),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    getValues,
  } = useForm({
    mode: "onBlur",
    resolver: yupResolver(yupSchemaValidation),
  });

  const submit = handleSubmit(async (data) => {
    // console.log(data)
    setError("");
    setSuccess("");
    setIsLoading(true);
    const hideMessage = () => {
      reset();
      setTimeout(() => {
        setSuccess("");
        setError("");
      }, 5000);
    };
    try {
      const resp = await ProfilService.ubahPassword(
        data.password_active,
        data.password,
        data.password_confirmation,
      );
      // console.log(resp)
      if (resp.data.success) {
        setSuccess("Berhasil memperbarui password");
        NotificationHelper.Success({
          message: "Berhasil memperbarui password",
          onClose: () => {
            hideMessage();
          },
        });
      } else {
        // setError(resp.data.result);
        NotificationHelper.Failed({
          message: resp.data.result,
          onClose: () => {
            hideMessage();
          },
        });
      }
    } catch (error) {
      setError("Gagal memperbarui");
      NotificationHelper.Failed({
        message: "Gagal memperbarui password",
        onClose: () => {
          hideMessage();
        },
      });
    }
    setIsLoading(false);
  });

  // return <></>
  return (
    <div className="m-5">
      {/* <a
        className={`mt-5 btn btn-block rounded-pill font-size-14 btn-primary btn-sm ${
          isLoading ? "disabled" : ""
        }`}
        disabled={isLoading}
        href={
          process.env.SSO_FORGOT_PASSWORD ?? routeSegment.SSO_FORGOT_PASSWORD
        }
        target="__blank"
      >
        {isLoading ? (
          <div className="spinner-border spinner-border-sm" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        ) : (
          <>Ubah Password</>
        )}
      </a> */}
      <div className="px-5 py-3">
        <div className="bg-info font-size-12 rounded-3 text-blue p-3">
          <i className="bi bi-info-circle-fill me-1"></i>Perubahan Password akan
          berlaku untuk seluruh akses pada platform lainnya
        </div>
        <h5 className="fw-bold my-3">Ubah Password</h5>
        <div className="form-group">
          <label>Password Aktif</label>
          <InputPassword
            inputProps={{
              placeholder: "Masukan password aktif",
              name: "password_active",
            }}
            yupRegister={{ ...register("password_active") }}
            error={errors.password_active?.message}
          >
            {" "}
          </InputPassword>
        </div>
        <div className="form-group">
          <label>Password Baru</label>
          <InputPassword
            inputProps={{
              placeholder: "Masukan password baru",
              name: "password",
            }}
            yupRegister={{ ...register("password") }}
            error={errors.password?.message}
          >
            {" "}
          </InputPassword>
        </div>
        <div className="form-group mb-4">
          <label>Konfirmasi Password Baru</label>
          <InputPassword
            inputProps={{
              placeholder: "Masukan kembali password baru",
              name: "password_confirmation",
            }}
            yupRegister={{ ...register("password_confirmation") }}
            error={errors.password_confirmation?.message}
          >
            {" "}
          </InputPassword>
        </div>

        {error && (
          <div key={"a"} className="alert alert-danger" role="alert">
            {error}
          </div>
        )}
        {success && (
          <div key={"b"} className="alert alert-success" role="alert">
            {success}
          </div>
        )}

        <button
          className={`mt-5 btn btn-block rounded-pill font-size-14 btn-primary btn-sm ${
            isLoading ? "disabled" : ""
          }`}
          disabled={isLoading}
          onClick={submit}
        >
          {isLoading ? (
            <div className="spinner-border spinner-border-sm" role="status">
              <span className="visually-hidden">Loading...</span>
            </div>
          ) : (
            <>SIMPAN PERUBAHAN</>
          )}
        </button>
      </div>
    </div>
  );
};

const UbahEmail = ({ user }) => {
  const [showOtp, setShowOtp] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [otpCode, setOtpCode] = useState("");

  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");
  const [userEmailCaptchaResponse, setUserEmailCaptchaResponse] =
    useState(null);
  const [captchaErrorMessage, setCaptchaErrorMessage] = useState();

  const yupSchemaValidation = yup.object({
    email: yup
      .string()
      .required(validationMessage.required.replace("__field", "Email"))
      .email(validationMessage.invalid.replace("__field", "Email")),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    getValues,
  } = useForm({
    mode: "onBlur",
    resolver: yupResolver(yupSchemaValidation),
  });

  const reqOtp = async (data) => {
    try {
      if (userEmailCaptchaResponse == null) {
        throw new CaptchaError("Captcha tidak boleh kosong");
      }
      setIsLoading(true);
      const capcthaValidate = await PortalService.captchaValidate(
        userEmailCaptchaResponse,
      );
      const resp = await ProfilService.reqOtpEmail(user?.email, data.email);
      if (resp.data.success) {
        if (resp.data?.result?.message == "OTP berhasil dikirim.") {
          setShowOtp(true);
        } else {
          setShowOtp(false);
          NotificationHelper.Failed({
            message:
              resp.data?.result?.message ??
              "Terjadi kesalahan penginputan aplikasi.",
          });
        }
      } else {
        setShowOtp(false);
        NotificationHelper.Failed({
          message:
            resp.data.result?.newemail[0].replace(
              "Newemail",
              "Email yg anda masukan",
            ) || resp.data.message,
        });
      }
      // setShowOtp(true)
    } catch (error) {
      if (error.name == "CaptchaError") {
        setCaptchaErrorMessage(error.message);
      } else {
        setShowOtp(false);
        console.log(error);
        NotificationHelper.Failed({ message: "Terjadi kesalahan" });
      }
    }
    setIsLoading(false);
  };

  const confirmChange = async (data) => {
    setIsLoading(true);
    const hideMessage = () => {
      reset({ email: "" });
      setShowOtp(false);
      setUserEmailCaptchaResponse(null);
    };
    try {
      // console.log(data)
      const resp = await ProfilService.verifyOtpEmail(data.pin, data.email);
      // console.log(resp)
      if (resp.data.success) {
        setSuccess(resp.data.message);
        NotificationHelper.Success({
          message: resp.data.message,
          onClose: () => {
            // eventBus.dispatch("setShowUserSetting", true)
            hideMessage();
          },
        });
        removeCookies(process.env.USER_DATA_KEY);
        PortalHelper.saveUserData(resp.data.result.data);
      } else {
        setError("Gagal memperbarui");
        NotificationHelper.Failed({
          message:
            resp.data.result?.newemail[0].replace(
              "Newemail",
              "Email yg anda masukan",
            ) || resp.data.message,
          onClose: () => {
            // eventBus.dispatch("setShowUserSetting", true)
            hideMessage();
          },
        });
      }
    } catch (error) {
      // console.log(error)
      setError("Gagal memperbarui");
      NotificationHelper.Failed({
        message: "Gagal memperbarui email",
        onClose: () => {
          // eventBus.dispatch("setShowUserSetting", true)
          hideMessage();
        },
      });
    }
    setIsLoading(false);
  };

  return (
    <div className="px-5 py-3">
      {!showOtp ? (
        <>
          <div className="px-5 py-3">
            <div className="bg-info font-size-12 rounded-3 text-blue p-3">
              <i className="bi bi-info-circle-fill me-1"></i>Perubahan Email
              akan berlaku untuk seluruh akses pada platform lainnya
            </div>
            <h5 className="fw-bold my-3">Ubah Email</h5>
            <p>
              Email Anda : <strong>{user?.email}</strong>
            </p>
            <div className="form-group mb-5">
              <input
                type="text"
                className={`form-control form-control-sm ${
                  errors.email ? "is-invalid" : ""
                }`}
                id="email"
                placeholder="Alamat email baru"
                name="email"
                {...register("email")}
              />
              <span className="text-red">{errors.email?.message}</span>
              <ReCAPTCHA
                className="mt-3"
                sitekey={process.env.CAPTCHA_SITEKEY}
                onChange={(e) => {
                  setUserEmailCaptchaResponse(e);
                }}
              ></ReCAPTCHA>
              {!userEmailCaptchaResponse && captchaErrorMessage && (
                <span className="text-red">{captchaErrorMessage}</span>
              )}
            </div>
            <button
              onClick={handleSubmit(reqOtp)}
              className={`mt-5 btn btn-block rounded-pill font-size-14  btn-primary btn-sm ${
                isLoading ? "disabled" : ""
              }`}
              disabled={isLoading}
            >
              {" "}
              {isLoading && (
                <span
                  className="spinner-border spinner-border-sm"
                  role="status"
                  aria-hidden="true"
                ></span>
              )}
              KIRIM OTP{" "}
            </button>
          </div>
        </>
      ) : (
        <Otp
          email={getValues("email")}
          isLoading={isLoading}
          onChange={(v) => {
            // console.log(v)
            setOtpCode(v);
          }}
          setHideOtp={() => {
            setShowOtp(false);
            setUserEmailCaptchaResponse(null);
          }}
          onComplete={(data) => {
            confirmChange(data);
          }}
          reqAgain={() => {
            reqOtp({ email: getValues("email") });
          }}
        ></Otp>
      )}
    </div>
  );
};

const UbahNoHp = ({ user }) => {
  const [phoneCode, setPhoneCode] = useState({
    country_name: "Indonesia",
    country_slug: "ID",
    emoji: "🇮🇩",
    phone: "62",
  });
  const [codes, setCodes] = useState(null);
  const [showOtp, setShowOtp] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [otpCode, setOtpCode] = useState("");

  const [timeInterval, setTimeInterval] = useState(null);
  const [duration, setDuration] = useState("");
  const [isResendOtp, setIsResendOtp] = useState(false);
  const [userHandphoneCaptchaResponse, setUserHandphoneCaptchaResponse] =
    useState(null);
  const [captchaErrorMessage, setCaptchaErrorMessage] = useState(null);
  // const [success, setSuccess] = useState("");

  // const [error, setError] = useState("");
  // const [success, setSuccess] = useState("");

  const [lenNoHp, setLenNoHp] = useState(0);
  const [phoneCheckResult, setPhoneCheckResult] = useState(false);
  const [phoneChecked, setPhoneChecked] = useState(0);

  const yupSchemaValidation = yup.object({
    nomor_handphone: yup
      .number()
      .typeError(
        validationMessage.required.replace("__field", "Nomor Handphone"),
      )
      .test(
        "minLength",
        validationMessage.minLength
          .replace("__field", "Nomor Handphone")
          .replace("__length", 7),
        (val) => String(val).length >= 8,
      )
      .test(
        "maxLength",
        validationMessage.maxLength
          .replace("__field", "Nomor Handphone")
          .replace("__length", 16 - phoneCode.phone.length + 1),
        (val) => String(val).length <= 16 - phoneCode.phone.length,
      )
      .required(
        validationMessage.required.replace("__field", "Nomor Handphone"),
      )
      .test(
        "unique",
        validationMessage.unique.replace("__field", "Nomor Handphone"),
        async (val) => {
          if (!phoneChecked) {
            try {
              setPhoneChecked(true);
              const resp = await PortalService.checkNomorHpDuplicate(
                `${phoneCode.phone}${val}`,
              );
              const res = resp.data;
              if (res.success) {
                setPhoneCheckResult(
                  res.result.data.length == 0 ||
                    !res.result.data[0].stat_notelp,
                );
                return (
                  res.result.data.length == 0 || !res.result.data[0].stat_notelp
                );
              } else {
                throw Error("Belum bisa memeriksa nomor hp.");
              }
            } catch (error) {
              setPhoneCheckResult(false);
              return false;
            }
          } else return phoneCheckResult;
        },
      )
      .test("phone_code", "Nomor Handphone tidak valid", (value) => {
        if (value) {
          if (
            (phoneCode.phone_code == "62" &&
              value.toString().startsWith(phoneCode.phone_code)) ||
            value.toString().startsWith("0")
          ) {
            return false;
          } else {
            return true;
          }
        }
      })
      .label("Nomor Handphone"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    getValues,
    setFocus,
    setError,
    watch,
    trigger,
    setValue,
  } = useForm({
    mode: "onBlur",
    resolver: yupResolver(yupSchemaValidation),
  });

  useEffect(() => {
    const subscription = watch((value, { name, type }) => {
      if (name == "nomor_handphone") {
        setLenNoHp(phoneCode.phone.length + value.nomor_handphone.length);
      }
    });
    return () => subscription.unsubscribe();
  }, [watch(["nomor_handphone"])]);

  useEffect(() => {
    // console.log("fp", user)
    setLenNoHp(phoneCode.phone.length + getValues("nomor_handphone")?.length);
  }, [phoneCode]);

  useEffect(async () => {
    // const resp = await PortalService.kodeNegaraList();
    const codes = CountryCode;
    setCodes(codes);
  }, []);

  const stopCountDown = () => {
    clearInterval(timeInterval);
  };

  const countDown = (isReset = false) => {
    if (isReset) {
      setDuration("");
      return;
    }
    const upperBounddate = moment().add(5, "minutes");
    setIsResendOtp(false);
    setDuration(
      moment.utc(moment(upperBounddate).diff(moment())).format("mm:ss"),
    );
    const localInterval = setInterval(() => {
      duration = moment.duration(
        moment(upperBounddate).diff(moment()),
        "milliseconds",
      );
      setDuration(moment.utc(duration.asMilliseconds()).format("mm:ss"));
      if (duration < 0) {
        setIsResendOtp(true);
        clearInterval(localInterval);
        return;
      }
    }, 1000);
    setTimeInterval(localInterval);
  };

  const reqOtp = async (data) => {
    setIsResendOtp(true);
    if (
      `${phoneCode.phone_code}${data.nomor_handphone}` ==
      `${user.nomor_handphone}`
    ) {
      setError("nomor_handphone", {
        message:
          "Nomor handphone yang anda masukan sama dengan nomor handphone sebelumnya",
      });
      setFocus("nomor_handphone");
      return;
    } else if (
      `${phoneCode.phone_code}${data.nomor_handphone}` ==
      `${user.nomor_handphone_darurat}`
    ) {
      setError("nomor_handphone", {
        message:
          "Nomor handphone yang anda masukan tidak boleh sama dengan nomor kontak darurat anda",
      });
      setFocus("nomor_handphone");
      return;
    }
    try {
      if (userHandphoneCaptchaResponse == null) {
        throw new CaptchaError("Captcha tidak boleh kosong");
      }
      setIsLoading(true);
      const capcthaValidate = await PortalService.captchaValidate(
        userHandphoneCaptchaResponse,
      );
      const resp = await PortalService.sendPhoneOTP(
        phoneCode.phone + data.nomor_handphone,
      );
      if (resp.data.success) {
        setShowOtp(true);
        countDown();
      } else {
        NotificationHelper.Failed({
          message:
            resp.data.result?.newhp[0].replace(
              "Newhp",
              "Nomor HP yg anda masukan",
            ) || resp.data.message,
        });
      }
      // setShowOtp(true)
    } catch (error) {
      // console.log(error);

      if (error.name == "CaptchaError") {
        setCaptchaErrorMessage(error.message);
      } else {
        NotificationHelper.Failed({ message: "Terjadi kesalahan" });
      }
    }
    setIsLoading(false);
  };

  const confirmChange = async (data) => {
    setIsLoading(true);
    const hideMessage = () => {
      setCodes(CountryCode);
      setPhoneCode({
        country_name: "Indonesia",
        country_slug: "ID",
        emoji: "🇮🇩",
        phone: "62",
      });

      reset({ nomor_handphone: "" });
      setShowOtp(false);
      setPhoneCheckResult(false);
      setUserHandphoneCaptchaResponse(null);
    };
    try {
      // console.log(data)
      const resp = await PortalService.verifyPhoneOTP(
        data.nomor_handphone,
        data.pin,
      );
      // console.log(resp)
      if (resp.data.success) {
        // setSuccess(resp.data.message);
        NotificationHelper.Success({
          message: resp.data.message,
          onClose: () => {
            hideMessage();
          },
        });
        // removeCookies
        removeCookies(process.env.USER_DATA_KEY);
        PortalHelper.saveUserData(resp.data.result.data);
      } else {
        setError("Gagal memperbarui");
        NotificationHelper.Failed({
          message:
            resp.data.result?.newhp[0].replace(
              "Newhp",
              "Nomor HP yg anda masukan",
            ) || resp.data.message,
          onClose: () => {
            // eventBus.dispatch("setShowUserSetting", true)
            hideMessage();
          },
        });
      }
    } catch (error) {
      // console.log(error)
      console.log(error);
      setError("Gagal memperbarui");
      NotificationHelper.Failed({
        message: "Gagal memperbarui nomor hp",
        onClose: () => {
          // eventBus.dispatch("setShowUserSetting", true)
          hideMessage();
        },
      });
    }
    setIsLoading(false);
  };

  return (
    <div className="px-5 py-3">
      {!showOtp ? (
        <>
          <div className="px-5 py-3">
            <h5 className="fw-bold my-3">Ubah Nomor Handphone</h5>
            <div className="d-flex justify-content-between">
              <label>
                Nomor aktif saat ini : <strong>{user?.nomor_handphone}</strong>{" "}
                &nbsp;
                {user?.handphone_verifikasi && (
                  <i
                    className="fa fa-check-circle text-green"
                    title="Verified"
                  ></i>
                )}
              </label>

              {lenNoHp > phoneCode.phone.length && (
                <a
                  href="#"
                  onClick={(e) => {
                    e.preventDefault();
                    setValue("nomor_handphone", "");
                  }}
                >
                  Clear
                </a>
              )}
            </div>
            <div className="form-group mb-5">
              <div className="input-group">
                {/* <div className='input-group-prepend'> */}
                <button
                  type="button"
                  className="btn btn-outline-secondary btn-sm dropdown-toggle"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <span style={{ fontWeight: "lighter" }}>
                    {phoneCode.emoji} {phoneCode.phone} +
                  </span>
                </button>
                {codes && (
                  <div
                    className="dropdown-menu h-200p"
                    style={{ overflowY: "auto" }}
                  >
                    {codes &&
                      codes.map((code, i) => {
                        return (
                          <a
                            key={i}
                            className="dropdown-item"
                            onClick={() => {
                              setPhoneCode(code);
                              setValue("nomor_handphone", "");
                            }}
                          >
                            {code.emoji} {code.name} {`+${code.phone}`}
                          </a>
                        );
                      })}
                  </div>
                )}
                <span
                  style={{
                    position: "absolute",
                    top: 14,
                    right: 10,
                    zIndex: 1,
                    fontSize: 12,
                  }}
                >
                  {lenNoHp}
                </span>

                <input
                  type="tel"
                  {...PortalHelper.addPhoneNumberValidation()}
                  className={`zero-input-focus form-control form-control-sm ${
                    errors.nomor_hp ? "is-invalid" : ""
                  }`}
                  placeholder="Masukan nomor handhpone baru anda"
                  name="nomor_hp"
                  maxLength={15 - phoneCode.phone.length}
                  {...register("nomor_handphone")}
                  onChange={(e) => {
                    setPhoneChecked(false);
                    if (phoneCode?.phone == 62) {
                      e.target.value = e.target.value.replace(/^62/, "");
                    }
                    setLenNoHp(e.target.value.length + phoneCode.phone.length);
                  }}
                />
              </div>
              <span className="form-caption font-size-sm text-italic my-2">
                Format penulisan tanpa kode negara, wajib menggunakan Nomor
                Handphone Aktif/WhatsApp
              </span>{" "}
              <br />
              <span className="text-red">
                {errors.nomor_handphone?.message}
              </span>
              <ReCAPTCHA
                className="mt-3"
                sitekey={process.env.CAPTCHA_SITEKEY}
                onChange={(e) => {
                  setUserHandphoneCaptchaResponse(e);
                }}
              ></ReCAPTCHA>
              {!userHandphoneCaptchaResponse && captchaErrorMessage && (
                <span className="text-red">{captchaErrorMessage}</span>
              )}
            </div>
            <button
              onClick={handleSubmit(reqOtp)}
              className={`mt-5 btn btn-block rounded-pill font-size-14 btn-primary btn-sm ${
                isLoading ? "disabled" : ""
              }`}
              disabled={isLoading}
            >
              {isLoading && (
                <span
                  className="spinner-border spinner-border-sm"
                  role="status"
                  aria-hidden="true"
                ></span>
              )}
              KIRIM OTP
            </button>
          </div>
        </>
      ) : (
        <Otp
          type="hp"
          email={phoneCode.phone + getValues("nomor_handphone")}
          isLoading={isLoading}
          onChange={(v) => {
            setOtpCode(v);
          }}
          setHideOtp={() => {
            setShowOtp(false);
            setPhoneCheckResult(false);
            setUserHandphoneCaptchaResponse(null);
            stopCountDown();
          }}
          onComplete={(data) => {
            confirmChange({
              pin: data.pin,
              nomor_handphone: phoneCode.phone + getValues("nomor_handphone"),
            });
          }}
          reqAgain={() => {
            countDown();
            reqOtp({ nomor_handphone: getValues("nomor_handphone") });
          }}
          duration={duration}
          otpSended={isResendOtp}
        ></Otp>
      )}
    </div>
  );
};

const TabSetting = ({ user }) => {
  const btnNavUbahPassword = useRef(null);

  return (
    <div className="card border rounded-5 mb-5 pb-6">
      <ul
        id="user-setting-tab"
        className="nav nav-tabs nav-fill"
        role="tablist"
      >
        <li className="nav-item">
          <a
            ref={btnNavUbahPassword}
            className="nav-link active py-2"
            data-bs-toggle="tab"
            data-bs-target="#nav-ubah-password"
            role="tab"
          >
            Password
          </a>
        </li>
        <li className="nav-item">
          <a
            className="nav-link py-2"
            data-bs-toggle="tab"
            data-bs-target="#nav-ubah-email"
            role="tab"
          >
            Email
          </a>
        </li>
        <li className="nav-item">
          <a
            className="nav-link py-2"
            data-bs-toggle="tab"
            data-bs-target="#nav-ubah-nohp"
            role="tab"
          >
            No HP
          </a>
        </li>
      </ul>
      <div id="user-setting-tab-content" className="tab-content">
        <div
          id="nav-ubah-password"
          className="tab-pane fade show active"
          role="tabpanel"
          aria-labelledby="nav-ubah-password"
        >
          {<UbahPassword></UbahPassword>}
        </div>
        <div
          id="nav-ubah-email"
          className="tab-pane fade"
          role="tabpanel"
          aria-labelledby="nav-ubah-email"
        >
          {<UbahEmail user={user}></UbahEmail>}
        </div>
        <div
          id="nav-ubah-nohp"
          className="tab-pane fade"
          role="tabpanel"
          aria-labelledby="nav-ubah-nohp"
        >
          {<UbahNoHp user={user}></UbahNoHp>}
        </div>
      </div>
    </div>
  );
};

const ModalBatalPelatihan = ({ idModal = "", onClose = () => {} }) => {
  // const [idModal] = useState(");

  const toggleOpenModal = useRef(null);
  const toggleCloseModal = useRef(null);
  const showSuccessModal = useRef(null);
  const [alasanBatalOpt, setAlasanBatalOpt] = useState(null);
  const [showPassword, setShowPassword] = useState(false);

  const yupSchemaValidation = yup.object({
    alasan: yup
      .string()
      .required(validationMessage.required.replace("__field", "Alasan")),
    password: yup
      .string()
      .required(validationMessage.required.replace("__field", "Password"))
      .min(
        8,
        validationMessage.minLength
          .replace("__field", "Password")
          .replace("__length", 8),
      ),
  });

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm({
    mode: "onBlur",
    resolver: yupResolver(yupSchemaValidation),
  });

  const [pelatihan, setPelatihan] = useState({
    nama_pelatihan: "",
    id: "",
    callBack: () => {},
  });

  const [show, setShow] = useState(false);

  useEffect(() => {
    eventBus.on("setModalBatalPendaftaran", async function (res) {
      // console.log(res)
      setPelatihan(res);
      setValue("password", "");
      setValue("alasan", "");
      // fetch alasan
      try {
        const resp = await PelatihanService.fetchListAlasanBatal();
        if (resp.data.result.Status && resp.data.result.Data[0] != null) {
          setAlasanBatalOpt(
            resp.data.result.Data.map((elem) => {
              return {
                label: elem.name,
                value: elem.id,
              };
            }),
          );
        } else {
          throw Error(resp.data.result.Message);
        }
      } catch (err) {
        const message =
          typeof err == "string" ? err : err?.response?.data?.Message;
        NotificationHelper.Failed({
          message:
            message ?? "Terjadi kesalahan saat mengambil alasan pembatalan...",
        });
      }

      // console.log(show)
      setShow(true);
    });
  }, []);

  useEffect(() => {
    showSuccessModal.current.addEventListener("hidden.bs.modal", () => {
      onClose();
      setShow(false);
    });
    if (show && idModal) {
      toggleOpenModal.current?.click();
    } else {
      toggleCloseModal.current?.click();
    }
  }, [show]);

  const batalPelatihan = async (data) => {
    try {
      const resp = await PelatihanService.batalPendaftaran(
        pelatihan.id,
        data.alasan,
        data.password,
      );
      // console.log(resp)
      eventBus.dispatch("success-batal-pendaftaran", resp);
      // if (typeof pelatihan.callBack == "function") {
      //   pelatihan.callBack(resp);
      // }
    } catch (err) {
      NotificationHelper.Failed({
        message:
          err.response.data.result ??
          "Terjadi kesalahaan, silahkan coba lagi nanti...",
      });
    }
  };

  return (
    <>
      <a
        href="#"
        ref={toggleOpenModal}
        data-bs-toggle="modal"
        data-bs-target={`#${idModal}`}
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseModal}
        data-bs-dismiss="modal"
        data-bs-target={`#${idModal}`}
        hidden
      ></a>

      <div
        className="modal fade"
        id={`${idModal}`}
        ref={showSuccessModal}
        tabIndex={-1}
        role="dialog"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <button
                type="button"
                className="close float-end"
                data-bs-dismiss="modal"
                aria-label="Close"
                onClick={() => {
                  onClose();
                }}
              >
                <span aria-hidden="true">
                  <i className="fa fa-times-circle text-muted fa-2x"></i>
                </span>
              </button>

              <h2
                className="fw-bold text-center mb-1 text-dark"
                id={`${idModal}Title`}
              >
                PERINGATAN
              </h2>
              <p className="font-size-lg text-center text-red fw-bold mb-0">
                Jika melakukan pembatalan, kamu tidak dapat lagi diproses untuk
                mengikuti pelatihan maupun mendaftar ulang pada pelatihan ini.
              </p>
              <p className="font-size-lg text-center text-muted mb-0">
                Apakah kamu yakin membatalkan pendaftaran pelatihan{" "}
                <span className="fw-bold font-size-lg">
                  {pelatihan.nama_pelatihan}
                </span>{" "}
                ?
              </p>
              <div className="mb-md-4 text-center">
                <img
                  src="/assets/img/illustrations/failed-illustration.jpg"
                  className="w-50 image-responsive"
                />
              </div>
              <div className="mb-md-4">
                <form onSubmit={handleSubmit(batalPelatihan)} id="alasan-batal">
                  <div className="mb-md-4 col-12">
                    <select
                      onChange={(e) => {
                        setAlasanPembatalanSel(e.target.value);
                      }}
                      disabled={!alasanBatalOpt}
                      className="form-select form-select-sm shadow-none text-primary rounded"
                      {...register("alasan")}
                    >
                      <option value="" hidden>
                        Pilih alasan
                      </option>
                      {alasanBatalOpt?.map((elem) => (
                        <option value={elem.value}>{elem.label}</option>
                      ))}
                    </select>
                    <span className="text-red">{errors.alasan?.message}</span>
                  </div>
                  <div className="form-group col-12 password-input">
                    <div className="input inner-addon right-addon">
                      <span
                        className="icon"
                        onClick={() => {
                          setShowPassword(!showPassword);
                        }}
                      >
                        <FlipMove>
                          {!showPassword && <i className="fa fa-eye "></i>}
                          {showPassword && <i className="fa fa-eye-slash "></i>}
                        </FlipMove>
                      </span>
                      <input
                        type={showPassword ? "text" : "password"}
                        className={`form-control rounded-3 form-control-sm ${
                          errors.password ? "is-invalid" : ""
                        }`}
                        name="password"
                        {...register("password")}
                        placeholder="Masukkan Password"
                      />
                    </div>
                    <span className="text-red">{errors.password?.message}</span>
                  </div>
                </form>
              </div>

              <div className="row">
                <div className="col-12">
                  <button
                    type="submit"
                    form="alasan-batal"
                    className="btn btn-block bg-red rounded-pill mt-3 lift text-white"
                    onClick={() => {
                      // batalPelatihan();
                    }}
                  >
                    BATALKAN PENDAFTARAN
                  </button>
                </div>
                <div className="col-12">
                  <button
                    className="btn text-dark btn-block mt-2"
                    onClick={() => {
                      setShow(false);
                    }}
                  >
                    TIDAK
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const TabPelatihan = ({ userProfile, timeDiff }) => {
  const [listPelatihan, setListPelatihan] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [limit, setLimit] = useState(5);
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState("terbaru");
  const [listOrder] = useState({
    terbaru: "Terbaru",
    daftar: "Tgl. Pendaftaran",
    status: "Status",
  });
  const [meta, setMeta] = useState({
    total: 1,
    total_current: 1,
    current_page: 0,
    first_page: 1,
    last_page: 1,
    per_page: 10,
    from: 0,
    to: 1,
    prev: null,
    next: 1,
  });
  const [loadMoreLoading, setLoadMoreLoading] = useState(false);
  const router = useRouter();
  useEffect(async () => {
    setPage(1);
    setListPelatihan([]);
    await getListPelatihan();
  }, [order]);

  useEffect(async () => {
    if (loadMoreLoading) {
      if (meta.total > meta.total_current) {
        await getListPelatihan(false, page + 1, listPelatihan);
        setLoadMoreLoading(false);
      }
    }
  }, [loadMoreLoading]);

  const getListPelatihan = async (
    enableSkeleton = true,
    cPage = 1,
    currentList = [],
  ) => {
    setPage(cPage);
    if (enableSkeleton) setIsLoading(true);
    try {
      const resp = await ProfilService.profilPelatihan(cPage, limit, order);
      // console.log("survey", resp.data)
      if (resp.data.success) {
        setListPelatihan([...currentList, ...resp.data.result.data]);
        setMeta(resp.data.result.meta);
      }
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  const callBackBatalDaftar = (resp, listPelatihan) => {
    // console.log("callback called")
    // console.log(resp)
    const { result, message, success } = resp.data;
    if (success) {
      const { user_id, pelatihan_id, status_peserta } = result.data[0];
      let idx = _.findIndex(listPelatihan, ["pelatian_id", pelatihan_id]);
      // console.log(listPelatihan, pelatihan_id, idx);
      if (idx >= 0) {
        let tmp = [...listPelatihan];
        tmp[idx] = { ...listPelatihan[idx], status_peserta: status_peserta };
        setListPelatihan(tmp);
      }
      NotificationHelper.Success({ message: message });
      // "result": {
      //     "data": [
      //         {
      //             "user_id": 156327,
      //             "pelatihan_id": 5071,
      //             "status_peserta": "Pembatalan"
      //         }
      //     ]
      // },
    } else {
      NotificationHelper.Failed({ message: message });
    }
  };

  useEffect(() => {
    // on succes pembatalan
    eventBus.on("success-batal-pendaftaran", (resp) => {
      setListPelatihan((lp) => {
        const { result, message, success } = resp.data;
        if (success) {
          const { user_id, pelatihan_id, status_peserta } = result.data[0];
          let tmp = [...lp];
          let idx = _.findIndex(lp, ["pelatian_id", pelatihan_id]);

          // console.log(lp,pelatihan_id,idx)
          if (idx >= 0) {
            tmp[idx] = { ...lp[idx], status_peserta: status_peserta };
          }
          NotificationHelper.Success({ message: message });

          return tmp;
        } else {
          NotificationHelper.Failed({ message: message });
          return lp;
        }
      });
    });
  }, []);

  const getListPelatihanData = () => {
    return listPelatihan;
  };

  return (
    <>
      {listPelatihan.length != 0 && (
        <div className="mb-4">
          <h4 className="fw-bold mt-5 text-muted mb-n1">Riwayat Pelatihan</h4>
          <div className="d-lg-flex align-items-center rounded">
            <div className="col-12 col-lg-4 col-xl-4 d-lg-flex flex-wrap">
              <p className="font-size-14 mb-lg-0">
                Ditemukan{" "}
                <span className="text-blue font-size-14">
                  {" "}
                  <LoadingOrData
                    isLoading={isLoading}
                    valueData={meta.total}
                  ></LoadingOrData>{" "}
                  Pelatihan
                </span>
              </p>
            </div>
            <div className="ms-lg-auto d-lg-flex flex-wrap">
              <div className="form-floating col-12">
                <select
                  value={order}
                  onChange={(e) => {
                    setOrder(e.target.value);
                  }}
                  className="form-control-xs shadow-none text-primary rounded"
                >
                  {Object.keys(listOrder).map((v, i) => (
                    <option key={i} value={v}>
                      {listOrder[v]}
                    </option>
                  ))}
                </select>
              </div>
            </div>
          </div>
        </div>
      )}
      {isLoading ? (
        <>
          <Skeleton className="h-120p mb-4" count={3}></Skeleton>
        </>
      ) : listPelatihan.length != 0 ? (
        <>
          {listPelatihan.map((pelatihan, i) => {
            const isCancelAvailabel = () => {
              if (
                pelatihan.status_peserta != "Submit" &&
                pelatihan.status_peserta?.toLowerCase() != "belum melengkapi"
              ) {
                return false;
              }
              return (
                moment().isSameOrAfter(moment(pelatihan.pendaftaran_mulai)) &&
                moment().isBefore(moment(pelatihan.pendaftaran_selesai))
              );
            };

            return (
              <div className="bg-white border rounded-5 px-5 mb-5" key={i}>
                <CardPelatihan
                  cardClassName="border-bottom pt-3 mb-3"
                  {...pelatihan}
                  customAction={() => <></>}
                  showStatusPelatihan={false}
                  id={pelatihan.pelatian_id}
                ></CardPelatihan>
                <div className="row gx-2 mb-3">
                  <div className="col-md mb-md-0">
                    <div
                      className={`badge badge-sm mt-3 ${PortalHelper.getColorClassStatusPendaftaran(
                        pelatihan.status_peserta || pelatihan.status,
                      )} mb-3 badge-pill`}
                    >
                      <span className="text-white font-size-sm fw-normal">
                        {pelatihan.status_peserta || pelatihan.status}
                      </span>
                    </div>
                  </div>
                  <div className="col-auto align-self-end">
                    {pelatihan.status_peserta != "Pembatalan" && (
                      <a
                        href="javascript:void(0)"
                        className="btn btn-outline-primary p-2 btn-rounded-circle btn-xs mb-3 font-size-15 me-2"
                        onClick={() => {
                          if (!pelatihan.file_path) {
                            router.push({
                              pathname: `/pelatihan/${pelatihan.pelatian_id}/daftar`,
                            });
                          } else {
                            PortalHelper.axiosDownloadFile(
                              process.env.STORAGE_URL +
                                "dts-dev/" +
                                pelatihan.file_path,
                              `Bukti Pendaftaran_${userProfile.nama}_${pelatihan.nama_pelatihan}.pdf`,
                            );

                            // let a = document.createElement("a");
                            // a.target = "_blank";
                            // a.href =
                            //   process.env.STORAGE_URL + pelatihan.file_path;
                            // a.click();
                            // a.remove();
                          }
                        }}
                      >
                        <i className="bi bi-person-vcard" />
                      </a>
                    )}
                    {isCancelAvailabel() && (
                      <a
                        className="btn bg-red text-white rounded-pill btn-xs px-3 mb-3 font-size-15 me-2"
                        onClick={() => {
                          eventBus.dispatch("setModalBatalPendaftaran", {
                            nama_pelatihan: pelatihan.nama_pelatihan,
                            id: pelatihan.pelatian_id,
                            // callBack: callBackBatalDaftar,
                          });
                        }}
                      >
                        <i className="bi bi-exclamation-octagon" />
                        <span className="ms-2 d-lg-inline-block bg-red text-white rounded-pill d-md-inline-block d-xl-inline-block">
                          Batal Pendaftaran
                        </span>
                      </a>
                    )}

                    <ButtonLMS
                      status_peserta={pelatihan.status_peserta}
                      id_pelatihan={pelatihan.pelatian_id}
                    ></ButtonLMS>
                    {/* <ButtonPresensi
                      status_peserta={pelatihan.status_peserta}
                      id_pelatihan={pelatihan.pelatian_id}
                    ></ButtonPresensi> */}
                  </div>

                  {
                    // status peserta lulus
                    ["7", "8", "19", "20", "21"].includes(pelatihan.status) ? (
                      <>
                        {pelatihan.file_sertifikat != null ? (
                          <ButtonDownloadSertifikat
                            pelatihanSertifikat={pelatihan.file_sertifikat}
                            pelatihanId={pelatihan.pelatian_id}
                            userNama={userProfile.nama}
                            userNoreg={pelatihan.nomor_registrasi}
                            pelatihanNama={pelatihan.nama_pelatihan}
                            timeDiff={timeDiff}
                            pelatihanTema={pelatihan.nama_tema}
                            pelatihanAkademi={pelatihan.nama_akademi}
                            pelatihanTahun={new Date(
                              pelatihan.pelatihan_selesai,
                            ).getFullYear()}
                            pelatihanBulan={
                              new Date(pelatihan.pelatihan_selesai).getMonth() +
                              1
                            }
                            pelatihanSelesai={pelatihan.pelatihan_selesai}
                          ></ButtonDownloadSertifikat>
                        ) : (
                          <>
                            <div className="col-auto align-self-end">
                              <a className="btn bg-yellow rounded-pill btn-xs px-3 mb-3 font-size-15 text-dark">
                                <i className="bi bi-exclamation-triangle"></i>
                                <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                                  Sertifikat Belum Tersedia
                                </span>
                              </a>
                            </div>
                          </>
                        )}
                      </>
                    ) : (
                      <></>
                    )
                  }
                </div>
              </div>
            );
          })}
          {listPelatihan.length < meta.total && (
            <div className="text-center mb-8">
              <a
                className={`btn btn-outline-white mw-300p d-flex mx-auto read-more ${
                  loadMoreLoading ? "disabled" : ""
                }`}
                onClick={() => {
                  setLoadMoreLoading(true);
                }}
                data-bs-toggle="collapse"
                href="#loadcollapseExample"
                role="button"
                aria-expanded="false"
                aria-controls="loadcollapseExample"
              >
                <span className="d-inline-flex mx-auto align-items-center more">
                  {loadMoreLoading ? (
                    <>
                      <span
                        className="spinner-border spinner-border-sm"
                        role="status"
                        aria-hidden="true"
                      ></span>
                      <span className="visually-hidden">Loading...</span>
                    </>
                  ) : (
                    <>
                      <i className="fa fa-chevron-down" />
                      <span className="ms-2">Muat lebih banyak</span>
                    </>
                  )}
                </span>
                <span className="less mx-auto">LOAD LESS</span>
              </a>
            </div>
          )}
        </>
      ) : (
        <>
          <BelumAdaData message="Belum ada riwayat pelatihan"></BelumAdaData>
        </>
      )}
    </>
  );
};

const TabSubstansi = () => {
  const [listSubstansi, setListSubstansi] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [limit, setLimit] = useState(5);
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState("terbaru");
  const [listOrder] = useState({
    terbaru: "Terbaru",
    daftar: "Tgl. Pendaftaran",
    status: "Status",
  });
  const [meta, setMeta] = useState({
    total: 1,
    total_current: 1,
    current_page: 0,
    first_page: 1,
    last_page: 1,
    per_page: 10,
    from: 0,
    to: 1,
    prev: null,
    next: 1,
  });
  const [loadMoreLoading, setLoadMoreLoading] = useState(false);

  useEffect(async () => {
    setPage(1);
    setListSubstansi([]);
    await getListTestSubstansi();
  }, [order]);

  useEffect(async () => {
    if (loadMoreLoading) {
      if (meta.total > meta.total_current) {
        await getListTestSubstansi(false, page + 1, listSubstansi);
        setLoadMoreLoading(false);
      }
    }
  }, [loadMoreLoading]);

  const getListTestSubstansi = async (
    enableSkeleton = true,
    cPage = 1,
    currentList = [],
  ) => {
    setPage(cPage);
    if (enableSkeleton) setIsLoading(true);
    try {
      const resp = await ProfilService.profilSubstansi(cPage, limit, order);
      // console.log("survey", resp.data)
      if (resp.data.success) {
        setListSubstansi([...currentList, ...resp.data.result.data]);

        setMeta(resp.data.result.meta);
      }
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  return (
    <>
      {listSubstansi.length != 0 && (
        <div className="mb-4">
          <h4 className="fw-bold mt-5 text-muted mb-n1">
            Riwayat Test Substansi
          </h4>
          <div className="d-lg-flex align-items-center rounded">
            <div className="col-12 col-lg-4 col-xl-4 d-lg-flex flex-wrap">
              <p className="font-size-14 mb-lg-0">
                Ditemukan{" "}
                <span className="text-blue font-size-14">
                  {" "}
                  <LoadingOrData
                    isLoading={isLoading}
                    valueData={meta.total}
                  ></LoadingOrData>{" "}
                  Test Substansi
                </span>
              </p>
            </div>
            <div className="ms-lg-auto d-lg-flex flex-wrap">
              <div className="form-floating col-12">
                <select
                  value={order}
                  onChange={(e) => {
                    setOrder(e.target.value);
                  }}
                  className="form-control-xs shadow-none text-primary rounded"
                >
                  {Object.keys(listOrder).map((v, i) => (
                    <option key={i} value={v}>
                      {listOrder[v]}
                    </option>
                  ))}
                </select>
              </div>
            </div>
          </div>
        </div>
      )}
      {isLoading ? (
        <>
          <Skeleton className="h-120p mb-4" count={3}></Skeleton>
        </>
      ) : listSubstansi.length != 0 ? (
        <>
          {listSubstansi.map((substansi, i) => {
            return <CardTesSubstansi {...substansi} key={i}></CardTesSubstansi>;
          })}

          {listSubstansi.length < meta.total && (
            <div className="text-center mb-8">
              <a
                className={`btn btn-outline-white mw-300p d-flex mx-auto read-more ${
                  loadMoreLoading ? "disabled" : ""
                }`}
                onClick={() => {
                  setLoadMoreLoading(true);
                }}
                data-bs-toggle="collapse"
                href="#loadcollapseExample"
                role="button"
                aria-expanded="false"
                aria-controls="loadcollapseExample"
              >
                <span className="d-inline-flex mx-auto align-items-center more">
                  {loadMoreLoading ? (
                    <>
                      <span
                        className="spinner-border spinner-border-sm"
                        role="status"
                        aria-hidden="true"
                      ></span>
                      <span className="visually-hidden">Loading...</span>
                    </>
                  ) : (
                    <>
                      <i className="fa fa-chevron-down" />
                      <span className="ms-2">Muat lebih banyak</span>
                    </>
                  )}
                </span>
                <span className="less mx-auto">LOAD LESS</span>
              </a>
            </div>
          )}
        </>
      ) : (
        <>
          <BelumAdaData message="Belum ada riwayat Test Substansi"></BelumAdaData>
        </>
      )}
    </>
  );
};

const TabSurvey = () => {
  const [listSurvey, setListSurvey] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [limit, setLimit] = useState(5);
  const [page, setPage] = useState(1);
  const [order, setOrder] = useState("terbaru");
  const [listOrder] = useState({ terbaru: "Terbaru", deadline: "Deadline" });
  const [meta, setMeta] = useState({
    total: 1,
    total_current: 1,
    current_page: 0,
    first_page: 1,
    last_page: 1,
    per_page: 10,
    from: 0,
    to: 1,
    prev: null,
    next: 1,
  });
  const [loadMoreLoading, setLoadMoreLoading] = useState(false);

  useEffect(async () => {
    setPage(1);
    setListSurvey([]);
    await getListSurvey();
  }, [order]);

  useEffect(async () => {
    if (loadMoreLoading) {
      if (meta.total > meta.total_current) {
        await getListSurvey(false, page + 1, listSurvey);
        setLoadMoreLoading(false);
      }
    }
  }, [loadMoreLoading]);

  const getListSurvey = async (
    enableSkeleton = true,
    cPage = 1,
    currentList = [],
  ) => {
    setPage(cPage);
    if (enableSkeleton) setIsLoading(true);
    try {
      const resp = await ProfilService.profilSurvey(cPage, limit, order);
      //console.log("survey", resp.data)
      if (resp.data.success) {
        setListSurvey([...currentList, ...resp.data.result.data]);

        setMeta(resp.data.result.meta);
      }
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  return (
    <>
      {listSurvey.length != 0 && (
        <div className="mb-4">
          <h4 className="fw-bold mt-5 text-muted mb-n1">Riwayat Survey</h4>
          <div className="d-lg-flex align-items-center rounded">
            <div className="col-12 col-lg-4 col-xl-4 d-lg-flex flex-wrap">
              <p className="font-size-14 mb-lg-0">
                Ditemukan{" "}
                <span className="font-size-14 text-blue">
                  {" "}
                  {meta.total} Survey
                </span>
              </p>
            </div>
            <div className="ms-lg-auto d-lg-flex flex-wrap">
              <div className="form-floating col-12">
                <select
                  value={order}
                  onChange={(e) => {
                    setOrder(e.target.value);
                  }}
                  className="form-control-xs shadow-none text-primary rounded"
                >
                  {Object.keys(listOrder).map((v, i) => (
                    <option key={i} value={v}>
                      {listOrder[v]}
                    </option>
                  ))}
                </select>
              </div>
            </div>
          </div>
        </div>
      )}
      {isLoading ? (
        <Skeleton count={3} className="h-120p"></Skeleton>
      ) : listSurvey.length != 0 ? (
        <>
          {listSurvey.map((survey, i) => {
            return <CardSurvey key={i} {...survey}></CardSurvey>;
          })}

          {listSurvey.length != meta.total && (
            <div className="text-center mb-8">
              <a
                className={`btn btn-outline-white mw-300p d-flex mx-auto read-more ${
                  loadMoreLoading ? "disabled" : ""
                }`}
                onClick={() => {
                  setLoadMoreLoading(true);
                }}
                data-bs-toggle="collapse"
                href="#loadcollapseExample"
                role="button"
                aria-expanded="false"
                aria-controls="loadcollapseExample"
              >
                <span className="d-inline-flex mx-auto align-items-center more">
                  {loadMoreLoading ? (
                    <>
                      <span
                        className="spinner-border spinner-border-sm"
                        role="status"
                        aria-hidden="true"
                      ></span>
                      <span className="visually-hidden">Loading...</span>
                    </>
                  ) : (
                    <>
                      <i className="fa fa-chevron-down" />
                      <span className="ms-2">Muat lebih banyak</span>
                    </>
                  )}
                </span>
                <span className="less mx-auto">LOAD LESS</span>
              </a>
            </div>
          )}
        </>
      ) : (
        <>
          <BelumAdaData message="Belum ada data Survey"></BelumAdaData>
        </>
      )}
    </>
  );
};

const TabTrivia = () => {
  const [listTrivia, setListTrivia] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [limit, setLimit] = useState(5);
  const [page, setPage] = useState(1);
  const [order, setOrder] = useState("terbaru");
  const [listOrder] = useState({ terbaru: "Terbaru", deadline: "Deadline" });
  const [meta, setMeta] = useState({
    total: 1,
    total_current: 1,
    current_page: 0,
    first_page: 1,
    last_page: 1,
    per_page: 10,
    from: 0,
    to: 1,
    prev: null,
    next: 1,
  });
  const [loadMoreLoading, setLoadMoreLoading] = useState(false);

  useEffect(async () => {
    setPage(1);
    setListTrivia([]);
    await getListTrivia();
  }, [order]);

  useEffect(async () => {
    if (loadMoreLoading) {
      if (meta.total > meta.total_current) {
        await getListTrivia(false, page + 1, listTrivia);
        setLoadMoreLoading(false);
      }
    }
  }, [loadMoreLoading]);

  const getListTrivia = async (
    enableSkeleton = true,
    cPage = 1,
    currentList = [],
  ) => {
    setPage(cPage);
    if (enableSkeleton) setIsLoading(true);
    try {
      const resp = await ProfilService.profilTrivia(cPage, limit, order);
      if (resp.data.success) {
        setListTrivia([...currentList, ...resp.data.result.data]);
        setMeta(resp.data.result.meta);
      }
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };
  return (
    <>
      {listTrivia.length != 0 && (
        <div className="mb-4">
          <h4 className="fw-bold mt-5 text-muted mb-n1">Riwayat Trivia</h4>
          <div className="d-lg-flex align-items-center rounded">
            <div className="col-12 col-lg-4 col-xl-4 d-lg-flex flex-wrap">
              <p className="font-size-14 mb-lg-0">
                Ditemukan{" "}
                <span className="font-size-14 text-blue">
                  {" "}
                  {meta.total} Trivia
                </span>
              </p>
            </div>
            <div className="ms-lg-auto d-lg-flex flex-wrap">
              <div className="form-floating col-12">
                <select
                  value={order}
                  onChange={(e) => {
                    setOrder(e.target.value);
                  }}
                  className="form-control-xs shadow-none text-primary rounded"
                >
                  {Object.keys(listOrder).map((v, i) => (
                    <option key={i} value={v}>
                      {listOrder[v]}
                    </option>
                  ))}
                </select>
              </div>
            </div>
          </div>
        </div>
      )}
      {isLoading ? (
        <Skeleton count={3} className="h-120p"></Skeleton>
      ) : listTrivia.length != 0 ? (
        <>
          {listTrivia.map((trivia, i) => {
            return <CardTrivia key={i} {...trivia}></CardTrivia>;
          })}

          {listTrivia.length != meta.total && (
            <div className="text-center mb-8">
              <a
                className={`btn btn-outline-white mw-300p d-flex mx-auto read-more ${
                  loadMoreLoading ? "disabled" : ""
                }`}
                onClick={() => {
                  setLoadMoreLoading(true);
                }}
                data-bs-toggle="collapse"
                href="#loadcollapseExample"
                role="button"
                aria-expanded="false"
                aria-controls="loadcollapseExample"
              >
                <span className="d-inline-flex mx-auto align-items-center more">
                  {loadMoreLoading ? (
                    <>
                      <span
                        className="spinner-border spinner-border-sm"
                        role="status"
                        aria-hidden="true"
                      ></span>
                      <span className="visually-hidden">Loading...</span>
                    </>
                  ) : (
                    <>
                      <i className="fa fa-chevron-down" />
                      <span className="ms-2">Muat lebih banyak</span>
                    </>
                  )}
                </span>
                <span className="less mx-auto">LOAD LESS</span>
              </a>
            </div>
          )}
        </>
      ) : (
        <>
          <BelumAdaData message="Belum ada data Trivia"></BelumAdaData>
        </>
      )}
    </>
  );
};

const TabBookmark = ({ timeDiff = null }) => {
  const [bookmark, setBookmark] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [resultBookmark, setResultBookmark] = useState([]);

  useEffect(async () => {
    try {
      const bmList = await ProfilService.bookmark();
      if (bmList.data?.success) {
        // console.log(bmList.data.resu)
        setBookmark(bmList.data.result.data);
        setResultBookmark(bmList.data.result.data);
        setIsLoading(false);
      } else {
        throw bmList;
      }
    } catch (error) {
      setIsLoading(false);
      // console.log("bookmark error");
    }

    // console.log(data)
  }, []);

  const removeItem = async (id) => {
    try {
      const resPelatihanBookmark =
        await PelatihanService.bookmarkByPelatihanUpdate(id);
      // console.log("resPelatihanBookmark", resPelatihanBookmark)
      if (resPelatihanBookmark.data?.success) {
        let data = bookmark;
        data = data.filter((item) => {
          return item.id != id;
        });
        setBookmark(data);
        setResultBookmark(data);
      }
      // console.log(resPelatihanBookmark)
    } catch (error) {}
  };

  const BookmarkItem = ({ item, removeBookmark }) => {
    const [isOnProgresUnBookmark, setIsOnProgresUnBookmark] = useState(false);
    const [time, setTime] = useState(1);
    const [initTime, setInitTime] = useState(5);
    const [countDown, setCountDown] = useState();

    const preUnBookmark = (e) => {
      e.stopPropagation();
      e.nativeEvent.stopImmediatePropagation();
      setIsOnProgresUnBookmark(true);
      // console.log(e)
    };

    useEffect(() => {
      if (isOnProgresUnBookmark) {
        // startCountDown()
        // setTime(initTime)
        // jika bukan 0 akan melakukan count down tapi masih ada bug untuk sementara di atur ke 0
        setTime(0);
      } else {
        cancelCountDown();
      }
    }, [isOnProgresUnBookmark]);

    useEffect(() => {
      // console.log(time)
      if (time == 0) {
        onCountDownEnd();
        cancelCountDown();
      } else if (time == initTime) {
        startCountDown();
      }
    }, [time]);

    const startCountDown = () => {
      // console.log("startCountDown")
      setCountDown(
        setInterval(() => {
          setTime(time--);
        }, 1000),
      );
    };

    const cancelCountDown = () => {
      clearInterval(countDown);
    };

    const onCountDownEnd = () => {
      // console.log("countDown end")
      removeBookmark();
    };

    return (
      <>
        {!isOnProgresUnBookmark ? (
          <>
            <div className="col mb-5">
              <CardPelatihan
                customAction={() => {
                  return (
                    <>
                      {" "}
                      <div className="d-none d-md-flex col pe-xl-2 align-self-center justify-content-end">
                        <a
                          className="badge badge-lg badge-rounded-circle badge-danger font-size-base badge-float-inside top-0"
                          style={{ color: "#de4d6e" }}
                          onClick={preUnBookmark}
                        >
                          <i className="fa fa-heart"></i>
                        </a>
                      </div>
                      <div className="d-flex d-md-none col-12 pe-xl-2 justify-content-end border-top pt-2">
                        <a
                          className="badge badge-lg badge-rounded-circle badge-danger font-size-base badge-float-inside top-0"
                          style={{ color: "#de4d6e" }}
                          onClick={preUnBookmark}
                        >
                          <i className="fa fa-heart"></i>
                        </a>
                      </div>
                    </>
                  );
                }}
                cardClassName="border rounded-5 p-3 lift shadow cursor-pointer"
                {...item}
                current_server_time={timeDiff.serverTime}
                diff={timeDiff.diff}
              ></CardPelatihan>
            </div>
          </>
        ) : (
          <>
            <div className="col mb-5">
              <div className="card border shadow card-secondary lift">
                <div className="row gx-0 py-2 px-3">
                  <div className="col d-flex place-center">
                    {time != 0 ? (
                      <>
                        <h6 className="mb-0">Batalkan perubahan ? ({time})</h6>
                      </>
                    ) : (
                      <>
                        <h6 className="mb-0">Menghapus Data</h6>
                      </>
                    )}
                  </div>
                  <div className="col-1 d-flex place-center justify-content-end">
                    {time != 0 ? (
                      <a
                        onClick={() => {
                          setIsOnProgresUnBookmark(false);
                        }}
                      >
                        Ya
                      </a>
                    ) : (
                      <>
                        <div
                          className="spinner-border text-secondary float-end"
                          style={{ width: 2, height: 2 }}
                          role="status"
                        >
                          <span className="visually-hidden">Loading...</span>
                        </div>
                      </>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      </>
    );
  };

  return (
    <>
      {isLoading ? (
        <>
          <div>
            <div className="container z-index-0">
              <div className="d-lg-flex flex-wrap">
                <div className="mb-5">
                  <p className="mb-lg-0">
                    <Skeleton />
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col-12 col-xl-12 mb-9">
                  <Skeleton className="h-100p mb-5" count={3}></Skeleton>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : (
        <>
          <div className="mb-4">
            <h4 className="fw-bold mt-5 text-muted mb-n1">Riwayat Bookmark</h4>
            <div className="d-lg-flex align-items-center rounded">
              <div className="col-12 col-lg-4 col-xl-4 d-lg-flex flex-wrap">
                <p className="font-size-14 mb-lg-0">
                  Ditemukan{" "}
                  <span className="font-size-14 text-blue">
                    {" "}
                    {resultBookmark.length} Pelatihan
                  </span>
                </p>
              </div>
            </div>
          </div>
          {resultBookmark.length != 0 ? (
            resultBookmark.map((data, i) => {
              return (
                <BookmarkItem
                  item={data}
                  removeBookmark={() => {
                    // console.log("remove data",data)
                    removeItem(data.id);
                  }}
                  key={i}
                ></BookmarkItem>
              );
            })
          ) : (
            <BelumAdaData message="Belum ada bookmark"></BelumAdaData>
          )}

          {/* <div className="text-center mb-8">
                    <a className="btn btn-outline-white mw-300p d-flex mx-auto read-more" data-bs-toggle="collapse" href="#loadcollapseExample" role="button" aria-expanded="false" aria-controls="loadcollapseExample">
                        <span className="d-inline-flex mx-auto align-items-center more">

                            <i className="fa fa-chevron-down" />
                            <span className="ms-2">Muat lebih banyak</span>
                        </span>
                        <span className="less mx-auto">
                            LOAD LESS
                        </span>
                    </a>
                </div> */}
        </>
      )}
    </>
  );
};

const notifStyle = [
  {},
  {
    color: "rgb(27, 142, 250)",
    backgroundColor: "rgba(176, 217, 255,0.5)",
    nama: "Pendaftaran Pelatihan",
    icon: "fa fa-file-alt",
  },
  {
    color: "rgb(230, 250, 52)",
    backgroundColor: "rgba(247, 255, 176,0.5)",
    nama: "Status Pendaftaran",
    icon: "fa fa-exclamation-circle ",
  },
  {
    color: "rgb(250, 154, 27)",
    backgroundColor: "rgba(250, 154, 27,0.2)",
    nama: "Test Substansi",
    icon: "fa fa-edit",
  },
  {
    color: "rgb(30, 247, 88)",
    backgroundColor: "rgba(148, 255, 177,0.5)",
    nama: "Trivia",
    icon: "fa fa-play ",
  },
  {
    color: "rgb(251, 43, 255)",
    backgroundColor: "rgba(253, 148, 255,0.5)",
    nama: "Survey",
    icon: "fa fa-clipboard",
  },
  {
    color: "rgb(247, 209, 20)",
    backgroundColor: "rgba(250, 230, 130,0.5)",
    nama: "Sertifikat Pelatihan",
    icon: "fa fa-award",
  },
  {
    color: "rgb(0, 193, 255)",
    backgroundColor: "rgba(133, 194, 213,0.5)",
    nama: "Status Pelatihan Peserta",
    icon: "fa fa-check",
  },
];

const TabNotifikasi = ({ shallowRouteTab }) => {
  const [idNotifikasi, setIdNotifikasi] = useState(null);
  const [isLoadingDetail, setIsLoadingDetail] = useState(false);
  const [detailNotifikasi, setDetailNotifikasi] = useState({
    notification_id: 10,
    jenis_id: 3,
    jenis_nama: "Tes Substansi",
    jenisref_id: 1268,
    judul: "Test SubstansitES SUBSSS123Test Substansi",
    detail: "Test Substansi sudah tersedia, mohon diselesaikan",
    status: 0,
    status_baca: 1,
    tgl_notification: "2022-08-28 13:31:07",
  });

  const [listNotifikasi, setListNotifikasi] = useState([]);
  const [pagination, setPagination] = useState({ page: 0, limit: 50 });

  const router = useRouter();
  const { id_notifikasi, keyword } = router.query;

  useEffect(async () => {
    try {
      const resp = await ProfilService.notification(
        pagination.page,
        pagination.limit,
        99,
      );
      // console.log(resp)
      if (resp.data.success) {
        setListNotifikasi([...resp.data.result.data]);
      }
    } catch (error) {}
  }, []);

  useEffect(async () => {
    // console.log(id_notifikasi)
    setIdNotifikasi(null);
    if (id_notifikasi) {
      setIdNotifikasi(id_notifikasi);
      setIsLoadingDetail(true);
      const notifSelected = _.filter(listNotifikasi, function (o) {
        return o.notification_id == id_notifikasi;
      });
      try {
        const resp = await ProfilService.notificationGetDetail(
          id_notifikasi,
          notifSelected[0]?.status_baca || 0,
        );
        // console.log(resp)
        if (resp.data.success) {
          if (resp.data.result?.data != null) {
            setDetailNotifikasi(resp.data.result.data[0]);
          }
        }
        eventBus.dispatch("getUnreadNotif", {});
      } catch (error) {}
      setIsLoadingDetail(false);
    }
  }, [id_notifikasi]);

  const goToTabNotifikasi = (id = null, keyword = "") => {
    shallowRouteTab(
      "notifikasi",
      id ? { id_notifikasi: id, keyword: keyword } : {},
    );
  };

  const SearcherAndList = forwardRef((props, ref) => {
    const [filteredData, setFilteredData] = useState([]);
    const [keyword, setKeyword] = useState("");

    useEffect(() => {
      if (props.listNotifikasi) {
        if (keyword) {
          const lk = keyword.toLocaleLowerCase();
          const filtered = _.filter(props.listNotifikasi, function (o) {
            const {
              notification_id,
              jenis_id,
              jenis_nama,
              jenisref_id,
              judul,
              detail,
              status,
              status_baca,
              tgl_notification,
            } = o;
            const jns = jenis_nama?.toLocaleLowerCase();
            const jdl = judul?.toLocaleLowerCase();
            const dtl = detail?.toLocaleLowerCase();
            return jns.includes(lk) || jdl.includes(lk) || dtl.includes(lk);
          });
          setFilteredData(filtered);
        } else {
          setFilteredData(props.listNotifikasi);
        }
      }
    }, [props.listNotifikasi, keyword]);
    useEffect(() => {
      if (props.keyword) {
        setKeyword(props.keyword);
      }
    }, [props.keyword]);

    return (
      <>
        <div style={{ height: "100%", backgroundColor: "var(--bs-gray-100)" }}>
          <div
            className="px-2 py-2"
            style={{ backgroundColor: "var(--bs-gray-100)" }}
          >
            <div className="input-group input-group-sm">
              <input
                type="text"
                className="form-control rounded-pill"
                placeholder="Pencarian"
                value={keyword}
                onChange={(e) => {
                  setKeyword(e.target.value);
                }}
              />
            </div>
          </div>
          <div
            className="overflow-auto"
            style={{ maxHeight: props.maxHeight, scrollbarWidth: "thin" }}
          >
            <div className="list-group">
              {filteredData?.map((v, i) => {
                const { backgroundColor, color, icon } =
                  notifStyle[v.jenis_id % 8];
                return (
                  <a
                    key={i}
                    onClick={() => {
                      props.goToTabNotifikasi(v.notification_id, keyword);
                    }}
                    className={`list-group-item list-group-item-action px-4 py-3 cursor-pointer ${
                      v.notification_id == id_notifikasi ? "notif-active" : ""
                    }`}
                  >
                    <div className="row">
                      <div
                        className="d-flex align-items-center justify-content-center"
                        style={{ width: 75 }}
                      >
                        <div
                          className="rounded-circle ms-2"
                          style={{
                            width: 65,
                            height: 65,
                            position: "absolute",
                            backgroundColor: backgroundColor,
                          }}
                        >
                          <i
                            className={icon}
                            style={{
                              fontSize: 25,
                              color: color,
                              left: 22,
                              top: 20,
                              position: "absolute",
                            }}
                          ></i>
                        </div>
                      </div>

                      <div className="col">
                        <div className="d-flex justify-content-between">
                          <h5
                            className={`mb-1 line-clamp-2 hover-clamp-off ${
                              v.status_baca == 1 ? "fw-normal" : ""
                            }`}
                          >
                            {v.judul}
                          </h5>
                          <small style={{ whiteSpace: "nowrap" }}>
                            <NotificationDate>
                              {v.tgl_notification}
                            </NotificationDate>
                          </small>
                        </div>
                        <p
                          className={`mb-1 line-clamp-1 ${
                            v.status_baca == 1 ? "fw-light" : ""
                          }`}
                        >{`[${v.jenis_nama}] ${
                          v.jenis_nama != "Broadcast"
                            ? v.detail
                                ?.replaceAll("<p>", "")
                                ?.replaceAll("</p>", "")
                            : ""
                        }`}</p>
                      </div>
                    </div>
                  </a>
                );
              })}
              {filteredData.length == 0 && keyword.length != 0 && (
                <>
                  <div className="list-group-item list-group-item-action">
                    <BelumAdaData
                      message={"data tidak ditemukan ..."}
                    ></BelumAdaData>
                  </div>
                </>
              )}
            </div>
          </div>
        </div>
      </>
    );
  });

  return (
    <>
      <div className="bg-white border rounded mb-5 py-1">
        <div className="card shadow">
          <div className="row">
            <div className="d-block col-12 d-md-none mb-4">
              <div className="accordion accordion-flush" id="accordionExample">
                <div className="accordion-item">
                  <h2 className="accordion-header" id="headingOne">
                    <button
                      className="accordion-button"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseOne"
                      aria-expanded="true"
                      aria-controls="collapseOne"
                    >
                      Pencarian
                    </button>
                  </h2>
                  <div
                    id="collapseOne"
                    className="accordion-collapse collapse show"
                    aria-labelledby="headingOne"
                    data-bs-parent="#accordionExample"
                  >
                    <div className="accordion-body p-0">
                      <SearcherAndList
                        keyword={keyword}
                        goToTabNotifikasi={goToTabNotifikasi}
                        maxHeight={400}
                        listNotifikasi={listNotifikasi}
                      ></SearcherAndList>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div
              className="d-none d-md-block col-md-5"
              style={{ minHeight: 600 }}
            >
              <SearcherAndList
                keyword={keyword}
                goToTabNotifikasi={goToTabNotifikasi}
                maxHeight={600}
                listNotifikasi={listNotifikasi}
              ></SearcherAndList>
            </div>
            <div className="col-12 col-md-7 ">
              <hr className="my-2 d-block d-md-none"></hr>
              {idNotifikasi == null ? (
                <div className="py-8">
                  <BelumAdaData message="belum ada notifikasi terpilih"></BelumAdaData>
                </div>
              ) : (
                <>
                  <div className="col-12 ">
                    <div className="row">
                      <div className="col-12">
                        <table className="my-3">
                          <tbody>
                            <tr>
                              <td style={{ width: 80 }}>
                                {/* { color: "rgb(230, 250, 52)", backgroundColor: "rgba(247, 255, 176,0.5)", nama: "Status Pendaftaran", icon: "fa fa-exclamation-circle " }, */}

                                <div
                                  className="rounded-circle text-center"
                                  style={{
                                    height: 75,
                                    width: 75,
                                    background:
                                      notifStyle[detailNotifikasi.jenis_id]
                                        ?.backgroundColor ||
                                      "rgb(230, 250, 52)",
                                    position: "relative",
                                  }}
                                >
                                  <i
                                    className={`${
                                      notifStyle[detailNotifikasi.jenis_id]
                                        ?.icon || "fa fa-exclamation-circle "
                                    }`}
                                    style={{
                                      fontSize: 30,
                                      position: "absolute",
                                      top: 20,
                                      left: 25,
                                      color:
                                        notifStyle[detailNotifikasi.jenis_id]
                                          ?.color || "rgba(247, 255, 176,0.5)",
                                    }}
                                  ></i>
                                </div>
                              </td>
                              <td valign="top">
                                <div className="ms-2">
                                  <h4 className="mt-2 mb-0">
                                    {detailNotifikasi.judul}
                                  </h4>
                                  <small>
                                    [
                                    <strong>
                                      {detailNotifikasi.jenis_nama}
                                    </strong>
                                    ]
                                    <div>
                                      <Moment
                                        locale="ID"
                                        format="dddd, DD MMMM YYYY HH:mm"
                                      >
                                        {detailNotifikasi.tgl_notification}
                                      </Moment>
                                    </div>
                                  </small>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <hr className="my-1"></hr>

                        {detailNotifikasi.jenis_nama == "Broadcast" ? (
                          <div>
                            {/* case jenis broadcast */}
                            {detailNotifikasi.jenis_content ==
                              "Rich Content" && (
                              <div
                                className="broadcast-paragraph-wrapper"
                                dangerouslySetInnerHTML={{
                                  __html: detailNotifikasi.detail,
                                }}
                              ></div>
                            )}
                            {detailNotifikasi.jenis_content == "Image" && (
                              <figure className="w-100 d-block">
                                <img
                                  className="w-100"
                                  src={PortalHelper.urlBroadcastImage(
                                    detailNotifikasi.detail,
                                  )}
                                  alt="Gambar broadcast"
                                />
                              </figure>
                            )}
                            {detailNotifikasi.jenis_content == "Video" && (
                              <div className="w-100 d-block">
                                <iframe
                                  src={detailNotifikasi.detail}
                                  frameBorder="0"
                                  allow="autoplay; encrypted-media"
                                  allowFullScreen
                                  width="100%"
                                  height="100%"
                                  title="video"
                                />
                              </div>
                            )}
                          </div>
                        ) : (
                          <div
                            className="paragraph-wrapper"
                            dangerouslySetInnerHTML={{
                              __html: detailNotifikasi.detail,
                            }}
                          ></div>
                        )}
                      </div>
                    </div>
                  </div>
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const NotificationButton = forwardRef((props, ref) => {
  const goToTabNotifikasi = (id = null) => {
    props.shallowRouteTab("notifikasi", id ? { id_notifikasi: id } : {});
  };
  return (
    <>
      <div ref={ref} id="notification-profile" className=" dropdown">
        <button
          className="btn btn-outline-secondary btn-sm mt-6 font-size-base"
          data-bs-toggle="dropdown"
          aria-expanded="false"
        >
          <i className="fa fa-bell" />{" "}
          {props.unreadNotif?.length != 0 && (
            <NotificationCounter
              withBg={true}
              number={props.unreadNotif?.length || 0}
            ></NotificationCounter>
          )}
        </button>
        <div className="dropdown-menu dropdown-menu-end p-0">
          <a
            className="list-group-item list-group-item-action px-4 py-2 pe-none"
            aria-current="true"
          >
            <p className="mb-1 text-center">
              <NotificationCounter
                number={props.unreadNotif?.length}
              ></NotificationCounter>{" "}
              Notifikasi
            </p>
          </a>
          <div
            className="list-group"
            style={{ maxHeight: 350, overflow: "auto" }}
          >
            {props.unreadNotif?.length > 0 ? (
              props.unreadNotif?.map((v, i) => {
                return (
                  <a
                    key={i}
                    onClick={() => {
                      goToTabNotifikasi(v.notification_id);
                    }}
                    className="list-group-item list-group-item-action px-4 py-2 cursor-pointer"
                    aria-current="true"
                  >
                    <div className="d-flex w-100 justify-content-between">
                      <h5 className="mb-1 line-clamp-1">{v.judul}</h5>
                      <small style={{ whiteSpace: "nowrap" }}>
                        <NotificationDate>
                          {v.tgl_notification}
                        </NotificationDate>
                      </small>
                    </div>
                    <p className="mb-1 line-clamp-1">{v.detail}</p>
                  </a>
                );
              })
            ) : (
              <a
                className="list-group-item list-group-item-action px-4 py-2 cursor-pointer pe-none"
                aria-current="true"
              >
                <div className="d-flex w-100 justify-content-between">
                  <h5 className="mb-1">Tidak ada notifikasi baru</h5>
                </div>
                <p className="mb-1 line-clamp-1">...</p>
              </a>
            )}
          </div>
          <a
            onClick={goToTabNotifikasi}
            className="list-group-item list-group-item-action px-4 py-3 cursor-pointer"
            aria-current="true"
          >
            <p className="mb-1 text-center fw-bold">Tampilkan semua</p>
          </a>
        </div>
      </div>
    </>
  );
});

export default function UserProfile({ showLoadingModal }) {
  const router = useRouter();

  const [namaUserApp, setNamaUserApp] = useState("");

  const [profileDetail, setProfileDetail] = useState({});

  // untuk zindex dropdown notifikasi
  const [isNotificationShown, setIsNotificationShown] = useState(false);
  const [serverMessage, setServerMessage] = useState("");

  const [isLoadingSiteData, setIsLoadingSiteData] = useState(true);
  const [showVerifikasiModal, setShowVerifikasiModal] = useState(false);
  const [unverfiedMessage, setUnverifiedMessage] = useState([]);
  const [timeDiff, setTimeDiff] = useState(0);
  const [displayPadiUMKM, setDisplayPadiUMKM] = useState(false);
  const [submitPadiUMKM, setSubmitPadiUMKM] = useState(false);
  const [loadingPadiUMKM, setLoadingPadiUMKM] = useState(false);
  const [successLoadDiploy, setSuccessLoadDiploy] = useState(false);
  const [urlDiploy, setUrlDiploy] = useState("https://diploy.id");
  const [loadingBeasiswa, setLoadingBeasiswa] = useState(false);
  const [urlBeasiswa, setUrlBeasiswa] = useState(
    "https://beasiswa.kominfo.go.id/profil/",
  );

  const [siteManajemenData, setSiteManajemenData] = useState({
    alamat: "",
    color: [],
    created_at: "",
    created_by: "",
    email: "",
    footer_logo: "",
    header_logo: "",
    koordinat: [],
    link: [],
    logo_description: "",
    notelp: [],
    social_media: [],
  });

  const [unreadNotif, setUnreadNotif] = useState([]);

  const notifProfile = useRef();

  const { menu } = router.query;

  useEffect(async () => {
    const unverfiedFlag = [];
    const userData = PortalHelper.getUserData();
    if (userData?.flag_nik == 0) {
      unverfiedFlag.push("NIK");
    }
    if (!userData?.handphone_verifikasi) {
      unverfiedFlag.push("Nomor Handphone");
    }
    if (userData?.wizard == null || userData?.wizard == 0) {
      unverfiedFlag.push("Wizard");
    }

    try {
      let resp = await PortalService.refreshUserData();
      if (resp.data?.success) {
        PortalHelper.refreshUserCookies(resp.data.result?.data?.user);
      }

      const respSetting = await PortalService.generalDataSiteManajamenen();
      if (respSetting.data.success) {
        let link = JSON.parse(respSetting.data.result[0].link);
        // find diploy
        let diploy = link.find((item) => item.nama_platform == "Diploy");
        if (diploy) {
          setUrlDiploy(diploy.link_public);
        }
        // find beasiswa
        let beasiswa = link.find(
          (item) => item.nama_platform == "Beasiswa Kominfo",
        );
        if (beasiswa) {
          setUrlBeasiswa(beasiswa.link_public);
        }
      }

      const respPelatihan = await ProfilService.profilPelatihan(1, 99);
      if (respPelatihan.data.success) {
        respPelatihan.data.result.data.map((item) => {
          if (item.slug_akademi == "DEA") {
            if (
              item.status == 7 ||
              item.status == 8 ||
              item.status == 21 ||
              item.status == 19 ||
              item.status == 20
            ) {
              setDisplayPadiUMKM(true);
            }
          }
          if (
            item.slug_akademi == "FGA" ||
            item.slug_akademi == "VSGA" ||
            item.slug_akademi == "TSA" ||
            item.slug_akademi == "PROA"
          ) {
            if (
              item.status == 7 ||
              item.status == 8 ||
              item.status == 21 ||
              item.status == 19 ||
              item.status == 20
            ) {
              setSuccessLoadDiploy(true);
            }
          }
        });
      }
      setLoadingBeasiswa(true);
    } catch (err) {
      console.log(err);
    }
    setUnverifiedMessage(unverfiedFlag);
    getTimeDiff();
  }, []);

  const getTimeDiff = async () => {
    const resp = await GeneralService.getServerTime();
    setTimeDiff(resp);
  };

  useEffect(() => {
    const ul = window.document.getElementById("pills-tab");
    const nodes = ul?.querySelector(".nav-link.text-uppercase.active");
    let actId = "";
    if (nodes != null) {
      actId = nodes.id;
    }
    // console.log("nodes ", nodes.id)
    if (router.isReady) {
      if (menu != actId.substring(7)) {
        const a = document.getElementById("nav-tab-" + menu);
        a?.click();
        // if (typeof $ != undefined && a) {
        //     const li = a.parentElement
        //     let moveTo = $(li).position().left;
        //     if (li.previousSibling == null) {
        //         moveTo = 0
        //     } else if (li.nextSibling == null) {
        //         moveTo = ul.clientWidth
        //     }
        //     console.log(moveTo)
        //     $(ul).animate({ scrollLeft: moveTo }, 500);
        // }
      } else {
        document.getElementById("nav-tab-dashboard").click();
      }
    }
  }, [menu, router.isReady]);

  useEffect(async () => {
    await getUserProfil();
    await getSettingData();
    await getListUnreadNotif();

    notifProfile.current?.addEventListener("hide.bs.dropdown", () => {
      setIsNotificationShown(false);
    });
    notifProfile.current?.addEventListener("show.bs.dropdown", () => {
      setIsNotificationShown(true);
    });
    eventBus.on("userDataUpdated", () => {
      // getUserProfil();
    });

    eventBus.on("getUnreadNotif", () => {
      getListUnreadNotif();
    });
  }, []);

  const getListUnreadNotif = async () => {
    try {
      const reps = await ProfilService.notification(0, 1000, 0);
      // console.log(reps)
      if (reps.data.success) {
        let list = reps.data.result?.data || [];
        setUnreadNotif(list);
        eventBus.dispatch("setNotificationCounter", list.length);
      }
    } catch (error) {}
  };

  const getUserProfil = async () => {
    // let objctUser = PortalHelper.userDataBaseObject;
    let objctUser = {};
    try {
      const resp = await ProfilService.profil(2);
      if (resp.data.success) {
        // console.log(resp)
        let profileData = resp.data.result.data;
        // console.log(profileData)
        // objctUser["nomor_handphone_darurat"] = PortalHelper.reValue(
        //   profileData["nomor_handphone_darurat"],
        //   "",
        // );
        Object.keys(profileData).forEach((props) => {
          // igoner some prop

          if (
            !["update_at_domisili", "count_update_domisili"].includes(props)
          ) {
            // console.log(props)
            objctUser[props] = PortalHelper.reValue(profileData[props], "--");
          } else {
            objctUser[props] = profileData[props];
          }
        });

        setNamaUserApp(objctUser.nama);
        setProfileDetail({ ...objctUser });
      } else {
        throw resp.data;
      }
    } catch (err) {
      setNamaUserApp("--");
      Object.keys(objctUser).forEach((props) => {
        objctUser[props] = PortalHelper.reValue(objctUser[props], "--");
      });
      setProfileDetail(objctUser);
      // console.log(err);
    }
  };

  const getSettingData = async () => {
    const settingData = await PortalService.generalDataSiteManajamenen();
    // console.log(settingData)
    if (settingData.data.success) {
      // console.log(settingData.data.result.Data)
      const {
        alamat,
        color,
        created_at,
        created_by,
        email,
        footer_logo,
        header_logo,
        koordinat,
        link,
        logo_description,
        notelp,
        social_media,
      } = settingData.data.result[0];

      setSiteManajemenData({
        alamat: alamat,
        color: JSON.parse(color),
        created_at: created_at,
        created_by: created_by,
        email: email,
        footer_logo: footer_logo,
        header_logo: header_logo,
        koordinat: koordinat?.split(","),
        logo_description: logo_description,
        notelp: JSON.parse(notelp),
        link: JSON.parse(link),
        social_media: JSON.parse(social_media),
      });
      // console.log(siteManajemenData)
    }
    setIsLoadingSiteData(false);
  };

  const shallowRouteTab = (tab, additionalParam = {}) => {
    router.replace(
      {
        pathname: "",
        query: { menu: tab, ...additionalParam },
      },
      undefined,
      { shallow: true },
    );
  };

  const onTabClick = (e) => {
    const m = e.target.id.substring(8);
    // console.log(m)
    shallowRouteTab(m);
  };

  const onSubmitPadiUMKM = async (e) => {
    e.preventDefault();
    try {
      setLoadingPadiUMKM(true);
      const resp = await PortalService.padiUMKMRegisterUser();
      if (resp.data.success) {
        setLoadingPadiUMKM(false);
        window.location.href =
          process.env.PADIUMKM_BASE_URL +
          "register-kominfo?token=" +
          resp.data.result["data"];
      } else {
        throw resp.data;
      }
    } catch (err) {
      setLoadingPadiUMKM(false);
      console.log(err);
    }
  };

  return (
    <div className="bg-white">
      <SurveyMandatoryModalDownloadSertifikat />
      {displayPadiUMKM && (
        <>
          <div
            className="modal fade"
            id="modalPadiUMKM"
            tabIndex="-1"
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
          >
            <div className="modal-dialog modal-lg modal-dialog-centered">
              <div className="modal-content">
                <div className="modal-body">
                  <div
                    className="d-flex justify-content-between"
                    style={{ marginBottom: "40px" }}
                  >
                    <img
                      class="img-fluid shadow-light-lg"
                      src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAEdCAMAAACBhapPAAAC5VBMVEUAAAAAff8AAAAAff8Aff8AAAAAAAAAff8AAAAAfv8AAAAAAAAAAAAAAAAAAAAAff8AAAAAAAAAAAAAAAAAAAAAf/8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAfv8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAff8AAAAAAAAAAAAAff8AAAAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAff8AAAAAff8AAAAAAAAAf/8AAAAAAAAAAAAAAAAAff8Aff8AAAAAAAAAff8AAAAAAAAAAAAAAAAA//8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAff8Aff8AAAAAAAAAff8Af/8Aff8AAAAAfv8AAAAAAAAAAAAAAAAAff8Af/8Aff8AAAAAAAAAAAAAAAAAAAAAAAAAfv8Aff8AAAAAAAAAff8AAAAAAAAAAAAAAAAAAAAAff8AAAAAf/8Ae/8Aff8Af/8AAAAAAAAAAAAAAAAAfP8Aff8AAAAAAAAAe/8AAAAAAAAAAAAAAAAAVf8Aff8AAAAAbf8Aff8Adf8AAAAAAAAAAAAAAAAAfP8AAAAAff8AfP8AAAAAgP8AAAAAff8Afv8Aff8Aff8AAAAAAAAAAAAAff8Aff8Aqv8Aff8Aff8Ae/8AAAAAAAAAZv8AAAAAAAAAAAAAff8AAAAAfv8Aff8AfP8Aff8Aff8Aff8Aev8Aff8Aff8Aff8Aff8Af/8Aff8Aff8AfP8Aff8Aev8AAAAAAAAAAAAAfP8Ae/8AAAAAf/8AAAAAff8AAAAAAAAAAAAAAAAAev8Aff8AeP8Ae/8AAAAAfv8Aff8Aef8Aff8AgP8AAAAAfv8Afv8Afv8Afv8Aff8Aff8Aff8Aff8AfP8Af/8AgP8Aef8Ajv8Afv8Aev8Afv8Afv8Ad/8Afv8Ai/8Af/8Amf8Adf8AeP8AfP8Aav8AgP8Acf8Aff8AgP8Afv8AfP8Af/8+1TolAAAA93RSTlMA///g6AEH80TfOANc95z+wbLS9tgiCBJ5DOICBBsLgoz+M+b7+fgQcmoF5P37UgEG/O/0tEb95/gKfAIP7CTg7vre8vXqu7rTASgxE0yNGJk9yM7ipdud7Ajet9epVivZKwzSf0kc1rGrw/LMQ89nlELLh606HUSBD6ZkWb1B2CJoH3YeGa4DuHQHxQzhbI9OZS5ONhYKknNer82LUDuiPgPlkDMfcAXQX8PAgZZhUrPwyjh1bb2rILYtf5MZQKPGi0y/BCXnYW5UlRZwJFygZ3wTpxaJOIZEyS9WeducGTomCVwuRogtSwsGBRggmAwUCTcsgCVAA99VYAAAD0FJREFUeJztnWdgVEUewPetLHAUg5cTgol4IQUMwdAOJAkk1NDFKF16l2ZHQEQEC+ipoCKgYi9nP3vv/U7P88Qrenav9/b53u6bmTevTNu83dnd+f++wHv7ZjLz29np770YAAAAAAAAAAAAAAAAAAAAABQO+/69544Zd3zw7D7dCcl56m6dbWGab63TnZyc5tY1RFWSDhfqTlDuctmTHlVJnpyoO1E5yr5TA64s6/Y5upOVk9z2Zogry5rdoDthuciNoa4sa67uhOUg7zNcWdbXupOWc9zTnymrCKotH/uZrizrU92JyzHqijiyRulOXY7xNMeVZb2nO3m5xQdcWY/rTl5u8QhX1uW6k5dbzOXK2qY7eblFO66sI3QnL7cAWQqALAVAlgIgSwGQpQDIUgBkKQCyFABZCoAsBUCWAiBLAZClAMhSAGQpALIUAFkKgCwFQJYCIEsBkKUAyFIAZCkAshQAWQqALAVAlgIgS4GLj+Zxnu7kAUABMvHKz6878MrB09d/py0sfWTPeZfpzkpmqXvsqTVDufW5CkPPKVhddRvv994kFwFFM3TnKhPc9v76qEU5NF+sO2tRs/gZ3n0BbWPonbpzFyUNf/hxxkwlKSqgntiB6Cp0Frt15zEiHu6QcVWWdb/uXEbC4rCbeTPAs7oz2nZefyo7qizrRt1ZbTMfZ0uVzVe6M9s2Jp6TRVfW8bqz2yaWuA//yAZX6M5vW9jIvus5I7TTneE28PPsqrKsE3XnOG3qfpFtV/l7J3Xdr7PuKn9L1mnZd2XN1p3p9Jiow1We9krrrtDhynpOd77TYoYWV9ZPdOc7HbLeZ3A4VXe+0+FCPa6sjbozngZfZbnfjsnHFf6GZj2urL/oznka3KLJ1be6M54G12ly9YTujKdBnaYKKy8fZ6qnh7XtQ935Toejdag67THd2U6PI5RzemKHttA89/I9S3RnOk02qmgau/TTLw1+XGvdWHlVt+RrgYiKa2VNNX9hcJFymCjZbWgObef5u5XzcpDMQ7LGYsw6Gba1m59dxOxNsA/e5j0ZV48yg5slS2YDyPPs4GbJkqjez+cEN0rWq2JXD/LCGyXrUaErdn2VxChZwl9hMz+8SbJ+IyxYgoUqk2Q9I3LFq9yTmCRLtMt9jehdQwbJukd088SvRDEYJOtLgauxwhgMknWHQNb/hDEYJEswiO4vviXQIFmCgnVQHIM5suYIZP1DHIU5sl7gu5LZ7GmOLEH/falEFObIEixV/FIiCnNkPc6XdZ1EFObIep4v60qJKMyRJbj5SyYKc2Tdz81pf5kozJF1OsiShy9L6gYkkAUlK4SD3JxCBe/hM76seySiMEfW+XxZMo/VMUfW13xZeySiMEfWF3xZMm9UNUfWxXxZgvXVFObIupIvy6oTR2GOrAaBrJ+KozBHVuxEvqw14hgMknW5oGi9IIzBIFmi3VmnC2MwSJZoecf6SBSDQbImCiotcdEySJb4CSH/EkRgkqyHRLKaBX0tk2S9LpJlncOPwCRZMfEjIx/mhjdK1n+Esqy/8sIbJevvYllFT3PCGyVL5olZ/Rezg5sl62GxLN7WUrNkxaSeNc3cI2KYLMFEPKL5n+GhDZO1REqWNfSV0NCGyYrJPmxz7BMhvfmHjvey//krqJv5C0/Wbvkn5D8o9WKATfvfLFhZSo+DGvXg4j9KTM0vvr1QZcnczUpTNHbbwb99P5zzv93nxHmgUGUdryZLVPiWph7J85H9FRSiLInhtBrtkttRX7+9MGX9N2JZljUj+SSWdgUpK/ZK5Lbm/tlexP2t7nxlBtFkvDrJZ+JzRuD5zIfRv+Nqve48ZY5PIpdlXas7T5kjAy/a+ZPuPGWO6B8Gn49PIZWkIfJKPm9fuCDBpqhlSe1KzVc+j/oNfft15yiTnDcqWlnP6M5QRnkh2nprm+78ZJZNkZatwhwbusyJ8hXIhS4rdluEL5TJz1fqKPFcZLI+052VLHCnwtOWubyqOyfZoCGi96vN0Z2R7PBxFP3TAp6k8bI7glc1fKM7E9nj6aVtdFXgXVIfd6q/r4FiqGkP2v/93PRl5eV7YtrGkifTdCVxS1kBsvvadNZgP9GdbG1sOrBebfWn3dG6k6yXb363VHb+pvkh3YnNBRqeffyRbbM7jOpfxGLoqDdnGF6qAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACBvKV3eOmztgs7K4TovWDvs5eWl0Scoh+k8KJ6kWDXXNcWpcIMK2tbwXg5l6Ph7qTzH42MSavGMQ+FGR5MslKpe0cQWFU6BiMe7OIcr0GE83kkpmoUk3CVRpCqBY1OvDjKJT9YukulZStFMJ+Hqo0hVfsgqIZl+SSman5Fwx6EznXsj8A9chYzK2osSdqxySJ+sN0im1ylF04mEG4nO9MQneiinKcOyjvF9rfL4ZMUq0HGVYjIHo3Dj8AkTZE1xcl2lWk+vqE6FG0zUmCArVlY/c0zT2QOUI2p/9ugxM+vdCsoIWVEBshQAWQoUhKxO0+/tV3HMKdNQE++XNamTw0J/uI4vTUiGu2ESigbRiD6ejI4vSh4kzu3U6Uyc347OB15njW9vHV1RMXP8huGpwz44OvQxW9bOkq2LKir6TVi9oLv/o70oDqf/tHBa68ymlfPunuK5Jpk9PDKb5Vzem2Wq8aWBOBnx4mXJ2tgv6yh03M0T7hJ82qbqcI09N1GFVaAr8JjyyORBmXs14QQqumt+6J5fdLJ94gfooCtfVuN0p811aPL9Gr6LzpfYw/rDleSyEfRlQ4IJq2C4cvvnKSrtnMrISpzhC7euTbKOxb8DROtwOVll7/rj7JoUHSLrRVdVknEr1GWVLQpceIOMrAEjAuGWtUEWFuNSNVxGVg+cVJrV1JQQkfV24LLJyrKCebZtiWW1x15o+qYt652Qz2qvQf/hyFpRHhIwHl8ZlPVa8KrynoqymkL/GIYpqzTMsf3n0b+qskaG5hmrZ8s6iZXwlwOywihGEUnKqudEZcOUdSQ/nKKs9m4DEwZTVtkY6qqBnkgekJEV36Eiawr1cdXWkrd2DMFDXweWrIuoa4rn7XirZDzdIDFkle7q25c0JvV9U7yRumwWFbRp+oYtV6/0lDSmLLeJWTTVbowTb8wn4Wrbh8gacdOhnZO6dKMid2K62U6JM3Eej7c4CdsQlHUpCVWJP72mq4Sss8gV1bjWeYtua0JlJQntlDa6AVeiaqRmvISsXoF0xkpJm3pvQFbtFnSqu1v3vEgSIeyU1pBAxW5nrhR3z9iy3FQOcsfI9G9CTdY0Em6Ze3KqWBapC851gyVIZw1pd2W5U+KJfvjcKnlZG0hEuMudZIBbRhiySO7K6XmIXrVpyiKt/9V04hYIZeFw4+lgPfBF232ySBpsrscnB8vLWovDeCdA7xLJImU9NYwhnJyeLNKkdfWumM0UyDoWHZXb1RUFrq2H+WT1oS/qhs+SPymUhdvmSt8KFynKDFm4CDX54iOtg5Ks7fjcmd7YyI+dIesBdORbZyPdW8cDloUjcSDfqzMKlZBFqix73OSBDHfDZQ3HH1/gC/ejtGStQqcG+hcxcF+OIesUdDSrj4e9+KrrPbKO8kRNugEnycoaGdCL4cuajD/2546UBSVZE9CpEf5U4LaaIYtqiELZ65HlXYwj3zdZzBHJIgUo8AnuNYXLwgWo1h+sDP8+lWThpuk1f3Rb+LK8XcIgTutHzTpQdFeWhQdkgUzHcA8tXNYOdFTtD1aK21ElWfjXttkfnWAgTXcIwzgUqazlacq6Ch1VRSML/5wuVZQVNt9A48zfRSWrNw4R+ARnOlwWng0IhKvhDqRZskajUxP80d3Nl0UPDMNwZn2jkkW6Zni9GENSFS6LNAykKUGQDqGSLDx2cjuICDzkYcgaho6u7hiO0/xEJYs0ei2+8xfwZZFE3+ALR+YwlGSR7SP+RhkXHYas+9BRoK7zEJksXDXFfaunZLKK0SnFVauvLCTIOElJFq4649O80ZGNTgxZuDPLmi53iEwWmXTwDMpih/BplizcHYy/7QlH+qRqsgaQYPQINZYgXyVDFunVeYKVdkegTmBkstzBHDVud2sepizyM/XUdjvdcGJZ9FoUGQTOpFciziaxsQbSuNIaTY/WyNwOypKqrJtYskirF4+7KyJTqBlH1nyWO7+AFgxt7qLm1JiySM+ZLBbYuO5XkjFBwnXFlNUXH5NpUXseD58rVyxZeN3mFKYs3GOyuQ+N3c9wRbBlUctPq5yUJ9xJSp4s0qYMpjaGJtyg1QucUxfhTU48WQnytZJpqS0kqsPojKysFizZs5BGU0bPWw9r2TykybtywJLVnjJa3m/e5nm+RT+2LPzTsecN+9ksT510p67sGZAJq+e3ekcyzGnlLuSSqs3n9uk5ssSNnbQ9srLcclOcTBieaKUh/dJwmAsWJ/DDsWX5VkjQpDR/jYm9uuNOb/sh06Kyskht6hDaxPqWlR0G8nvwSVaHhavGBZUti2o9kiBZZd71DgTu2rNlDSAtpg+3FpOVFbvXE0F4fwTPJlGU3yWxIo0nVihqp3AXWR1u8gTByx2N3qX1FIM6ov9wFllLw21RPTZpWd71WkbnzZv6JJOl9jrMD7jayV++R5BOGi0r1jMwLB6dkFm+L20NcTWV+nPSsuglEnZPt7d3rqPCHvFJ7aJZ5y0Na3sKNoZgloVuDKlx94GnsCfr5HbRdPT/hM/yzLbLy4otpLYkMIcFiQvI0lC86ebkGazPL+s+T7jO291F/LXJCZGALNx58d5rMHzXSly50VuOTlpN2ubK6ckMS245SkylqoTK+d5FFBVZ9l9swe0wWu4Ipdc708e3trx7Alo+HNDeAaeqBh1711FsGrvM2to6b9o658KALBxP8BaBUucj3x1QC+tXdXtt9S40LihFocnIFR3jxWaXxOSS+VtfbjlyS3DLvz8rvqiCNyM5eVXfa6xKZ1w0mPvmAAIp3P6JLiAIWfbJrTuRNFON8O05xpVpsZZE5SrhN12SOZohelKVo5ANVWg3WAp37KF271ihM4n0a+aRc73c1RZ9CctJ3C3rlS2H7N5XzZlr3aHVVbpTl2O4RStINbSFPo5juqqN5ObxwiJkh2+KcnrIDyBCpw3j5e6WVoBiYcjOjGHODjIgQGIqvQaTVLVc8YkiZtFj+/hxzlTDmLPqoWaXobQzlCgAAAAAKGD+D8u7VwIn6yc/AAAAAElFTkSuQmCC"
                      style={{ width: "70px" }}
                    />
                    <img
                      class="img-fluid shadow-light-lg"
                      src="/padi.svg"
                      style={{ width: "100px" }}
                    />
                  </div>
                  <div
                    className="alert bg-info rounded-3"
                    role="alert"
                    style={{ marginBottom: "20px" }}
                  >
                    <div className="row align-items-center mx-n3">
                      <div className="col-auto px-3">
                        <div className="icon-h-p secondary">
                          <i className="bi bi-shop text-blue font-size-18"></i>
                        </div>
                      </div>
                      <div className="col px-3">
                        <h5 className="alert-heading fw-bolder text-blue py-0 my-0">
                          Satu langkah mudah untuk menjual produkmu ke BUMN dan
                          ritel
                        </h5>
                      </div>
                    </div>
                  </div>
                  <h3 className="fw-bold" style={{ marginBottom: "15px" }}>
                    Dengan Menjadi Mitra anda dapat:
                  </h3>
                  <ol>
                    <li>
                      Menjual produk ke 20.000+ pembeli dari BUMN dan ritel
                    </li>
                    <li>Meningkatkan omset penjualan Bersama PaDiUMKM</li>
                    <li>Mendapatkan landing page khusus di PaDiUMKM</li>
                    <li>Berkesempatan mengikuti Bazar di Sarinah, Gratis!</li>
                    <li>Mengikuti campaign/promosi dari PaDiUMKM</li>
                  </ol>
                  <hr style={{ marginTop: "30px", marginBottom: "30px" }} />
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      type="checkbox"
                      value=""
                      id="padiUMKMCheck1"
                      onChange={() => {
                        if (
                          document.getElementById("padiUMKMCheck1").checked &&
                          document.getElementById("padiUMKMCheck2").checked
                        ) {
                          setSubmitPadiUMKM(true);
                        } else {
                          setSubmitPadiUMKM(false);
                        }
                      }}
                    />
                    <label className="form-check-label" for="padiUMKMCheck1">
                      Dengan ini saya menyetujui, dan mengizinkan akun
                      digitalent saya terintegrasi dengan PaDiUMKM untuk menjadi
                      penjual.
                    </label>
                  </div>
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      type="checkbox"
                      value=""
                      id="padiUMKMCheck2"
                      onChange={() => {
                        if (
                          document.getElementById("padiUMKMCheck1").checked &&
                          document.getElementById("padiUMKMCheck2").checked
                        ) {
                          setSubmitPadiUMKM(true);
                        } else {
                          setSubmitPadiUMKM(false);
                        }
                      }}
                    />
                    <label className="form-check-label" for="padiUMKMCheck2">
                      Dengan ini saya meyetujui{" "}
                      <Link href={"https://padiumkm.id/syarat-dan-ketentuan"}>
                        <a target="_blank">Syarat dan Ketentuan</a>
                      </Link>{" "}
                      dan{" "}
                      <Link href={"https://padiumkm.id/kebijakan-privasi"}>
                        <a target="_blank">Kebijakan Privasi</a>
                      </Link>{" "}
                      dari PaDiUMKM
                    </label>
                  </div>
                  {loadingPadiUMKM ? (
                    <div className="d-flex justify-content-center mt-4">
                      <div
                        className="spinner-border text-primary"
                        role="status"
                      >
                        <span className="visually-hidden">Loading...</span>
                      </div>
                    </div>
                  ) : (
                    <button
                      type="button"
                      className={`btn btn-primary rounded-pill w-100 mt-4 ${
                        !submitPadiUMKM ? "disabled" : ""
                      }`}
                      onClick={onSubmitPadiUMKM}
                    >
                      Mulai Sekarang
                    </button>
                  )}
                </div>
              </div>
            </div>
          </div>
        </>
      )}
      <header
        className="pt-5 mb-8 bg-white"
        style={{ backgroundImage: "none" }}
      >
        <VerifikasiHandphone
          show={showVerifikasiModal}
          handleClose={() => {
            setShowVerifikasiModal(false);
          }}
          children={serverMessage}
          onVerify={() => {
            setShowVerifikasiModal(false);
            setUnverifiedMessage([]);
          }}
        />
        <ModalBatalPelatihan idModal="modalBatalPelatihan"></ModalBatalPelatihan>
        {/* Img */}
      </header>
      <section className="pb-80">
        <div
          className="container"
          style={isNotificationShown ? { zIndex: 0 } : {}}
        >
          <div className="row pb-5 pb-md-7">
            <div className="col-lg-3 col-xl-3 col-xxl-3 d-none d-lg-block mb-5">
              <div className="card border-0">
                <div className="bg-light rounded-5">
                  <nav className="d-flex justify-content-between flex-column pt-6 pb-100">
                    <h6 className="fw-bolder text-uppercase ms-5 mb-1">Menu</h6>
                    <ul
                      id="pills-tab"
                      className="nav d-block list-unstyled text-gray-800 course-tab-user font-size-sm-alone mb-3"
                      role="tablist"
                      // style={{ minWidth: 1000 }}
                    >
                      <li className="nav-item">
                        <a
                          onClick={onTabClick}
                          id="nav-tab-dashboard"
                          className="nav-link d-block active"
                          data-bs-toggle="tab"
                          data-bs-target="#nav-dashboard"
                          role="tab"
                          data-offset={0}
                        >
                          <i className="bi bi-bar-chart me-1"></i>Dashboard
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          onClick={onTabClick}
                          id="nav-tab-profile"
                          className="nav-link"
                          data-bs-toggle="tab"
                          data-bs-target="#nav-profile"
                          role="tab"
                          data-offset={0}
                        >
                          <i className="bi bi-person me-1"></i>Data Diri
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          onClick={onTabClick}
                          id="nav-tab-pelatihan"
                          className="nav-link"
                          data-bs-toggle="tab"
                          data-bs-target="#nav-pelatihan"
                          role="tab"
                          data-offset={0}
                        >
                          <i className="bi bi-mortarboard me-1"></i>Pelatihan
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          onClick={onTabClick}
                          id="nav-tab-tes-substansi"
                          className="nav-link"
                          data-bs-toggle="tab"
                          data-bs-target="#nav-tes-substansi"
                          role="tab"
                          data-offset={0}
                        >
                          <i className="bi bi-book me-1"></i>Tes Substansi
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          onClick={onTabClick}
                          id="nav-tab-survey"
                          className="nav-link"
                          data-bs-toggle="tab"
                          data-bs-target="#nav-survey"
                          role="tab"
                          data-offset={0}
                        >
                          <i className="bi bi-clipboard-check me-1"></i>Survey
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          onClick={onTabClick}
                          id="nav-tab-trivia"
                          className="nav-link"
                          data-bs-toggle="tab"
                          data-bs-target="#nav-trivia"
                          role="tab"
                          data-offset={0}
                        >
                          <i className="bi bi-gem me-1"></i>Trivia
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          onClick={onTabClick}
                          id="nav-tab-bookmark"
                          className="nav-link"
                          data-bs-toggle="tab"
                          data-bs-target="#nav-bookmark"
                          role="tab"
                          data-offset={0}
                        >
                          <i className="bi bi-bookmark me-1"></i>Bookmark
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          onClick={onTabClick}
                          id="nav-tab-notifikasi"
                          className="nav-link"
                          data-bs-toggle="tab"
                          data-bs-target="#nav-notifikasi"
                          role="tab"
                          data-offset={0}
                        >
                          <i className="bi bi-bell me-2"></i>
                          Notifikasi{" "}
                          <span className="badge bg-red badge-pill ms-2 text-white">
                            <NotificationCounter></NotificationCounter>
                          </span>
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          onClick={onTabClick}
                          id="nav-tab-setting"
                          className="nav-link"
                          data-bs-toggle="tab"
                          data-bs-target="#nav-setting"
                          role="tab"
                          data-offset={0}
                        >
                          <i className="bi bi-gear me-1"></i>Settings
                        </a>
                      </li>
                      <li className="nav-item">
                        <a
                          onClick={() => {
                            eventBus.dispatch("logout");
                          }}
                          id="nav-tab-setting"
                          className="nav-link"
                          data-bs-toggle="tab"
                          data-bs-target="#nav-setting"
                          role="tab"
                          data-offset={0}
                        >
                          <i className="bi bi-box-arrow-right me-1" />
                          Logout
                        </a>
                      </li>
                    </ul>
                    <h6 className="fw-bolder ms-5 pt-6 mb-1 text-uppercase">
                      Platform
                    </h6>
                    <ul
                      id="pills-tab"
                      className="nav d-block list-unstyled text-gray-800 course-tab-v2 font-size-sm-alone mb-3"
                      role="tablist"
                      // style={{ minWidth: 1000 }}
                    >
                      {isLoadingSiteData ? (
                        <li className="nav-item">
                          <a className="nav-link d-block">
                            <Skeleton
                              className="d-block w-20"
                              count={4}
                            ></Skeleton>
                          </a>
                        </li>
                      ) : (
                        siteManajemenData.link.map((link) => (
                          <li className="nav-item mb-n2">
                            <Link href={link.link_member}>
                              <a className="nav-link d-block">
                                <img
                                  src={PortalHelper.linkIcon(link.icon)}
                                  alt={link.nama_platform}
                                  className="avatar-img shadow-0 avatar-platform img-thumbnail me-1"
                                />
                                <span className="font-size-12">
                                  {link.nama_platform}
                                </span>
                              </a>
                            </Link>
                          </li>
                        ))
                      )}
                    </ul>
                  </nav>
                </div>
              </div>
            </div>

            <div
              className={`${
                menu == "notifikasi"
                  ? "col-lg-9 col-md-12"
                  : "col-lg-9 col-md-12"
              }`}
            >
              <div className="container px-0">
                <div className="row gx-0">
                  <div className="col-10">
                    <div className="d-flex py-5">
                      <div className="w-80p h-80p rounded-circle overflow-hidden">
                        <AvatarUser
                          width={80}
                          isLoggedin={true}
                          isLoading={isLoadingSiteData}
                        ></AvatarUser>
                      </div>

                      <div className="flex-grow-1 mt-1 ms-4">
                        <div className="mb-0">
                          <span className="text-muted font-size-14">
                            Selamat Datang,
                          </span>
                        </div>
                        <span className="d-block mb-2">
                          <h4 className="fw-bolder mb-3">{namaUserApp}</h4>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="col-2">
                    <Link
                      href={{
                        pathname: "/auth/user-profile",
                        query: {
                          menu: "profile",
                          action: "edit",
                          rand: moment().unix(),
                        },
                      }}
                    >
                      <a className="btn btn-outline-blue rounded-pill btn-xs mt-8 float-end px-3 font-size-15">
                        <i className="bi bi-pencil me-1"></i>
                        <span className="d-none d-lg-inline-block">Edit</span>
                      </a>
                    </Link>
                  </div>

                  {/* <div className="col-1">
                    <NotificationButton
                        ref={notifProfile}
                        shallowRouteTab={shallowRouteTab}
                        unreadNotif={unreadNotif}
                    ></NotificationButton>
                  </div>*/}
                </div>

                <div className="mt-5 py-3 border-top z-index-2">
                  <div className="col-12">
                    <div
                      className="bg-white rounded p-2"
                      style={{ overflowX: "auto" }}
                    ></div>
                  </div>
                </div>
              </div>
              <div className="tab-content" id="nav-tabContent">
                <div
                  className="tab-pane fade show active"
                  id="nav-dashboard"
                  role="tabpanel"
                  aria-labelledby="nav-dashboard"
                >
                  {
                    <TabDashboard
                      userProfile={profileDetail}
                      timeDiff={timeDiff}
                      displayPadiUMKM={displayPadiUMKM}
                      successLoadDiploy={successLoadDiploy}
                      urlDiploy={urlDiploy}
                      loadingBeasiswa={loadingBeasiswa}
                      urlBeasiswa={urlBeasiswa}
                    ></TabDashboard>
                  }
                </div>
                <div
                  className="tab-pane fade show"
                  id="nav-profile"
                  role="tabpanel"
                  aria-labelledby="nav-profile"
                >
                  {
                    <TabProfile
                      setMainLoading={showLoadingModal}
                      userProfile={profileDetail}
                      refreshProfile={getUserProfil}
                    ></TabProfile>
                  }
                </div>
                <div
                  className="tab-pane fade show"
                  id="nav-setting"
                  role="tabpanel"
                  aria-labelledby="nav-setting"
                >
                  {<TabSetting user={profileDetail} show={true}></TabSetting>}
                </div>
                <div
                  className="tab-pane fade show"
                  id="nav-pelatihan"
                  role="tabpanel"
                  aria-labelledby="nav-pelatihan"
                >
                  {
                    <TabPelatihan
                      userProfile={profileDetail}
                      timeDiff={timeDiff}
                    ></TabPelatihan>
                  }
                </div>
                <div
                  className="tab-pane fade show"
                  id="nav-tes-substansi"
                  role="tabpanel"
                  aria-labelledby="nav-tes-substansi"
                >
                  <TabSubstansi></TabSubstansi>
                </div>
                <div
                  className="tab-pane fade show"
                  id="nav-survey"
                  role="tabpanel"
                  aria-labelledby="nav-survey"
                >
                  <TabSurvey></TabSurvey>
                </div>
                <div
                  className="tab-pane fade show"
                  id="nav-trivia"
                  role="tabpanel"
                  aria-labelledby="nav-trivia"
                >
                  <TabTrivia></TabTrivia>
                </div>
                <div
                  className="tab-pane fade show"
                  id="nav-bookmark"
                  role="tabpanel"
                  aria-labelledby="nav-bookmark"
                >
                  {<TabBookmark timeDiff={timeDiff}></TabBookmark>}
                </div>
                <div
                  className="tab-pane fade show"
                  id="nav-notifikasi"
                  role="tabpanel"
                  aria-labelledby="nav-notifikasi"
                >
                  {
                    <TabNotifikasi
                      shallowRouteTab={shallowRouteTab}
                    ></TabNotifikasi>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
