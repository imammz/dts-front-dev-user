import React from "react";
import Link from "next/link";

export default function UserProfile() {
  return (
    <div className="bg-gray-100">
      <header className="navbar d-sm-block d-lg-block d-xl-none navbar-expand-xl bg-blue py-0">
        <div className="container">
          {/* Toggler */}
          <button
            className="navbar-toggler ms-0 shadow-none bg-dark-blue text-white icon-xs p-0 outline-0 h-40p w-40p d-flex d-xl-none place-flex-center"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarCollapse"
            aria-controls="navbarCollapse"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            {/* Icon */}
            <i className="fa fa-bars" />
          </button>
          {/* Brand */}
          <Link href="/">
            <a className="navbar-brand ms-3 me-0">
              <img
                src="/assets/img/dts-mono.png"
                className="navbar-brand-img"
                alt="..."
              />
            </a>
          </Link>
          {/* Collapse */}
          <div
            className="collapse navbar-collapse z-index-lg"
            id="navbarCollapse"
          >
            {/* Toggler */}
            <button
              className="navbar-toggler outline-0 text-primary"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarCollapse"
              aria-controls="navbarCollapse"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              {/* Icon */}
              <svg
                width={16}
                height={17}
                viewBox="0 0 16 17"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M0.142135 2.00015L1.55635 0.585938L15.6985 14.7281L14.2843 16.1423L0.142135 2.00015Z"
                  fill="currentColor"
                />
                <path
                  d="M14.1421 1.0001L15.5563 2.41431L1.41421 16.5564L0 15.1422L14.1421 1.0001Z"
                  fill="currentColor"
                />
              </svg>
            </button>
            <ul className="navbar-nav ms-auto">
              {/* Navigation */}
              <li className="nav-item">
                <Link href="/">
                  <a className="nav-link px-xl-4">HOME</a>
                </Link>
              </li>
              <li className="nav-item dropdown dropdown-full-width">
                <a
                  className="nav-link dropdown-toggle px-xl-4"
                  id="navbarDocumentation"
                  data-bs-toggle="dropdown"
                  href="#"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  PELATIHAN
                </a>
                <div
                  className="dropdown-menu border-xl shadow-none dropdown-full pt-xl-7 px-xl-8"
                  aria-labelledby="navbarLandings"
                >
                  <div className="row row-cols-1 row-cols-md-2 row-cols-lg-2 row-cols-xl-2">
                    <div className="col-md mb-md-6 mb-4 px-2">
                      {/* Card */}
                      <a
                        href="detail-akademi.html"
                        className="card icon-category icon-category-sm"
                      >
                        {/* Image */}
                        <div className="row align-items-center mx-n3">
                          <div className="col-auto px-3">
                            <div className="icon-h-p secondary">
                              <img
                                src="/assets/img/akademi/fga.png"
                                className="h-60p"
                              />
                            </div>
                          </div>
                          <div className="col px-3">
                            {/* Body */}
                            <div className="card-body p-0">
                              <h6 className="text-primary mb-n1">
                                Lulusan Baru
                              </h6>
                              <p className="font-size-sm text-gray-700 mb-0">
                                Fresh Graduate Academy
                              </p>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>
                    <div className="col-md mb-md-6 mb-4 px-2">
                      {/* Card */}
                      <a
                        href="detail-akademi.html"
                        className="card icon-category icon-category-sm"
                      >
                        {/* Image */}
                        <div className="row align-items-center mx-n3">
                          <div className="col-auto px-3">
                            <div className="icon-h-p secondary">
                              <img
                                src="/assets/img/akademi/vsga.png"
                                className="h-60p"
                              />
                            </div>
                          </div>
                          <div className="col px-3">
                            {/* Body */}
                            <div className="card-body p-0">
                              <h6 className="text-primary mb-n1">
                                Lulusan Vokasi
                              </h6>
                              <p className="font-size-sm text-gray-700 mb-0">
                                Vocational School Graduate Academy
                              </p>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>
                    <div className="col-md mb-md-6 mb-4 px-2">
                      {/* Card */}
                      <a
                        href="detail-akademi.html"
                        className="card icon-category icon-category-sm"
                      >
                        {/* Image */}
                        <div className="row align-items-center mx-n3">
                          <div className="col-auto px-3">
                            <div className="icon-h-p secondary">
                              <img
                                src="/assets/img/akademi/proa.png"
                                className="h-60p"
                              />
                            </div>
                          </div>
                          <div className="col px-3">
                            {/* Body */}
                            <div className="card-body p-0">
                              <h6 className="text-primary mb-n1">
                                Profesional
                              </h6>
                              <p className="font-size-sm text-gray-700 mb-0">
                                Profesional Academy
                              </p>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>
                    <div className="col-md mb-md-6 mb-4 px-2">
                      {/* Card */}
                      <a
                        href="detail-akademi.html"
                        className="card icon-category icon-category-sm"
                      >
                        {/* Image */}
                        <div className="row align-items-center mx-n3">
                          <div className="col-auto px-3">
                            <div className="icon-h-p secondary">
                              <img
                                src="/assets/img/akademi/gta.png"
                                className="h-60p"
                              />
                            </div>
                          </div>
                          <div className="col px-3">
                            {/* Body */}
                            <div className="card-body p-0">
                              <h6 className="text-primary mb-n1">
                                Aparatur Sipil Negara
                              </h6>
                              <p className="font-size-sm text-gray-700 mb-0">
                                Government Transformation Academy
                              </p>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>
                    <div className="col-md mb-md-6 mb-4 px-2">
                      {/* Card */}
                      <a
                        href="detail-akademi.html"
                        className="card icon-category icon-category-sm"
                      >
                        {/* Image */}
                        <div className="row align-items-center mx-n3">
                          <div className="col-auto px-3">
                            <div className="icon-h-p secondary">
                              <img
                                src="/assets/img/akademi/dea.png"
                                className="h-60p"
                              />
                            </div>
                          </div>
                          <div className="col px-3">
                            {/* Body */}
                            <div className="card-body p-0">
                              <h6 className="text-primary mb-n1">Wirausaha</h6>
                              <p className="font-size-sm text-gray-700 mb-0">
                                Digital Enterpreneurship Academy
                              </p>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>
                    <div className="col-md mb-md-6 mb-4 px-2">
                      {/* Card */}
                      <a
                        href="detail-akademi.html"
                        className="card icon-category icon-category-sm"
                      >
                        {/* Image */}
                        <div className="row align-items-center mx-n3">
                          <div className="col-auto px-3">
                            <div className="icon-h-p secondary">
                              <img
                                src="/assets/img/akademi/tsa.png"
                                className="h-60p"
                              />
                            </div>
                          </div>
                          <div className="col px-3">
                            {/* Body */}
                            <div className="card-body p-0">
                              <h6 className="text-primary mb-n1">Mahasiswa</h6>
                              <p className="font-size-sm text-gray-700 mb-0">
                                Talent Scouting Academy
                              </p>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>
                    <div className="col-md mb-md-6 mb-4 px-2">
                      {/* Card */}
                      <a
                        href="detail-akademi.html"
                        className="card icon-category icon-category-sm"
                      >
                        {/* Image */}
                        <div className="row align-items-center mx-n3">
                          <div className="col-auto px-3">
                            <div className="icon-h-p secondary">
                              <img
                                src="/assets/img/akademi/ta.png"
                                className="h-60p"
                              />
                            </div>
                          </div>
                          <div className="col px-3">
                            {/* Body */}
                            <div className="card-body p-0">
                              <h6 className="text-primary mb-n1">
                                Pelatihan Tematik
                              </h6>
                              <p className="font-size-sm text-gray-700 mb-0">
                                Thematic Academy
                              </p>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>
                    <div className="col-md mb-md-6 mb-4 px-2">
                      {/* Card */}
                      <a
                        href="detail-akademi.html"
                        className="card icon-category icon-category-sm"
                      >
                        {/* Image */}
                        <div className="row align-items-center mx-n3">
                          <div className="col-auto px-3">
                            <div className="icon-h-p secondary">
                              <img
                                src="/assets/img/akademi/dla.png"
                                className="h-60p"
                              />
                            </div>
                          </div>
                          <div className="col px-3">
                            {/* Body */}
                            <div className="card-body p-0">
                              <h6 className="text-primary mb-n1">Pimpinan</h6>
                              <p className="font-size-sm text-gray-700 mb-0">
                                Digital Leadership Academy
                              </p>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>
                    <div className="col-md mb-md-6 mb-4 px-2">
                      {/* Card */}
                      <a
                        href="all-pelatihan.html"
                        className="card icon-category icon-category-sm"
                      >
                        {/* Image */}
                        <div className="row align-items-center mx-n3">
                          <div className="col-auto px-3">
                            <div className="icon-h-p secondary">
                              <img
                                src="/assets/img/akademi/all.png"
                                className="h-60p"
                              />
                            </div>
                          </div>
                          <div className="col px-3">
                            {/* Body */}
                            <div className="card-body p-0">
                              <h6 className="text-primary mb-n1">
                                Semua Pelatihan
                              </h6>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </li>
              <li className="nav-item">
                <a href="jadwal-pelatihan.html" className="nav-link px-xl-4">
                  JADWAL
                </a>
              </li>
              <li className="nav-item dropdown">
                <Link href="/rilis-media">
                  <a
                    className="nav-link dropdown-toggle"
                    id="navbarShop"
                    data-bs-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    RILIS MEDIA
                  </a>
                </Link>
                <ul
                  className="dropdown-menu border-xl shadow-none"
                  aria-labelledby="navbarShop"
                >
                  <li className="dropdown-item">
                    <Link href="/informasi">
                      <a className="dropdown-link">Informasi</a>
                    </Link>
                  </li>
                  <li className="dropdown-item">
                    <Link href="/artikel">
                      <a className="dropdown-link">Artikel</a>
                    </Link>
                  </li>
                  <li className="dropdown-item">
                    <Link href="/gallery">
                      <a className="dropdown-link">Galeri</a>
                    </Link>
                  </li>
                  <li className="dropdown-item">
                    <a href="video.html" className="dropdown-link">
                      Video
                    </a>
                  </li>
                </ul>
              </li>
              <li className="nav-item">
                <Link href="/event">
                  <a className="nav-link px-xl-4">EVENT</a>
                </Link>
              </li>
              <li className="nav-item dropdown">
                <a
                  href="#"
                  className="nav-link dropdown-toggle"
                  id="navbarShop"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  TENTANG KAMI
                </a>
                <ul
                  className="dropdown-menu border-xl shadow-none"
                  aria-labelledby="navbarShop"
                >
                  <li className="dropdown-item">
                    <a href="program.html" className="dropdown-link">
                      Program
                    </a>
                  </li>
                  <li className="dropdown-item">
                    <a href="faq.html" className="dropdown-link">
                      FAQ
                    </a>
                  </li>
                  <li className="dropdown-item">
                    <Link href="helpdesk">
                      <a className="text-reset">Helpdesk</a>
                    </Link>
                  </li>
                  <li className="dropdown-item">
                    <Link href="/mitra/">
                      <a className="dropdown-link">Mitra Pelatihan</a>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item">
                <Link href="/kontak">
                  <a className="nav-link px-xl-4">KONTAK</a>
                </Link>
              </li>
            </ul>
          </div>
          {/* Search, Account */}
          <ul className="navbar-nav flex-row ms-auto ms-xl-0 me-n2 me-md-n4 align-items-center">
            <li className="nav-item border-0 px-0 d-sm-block d-lg-block d-xl-none">
              <a
                className="nav-link d-flex px-3 px-md-4 search-mobile text-white-all icon-xs"
                data-bs-toggle="collapse"
                href="#collapseSearchMobile"
                role="button"
                aria-expanded="false"
                aria-controls="collapseSearchMobile"
              >
                {/* Icon */}
                <svg
                  width={20}
                  height={20}
                  viewBox="0 0 20 20"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M8.80758 0C3.95121 0 0 3.95121 0 8.80758C0 13.6642 3.95121 17.6152 8.80758 17.6152C13.6642 17.6152 17.6152 13.6642 17.6152 8.80758C17.6152 3.95121 13.6642 0 8.80758 0ZM8.80758 15.9892C4.8477 15.9892 1.62602 12.7675 1.62602 8.80762C1.62602 4.84773 4.8477 1.62602 8.80758 1.62602C12.7675 1.62602 15.9891 4.8477 15.9891 8.80758C15.9891 12.7675 12.7675 15.9892 8.80758 15.9892Z"
                    fill="currentColor"
                  />
                  <path
                    d="M19.762 18.6121L15.1007 13.9509C14.7831 13.6332 14.2687 13.6332 13.9511 13.9509C13.6335 14.2682 13.6335 14.7831 13.9511 15.1005L18.6124 19.7617C18.7712 19.9205 18.9791 19.9999 19.1872 19.9999C19.395 19.9999 19.6032 19.9205 19.762 19.7617C20.0796 19.4444 20.0796 18.9295 19.762 18.6121Z"
                    fill="currentColor"
                  />
                </svg>
                {/* Icon */}
                <svg
                  width={16}
                  height={17}
                  viewBox="0 0 16 17"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M0.142135 2.00015L1.55635 0.585938L15.6985 14.7281L14.2843 16.1423L0.142135 2.00015Z"
                    fill="currentColor"
                  />
                  <path
                    d="M14.1421 1.0001L15.5563 2.41431L1.41421 16.5564L0 15.1422L14.1421 1.0001Z"
                    fill="currentColor"
                  />
                </svg>
              </a>
              <div
                className="collapse position-absolute right-0 left-0 mx-4"
                id="collapseSearchMobile"
              >
                <div className="card card-body p-4 mt-6 mt-xl-4 shadow-dark">
                  {/* Search */}
                  <form className="w-100">
                    <div className="input-group border rounded">
                      <div className="input-group-prepend">
                        <button
                          className="btn btn-sm text-secondary icon-xs d-flex align-items-center"
                          type="submit"
                        >
                          {/* Icon */}
                          <svg
                            width={20}
                            height={20}
                            viewBox="0 0 20 20"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M8.80758 0C3.95121 0 0 3.95121 0 8.80758C0 13.6642 3.95121 17.6152 8.80758 17.6152C13.6642 17.6152 17.6152 13.6642 17.6152 8.80758C17.6152 3.95121 13.6642 0 8.80758 0ZM8.80758 15.9892C4.8477 15.9892 1.62602 12.7675 1.62602 8.80762C1.62602 4.84773 4.8477 1.62602 8.80758 1.62602C12.7675 1.62602 15.9891 4.8477 15.9891 8.80758C15.9891 12.7675 12.7675 15.9892 8.80758 15.9892Z"
                              fill="currentColor"
                            />
                            <path
                              d="M19.762 18.6121L15.1007 13.9509C14.7831 13.6332 14.2687 13.6332 13.9511 13.9509C13.6335 14.2682 13.6335 14.7831 13.9511 15.1005L18.6124 19.7617C18.7712 19.9205 18.9791 19.9999 19.1872 19.9999C19.395 19.9999 19.6032 19.9205 19.762 19.7617C20.0796 19.4444 20.0796 18.9295 19.762 18.6121Z"
                              fill="currentColor"
                            />
                          </svg>
                        </button>
                      </div>
                      <input
                        className="form-control form-control-sm border-0 ps-0"
                        type="search"
                        placeholder="Pencarian Akademi, Pelatihan, Tema..."
                        aria-label="Search"
                      />
                    </div>
                  </form>
                </div>
              </div>
            </li>
            <li className="nav-item border-0 px-0">
              {/* Button trigger account modal */}
              <a
                href="user-profile.html"
                className="nav-link d-flex px-3 px-md-4 text-white-all icon-xs"
                data-bs-toggle="modal"
                data-bs-target="#accountModal"
              >
                {/* Icon */}
                <img
                  src="/assets/img/avatars/avatar-1.jpg"
                  alt="..."
                  className="avatar-img border rounded-circle img-fluid"
                  style={{ width: 32 }}
                />
              </a>
            </li>
          </ul>
        </div>
      </header>

      <div
        className="modal modal-sidebar col-5 left fade-left fade"
        id="accountModal"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <div className="row align-items-end">
                <div className="col-auto d-md-block d-lg-block d-xl-block px-3">
                  <div className="avatar d-inline">
                    <img
                      src="/assets/img/avatars/avatar-1.jpg"
                      alt="..."
                      className="avatar-img rounded-circle avatar-xl img-fluid"
                    />
                  </div>
                </div>
                <div className="col">
                  <div className="card-body p-0">
                    <p className="mb-0 line-clamp-1">Selamat Datang,</p>
                    <h6 className="mb-0 line-clamp-1">Willy Wize A.Z</h6>
                  </div>
                </div>
              </div>
              <button
                type="button"
                className="close text-primary"
                data-bs-dismiss="modal"
                aria-label="Close"
              >
                {/* Icon */}
                <svg
                  width={16}
                  height={17}
                  viewBox="0 0 16 17"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M0.142135 2.00015L1.55635 0.585938L15.6985 14.7281L14.2843 16.1423L0.142135 2.00015Z"
                    fill="currentColor"
                  />
                  <path
                    d="M14.1421 1.0001L15.5563 2.41431L1.41421 16.5564L0 15.1422L14.1421 1.0001Z"
                    fill="currentColor"
                  />
                </svg>
              </button>
            </div>
            <div className="modal-body py-5 border-top">
              <div>
                <h5>User Menu</h5>
                <ul className="list-unstyled text-gray-800 font-size-sm-alone mb-6 mb-md-8 mb-lg-">
                  <li className="mb-4">
                    <Link href="/auth/user-profile">
                      <a className="list-link">
                        <i className="fa fa-user-alt me-3" />
                        Profile
                      </a>
                    </Link>
                  </li>
                  <li className="mb-4">
                    <a href="user-profile.html" className="list-link">
                      <i className="fa fa-edit me-3" />
                      Edit Profile
                    </a>
                  </li>
                  <li className="mb-4">
                    <a href="user-profile.html" className="list-link">
                      <i className="fa fa-heart me-3" />
                      Bookmark
                    </a>
                  </li>
                  <li className="mb-4">
                    <a href="user-profile.html" className="list-link">
                      <i className="fa fa-bell me-3" />
                      Notifikasi{" "}
                      <span className="badge bg-red badge-pill ms-2 text-white">
                        4
                      </span>
                    </a>
                  </li>
                  <li className="mb-4">
                    <a href="user-profile.html" className="list-link">
                      <i className="fa fa-cog me-3" />
                      Setting
                    </a>
                  </li>
                  <li className="mb-4">
                    <Link href="/login">
                      <a className="list-link">
                        <i className="fa fa-sign-out-alt me-3" />
                        Logout
                      </a>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

      <header
        className="pt-8 mb-8 bg-white"
        style={{ backgroundImage: "none" }}
      >
        <div className="container">
          <div className="row px-2 align-items-end pb-6">
            <div className="col-auto d-md-block d-lg-block d-xl-block px-3">
              <div className="avatar d-inline">
                <img
                  src="/assets/img/avatars/avatar-1.jpg"
                  alt="..."
                  className="avatar-img rounded-circle avatar-xxl img-fluid"
                />
              </div>
            </div>
            <div className="col d-inline-block">
              <div className="card-body mt-n3 p-0">
                <p className="mb-0 line-clamp-1">Selamat Datang,</p>
                <h6 className="mb-0 line-clamp-1">
                  <a href="user-profile.html">Willy Wize A.Z</a>
                </h6>
              </div>
            </div>
            <div className="col-auto">
              <a
                href="#"
                className="btn btn-outline-secondary btn-sm mt-6 font-size-base"
              >
                <i className="fa fa-bell" />
                <span className="notif-dot bg-red mt-n1 me-2">2</span>
              </a>
            </div>
          </div>
          <div className="py-2 border-top z-index-2">
            <div className="col-12">
              <div className="bg-white rounded p-2">
                <ul
                  id="pills-tab"
                  className="nav nav-pills course-tab-v2 h6 mb-0 flex-nowrap overflow-auto"
                  role="tablist"
                >
                  <li className="nav-item">
                    <a
                      className="nav-link text-uppercase active"
                      data-bs-toggle="tab"
                      data-bs-target="#nav-dashboard"
                      role="tab"
                      data-offset={0}
                    >
                      Dashboard
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link text-uppercase"
                      data-bs-toggle="tab"
                      data-bs-target="#nav-profile"
                      role="tab"
                      data-offset={0}
                    >
                      Profile
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link text-uppercase"
                      data-bs-toggle="tab"
                      data-bs-target="#nav-pelatihan"
                      role="tab"
                      data-offset={0}
                    >
                      Pelatihan
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link text-uppercase"
                      data-bs-toggle="tab"
                      data-bs-target="#nav-tes-substansi"
                      role="tab"
                      data-offset={0}
                    >
                      Tes Substansi
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link text-uppercase"
                      data-bs-toggle="tab"
                      data-bs-target="#nav-survey"
                      role="tab"
                      data-offset={0}
                    >
                      Survey
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link text-uppercase"
                      data-bs-toggle="tab"
                      data-bs-target="#nav-trivia"
                      role="tab"
                      data-offset={0}
                    >
                      Trivia
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        {/* Img */}
      </header>

      <section>
        <div className="container">
          <div className="row mb-5 mb-md-7">
            <div className="col-lg-8">
              <div className="tab-content" id="nav-tabContent">
                <div
                  className="tab-pane fade show active"
                  id="nav-dashboard"
                  role="tabpanel"
                  aria-labelledby="nav-dashboard"
                >
                  <h3>Statistik</h3>
                  <div className="row gx-2">
                    <div className="col-4 mb-md-6 mb-4">
                      <div className="card card-border-hover bg-info icon-category icon-category-sm p-5">
                        {/* Image */}
                        <div className="row align-items-center mx-n3">
                          <div className="col-auto px-3">
                            <div className="icon-h-p secondary">
                              <i className="fa fa-book text-blue" />
                            </div>
                          </div>
                          <div className="col px-3">
                            {/* Body */}
                            <div className="card-body p-0">
                              <h6 className="mb-0 line-clamp-1">Pelatihan</h6>
                              <h6 className="mb-0 line-clamp-1">
                                10 Pelatihan
                              </h6>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-4 mb-md-6 mb-4">
                      <div className="card card-border-hover bg-success icon-category icon-category-sm p-5">
                        {/* Image */}
                        <div className="row align-items-center mx-n3">
                          <div className="col-auto px-3">
                            <div className="icon-h-p secondary">
                              <i className="far fa-id-card text-green" />
                            </div>
                          </div>
                          <div className="col px-3">
                            {/* Body */}
                            <div className="card-body p-0">
                              <h6 className="mb-0 line-clamp-1">Lulus</h6>
                              <h6 className="mb-0 line-clamp-1">7 Pelatihan</h6>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-4 mb-md-6 mb-4">
                      <div className="card card-border-hover bg-danger icon-category icon-category-sm p-5">
                        {/* Image */}
                        <div className="row align-items-center mx-n3">
                          <div className="col-auto px-3">
                            <div className="icon-h-p">
                              <i className="far fa-times-circle text-red" />
                            </div>
                          </div>
                          <div className="col px-3">
                            {/* Body */}
                            <div className="card-body p-0">
                              <h6 className="mb-0 line-clamp-1">Tidak Lulus</h6>
                              <h6 className="mb-0 line-clamp-1">3 Pelatihan</h6>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <h3 className="mt-5">Aktivitas</h3>
                  <div className="bg-white border px-5 mb-5">
                    <div className="card border-bottom pt-3 mb-3">
                      <div className="row gx-0">
                        {/* Image */}
                        <a
                          href="detail-pelatihan.html"
                          className="col-auto p-3 d-block"
                          style={{ maxWidth: 80 }}
                        >
                          <img
                            className="img-fluid shadow-light-lg"
                            src="/assets/img/microsoft.webp"
                            alt="Mitra"
                          />
                        </a>
                        {/* Body */}
                        <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                          <div className="card-body p-2">
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Fresh Graduate Academy
                              </span>
                            </div>
                            <a
                              href="detail-pelatihan.html"
                              className="d-block mb-2"
                            >
                              <h6 className="line-clamp-2 mb-3">
                                Database Design and Programming With SQL{" "}
                              </h6>
                            </a>
                            <ul className="nav mx-n3 d-block d-md-flex">
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-calendar" />
                                  </div>
                                  <div className="font-size-sm">
                                    1 - 10 Maret 2022
                                  </div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-map-marker-alt" />
                                  </div>
                                  <div className="font-size-sm">
                                    DKI Jakarta
                                  </div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-microphone-alt" />
                                  </div>
                                  <div className="font-size-sm">
                                    BPSDMP Jakarta
                                  </div>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row gx-2 mb-3">
                      <div className="col-md mb-md-0">
                        <div className="badge badge-sm bg-yellow mt-3 mb-3 badge-pill">
                          <span className="text-white font-size-sm fw-normal">
                            Seleksi Administrasi
                          </span>
                        </div>
                      </div>
                      <div className="col-auto align-self-end">
                        <a
                          href="#"
                          className="btn btn-outline-blue btn-xs px-3 mb-3 font-size-15"
                        >
                          <i className="fa fa-file-download" />
                          <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                            Download Bukti Pendaftaran
                          </span>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div className="bg-white border px-5 mb-5">
                    <div className="card border-bottom pt-3 mb-3">
                      <div className="row gx-0">
                        {/* Image */}
                        <a
                          href="detail-pelatihan.html"
                          className="col-auto p-3 d-block"
                          style={{ maxWidth: 80 }}
                        >
                          <img
                            className="img-fluid shadow-light-lg"
                            src="/assets/img/exam.png"
                            alt="Mitra"
                          />
                        </a>
                        {/* Body */}
                        <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                          <div className="card-body p-2">
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Tes Substansi
                              </span>
                            </div>
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Fresh Graduate Academy
                              </span>
                            </div>
                            <a
                              href="detail-pelatihan.html"
                              className="d-block mb-2"
                            >
                              <h6 className="line-clamp-2 mb-3">
                                Database Design and Programming With SQL{" "}
                              </h6>
                            </a>
                            <ul className="nav mx-n3 d-block d-md-flex">
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-edit" />
                                  </div>
                                  <div className="font-size-sm">20 Soal</div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-clock" />
                                  </div>
                                  <div className="font-size-sm">60 Menit</div>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row gx-2 mb-3">
                      <div className="col-md mb-md-0">
                        <div className="d-flex align-items-center text-alizarin pb-3">
                          {/* Icon */}
                          <i className="fa fa-bell" />
                          <span className="ms-2">Deadline : 20 April 2022</span>
                        </div>
                      </div>
                      <div className="col-auto align-self-end pb-3">
                        <Link href="/user-guideline-subvit">
                          <a className="btn btn-blue btn-xs px-3 font-size-15">
                            <i className="fa fa-play" />
                            <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                              Kerjakan Tes Substansi
                            </span>
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                  <div className="bg-white border px-5 mb-5">
                    <div className="card border-bottom pt-3 mb-3">
                      <div className="row gx-0">
                        {/* Image */}
                        <a
                          href="detail-pelatihan.html"
                          className="col-auto p-3 d-block"
                          style={{ maxWidth: 80 }}
                        >
                          <img
                            className="img-fluid shadow-light-lg"
                            src="/assets/img/survey.png"
                            alt="Mitra"
                          />
                        </a>
                        {/* Body */}
                        <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                          <div className="card-body p-2">
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Survey
                              </span>
                            </div>
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Fresh Graduate Academy
                              </span>
                            </div>
                            <a
                              href="detail-pelatihan.html"
                              className="d-block mb-2"
                            >
                              <h6 className="line-clamp-2 mb-3">
                                Survey Evaluasi Pelaksanaan Pelatihan
                              </h6>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row gx-2 mb-3">
                      <div className="col-md mb-md-0">
                        <div className="d-flex align-items-center text-alizarin pb-3">
                          {/* Icon */}
                          <i className="fa fa-bell" />
                          <span className="ms-2">Deadline : 20 April 2022</span>
                        </div>
                      </div>
                      <div className="col-auto align-self-end pb-3">
                        <Link href="/user-guideline-survey">
                          <a
                            href="guideline-subvit.html"
                            className="btn btn-blue btn-xs px-3 font-size-15"
                          >
                            <i className="fa fa-play" />
                            <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                              Kerjakan Survey
                            </span>
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                  <h3 className="mt-8">Informasi</h3>
                  <div className="bg-white border px-5 pt-6 pb-6 mb-5">
                    <div className="row mb-3">
                      <div className="col-md mb-md-0">
                        <h4 className="mb-0">Lowongan Terbaru</h4>
                      </div>
                      <div className="col-md-auto">
                        <a
                          href="#"
                          className="d-flex align-items-center fw-medium"
                        >
                          Lihat Semua
                          <div className="ms-2 d-flex">
                            {/* Icon */}
                            <svg
                              width={10}
                              height={10}
                              viewBox="0 0 10 10"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                d="M7.7779 4.6098L3.32777 0.159755C3.22485 0.0567475 3.08745 0 2.94095 0C2.79445 0 2.65705 0.0567475 2.55412 0.159755L2.2264 0.487394C2.01315 0.700889 2.01315 1.04788 2.2264 1.26105L5.96328 4.99793L2.22225 8.73895C2.11933 8.84196 2.0625 8.97928 2.0625 9.1257C2.0625 9.27228 2.11933 9.4096 2.22225 9.51269L2.54998 9.84025C2.65298 9.94325 2.7903 10 2.9368 10C3.0833 10 3.2207 9.94325 3.32363 9.84025L7.7779 5.38614C7.88107 5.2828 7.93774 5.14484 7.93741 4.99817C7.93774 4.85094 7.88107 4.71305 7.7779 4.6098Z"
                                fill="currentColor"
                              />
                            </svg>
                          </div>
                        </a>
                      </div>
                    </div>
                    <div className="card border-bottom p-3 mb-1">
                      <div className="row gx-0">
                        {/* Image */}
                        <a
                          href="detail-pelatihan.html"
                          className="col-auto p-3 d-block"
                          style={{ maxWidth: 80 }}
                        >
                          <img
                            className="img-fluid shadow-light-lg"
                            src="/assets/img/metrodata.png"
                            alt="Mitra"
                          />
                        </a>
                        {/* Body */}
                        <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                          <div className="card-body p-2">
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Metrodata
                              </span>
                            </div>
                            <a href="#" className="d-block mb-2">
                              <h6 className="line-clamp-2 mb-3">
                                Application Developer
                              </h6>
                            </a>
                            <ul className="nav mx-n3 d-block d-md-flex">
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-building" />
                                  </div>
                                  <div className="font-size-sm">Technology</div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-map-marker-alt" />
                                  </div>
                                  <div className="font-size-sm">
                                    DKI Jakarta
                                  </div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-briefcase" />
                                  </div>
                                  <div className="font-size-sm">
                                    Karyawan Kontrak
                                  </div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="badge badge-sm bg-green badge-pill">
                                    <span className="text-white font-size-sm fw-normal">
                                      Buka
                                    </span>
                                  </div>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="card border-bottom p-3 mb-1">
                      <div className="row gx-0">
                        {/* Image */}
                        <a
                          href="detail-pelatihan.html"
                          className="col-auto p-3 d-block"
                          style={{ maxWidth: 80 }}
                        >
                          <img
                            className="img-fluid shadow-light-lg"
                            src="/assets/img/metrodata.png"
                            alt="Mitra"
                          />
                        </a>
                        {/* Body */}
                        <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                          <div className="card-body p-2">
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Metrodata
                              </span>
                            </div>
                            <a href="#" className="d-block mb-2">
                              <h6 className="line-clamp-2 mb-3">
                                Application Developer
                              </h6>
                            </a>
                            <ul className="nav mx-n3 d-block d-md-flex">
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-building" />
                                  </div>
                                  <div className="font-size-sm">Technology</div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-map-marker-alt" />
                                  </div>
                                  <div className="font-size-sm">
                                    DKI Jakarta
                                  </div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-briefcase" />
                                  </div>
                                  <div className="font-size-sm">
                                    Karyawan Kontrak
                                  </div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="badge badge-sm bg-green badge-pill">
                                    <span className="text-white font-size-sm fw-normal">
                                      Buka
                                    </span>
                                  </div>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="card border-bottom p-3 mb-1">
                      <div className="row gx-0">
                        {/* Image */}
                        <a
                          href="detail-pelatihan.html"
                          className="col-auto p-3 d-block"
                          style={{ maxWidth: 80 }}
                        >
                          <img
                            className="img-fluid shadow-light-lg"
                            src="/assets/img/metrodata.png"
                            alt="Mitra"
                          />
                        </a>
                        {/* Body */}
                        <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                          <div className="card-body p-2">
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Metrodata
                              </span>
                            </div>
                            <a href="#" className="d-block mb-2">
                              <h6 className="line-clamp-2 mb-3">
                                Application Developer
                              </h6>
                            </a>
                            <ul className="nav mx-n3 d-block d-md-flex">
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-building" />
                                  </div>
                                  <div className="font-size-sm">Technology</div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-map-marker-alt" />
                                  </div>
                                  <div className="font-size-sm">
                                    DKI Jakarta
                                  </div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-briefcase" />
                                  </div>
                                  <div className="font-size-sm">
                                    Karyawan Kontrak
                                  </div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="badge badge-sm bg-green badge-pill">
                                    <span className="text-white font-size-sm fw-normal">
                                      Buka
                                    </span>
                                  </div>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="card border-bottom p-3 mb-1">
                      <div className="row gx-0">
                        {/* Image */}
                        <a
                          href="detail-pelatihan.html"
                          className="col-auto p-3 d-block"
                          style={{ maxWidth: 80 }}
                        >
                          <img
                            className="img-fluid shadow-light-lg"
                            src="/assets/img/metrodata.png"
                            alt="Mitra"
                          />
                        </a>
                        {/* Body */}
                        <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                          <div className="card-body p-2">
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Metrodata
                              </span>
                            </div>
                            <a href="#" className="d-block mb-2">
                              <h6 className="line-clamp-2 mb-3">
                                Application Developer
                              </h6>
                            </a>
                            <ul className="nav mx-n3 d-block d-md-flex">
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-building" />
                                  </div>
                                  <div className="font-size-sm">Technology</div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-map-marker-alt" />
                                  </div>
                                  <div className="font-size-sm">
                                    DKI Jakarta
                                  </div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-briefcase" />
                                  </div>
                                  <div className="font-size-sm">
                                    Karyawan Kontrak
                                  </div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="badge badge-sm bg-green badge-pill">
                                    <span className="text-white font-size-sm fw-normal">
                                      Buka
                                    </span>
                                  </div>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="card p-3">
                      <div className="row gx-0">
                        {/* Image */}
                        <a
                          href="detail-pelatihan.html"
                          className="col-auto p-3 d-block"
                          style={{ maxWidth: 80 }}
                        >
                          <img
                            className="img-fluid shadow-light-lg"
                            src="/assets/img/metrodata.png"
                            alt="Mitra"
                          />
                        </a>
                        {/* Body */}
                        <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                          <div className="card-body p-2">
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Metrodata
                              </span>
                            </div>
                            <a href="#" className="d-block mb-2">
                              <h6 className="line-clamp-2 mb-3">
                                Application Developer
                              </h6>
                            </a>
                            <ul className="nav mx-n3 d-block d-md-flex">
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-building" />
                                  </div>
                                  <div className="font-size-sm">Technology</div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-map-marker-alt" />
                                  </div>
                                  <div className="font-size-sm">
                                    DKI Jakarta
                                  </div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-briefcase" />
                                  </div>
                                  <div className="font-size-sm">
                                    Karyawan Kontrak
                                  </div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="badge badge-sm bg-green badge-pill">
                                    <span className="text-white font-size-sm fw-normal">
                                      Buka
                                    </span>
                                  </div>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="bg-white border px-5 pt-6 pb-6 mb-5">
                    <div className="row mb-3">
                      <div className="col-md mb-md-0">
                        <h4 className="mb-0">Informasi Beasiswa</h4>
                      </div>
                      <div className="col-md-auto">
                        <a
                          href="#"
                          className="d-flex align-items-center fw-medium"
                        >
                          Lihat Semua
                          <div className="ms-2 d-flex">
                            {/* Icon */}
                            <svg
                              width={10}
                              height={10}
                              viewBox="0 0 10 10"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                d="M7.7779 4.6098L3.32777 0.159755C3.22485 0.0567475 3.08745 0 2.94095 0C2.79445 0 2.65705 0.0567475 2.55412 0.159755L2.2264 0.487394C2.01315 0.700889 2.01315 1.04788 2.2264 1.26105L5.96328 4.99793L2.22225 8.73895C2.11933 8.84196 2.0625 8.97928 2.0625 9.1257C2.0625 9.27228 2.11933 9.4096 2.22225 9.51269L2.54998 9.84025C2.65298 9.94325 2.7903 10 2.9368 10C3.0833 10 3.2207 9.94325 3.32363 9.84025L7.7779 5.38614C7.88107 5.2828 7.93774 5.14484 7.93741 4.99817C7.93774 4.85094 7.88107 4.71305 7.7779 4.6098Z"
                                fill="currentColor"
                              />
                            </svg>
                          </div>
                        </a>
                      </div>
                    </div>
                    <div className="card border-bottom p-3 mb-1">
                      <div className="row gx-0">
                        {/* Image */}
                        <a
                          href="detail-pelatihan.html"
                          className="col-auto p-3 d-block"
                          style={{ maxWidth: 80 }}
                        >
                          <img
                            className="img-fluid shadow-light-lg"
                            src="/assets/img/ugm.png"
                            alt="Mitra"
                          />
                        </a>
                        {/* Body */}
                        <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                          <div className="card-body p-2">
                            <a href="#" className="d-block mb-2">
                              <h6 className="line-clamp-2 mb-0">
                                Universitas Gadjah Mada
                              </h6>
                            </a>
                            <ul className="nav mx-n3 d-block d-md-flex">
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-map-marker-alt" />
                                  </div>
                                  <div className="font-size-sm">Yogyakarta</div>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="card border-bottom p-3 mb-1">
                      <div className="row gx-0">
                        {/* Image */}
                        <a
                          href="detail-pelatihan.html"
                          className="col-auto p-3 d-block"
                          style={{ maxWidth: 80 }}
                        >
                          <img
                            className="img-fluid shadow-light-lg"
                            src="/assets/img/ugm.png"
                            alt="Mitra"
                          />
                        </a>
                        {/* Body */}
                        <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                          <div className="card-body p-2">
                            <a href="#" className="d-block mb-2">
                              <h6 className="line-clamp-2 mb-0">
                                Universitas Gadjah Mada
                              </h6>
                            </a>
                            <ul className="nav mx-n3 d-block d-md-flex">
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-map-marker-alt" />
                                  </div>
                                  <div className="font-size-sm">Yogyakarta</div>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="card border-bottom p-3 mb-1">
                      <div className="row gx-0">
                        {/* Image */}
                        <a
                          href="detail-pelatihan.html"
                          className="col-auto p-3 d-block"
                          style={{ maxWidth: 80 }}
                        >
                          <img
                            className="img-fluid shadow-light-lg"
                            src="/assets/img/ugm.png"
                            alt="Mitra"
                          />
                        </a>
                        {/* Body */}
                        <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                          <div className="card-body p-2">
                            <a href="#" className="d-block mb-2">
                              <h6 className="line-clamp-2 mb-0">
                                Universitas Gadjah Mada
                              </h6>
                            </a>
                            <ul className="nav mx-n3 d-block d-md-flex">
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-map-marker-alt" />
                                  </div>
                                  <div className="font-size-sm">Yogyakarta</div>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="card border-bottom p-3 mb-1">
                      <div className="row gx-0">
                        {/* Image */}
                        <a
                          href="detail-pelatihan.html"
                          className="col-auto p-3 d-block"
                          style={{ maxWidth: 80 }}
                        >
                          <img
                            className="img-fluid shadow-light-lg"
                            src="/assets/img/ugm.png"
                            alt="Mitra"
                          />
                        </a>
                        {/* Body */}
                        <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                          <div className="card-body p-2">
                            <a href="#" className="d-block mb-2">
                              <h6 className="line-clamp-2 mb-0">
                                Universitas Gadjah Mada
                              </h6>
                            </a>
                            <ul className="nav mx-n3 d-block d-md-flex">
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-map-marker-alt" />
                                  </div>
                                  <div className="font-size-sm">Yogyakarta</div>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="card p-3">
                      <div className="row gx-0">
                        {/* Image */}
                        <a
                          href="detail-pelatihan.html"
                          className="col-auto p-3 d-block"
                          style={{ maxWidth: 80 }}
                        >
                          <img
                            className="img-fluid shadow-light-lg"
                            src="/assets/img/ugm.png"
                            alt="Mitra"
                          />
                        </a>
                        {/* Body */}
                        <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                          <div className="card-body p-2">
                            <a href="#" className="d-block mb-2">
                              <h6 className="line-clamp-2 mb-0">
                                Universitas Gadjah Mada
                              </h6>
                            </a>
                            <ul className="nav mx-n3 d-block d-md-flex">
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-map-marker-alt" />
                                  </div>
                                  <div className="font-size-sm">Yogyakarta</div>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  className="tab-pane fade show"
                  id="nav-profile"
                  role="tabpanel"
                  aria-labelledby="nav-profile"
                >
                  <div className="bg-white border px-5 mb-5">
                    <div className="card p-3 mb-1">
                      <div className="my-5">
                        <h3 className="mb-1">Profile</h3>
                        <div className="row my-5">
                          <div className="col-12 col-md-6">
                            <div className="form-group mb-3">
                              <div className="row align-items-end">
                                <div className="col-md mb-md-0">
                                  <label>Nama Lengkap Sesuai KTP</label>
                                  <p>Willy Wize Ananda Zen</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-12 col-md-6">
                            <div className="form-group mb-3">
                              <div className="row align-items-end">
                                <div className="col-md mb-md-0">
                                  <label>NIK</label>
                                  <p>123456789101112</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-12 col-md-6">
                            <div className="form-group mb-3">
                              <div className="row align-items-end">
                                <div className="col-md mb-md-0">
                                  <label>Email</label>
                                  <p>will004@kominfo.go.id</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-12 col-md-6">
                            <div className="form-group mb-3">
                              <div className="row align-items-end">
                                <div className="col-md mb-md-0">
                                  <label>No. Handphone</label>
                                  <p>0812345678900</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-12 col-md-6">
                            <div className="form-group mb-3">
                              <div className="row align-items-end">
                                <div className="col-md mb-md-0">
                                  <label>Tempat Lahir</label>
                                  <p>Jakarta</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-12 col-md-6">
                            <div className="form-group mb-3">
                              <div className="row align-items-end">
                                <div className="col-md mb-md-0">
                                  <label>Tgl. Lahir</label>
                                  <p>29 Juli 1989</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-12 col-md-6">
                            <div className="form-group mb-3">
                              <div className="row align-items-end">
                                <div className="col-md mb-md-0">
                                  <label>Jenis Kelamin</label>
                                  <p>Pria</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-12 col-md-6">
                            <div className="row align-items-end">
                              <div className="col-md mb-md-0">
                                <label>Pendidikan Terakhir</label>
                                <p>S2</p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="row mb-5">
                          <div className="form-row">
                            <h3 className="mb-1">Domisili</h3>
                          </div>
                          <div className="col-12">
                            <div className="form-group mb-3">
                              <div className="row align-items-end">
                                <div className="col-md mb-md-0">
                                  <label>Alamat Lengkap</label>
                                  <p>Jl. Medan Merdeka Barat No.9</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-12 col-md-6">
                            <div className="form-group mb-3">
                              <div className="row align-items-end">
                                <div className="col-md mb-md-0">
                                  <label>Provinsi</label>
                                  <p>DKI Jakarta</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-12 col-md-6">
                            <div className="form-group mb-3">
                              <div className="row align-items-end">
                                <div className="col-md mb-md-0">
                                  <label>Kota/Kab</label>
                                  <p>Jakarta Pusat</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-12 col-md-6">
                            <div className="form-group mb-3">
                              <div className="row align-items-end">
                                <div className="col-md mb-md-0">
                                  <label>Kecamatan</label>
                                  <p>Gambir</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-12 col-md-6">
                            <div className="form-group mb-3">
                              <div className="row align-items-end">
                                <div className="col-md mb-md-0">
                                  <label>Desa/Kelurahan</label>
                                  <p>Gambir</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="row mb-5">
                          <div className="form-row">
                            <h3 className="mb-1">Pekerjaan Saat ini</h3>
                          </div>
                          <div className="col-12 col-md-12">
                            <div className="form-group mb-3">
                              <div className="row align-items-end">
                                <div className="col-md mb-md-0">
                                  <label>Status</label>
                                  <p>Bekerja</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-12 col-md-6">
                            <div className="form-group mb-3">
                              <div className="row align-items-end">
                                <div className="col-md mb-md-0">
                                  <label>Pekerjaan</label>
                                  <p>Pegawai Negeri Sipil</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-12 col-md-6">
                            <div className="form-group mb-3">
                              <div className="row align-items-end">
                                <div className="col-md mb-md-0">
                                  <label>Institusi Tempat Bekerja</label>
                                  <p>Kementerian Kominfo</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="row mb-5">
                          <div className="form-row">
                            <h3 className="mb-1">Kontak Darurat</h3>
                          </div>
                          <div className="col-6 col-md-4">
                            <div className="form-group mb-3">
                              <div className="row align-items-end">
                                <div className="col-md mb-md-0">
                                  <label>Nama Kontak Darurat</label>
                                  <p>Jenifer Blabla</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-12 col-md-4">
                            <div className="form-group mb-3">
                              <div className="row align-items-end">
                                <div className="col-md mb-md-0">
                                  <label>Nomor Handphone</label>
                                  <p>081272727272</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-12 col-md-4">
                            <div className="form-group mb-3">
                              <div className="row align-items-end">
                                <div className="col-md mb-md-0">
                                  <label>Hubungan</label>
                                  <p>Saudara</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="row mb-5">
                          <div className="form-row place-order">
                            <a
                              href="#"
                              data-bs-toggle="modal"
                              data-bs-target="#modalWarning"
                              className="btn btn-outline-blue btn-block"
                            >
                              EDIT PROFILE
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  className="tab-pane fade show"
                  id="nav-pelatihan"
                  role="tabpanel"
                  aria-labelledby="nav-pelatihan"
                >
                  <div className="mb-3">
                    <div className="d-lg-flex align-items-center rounded">
                      <div className="col-12 col-lg-4 col-xl-4 d-lg-flex flex-wrap">
                        <p className="mb-lg-0">
                          Ditemukan{" "}
                          <span className="text-dark">25 Pelatihan</span>
                        </p>
                      </div>
                      <div className="ms-lg-auto d-lg-flex flex-wrap">
                        <div className="form-floating col-12">
                          <select className="form-control-xs shadow-none text-primary rounded">
                            <option>Terbaru</option>
                            <option>Tgl. Pendaftaran</option>
                            <option>Status</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="bg-white border px-5 mb-5">
                    <div className="card border-bottom pt-3 mb-3">
                      <div className="row gx-0">
                        {/* Image */}
                        <a
                          href="detail-pelatihan.html"
                          className="col-auto p-3 d-block"
                          style={{ maxWidth: 80 }}
                        >
                          <img
                            className="img-fluid shadow-light-lg"
                            src="/assets/img/microsoft.webp"
                            alt="Mitra"
                          />
                        </a>
                        {/* Body */}
                        <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                          <div className="card-body p-2">
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Fresh Graduate Academy
                              </span>
                            </div>
                            <a
                              href="detail-pelatihan.html"
                              className="d-block mb-2"
                            >
                              <h6 className="line-clamp-2 mb-3">
                                Database Design and Programming With SQL{" "}
                              </h6>
                            </a>
                            <ul className="nav mx-n3 d-block d-md-flex">
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-calendar" />
                                  </div>
                                  <div className="font-size-sm">
                                    1 - 10 Maret 2022
                                  </div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-map-marker-alt" />
                                  </div>
                                  <div className="font-size-sm">
                                    DKI Jakarta
                                  </div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-microphone-alt" />
                                  </div>
                                  <div className="font-size-sm">
                                    BPSDMP Jakarta
                                  </div>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row gx-2 mb-3">
                      <div className="col-md mb-md-0">
                        <div className="badge badge-sm bg-green mt-3 mb-3 badge-pill">
                          <span className="text-white font-size-sm fw-normal">
                            Lulus Pelatihan
                          </span>
                        </div>
                      </div>
                      <div className="col-auto align-self-end">
                        <a
                          href="#"
                          className="btn btn-outline-blue btn-xs px-3 mb-3 font-size-15"
                        >
                          <i className="fa fa-file-download" />
                          <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                            Download Bukti Pendaftaran
                          </span>
                        </a>
                      </div>
                      <div className="col-auto align-self-end">
                        <a
                          href="#"
                          className="btn btn-blue btn-xs px-3 mb-3 font-size-15"
                        >
                          <i className="fa fa-id-card" />
                          <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                            Download Sertifikat
                          </span>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div className="bg-white border px-5 mb-5">
                    <div className="card border-bottom pt-3 mb-3">
                      <div className="row gx-0">
                        {/* Image */}
                        <a
                          href="detail-pelatihan.html"
                          className="col-auto p-3 d-block"
                          style={{ maxWidth: 80 }}
                        >
                          <img
                            className="img-fluid shadow-light-lg"
                            src="/assets/img/microsoft.webp"
                            alt="Mitra"
                          />
                        </a>
                        {/* Body */}
                        <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                          <div className="card-body p-2">
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Fresh Graduate Academy
                              </span>
                            </div>
                            <a
                              href="detail-pelatihan.html"
                              className="d-block mb-2"
                            >
                              <h6 className="line-clamp-2 mb-3">
                                Database Design and Programming With SQL{" "}
                              </h6>
                            </a>
                            <ul className="nav mx-n3 d-block d-md-flex">
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-calendar" />
                                  </div>
                                  <div className="font-size-sm">
                                    1 - 10 Maret 2022
                                  </div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-map-marker-alt" />
                                  </div>
                                  <div className="font-size-sm">
                                    DKI Jakarta
                                  </div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-microphone-alt" />
                                  </div>
                                  <div className="font-size-sm">
                                    BPSDMP Jakarta
                                  </div>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row gx-2 mb-3">
                      <div className="col-md mb-md-0">
                        <div className="badge badge-sm bg-red mt-3 mb-3 badge-pill">
                          <span className="text-white font-size-sm fw-normal">
                            Tidak Lulus Pelatihan
                          </span>
                        </div>
                      </div>
                      <div className="col-auto align-self-end">
                        <a
                          href="#"
                          className="btn btn-outline-blue btn-xs px-3 mb-3 font-size-15"
                        >
                          <i className="fa fa-file-download" />
                          <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                            Download Bukti Pendaftaran
                          </span>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div className="text-center mb-8">
                    <a
                      className="btn btn-outline-white mw-300p d-flex mx-auto read-more"
                      data-bs-toggle="collapse"
                      href="#loadcollapseExample"
                      role="button"
                      aria-expanded="false"
                      aria-controls="loadcollapseExample"
                    >
                      <span className="d-inline-flex mx-auto align-items-center more">
                        {/* Icon */}
                        <i className="fa fa-chevron-down" />
                        <span className="ms-2">Muat lebih banyak</span>
                      </span>
                      <span className="less mx-auto">LOAD LESS</span>
                    </a>
                  </div>
                </div>
                <div
                  className="tab-pane fade show"
                  id="nav-tes-substansi"
                  role="tabpanel"
                  aria-labelledby="nav-tes-substansi"
                >
                  <div className="mb-3">
                    <div className="d-lg-flex align-items-center rounded">
                      <div className="col-12 col-lg-4 col-xl-4 d-lg-flex flex-wrap">
                        <p className="mb-lg-0">
                          Ditemukan{" "}
                          <span className="text-dark">2 Tes Substansi</span>
                        </p>
                      </div>
                      <div className="ms-lg-auto d-lg-flex flex-wrap">
                        <div className="form-floating col-12">
                          <select className="form-control-xs shadow-none text-primary rounded">
                            <option>Terbaru</option>
                            <option>Tgl. Pendaftaran</option>
                            <option>Status</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="bg-white border px-5 mb-5">
                    <div className="card border-bottom pt-3 mb-3">
                      <div className="row gx-0">
                        {/* Image */}
                        <a
                          href="detail-pelatihan.html"
                          className="col-auto p-3 d-block"
                          style={{ maxWidth: 80 }}
                        >
                          <img
                            className="img-fluid shadow-light-lg"
                            src="/assets/img/exam.png"
                            alt="Mitra"
                          />
                        </a>
                        {/* Body */}
                        <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                          <div className="card-body p-2">
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Tes Substansi
                              </span>
                            </div>
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Fresh Graduate Academy
                              </span>
                            </div>
                            <a
                              href="detail-pelatihan.html"
                              className="d-block mb-2"
                            >
                              <h6 className="line-clamp-2 mb-3">
                                Database Design and Programming With SQL{" "}
                              </h6>
                            </a>
                            <ul className="nav mx-n3 d-block d-md-flex">
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-edit" />
                                  </div>
                                  <div className="font-size-sm">20 Soal</div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-clock" />
                                  </div>
                                  <div className="font-size-sm">60 Menit</div>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row gx-2 mb-3">
                      <div className="col-md mb-md-0">
                        <div className="d-flex align-items-center text-alizarin pb-3">
                          {/* Icon */}
                          <i className="fa fa-bell" />
                          <span className="ms-2">Deadline : 20 April 2022</span>
                        </div>
                      </div>
                      <div className="col-auto align-self-end pb-3">
                        <Link href="/user-guideline-subvit">
                          <a className="btn btn-blue btn-xs px-3 font-size-15">
                            <i className="fa fa-play" />
                            <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                              Kerjakan Tes Substansi
                            </span>
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                  <div className="bg-white border px-5 mb-5">
                    <div className="card border-bottom pt-3 mb-3">
                      <div className="row gx-0">
                        {/* Image */}
                        <a
                          href="detail-pelatihan.html"
                          className="col-auto p-3 d-block"
                          style={{ maxWidth: 80 }}
                        >
                          <img
                            className="img-fluid shadow-light-lg"
                            src="/assets/img/exam.png"
                            alt="Mitra"
                          />
                        </a>
                        {/* Body */}
                        <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                          <div className="card-body p-2">
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Tes Substansi
                              </span>
                            </div>
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Fresh Graduate Academy
                              </span>
                            </div>
                            <a
                              href="detail-pelatihan.html"
                              className="d-block mb-2"
                            >
                              <h6 className="line-clamp-2 mb-3">
                                Database Design and Programming With SQL{" "}
                              </h6>
                            </a>
                            <ul className="nav mx-n3 d-block d-md-flex">
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-edit" />
                                  </div>
                                  <div className="font-size-sm">20 Soal</div>
                                </div>
                              </li>
                              <li className="nav-item px-3 mb-3 mb-md-0">
                                <div className="d-flex align-items-center">
                                  <div className="me-2 d-flex text-secondary icon-uxs">
                                    {/* Icon */}
                                    <i className="fa fa-clock" />
                                  </div>
                                  <div className="font-size-sm">60 Menit</div>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row gx-2 mb-3">
                      <div className="col-md mb-md-0">
                        <div className="d-flex align-items-center pb-3">
                          {/* Icon */}
                          <i className="fa fa-pencil-alt" />
                          <span className="ms-2">
                            Sudah Dikerjakan : 20 April 2022
                          </span>
                        </div>
                      </div>
                      <div className="col-auto align-self-end pb-3">
                        <div className="badge badge-sm bg-green badge-pill">
                          <span className="text-white font-size-sm fw-normal">
                            Lulus Tes Substansi
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="text-center mb-8">
                    <a
                      className="btn btn-outline-white mw-300p d-flex mx-auto read-more"
                      data-bs-toggle="collapse"
                      href="#loadcollapseExample"
                      role="button"
                      aria-expanded="false"
                      aria-controls="loadcollapseExample"
                    >
                      <span className="d-inline-flex mx-auto align-items-center more">
                        {/* Icon */}
                        <i className="fa fa-chevron-down" />
                        <span className="ms-2">Muat lebih banyak</span>
                      </span>
                      <span className="less mx-auto">LOAD LESS</span>
                    </a>
                  </div>
                </div>
                <div
                  className="tab-pane fade show"
                  id="nav-survey"
                  role="tabpanel"
                  aria-labelledby="nav-survey"
                >
                  <div className="mb-3">
                    <div className="d-lg-flex align-items-center rounded">
                      <div className="col-12 col-lg-4 col-xl-4 d-lg-flex flex-wrap">
                        <p className="mb-lg-0">
                          Ditemukan <span className="text-dark">25 Survey</span>
                        </p>
                      </div>
                      <div className="ms-lg-auto d-lg-flex flex-wrap">
                        <div className="form-floating col-12">
                          <select className="form-control-xs shadow-none text-primary rounded">
                            <option>Terbaru</option>
                            <option>Tgl. Pendaftaran</option>
                            <option>Status</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="bg-white border px-5 mb-5">
                    <div className="card border-bottom pt-3 mb-3">
                      <div className="row gx-0">
                        {/* Image */}
                        <a
                          href="detail-pelatihan.html"
                          className="col-auto p-3 d-block"
                          style={{ maxWidth: 80 }}
                        >
                          <img
                            className="img-fluid shadow-light-lg"
                            src="/assets/img/survey.png"
                            alt="Mitra"
                          />
                        </a>
                        {/* Body */}
                        <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                          <div className="card-body p-2">
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Survey
                              </span>
                            </div>
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Fresh Graduate Academy
                              </span>
                            </div>
                            <a
                              href="detail-pelatihan.html"
                              className="d-block mb-2"
                            >
                              <h6 className="line-clamp-2 mb-3">
                                Survey Evaluasi Pelaksanaan Pelatihan
                              </h6>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row gx-2 mb-3">
                      <div className="col-md mb-md-0">
                        <div className="d-flex align-items-center text-alizarin pb-3">
                          {/* Icon */}
                          <i className="fa fa-bell" />
                          <span className="ms-2">Deadline : 20 April 2022</span>
                        </div>
                      </div>
                      <div className="col-auto align-self-end pb-3">
                        <Link href="/user-guideline-survey">
                          <a className="btn btn-blue btn-xs px-3 font-size-15">
                            <i className="fa fa-play" />
                            <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                              Kerjakan Survey
                            </span>
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                  <div className="bg-white border px-5 mb-5">
                    <div className="card border-bottom pt-3 mb-3">
                      <div className="row gx-0">
                        {/* Image */}
                        <a
                          href="detail-pelatihan.html"
                          className="col-auto p-3 d-block"
                          style={{ maxWidth: 80 }}
                        >
                          <img
                            className="img-fluid shadow-light-lg"
                            src="/assets/img/survey.png"
                            alt="Mitra"
                          />
                        </a>
                        {/* Body */}
                        <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                          <div className="card-body p-2">
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Survey
                              </span>
                            </div>
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Fresh Graduate Academy
                              </span>
                            </div>
                            <a
                              href="detail-pelatihan.html"
                              className="d-block mb-2"
                            >
                              <h6 className="line-clamp-2 mb-3">
                                Survey Evaluasi Pengajar Pelatihan
                              </h6>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row gx-2 mb-3">
                      <div className="col-md mb-md-0">
                        <div className="d-flex align-items-center pb-3">
                          {/* Icon */}
                          <i className="fa fa-pencil-alt" />
                          <span className="ms-2">
                            Sudah Dikerjakan : 20 April 2022
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="text-center mb-8">
                    <a
                      className="btn btn-outline-white mw-300p d-flex mx-auto read-more"
                      data-bs-toggle="collapse"
                      href="#loadcollapseExample"
                      role="button"
                      aria-expanded="false"
                      aria-controls="loadcollapseExample"
                    >
                      <span className="d-inline-flex mx-auto align-items-center more">
                        {/* Icon */}
                        <i className="fa fa-chevron-down" />
                        <span className="ms-2">Muat lebih banyak</span>
                      </span>
                      <span className="less mx-auto">LOAD LESS</span>
                    </a>
                  </div>
                </div>
                <div
                  className="tab-pane fade show"
                  id="nav-trivia"
                  role="tabpanel"
                  aria-labelledby="nav-trivia"
                >
                  <div className="mb-3">
                    <div className="d-lg-flex align-items-center rounded">
                      <div className="col-12 col-lg-4 col-xl-4 d-lg-flex flex-wrap">
                        <p className="mb-lg-0">
                          Ditemukan <span className="text-dark">2 Trivia</span>
                        </p>
                      </div>
                      <div className="ms-lg-auto d-lg-flex flex-wrap">
                        <div className="form-floating col-12">
                          <select className="form-control-xs shadow-none text-primary rounded">
                            <option>Terbaru</option>
                            <option>Tgl. Pendaftaran</option>
                            <option>Status</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="bg-white border px-5 mb-5">
                    <div className="card border-bottom pt-3 mb-3">
                      <div className="row gx-0">
                        {/* Image */}
                        <a
                          href="detail-pelatihan.html"
                          className="col-auto p-3 d-block"
                          style={{ maxWidth: 80 }}
                        >
                          <img
                            className="img-fluid shadow-light-lg"
                            src="/assets/img/trivia.png"
                            alt="Trivia"
                          />
                        </a>
                        {/* Body */}
                        <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                          <div className="card-body p-2">
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Trivia
                              </span>
                            </div>
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Fresh Graduate Academy
                              </span>
                            </div>
                            <a
                              href="detail-pelatihan.html"
                              className="d-block mb-2"
                            >
                              <h6 className="line-clamp-2 mb-3">
                                Trivia Tebak Kata Digitalent
                              </h6>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row gx-2 mb-3">
                      <div className="col-md mb-md-0">
                        <div className="d-flex align-items-center text-alizarin pb-3">
                          {/* Icon */}
                          <i className="fa fa-bell" />
                          <span className="ms-2">Deadline : 20 April 2022</span>
                        </div>
                      </div>
                      <div className="col-auto align-self-end pb-3">
                        <Link href="/user-guideline-trivia">
                          <a className="btn btn-blue btn-xs px-3 font-size-15">
                            <i className="fa fa-play" />
                            <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                              Kerjakan Trivia
                            </span>
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                  <div className="bg-white border px-5 mb-5">
                    <div className="card border-bottom pt-3 mb-3">
                      <div className="row gx-0">
                        {/* Image */}
                        <a
                          href="detail-pelatihan.html"
                          className="col-auto p-3 d-block"
                          style={{ maxWidth: 80 }}
                        >
                          <img
                            className="img-fluid shadow-light-lg"
                            src="/assets/img/trivia.png"
                            alt="Trivia"
                          />
                        </a>
                        {/* Body */}
                        <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                          <div className="card-body p-2">
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Trivia
                              </span>
                            </div>
                            <div className="mb-0">
                              <span className="text-dark font-size-sm fw-normal">
                                Fresh Graduate Academy
                              </span>
                            </div>
                            <a
                              href="detail-pelatihan.html"
                              className="d-block mb-2"
                            >
                              <h6 className="line-clamp-2 mb-3">
                                Trivia Tebak Kata Digitalent
                              </h6>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row gx-2 mb-3">
                      <div className="col-md mb-md-0">
                        <div className="d-flex align-items-center pb-3">
                          {/* Icon */}
                          <i className="fa fa-pencil-alt" />
                          <span className="ms-2">
                            Sudah Dikerjakan : 20 April 2022
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="text-center mb-8">
                    <a
                      className="btn btn-outline-white mw-300p d-flex mx-auto read-more"
                      data-bs-toggle="collapse"
                      href="#loadcollapseExample"
                      role="button"
                      aria-expanded="false"
                      aria-controls="loadcollapseExample"
                    >
                      <span className="d-inline-flex mx-auto align-items-center more">
                        {/* Icon */}
                        <i className="fa fa-chevron-down" />
                        <span className="ms-2">Muat lebih banyak</span>
                      </span>
                      <span className="less mx-auto">LOAD LESS</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-4 px-4 mb-5 mb-xl-0">
              {/* SIDEBAR FILTER ================================================== */}
              <div className="col-lg-12 mb-5">
                <div className="card border px-5 pt-6 pb-8">
                  <h4 className="mb-5">Akses Platform</h4>
                  <div className="row">
                    <div className="col-12 border-bottom px-2 py-3">
                      {/* Card */}
                      <a href="#" className="mb-3">
                        <div className="row align-items-center px-2">
                          <div className="col-auto">
                            <div className="avatar avatar-custom d-inline">
                              <img
                                src="/assets/img/logo-dts.svg"
                                alt="..."
                                className="avatar-img avatar-md img-fluid"
                              />
                            </div>
                          </div>
                          <div className="col px-3">
                            {/* Body */}
                            <div className="card-body p-0">
                              <h6 className="mb-0 line-clamp-1">
                                Digital Talent Scholarship
                              </h6>
                              <p className="mb-0 line-clamp-1">
                                Platform Pelatihan
                              </p>
                            </div>
                          </div>
                          <div className="col-auto">
                            <i className="fa fa-chevron-right text-muted" />
                          </div>
                        </div>
                      </a>
                    </div>
                    <div className="col-12 border-bottom px-2 py-3">
                      {/* Card */}
                      <a href="#" className="mb-3">
                        <div className="row align-items-center px-2">
                          <div className="col-auto">
                            <div className="avatar avatar-custom d-inline">
                              <img
                                src="/assets/img/simonas.png"
                                alt="..."
                                className="avatar-img avatar-md img-fluid"
                              />
                            </div>
                          </div>
                          <div className="col px-3">
                            {/* Body */}
                            <div className="card-body p-0">
                              <h6 className="mb-0 line-clamp-1">Simonas</h6>
                              <p className="mb-0 line-clamp-1">
                                Platform magang &amp; kerja
                              </p>
                            </div>
                          </div>
                          <div className="col-auto">
                            <i className="fa fa-chevron-right text-muted" />
                          </div>
                        </div>
                      </a>
                    </div>
                    <div className="col-12 border-bottom px-2 py-3">
                      {/* Card */}
                      <a href="#" className="mb-3">
                        <div className="row align-items-center px-2">
                          <div className="col-auto">
                            <div className="avatar avatar-custom d-inline">
                              <img
                                src="/assets/img/beasiswa.png"
                                alt="..."
                                className="avatar-img avatar-md img-fluid"
                              />
                            </div>
                          </div>
                          <div className="col px-3">
                            {/* Body */}
                            <div className="card-body p-0">
                              <h6 className="mb-0 line-clamp-1">
                                Beasiswa Kominfo
                              </h6>
                              <p className="mb-0 line-clamp-1">
                                Platform informasi beasiswa
                              </p>
                            </div>
                          </div>
                          <div className="col-auto">
                            <i className="fa fa-chevron-right text-muted" />
                          </div>
                        </div>
                      </a>
                    </div>
                    <div className="col-12 border-bottom px-2 py-3">
                      {/* Card */}
                      <a href="#" className="mb-3">
                        <div className="row align-items-center px-2">
                          <div className="col-auto">
                            <div className="avatar avatar-custom d-inline">
                              <img
                                src="/assets/img/dla.png"
                                alt="..."
                                className="avatar-img avatar-md img-fluid"
                              />
                            </div>
                          </div>
                          <div className="col px-3">
                            {/* Body */}
                            <div className="card-body p-0">
                              <h6 className="mb-0 line-clamp-1">Digileader</h6>
                              <p className="mb-0 line-clamp-1">
                                Platform pelatihan digileader
                              </p>
                            </div>
                          </div>
                          <div className="col-auto">
                            <i className="fa fa-chevron-right text-muted" />
                          </div>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
