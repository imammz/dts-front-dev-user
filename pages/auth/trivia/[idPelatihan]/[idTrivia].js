import moment from "moment";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useMemo, useRef, useState } from "react";
import Skeleton from "react-loading-skeleton";
import FormTrivia from "../../../../components/Form/Trivia";
import PortalService from "../../../../services/PortalService";
import ProfilService from "../../../../services/ProfileService";
import NotificationHelper from "../../../../src/helper/NotificationHelper";

import Lottie, { useLottie } from "lottie-react";
import * as animation from "../../../../public/assets/json/loading-animation.json";
import PortalHelper from "../../../../src/helper/PortalHelper";
import { CacheHelperV2 as CacheHelper } from "../../../../src/helper/CacheHelper";
import EncryptionHelper from "../../../../src/helper/EncryptionHelper";

// untuk keperluan development, jika true beberapa validasi akan dilewat
const forceTest = false;

// Belum ada data
const BelumAdaData = ({ message = "Belum Ada Data" }) => {
  return (
    <>
      <div className="container py-8">
        <div className="row">
          <div className="col-12 mx-auto text-center">
            <h2>{message}</h2>
            {/* <p className="font-size-lg mb-6">Pelatihan pada akademi <span className="text-blue font-weight-semi-bold">{AkademiDetail.nama}</span> belum tersedia</p> */}
            <img
              src="/assets/img/empty-state/not-found.png"
              alt="..."
              className="img-fluid align-self-center"
              style={{ width: "500px" }}
            />
          </div>
        </div>
      </div>
    </>
  );
};

// selesai
const Selesai = ({
  namaTest = "",
  namaPelatihan = "",
  namaAkademi = "",
  idPelatihan,
  idTrivia,
}) => {
  return (
    <>
      <div className="container mb-8">
        <div className="row py-8">
          <div className="col-lg-7 border rounded-5 border-0 border-lg border-xl mx-auto">
            <div className="d-block rounded-5 w-100">
              <div className="pt-6 px-5 px-lg-3 px-xl-5 pt-6 px-5 px-lg-3 px-xl-5 text-center">
                <img
                  src="/assets/img/empty-state/finished-trivia.png"
                  className="img-fluid align-self-center mb-5"
                  style={{ width: "400px" }}
                />
                <div className="row">
                  <div className="col-12 px-4">
                    <h2 className="fw-bold text-green mb-0">Trivia selesai</h2>
                    <p>
                      Terima kasih atas partisipasi kamu dalam mengikuti
                      <br />
                      <strong>{namaTest}</strong>
                    </p>
                    <div className="mb-5">
                      <div className="my-7">
                        <Link
                          href={{
                            pathname: "/auth/user-profile",
                            query: { menu: "trivia" },
                          }}
                        >
                          <a className="btn btn-md py-2 rounded-pill font-size-14 btn-primary">
                            TUTUP
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

// Guideline
const Guidline = ({ setShowRenderer, namaTest }) => {
  return (
    <>
      <div className="container my-8">
        <div className="row">
          <div className="col-lg-12">
            <div className="card border rounded-5 px-6 py-8">
              <div className="row">
                <div className="col-lg-5 mb-6">
                  <h3 className="fw-bold text-center mb-4">
                    Trivia<br></br>
                    {namaTest}
                  </h3>

                  <img
                    src="/assets/img/empty-state/choose.png"
                    className="img-fluid pt-5"
                  />
                </div>
                <div className="col-lg-7">
                  <div className="d-block rounded-5 bg-info">
                    <div className="pt-6 px-5">
                      <div className="d-flex align-items-center">
                        <div>
                          <h4 className="fw-semi-bold mb-4">Panduan:</h4>
                          <p>
                            Sebelum mengerjakan, mohon perhatikan hal-hal
                            berikut:
                          </p>
                          <ul
                            className="list-style-v2 list-unstyled"
                            style={{ textAlign: "left" }}
                          >
                            <li>
                              Peserta wajib menjawab seluruh soal. Tidak ada
                              nilai negatif untuk jawaban yang salah.
                            </li>
                            <li>
                              Masing-masing soal memiliki batas waktu pengerjaan
                              yang berbeda.
                            </li>
                            <li>
                              Setelah Trivia dimulai, waktu pengerjaan tidak
                              dapat dihentikan dan tidak dapat diulang. Setelah
                              waktu habis, halaman soal akan tertutup secara
                              otomatis.
                            </li>
                            <li>
                              Skor untuk soal yang telah dijawab tetap terhitung
                              meskipun peserta belum melakukan submit/ kehabisan
                              habis/ peserta mengalami force majeure.
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div className="form-row place-order p-5">
                      <a
                        onClick={() => {
                          setShowRenderer(true);
                        }}
                        className="btn btn-blue rounded-pill btn-block"
                      >
                        <i className="fa fa-play me-2"></i>MULAI TRIVIA
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

// komponen formrenderer
const Timer = ({ no, duration, onTimesUp = () => {}, textOnly = false }) => {
  const [cur, setCur] = useState(null);

  useEffect(() => {
    // console.log(no, duration)
    if (no && duration) {
      setCur(duration * 1000);
    }
  }, [no, duration]);

  useEffect(() => {
    let a;
    if (cur != null) {
      if (cur > 0) {
        a = setTimeout(() => {
          setCur((c) => c - 1000);
        }, 1000);
      } else if (cur <= 0) {
        onTimesUp();
        setCur(null);
      }
    }
    return function () {
      clearTimeout(a);
    };
  }, [cur]);

  return !textOnly ? (
    <div className="card border px-5 py-5 mb-5">
      <h5 className="mb-0">Waktu</h5>
      <div className="col">
        {cur > 0 ? (
          <>
            <h4 className="text-green my-0">
              {moment.utc(cur).format("HH:mm:ss")}
            </h4>
          </>
        ) : (
          <>
            <h4 className="text-red my-0">00:00:00</h4>
          </>
        )}
      </div>
    </div>
  ) : cur > 0 ? (
    <>
      <span className="text-green my-0">
        {moment.utc(cur).format("HH:mm:ss")}
      </span>
    </>
  ) : (
    <>
      <span className="text-red my-0">00:00:00</span>
    </>
  );
};

const ModalWaktuHabis = ({ show, onClose = () => {} }) => {
  const [idModal] = useState("modalWaktuHabis");

  const toggleOpenModal = useRef(null);
  const toggleCloseModal = useRef(null);
  const showSuccessModal = useRef(null);

  useEffect(() => {
    showSuccessModal.current.addEventListener("hidden.bs.modal", () => {
      onClose();
    });
    if (show && idModal) {
      toggleOpenModal.current?.click();
    } else {
      toggleCloseModal.current?.click();
    }
  }, [show]);

  return (
    <>
      <a
        href="#"
        ref={toggleOpenModal}
        data-bs-toggle="modal"
        data-bs-target={`#${idModal}`}
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseModal}
        data-bs-dismiss="modal"
        data-bs-target={`#${idModal}`}
        hidden
      ></a>

      <div
        className="modal fade"
        id={`${idModal}`}
        ref={showSuccessModal}
        tabIndex={-1}
        role="dialog"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <button
                type="button"
                className="close"
                data-bs-dismiss="modal"
                aria-label="Close"
                onClick={() => {
                  onClose();
                }}
              >
                <span aria-hidden="true">
                  <svg
                    width={16}
                    height={17}
                    viewBox="0 0 16 17"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M0.142135 2.00015L1.55635 0.585938L15.6985 14.7281L14.2843 16.1423L0.142135 2.00015Z"
                      fill="currentColor"
                    />
                    <path
                      d="M14.1421 1.0001L15.5563 2.41431L1.41421 16.5564L0 15.1422L14.1421 1.0001Z"
                      fill="currentColor"
                    />
                  </svg>
                </span>
              </button>

              <h2
                className="fw-bold text-center mb-1 text-blue"
                id={`${idModal}Title`}
              >
                Perhatian
              </h2>

              <p className="font-size-lg text-center text-muted mb-0">
                Waktu pengerjaan trivia telah selesai
              </p>
              <div className="mb-md-4 text-center">
                <img
                  src="/assets/img/illustrations/times-up.png"
                  className="w-50 image-responsive"
                />
              </div>

              <div className="row">
                <div className="col-12">
                  <button
                    className="btn btn-block btn-primary rounded-pill mt-3 lift"
                    onClick={() => {
                      onClose();
                    }}
                  >
                    Submit Jawaban
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const ModalResumePengerjaan = ({
  show,
  onClose = () => {},
  onOk = () => {},
  children,
}) => {
  const [idModal] = useState("modalResumePengerjaan");

  const toggleOpenModal = useRef(null);
  const toggleCloseModal = useRef(null);
  const showSuccessModal = useRef(null);

  useEffect(() => {
    showSuccessModal.current.addEventListener("hidden.bs.modal", () => {
      onClose();
    });
    if (show && idModal) {
      toggleOpenModal.current?.click();
    } else {
      toggleCloseModal.current?.click();
    }
  }, [show]);

  return (
    <>
      <a
        href="#"
        ref={toggleOpenModal}
        data-bs-toggle="modal"
        data-bs-target={`#${idModal}`}
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseModal}
        data-bs-dismiss="modal"
        data-bs-target={`#${idModal}`}
        hidden
      ></a>

      <div
        className="modal fade"
        id={`${idModal}`}
        ref={showSuccessModal}
        tabIndex={-1}
        role="dialog"
      >
        <div
          className="modal-dialog modal-dialog-centered modal-fullscreen-sm-down"
          role="document"
        >
          <div className="modal-content">
            <div className="modal-header py-3 px-4">
              <h5 className="modal-title" id="exampleModalLabel">
                Status Pengerjaan
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body py-3 px-4">{children}</div>
            <div className="modal-footer py-3 px-4 justify-content-between">
              <button
                type="button"
                className="btn btn-sm btn-secondary"
                data-bs-dismiss="modal"
              >
                Kembali
              </button>
              <button
                type="button"
                className="btn btn-sm btn-primary"
                onClick={() => {
                  onOk();
                }}
              >
                Selesaikan Test Substansi
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

function ModalLoading({
  id = "mdlLoadingTrivia",
  show,
  handleClose,
  loadingMessage = "Loading ...",
  backdropOnly = true,
}) {
  const toggleOpenLoadignModal = useRef(null);
  const toggleCloseLoadingModal = useRef(null);
  const showLoadingModal = useRef(null);

  const options = {
    animationData: animation,
    loop: true,
  };
  const lottie = useLottie(options);
  lottie.setSpeed(2);
  const { View } = lottie;

  useEffect(() => {
    //console.log(show)
    showLoadingModal.current?.addEventListener("hidden.bs.modal", () => {
      handleClose();
    });
    if (show) {
      toggleOpenLoadignModal.current?.click();
    } else {
      toggleCloseLoadingModal.current.click();
      // showLoadingModal.current.addEventListener('shown.bs.modal', () => {
      //     if (!show) {
      //     }
      // })
    }
  }, [show]);

  return (
    <>
      <a
        href="#"
        ref={toggleOpenLoadignModal}
        data-bs-toggle="modal"
        data-bs-target={"#" + id}
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseLoadingModal}
        data-bs-dismiss="modal"
        data-bs-target={"#" + id}
        hidden
      ></a>

      <div
        className="modal"
        id={id}
        ref={showLoadingModal}
        data-bs-backdrop="static"
        data-bs-keyboard="false"
        role="dialog"
      >
        {!backdropOnly && (
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">
              <div className="modal-body" style={{ textAlign: "center" }}>
                <div className="w-50 m-auto">
                  {View}
                  <h2
                    className="fw-bold text-center mb-1 text-primary"
                    id="title"
                  >
                    {loadingMessage}
                  </h2>
                </div>
              </div>
            </div>
          </div>
        )}
        {backdropOnly && (
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-body" style={{ background: "transparent" }}>
              <div className="w-50 m-auto" style={{ textAlign: "center" }}>
                {View}
                <h2 className="fw-bold text-center mb-1 text-white" id="title">
                  {loadingMessage}
                </h2>
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  );
}

const ModalDetailTrivia = ({
  show,
  onCancel = () => {},
  onClose = () => {},
  onOk = () => {},
  children,
  currentNumber = 1,
}) => {
  const [idModal] = useState("modalDetailTrivia");

  const toggleOpenModal = useRef(null);
  const toggleCloseModal = useRef(null);
  const showSuccessModal = useRef(null);

  useEffect(() => {
    showSuccessModal.current.addEventListener("hidden.bs.modal", () => {
      onClose();
    });
    if (show && idModal) {
      toggleOpenModal.current?.click();
    } else {
      toggleCloseModal.current?.click();
    }
  }, [show]);

  return (
    <>
      <a
        href="#"
        ref={toggleOpenModal}
        data-bs-toggle="modal"
        data-bs-target={`#${idModal}`}
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseModal}
        data-bs-dismiss="modal"
        data-bs-target={`#${idModal}`}
        hidden
      ></a>

      <div
        className="modal fade"
        id={`${idModal}`}
        ref={showSuccessModal}
        data-bs-backdrop="static"
        tabIndex={-1}
        role="dialog"
      >
        <div
          className="modal-dialog modal-dialog-centered modal-fullscreen-sm-down"
          role="document"
        >
          <div className="modal-content">
            <div className="modal-header py-3 px-4">
              <h5 className="modal-title fw-bold" id="exampleModalLabel">
                Trivia
              </h5>
              <button
                type="button"
                onClick={() => {
                  onCancel();
                }}
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body py-3 px-4">{children}</div>
            <div className="modal-footer py-3 px-4 justify-content-center">
              <button
                type="button"
                className="btn btn-sm rounded-pill btn-outline-secondary"
                onClick={() => {
                  onCancel();
                }}
                data-bs-dismiss="modal"
              >
                Batal
              </button>
              <button
                type="button"
                className="btn btn-sm rounded-pill btn-primary"
                onClick={() => {
                  onOk();
                }}
              >
                {currentNumber == 1
                  ? "Mulai"
                  : `Mulai Pertanyaan No. ${currentNumber}`}
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const DurasiPengerjaan = ({ duration }) => {
  const [dur, setDur] = useState(0);
  useEffect(() => {
    const dur = moment.duration(duration, "seconds");
    dur.locale("ID");
    const h = Math.floor(dur.asHours());
    const m = Math.floor(
      h > 0 ? dur.subtract(h, "hours").asMinutes() : dur.asMinutes(),
    );
    const d = Math.floor(
      m > 0 ? dur.subtract(m, "minutes").seconds() : dur.seconds(),
    );
    setDur(
      `${h > 0 ? h + " Jam, " : ""}${m > 0 ? m + " Menit, " : ""}${d} Detik`,
    );
  }, [duration]);

  return dur;
};

function ModalPreSoal({ show, handleClose, no, duration }) {
  const toggleOpenLoadignModal = useRef(null);
  const toggleCloseLoadingModal = useRef(null);
  const showLoadingModal = useRef(null);
  const [cur, setCur] = useState(null);

  const countDown = useMemo(() => 3, []);

  useEffect(() => {
    //console.log(show)
    showLoadingModal.current?.addEventListener("hidden.bs.modal", () => {
      handleClose();
    });
    if (show) {
      setCur(countDown);
      toggleOpenLoadignModal.current?.click();
    } else {
      toggleCloseLoadingModal.current.click();
      setCur(null);
    }
  }, [show]);

  useEffect(() => {
    if (cur != null) {
      if (cur > 0) {
        setTimeout(() => {
          setCur((c) => c - 1);
        }, 1000);
      } else if (cur <= 0) {
        // window.alert("waktu habis")
        handleClose();
      }
    }
  }, [cur]);

  return (
    <>
      <a
        href="#"
        ref={toggleOpenLoadignModal}
        data-bs-toggle="modal"
        data-bs-target="#modalPreSoal"
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseLoadingModal}
        data-bs-dismiss="modal"
        data-bs-target="#modalPreSoal"
        hidden
      ></a>

      <div
        className="modal"
        id="modalPreSoal"
        ref={showLoadingModal}
        data-bs-backdrop="static"
        data-bs-keyboard="false"
        role="dialog"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-body" style={{ background: "transparent" }}>
            <div className=" m-auto text-white" style={{ textAlign: "center" }}>
              <h1 className="fw-bold text-center mb-1 text-white" id="title">
                {`Pertanyaan Ke ${no}`}
              </h1>
              <h4
                className="fw-bold text-center mb-1 text-white mb-5"
                id="title"
              >
                Waktu Pengerjaan{" "}
                <h4 className="text-success fw-bold">
                  {" "}
                  <DurasiPengerjaan duration={duration}></DurasiPengerjaan>
                </h4>
              </h4>
              <h4 className="text-white">mulai dalam ({cur}) ...</h4>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const FormRenderer = ({
  namaTest,
  namaPelatihan,
  namaAkademi,
  showRenderer,
  idPelatihan = "",
  idTrivia = "",
  jumlahHarusDijawab,
}) => {
  const [listSoal, setListSoal] = useState([]);
  const [currentNumber, setCurrentNumber] = useState(0);
  const [waktu, setWaktu] = useState({ duration: 10, no: 0 });

  // loading awal dan submit
  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingSubmit, setIsLoadingSubmit] = useState(false);
  const [loadingMessage, setLoadingMessage] = useState("Memuat soal ...");

  const [statPengerjaan, setStatPengerjaan] = useState({
    terjawab: 0,
    totalSoal: 0,
    belumDijawab: 0,
  });
  const [showModalResumePengerjaan, setShowModalResumePengerjaan] =
    useState(false);
  const [showModalWaktuHabis, setShowModalWaktuHabis] = useState(false);

  const headerMobileSubstansi = useRef();
  const [isHeaderSticy, setIsHeaderSticky] = useState(false);

  const [user] = useState(PortalHelper.getUserData());

  const cacheId = "test-trivia-" + user.id + "-" + idPelatihan + "-" + idTrivia;

  const [isShownModalDetailTrivia, setIsShownModalDetailTrivia] =
    useState(false);
  const [detailTrivia, setDetailTrivia] = useState({
    jumlahSoal: 0,
    totalDurasi: 10,
  });

  const [isShownModalPreSoal, setShownModalPreSoal] = useState(false);
  const [dataPreSoal, setDataPreSoal] = useState({ no: 0, duration: 10 });

  const [wasSubmitted, setWasSubmitted] = useState(false);
  const [readySubmit, setReadyForSubmit] = useState(false);

  const [isSubmitDisabled, setSubmitDisabled] = useState(true);

  const popOver = useRef();

  const router = useRouter();

  // useEffect(() => {
  //     if (!isLoading && !isShownModalDetailTrivia && !isShownModalPreSoal) {
  //         const el = document.getElementById("trivia-next-button")
  //         if (el != null) {
  //             if (typeof bootstrap.Popover == "function") {
  //                 popOver.current = new bootstrap.Popover(el, {
  //                     placement: "top",
  //                     html: true,
  //                     content: `
  //                     <div>
  //                           <p className="mb-2">Harap isi jawaban dari pertanyaan diatas</p>
  //                           <div className="">
  //                               <button id="popover-cancel" className="btn btn-xs " type="button">Batal</button>
  //                           </div>

  //                       </div>
  //                     `,
  //                     trigger: "click",
  //                     sanitize: false
  //                 })
  //                 // const clickEventYes = function () {
  //                 //     popOver.current?.hide()
  //                 //     // popOver.current?.dispose()
  //                 //     setCurrentNumber(c => c + 1)
  //                 // }
  //                 el.addEventListener('shown.bs.popover', () => {
  //                     const btnYes = document.getElementById("popover-next")
  //                     const btnNo = document.getElementById("popover-cancel")
  //                     // console.log(btn)
  //                     if (btnYes != null) {
  //                         btnYes.addEventListener("click", clickEventYes)
  //                     }
  //                     if (btnNo != null) {
  //                         btnNo.addEventListener("click", function () {
  //                             popOver.current?.hide()
  //                             btnYes.removeEventListener("click", clickEventYes)
  //                         })

  //                     }
  //                     // do something...
  //                 })
  //                 // el.addEventListener('hidden.bs.tooltip', () => {
  //                 //     const btnYes = document.getElementById("popover-next")
  //                 //     // console.log(btn)
  //                 //     if (btnYes != null) {
  //                 //         btnYes.removeEventListener("click", clickEventYes)
  //                 //     }

  //                 //     // do something...
  //                 // })

  //             }
  //         }
  //     }

  // }, [isLoading, isShownModalDetailTrivia, isShownModalPreSoal])

  //  pengaturan untuk tampilan mobile
  useEffect(() => {
    window.onscroll = function () {
      myFunction();
    };

    function myFunction() {
      var sticky = headerMobileSubstansi.current?.getBoundingClientRect().top;
      if (window.pageYOffset > sticky) {
        setIsHeaderSticky(true);
      } else {
        setIsHeaderSticky(false);
      }
    }
    return () => {
      window.onscroll = true;
    };
  }, []);

  // event ketika page pertamakali diload dan mengambil data soal + waktu pengerjaan
  useEffect(async () => {
    try {
      if (showRenderer && idTrivia) {
        const resp = await ProfilService.getPertanyaanTrivia(idTrivia);

        // cek jawaban yg tersimpan
        const objJawaban = await getLocal();
        const objSeen = await getLocalSeen();
        // console.log("jawban", objJawaban)
        // console.log("objs", objSeen)
        const getJawabanByIdSoal = (idSoal) => {
          // if (objJawaban[idSoal] && idSoal) {
          //     return objJawaban[idSoal]
          // } else {
          return objJawaban[idSoal];
          // }
        };

        const getSeenByIdSoal = (idSoal) => {
          // if (objJawaban[idSoal] && idSoal) {
          //     return objJawaban[idSoal]
          // } else {
          return objSeen[idSoal] == "1";
          // }
        };

        const orders = await CacheHelper.GetFromCache({
          index: cacheId + "-orders",
        });
        // console.log(orders)

        // console.log("local", objJawaban)
        let newSoal = [];
        let totalSoal = 0;
        let totalDurasi = 0;
        let found = false;
        if (resp.data.success) {
          // console.log(resp.data.result)
          let soals = [];
          if (orders) {
            orders.forEach((id) => {
              let s = _.find(resp.data.result.data, function (o) {
                return o.trivia_question_bank_detail_id == id;
              });
              soals.push(s);
            });
          } else {
            soals = _.shuffle(resp.data.result.data).slice(
              0,
              jumlahHarusDijawab,
            );
          }

          // console.log(soals)
          let orderSoals = [];

          for (let i = 0; i < soals.length; i++) {
            // set preSoal
            // let jawab = getJawabanByIdSoal()

            let no = i + 1;
            // console.log(soals[i])
            const idSoal = soals[i].trivia_question_bank_detail_id;
            const jwb = getJawabanByIdSoal(idSoal);
            orderSoals.push(idSoal);
            // console.log(idSoal, jwb)
            totalSoal++;
            totalDurasi += parseInt(soals[i].duration);

            let seen = getSeenByIdSoal(idSoal);
            let curr = { ...soals[i], seen: seen };

            // console.log(i, found, seen, curr)
            if (!found && !seen) {
              found = true;
              // console.log("found", soals[soals.length - 1])
              // // soals[i] = { ...soals[i], seen: true }
              // soals[soals.length - 1] = { ...soals[soals.length - 1], seen: true }
              curr = { ...curr, seen: true };
              setCurrentNumber(i + 1);
              setDataPreSoal({ no: i + 1, duration: soals[i].duration });
            }

            newSoal.push({
              ...curr,
              answer_key: jwb == undefined ? "" : jwb,
              no: no,
            });
          }

          CacheHelper.CreateCache({
            index: cacheId + "-orders",
            data: orderSoals,
          });

          // console.log("ns", newSoal)
          setListSoal(newSoal);
        }

        // if (newSoal.length == 1 && !found) {

        //     found = true;
        //     setCurrentNumber(1)
        //     setDataPreSoal({ no: 1, duration: newSoal[0].duration })
        // } else if (newSoal[0].seen == true && newSoal[1].seen == false) {

        //     setCurrentNumber(1)
        //     setDataPreSoal({ no: 1, duration: newSoal[0].duration })
        // }
        if (!found) {
          setIsLoading(false);
          // setCurrentNumber(newSoal.length + 1)
          setReadyForSubmit(true);
          setShowModalWaktuHabis(true);
        } else {
          setIsLoading(false);
          setDetailTrivia({ totalDurasi: totalDurasi, jumlahSoal: totalSoal });
          setIsShownModalDetailTrivia(true);
        }
      }
    } catch (error) {
      console.log(error);
    }
  }, [showRenderer, idTrivia, idPelatihan]);

  // event ketika perubahan page number
  useEffect(() => {
    let soals = [...listSoal];
    if (soals.length > 0 && currentNumber > 0) {
      // const el = document.getElementById("trivia-next-button")

      if (popOver.current) {
        popOver.current?.hide();
      }
      const soal = soals[currentNumber - 1];
      soal = { ...soal, seen: true };
      soals[currentNumber - 1] = soal;
      setDataPreSoal({ no: soal.no, duration: soal.duration });
      setShownModalPreSoal(true);
      // console.log(soals)
      setListSoal(soals);
      saveLocal(soal);
      // saveLocalSeen(soal)
    }
    // setTimeout(() => {
    PortalHelper.scrollSpy("#fieldPengerjaanTrivia", 120);
    // }, 1000);
  }, [currentNumber]);

  // const updateStatPengerjaan = () => {
  //     let jawab = 0;
  //     let totalSoal = listSoal.length
  //     for (let i = 0; i < totalSoal; i++) {
  //         if (listSoal[i].answer_key != "") {
  //             jawab++
  //         }
  //     }
  //     setStatPengerjaan({ terjawab: jawab, totalSoal: totalSoal, belumDijawab: (totalSoal - jawab) })
  // }

  const onJawab = (v) => {
    // console.log(v)
    let soals = [...listSoal];
    soals[currentNumber - 1] = { ...soals[currentNumber - 1], answer_key: v };
    setListSoal(soals);
    saveLocal({ ...soals[currentNumber - 1] });

    if (soals.length == currentNumber) {
      setSubmitDisabled(false);
    }
  };

  // ketika hitungan waktu tersisa = 0
  const onTimesUp = () => {
    // window.alert("times up")
    // if (!forceTest) {
    //     setShowModalWaktuHabis(true)
    // }
    if (currentNumber < listSoal.length) {
      setCurrentNumber((c) => c + 1);
    } else {
      if (!wasSubmitted) {
        setShowModalWaktuHabis(true);
      }
    }
  };

  const submitJawban = async () => {
    try {
      setLoadingMessage("Mengunggah jawaban ...");
      setIsLoadingSubmit(true);
      let data = [];
      listSoal.forEach((soal) => {
        // console.log(soal)
        data.push({
          trivia_question_bank_id: soal.trivia_question_bank_id,
          trivia_question_bank_detail_id: soal.trivia_question_bank_detail_id,
          question: soal.question,
          question_image: soal.question_image,
          type: soal.type,
          answer_key: soal.answer_key,
          answer: soal.answer,
          status: soal.status,
          duration: soal.duration,
          value: soal.answer_key,
          nourut: soal.nourut,
        });
        // data.push({
        //     trivia_question_bank_id: soal.trivia_question_bank_id,
        //     trivia_question_bank_detail_id: soal.trivia_question_bank_detail_id,
        //     question: soal.question,
        //     question_image: soal.question_image,
        //     answer_key: soal.answer_key,
        //     answer: soal.answer,
        //     status: soal.status,
        //     duration: soal.duration,
        //     value: soal.value,
        //     nourut: soal.nourut
        //     // answer_key: soal.answer_key,
        //     // question: soal.question,
        //     // question_image: soal.question_image,
        //     // answer: soal.answer,
        //     // subtance_question_bank_detail_id: soal.question_id,
        //     // subtance_question_bank_id: soal.substansi_id
        // })
      });
      // console.log(data)
      const resp = await ProfilService.profilSubmitTrivia(
        idPelatihan,
        idTrivia,
        JSON.stringify(data),
      );
      if (resp.data.success) {
        if (resp.data.result.data[0]?.code == 200) {
          CacheHelper.ClearCache({ index: cacheId });
          CacheHelper.ClearCache({ index: cacheId + "-seen" });
          CacheHelper.ClearCache({ index: cacheId + "-orders" });
          NotificationHelper.Success({
            message: "Jawaban berhasil disubmit",
            onClose: () => {
              window.location.reload();
            },
          });
        } else if (resp.data.result.data[0]?.code == 500) {
          NotificationHelper.Failed({
            message: resp.data.result.data[0]?.message,
            onClose: () => {
              window.location.reload();
            },
          });
        } else {
          NotificationHelper.Failed({
            message: "Terjadi kesalahan",
            onClose: () => {
              window.location.reload();
            },
          });
        }
      } else {
        NotificationHelper.Failed({
          message: resp.data.result,
          onClose: () => {
            window.location.reload();
          },
        });
      }
    } catch (error) {
      console.log(error);
      NotificationHelper.Failed({
        message: error.toString(),
        onClose: () => {
          window.location.reload();
        },
      });
    }
    setIsLoadingSubmit(false);
  };

  useEffect(() => {
    // console.log(currentNumber, listSoal.length)
    if (
      !showModalWaktuHabis &&
      (currentNumber == listSoal.length || readySubmit) &&
      listSoal.length != 0
    ) {
      // console.log("sumit")
      submitJawban();
    }
  }, [showModalWaktuHabis]);

  //
  const saveLocal = async ({ trivia_question_bank_detail_id, answer_key }) => {
    const id_soal = trivia_question_bank_detail_id;
    const jawaban = answer_key;

    let data = await getLocal();

    const save = (d) => {
      let dataSimpan = EncryptionHelper.encrypt(JSON.stringify(d));
      CacheHelper.CreateCache({ index: cacheId, data: dataSimpan });
    };

    if (data != null) {
      data[id_soal] = jawaban;
      // console.log(data)
      save(data);
    } else {
      data = {};
      data[id_soal] = jawaban;
      // console.log(data)
      save(data);
    }
  };

  const getLocal = async () => {
    // console.log("called get local")
    try {
      let dt = await CacheHelper.GetFromCache({ index: cacheId, force: true });
      // console.log("dt", dt)
      if (dt != undefined) {
        // console.log("return")
        return JSON.parse(EncryptionHelper.decrypt(dt));
      } else {
        // console.log("return und")
        return {};
      }
    } catch (error) {
      // console.log(error)
      return {};
    }
  };

  const saveLocalSeen = async ({ trivia_question_bank_detail_id }) => {
    const id_soal = trivia_question_bank_detail_id;
    const jawaban = "1";

    let data = await getLocalSeen();

    const save = (d) => {
      // console.log("set seen", d)
      let dataSimpan = EncryptionHelper.encrypt(JSON.stringify(d));
      CacheHelper.CreateCache({ index: cacheId + "-seen", data: dataSimpan });
    };

    if (data != null) {
      data[id_soal] = jawaban;
      // console.log(data)
      save(data);
    } else {
      data = {};
      if (id_soal) {
        data[id_soal] = jawaban;
        // console.log(data)
        save(data);
      }
    }
  };

  const getLocalSeen = async () => {
    // console.log("called get local")
    try {
      let dt = await CacheHelper.GetFromCache({
        index: cacheId + "-seen",
        force: true,
      });
      // console.log("dt", dt)
      if (dt != undefined) {
        // console.log("return")
        return JSON.parse(EncryptionHelper.decrypt(dt));
      } else {
        // console.log("return und")
        return {};
      }
    } catch (error) {
      // console.log(error)
      return {};
    }
  };

  useEffect(() => {
    if (!isShownModalPreSoal && currentNumber > 0) {
      let soals = [...listSoal];

      saveLocalSeen({ ...soals[currentNumber - 1] });
    }
  }, [isShownModalPreSoal]);

  const startTrivia = async () => {
    try {
      const resp = await ProfilService.profilTriviaStart(idPelatihan, idTrivia);
      if (resp.data.success) {
        setIsShownModalDetailTrivia(false);
        setShownModalPreSoal(true);
      } else {
        NotificationHelper.Failed({
          message: "Gagal memulai trivia",
          onClose: () => {
            window.location.reload();
          },
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  const nextSoal = () => {
    // const curPos = currentNumber
    const curSoal = listSoal[currentNumber - 1];
    if (curSoal.answer_key != "") {
      if (popOver.current) {
        popOver.current.hide();
        popOver.current = null;
      }

      setCurrentNumber((n) => n + 1);
    } else {
      if (!popOver.current) {
        const el = document.getElementById("trivia-next-button");
        if (el != null) {
          if (typeof bootstrap.Popover == "function") {
            popOver.current = new bootstrap.Popover(el, {
              placement: "top",
              html: true,
              content: `
                            <div>
                                  <p className="mb-2">Harap pilih jawaban dari pertanyaan diatas</p>
                                  <div className="">
                                      <button id="popover-cancel" className="btn btn-xs " type="button">Kembali</button>
                                  </div>
                                  
                              </div>
                            `,
              trigger: "click",
              sanitize: false,
            });
            // const clickEventYes = function () {
            //     popOver.current?.hide()
            //     // popOver.current?.dispose()
            //     setCurrentNumber(c => c + 1)
            // }
            el.addEventListener("shown.bs.popover", () => {
              // const btnYes = document.getElementById("popover-next")
              const btnNo = document.getElementById("popover-cancel");
              // console.log(btn)
              // if (btnYes != null) {
              //     btnYes.addEventListener("click", clickEventYes)
              // }
              if (btnNo != null) {
                btnNo.addEventListener("click", function () {
                  popOver.current?.hide();
                  // btnYes.removeEventListener("click", clickEventYes)
                });
              }
              // do something...
            });
            el.click();
            // el.addEventListener('hidden.bs.tooltip', () => {
            //     const btnYes = document.getElementById("popover-next")
            //     // console.log(btn)
            //     if (btnYes != null) {
            //         btnYes.removeEventListener("click", clickEventYes)
            //     }

            //     // do something...
            // })
          }
        }
      }
      popOver.current.show();
    }
  };

  return (
    <>
      <ModalDetailTrivia
        show={isShownModalDetailTrivia}
        handleClose={() => {
          setIsShownModalDetailTrivia(false);
        }}
        onOk={startTrivia}
        onCancel={() => {
          router.replace({
            pathname: "/auth/user-profile",
            query: { menu: "trivia" },
          });
        }}
        currentNumber={currentNumber}
      >
        <h3 className="fw-bold mt-5 mb-6 text-center">{namaTest}</h3>
        <table className="font-size-12 mx-auto table border">
          <tbody>
            <tr>
              <td>Total Pertanyaan</td>
              <td>:</td>
              <td className="line-clamp-2 mb-0">
                {" "}
                {detailTrivia.jumlahSoal} Pertanyaan
              </td>
            </tr>
            <tr>
              <td>Estimasi Pengerjaan</td>
              <td>:</td>
              <td className="line-clamp-2 mb-0">
                {" "}
                <DurasiPengerjaan
                  duration={detailTrivia.totalDurasi}
                ></DurasiPengerjaan>
              </td>
            </tr>
          </tbody>
        </table>
      </ModalDetailTrivia>

      <ModalLoading
        show={isLoading}
        handleClose={() => {
          // NotificationHelper.Success({ message: "berhasil submit" })
          setIsLoading(false);
        }}
        loadingMessage={loadingMessage}
      ></ModalLoading>
      <ModalLoading
        id="modalLoadingSubmitTriv2"
        show={isLoadingSubmit}
        handleClose={() => {
          // NotificationHelper.Success({ message: "berhasil submit" })
          setIsLoadingSubmit(false);
        }}
        loadingMessage={loadingMessage}
      ></ModalLoading>

      <ModalWaktuHabis
        show={showModalWaktuHabis}
        onClose={() => {
          // console.log("okoko")
          setWasSubmitted(true);
          setShowModalWaktuHabis(false);
          // submitJawban()
        }}
      ></ModalWaktuHabis>

      <ModalPreSoal
        show={isShownModalPreSoal}
        {...dataPreSoal}
        handleClose={() => {
          setShownModalPreSoal(false);
          // console.log(dataPreSoal)
          setWaktu({ ...dataPreSoal });
        }}
      ></ModalPreSoal>

      {isLoading || isShownModalDetailTrivia || isShownModalPreSoal ? (
        <>
          <div className="container py-8">
            <Skeleton style={{ height: 400 }}></Skeleton>
          </div>
        </>
      ) : (
        <>
          <div
            id="headerMobileSubstansi"
            ref={headerMobileSubstansi}
            style={
              isHeaderSticy
                ? {
                    position: "fixed",
                    top: 0,
                    width: "100%",
                    zIndex: 1044,
                  }
                : {}
            }
            className="bg-white shadow "
          >
            <div className="row align-items-end px-4 py-4 d-block d-md-none">
              <div className="col-md mb-md-0 ">
                <h5 className="fw-semi-bold text-muted mb-0">
                  Trivia {namaTest}
                </h5>
                <span className="text-dark font-size-sm fw-normal line-clamp-1">
                  {namaPelatihan}
                </span>
              </div>
              <div className="col-md-auto">
                <span className="text-dark font-size-sm fw-normal line-clamp-1">
                  {namaAkademi}
                </span>
              </div>
              <div className="col-12 text-end">
                <Timer {...waktu} textOnly={true}></Timer>
              </div>
            </div>
          </div>
          <div
            id="fieldPengerjaanSubstansi"
            className={`container my-8 ${isHeaderSticy ? "py-8" : ""} py-md-0 `}
          >
            <div className="row">
              <div className="col-lg-12">
                <div className="row">
                  <div className="col-lg-9 mb-5">
                    <div className="card border rounded-5 px-6 py-5">
                      <div className="d-block">
                        <div className="pb-5">
                          <div className="row align-items-end pb-5 mb-3">
                            <div className="col-md mb-md-0 ">
                              <h3 className="fw-bold text-dark mb-0">
                                {namaTest}
                              </h3>
                              <p className="text-muted font-size-14 fw-bold mb-0">
                                {namaPelatihan}
                              </p>
                              <p className="text-muted font-size-14">
                                {namaAkademi}
                              </p>
                            </div>
                          </div>
                          <hr className="d-none d-md-block" />
                          <div className="d-flex align-items-center">
                            <div className="col-12 pt-5">
                              <h6 className="mb-0 fw-semi-bold text-muted">
                                Soal {currentNumber} dari {listSoal.length}
                              </h6>
                              <FormTrivia
                                {...listSoal[currentNumber - 1]}
                                onChange={onJawab}
                              ></FormTrivia>
                            </div>
                          </div>
                        </div>
                        <div className="row gx-2 mt-8">
                          <div className="col-auto align-self-start">
                            {/* {currentNumber != 1 && <a onClick={() => { setCurrentNumber(c => { return c - 1 }) }} className="btn btn-outline-secondary btn-sm px-3 mb-3 font-size-14">
                                                        <i className="fa fa-chevron-left"></i><span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">Sebelumnya</span>
                                                    </a>} */}
                          </div>
                          <div className="col-auto align-self-start">
                            {currentNumber != listSoal.length ? (
                              <a
                                id="trivia-next-button"
                                className="btn btn-primary rounded-pill btn-sm px-3 mb-3 font-size-12"
                                onClick={nextSoal}
                              >
                                <i className="fa fa-chevron-right"></i>
                                <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                                  Selanjutnya
                                </span>
                              </a>
                            ) : (
                              <>
                                <a
                                  onClick={() => {
                                    // updateStatPengerjaan()
                                    // setShowModalResumePengerjaan(true)
                                    if (!isSubmitDisabled)
                                      setShowModalWaktuHabis(true);
                                  }}
                                  className={`btn btn-primary rounded-pill btn-sm px-3 mb-3 font-size-12 ${
                                    isSubmitDisabled ? "disabled" : ""
                                  }`}
                                >
                                  <i className="fa fa-paper-plane me-1"></i>
                                  <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                                    Submit Trivia
                                  </span>
                                </a>
                              </>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-3 border-left">
                    <Timer {...waktu} onTimesUp={onTimesUp}></Timer>
                    <div className="card border rounded-5 px-4 py-5 mb-5">
                      <h5 className="fw-bold text-muted">Soal</h5>
                      <div className="col">
                        {listSoal.map((soal, i) => {
                          let n = i + 1;
                          return (
                            <a
                              key={i}
                              // className="cursor-pointer"
                              // onClick={() => {
                              //     setCurrentNumber(n)
                              // }}
                            >
                              <span
                                className={`badge ${
                                  n == currentNumber
                                    ? "bg-green"
                                    : soal.seen
                                      ? soal.answer_key
                                        ? "bg-blue"
                                        : "bg-red"
                                      : "badge-primary-soft"
                                } w-40p p-2 mb-2 mx-1`}
                              >
                                {n}
                              </span>
                            </a>
                          );
                        })}
                      </div>
                      <hr />
                      <ul className="list mb-0">
                        <li className="list-item">
                          <span className="badge bg-blue">Sudah dijawab</span>
                        </li>
                        <li className="list-item">
                          <span className="badge bg-red">Belum dijawab</span>
                        </li>
                        <li className="list-item">
                          <span className="badge bg-green">Soal Saat ini</span>
                        </li>
                        <li className="list-item">
                          <span className="badge badge-primary-soft">
                            Belum dikerjakan
                          </span>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
};

// Page Utama
function Trivia(props) {
  const [showRenderer, setShowRenderer] = useState(false);
  const [isSelesai, setIsSelesai] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [namaTest, setNamaTest] = useState("");
  const [namaPelatihan, setNamaPelatihan] = useState("");
  const [namaAkademi, setNamaAkademi] = useState("");
  const [jumlahHarusJawab, setJumlahHarusJawab] = useState(0);

  const router = useRouter();
  const {
    query: { idTrivia, idPelatihan },
  } = router;
  useEffect(async () => {
    if (router.isReady) {
      if (idTrivia && idPelatihan) {
        await getPelatihanDetail(idPelatihan);
        await checkStatus(idPelatihan, idTrivia);
      }
    }
  }, [router.isReady, idTrivia, idPelatihan]);

  const getPelatihanDetail = async (idPelatihan) => {
    try {
      const resp = await PortalService.pelatihanDetail(idPelatihan);
      if (resp.data.success) {
        const { nama_pelatihan, nama_akademi } = resp.data.result.data;
        setNamaPelatihan(nama_pelatihan);
        setNamaAkademi(nama_akademi);
      }
    } catch (error) {}
  };

  const checkStatus = async (idPelatihan, idTrivia) => {
    try {
      // sementara cek pake list
      const resp = await ProfilService.profilStatTrivia(idPelatihan, idTrivia);
      // console.log(resp)
      if (resp.data.success) {
        // cek available pengerjaan
        if (
          resp?.data?.result?.data[0]?.status_peserta_trivia.toLowerCase() ==
            "false" ||
          resp?.data?.result?.data[0]?.status_peserta_trivia?.toLowerCase() ==
            false
        ) {
          NotificationHelper.Failed({
            message: "Anda belum bisa mengerjakan trivia ini",
            onClose: () => {
              router.push("/auth/user-profile");
            },
            okText: "Kembali",
          });
          return;
        }
        if (resp.data.result?.data == null) {
          setIsError(true);
          NotificationHelper.Failed({
            message: "Trivia tidak ditemukan",
            onClose: () => {
              router.push("/auth/user-profile?menu=trivia");
            },
          });
        } else {
          setNamaTest(resp.data.result?.data[0]?.trivia_nama);
          setJumlahHarusJawab(resp.data.result?.data[0]?.questions_to_share);
          switch (resp.data.result?.data[0]?.status) {
            case "sudah mengerjakan":
              if (!forceTest) {
                setIsSelesai(true);
                setNamaTest(resp.data.result?.data[0]?.trivia_nama);
              }
              break;
            case "sedang mengerjakan":
              setShowRenderer(true);
            case "belum mengerjakan":
              break;
            default:
              setIsError(true);
              NotificationHelper.Failed({
                message: "Terjadi kesalahan",
                onClose: () => {
                  router.push("/auth/user-profile?menu=trivia");
                },
              });
              break;
          }
          setIsLoading(false);
        }
      } else {
        throw new Error("terjadi kesalahan");
      }
      // console.log("jalan")
    } catch (error) {
      NotificationHelper.Failed({ message: error.message });
    }
  };

  return (
    <>
      <div>
        {isError ? (
          <BelumAdaData></BelumAdaData>
        ) : isLoading ? (
          <div className="container py-8">
            <Skeleton className="h-400p"></Skeleton>
          </div>
        ) : isSelesai ? (
          <Selesai
            namaPelatihan={namaPelatihan}
            namaTest={namaTest}
            namaAkademi={namaAkademi}
            idTrivia={idTrivia}
            idPelatihan={idPelatihan}
          ></Selesai>
        ) : showRenderer ? (
          <FormRenderer
            namaTest={namaTest}
            namaAkademi={namaAkademi}
            namaPelatihan={namaPelatihan}
            showRenderer={showRenderer}
            idTrivia={idTrivia}
            idPelatihan={idPelatihan}
            jumlahHarusDijawab={jumlahHarusJawab}
          ></FormRenderer>
        ) : (
          <Guidline
            setShowRenderer={setShowRenderer}
            namaTest={namaTest}
          ></Guidline>
        )}
      </div>
    </>
  );
}

// TestSubstansi.bodyBackground = "bg-white"
export default Trivia;
