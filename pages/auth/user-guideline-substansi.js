import React from "react";
import Link from "next/link";

export default function UserGuidelineSubstansi() {
  return (
    <div className="bg-gray-100">
      <section>
        <div className="container">
          <div className="row my-8">
            <div className="col-lg-12">
              <div className="card border rounded-5 border-0 border-lg border-xl px-6 py-8">
                <div className="row">
                  <div className="col-lg-5 mb-lg-0">
                    <h2 className="text-center mb-8">Panduan Tes Substansi</h2>
                    {/* Illustrator */}
                    <img
                      src="/assets/img/illustrations/illustration-11.svg"
                      alt="..."
                      className="img-fluid pt-5"
                    />
                  </div>
                  <div className="col-lg-7">
                    <div className="d-block rounded bg-info">
                      <div className="pt-6 px-5">
                        <div className="d-flex align-items-center">
                          <div>
                            <h4 className="mb-4">Harap dibaca :</h4>
                            <p>
                              Sebelum mengerjakan, harap perhatikan dan lakukan
                              hal-hal berikut
                            </p>
                            <ul
                              className="list-style-v2 list-unstyled"
                              style={{ textAlign: "left" }}
                            >
                              <li>
                                Alokasi waktu yang diberikan untuk mengerjakan
                                Tes Substansi sesuai dengan masing-masing tema
                                pelatihan. Informasi tersebut dapat di akses
                                pada dashboard Tes Substansi.
                              </li>
                              <li>
                                Peserta wajib menjawab seluruh soal Tes
                                Substansi dan jumlah soal sesuai dengan
                                masing-masing tema pelatihan. Tidak ada nilai
                                negatif untuk jawaban yang salah.
                              </li>
                              <li>
                                Setelah Tes Substansi dimulai, waktu tes tidak
                                dapat diberhentikan dan tes tidak dapat diulang.
                                Setelah waktu habis, halaman soal akan tertutup
                                secara otomatis.
                              </li>
                              <li>
                                Skor untuk soal yang sudah dijawab tetap
                                terhitung walaupun peserta belum menekan tombol
                                submit atau peserta mengalami force majeure.
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div className="form-row place-order p-5">
                        <Link href="/user-test-subvit">
                          <a className="btn btn-blue btn-block">
                            <i className="fa fa-play me-2" />
                            MULAI TES SUBSTANSI
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
