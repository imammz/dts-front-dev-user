import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import Moment from "react-moment";
import "moment/locale/id";
import PortalHelper from "../../src/helper/PortalHelper";
import { useRouter } from "next/router";

import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import PortalService from "../../services/PortalService";
import ShareModal from "../../components/Modal/ShareModal";

function Artikel() {
  const [dataArtikel, setdataArtikel] = useState(null);
  const [loading, setLoading] = useState(false);
  const [skeletons, setSkeletons] = useState([1, 2, 3, 4]);
  const router = useRouter();
  const [clientHeight, setClientHeight] = useState(null);
  const parentContainer = useRef(null);
  const [showShareModal, setShowShareModal] = useState(false);

  useEffect(async () => {
    try {
      setLoading(true);
      if (router.isReady) {
        const artikel = PortalService.artikelFindById(router.query.id);

        const resps = await axios.all([artikel]);
        setdataArtikel(resps[0].data.result.Data);
        setLoading(false);
      }
    } catch (err) {
      console.error(err);
    }
  }, [router.isReady]);

  useEffect(() => {
    setTimeout(() => {
      if (parentContainer.current && parentContainer.current.clientHeight) {
        setClientHeight(parentContainer.current.clientHeight);
      }
    }, 500);
  }, [dataArtikel]);

  const handleShareClose = () => {
    setShowShareModal(false);
  };

  return (
    <>
      <ShareModal handleClose={handleShareClose} show={showShareModal} />
      <div className="row">
        {loading && !dataArtikel && (
          <div className="col-md-7 col-lg-8 col-xl-9 mb-5 mb-md-0">
            <Skeleton className="h-260p mb-6" />
            <div className="row mb-6 mb-md-10 align-items-center">
              <div className="col-md-8">
                <Skeleton height={20} width={40} />
                <Skeleton height={20} />
                {skeletons.map((tag) => (
                  <Skeleton height={20} />
                ))}
              </div>
            </div>
          </div>
        )}

        {!loading && dataArtikel && (
          <div className="col-12" ref={parentContainer}>
            <div className="mb-5">
              <img
                src={PortalHelper.urlPublikasiLogo(dataArtikel.gambar)}
                alt="..."
                className="img-responsive"
              />
            </div>
            <div className="mx-3 mb-5">
              <span className="badge badge-blue font-size-sm fw-normal mb-1 rounded-5">
                Artikel
              </span>
              <h3 className="fw-bolder text-capitalize w-96 mb-2">
                {dataArtikel.judul_artikel}
              </h3>
              <ul className="nav mx-n3 mb-4 d-flex">
                <li className="nav-item px-3 mb-2">
                  <span className="font-size-14 text-muted">
                    <Moment format="DD MMMM YYYY" locale="id">
                      {dataArtikel.created_at}
                    </Moment>
                  </span>
                </li>
                <li className="nav-item px-3 mb-2">
                  <span className="font-size-14 text-muted">
                    {" "}
                    {`${dataArtikel.total_views} kali dibaca`}
                  </span>
                </li>
              </ul>
              <div
                className="post-body"
                dangerouslySetInnerHTML={{ __html: dataArtikel.isi_artikel }}
              ></div>
            </div>
          </div>
        )}
        <div className="col-md-5 col-lg-4 col-xl-3"></div>
      </div>
      {clientHeight && (
        <span id="windowHeight" hidden>
          {clientHeight}
        </span>
      )}
    </>
  );
}

Artikel.bodyBackground = "bg-white";
export default Artikel;
