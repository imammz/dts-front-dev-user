import react, { useState, useEffect, useRef } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import PortalHelper from "../../src/helper/PortalHelper";
import ProfilService from "../../services/ProfileService";
import PelatihanService from "../../services/PelatihanService";
import ZonasiModal from "../../components/Modal/ZonasiModal";
import ShareModal from "../../components/Modal/ShareModal";

/** Libraries */
import Moment from "react-moment";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import ValidationMessage from "../../src/validation-message";
import SimpleDateRange from "../../components/SimpleDateRange";
import PortalService from "../../services/PortalService";
import ButtonWithLoading from "../../components/ButtonWithLoading";
import routeSegment from "../../src/route-segment";
import moment from "moment";
import NotificationHelper from "../../src/helper/NotificationHelper";
import GeneralService from "../../services/GeneralService";
import eventBus from "../../components/EventBus";
import Select from "react-select";
import React from "react";
import axios from "axios";

function daftarPelatihan({ showLoadingModal, loginRefresh }) {
  const router = useRouter();
  const [Loading, setLoading] = useState(false);
  const { pelatihanID, token } = router.query;

  const [PelatihanDetail, setPelatihanDetail] = useState([]);
  const [PelatihanByUser, setPelatihanByUser] = useState(null);
  const [showZonasiModal, setShowZonasiModal] = useState(false);
  const [PelatihanForm, setPelatihanForm] = useState([]);
  const [timeDiff, setTimeDiff] = useState({});
  const [PelatihanFormConfiguration, setPelatihanFormConfiguration] = useState({
    mode: "onChange",
  });
  const [showShareModal, setShowShareModal] = useState(false);
  const [PageWizard, setPageWizard] = useState(1);

  const [user, setUser] = useState(PortalHelper.getUserData());
  const [loadingSubmit, setLoadingSubmit] = useState(false);
  const [timeInterval, setTimeInterval] = useState(null);
  const [zonasi, setZonasi] = useState([]);
  const [duration, setDuration] = useState("");
  const redirectDelay = 30;
  const [isReady, setReady] = useState(false);

  useEffect(() => {
    if (loginRefresh.is_login) {
      PortalHelper.saveUserToken(loginRefresh.token, true);
      PortalHelper.saveUserData(loginRefresh.user, true);
    } else {
      PortalHelper.saveUserToken(null, true);
      PortalHelper.saveUserData(null, true);
    }
  }, [loginRefresh]);

  useEffect(async () => {
    if (timeInterval) {
      clearInterval(timeInterval);
    }

    setLoading(false);
  }, [router, router.pathname, token]);

  useEffect(() => {
    const removeInterval = () => {
      if (timeInterval) {
        console.log("dalem clear interval");
        clearInterval(timeInterval);
      }
    };

    router.events.on("routeChangeStart", removeInterval);

    return () => {
      if (timeInterval) {
        clearInterval(timeInterval);
      }
      router.events.off("routeChangeStart", removeInterval);
    };
  }, []);

  const countDown = (isReset = false) => {
    if (isReset) {
      setDuration("");
      return;
    }
    const upperBounddate = moment().add(redirectDelay, "seconds");
    setDuration(
      moment.utc(moment(upperBounddate).diff(moment())).format("mm:ss"),
    );
    const localInterval = setInterval(() => {
      duration = moment.duration(
        moment(upperBounddate).diff(moment()),
        "milliseconds",
      );
      setDuration(moment.utc(duration.asMilliseconds()).format("mm:ss"));
      if (duration < 1) {
        setDuration(moment.utc(0).format("mm:ss"));
        clearInterval(localInterval);
        if (PelatihanDetail && PelatihanDetail?.substansi_id) {
          if (
            router &&
            window?.location?.pathname.match(routeSegment.PELATIHAN_DAFTAR)
          ) {
            router.push(
              `/mobile/test-substansi?idPelatihan=${PelatihanDetail?.id}&idTest=${PelatihanDetail?.substansi_id}&token=${token}`,
            );
          }
        }
        return;
      }
    }, 1000);
    setTimeInterval(localInterval);
  };

  const checkPelatihanByUser = async () => {
    /** Cek apakah user ini sudah mendaftar atau belum */
    const resPelatihanByUser = await ProfilService.pelatihan(pelatihanID);
    if (resPelatihanByUser.data.success) {
      // if (resPelatihanByUser.data.result.data) {
      setPelatihanByUser(resPelatihanByUser.data.result.data);
      if (
        resPelatihanByUser.data.result.data &&
        resPelatihanByUser.data.result.data.status != "14"
      ) {
        try {
          if (
            resPelatihanByUser.data.result.data.alur_pendaftaran ==
            "Test Substansi - Administrasi"
          ) {
            // countDown();
            if (PelatihanDetail.substansi_id) {
              router.push(
                `/mobile/test-substansi?idPelatihan=${PelatihanDetail?.id}&idTest=${PelatihanDetail?.substansi_id}&token=${token}`,
              );
            }
          }
          let resp = await PortalService.refreshUserData();
          if (resp.data?.success) {
            PortalHelper.refreshUserCookies(resp.data.result?.data?.user);
            setUser(PortalHelper.getUserData());
          }
        } catch (err) {
          console.log(err);
        }
      }
    }
  };

  const getPelatihanForm = async () => {
    const resPelatihanPendaftaran =
      await PelatihanService.pendaftaran(pelatihanID);
    const resp = await GeneralService.getServerTime(true);

    if (
      resPelatihanPendaftaran.success &&
      resPelatihanPendaftaran.result == "Error" &&
      resPelatihanPendaftaran.message == "Not Validate"
    ) {
      NotificationHelper.Failed({
        message: "Pendaftaran gagal, kesalahan validasi user",
        onClose: () => {
          router.push("/jadwal-pendaftaran");
        },
      });
    }
    setTimeDiff(resp);
    const pelatihanDetail = {
      ...resPelatihanPendaftaran.data.result.info,
      diff: resp.diff,
      current_server_time: resp.serverTime,
    };
    setPelatihanDetail(pelatihanDetail);
    setZonasi(resPelatihanPendaftaran.data.result.pelatihan_provinsi);
    setPelatihanForm(resPelatihanPendaftaran.data.result.form);

    let formValidation = {};

    let regexValidation = {
      email:
        /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
      url: /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/,
      number: /^[0-9]+$/,
      date: /^(0[1-9]|1\d|2\d|3[01])\/(0[1-9]|1[0-2])\/(19|20)\d{2}$/,
      datepicker: /^(0[1-9]|1\d|2\d|3[01])\/(0[1-9]|1[0-2])\/(19|20)\d{2}$/,
    };

    resPelatihanPendaftaran.data.result.form.forEach((form) => {
      /** Validation - Declare */
      if (form.element == "text-email") {
        formValidation[form.file_name] = Yup.string()
          .matches(
            regexValidation.email,
            ValidationMessage.email.replace("__field", form.name),
          )
          .nullable(true)
          .transform((value, originalValue) =>
            originalValue.trim() === "" ? null : value,
          );
      } else if (form.element == "text-numeric") {
        formValidation[form.file_name] = Yup.string()
          .matches(
            regexValidation.number,
            ValidationMessage.number.replace("__field", form.name),
          )
          .nullable(true)
          .transform((value, originalValue) =>
            originalValue.trim() === "" ? null : value,
          );
      } else if (form.element == "text-URL") {
        formValidation[form.file_name] = Yup.string()
          .matches(
            regexValidation.url,
            ValidationMessage.url.replace("__field", form.name),
          )
          .nullable(true)
          .transform((value, originalValue) =>
            originalValue.trim() === "" ? null : value,
          );
      } else if (form.element == "datepicker") {
        formValidation[form.file_name] = Yup.string()
          .matches(
            regexValidation.datepicker,
            ValidationMessage.datepicker.replace("__field", form.name),
          )
          .nullable(true)
          .transform((value, originalValue) =>
            originalValue.trim() === "" ? null : value,
          );
      } else if (form.element == "fileimage") {
        formValidation[form.file_name] = Yup.mixed()
          .test(
            "FILE_FORMAT",
            ValidationMessage.image.replace("__field", form.name),
            (value) =>
              !value[0] ||
              (value[0] &&
                ["image/jpg", "image/jpeg", "image/png"].includes(
                  value[0]?.type,
                )),
          )
          .nullable(true);
      } else if (form.element == "file-doc") {
        formValidation[form.file_name] = Yup.mixed()
          .test(
            "FILE_FORMAT",
            ValidationMessage.pdf.replace("__field", form.name),
            (value) =>
              !value[0] ||
              (value[0] && ["application/pdf"].includes(value[0]?.type)),
          )
          .nullable(true);
      } else if (form.element == "checkbox") {
        formValidation[form.file_name] = Yup.array().nullable(true);
      } else if (form.element == "select") {
        formValidation[form.file_name] = Yup.object().shape({
          label: Yup.string().required(
            ValidationMessage.required.replace("__field", form.name),
          ),
          value: Yup.string().required(
            ValidationMessage.required.replace("__field", form.name),
          ),
        });
      } else {
        formValidation[form.file_name] = Yup.string().nullable(true);
      }

      let requiredFileType = ["fileimage", "file-doc"];
      /** Validation - Conditional */
      if (form.required && form.element == "checkbox") {
        // formValidation[form.file_name] = formValidation[form.file_name].oneOf(form.data_option.split(';'), ValidationMessage.required.replace('__field', form.name))
        formValidation[form.file_name] = formValidation[form.file_name].test(
          "checkbox-validation",
          ValidationMessage.required.replace("__field", form.name),
          (value) => {
            if (value != null) {
              let isValid = true;
              const optionList = form.data_option.split(";");
              Object.keys(value).forEach((k) => {
                if (!optionList.includes(value[k])) {
                  isValid = false;
                }
              });
              return isValid;
            } else {
              return false;
            }
          },
        );
      } else if (form.required && !requiredFileType.includes(form.element)) {
        formValidation[form.file_name] = formValidation[
          form.file_name
        ].required(ValidationMessage.required.replace("__field", form.name));
      } else if (form.required && requiredFileType.includes(form.element)) {
        formValidation[form.file_name] = formValidation[form.file_name].test(
          "name",
          ValidationMessage.required.replace("__field", form.name),
          (value) => value[0] && value[0].size > 0,
        );
      }

      if (form.min) {
        formValidation[form.file_name] = formValidation[form.file_name].min(
          form.min,
          ValidationMessage.minLength
            .replace("__field", form.name)
            .replace("__length", form.min),
        );
      }

      if (form.max) {
        formValidation[form.file_name] = formValidation[form.file_name].max(
          form.max,
          ValidationMessage.maxLength
            .replace("__field", form.name)
            .replace("__length", form.max),
        );
      }
    });

    if (formValidation) {
      setPelatihanFormConfiguration({
        mode: "onBlur",
        resolver: yupResolver(Yup.object(formValidation)),
      });
    }

    /** Cek apakah user ini sudah mendaftar atau belum */
    await checkPelatihanByUser();
  };

  const handleShareClose = () => {
    setShowShareModal(false);
  };

  /** Handler Form Pendaftaran */
  const {
    register,
    handleSubmit,
    trigger,
    control,
    formState: { errors },
  } = useForm(PelatihanFormConfiguration);

  const onClickPageWizard = async (pageNumber) => {
    if (pageNumber == 2) {
      const result = await trigger();
      // console.log(result);
      if (result) {
        setPageWizard(2);
      }
    } else if (pageNumber == 1) {
      setPageWizard(1);
    }
  };

  const onClickFormKomitmen = async (event) => {
    let checkBoxKomitmen = event.target.checked;
    let buttonModalKonfirmasi = document.getElementById(
      "buttonModalKonfirmasi",
    );
    if (checkBoxKomitmen == true) {
      buttonModalKonfirmasi.classList.remove("disabled");
    } else {
      buttonModalKonfirmasi.classList.add("disabled");
    }
  };

  const onSubmiDaftarPelatihanForm = async (data) => {
    try {
      showLoadingModal(true);
      setLoadingSubmit(true);
      const formData = new FormData();
      for (let key in data) {
        if (data[key] instanceof FileList) {
          if (document.getElementsByName(key)[0]?.files[0]) {
            formData.append(key, document.getElementsByName(key)[0].files[0]);
          }
        } else if (data[key] instanceof Object) {
          if (data[key].value) {
            formData.append(key, data[key].value);
          } else {
            formData.append(key, data[key]);
          }
        } else if (Array.isArray(data[key])) {
          data[key].forEach((item) => {
            formData.append(key, item);
          });
        } else if (data[key]) {
          formData.append(key, data[key]);
        }
      }
      const resDaftarSubmit = await PelatihanService.pendaftaranSubmit(
        pelatihanID,
        formData,
      );
      if (
        resDaftarSubmit.data.success == false ||
        resDaftarSubmit.data.result == "Error"
      ) {
        NotificationHelper.Failed({
          message: "Tidak dapat mendaftar Pelatihan",
        });
      } else {
        let resp = await PortalService.refreshUserData();
        if (resp.data?.success) {
          PortalHelper.refreshUserCookies(resp.data.result?.data?.user);
          setUser(PortalHelper.getUserData());
          eventBus.dispatch("profil-updated");
        }
      }
      if (
        PelatihanDetail?.alur_pendaftaran == "Test Substansi - Administrasi"
      ) {
        if (PelatihanDetail.substansi_id) {
          // countDown();
          router.push(
            `/mobile/test-substansi?idPelatihan=${PelatihanDetail?.id}&idTest=${PelatihanDetail?.substansi_id}&token=${token}`,
          );
        }
      }
      /** Cek apakah user ini sudah mendaftar atau belum */
      checkPelatihanByUser();
      showLoadingModal(false);
      setLoadingSubmit(false);
    } catch (error) {
      setLoadingSubmit(false);
    }
  };

  useEffect(async () => {
    setLoading(true);
    try {
      if (router.isReady) {
        /** Set Form */
        await getPelatihanForm();
        setLoading(false);
      }
    } catch (error) {
      //
    }
  }, [router.isReady, pelatihanID]);

  return (
    <div>
      <ZonasiModal
        show={showZonasiModal}
        handleClose={() => setShowZonasiModal(false)}
        zonasi={zonasi}
      />
      <ShareModal
        handleClose={handleShareClose}
        show={showShareModal}
        title="Bagikan pelatihan ini!"
      />
      {Loading && (
        <div>
          <div className="container pb-9">
            <div className="row mb-8">
              <div className="col-lg-8 mb-6 mb-lg-0 position-relative">
                <div className="row mt-6">
                  <div className="col-12 col-lg-6">
                    <h6 className="mb-0">
                      <Skeleton />
                    </h6>
                    <h6 className="text-primary">
                      <Skeleton />
                    </h6>
                  </div>
                </div>

                <div className="row mt-6">
                  <div className="col-12 mb-3 px-5">
                    <div className="row align-items-center">
                      <div className="col px-3">
                        <div className="card-body p-0">
                          <h6 className="mb-0 line-clamp-1">
                            <Skeleton />
                          </h6>
                          <p className="mb-0 line-clamp-1">
                            <Skeleton />
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-12 mb-3 px-5">
                    <div className="row align-items-center">
                      <div className="col px-3">
                        <div className="card-body p-0">
                          <h6 className="mb-0 line-clamp-1">
                            <Skeleton />
                          </h6>
                          <p className="mb-0 line-clamp-1">
                            <Skeleton />
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-12 mt-lg-n12">
                <Skeleton className="h-500p"></Skeleton>
              </div>
            </div>
          </div>
        </div>
      )}

      {!Loading &&
        PelatihanByUser &&
        PelatihanByUser.status != "14" &&
        user.lengkapi_pendaftaran.length == 0 && (
          <div className="container my-8">
            <div className="row">
              <div className="col-12">
                <div className="card border rounded-5 border-lg border-xl px-5 py-8">
                  <div className="row">
                    <div className="col-12">
                      <div className="pt-6 px-5 px-lg-3 px-xl-5 text-center">
                        <img
                          src="/assets/img/empty-state/success-data.png"
                          className="img-fluid align-self-center mb-5"
                          style={{ width: "200px" }}
                        />
                        <div className="row">
                          <div className="col-12 px-2">
                            <h3 className="fw-bold text-green mb-0">
                              Pendaftaran berhasil
                            </h3>
                            <div className="mb-5">
                              <p className="font-size-14 mb-0">
                                Untuk tahapan dan proses selanjutnya, silahkan
                                menunggu informasi/pengumuman lebih lanjut dari
                                panitia pelatihan melalui
                                notifikasi/Email/SMS/WA.
                              </p>
                            </div>
                          </div>
                          {PelatihanDetail.alur_pendaftaran ==
                            "Test Substansi - Administrasi" &&
                            PelatihanDetail?.substansi_id &&
                            duration && (
                              <div className="col-12 px-2">
                                <p className="color-silver mt-3 text-center">
                                  {`Pindah menuju halaman Test Substansi dalam`}
                                  <div>
                                    <span className="fw-bolder">
                                      {duration}
                                    </span>{" "}
                                    atau{" "}
                                    <Link
                                      className="fw-bolder"
                                      href={`/mobile/test-substansi?idPelatihan=${PelatihanDetail?.id}&idTest=${PelatihanDetail?.substansi_id}&token=${token}`}
                                    >
                                      <a>Kerjakan Test Substansi Sekarang</a>
                                    </Link>
                                  </div>
                                </p>
                              </div>
                            )}
                          {PelatihanDetail.alur_pendaftaran ==
                            "Test Substansi - Administrasi" &&
                            (!PelatihanDetail?.substansi_id ||
                              PelatihanDetail.substansi_id == "") && (
                              <div className="alert bg-highlight rounded">
                                <div className="col-lg-12 fv-row">
                                  <h5 className="text-primary-alert fs-5 fw-bolder">
                                    Perhatian
                                  </h5>
                                  <p className="font-size-14 text-primary-alert m-0">
                                    <strong>Test Substansi</strong> saat ini
                                    belum tersedia pantau terus Notifikasimu
                                    untuk segera mendapatkan update terkait
                                    ketersediaan tes ya.
                                  </p>
                                </div>
                              </div>
                            )}
                        </div>
                      </div>
                    </div>
                    {/*<div className="col-lg mb-5">
                          <div className="card rounded-5 border p-3">
                            <div className="row gx-0">
                              <div
                                  className="col-auto p-3 d-block"
                                  style={{ maxWidth: "80px" }}
                              >
                                <img
                                    className="img-fluid shadow-light-lg"
                                    src={
                                      PelatihanDetail?.metode_pelaksanaan == "Swakelola"
                                          ? "/assets/img/kominfo.png"
                                          : PortalHelper.urlMitraLogo(
                                              PelatihanDetail?.logo
                                          )
                                    }
                                    alt="Mitra"
                                />
                              </div>
                              <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
                                <div className="card-body p-3">
                                  <div className="mb-0">
                              <span className="text-dark font-size-sm">
                                {PelatihanDetail?.nama_akademi}
                              </span>
                                  </div>
                                  <h5 className="fw-bolder mb-3">
                                    {PelatihanDetail?.nama_pelatihan}
                                  </h5>
                                  <ul className="nav mx-n3 d-block d-md-flex">
                                    <li className="nav-item px-3 mb-2">
                                      <div className="d-flex align-items-center">
                                        <div className="me-1 d-flex text-secondary icon-uxs">
                                          <i className="fa fa-calendar"></i>
                                        </div>
                                        <div className="font-size-sm">
                                          <SimpleDateRange
                                              startDate={
                                                PelatihanDetail?.pendaftaran_mulai
                                              }
                                              endDate={
                                                PelatihanDetail?.pendaftaran_selesai
                                              }
                                              diff={timeDiff.diff}
                                          ></SimpleDateRange>
                                        </div>
                                      </div>
                                    </li>
                                    <li className="nav-item px-3 mb-2">
                                      <div className="d-flex align-items-center">
                                        <div className="me-1 d-flex text-secondary icon-uxs">
                                          <i className="fa fa-map-marker-alt"></i>
                                        </div>
                                        <div className="font-size-sm">
                                          {PelatihanDetail?.metode_pelatihan ==
                                          "Online"
                                              ? "Online"
                                              : "" + PelatihanDetail?.lokasi_pelatihan}
                                        </div>
                                      </div>
                                    </li>
                                    <li className="nav-item px-3 mb-2">
                                      <div className="d-flex align-items-center">
                                        <div className="me-1 d-flex text-secondary icon-uxs">
                                          <i className="fa fa-microphone-alt"></i>
                                        </div>
                                        <div className="font-size-sm">
                                          {PelatihanDetail?.metode_pelaksanaan ==
                                          "Mitra"
                                              ? PelatihanDetail?.nama_mitra
                                              : PelatihanDetail?.nama_penyelenggara
                                                  ? PelatihanDetail?.nama_penyelenggara
                                                  : "Kominfo"}
                                        </div>
                                      </div>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>*/}

                    {/*
                            <div className="form-row place-order text-center mt-5 mb-8">
                            <button
                                // href={process.env.STORAGE_URL + PelatihanByUser.file_path}
                                // target="_blank"
                                className="btn btn-blue rounded-3"
                                onClick={() => {
                                  PortalHelper.axiosDownloadFile(
                                      process.env.STORAGE_URL + PelatihanByUser.file_path,
                                      `Bukti Pendaftaran_${user.nama}_${PelatihanDetail.nama_pelatihan}.pdf`
                                  );
                                }}
                            >
                              DOWNLOAD BUKTI PENDAFTARAN
                            </button>
                          </div>*/}
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}

      {/* lengkapi_pendaftaran prop untuk cek user daftar via import  */}
      {!Loading &&
        (!PelatihanByUser ||
          PelatihanByUser.status == 14 ||
          pelatihanID == user.lengkapi_pendaftaran[0]?.pelatian_id) && (
          <div>
            <div
              className="modal fade"
              id="modalFormSubmit"
              tabindex="-1"
              role="dialog"
              aria-labelledby="modalFormSubmitTitle"
              aria-hidden="true"
            >
              <div
                className="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg"
                role="document"
              >
                <div className="modal-content">
                  <div className="modal-body">
                    <button
                      type="button"
                      className="close"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                      id="closeButtonFormKomitmen"
                      style={{ display: "none" }}
                    >
                      <i className="fa fa-times-circle fa-2x"></i>
                    </button>
                    <h2
                      className="fw-bold text-center mb-3"
                      id="modalExampleTitle"
                    >
                      Konfirmasi
                    </h2>
                    <p className="text-center font-size-14">
                      Apakah seluruh isian data pendaftaran telah benar?
                      <br />
                      Data yang telah disubmit tidak dapat diubah kembali
                    </p>
                    <div className="row">
                      <div className="col-6">
                        <button
                          type="submit"
                          className="btn btn-outline-secondary font-size-14 rounded-3 btn-block mt-3"
                          data-bs-dismiss="modal"
                          aria-label="Close"
                        >
                          BATAL
                        </button>
                      </div>
                      <div className="col-6">
                        <ButtonWithLoading
                          loading={loadingSubmit}
                          type="submit"
                          className="btn btn-blue rounded-3 font-size-14 btn-block mt-3"
                          form="submitPendaftaran"
                        >
                          DAFTAR
                        </ButtonWithLoading>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <header className="py-6 z-index-0">
              <div className="container mb-0">
                <div className="row">
                  <div className="col-lg-9 mb-0 position-relative">
                    <div className="mb-0 pt-5 me-6">
                      <h6 className="text-muted font-size-14 mb-0">
                        {PelatihanDetail.namasub_akademi} (
                        {PelatihanDetail.nama_akademi}) -{" "}
                        {PelatihanDetail.nama_tema}
                      </h6>
                    </div>
                    <h3 className="fw-bold me-xl-14 mb-0">
                      {PelatihanDetail?.nama_pelatihan}
                    </h3>
                  </div>
                </div>
              </div>
            </header>

            <div className="container pb-5">
              <div className="row">
                <div className="col-lg-12 mb-lg-0 position-relative">
                  <div className="row">
                    {PelatihanDetail?.metode_pelaksanaan == "Mitra" && (
                      <div className="col-md-6 mb-6 px-5">
                        <div className="card icon-category icon-category-sm">
                          <div className="row align-items-center">
                            <div className="col-auto px-3">
                              <div className="avatar avatar-custom d-inline">
                                <img
                                  src={
                                    PelatihanDetail.metode_pelaksanaan ==
                                    "Swakelola"
                                      ? "/assets/img/kominfo.png"
                                      : PortalHelper.urlMitraLogo(
                                          PelatihanDetail.logo,
                                        )
                                  }
                                  alt="..."
                                  className="avatar-img avatar-md img-fluid"
                                  style={{ objectFit: "contain" }}
                                />
                              </div>
                            </div>

                            <div className="col px-3">
                              <div className="card-body p-0">
                                <h6 className="mb-0 text-muted font-size-14">
                                  Mitra
                                </h6>
                                <p className="mb-0 text-dark font-size-14">
                                  {PelatihanDetail.nama_mitra}
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                    <div className="col-md-6 px-5">
                      <div className="card icon-category icon-category-sm">
                        <div className="row align-items-center">
                          <div className="col-auto px-3">
                            <div className="avatar avatar-custom d-inline">
                              <img
                                src="/assets/img/kominfo.png"
                                alt="{ PelatihanDetail.nama_penyelenggara }"
                                className="avatar-img avatar-md img-fluid"
                              />
                            </div>
                          </div>

                          <div className="col px-3">
                            <div className="card-body p-0">
                              <h6 className="mb-0 text-muted font-size-14">
                                Penyelenggara
                              </h6>
                              <p className="mb-0 text-dark font-size-14">
                                {PelatihanDetail.nama_penyelenggara}
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <form
                    className="my-5"
                    onSubmit={handleSubmit(onSubmiDaftarPelatihanForm)}
                    id="submitPendaftaran"
                  >
                    <div
                      className="bg-gray rounded-5 p-6 my-5"
                      style={{ display: PageWizard == 1 ? "block" : "none" }}
                    >
                      {PelatihanDetail.alur_pendaftaran ==
                        "Test Substansi - Administrasi" && (
                        <div className="alert bg-highlight rounded">
                          <div className="col-lg-12 fv-row">
                            <h5 className="font-size-16 text-primary-alert fs-5 fw-bolder">
                              Perhatian
                            </h5>
                            <p className="font-size-14 text-primary-alert m-0">
                              Setelah melengkapi form pendaftaran, kamu akan
                              diarahkan untuk mengerjakan{" "}
                              <strong>Test Substansi.</strong>
                            </p>
                          </div>
                        </div>
                      )}
                      <h className="font-size-16 text-dark fw-bold mb-1">
                        Form Pendaftaran Pelatihan
                      </h>
                      <p className="font-size-14 mb-3 text-capitalize">
                        Mohon lengkapi form berikut dengan benar sebagai
                        persyaratan untuk dapat mengikuti pelatihan
                      </p>
                      {PelatihanForm.map((Form) => (
                        <>
                          {(Form.element == "text" ||
                            Form.element == "text-URL" ||
                            Form.element == "text-numeric" ||
                            Form.element == "text-email" ||
                            Form.element == "datepicker") && (
                            <>
                              <div className="form-group mb-5">
                                <label>
                                  {Form.name}
                                  {Form.required && (
                                    <span className="text-red">&nbsp;*</span>
                                  )}
                                </label>
                                <span className="form-caption font-size-sm text-italic my-2">
                                  &nbsp;{Form.span}
                                </span>
                                <input
                                  type="text"
                                  className={
                                    errors[Form.file_name]
                                      ? "form-control form-control-sm is-invalid"
                                      : "form-control form-control-sm"
                                  }
                                  placeholder={Form.placeholder}
                                  {...register(Form.file_name)}
                                />
                                <span className="text-red">
                                  {errors[Form.file_name]?.message}
                                </span>
                              </div>
                            </>
                          )}
                          {Form.element == "textarea" && (
                            <>
                              <div className="form-group mb-5">
                                <label>
                                  {Form.name}
                                  {Form.required && (
                                    <span className="text-red">&nbsp;*</span>
                                  )}
                                </label>
                                <span className="form-caption font-size-sm text-italic my-2">
                                  &nbsp;{Form.span}
                                </span>
                                <textarea
                                  type={Form.element}
                                  className={
                                    errors[Form.file_name]
                                      ? "form-control form-control-sm is-invalid"
                                      : "form-control form-control-sm"
                                  }
                                  placeholder={Form.placeholder}
                                  {...register(Form.file_name)}
                                />
                                <span className="text-red">
                                  {errors[Form.file_name]?.message}
                                </span>
                              </div>
                            </>
                          )}
                          {Form.element == "checkbox" && (
                            <>
                              <div className="form-group mb-5">
                                <label>
                                  {Form.name}
                                  {Form.required && (
                                    <span className="text-red">&nbsp;*</span>
                                  )}
                                </label>
                                <span className="form-caption font-size-sm text-italic my-2">
                                  &nbsp;{Form.span}
                                </span>
                                {Form.data_option.split(";").map((Option) => (
                                  <div className="form-check">
                                    <input
                                      className="form-check-input"
                                      type="checkbox"
                                      value={Option}
                                      id={Form.file_name + "_" + Option}
                                      {...register(Form.file_name)}
                                    />
                                    <label
                                      className="form-check-label"
                                      for={Form.file_name + "_" + Option}
                                    >
                                      {Option}
                                    </label>
                                  </div>
                                ))}
                                <span className="text-red">
                                  {errors[Form.file_name]?.message}
                                </span>
                              </div>
                            </>
                          )}
                          {Form.element == "radiogroup" && (
                            <>
                              <div className="form-group mb-5">
                                <label>
                                  {Form.name}
                                  {Form.required && (
                                    <span className="text-red">&nbsp;*</span>
                                  )}
                                </label>
                                <span className="form-caption font-size-sm text-italic my-2">
                                  &nbsp;{Form.span}
                                </span>
                                {Form.data_option.split(";").map((Option) => (
                                  <div className="form-check">
                                    <input
                                      className="form-check-input"
                                      type="radio"
                                      name={Form.file_name}
                                      value={Option}
                                      id={Form.file_name + "_" + Option}
                                      {...register(Form.file_name)}
                                    />
                                    <label
                                      className="form-check-label"
                                      for={Form.file_name + "_" + Option}
                                    >
                                      {Option}
                                    </label>
                                  </div>
                                ))}
                                <span className="text-red">
                                  {errors[Form.file_name]?.message}
                                </span>
                              </div>
                            </>
                          )}
                          {Form.element == "select" && (
                            <>
                              <div className="form-group mb-5">
                                <label>
                                  {Form.name}
                                  {Form.required && (
                                    <span className="text-red">&nbsp;*</span>
                                  )}
                                </label>
                                <span className="form-caption font-size-sm text-italic my-2">
                                  &nbsp;{Form.span}
                                </span>
                                <Controller
                                  control={control}
                                  name={Form.file_name}
                                  render={({
                                    field: {
                                      onChange,
                                      onBlur,
                                      value,
                                      name,
                                      ref,
                                    },
                                  }) => (
                                    <Select
                                      styles={{
                                        control: (baseStyles, state) => ({
                                          ...baseStyles,
                                          fontWeight: "normal",
                                          fontSize: "0.875rem",
                                          fontFamily: "inherit",
                                        }),
                                        option: (baseStyles, state) => ({
                                          ...baseStyles,
                                          fontWeight: "normal",
                                          fontSize: "0.875rem",
                                          fontFamily: "inherit",
                                        }),
                                      }}
                                      className="react-select-container"
                                      classNamePrefix="react-select"
                                      options={Form.data_option
                                        .split(";")
                                        .map((Option) => ({
                                          value: Option,
                                          label: Option,
                                        }))}
                                      onChange={onChange}
                                      onBlur={onBlur}
                                      value={value}
                                      name={name}
                                      ref={ref}
                                      placeholder="Ketik kata kunci untuk memudahkan pencarian"
                                    />
                                  )}
                                />
                                <span className="text-red">
                                  {errors[Form.file_name]?.message}
                                </span>
                              </div>
                            </>
                          )}
                          {Form.element == "trigered" && (
                            <>
                              <div
                                className="form-group mb-5"
                                style={{ display: "none" }}
                              >
                                <label>
                                  {Form.name}
                                  {Form.required && (
                                    <span className="text-red">&nbsp;*</span>
                                  )}
                                </label>
                                <span className="form-caption font-size-sm text-italic my-2">
                                  &nbsp;{Form.span}
                                </span>
                                <select
                                  className={`form-select form-select-sm ${
                                    errors[Form.file_name] ? "is-invalid" : ""
                                  }`}
                                  {...register(Form.file_name)}
                                >
                                  <option value="" selected disabled>
                                    Pilih
                                  </option>
                                  {Form.data_option.split(";").map((Option) => (
                                    <option value={Option}>{Option}</option>
                                  ))}
                                </select>
                                <span className="text-red">
                                  {errors[Form.file_name]?.message}
                                </span>
                              </div>
                            </>
                          )}
                          {Form.element == "fileimage" && (
                            <>
                              <div className="form-group mb-5">
                                <label>
                                  {Form.name}
                                  {Form.required && (
                                    <span className="text-red">&nbsp;*</span>
                                  )}
                                </label>
                                <span className="form-caption font-size-sm text-italic my-2">
                                  &nbsp;{Form.span}
                                </span>
                                <input
                                  type="file"
                                  className={
                                    errors[Form.file_name]
                                      ? "form-control form-control-sm is-invalid"
                                      : "form-control form-control-sm"
                                  }
                                  placeholder={Form.placeholder}
                                  accept="image/*"
                                  {...register(Form.file_name)}
                                />
                                <span className="text-red">
                                  {errors[Form.file_name]?.message}
                                </span>
                              </div>
                            </>
                          )}
                          {Form.element == "file-doc" && (
                            <>
                              <div className="form-group mb-5">
                                <div className="row align-items-end">
                                  <div className="col-md mb-md-0">
                                    <label>
                                      {Form.name}
                                      {Form.required && (
                                        <span className="text-red">
                                          &nbsp;*
                                        </span>
                                      )}
                                    </label>
                                    <span className="form-caption font-size-sm text-italic my-2">
                                      &nbsp;{Form.span}
                                    </span>
                                  </div>
                                  <div className="col-md-auto mb-3">
                                    {Form.data_option &&
                                      Form.data_option != "" && (
                                        <Link
                                          href={
                                            process.env.STORAGE_URL +
                                            "dts-pelatihan/" +
                                            Form.data_option
                                          }
                                        >
                                          <a
                                            className="d-flex align-items-center fw-medium"
                                            target="_blank"
                                          >
                                            Download Template
                                          </a>
                                        </Link>
                                      )}
                                  </div>
                                </div>
                                <input
                                  type="file"
                                  className={
                                    errors[Form.file_name]
                                      ? "form-control form-control-sm is-invalid"
                                      : "form-control form-control-sm"
                                  }
                                  placeholder={Form.placeholder}
                                  accept="application/pdf"
                                  {...register(Form.file_name)}
                                />
                                <span className="text-red">
                                  {errors[Form.file_name]?.message}
                                </span>
                              </div>
                            </>
                          )}
                        </>
                      ))}
                      <div className="form-row place-order">
                        <button
                          type="button"
                          className="btn btn-blue rounded-3 btn-block"
                          onClick={() => {
                            onClickPageWizard(2);
                          }}
                        >
                          SELANJUTNYA
                        </button>
                      </div>
                    </div>
                    <div
                      className="bg-gray rounded-5 p-5 my-5"
                      style={{ display: PageWizard == 2 ? "block" : "none" }}
                    >
                      <h3 className="fw-bold mb-1">Ketentuan & Pernyataan</h3>
                      <div
                        className="font-size-lg text-muted my-3"
                        dangerouslySetInnerHTML={{
                          __html: PelatihanDetail?.deskripsi_komitmen,
                        }}
                      ></div>
                      <div className="form-group mb-5">
                        <div className="form-check">
                          <input
                            className="form-check-input text-gray-800"
                            type="checkbox"
                            value={1}
                            id="menyetujui_komitmen"
                            {...register("menyetujui_komitmen")}
                            onClick={onClickFormKomitmen}
                          />
                          <label
                            className="form-check-label font-size-base text-gray-800"
                            htmlFor="menyetujui_komitmen"
                          >
                            Saya telah telah bersedia mengikuti persyaratan yang
                            ada pada formulir komitmen pelatihan ini.
                          </label>
                        </div>
                      </div>
                      <div className="form-row place-order">
                        <div className="row">
                          <div className="col-6">
                            <button
                              type="button"
                              className="btn btn-outline-secondary rounded-3 btn-block"
                              onClick={() => {
                                onClickPageWizard(1);
                              }}
                            >
                              SEBELUMNYA
                            </button>
                          </div>
                          <div className="col-6">
                            <ButtonWithLoading
                              type="button"
                              className="btn btn-blue rounded-3 btn-block disabled"
                              data-bs-toggle="modal"
                              data-bs-target="#modalFormSubmit"
                              id="buttonModalKonfirmasi"
                              loading={loadingSubmit}
                            >
                              SUBMIT PENDAFTARAN
                            </ButtonWithLoading>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        )}
    </div>
  );
}

daftarPelatihan.bodyBackground = "bg-white";

export async function getServerSideProps({ query }) {
  const token = query.token;

  /** Add Login from parameter Cookies */
  let requestLogin = axios.create({
    baseURL: `${process.env.RESTURL_SESSIONS}`,
    headers: { Authorization: `Bearer ${token}` },
  });

  let data = null;
  await requestLogin.post("/auth/login_refresh").then((res) => {
    if (res?.data?.success) {
      const user = {
        ...(res?.data?.result?.data?.user || {}),
        ["mandatory_survey"]: [],
      };

      data = {
        user: user,
        token: token,
        is_login: true,
      };
    } else {
      data = {
        user: null,
        token: token,
        is_login: false,
      };
    }
  });

  return {
    props: {
      loginRefresh: data,
    },
  };
}

export default daftarPelatihan;
