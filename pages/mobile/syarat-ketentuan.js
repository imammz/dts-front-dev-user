import React, { useState, useEffect, useRef } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import axios from "axios";
import Moment from "react-moment";
import "moment/locale/id";
import FlipMove from "react-flip-move";
import PortalService from "../../services/PortalService";
import Failed from "../../components/Modal/Failed";

//skeleton
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import PortalHelper from "../../src/helper/PortalHelper";

function Program() {
  const router = useRouter();
  const [listAkademi, setListAkademi] = useState(null);
  const [program, setProgram] = useState(null);
  const [akademi, setAkademi] = useState(null);
  const [akademiDetailCount, setAkademiDetailCount] = useState(null);
  const [length, setLength] = useState(6);
  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [showFailModal, setShowFailModal] = useState("");

  useEffect(async () => {
    try {
      setLoading(true);
      if (router.isReady) {
        const program = PortalService.generalDataSiteManajamenen();
        const listAkademiRequest = PortalService.akademiList();
        const resps = await axios.all([program, listAkademiRequest]);
        if (router.query.akademi) {
          const akademi = await PortalService.akademiDetail(
            router.query.akademi,
          );
          if (akademi.data.success) {
            setAkademi(akademi.data.result.data);
            setAkademiDetailCount(akademi.data.result.total);
          } else {
            if (res.data?.result) {
              throw Error(PortalHelper.handleErrorResponse(res.data.result));
            } else {
              if (res.data?.result) {
                throw Error(PortalHelper.handleErrorResponse(res.data.result));
              } else {
                throw Error(res.data.message);
              }
            }
          }
          setLoading(false);
        } else {
          // console.log(resps[0].data)
          // setProgram(resps[0].data.result[0].logo_description);
          setProgram({
            id: 1,
            judul_content: "Syarat dan Ketentuan",
            text_content: resps[0].data.result[0].syarat_ketentuan,
          });
        }

        if (resps[1].data.success) {
          if (resps[1].data.result && resps[1].data.result[0] != null) {
            setListAkademi(resps[1].data.result.slice(0, length));
          }

          if (resps[1].data.success) {
            if (resps[1].data.result && resps[1].data.result[0] != null) {
              setListAkademi(resps[1].data.result.slice(0, length));
            }
          } else {
            throw resps[1].data.message;
          }
          setLoading(false);
        }
      }
    } catch (err) {
      console.log(err);
    }
  }, [router]);

  const getAkademiBySlug = async (slug) => {
    try {
      setLoading(true);
      const akademi = await PortalService.akademiDetail(slug);
      if (akademi.data.success) {
        setAkademi(akademi.data.result.data);
        setAkademiDetailCount(akademi.data.result.total);
      } else {
        if (res.data?.result) {
          throw Error(PortalHelper.handleErrorResponse(res.data.result));
        } else {
          throw Error(res.data.message);
        }
      }
      setLoading(false);
    } catch (err) {
      console.log(err);
      const message = err.message;
      setLoading(false);
      setErrorMessage(message);
      setShowFailModal(true);
    }
  };

  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-12 py-6 position-relative">
            {loading && (
              <div>
                <Skeleton height={40} style={{ marginBottom: "0.5rem" }} />
                <Skeleton
                  height={120}
                  count={3}
                  style={{ marginBottom: "1rem" }}
                />

                <Skeleton height={40} style={{ marginBottom: "0.5rem" }} />
                <div className="row row-cols-lg-2 mb-8">
                  <div className="col-md">
                    <Skeleton count={6} />
                  </div>

                  <div className="col-md">
                    <Skeleton count={6} />
                  </div>
                </div>
                <div className="row g-3" style={{ textAlign: "center" }}>
                  <Skeleton
                    height={70}
                    inline={true}
                    width={240}
                    count={3}
                    className="me-3"
                  />
                </div>
              </div>
            )}
            {!loading && !akademi && program && (
              <div
                className="paragraph-wrapper text-justify"
                dangerouslySetInnerHTML={{ __html: program.text_content }}
              ></div>
            )}
            {!loading && akademi && (
              <>
                <div
                  className="post-body mb-8"
                  dangerouslySetInnerHTML={{ __html: akademi.deskripsi }}
                ></div>
                <div className="row g-3 mt-8">
                  <h3 className="fw-bolder">Eksplor lebih lanjut</h3>
                  <div className="col-12 col-md-6 col-lg-4 col-xl-4 mb-5">
                    <Link href={`/tema/${akademi.slug}`}>
                      <a className="card border rounded-5 icon-category icon-category-sm p-3">
                        <div className="row align-items-center mx-n3">
                          <div className="col-auto px-3">
                            <div
                              className="icon-circle"
                              style={{ color: "#196ECD" }}
                            >
                              <i className="fa fa-tag"></i>
                            </div>
                          </div>
                          <div className="col px-1">
                            <div className="card-body p-0">
                              <h3 className="text-primary fw-bolder mb-n1">
                                {akademiDetailCount.jml_tema}
                              </h3>
                              <p className="text-muted mb-0">Tema Pelatihan</p>
                            </div>
                          </div>
                        </div>
                      </a>
                    </Link>
                  </div>
                  <div className="col-12 col-md-6 col-lg-4 col-xl-4 mb-5">
                    <Link href="/mitra">
                      <a className="card border rounded-5 icon-category icon-category-sm p-3">
                        <div className="row align-items-center mx-n3">
                          <div className="col-auto px-3">
                            <div
                              className="icon-circle"
                              style={{ color: "#196ECD" }}
                            >
                              <i className="fa fa-building"></i>
                            </div>
                          </div>
                          <div className="col px-1">
                            <div className="card-body p-0">
                              <h3 className="text-primary fw-bolder mb-n1">
                                {akademiDetailCount.jml_mitra}
                              </h3>
                              <p className="text-muted mb-0">Mitra Pelatihan</p>
                            </div>
                          </div>
                        </div>
                      </a>
                    </Link>
                  </div>
                  <div className="col-12 col-md-6 col-lg-4 col-xl-4 mb-5">
                    <Link href={`akademi/${akademi.slug}`}>
                      <a className="card border rounded-5 icon-category icon-category-sm p-3">
                        <div className="row align-items-center mx-n3">
                          <div className="col-auto px-3">
                            <div
                              className="icon-circle"
                              style={{ color: "#196ECD" }}
                            >
                              <i className="fa fa-book"></i>
                            </div>
                          </div>

                          <div className="col px-1">
                            <div className="card-body p-0">
                              <h3 className="text-primary fw-bolder mb-n1">
                                {akademiDetailCount.jml_pelatihan}
                              </h3>
                              <p className="text-muted mb-0">Pelatihan</p>
                            </div>
                          </div>
                        </div>
                      </a>
                    </Link>
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      </div>
      <Failed
        show={showFailModal}
        handleClose={() => {
          setShowFailModal(false);
          if (window !== undefined) {
            window.location.reload();
          }
        }}
        children={errorMessage}
      />
    </>
  );
}

export default Program;
