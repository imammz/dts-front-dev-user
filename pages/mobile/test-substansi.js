// import moment from "moment-timezone";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useRef, useState } from "react";
import Skeleton from "react-loading-skeleton";
import FormTestSubstansi from "../../components/Form/TestSubstansi";
import PortalService from "../../services/PortalService";
import ProfilService from "../../services/ProfileService";
import NotificationHelper from "../../src/helper/NotificationHelper";

import Lottie, { useLottie } from "lottie-react";
import * as animation from "../../public/assets/json/loading-animation.json";
import PortalHelper from "../../src/helper/PortalHelper";
import { CacheHelperV2 as CacheHelper } from "../../src/helper/CacheHelper";
import EncryptionHelper from "../../src/helper/EncryptionHelper";
import GeneralService from "../../services/GeneralService";
import moment from "moment";
import axios from "axios";

// untuk keperluan development, jika true beberapa validasi akan dilewat
const forceTest = false;
const disableOnTimesUp = false;

// Belum ada data
const BelumAdaData = ({ message = "Belum Ada Data" }) => {
  return (
    <>
      <div className="container py-8">
        <div className="row">
          <div className="col-12 mx-auto text-center">
            <h2>{message}</h2>
            {/* <p className="font-size-lg mb-6">Pelatihan pada akademi <span className="text-blue font-weight-semi-bold">{AkademiDetail.nama}</span> belum tersedia</p> */}
            <img
              src="/assets/img/empty-state/not-found.png"
              alt="..."
              className="img-fluid align-self-center"
              style={{ width: "500px" }}
            />
          </div>
        </div>
      </div>
    </>
  );
};

const isInRangeSubstansi = (start, end, current) => {
  return (
    moment(current).diff(moment(start) >= 0) &&
    moment(current).diff(moment(end) <= 0)
  );
};

// selesai
const Selesai = ({
  namaTest = "",
  namaPelatihan = "",
  namaAkademi = "",
  idPelatihan,
  idTest,
  category = "",
  score = 70,
}) => {
  // const [skor, setSkor] = useState()

  // useEffect(async () => {
  //     const resp = await ProfilService.profilSubstansiStart(idPelatihan, idTest)
  //     console.log(resp)
  // }, [])

  return (
    <>
      <div className="container my-8">
        <div className="row">
          <div className="col-lg-12">
            <div className="card border rounded-5 border-lg border-xl px-5 py-8">
              <div className="row">
                <div className="col-12">
                  <div className="pt-6 px-5 px-lg-3 px-xl-5 text-center">
                    <img
                      src="/assets/img/empty-state/finished-test.png"
                      className="img-fluid align-self-center mb-5"
                      style={{ width: "350" }}
                    />
                    <div className="row">
                      <div className="col-12 px-4">
                        <h2 className="fw-bold text-green mb-0">
                          Test Substansi selesai
                        </h2>
                        <p>
                          Terima kasih kamu telah menyelesaikan
                          <br />
                          <strong>{namaTest}</strong>
                        </p>
                        {/*<div className="mb-5">
                        <div className="my-7">
                          <Link
                              href={{
                                pathname: "/auth/user-profile",
                                query: { menu: "tes-substansi" },
                              }}
                          >
                            <a className="btn btn-sm py-2 rounded-3 font-size-14 btn-primary">
                              TUTUP
                            </a>
                          </Link>
                        </div>
                      </div>*/}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

// Guideline
const Guidline = ({
  setShowRenderer,
  namaTest,
  currentTime,
  substansiMulai,
  substansiSelesai,
}) => {
  return (
    <>
      <div className="container mt-8 mb-10">
        <div className="row">
          <div className="col-lg-12">
            <div className="card border rounded-5 border-lg border-xl px-6 py-8">
              <div className="row">
                <div className="col-lg-5 mb-6">
                  <h3 className="fw-bold text-center mb-4">{namaTest}</h3>

                  <img
                    src="/assets/img/empty-state/panduan-substansi.png"
                    className="img-fluid align-self-center mb-5"
                    style={{ width: "400px" }}
                  />
                </div>
                <div className="col-lg-7">
                  <div className="d-block rounded-5 bg-info">
                    <div className="pt-6 px-5">
                      <div className="d-flex align-items-center">
                        <div>
                          <h4 className="fw-semi-bold mb-4">Panduan:</h4>
                          <p>
                            Sebelum mengerjakan, harap membaca panduan berikut :
                          </p>
                          <ul
                            className="list-style-v2 list-unstyled"
                            style={{ textAlign: "left" }}
                          >
                            <li>
                              Pastikan koneksi internet stabil dan pengaturan
                              browser dengan "Javascript Enabled". Disarankan
                              menggunakan browser versi terbaru.
                            </li>
                            <li>
                              Disarankan untuk menonaktifkan add-on pihak ketiga
                              pada browser.
                            </li>
                            <li>
                              Peserta wajib menjawab seluruh soal Tes Substansi
                              dan jumlah soal sesuai dengan masing-masing tema
                              pelatihan. Tidak ada nilai negatif untuk jawaban
                              yang salah.
                            </li>
                            <li>
                              Setelah Tes Substansi dimulai, waktu tes tidak
                              dapat dihentikan dan tes tidak dapat diulang.
                              Setelah waktu habis, halaman soal akan tertutup
                              secara otomatis.
                            </li>
                            <li>
                              Skor untuk soal yang sudah dijawab tetap terhitung
                              walaupun peserta belum menekan tombol submit atau
                              peserta mengalami force majeure.
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div className="form-row place-order p-5">
                      {isInRangeSubstansi(
                        substansiMulai,
                        substansiSelesai,
                        currentTime,
                      ) ? (
                        <p className="text-center text-danger text-red">
                          Tidak dapat mengerjakan Test Substansi karena masih
                          diluar rentang pengerjaan.
                        </p>
                      ) : (
                        <a
                          onClick={() => {
                            setShowRenderer(true);
                          }}
                          className="btn btn-blue rounded-3 btn-block"
                        >
                          <i className="fa fa-play me-2"></i>MULAI TES SUBSTANSI
                        </a>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

// komponen formrenderer
const Timer = ({
  waktu_mulai,
  waktu_sekarang,
  waktu_selesai,
  onTimesUp = () => {},
  textOnly = false,
}) => {
  const [totalDuration, setTotalDuration] = useState();
  const [currentDuration, setCurrentDuration] = useState(null);
  const [intCountdown, setIntCountdown] = useState(null);

  useEffect(() => {
    // console.log(currentDuration)
    if (currentDuration > 0) {
      // console.log(currentDuration)
      // setCurrentDuration((c) => { return c - 1000 })
    } else if (currentDuration <= 0 && currentDuration != null) {
      if (typeof onTimesUp == "function") {
        onTimesUp();
      }
      // console.log("timesup")
      clearInterval(intCountdown);
    }
    // // console.log(currentDuration)
    // return () => {
    //     console.log("cleanup")
    //     clearInterval(intCountdown)
    // }
  }, [currentDuration]);

  useEffect(() => {
    // let interval
    if (waktu_mulai && waktu_sekarang && waktu_selesai) {
      if (intCountdown != null) {
        clearInterval(intCountdown);
      }
      const wc = PortalHelper.timezoneConverter(new Date(), "Asia/Jakarta");
      const ws = new Date(waktu_mulai);
      const we = new Date(waktu_selesai);

      setTotalDuration(
        // Get duration in minutes
        Math.floor((we.getTime() - ws.getTime()) / 1000 / 60),
      );
      // Get Duration in seconds
      const durationLeft = Math.floor((we.getTime() - wc.getTime()) / 1000);
      setCurrentDuration(durationLeft);
      if (durationLeft > 0) {
        let intv = setInterval(() => {
          console.log(durationLeft);

          setCurrentDuration((c) => {
            return c - 1;
          });
        }, 1000);
        setIntCountdown(intv);
      } else {
        if (typeof onTimesUp == "function") {
          onTimesUp();
        }
      }
    }

    return () => {
      // console.log("cleanup")
      clearInterval(intCountdown);
    };
  }, [waktu_mulai, waktu_sekarang, waktu_selesai]);

  return !textOnly ? (
    <div className="card border rounded-5 px-5 py-5 mb-5">
      <h5 className="mb-0">
        Waktu <small className="font-size-sm">({totalDuration} menit)</small>
      </h5>
      <div className="col">
        {currentDuration > 0 ? (
          <>
            <h4 className="text-green my-0">
              {
                // Create HH:MM:SS from seconds with Date Javascript
                new Date(currentDuration * 1000).toISOString().substr(11, 8)
              }
            </h4>
          </>
        ) : (
          <>
            <h4 className="text-red my-0">00:00:00</h4>
          </>
        )}
      </div>
    </div>
  ) : currentDuration > 0 ? (
    <>
      <span className="text-green my-0">
        {new Date(currentDuration * 1000).toISOString().substr(11, 8)}
      </span>
    </>
  ) : (
    <>
      <span className="text-red my-0">00:00:00</span>
    </>
  );
};

const ModalWaktuHabis = ({ show, onClose = () => {}, statPengerjaan }) => {
  const [idModal] = useState("modalWaktuHabis");

  const toggleOpenModal = useRef(null);
  const toggleCloseModal = useRef(null);
  const showSuccessModal = useRef(null);

  useEffect(() => {
    showSuccessModal.current.addEventListener("hidden.bs.modal", () => {
      onClose();
    });
    if (show && idModal) {
      toggleOpenModal.current?.click();
    } else {
      toggleCloseModal.current?.click();
    }
  }, [show]);

  return (
    <>
      <a
        href="#"
        ref={toggleOpenModal}
        data-bs-toggle="modal"
        data-bs-target={`#${idModal}`}
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseModal}
        data-bs-dismiss="modal"
        data-bs-target={`#${idModal}`}
        hidden
      ></a>

      <div
        className="modal fade"
        id={`${idModal}`}
        ref={showSuccessModal}
        tabIndex={-1}
        role="dialog"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <button
                type="button"
                className="close"
                data-bs-dismiss="modal"
                aria-label="Close"
                onClick={() => {
                  onClose();
                }}
              >
                <span aria-hidden="true">
                  <svg
                    width={16}
                    height={17}
                    viewBox="0 0 16 17"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M0.142135 2.00015L1.55635 0.585938L15.6985 14.7281L14.2843 16.1423L0.142135 2.00015Z"
                      fill="currentColor"
                    />
                    <path
                      d="M14.1421 1.0001L15.5563 2.41431L1.41421 16.5564L0 15.1422L14.1421 1.0001Z"
                      fill="currentColor"
                    />
                  </svg>
                </span>
              </button>

              <h2
                className="fw-bold text-center mb-1 text-blue"
                id={`${idModal}Title`}
              >
                Perhatian
              </h2>

              <p className="font-size-lg text-center text-muted mb-0">
                Waktu pengerjaan substansi telah habis
              </p>
              <div className="mb-md-4 text-center">
                <img
                  src="/assets/img/illustrations/times-up.png"
                  className="w-50 image-responsive"
                />
              </div>

              <div className="row">
                <div className="col-12">
                  <button
                    className={`btn btn-block btn-primary mt-3 lift`}
                    onClick={() => {
                      onClose();
                    }}
                  >
                    Submit Jawaban
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const ModalResumePengerjaan = ({
  show,
  onClose = () => {},
  onOk = () => {},
  children,
  isSubmitDisabled,
}) => {
  const [idModal] = useState("modalResumePengerjaan");

  const toggleOpenModal = useRef(null);
  const toggleCloseModal = useRef(null);
  const showSuccessModal = useRef(null);

  useEffect(() => {
    showSuccessModal.current.addEventListener("hidden.bs.modal", () => {
      onClose();
    });
    if (show && idModal) {
      toggleOpenModal.current?.click();
    } else {
      toggleCloseModal.current?.click();
    }
  }, [show]);

  return (
    <>
      <a
        href="#"
        ref={toggleOpenModal}
        data-bs-toggle="modal"
        data-bs-target={`#${idModal}`}
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseModal}
        data-bs-dismiss="modal"
        data-bs-target={`#${idModal}`}
        hidden
      ></a>

      <div
        className="modal fade"
        id={`${idModal}`}
        ref={showSuccessModal}
        tabIndex={-1}
        role="dialog"
      >
        <div
          className="modal-dialog modal-dialog-centered modal-fullscreen-sm-down"
          role="document"
        >
          <div className="modal-content">
            <div className="modal-header py-3 px-4">
              <h5 className="fw-bold modal-title" id="exampleModalLabel">
                Rekap Test Substansi
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body py-3 px-4">{children}</div>
            <div className="modal-footer py-3 px-4 justify-content-center">
              <button
                type="button"
                className="btn btn-sm rounded-3 btn-outline-secondary"
                data-bs-dismiss="modal"
              >
                Batal
              </button>
              <button
                type="button"
                className={`btn btn-sm rounded-3 btn-blue ${
                  isSubmitDisabled ? "disabled" : ""
                }`}
                onClick={() => {
                  if (!isSubmitDisabled) onOk();
                }}
              >
                <i className="fa fa-paper-plane me-1"></i>Kirim Test Substansi
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

function ModalLoading({
  show,
  handleClose,
  loadingMessage = "Loading ...",
  backdropOnly = true,
}) {
  const toggleOpenLoadignModal = useRef(null);
  const toggleCloseLoadingModal = useRef(null);
  const showLoadingModal = useRef(null);

  const options = {
    animationData: animation,
    loop: true,
  };
  const lottie = useLottie(options);
  lottie.setSpeed(2);
  const { View } = lottie;

  useEffect(() => {
    // console.log(show)
    showLoadingModal.current?.addEventListener("hidden.bs.modal", () => {
      handleClose();
    });
    if (show) {
      toggleOpenLoadignModal.current?.click();
    } else {
      toggleCloseLoadingModal.current.click();
      // showLoadingModal.current.addEventListener('shown.bs.modal', () => {
      //     if (!show) {
      //     }
      // })
    }
  }, [show]);

  return (
    <>
      <a
        href="#"
        ref={toggleOpenLoadignModal}
        data-bs-toggle="modal"
        data-bs-target="#loadingModalTest"
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseLoadingModal}
        data-bs-dismiss="modal"
        data-bs-target="#loadingModalTest"
        hidden
      ></a>

      <div
        className="modal"
        id="loadingModalTest"
        ref={showLoadingModal}
        data-bs-backdrop="static"
        data-bs-keyboard="false"
        role="dialog"
      >
        {!backdropOnly && (
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">
              <div className="modal-body" style={{ textAlign: "center" }}>
                <div className="w-50 m-auto">
                  {View}
                  <h2
                    className="fw-bold text-center mb-1 text-primary"
                    id="title"
                  >
                    {loadingMessage}
                  </h2>
                </div>
              </div>
            </div>
          </div>
        )}
        {backdropOnly && (
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-body" style={{ background: "transparent" }}>
              <div className="w-50 m-auto" style={{ textAlign: "center" }}>
                {View}
                <h2 className="fw-bold text-center mb-1 text-white" id="title">
                  {loadingMessage}
                </h2>
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  );
}

const FormRenderer = ({
  namaTest,
  namaPelatihan,
  namaAkademi,
  showRenderer,
  idPelatihan = "",
  idTest = "",
  category = "",
  alurPendaftaran = "",
  router = null,
  jumlahHarusDijawab = 2,
}) => {
  const [listSoal, setListSoal] = useState([]);
  const [currentNumber, setCurrentNumber] = useState(1);
  const [waktu, setWaktu] = useState({
    waktu_mulai: null,
    waktu_sekarang: null,
    waktu_selesai: null,
  });

  // loading awal dan submit
  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingSubmit, setIsLoadingSubmit] = useState(false);
  const [loadingMessage, setLoadingMessage] = useState("Memuat soal ...");

  const [statPengerjaan, setStatPengerjaan] = useState({
    terjawab: 0,
    totalSoal: 0,
    belumDijawab: 0,
  });
  const [showModalResumePengerjaan, setShowModalResumePengerjaan] =
    useState(false);
  const [showModalWaktuHabis, setShowModalWaktuHabis] = useState(false);

  const headerMobileSubstansi = useRef();
  const [isHeaderSticy, setIsHeaderSticky] = useState(false);

  const [user] = useState(PortalHelper.getUserData());
  const [wasSubmit, setWasSubmit] = useState(false);

  const cacheId =
    "test-substansi-" + user.id + "-" + idPelatihan + "-" + idTest;

  //  pengaturan untuk tampilan mobile
  useEffect(() => {
    window.onscroll = function () {
      myFunction();
    };

    // var header = document.getElementById("headerMobileSubstansi");
    function myFunction() {
      var sticky = headerMobileSubstansi.current?.getBoundingClientRect().top;
      // console.log(headerMobileSubstansi.current?.getBoundingClientRect().top)
      // console.log(window.pageYOffset)
      if (window.pageYOffset > sticky) {
        setIsHeaderSticky(true);
      } else {
        setIsHeaderSticky(false);
      }
    }
    return () => {
      window.onscroll = true;
    };
  }, []);

  // event ketika page pertamakali diload dan mengambil data soal + waktu pengerjaan
  useEffect(async () => {
    // console.log("itsshonw")
    try {
      if (showRenderer && idTest && idPelatihan) {
        const resp = await ProfilService.getPertanyaanTestSubstansi(idTest);

        // cek jawaban yg tersimpan
        const objJawaban = await getLocal();
        // console.log("objJawaban",objJawaban)

        const getJawabanByIdSoal = (idSoal) => {
          if (objJawaban[idSoal] && idSoal) {
            return objJawaban[idSoal];
          } else {
            return "";
          }
        };

        const orders = await CacheHelper.GetFromCache({
          index: cacheId + "-orders",
        });

        let newSoal = [];
        if (resp.data.success) {
          // console.log(resp.data.result,orders)
          let soals = [];
          if (orders) {
            orders.forEach((id) => {
              let s = _.find(resp.data.result.data, function (o) {
                return o.question_id == id;
              });
              soals.push(s);
            });
          } else {
            soals = _.shuffle(resp.data.result.data).slice(
              0,
              jumlahHarusDijawab,
            );
          }

          let orderSoals = [];

          for (let i = 0; i < soals.length; i++) {
            let no = i + 1;
            // console.log(soals[i])

            const idSoal = soals[i].question_id;
            const jwb = getJawabanByIdSoal(idSoal);
            // console.log(idSoal, jwb)

            orderSoals.push(idSoal);

            newSoal.push({
              ...soals[i],
              answer_key: jwb,
              no: no,
              seen: no == 1 || jwb != "" ? true : false,
            });
          }

          CacheHelper.CreateCache({
            index: cacheId + "-orders",
            data: orderSoals,
          });
          // console.log(newSoal)
          // getwaktu
          const respWaktu = await ProfilService.profilSubstansiStart(
            idPelatihan,
            idTest,
          );
          if (respWaktu.data.success) {
            const { finish_datetime, start_datetime, time_server } =
              respWaktu.data.result.data[0];
            let tms = time_server;
            if (tms.includes(",")) {
              tms = tms.split(".")[0];
            }
            setWaktu({
              waktu_mulai: start_datetime,
              waktu_selesai: finish_datetime,
              waktu_sekarang: tms,
            });
            // setWaktu({ waktu_mulai: "2022-08-01 07:00:00", waktu_sekarang: "2022-08-01 08:59:50", waktu_selesai: "2022-08-01 09:00:00" })
            setListSoal(newSoal);
          } else {
            NotificationHelper.Failed({
              message: "Tidak dapat memulai test",
              okText: "Muat ulang",
              onClose: () => {
                window.location.reload();
              },
            });
          }
          // setListSoal(newSoal)
        }
        setIsLoading(false);
      }
    } catch (error) {}
  }, [showRenderer, idTest, idPelatihan]);

  // event ketika perubahan page number
  useEffect(() => {
    let soals = [...listSoal];
    if (soals.length > 0) {
      soals[currentNumber - 1] = { ...soals[currentNumber - 1], seen: true };
      // console.log(soals)
      setListSoal(soals);
    }
    // setTimeout(() => {
    PortalHelper.scrollSpy("#fieldPengerjaanSubstansi", 120);
    // }, 1000);
  }, [currentNumber]);

  // ketika modal timesup muncul
  useEffect(() => {
    if (!showModalWaktuHabis && wasSubmit) {
      submitJawban();
    }
  }, [showModalWaktuHabis, wasSubmit]);

  const updateStatPengerjaan = () => {
    let jawab = 0;
    let totalSoal = listSoal.length;
    for (let i = 0; i < totalSoal; i++) {
      if (listSoal[i].answer_key != "") {
        jawab++;
      }
    }
    setStatPengerjaan({
      terjawab: jawab,
      totalSoal: totalSoal,
      belumDijawab: totalSoal - jawab,
    });
  };

  const onJawab = (v) => {
    // console.log(v)
    let soals = [...listSoal];
    soals[currentNumber - 1] = { ...soals[currentNumber - 1], answer_key: v };
    setListSoal(soals);
    saveLocal({ ...soals[currentNumber - 1] });
  };

  // ketika hitungan waktu tersisa = 0
  const onTimesUp = () => {
    // window.alert("times up")
    if (!disableOnTimesUp) {
      setShowModalWaktuHabis(true);
    }
  };

  const submitJawban = async () => {
    try {
      setLoadingMessage("Mengunggah jawaban ...");
      setIsLoadingSubmit(true);
      let data = [];
      listSoal.forEach((soal) => {
        // console.log(soal)
        data.push({
          answer_key: soal.answer_key,
          question: soal.question,
          question_image: soal.question_image,
          answer: soal.answer,
          subtance_question_bank_detail_id: soal.question_id,
          // subtance_question_bank_id: soal.question_type_id,
          subtance_question_bank_id: soal.substansi_id,
        });
      });
      // console.log(data)
      const resp = await ProfilService.profilSubmitSubstansi(
        idPelatihan,
        JSON.stringify(data),
        category,
      );
      if (resp.data.success) {
        if (resp.data.result.data[0]?.code == 200) {
          const userData = PortalHelper.getUserData();
          const surveys = (userData?.mandatory_survey || [])?.filter(
            (elem) =>
              elem.question_bank_id != idTest && elem.type == "test-substansi",
          );
          PortalHelper.updateUserData("mandatory_survey", surveys);

          await CacheHelper.ClearCache({ index: cacheId });
          NotificationHelper.Success({
            message: "Jawaban berhasil disubmit",
            onClose: () => {
              if (alurPendaftaran == "Test Substansi - Administrasi") {
                router?.push(`/pelatihan/${idPelatihan}/daftar`);
              }
              window.location.reload();
            },
          });
        } else if (resp.data.result.data[0]?.code == 500) {
          NotificationHelper.Failed({
            message: resp.data.result.data[0]?.message,
            onClose: () => {
              window.location.reload();
            },
          });
        } else {
          NotificationHelper.Failed({
            message: "Terjadi kesalahan",
            onClose: () => {
              window.location.reload();
            },
          });
        }
      } else {
        NotificationHelper.Failed({
          message: resp.data.result,
          onClose: () => {
            window.location.reload();
          },
        });
      }
    } catch (error) {
      console.log(error);
      NotificationHelper.Failed({
        message: error.toString(),
        onClose: () => {
          window.location.reload();
        },
      });
    }
    setIsLoadingSubmit(false);
  };

  //
  const saveLocal = async ({ question_id, answer_key }) => {
    const id_soal = question_id;
    const jawaban = answer_key;

    let data = await getLocal();

    const save = (d) => {
      let dataSimpan = EncryptionHelper.encrypt(JSON.stringify(d));
      CacheHelper.CreateCache({ index: cacheId, data: dataSimpan });
    };

    if (data != null) {
      data[id_soal] = jawaban;
      // console.log(data)
      save(data);
    } else {
      data = {};
      data[id_soal] = jawaban;
      // console.log(data)
      save(data);
    }
  };

  const getLocal = async () => {
    // console.log("called get local")
    try {
      let dt = await CacheHelper.GetFromCache({ index: cacheId, force: true });
      // console.log("dt", dt)
      if (dt != undefined) {
        // console.log("return")
        return JSON.parse(EncryptionHelper.decrypt(dt));
      } else {
        // console.log("return und")
        return {};
      }
    } catch (error) {
      // console.log(error)
      return {};
    }
  };

  return (
    <>
      <ModalLoading
        show={isLoading || isLoadingSubmit}
        handleClose={() => {
          // NotificationHelper.Success({ message: "berhasil submit" })
          setIsLoading(false);
        }}
        loadingMessage={loadingMessage}
      ></ModalLoading>
      <ModalResumePengerjaan
        show={showModalResumePengerjaan}
        onClose={() => {
          setShowModalResumePengerjaan(false);
        }}
        onOk={() => {
          setShowModalResumePengerjaan(false);
          submitJawban();
        }}
        isSubmitDisabled={statPengerjaan.belumDijawab != 0}
      >
        <h3 className="fw-bold mt-5 mb-6 text-center">{namaTest}</h3>
        <table className="font-size-12 mx-auto table border">
          <tbody>
            <tr>
              <td width={90}>Nama</td>
              <td>:</td>
              <td>{user.nama}</td>
            </tr>
            <tr>
              <td>Akademi</td>
              <td>:</td>
              <td>
                <p className="line-clamp-2 mb-0">{namaAkademi}</p>
              </td>
            </tr>
            <tr>
              <td>Pelatihan</td>
              <td>:</td>
              <td>
                <p className="line-clamp-2 mb-0">{namaPelatihan}</p>
              </td>
            </tr>
            <tr>
              <td>Sisa waktu</td>
              <td>:</td>
              <td className="line-clamp-2">
                <Timer {...waktu} textOnly={true}></Timer>
              </td>
            </tr>
          </tbody>
        </table>
        <div className="w-100">
          <div className="mx-auto d-flex justify-content-around">
            <div>
              <div className="rounded d-flex align-items-center justify-content-center bg-secondary w-100p h-100p">
                <div>
                  <span className="stat-pengerjaan-jumlah text-white">
                    {statPengerjaan.totalSoal}
                  </span>
                </div>
              </div>
              <p className="text-center fw-bold">Total</p>
            </div>
            <div>
              <div className="rounded d-flex align-items-center justify-content-center bg-green w-100p h-100p">
                <div>
                  <span className="stat-pengerjaan-jumlah text-white">
                    {statPengerjaan.terjawab}
                  </span>
                </div>
              </div>
              <p className="text-center fw-bold">Dijawab</p>
            </div>
            <div>
              <div className="rounded d-flex align-items-center justify-content-center bg-red w-100p h-100p">
                <div>
                  <span className="stat-pengerjaan-jumlah text-white">
                    {statPengerjaan.belumDijawab}
                  </span>
                </div>
              </div>
              <p className="text-center fw-bold">Belum Dijawab</p>
            </div>
          </div>
        </div>
      </ModalResumePengerjaan>
      <ModalWaktuHabis
        show={showModalWaktuHabis}
        onClose={() => {
          setWasSubmit(true);
          setShowModalWaktuHabis(false);
          // submitJawban();
        }}
      ></ModalWaktuHabis>
      {isLoading ? (
        <>
          <div className="container py-8">
            <Skeleton style={{ height: 400 }}></Skeleton>
          </div>
        </>
      ) : (
        <>
          <div
            id="fieldPengerjaanSubstansi"
            className={`container my-8 ${isHeaderSticy ? "py-8" : ""} py-md-0 `}
          >
            <div className="row">
              <div className="col-lg-12">
                <div className="row">
                  <div className="col-lg-12 mb-5">
                    <div className="card border rounded-5 border-lg border-xl px-5 py-5">
                      <div className="d-block">
                        <div className="pb-5">
                          <div className="row align-items-end pb-5 mb-3">
                            <div className="col-md mb-md-0 ">
                              <h3 className="fw-bold text-dark mb-0">
                                {namaTest}
                              </h3>
                              <p className="text-muted font-size-14 fw-bold mb-0">
                                {namaPelatihan}
                              </p>
                              <p className="text-muted font-size-14 mb-0">
                                {namaAkademi}
                              </p>
                            </div>
                          </div>
                          <Timer {...waktu} onTimesUp={onTimesUp}></Timer>
                          <hr className=" d-md-block" />
                          <div className="d-flex align-items-center">
                            <div className="col-12 pt-5">
                              <h6 className="mb-0 fw-semi-bold text-muted">
                                Soal {currentNumber} dari {listSoal.length}
                              </h6>
                              <FormTestSubstansi
                                {...listSoal[currentNumber - 1]}
                                onChange={onJawab}
                              ></FormTestSubstansi>
                            </div>
                          </div>
                        </div>
                        <div className="row gx-2 mt-8">
                          <div className="col-auto align-self-start">
                            {currentNumber != 1 && (
                              <a
                                onClick={() => {
                                  setCurrentNumber((c) => {
                                    return c - 1;
                                  });
                                }}
                                className="btn btn-outline-secondary rounded-3 btn-sm px-3 mb-3 font-size-12"
                              >
                                <i className="fa fa-chevron-left"></i>
                                <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                                  Sebelumnya
                                </span>
                              </a>
                            )}
                          </div>
                          <div className="col-auto align-self-start">
                            {currentNumber != listSoal.length ? (
                              <a
                                onClick={() => {
                                  setCurrentNumber((c) => {
                                    return c + 1;
                                  });
                                }}
                                className="btn btn-primary rounded-3 btn-sm px-3 mb-3 font-size-12"
                              >
                                <i className="fa fa-chevron-right"></i>
                                <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                                  Selanjutnya
                                </span>
                              </a>
                            ) : (
                              <>
                                <a
                                  onClick={() => {
                                    updateStatPengerjaan();
                                    setShowModalResumePengerjaan(true);
                                  }}
                                  className="btn btn-primary rounded-3 btn-sm px-3 mb-3 font-size-12"
                                >
                                  <i className="fa fa-paper-plane me-1"></i>
                                  <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                                    Submit Tes Substansi
                                  </span>
                                </a>
                              </>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-12  border-left">
                    <div className="card border rounded-5 px-4 py-5 mb-5">
                      <h5 className="fw-bold text-muted">Soal</h5>
                      <div className="col">
                        {listSoal.map((soal, i) => {
                          let n = i + 1;
                          return (
                            <a
                              key={i}
                              className="cursor-pointer"
                              onClick={() => {
                                setCurrentNumber(n);
                              }}
                            >
                              <span
                                className={`badge font-size-14 ${
                                  n == currentNumber
                                    ? "bg-green"
                                    : soal.seen
                                      ? soal.answer_key
                                        ? "bg-blue"
                                        : "bg-red"
                                      : "badge-primary-soft"
                                } w-40p p-2 mb-2 mx-1`}
                              >
                                {n}
                              </span>
                            </a>
                          );
                        })}
                      </div>
                      <hr />
                      <ul className="list mb-0">
                        <li className="list-item">
                          <span className="badge bg-blue">Sudah dijawab</span>
                        </li>
                        <li className="list-item">
                          <span className="badge bg-red">Belum dijawab</span>
                        </li>
                        <li className="list-item">
                          <span className="badge bg-green">Soal Saat ini</span>
                        </li>
                        <li className="list-item">
                          <span className="badge badge-primary-soft">
                            Belum dikerjakan
                          </span>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
};

// Page Utama
function TestSubstansi({ loginRefresh }) {
  const [showRenderer, setShowRenderer] = useState(false);
  const [isSelesai, setIsSelesai] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  const [namaTest, setNamaTest] = useState("");
  const [namaPelatihan, setNamaPelatihan] = useState("");
  const [namaAkademi, setNamaAkademi] = useState("");
  const [questions_to_share, setQuestionToShare] = useState();
  const [category, setCategory] = useState("");
  const [alurPendaftaran, setAlurPendaftaran] = useState("");
  const [tanggalMulaiSubstansi, setTanggalMulaiSubstansi] = useState("");
  const [tanggalSelesaiSubstansi, setTanggalSelesaiSubstansi] = useState("");
  const [serverTime, setServerTime] = useState("");
  const [score, setScore] = useState(0);
  const [isReady, setReady] = useState(false);

  const router = useRouter();
  const {
    query: { idTest, idPelatihan, token },
  } = router;

  useEffect(() => {
    if (loginRefresh.is_login) {
      PortalHelper.saveUserToken(loginRefresh.token, true);
      PortalHelper.saveUserData(loginRefresh.user, true);
    } else {
      PortalHelper.saveUserToken(null, true);
      PortalHelper.saveUserData(null, true);
    }
  }, [loginRefresh]);

  // console.log(this)
  useEffect(async () => {
    if (router.isReady) {
      // console.log(idTest)
      if (idTest && idPelatihan) {
        await getPelatihanDetail(idPelatihan);
        await checkStatus(idPelatihan, idTest);
      }
    }
  }, [router.isReady, idTest, idPelatihan, token]);

  const isInRangeSubstansi = (start, end, current) => {
    return (
      moment(current).diff(moment(start)) >= 0 &&
      moment(current).diff(moment(end)) <= 0
    );
  };

  const getPelatihanDetail = async (idPelatihan) => {
    try {
      const resp = await PortalService.pelatihanDetail(idPelatihan);
      if (resp.data.success) {
        const {
          nama_pelatihan,
          nama_akademi,
          alur_pendaftaran,
          substansi_mulai,
          substansi_selesai,
        } = resp.data.result.data;
        setNamaPelatihan(nama_pelatihan);
        setNamaAkademi(nama_akademi);
        setAlurPendaftaran(alur_pendaftaran);
        setTanggalMulaiSubstansi(substansi_mulai);
        setTanggalSelesaiSubstansi(substansi_selesai);

        if (alur_pendaftaran == "Administrasi - Test Substansi") {
          try {
            const resp =
              await ProfilService.cekStatusPelatihanPeserta(idPelatihan);
            if (resp.data.success) {
              if (resp.data.result.data[0].status_peserta != "Tes Substansi") {
                NotificationHelper.Failed({
                  message:
                    "Maaf Digiers, kamu belum bisa mengerjakan Tes Substansi karena belum lolos seleksi Administrasi.",
                  onClose: () => {
                    router.push("/auth/user-profile");
                  },
                });
              }
            } else {
              throw Error(resp.data.message);
            }
          } catch (err) {
            console.log(err);
          }
        }

        GeneralService.getServerTime().then((res) => {
          setServerTime(res.serverTime);
          // server time now
          if (
            !isInRangeSubstansi(
              substansi_mulai,
              substansi_selesai,
              res.serverTime,
            )
          ) {
            // diluar rentang
            NotificationHelper.Failed({
              message:
                "Maaf Digiers, kamu tidak bisa mengerjakan Tes Substansi diluar rentang tanggal pengerjaan.",
              onClose: () => {
                router.push("/auth/user-profile");
              },
            });
          }
        });
      }
      // console.log(resp)
    } catch (error) {}
  };

  const checkStatus = async (idPelatihan, idTest) => {
    try {
      // sementara cek pake list
      const resp = await ProfilService.profilStatSubstansi(idPelatihan, idTest);
      // console.log(resp)
      if (resp.data.success) {
        if (resp.data.result?.data == null) {
          setIsError(true);
          NotificationHelper.Failed({
            message: "Test substansi tidak ditemukan",
            onClose: () => {
              router.push("/auth/user-profile?menu=tes-substansi");
            },
          });
        } else {
          setNamaTest(resp.data.result?.data[0]?.substansi_nama);
          setCategory(resp.data.result?.data[0]?.category);
          setScore(resp.data.result?.data[0]?.score);
          setQuestionToShare(resp.data.result?.data[0]?.questions_to_share);
          switch (resp.data.result?.data[0]?.status) {
            case "sudah mengerjakan":
              if (!forceTest) {
                setIsSelesai(true);
                setNamaTest(resp.data.result?.data[0]?.substansi_nama);
              }

              break;
            case "sedang mengerjakan":
              setShowRenderer(true);
            case "belum mengerjakan":
              break;

            default:
              setIsError(true);
              NotificationHelper.Failed({
                message: "Terjadi kesalahan",
                onClose: () => {
                  router.push("/auth/user-profile?menu=tes-substansi");
                },
              });

              break;
          }
        }
      } else {
        throw new Error("terjadi kesalahan");
      }
    } catch (error) {
      NotificationHelper.Failed({ message: error.message });
    }
    setIsLoading(false);
  };

  return (
    <>
      <div>
        {isError ? (
          <BelumAdaData></BelumAdaData>
        ) : isLoading ? (
          <div className="container py-8">
            <Skeleton className="h-400p"></Skeleton>
          </div>
        ) : isSelesai ? (
          <Selesai
            score={score}
            namaPelatihan={namaPelatihan}
            namaTest={namaTest}
            namaAkademi={namaAkademi}
            idTest={idTest}
            idPelatihan={idPelatihan}
            category={category}
          ></Selesai>
        ) : showRenderer ? (
          <FormRenderer
            namaTest={namaTest}
            namaAkademi={namaAkademi}
            namaPelatihan={namaPelatihan}
            showRenderer={showRenderer}
            idTest={idTest}
            idPelatihan={idPelatihan}
            category={category}
            alurPendaftaran={alurPendaftaran}
            router={router}
            jumlahHarusDijawab={questions_to_share}
          ></FormRenderer>
        ) : (
          <Guidline
            setShowRenderer={setShowRenderer}
            namaTest={namaTest}
            currentTime={serverTime}
            substansiMulai={tanggalMulaiSubstansi}
            substansiSelesai={tanggalSelesaiSubstansi}
          ></Guidline>
        )}
      </div>
    </>
  );
}

export async function getServerSideProps({ query }) {
  const token = query.token;
  console.log(query);

  /** Add Login from parameter Cookies */
  let requestLogin = axios.create({
    baseURL: `${process.env.RESTURL_SESSIONS}`,
    headers: { Authorization: `Bearer ${token}` },
  });

  let data = null;
  await requestLogin.post("/auth/login_refresh").then((res) => {
    if (res?.data?.success) {
      const user = {
        ...(res?.data?.result?.data?.user || {}),
        ["mandatory_survey"]: [],
      };

      data = {
        user: user,
        token: token,
        is_login: true,
      };
    } else {
      data = {
        user: null,
        token: token,
        is_login: false,
      };
    }
  });

  return {
    props: {
      loginRefresh: data,
    },
  };
}

// TestSubstansi.bodyBackground = "bg-white"
export default TestSubstansi;
