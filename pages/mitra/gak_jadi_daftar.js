// wizard resgitrasi
import React, { useState, useEffect, useRef, useMemo } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import Link from "next/link";
import { useRouter } from "next/router";
import ValidationMessage from "../../src/validation-message";
import SSOInfo from "../../components/SSOSInfo";
import Failed from "../../components/Modal/Failed";
import Success from "../../components/Modal/Success";
import PortalHelper from "../../src/helper/PortalHelper";
import FlipMove from "react-flip-move";
import CountryCode from "../../public/assets/json/phone_code.js";
import EncryptionHelper from "../../src/helper/EncryptionHelper";
import VerifikasiEmail from "../../components/Modal/VerifikasiEmailModal";
import PortalService from "../../services/PortalService";
import { CacheHelperV2 } from "../../src/helper/CacheHelper";
import ProfilService from "../../services/ProfileService";
import ReCAPTCHA from "react-google-recaptcha";
// import eventBus
class CaptchaError extends Error {
  constructor(message) {
    super(message); // (1)
    this.name = "CaptchaError"; // (2)
  }
}
export default function Daftar({ showLoadingModal }) {
  const cacheIdRegister = useMemo(() => "register-mitra", []);
  const adminUrl = process.env.ADMIN_BASE_URL
    ? process.env.ADMIN_BASE_URL
    : "https://front.dev.dtsnet.net/";

  const [isLoadingGetLinks, setIsLoadingGetLinks] = useState(true);
  const [showFailModal, setShowFailModal] = useState(false);
  const [showSuccessModal, setShowSuccessModal] = useState(false);
  const [showVerifikasiModal, setShowVerifikasiModal] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showPasswordConf, setShowPasswordConf] = useState(false);
  const [serverMessage, setServerMessage] = useState("");
  const [isUserMitra, setIsUserMitra] = useState(false);

  const [codes, setCodes] = useState(null);
  const [phoneCode, setPhoneCode] = useState({
    country_name: "Indonesia",
    country_slug: "ID",
    emoji: "🇮🇩",
    phone_code: "62",
  });
  const registerForm = useRef(null);
  const [inputNIK, setinputNIK] = useState(0);
  const [siteManajemenData, setSiteManajemenData] = useState({
    alamat: "",
    color: [],
    created_at: "",
    created_by: "",
    email: "",
    footer_logo: "",
    header_logo: "",
    koordinat: [],
    link: [],
    logo_description: "",
    notelp: [],
    social_media: [],
  });
  const [listMitra, setListMitra] = useState([]);
  const [onModalFailedClose, setOnModaModalFailedClose] = useState({
    func: () => {
      console.log("run");
    },
  });

  const [isSuccessRegister, setIsSuccessRegister] = useState(false);
  const [registeredData, setRegisteredData] = useState({
    id: 156499,
    foto: null,
    nama: "MOHAMAD YUSUP",
    nik: "3174025902610004",
    email: "alkhawarizmi2111@gmail.com",
    nomor_handphone: null,
    tempat_lahir: null,
    tanggal_lahir: null,
    jenis_kelamin: null,
    jenjang_id: null,
    jenjang_nama: null,
    address: null,
    provinsi_id: null,
    provinsi_nama: null,
    kota_id: null,
    kota_nama: null,
    kecamatan_id: null,
    kecamatan_nama: null,
    kelurahan_id: null,
    kelurahan_nama: null,
    status_pekerjaan_id: null,
    status_pekerjaan_nama: null,
    pekerjaan_id: null,
    pekerjaan_nama: null,
    perusahaan: null,
    nama_kontak_darurat: null,
    hubungan: null,
    nomor_handphone_darurat: null,
    verifikasi_otp: null,
    email_verifikasi: null,
    handphone_verifikasi: null,
    status_verified: null,
    wizard: null,
  });

  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [isLoadingCheckUser, setIsLoadingCheckUser] = useState(true);

  const [captchaErrorMessage, setCaptchaErrorMessage] = useState();
  const [userCaptchaResponse, setUserCaptchaResponse] = useState(null);

  const [defaultValue, setDefaultValue] = useState({
    name: "",
    nik: "",
    email: "",
    email_mitra: "",
    // nomor_hp: Yup.number()
    //   .typeError(
    //     ValidationMessage.required.replace("__field", "Nomor Handphone")
    //   )
    //   .test(
    //     "minLength",
    //     ValidationMessage.minLength
    //       .replace("__field", "Nomor Handphone")
    //       .replace("__length", 7),
    //     (val) => String(val).length >= 8
    //   )
    //   .test(
    //     "maxLength",
    //     ValidationMessage.maxLength
    //       .replace("__field", "Nomor Handphone")
    //       .replace("__length", 16 - phoneCode.phone_code.length + 1),
    //     (val) => String(val).length <= 16 - phoneCode.phone_code.length
    //   )
    //   .required(
    //     ValidationMessage.required.replace("__field", "Nomor Handphone")
    //   ),
    password: "",
    password_confirmation: "",
  });
  // console.log(onModalFailedClose,"ff")
  // onModalFailedClose.func()
  const closeFailModal = () => {
    showLoadingModal(false);
    setShowFailModal(false);
    // console.log(onModalFailedClose.func, typeof onModalFailedClose.func)
    if (typeof onModalFailedClose.func == "function") {
      const { func } = onModalFailedClose;
      func();
      setOnModaModalFailedClose({ func: () => {} });
    }
  };

  const closeSuccessModal = () => {
    showLoadingModal(false);
    setShowSuccessModal(false);
    showLoadingModal(true);
    router.replace({
      pathname: "/verify-user/verifikasi",
    });
  };

  const resendEmail = async () => {
    try {
      let encrypted = await CacheHelperV2.GetFromCache({
        index: cacheIdRegister,
        force: true,
      });
      const email = JSON.parse(EncryptionHelper.decrypt(encrypted))?.email;
      const resp = await PortalService.sendPinRegister(email);
      if (!resp.data.success) {
        throw Error(resp.data.result.message);
      }
    } catch (err) {
      const message =
        typeof err == "string" ? err : err?.response?.data?.message;
      setServerMessage(message ?? "Terjadi kesalahan.");
      setShowFailModal(true);
    }
  };

  // ambil data mitra
  useEffect(async () => {
    try {
      setIsLoggedIn(PortalHelper.isLoggedin());
      const resp = await PortalService.mitraList("", "Newest", 5, 1);
      if (resp.data.success) {
        setListMitra(resp.data.result.data);
      } else {
        throw Error(resp.data.message);
      }
    } catch (error) {
      console.log(error);
    }
    setIsLoadingGetLinks(false);
  }, []);

  useEffect(async () => {
    if (isLoggedIn) {
      // setLoadingLoggedIn(true)
      const user = PortalHelper.getUserData();
      // console.log(user)
      setRegisteredData({ ...user });
      // setValue("name", user.nama)
      // setValue("nik", user.nik)
      // setValue("email", user.email)
      // setValue("password", "Qwerty!23")
      // setValue("password_confirmation", "Qwerty!23")
      const tmpUser = {
        name: user.nama,
        nik: user.nik,
        email: user.email,
        password: "Qwerty123!",
        password_confirmation: "Qwerty123!",
      };
      setDefaultValue(tmpUser);
      reset(tmpUser);
      // console.log(user)
      // window.alert(isLoggedIn)
      const resp = await ProfilService.cekIsUserMitra();
      // console.log(resp)

      if (resp.data.success) {
        let roleUser = resp.data.result?.data[0];
        if (roleUser?.mrole_id == 4) {
          setIsSuccessRegister(true);
        }
      }
      setIsLoadingCheckUser(false);
      // setTimeout(() => {
      //     setIsLoadingCheckUser(false)
      // }, 2000);
    }
  }, [isLoggedIn]);

  const validationSchema = Yup.object({
    name: Yup.string().required(
      ValidationMessage.required.replace("__field", "Nama Lengkap"),
    ),
    nik: Yup.number()
      .typeError(ValidationMessage.required.replace("__field", "Nomor KTP"))
      .required(ValidationMessage.required.replace("__field", "Nomor KTP"))
      .test(
        "nik",
        ValidationMessage.fixLength
          .replace("__field", "Nomor KTP")
          .replace("__length", 16),
        (val) => String(val).length === 16,
      ),
    email: Yup.string()
      .required(ValidationMessage.required.replace("__field", "Email"))
      .email(ValidationMessage.invalid.replace("__field", "Email")),
    email_mitra: Yup.string()
      .required(ValidationMessage.required.replace("__field", "Email PIC"))
      .email(ValidationMessage.invalid.replace("__field", "Email PIC")),
    // nomor_hp: Yup.number()
    //   .typeError(
    //     ValidationMessage.required.replace("__field", "Nomor Handphone")
    //   )
    //   .test(
    //     "minLength",
    //     ValidationMessage.minLength
    //       .replace("__field", "Nomor Handphone")
    //       .replace("__length", 7),
    //     (val) => String(val).length >= 8
    //   )
    //   .test(
    //     "maxLength",
    //     ValidationMessage.maxLength
    //       .replace("__field", "Nomor Handphone")
    //       .replace("__length", 16 - phoneCode.phone_code.length + 1),
    //     (val) => String(val).length <= 16 - phoneCode.phone_code.length
    //   )
    //   .required(
    //     ValidationMessage.required.replace("__field", "Nomor Handphone")
    //   ),
    password: Yup.string().matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!#%*?&]{8,}$/,
      ValidationMessage.password.replace("__field", "Password"),
    ),
    password_confirmation: Yup.string().oneOf(
      [Yup.ref("password"), null],
      ValidationMessage.match
        .replace("__field", "Konfirmasi Password")
        .replace("__ref", "Password"),
    ),
  });

  const cacheId = "register-data";

  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    getValues,
    reset,
  } = useForm({
    defaultValues: defaultValue,
    mode: "onBlur",
    resolver: yupResolver(validationSchema),
  });
  const router = useRouter();

  const pinResend = async () => {
    try {
      showLoadingModal(true);
      const resp = await PortalService.pinResend();
      if (resp.data.success) {
        setServerMessage(resp.data.message);
        // setShowSuccessModal(true);
        // showLoadingModal(false);
      } else {
        throw Error(resp.data.message);
      }
    } catch (err) {
      console.log(err);
      const message = err.response?.data?.result?.Message
        ? err.response.data.result.Message
        : err.message;
      // showLoadingModal(false);
      setServerMessage(
        message || "Terjadi kesalahan saat mencoba terhubung dengan server!",
      );
      // setShowFailModal(true);
    }
  };

  async function onSubmit(data) {
    showLoadingModal(true);
    // data.nomor_hp = `${phoneCode.phone_code}${data.nomor_hp}`;
    data.email = data.email.toLowerCase();
    try {
      if (userCaptchaResponse == null) {
        throw new CaptchaError("Captcha tidak boleh kosong");
      }
      let res;
      if (isLoggedIn) {
        res = await PortalService.mitraSentOtp(registeredData.email);
      } else {
        res = await PortalService.sendPinRegister(data.email);
      }

      // ProfilService
      showLoadingModal(false);

      if (res.data.success) {
        let dataSimpan = EncryptionHelper.encrypt(JSON.stringify(data));

        PortalHelper.saveUserEmailRegister(dataSimpan);
        await CacheHelperV2.CreateCache({
          data: dataSimpan,
          index: cacheIdRegister,
        });

        // PortalHelper.saveUserData(userData);

        setServerMessage(
          res.data.message ||
            "Periksa email " +
              data.email +
              " untuk mendapatkan kode verifikasi.",
        );
        setShowVerifikasiModal(true);
        // setShowSuccessModal(true);
      } else {
        if (res.data?.result) {
          throw Error(PortalHelper.handleErrorResponse(res.data.result));
        } else {
          throw Error(res.data.message);
        }
      }
      showLoadingModal(false);
    } catch (err) {
      showLoadingModal(false);
      if (err.name == "CaptchaError") {
        setCaptchaErrorMessage(err.message);
      } else {
        console.log(err);
        const message = err.response?.data?.result?.Message
          ? err.response.data.result.Message
          : err.message;

        setServerMessage(
          message || "Terjadi kesalahan saat mencoba terhubung dengan server!",
        );
        setShowFailModal(true);
      }
    }
  }

  async function submitUserData(pin) {
    setOnModaModalFailedClose({ func: () => {} });
    let encrypted = await CacheHelperV2.GetFromCache({
      index: cacheIdRegister,
      force: true,
    });
    const userData = JSON.parse(EncryptionHelper.decrypt(encrypted));
    userData["otp"] = pin;
    try {
      let fd = new FormData();
      Object.keys(userData).forEach((k) => {
        fd.append(k, userData[k]);
      });
      const sendData = await PortalService.registerMitra(fd);
      // const sendData = { data: { success: true, result: { data: { user: { ...registeredData } } } } }
      // const sendData = { data: { success: false, message: "Terjadi kesalahan", } }
      // console.log(sendData);
      if (sendData.data.success) {
        const userData = sendData.data.result.data.user;
        let tmpC = EncryptionHelper.encrypt(JSON.stringify(userData));
        CacheHelperV2.CreateCache({
          index: cacheId + "-data",
          data: tmpC,
          duration: 24,
        });
        // PortalHelper.saveUserToken(sendData.data.result.data.token);
        // const baseData = PortalHelper.userDataBaseObject;

        // PortalHelper.saveUserData(userData);
        setShowVerifikasiModal(false);
        setIsSuccessRegister(true);
        setRegisteredData(userData);
        // router.push("/user-profil");
      } else {
        // console.log(sendData)
        throw sendData;
      }
    } catch (err) {
      // console.log("tr", err);
      const message = err?.data?.message ? err.data.message : err.message;
      let msgString = message;
      let showModalVerify = false;
      if (message == "Validation Error.") {
        const objError = err?.data?.result;
        if (typeof objError == "object") {
          msgString += "<ul>";
          Object.keys(objError).forEach((k) => {
            msgString += `<li>${objError[k][0]} </li>`;
          });
          msgString += "</ul>";
        }
      } else if (message.includes("Tidak Terdaftar")) {
      } else {
        showModalVerify = true;
      }
      setShowVerifikasiModal(false);
      showLoadingModal(false);
      setServerMessage(
        msgString || "Terjadi kesalahan saat mencoba terhubung dengan server!",
      );
      if (showModalVerify) {
        // console.log("setmodalonclose")
        setOnModaModalFailedClose({
          func: () => {
            console.log("verif on");
            setShowVerifikasiModal(true);
          },
        });
      }
      setShowFailModal(true);
    }
  }

  const inputNIKChange = (e) => {
    setinputNIK(e.target.value.length);
  };

  return (
    <>
      <div className="container my-9" style={{ minHeight: "50vh" }}>
        <div className="row mb-8">
          <div className="col-lg-7 mb-6 mb-lg-0 position-relative">
            <div>
              <h3 className="mb-1">Registrasi Mitra Pelatihan</h3>
              <p className="font-size-lg mb-0 text-capitalize">
                Ayo bergabung menjadi mitra pelatihan{" "}
                <span className="text-blue font-weight-medium">
                  #JagoanDigital
                </span>
              </p>
              <hr />
              {isSuccessRegister ? (
                <>
                  <div className="col-12 px-2 mt-7">
                    <div className="alert col-12 bg-green mb-5" role="alert">
                      <div className="row mb-md-0">
                        <div className="row align-items-center">
                          <div className="col-auto px-3">
                            <i className="fa fa-check-circle text-white fa-2x"></i>
                          </div>
                          <div className="col px-3">
                            <h5 className="text-white mb-0">
                              Selamat, pendaftaran anda sebagai mitra telah
                              berhasil.
                            </h5>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-12 col-md-4 mb-6">
                        <img
                          style={{ maxWidth: 200 }}
                          src="/assets/img/illustrations/file-mail.png"
                          alt="..."
                          className="rounded mx-auto d-block"
                        />
                      </div>
                      <div className="col-12 col-md-8 mb-6">
                        <strong className="font-size-14">
                          Rincian akun anda adalah sebagai berikut
                        </strong>
                        <table className="font-size-14 mx-auto table border mt-4">
                          <tbody>
                            <tr>
                              <td width={90}>Nama</td>
                              <td>:</td>
                              <td>{registeredData.nama}</td>
                            </tr>
                            <tr>
                              <td width={90}>NIK</td>
                              <td>:</td>
                              <td>{registeredData.nik}</td>
                            </tr>
                            <tr>
                              <td width={90}>Email</td>
                              <td>:</td>
                              <td>{registeredData.email}</td>
                            </tr>
                          </tbody>
                        </table>
                        <p className="mb-1">
                          Untuk mengelola <strong>pelatihan</strong> dan{" "}
                          <strong>kerjasama</strong> silakan login pada tautan
                          berikut{" "}
                          <a target={"_blank"} href={adminUrl + "signin-mitra"}>
                            klik disini.
                          </a>
                        </p>
                        {/* <p className="mb-1">atau <a>da</a></p> */}
                      </div>
                    </div>
                  </div>
                </>
              ) : (
                <form
                  className="my-5"
                  onSubmit={handleSubmit(onSubmit)}
                  ref={registerForm}
                >
                  <div className="row">
                    <div className="col-12 col-md-6">
                      <div className="form-group mb-5">
                        <div className="row align-items-end" data-aos="fade-up">
                          <div className="col-md mb-4 mb-md-0">
                            <label for="nama">
                              Nama Lengkap
                              <span
                                className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                                title="required"
                              >
                                *
                              </span>
                            </label>
                          </div>
                        </div>
                        <input
                          type="text"
                          className={`form-control form-control-sm ${
                            errors.name ? "is-invalid" : ""
                          } ${isLoggedIn ? "disabled" : ""}`}
                          id="nama"
                          disabled={isLoggedIn}
                          placeholder=""
                          name="name"
                          style={{ textTransform: "uppercase" }}
                          {...register("name")}
                          {...PortalHelper.addNameValidation()}
                          // value={getValues("name")}
                        />
                        <span className="form-caption font-size-sm text-italic my-2">
                          Sesuai KTP
                        </span>{" "}
                        <br />
                        <span className="text-red">{errors.name?.message}</span>
                      </div>
                    </div>
                    <div className="col-12 col-md-6">
                      <div className="form-group mb-5">
                        <div className="row align-items-end" data-aos="fade-up">
                          <div className="col-md mb-4 mb-md-0">
                            <label for="nik">
                              Nomor KTP (NIK)
                              <span
                                className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                                title="required"
                              >
                                *
                              </span>
                            </label>
                          </div>
                        </div>
                        <div className="input inner-addon right-addon">
                          <span className="icon">{inputNIK}</span>
                          <input
                            type="text"
                            onKeyDown={(e) => {
                              PortalHelper.inputNumberOnly(e);
                            }}
                            onKeyUpCapture={(e) => {
                              inputNIKChange(e);
                              PortalHelper.keyup(e);
                            }}
                            disabled={isLoggedIn}
                            className={`form-control form-control-sm ${
                              errors.nik ? "is-invalid" : ""
                            } ${isLoggedIn ? "disabled" : ""}`}
                            id="nik"
                            placeholder=""
                            name="nik"
                            // value={getValues("nik")}
                            {...register("nik")}
                            maxLength={16}
                          />
                        </div>
                        <span className="form-caption font-size-sm text-italic my-2">
                          Sesuai KTP
                        </span>{" "}
                        <br />
                        <span className="text-red">{errors.nik?.message}</span>
                      </div>
                    </div>
                    <div className="col-12">
                      <div className="form-group mb-5">
                        <div className="row align-items-end" data-aos="fade-up">
                          <div className="col-md mb-4 mb-md-0">
                            <label for="email">
                              Email
                              <span
                                className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                                title="required"
                              >
                                *
                              </span>
                            </label>
                          </div>
                        </div>
                        <input
                          type="text"
                          disabled={isLoggedIn}
                          className={`form-control form-control-sm ${
                            errors.email ? "is-invalid" : ""
                          } ${isLoggedIn ? "disabled" : ""}`}
                          id="email"
                          placeholder=""
                          name="email"
                          // value={getValues("email")}
                          {...register("email")}
                        />
                        <span className="form-caption font-size-sm text-italic my-2">
                          Masukkan email yang valid
                        </span>{" "}
                        <br />
                        <span className="text-red">
                          {errors.email?.message}
                        </span>
                      </div>
                    </div>
                    {!isLoggedIn && (
                      <>
                        <div className="col-12 col-md-6">
                          <div className="form-group mb-5 password-input">
                            <div
                              className="row align-items-end"
                              data-aos="fade-up"
                            >
                              <div className="col-md mb-4 mb-md-0">
                                <label for="pass">
                                  Password
                                  <span
                                    className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                                    title="required"
                                  >
                                    *
                                  </span>
                                </label>
                              </div>
                            </div>
                            <div className="input inner-addon right-addon">
                              <span
                                className="icon"
                                onClick={() => {
                                  setShowPassword(!showPassword);
                                }}
                              >
                                <FlipMove>
                                  {!showPassword && (
                                    <i className="fa fa-eye grey"></i>
                                  )}
                                  {showPassword && (
                                    <i className="fa fa-eye-slash "></i>
                                  )}
                                </FlipMove>
                              </span>
                              <input
                                placeholder="***********"
                                type={showPassword ? "text" : "password"}
                                className={`form-control form-control-sm ${
                                  errors.password ? "is-invalid" : ""
                                }`}
                                id="pass"
                                name="password"
                                // value={getValues("password")}
                                {...register("password")}
                              />
                            </div>
                            <span className="form-caption font-size-sm text-italic my-2">
                              {!errors.password
                                ? "Minimal 8 Karakter kombinasi huruf kapital, huruf kecil, angka dan simbol."
                                : ""}
                            </span>
                            <span className="text-red">
                              {errors.password?.message}
                            </span>
                          </div>
                        </div>
                        <div className="col-12 col-md-6">
                          <div className="form-group mb-5">
                            <div
                              className="row align-items-end"
                              data-aos="fade-up"
                            >
                              <div className="col-md mb-4 mb-md-0">
                                <label for="pass2">
                                  Konfirmasi Password
                                  <span
                                    className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                                    title="required"
                                  >
                                    *
                                  </span>
                                </label>
                              </div>
                            </div>
                            <div className="input inner-addon right-addon">
                              <span
                                className="icon"
                                onClick={() => {
                                  setShowPasswordConf(!showPasswordConf);
                                }}
                              >
                                <FlipMove>
                                  {!showPasswordConf && (
                                    <i className="fa fa-eye grey"></i>
                                  )}
                                  {showPasswordConf && (
                                    <i className="fa fa-eye-slash "></i>
                                  )}
                                </FlipMove>
                              </span>
                              <input
                                placeholder="***********"
                                type={showPasswordConf ? "text" : "password"}
                                className={`form-control form-control-sm ${
                                  errors.password_confirmation
                                    ? "is-invalid"
                                    : ""
                                }`}
                                id="pass2"
                                name="password_confirmation"
                                // value={getValues("password_confirmation")}
                                {...register("password_confirmation")}
                              />
                            </div>
                            <span className="text-red">
                              {errors.password_confirmation?.message}
                            </span>
                          </div>
                        </div>
                      </>
                    )}
                    <div className="col-12">
                      <div className="form-group mb-5">
                        <div className="row align-items-end" data-aos="fade-up">
                          <div className="col-md mb-4 mb-md-0">
                            <label for="email">
                              Email PIC Mitra yg terdaftar
                              <span
                                className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                                title="required"
                              >
                                *
                              </span>
                            </label>
                          </div>
                        </div>
                        <input
                          type="text"
                          // disabled={isLoggedIn}
                          className={`form-control form-control-sm ${
                            errors.email_mitra ? "is-invalid" : ""
                          } ${isLoggedIn ? "disabled" : ""}`}
                          id="email"
                          placeholder=""
                          name="email"
                          // value={getValues("email_mitra")}
                          {...register("email_mitra")}
                        />
                        <span className="form-caption font-size-sm text-italic my-2">
                          Masukkan email yang valid
                        </span>{" "}
                        <br />
                        <span className="text-red">
                          {errors.email_mitra?.message}
                        </span>
                      </div>
                    </div>
                    <div className="d-flex align-items-center mb-5 font-size-sm">
                      <div>
                        <ReCAPTCHA
                          sitekey={process.env.CAPTCHA_SITEKEY}
                          onChange={(e) => {
                            setUserCaptchaResponse(e);
                          }}
                        ></ReCAPTCHA>
                      </div>
                      {!userCaptchaResponse && captchaErrorMessage && (
                        <span className="text-red">{captchaErrorMessage}</span>
                      )}
                    </div>
                    <div className="form-row place-order">
                      {/* verified.html                    */}
                      {!isLoggedIn ? (
                        <button
                          className="btn btn-blue btn-block"
                          type="submit"
                        >
                          DAFTAR SEBAGAI MITRA
                        </button>
                      ) : (
                        <button
                          className={`btn btn-blue btn-block ${
                            isLoadingCheckUser ? "disabled" : ""
                          }`}
                          disabled={isLoadingCheckUser}
                          type="submit"
                        >
                          {isLoadingCheckUser ? (
                            <>
                              <span
                                class="spinner-border spinner-border-sm"
                                role="status"
                                aria-hidden="true"
                              ></span>{" "}
                              Pengecekan Data
                            </>
                          ) : (
                            "DAFTAR SEBAGAI MITRA"
                          )}
                        </button>
                      )}
                    </div>
                  </div>
                </form>
              )}
            </div>
          </div>

          <div className="col-lg-5">
            <div className="d-block rounded p-2 mb-6 bg-light">
              <div className="pt-6 pb-5 px-5 px-lg-3 px-xl-5">
                <div className="d-flex align-items-center">
                  <div className="mb-4">
                    <h4 className="mb-4">Mitra Pelatihan</h4>
                    <p className="font-size-base mb-0 text-capitalize">
                      Ayo bergabung dan menjadi bagian dari digitalent kominfo.
                      Beberapa mitra aktif kami
                    </p>
                  </div>
                </div>
                <div className="row">
                  {listMitra.map((mitra, i) => (
                    <div className="col-12 mb-4 px-2" key={i}>
                      <a
                        href={PortalHelper.setUpUrl(mitra.website, true, true)}
                        target="__blank"
                      >
                        <div className="row align-items-center px-2">
                          <div className="col-auto">
                            <div className="avatar avatar-custom d-inline">
                              <img
                                src={PortalHelper.urlMitraLogo(
                                  mitra.agency_logo,
                                )}
                                alt={mitra.nama_mitra}
                                className="avatar-img avatar-md img-fluid"
                              />
                            </div>
                          </div>

                          <div className="col px-3">
                            <div className="card-body p-0">
                              <h6 className="mb-0 line-clamp-1">
                                {mitra.nama_mitra}
                              </h6>
                              <p className="mb-0 line-clamp-1">
                                {mitra.website}
                              </p>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Failed
        show={showFailModal}
        handleClose={closeFailModal}
        children={serverMessage}
        useHtml={true}
      />
      <Success
        show={showSuccessModal}
        handleClose={closeSuccessModal}
        children={serverMessage}
      />
      <VerifikasiEmail
        show={showVerifikasiModal}
        handleClose={() => {
          setShowVerifikasiModal(false);
        }}
        children={serverMessage}
        onVerify={({ pin }) => {
          submitUserData(pin);
        }}
        onResend={() => {
          resendEmail;
        }}
      />
    </>
  );
}
