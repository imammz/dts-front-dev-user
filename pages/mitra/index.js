import React, { useState, useEffect } from "react";
import Link from "next/link";
import axios from "axios";
import Moment from "react-moment";
import "moment/locale/id";
import PortalService from "./../../services/PortalService";
import Select from "react-select";

import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

import FlipMove from "react-flip-move";
import { useRouter } from "next/router";
import Pagination from "../../components/Pagination";
import SideWidget from "../../components/SideWidget";
import PortalHelper from "../../src/helper/PortalHelper";
import EncryptionHelper from "../../src/helper/EncryptionHelper";

function ListMitra() {
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [perPage, setPerPage] = useState(20);
  const [dataMitra, setDataMitra] = useState(null);
  const [skeletons, setSkeletons] = useState([...Array(10).keys()]);
  const [metaData, setMetaData] = useState(null);
  const [sortBy, setSortBy] = useState("Alphabet");
  const page = router.query.page ? router.query.page : 1;

  useEffect(async () => {
    try {
      if (router.isReady) {
        setLoading(true);
        const resp = await PortalService.mitraList(
          router.query.akademiSlug ?? "",
          sortBy,
          perPage,
          page,
        );
        if (resp.data.success) {
          setDataMitra(resp.data.result.data);
          setMetaData(resp.data.result.meta);
        } else {
          throw Error(resp.data.message);
        }
        setLoading(false);
      }
    } catch (err) {
      console.error(err);
    }
  }, [sortBy, page, perPage, router]);

  return (
    <>
      <header
        className="py-6 bg-blue mb-6 z-index-0"
        style={{ backgroundImage: "none" }}
      >
        <div className="container">
          <div className="row align-items-end" data-aos="fade-up">
            {/*<nav aria-label="breadcrumb">
              <ol className="breadcrumb breadcrumb-scroll">
                <li className="breadcrumb-item">
                  <Link href="/">
                    <a className="text-gray-700">Home</a>
                  </Link>
                </li>
                <li className="breadcrumb-item">
                  <a className="text-gray-700">Tentang Kami</a>
                </li>
                <li
                  className="breadcrumb-item text-blue active"
                  aria-current="page"
                >
                  Mitra
                </li>
              </ol>
            </nav>*/}
            <div className="col-md mb-md-0">
              <h3 className="fw-bolder text-white mb-0">Mitra Pelatihan</h3>
              <p className="mb-lg-0 text-white">
                Para Mitra yang telah menjadi bagian dalam program pelatihan
              </p>
              {/*<p className="font-size-lg mb-lg-0">
                Tertarik menjadi mitra pelatihan ?{" "}
                <Link href={"/mitra/daftar"}>Daftar disini !</Link>
              </p>*/}
            </div>
            <div className="col-md-auto col-sm-12">
              <div className="d-lg-flex rounded">
                <div className="d-lg-flex flex-wrap">
                  <div>
                    <div className="d-flex align-items-center h-50p">
                      <div className="form-floating col-12">
                        <select
                          value={sortBy}
                          onChange={(e) => {
                            setSortBy(e.target.value);
                          }}
                          className="form-select form-select-sm form-control-sm ps-5 shadow-none text-primary fw-medium rounded-xl"
                        >
                          <option value="Newest">Terbaru</option>
                          <option value="Alphabet">Alphabet</option>
                          <option value="Pelatihan">Pelatihan</option>
                        </select>
                        <label className="ps-5">Urutkan</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>

      <div className="container z-index-0">
        <div className="row mb-9">
          <div className="col-xl-9 pt-lg-6">
            {loading && (
              <ul className="nav row" id="pills-tab" role="tablist">
                {skeletons.map((index) => (
                  <li
                    className="nav-item col-lg-4 mb-4"
                    role="presentation"
                    key={index}
                  >
                    <div className="nav-link p-0">
                      <div className="card border p-3 lift rounded-5">
                        <div
                          href="#"
                          className="card icon-category icon-category-sm"
                        >
                          <div className="row align-items-center row-cols-2">
                            <div className="col-auto">
                              <Skeleton circle height={50} width={50} />
                            </div>

                            <div className="col px-3">
                              <div className="card-body p-0">
                                <Skeleton className="mb-0 line-clamp-1" />
                                <Skeleton className="mb-0 line-clamp-1" />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                ))}
              </ul>
            )}
            {!loading && (
              <ul>
                <div className="row">
                  {dataMitra &&
                    dataMitra[0] != null &&
                    dataMitra.map((mitra) => (
                      <li
                        className="nav-item col-lg-4 mb-4"
                        role="presentation"
                        key={mitra.id}
                      >
                        <Link href={`/mitra/${[mitra.id]}`}>
                          <a className="nav-link p-0">
                            <div className="card border p-3 lift rounded-5">
                              <div href="#" className="card">
                                <img
                                  //   src={mitra.agency_logo}
                                  src={`${PortalHelper.urlMitraLogo(
                                    mitra.agency_logo,
                                  )}`}
                                  alt={mitra.nama_mitra}
                                  className="avatar-img avatar-xl ms-3"
                                />
                                <div className="card-body ms-3 mb-2 p-0">
                                  <h6 className="fw-bold mb-n1 me-3 line-clamp-1">
                                    {mitra.nama_mitra}
                                  </h6>
                                  <p className="mb-0 fw-semi-bold font-size-12 text-muted">
                                    {mitra.jml_pelatihan} Pelatihan
                                  </p>
                                </div>
                              </div>
                            </div>
                          </a>
                        </Link>
                      </li>
                    ))}
                </div>
              </ul>
            )}

            <div className="d-flex justify-content-center align-items-center my-9">
              <Pagination
                pageSize={metaData?.last_page}
                currentPage={page}
              ></Pagination>
            </div>
          </div>
          <div className="col-xl-3 pt-lg-6 px-4 mb-xl-0">
            <div className="card rounded-5 border px-5 pt-6 pb-6 mb-5">
              <div className="d-inline-block rounded-circle mb-4">
                <div
                  className="icon-circle"
                  style={{ backgroundColor: "#c2dffb", color: "#196ECD" }}
                >
                  <i className="fa fa-handshake fa-2x"></i>
                </div>
              </div>
              <h5>Menjadi Mitra</h5>
              <p className="font-size-12 mb-5">
                Ayo, bergabung menjadi mitra pelatihan untuk mencetak talenta
                digital Indonesia
              </p>
              <a
                href="/mitra/daftar"
                className="btn btn-outline-primary btn-xs btn-pill mb-1"
              >
                Registrasi Mitra
              </a>
              <div className="row">
                <div className="col-12 text-center">
                  <p className="font-size-12 my-2 text-center">
                    Sudah Punya Akun? &nbsp;
                    <Link href={process.env.ADMIN_BASE_URL + "/signin-mitra"}>
                      <a
                        className="text-blue font-size-12 text-underline"
                        onClick={() => {
                          // showLoadingModal(true);
                        }}
                      >
                        Login
                      </a>
                    </Link>
                  </p>
                </div>
              </div>
            </div>

            <SideWidget loading={loading} />
          </div>
        </div>
      </div>
    </>
  );
}
export default ListMitra;
