// wizard resgitrasi
import { yupResolver } from "@hookform/resolvers/yup";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import FlipMove from "react-flip-move";
import ReCAPTCHA from "react-google-recaptcha";
import { useForm } from "react-hook-form";
import * as Yup from "yup";
import ButtonWithLoading from "../../components/ButtonWithLoading";
import CountryCode from "../../public/assets/json/phone_code.js";
import GeneralService from "../../services/GeneralService";
import PortalService from "../../services/PortalService";
import NotificationHelper from "../../src/helper/NotificationHelper";
import PortalHelper from "../../src/helper/PortalHelper";
import Link from "next/link";
import {
  default as ValidationMessage,
  default as validationMessage,
} from "../../src/validation-message";

// import eventBus
let regexValidation = {
  email:
    /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
  url: /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/,
  number: /^[0-9]+$/,
  date: /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/,
  password:
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!#%*?&]{8,}$/,
};

const lembagaSchema = Yup.object({
  nama: Yup.string().required(
    ValidationMessage.required.replace("__field", "Nama Lembaga/Institusi"),
  ),
  website: Yup.string()
    .matches(
      regexValidation.url,
      ValidationMessage.url.replace("__field", "Format link harus"),
    )
    .nullable(true)
    .transform((value, originalValue) =>
      originalValue.trim() === "" ? null : value,
    ),
  email: Yup.string()
    .required(
      ValidationMessage.required.replace("__field", "Email Lembaga/Institusi"),
    )
    .email(
      ValidationMessage.invalid.replace("__field", "Email Lembaga/Institusi"),
    ),
  agency_logo: Yup.mixed()
    .test(
      "required",
      ValidationMessage.required.replace("__field", "Logo Lembaga/Institusi"),
      (files) => {
        if (files.length > 0) return true;
        return false;
      },
    )
    .test(
      "fileSize",
      ValidationMessage.fileSize
        .replace("__field", "Logo Lembaga/Institusi")
        .replace("_max", "2MB"),
      (files) => {
        if (files.length == 0) return true;
        return files[0].size <= 2000000;
      },
    ),
  asal: Yup.mixed().test(
    "required",
    ValidationMessage.required.replace("__field", "Asal"),
    (value) => {
      if (value) return true;
      return false;
    },
  ),
  provinsi: Yup.string().when("asal", {
    is: (value) => value == "2",
    then: Yup.string().required(
      ValidationMessage.required.replace("__field", "Provinsi"),
    ),
  }),
  kabupaten: Yup.string().when("asal", {
    is: (value) => value == "2",
    then: Yup.string().required(
      ValidationMessage.required.replace("__field", "Kabupaten/Kota"),
    ),
  }),
  negara: Yup.string().when("asal", {
    is: (value) => value == "1",
    then: Yup.string().required(
      ValidationMessage.required.replace("__field", "Negara"),
    ),
  }),
  alamat: Yup.string().required(
    ValidationMessage.required.replace("__field", "Alamat"),
  ),
  kode_pos: Yup.number()
    .typeError(ValidationMessage.invalid.replace("__field", "Kode Pos"))
    .required(ValidationMessage.required.replace("__field", "Kode Pos")),
});

const finalizeSchema = Yup.object({
  nama_pic: Yup.string().required(
    ValidationMessage.required.replace("__field", "Nama PIC"),
  ),
  nik_pic: Yup.number()
    .typeError(ValidationMessage.nik.replace("__field", "Nomor KTP"))
    .required(ValidationMessage.required.replace("__field", "Nomor KTP"))
    .test(
      "nik",
      ValidationMessage.fixLength
        .replace("__field", "Nomor KTP")
        .replace("__length", 16),
      (val) => String(val).length === 16,
    ),
  hp_pic: Yup.number()
    .typeError(ValidationMessage.required.replace("__field", "Nomor Handphone"))
    .test(
      "minLength",
      ValidationMessage.minLength
        .replace("__field", "Nomor Handphone")
        .replace("__length", 7),
      (val) => String(val).length >= 8,
    )
    .test(
      "maxLength",
      ValidationMessage.maxLength
        .replace("__field", "Nomor Handphone")
        .replace("__length", 16 - 3 + 1),
      (val) => String(val).length <= 16 - 3,
    )
    .required(ValidationMessage.required.replace("__field", "Nomor Handphone")),
  email_pic: Yup.string()
    .required(ValidationMessage.required.replace("__field", "Email"))
    .email(ValidationMessage.invalid.replace("__field", "Email")),
  password: Yup.string().matches(
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!#%*?&])[A-Za-z\d@$!#%*?&]{8,}$/,
    ValidationMessage.password.replace("__field", "Password"),
  ),
});

const validationSchema = Yup.object({});

const useNegara = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    (async () => {
      const res = await GeneralService.negara();
      if (res?.data?.success) setData(res?.data?.result);
    })();
  }, []);

  return data;
};

const useProvinsi = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    (async () => {
      const res = await GeneralService.provinsi();
      if (res?.data?.success) setData(res?.data?.result);
    })();
  }, []);

  return data;
};

const useKotaKabupaten = (idProvinsi) => {
  const [data, setData] = useState([]);
  useEffect(() => {
    (async () => {
      const res = await GeneralService.kota(idProvinsi);
      if (res?.data?.success) setData(res?.data?.result);
    })();
  }, [idProvinsi]);

  return data;
};

const useLogginUser = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    (async () => {
      const res = PortalHelper.getUserData();
      setData(res);
    })();
  }, []);

  return data;
};

const RegisterLembaga = ({
  form: {
    register,
    handleSubmit,
    reset,
    formState: { errors },
    getValues,
    setError,
    setFocus,
    watch,
    setValue,
  },
}) => {
  const dataProvinsi = useProvinsi();
  const dataKotaKabupaten = useKotaKabupaten(watch("provinsi"));
  const dataNegara = useNegara();

  const agency_logo = (watch("agency_logo") || [])[0];
  useEffect(() => {
    if (agency_logo) {
      const r = new FileReader();
      r.onload = () => {
        setValue("agency_logo_base64", r.result);
      };
      r.readAsDataURL(agency_logo);
    }
  }, [agency_logo]);

  return (
    <>
      <div className="row mb-4">
        <div className="col-12">
          <h3 className="fw-bolder text-muted mb-5">Data Lembaga/Institusi</h3>
          <div className="form-group mb-4">
            <div className="row align-items-end" data-aos="fade-up">
              <div className="col-md mb-2 mb-md-0">
                <label for="nama" className="font-size-14 fw-bold">
                  Nama Lembaga/Institusi
                  <span
                    className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                    title="required"
                  >
                    *
                  </span>
                </label>
              </div>
            </div>
            <input
              type="text"
              className={`form-control form-control-sm`}
              placeholder="Masukkan nama lembaga/institusi"
              autoComplete="off"
              {...register("nama", {
                // required: 'Nama harus terisi'
              })}
            />
            <span className="text-red">{errors.nama?.message}</span>
          </div>
        </div>
      </div>
      <div className="row mb-4">
        <div className="col-12 col-md-6">
          <div className="form-group mb-4">
            <div className="row align-items-end" data-aos="fade-up">
              <div className="col-md mb-2 mb-md-0">
                <label className="font-size-14 fw-bold">
                  Email Lembaga/Institusi
                  <span
                    className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                    title="required"
                  >
                    *
                  </span>
                </label>
              </div>
            </div>
            <input
              type="text"
              className={`form-control form-control-sm`}
              placeholder="Masukkan Email Lembaga/Institusi"
              autoComplete="off"
              name="email"
              {...register("email", {
                // required: 'Email harus terisi',
                // pattern: {
                //   value: regexValidation.email,
                //   message: 'Email tidak valid',
                // },
              })}
            />
            <span className="text-red">{errors.email?.message}</span>
          </div>
        </div>
        <div className="col-12 col-md-6">
          <div className="form-group mb-4">
            <div className="row align-items-end" data-aos="fade-up">
              <div className="col-md mb-2 mb-md-0">
                <label className="font-size-14 fw-bold">
                  Website Lembaga/Institusi
                </label>
              </div>
            </div>
            <input
              type="text"
              className={`form-control form-control-sm`}
              placeholder="Contoh: https://digitalent.kominfo.go.id"
              autoComplete="off"
              {...register("website", {
                // required: 'Website harus terisi',
                // pattern: {
                //   value: regexValidation.url,
                //   message: 'Website tidak valid',
                // },
              })}
            />
            <span className="text-red">{errors.website?.message}</span>
          </div>
        </div>
      </div>
      <div className="row mb-4">
        <div className="col-12">
          <div className="form-group mb-4">
            <div className="row align-items-end" data-aos="fade-up">
              <div className="col-12 mb-2 mb-md-0">
                <label for="logo" className="font-size-14 fw-bold">
                  Logo Lembaga/Institusi
                  <span
                    className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                    title="required"
                  >
                    *
                  </span>
                </label>
              </div>
            </div>
            <input
              type="file"
              className={`form-control form-control-sm`}
              placeholder="Upload logo Institusi"
              autoComplete="off"
              {...register("agency_logo", {
                // required: 'Logo harus terisi',
              })}
              accept=".png,.svg,.jpg,.jpeg"
            />
            <span className="text-red">{errors.agency_logo?.message}</span>
          </div>
        </div>
      </div>
      <div className="row mb-4">
        <div className="col-12 col-md-6">
          <div className="form-group mb-4">
            <div className="row align-items-end" data-aos="fade-up">
              <div className="col-md mb-2 mb-md-0">
                <label for="asal" className="font-size-14 fw-bold">
                  Asal Lembaga/Institusi
                  <span
                    className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                    title="required"
                  >
                    *
                  </span>
                </label>
              </div>
            </div>
            <div className="d-block">
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="radio"
                  name="inlineRadioOptions"
                  id="inlineRadio1"
                  value="2"
                  {...register("asal", {
                    required: "Asal harus terisi",
                  })}
                />
                <label className="form-check-label" for="inlineRadio1">
                  Dalam Negeri
                </label>
              </div>
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="radio"
                  name="inlineRadioOptions"
                  id="inlineRadio2"
                  value="1"
                  {...register("asal", {
                    required: "Asal harus terisi",
                  })}
                />
                <label className="form-check-label" for="inlineRadio2">
                  Luar Negeri
                </label>
              </div>
            </div>
            <span className="text-red">{errors.asal?.message}</span>
          </div>
        </div>
      </div>
      {watch("asal") == "2" && (
        <>
          <div className="row mb-4">
            <div className="col-12 mb-5">
              <div className="form-group">
                <label for="prov" className="font-size-14 fw-bold">
                  Provinsi&nbsp;<span className="text-red">*</span>
                </label>
                <select
                  className={`form-select form-select-sm`}
                  {...register("provinsi", {
                    // required: 'Provinsi harus terisi',
                  })}
                >
                  <option value="">Pilih Provinsi Domisili</option>
                  {dataProvinsi.map((i) => (
                    <option key={i.id} value={i.id}>
                      {i.name}
                    </option>
                  ))}
                </select>
                <span className="text-red">{errors.provinsi?.message}</span>
              </div>
            </div>
          </div>
          <div className="row mb-4">
            <div className="col-12 mb-5">
              <div className="form-group">
                <label for="kota/kab" className="font-size-14 fw-bold">
                  Kota/Kabupaten&nbsp;<span className="text-red">*</span>
                </label>
                <select
                  className={`form-select form-select-sm`}
                  {...register("kabupaten", {
                    // required: 'Kabupaten harus terisi',
                  })}
                >
                  <option value="">Pilih Kota/Kabupaten Domisili</option>
                  {dataKotaKabupaten.map((i) => (
                    <option key={i.id} value={i.id}>
                      {i.name}
                    </option>
                  ))}
                </select>
                <span className="text-red">{errors.kabupaten?.message}</span>
              </div>
            </div>
          </div>
        </>
      )}
      {watch("asal") == "1" && (
        <>
          <div className="row mb-4">
            <div className="col-12 mb-5">
              <div className="form-group">
                <label for="negara" className="font-size-14 fw-bold">
                  Negara&nbsp;<span className="text-red">*</span>
                </label>
                <select
                  className={`form-select form-select-sm`}
                  {...register("negara", {
                    // required: 'Negara harus terisi',
                  })}
                >
                  <option value="">Pilih Negara Domisili</option>
                  {dataNegara.map((i) => (
                    <option key={i.id} value={i.id}>
                      {i.country_name}
                    </option>
                  ))}
                </select>
                <span className="text-red">{errors.negara?.message}</span>
              </div>
            </div>
          </div>
        </>
      )}
      <div className="row mb-4">
        <div className="col-12 ">
          <div className="form-group mb-4">
            <div className="row align-items-end" data-aos="fade-up">
              <div className="col-md mb-2 mb-md-0">
                <label for="alamat" className="font-size-14 fw-bold">
                  Alamat Lembaga/Institusi
                  <span
                    className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                    title="required"
                  >
                    *
                  </span>
                </label>
              </div>
            </div>
            <textarea
              type="text"
              className={`form-control form-control-sm`}
              placeholder="Masukkan alamat Lembaga/Institusi"
              autoComplete="off"
              style={{ resize: "none" }}
              {...register("alamat", {
                // required: 'Alamat harus terisi',
              })}
            />
            <span className="text-red">{errors.alamat?.message}</span>
          </div>
        </div>
      </div>
      <div className="row mb-4">
        <div className="col-12 ">
          <div className="form-group mb-4">
            <div className="row align-items-end" data-aos="fade-up">
              <div className="col-md mb-2 mb-md-0">
                <label for="zip" className="font-size-14 fw-bold">
                  Kode Pos
                  <span
                    className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                    title="required"
                  >
                    *
                  </span>
                </label>
              </div>
            </div>
            <input
              type="text"
              className={`form-control form-control-sm number-only`}
              placeholder="Masukkan Kode Pos"
              autoComplete="off"
              {...register("kode_pos", {
                // required: 'Kode Pos harus terisi',
              })}
            />
            <span className="text-red">{errors.kode_pos?.message}</span>
          </div>
        </div>
      </div>
    </>
  );
};

const RegisterMitra = ({
  form: {
    register,
    handleSubmit,
    reset,
    formState: { errors },
    getValues,
    setError,
    setFocus,
    watch,
  },
}) => {
  const [showPassword, setShowPassword] = useState(false);
  const [showPasswordConf, setShowPasswordConf] = useState(false);

  return (
    <>
      <h3 className="mb-4">Pembuatan Akun</h3>

      <div className="row mb-4">
        <div className="col-12 col-md-6">
          <div className="form-group mb-5">
            <div className="row align-items-end" data-aos="fade-up">
              <div className="col-md mb-2 mb-md-0">
                <label for="nama">
                  Nama Lengkap
                  <span
                    className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                    title="required"
                  >
                    *
                  </span>
                </label>
              </div>
            </div>
            <input
              type="text"
              className={`form-control form-control-sm`}
              placeholder="Masukkan Nama Lengkap Anda"
              style={{ textTransform: "uppercase" }}
              autoComplete="off"
              {...register("nama", {
                // required: 'Nama harus terisi'
              })}
            />
            <span className="form-caption font-size-sm text-italic my-2">
              Sesuai KTP
            </span>
            <br />
            <span className="text-red">{errors.nama?.message}</span>
          </div>
        </div>

        <div className="col-12 col-md-6">
          <div className="form-group mb-5">
            <div className="row align-items-end" data-aos="fade-up">
              <div className="col-md mb-4 mb-md-0">
                <label for="nik">
                  Nomor KTP (NIK)
                  <span
                    className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                    title="required"
                  >
                    *
                  </span>
                </label>
              </div>
            </div>
            <div className="input inner-addon right-addon">
              <span className="icon">{(watch("nik") || "").length}</span>
              <input
                type="text"
                // onKeyDown={(e) => {
                //   console.log(e.key, e.code)
                //   PortalHelper.inputNumberOnly(e);
                // }}
                // onKeyUpCapture={(e) => {
                //   PortalHelper.keyup(e);
                // }}
                autoComplete="off"
                disabled={PortalHelper.isLoggedin()}
                className={`form-control form-control-sm ${
                  errors.nik ? "is-invalid" : ""
                } ${PortalHelper.isLoggedin() ? "disabled" : ""}`}
                {...register("nik", {
                  required: "NIK harus terisi",
                  pattern: {
                    value: regexValidation.number,
                    message: "NIK tidak valid",
                  },
                })}
                maxLength={16}
              />
            </div>
            <span className="form-caption font-size-sm text-italic my-2">
              Sesuai KTP
            </span>
            <br />
            <span className="text-red">{errors.nik?.message}</span>
          </div>
        </div>

        <div className="col-12 col-md-6">
          <div className="form-group mb-4">
            <div className="row align-items-end" data-aos="fade-up">
              <div className="col-md mb-2 mb-md-0">
                <label for="nama">
                  Nomor Handphone
                  <span
                    className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                    title="required"
                  >
                    *
                  </span>
                </label>
              </div>
            </div>
            <input
              type="text"
              className={`form-control form-control-sm`}
              placeholder="Masukkan Nomor Handphone"
              autoComplete="off"
              {...register("hp", {
                required: "HP harus terisi",
                pattern: {
                  value: regexValidation.number,
                  message: "HP tidak valid",
                },
              })}
            />

            <span className="text-red">{errors.hp?.message}</span>
          </div>
        </div>

        <div className="col-12 col-md-6">
          <div className="form-group mb-5">
            <div className="row align-items-end" data-aos="fade-up">
              <div className="col-md mb-4 mb-md-0">
                <label for="email">
                  Email
                  <span
                    className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                    title="required"
                  >
                    *
                  </span>
                </label>
              </div>
            </div>
            <input
              type="text"
              // disabled={PortalHelper.isLoggedin()}
              className={`form-control form-control-sm ${
                errors.email ? "is-invalid" : ""
              } ${PortalHelper.isLoggedin() ? "disabled" : ""}`}
              placeholder="Masukkan Email Anda"
              autoComplete="off"
              {...register("email", {
                required: "Email harus terisi",
                pattern: {
                  value: regexValidation.email,
                  message: "Email tidak valid",
                },
              })}
            />
            <span className="form-caption font-size-sm text-italic my-2">
              Masukkan email yang valid
            </span>{" "}
            <br />
            <span className="text-red">{errors.email?.message}</span>
          </div>
        </div>

        {
          // !PortalHelper.isLoggedin() &&
          <>
            <div className="col-12 col-md-6">
              <div className="form-group mb-5 password-input">
                <div className="row align-items-end" data-aos="fade-up">
                  <div className="col-md mb-4 mb-md-0">
                    <label for="pass">
                      Password
                      <span
                        className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                        title="required"
                      >
                        *
                      </span>
                    </label>
                  </div>
                </div>
                <div className="input inner-addon right-addon">
                  <span
                    className="icon"
                    onClick={() => {
                      setShowPassword(!showPassword);
                    }}
                  >
                    <FlipMove>
                      {!showPassword && <i className="fa fa-eye grey"></i>}
                      {showPassword && <i className="fa fa-eye-slash "></i>}
                    </FlipMove>
                  </span>
                  <input
                    placeholder="***********"
                    type={showPassword ? "text" : "password"}
                    className={`form-control form-control-sm ${
                      errors.password ? "is-invalid" : ""
                    }`}
                    {...register("password", {
                      required: "Password harus terisi",
                    })}
                  />
                </div>
                <span className="form-caption font-size-sm text-italic my-2">
                  {!errors.password ? validationMessage.password : ""}
                </span>
                <span className="text-red">{errors.password?.message}</span>
              </div>
            </div>
            <div className="col-12 col-md-6">
              <div className="form-group mb-5">
                <div className="row align-items-end" data-aos="fade-up">
                  <div className="col-md mb-4 mb-md-0">
                    <label for="pass2">
                      Konfirmasi Password
                      <span
                        className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                        title="required"
                      >
                        *
                      </span>
                    </label>
                  </div>
                </div>
                <div className="input inner-addon right-addon">
                  <span
                    className="icon"
                    onClick={() => {
                      setShowPasswordConf(!showPasswordConf);
                    }}
                  >
                    <FlipMove>
                      {!showPasswordConf && <i className="fa fa-eye grey"></i>}
                      {showPasswordConf && <i className="fa fa-eye-slash "></i>}
                    </FlipMove>
                  </span>
                  <input
                    placeholder="***********"
                    type={showPasswordConf ? "text" : "password"}
                    className={`form-control form-control-sm ${
                      errors.password_confirmation ? "is-invalid" : ""
                    }`}
                    name="password_confirmation"
                    {...register("password_confirmation", {
                      required: "Konfirmasi password harus terisi",
                    })}
                  />
                </div>
                <span className="text-red">
                  {errors.password_confirmation?.message}
                </span>
              </div>
            </div>
          </>
        }
      </div>
    </>
  );
};

const Finalize = ({
  form: {
    register,
    handleSubmit,
    reset,
    formState: { errors },
    getValues,
    setError,
    setFocus,
    watch,
    setValue,
  },
}) => {
  const [showPassword, setShowPassword] = useState(false);
  const [showPasswordConf, setShowPasswordConf] = useState(false);
  const [FNomorHandphone, setFNomorHandphone] = useState({
    name: "Indonesia",
    code: "ID",
    emoji: "🇮🇩",
    unicode: "U+1F1EE U+1F1E9",
    image:
      "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ID.svg",
    phone: "62",
  });

  return (
    <>
      <div className="row mb-4">
        <h3 className="fw-bolder text-muted mb-5">Akun PIC</h3>
        <div className="col-12 col-md-6">
          <div className="form-group mb-4">
            <div className="row align-items-end" data-aos="fade-up">
              <div className="col-md mb-2 mb-md-0">
                <label for="nama" className="font-size-14 fw-bold">
                  Nama Lengkap PIC (Sesuai KTP)
                  <span
                    className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                    title="required"
                  >
                    *
                  </span>
                </label>
              </div>
            </div>
            <input
              disabled={PortalHelper.isLoggedin()}
              type="text"
              className={`form-control form-control-sm ${
                PortalHelper.isLoggedin() ? "disabled" : ""
              }`}
              placeholder="Masukkan Nama Lengkap"
              autoComplete="off"
              id="name-uppercase"
              {...register("nama_pic", {
                // required: 'Nama PIC harus terisi'
              })}
              {...PortalHelper.addNameValidation()}
            />

            <span className="text-red">{errors.nama_pic?.message}</span>
          </div>
        </div>
        <div className="col-12 col-md-6">
          <div className="form-group mb-4">
            <div className="row align-items-end" data-aos="fade-up">
              <div className="col-md mb-2 mb-md-0">
                <label for="nama" className="font-size-14 fw-bold">
                  NIK
                  <span
                    className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                    title="required"
                  >
                    *
                  </span>
                </label>
              </div>
            </div>
            <div className="input inner-addon right-addon">
              <span className="icon">{(watch("nik_pic") || "").length}</span>
              <input
                type="text"
                onKeyDown={(e) => {
                  PortalHelper.inputNumberOnly(e);
                }}
                onKeyUpCapture={(e) => {
                  PortalHelper.keyup(e);
                }}
                disabled={PortalHelper.isLoggedin()}
                className={`form-control form-control-sm ${
                  errors.nik_pic ? "is-invalid" : ""
                } ${PortalHelper.isLoggedin() ? "disabled" : ""}`}
                placeholder="Masukkan Nomor KTP"
                autoComplete="off"
                {...register("nik_pic", {
                  // required: 'NIK PIC harus terisi',
                  // pattern: {
                  //   value: regexValidation.number,
                  //   message: 'NIK PIC tidak valid',
                  // },
                })}
                maxLength={16}
              />
            </div>
            <span className="text-red">{errors.nik_pic?.message}</span>
          </div>
        </div>
      </div>
      {/* <div className="col-12 mb-5">
      {!PortalHelper.isLoggedin() && (
        <a
          href="#"
          className="btn btn-outline-blue btn-block btn-sm"
          onClick={async (e) => {
            // const validationResult = await trigger([
            //   "nama",
            //   "nik",
            // ]);
            // if (validationResult) {
            //   checkNik(getValues("nik"), getValues("nama"));
            // }
          }}
        >
          Validasi NIK dan Nama
        </a>
      )}
    </div> */}
      <div className="row mb-4">
        <div className="col-12 col-md-6">
          <div className="form-group mb-4">
            <div className="row align-items-end" data-aos="fade-up">
              <div className="col-md mb-2 mb-md-0">
                <label for="nama" className="font-size-14 fw-bold">
                  Nomor Handphone PIC
                  <span
                    className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                    title="required"
                  >
                    *
                  </span>
                </label>
              </div>
            </div>
            <div className="input-group">
              <span
                style={{
                  position: "absolute",
                  top: 14,
                  right: 10,
                  zIndex: 100,
                  fontSize: 12,
                }}
              >
                {(watch("kode_negara")?.length || 0) +
                  (watch("hp_pic")?.length || 0)}
              </span>
              <button
                type="button"
                className="btn btn-outline-secondary btn-sm dropdown-toggle"
                data-bs-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                {watch("kode_negara") && (
                  <span style={{ fontWeight: "lighter" }}>
                    {`${
                      CountryCode.find((c) => c?.phone == watch("kode_negara"))
                        ?.emoji
                    } ${
                      CountryCode.find((c) => c?.phone == watch("kode_negara"))
                        ?.phone
                    }`}
                  </span>
                )}
              </button>
              <div
                className="dropdown-menu h-200p"
                style={{ overflowY: "auto" }}
              >
                {CountryCode &&
                  CountryCode.map((Code, idx) => (
                    <a
                      key={idx}
                      className="dropdown-item"
                      onClick={() => {
                        setValue("kode_negara", Code?.phone);
                      }}
                    >
                      {Code.emoji} {Code.code} {`${Code.phone}`}
                    </a>
                  ))}
              </div>
              <input
                type="tel"
                {...PortalHelper.addPhoneNumberValidation()}
                disabled={PortalHelper.isLoggedin()}
                className={`form-control form-control-sm ${
                  errors.hp_pic ? "is-invalid" : ""
                } ${PortalHelper.isLoggedin() ? "disabled" : ""}`}
                placeholder="Masukkan no handphone PIC"
                autoComplete="off"
                {...register("hp_pic")}
                maxLength={15 - (watch("kode_negara")?.length || 0)}
              />
            </div>
            <span className="text-red">{errors.hp_pic?.message}</span>
          </div>
        </div>
        <div className="col-12 col-md-6">
          <div className="form-group mb-4">
            <div className="row align-items-end" data-aos="fade-up">
              <div className="col-md mb-2 mb-md-0">
                <label for="nama" className="font-size-14 fw-bold">
                  Email PIC
                  <span
                    className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                    title="required"
                  >
                    *
                  </span>
                </label>
              </div>
            </div>
            <input
              disabled={PortalHelper.isLoggedin()}
              type="text"
              className={`form-control form-control-sm ${
                PortalHelper.isLoggedin() ? "disabled" : ""
              }`}
              placeholder="Masukkan email PIC"
              autoComplete="off"
              {...register("email_pic", {
                // required: 'Email PIC harus terisi',
                // pattern: {
                //   value: regexValidation.email,
                //   message: 'Email PIC tidak valid',
                // },
              })}
            />

            <span className="text-red">{errors.email_pic?.message}</span>
          </div>
        </div>
      </div>

      {
        // !PortalHelper.isLoggedin() &&
        <div className="row mb-4">
          <div className="col-12 col-md-6">
            <div className="form-group mb-5 password-input">
              <div className="row align-items-end" data-aos="fade-up">
                <div className="col-md mb-4 mb-md-0">
                  <label for="pass" className="font-size-14 fw-bold">
                    Password
                    <span
                      className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                      title="required"
                    >
                      *
                    </span>
                  </label>
                </div>
              </div>
              <div className="input inner-addon right-addon">
                <span
                  className="icon"
                  onClick={() => {
                    setShowPassword(!showPassword);
                  }}
                >
                  <FlipMove>
                    {!showPassword && <i className="fa fa-eye grey"></i>}
                    {showPassword && <i className="fa fa-eye-slash "></i>}
                  </FlipMove>
                </span>
                <input
                  placeholder="***********"
                  type={showPassword ? "text" : "password"}
                  className={`form-control form-control-sm ${
                    errors.password ? "is-invalid" : ""
                  }`}
                  autoComplete="off"
                  {...register("password", {
                    required: "Password harus terisi",
                    minLength: {
                      value: 8,
                      message: "Password harus terdiri dari 8 karakter",
                    },
                    // pattern: {
                    //   value: regexValidation.password,
                    //   message: 'Minimal 8 Karakter kombinasi huruf kapital, huruf kecil, angka dan simbol',
                    // },
                  })}
                />
              </div>
              <div className="form-caption font-size-sm text-italic my-2">
                Minimal 8 Karakter kombinasi huruf kapital, huruf kecil, angka
                dan simbol.
              </div>
              <span className="text-red">{errors.password?.message}</span>
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="form-group mb-5">
              <div className="row align-items-end" data-aos="fade-up">
                <div className="col-md mb-4 mb-md-0">
                  <label for="pass2" className="font-size-14 fw-bold">
                    Konfirmasi Password
                    <span
                      className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                      title="required"
                    >
                      *
                    </span>
                  </label>
                </div>
              </div>
              <div className="input inner-addon right-addon">
                <span
                  className="icon"
                  onClick={() => {
                    setShowPasswordConf(!showPasswordConf);
                  }}
                >
                  <FlipMove>
                    {!showPasswordConf && <i className="fa fa-eye grey"></i>}
                    {showPasswordConf && <i className="fa fa-eye-slash "></i>}
                  </FlipMove>
                </span>
                <input
                  placeholder="***********"
                  type={showPasswordConf ? "text" : "password"}
                  className={`form-control form-control-sm ${
                    errors.password_confirmation ? "is-invalid" : ""
                  }`}
                  autoComplete="off"
                  name="password_confirmation"
                  {...register("password_confirmation", {
                    required: "Konfirmasi password harus terisi",
                  })}
                />
              </div>
              <span className="text-red">
                {errors.password_confirmation?.message}
              </span>
            </div>
          </div>
        </div>
      }

      <div className="row mb-4">
        <div className="align-items-center mb-5 font-size-sm">
          <div className="d-block">
            <ReCAPTCHA
              sitekey={process.env.CAPTCHA_SITEKEY}
              onChange={(e) => {
                setValue("captcha", e);
              }}
            ></ReCAPTCHA>
          </div>
          <div className="text-red">{errors.captcha?.message}</div>
        </div>
      </div>
    </>
  );
};

export default function MitraRegister({ showLoadingModal }) {
  const adminUrl = process.env.ADMIN_BASE_URL || window?.location?.hostname;
  const [loading, setLoading] = useState(false);

  const user = useLogginUser();
  const formLembaga = useForm({
    mode: "onBlur",
    resolver: yupResolver(lembagaSchema),
  });
  const formMitra = useForm({
    mode: "onBlur",
    resolver: yupResolver(validationSchema),
  });
  const formFinalize = useForm({
    mode: "onBlur",
    resolver: yupResolver(finalizeSchema),
    defaultValues: { kode_negara: "62" },
  });
  const [registeredMitra, setRegisteredMitra] = useState(null);

  useEffect(() => {
    try {
      window.$("input.number-only").bind({
        keydown: function (e) {
          if (e.shiftKey === true) {
            if (e.which == 9) {
              return true;
            }
            return false;
          }
          if (e.which > 57) {
            return false;
          }
          if (e.which == 32) {
            return false;
          }
          return true;
        },
      });
    } catch (err) {}
  }, []);

  console.log("user", user);

  useEffect(() => {
    if (user) {
      const kdHp = user?.nomor_handphone?.split("-") || [];
      formFinalize.setValue("nama_pic", user?.nama);
      formFinalize.setValue("hp_pic", kdHp.length > 1 ? `0${kdHp[1]}` : null);
      formFinalize.setValue("email_pic", user?.email);
      formFinalize.setValue("nik_pic", user?.nik);
    }
  }, [JSON.stringify(user)]);

  const [wizPos, setWizPos] = useState(1);
  const [listProv, setLisProv] = useState([]);
  const [listKab, setListKab] = useState([]);
  const [listNegara, setListNegara] = useState([]);

  const [listMitra, setListMitra] = useState([
    {
      id: 211,
      nama_mitra: "Lion Air",
      agency_logo:
        "https://s3d.dtsnet.net:9000/dts-partnership/logo/logo/d96d7f3a-b0c2-437f-8438-8e7143c22c23.png?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=TEST1%2F20220923%2Fus-west-0%2Fs3%2Faws4_request&X-Amz-Date=20220923T121307Z&X-Amz-SignedHeaders=host&X-Amz-Expires=60&X-Amz-Signature=4d0367e76650bde7d66598e675186428477a1a22fefc65cae7c627d171ae4eba",
      website: "LionAir.com",
      address: "jL aCEH bARAT",
      jml_pelatihan: 6,
      nama_akademi: "Akademi QC",
      slug_akademi: "AQC",
    },
  ]);

  const router = useRouter();

  useEffect(async () => {
    if (router.isReady) {
      const provs = await GeneralService.provinsi();
      const resp = await PortalService.mitraList("", "Newest", 5, 1);
      if (resp.data.success) {
        setListMitra(resp.data.result.data);
      } else {
        throw Error(resp.data.message);
      }
      // const negaras = await GeneralService.
    }
  }, [router.isReady]);

  const validateAndNext = async () => {
    try {
      let lembaga = {};
      let mitra = {};
      let finalize = {};

      if (wizPos >= 1) {
        lembaga = await new Promise((resolve, reject) =>
          formLembaga.handleSubmit(resolve, reject)(),
        );
      }
      // if (wizPos >= 2) {
      //   mitra = await new Promise((resolve, reject) => formMitra.handleSubmit(resolve, reject)());
      // }
      if (wizPos >= 2) {
        if (
          formFinalize.watch("password") !=
          formFinalize.watch("password_confirmation")
        )
          return formFinalize.setError("password_confirmation", {
            message: "Konfirmasi password tidak sama",
          });
        if (!formFinalize.watch("captcha"))
          return formFinalize.setError("captcha", {
            message: "Captcha tidak boleh kosong",
          });

        finalize = await new Promise((resolve, reject) =>
          formFinalize.handleSubmit(resolve, reject)(),
        );

        var formdata = new FormData();
        formdata.append("name", lembaga?.nama);
        formdata.append("email", lembaga?.email);
        formdata.append("website", lembaga?.website);
        formdata.append("agency_logo", lembaga?.agency_logo[0]);
        formdata.append("address", lembaga?.alamat);
        formdata.append("postal_code", lembaga?.kode_pos);

        formdata.append("origin_country", lembaga?.asal || 0);
        formdata.append("country_id", lembaga?.asal == 1 ? lembaga?.negara : 0);
        formdata.append(
          "indonesia_provinces_id",
          lembaga?.asal == 2 ? lembaga?.provinsi : 0,
        );
        formdata.append(
          "indonesia_cities_id",
          lembaga?.asal == 2 ? lembaga?.kabupaten : 0,
        );

        // formdata.append("name", mitra?.nama);
        // formdata.append("contact_number", mitra?.hp);
        // formdata.append("email", mitra?.email);
        // formdata.append("nik", mitra?.nik);

        formdata.append("pass", finalize?.password);

        formdata.append("pic_name", finalize?.nama_pic);
        formdata.append(
          "pic_contact_number",
          `${finalize?.kode_negara}${finalize?.hp_pic}`,
        );
        formdata.append("pic_email", finalize?.email_pic);
        // formdata.append("pic_nik", finalize?.nik_pic);
        formdata.append("nik", finalize?.nik_pic);

        formdata.append("status", "1");

        try {
          setLoading(true);

          const savedMitra = await PortalService.mitraSave(formdata);
          setRegisteredMitra(savedMitra);

          // NotificationHelper.Success({
          //   message: "Mitra berhasil disimpan",
          //   onClose: () => window.location.href = '/',
          // });
        } catch (err2) {
          NotificationHelper.Failed({
            message: err2.message,
          });
        }

        setLoading(false);
      }

      PortalHelper.scrollSpy("#form-placement");
      if (wizPos < 2) setWizPos(wizPos + 1);
    } catch (err) {
      setLoading(false);
      // NotificationHelper.Failed({
      //   message: "Masih terdapat isian yang belum lengkap",
      // });
    }
  };

  return (
    <>
      <div className="row vh-100">
        <div className="col-lg-5 mb-0 bg-info d-none d-lg-block">
          <div className="container py-8">
            <div className="text-center">
              <h2 className="fw-bolder mb-1">Registrasi Mitra Pelatihan</h2>
              <p className="font-size-lg mb-0 text-capitalize">
                Ayo bergabung dan berkontribusi mengembangkan Talenta Digital
              </p>
              <div className="d-none d-lg-block d-xl-block rounded p-2 mb-5">
                <div className="pt-6 pb-5">
                  <ul className="wizard mt-5 mx-lg-5 mx-xl-5 mx-xxl-5">
                    <li className={`${1 == wizPos ? "active" : ""}`}>
                      Institusi/Lembaga
                    </li>
                    <li className={`${2 == wizPos ? "active" : ""}`}>
                      Akun PIC
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="text-center">
              <img
                src="/assets/img/empty-state/regist-mitra.png"
                className="img-fluid align-self-center mt-8"
                style={{ width: "350px" }}
              />
            </div>
          </div>
        </div>
        <div className="col-lg-7 mb-0 position-relative">
          <div className="container py-8">
            <div className="mb-5">
              <a
                href="/"
                className="text-blue font-size-lg"
                data-kt-menu-dismiss="true"
              >
                <i className="fa fa-chevron-left me-2"></i>Beranda{" "}
              </a>
            </div>
            {/* FORM LOGIN ================================================== */}
            <div>
              <div className="d-block d-lg-none d-xl-none">
                <h2 className="fw-bolder mb-1">Registrasi Mitra Pelatihan</h2>
                <p className="font-size-lg mb-0 text-capitalize">
                  Ayo bergabung dan berkontribusi mengembangkan Talenta Digital
                </p>
              </div>
              <div className="d-block d-lg-none d-xl-none rounded p-2 mb-5">
                <div className="pt-6 pb-5">
                  <ul className="wizard mt-5 mx-lg-5 mx-xl-5 mx-xxl-5">
                    <li className={`${1 == wizPos ? "active" : ""}`}>
                      Institusi/Lembaga
                    </li>
                    <li className={`${2 == wizPos ? "active" : ""}`}>
                      Akun PIC
                    </li>
                  </ul>
                </div>
              </div>
              <hr className="mb-5" />
              {registeredMitra ? (
                <>
                  <div className="col-lg-12 border rounded-5 border-0 border-lg border-xl mx-auto">
                    <div className="d-block rounded-5 w-100">
                      <div className="pt-6 px-5 px-lg-3 px-xl-5 text-center">
                        <img
                          src="/assets/img/empty-state/mitra-success.png"
                          className="img-fluid align-self-center mb-5"
                          style={{ width: "400px" }}
                        />
                        <div className="row">
                          <div className="col-12 px-4">
                            <h2 className="fw-bold text-green mb-0">
                              Pendaftaran Mitra Berhasil
                            </h2>
                            <p>
                              Terima kasih atas pendaftaran anda sebagai mitra,
                              <br />
                              Data anda akan kami verifikasi, mohon menunggu
                              informasi selanjutnya.
                            </p>
                            <div className="mb-5">
                              <div className="my-7">
                                <a
                                  href="/"
                                  className="btn btn-sm rounded-pill font-size-14 btn-primary"
                                >
                                  Selesai
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </>
              ) : (
                <>
                  <div className="row mb-8">
                    <div
                      className="col-lg-12 mb-6 mb-lg-0 position-relative"
                      id="form-placement"
                    >
                      <FlipMove>
                        <div
                          className={`my-5 ${
                            wizPos == 1 ? "d-block" : "d-none"
                          }`}
                        >
                          <RegisterLembaga form={formLembaga} />
                        </div>

                        {/* <div className={`my-5 ${wizPos == 2 ? 'd-block' : 'd-none'}`}>
                      <RegisterMitra form={formMitra} />
                    </div> */}

                        <div
                          className={`my-5 ${
                            wizPos == 2 ? "d-block" : "d-none"
                          }`}
                        >
                          <Finalize form={formFinalize} />
                        </div>
                      </FlipMove>

                      <div className="row mt-7">
                        <div className="d-flex justify-content-between">
                          {wizPos > 1 && (
                            <div className="pe-1 w-100">
                              <a
                                className="btn btn-btn-outline-blue rounded-pill btn-block"
                                onClick={() => {
                                  PortalHelper.scrollSpy("#form-placement");
                                  setWizPos(wizPos - 1);
                                }}
                              >
                                SEBELUMNYA
                              </a>
                            </div>
                          )}

                          {wizPos < 2 && (
                            <div className="pe-1 w-100">
                              <a
                                className="btn btn-blue font-size-14 rounded-pill btn-block"
                                onClick={() => validateAndNext()}
                              >
                                LANJUTKAN
                              </a>
                            </div>
                          )}

                          {wizPos == 2 && (
                            <div className="pe-1 w-100">
                              <ButtonWithLoading
                                className={
                                  "btn btn-blue font-size-14 rounded-pill btn-block"
                                }
                                onClick={() => validateAndNext()}
                                loading={loading}
                              >
                                SUBMIT
                              </ButtonWithLoading>
                            </div>
                          )}
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-12 text-center align-items-top">
                          <p className="text-center my-5">
                            Sudah Punya Akun? &nbsp;
                            <Link
                              href={
                                process.env.ADMIN_BASE_URL + "/signin-mitra"
                              }
                            >
                              <a
                                className="text-blue font-size-14 text-underline"
                                onClick={() => {
                                  // showLoadingModal(true);
                                }}
                              >
                                Login
                              </a>
                            </Link>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
