import React, { useState, useEffect } from "react";
import Link from "next/link";
import "moment/locale/id";
import PortalHelper from "../../src/helper/PortalHelper";
import PortalService from "../../services/PortalService";
import GeneralService from "../../services/GeneralService";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

import { useRouter } from "next/router";
import Pagination from "../../components/Pagination";
import mitra from "./index";
import CardPelatihan from "../../components/Card/Pelatihan";

function TidakAdaData({ nama }) {
  return (
    <>
      <div className="row">
        <div className="col-12 mx-auto text-center pt-lg-6 pb-80">
          <h2 className="fw-bolder mb-0">Belum Ada Data Pelatihan</h2>
          <p className="font-size-14 mb-6">
            Mitra &nbsp;
            <strong className="text-blue">{nama}</strong> belum memiliki
            pelatihan, <br />
            silahkan coba &nbsp;
            <span>
              <Link
                href={{
                  pathname: "/mitra",
                }}
              >
                <a>Mitra lainnya.</a>
              </Link>
            </span>
          </p>
          <img
            src="/assets/img/empty-state/no-data.png"
            alt="..."
            className="img-fluid align-self-center"
            style={{ width: "500px" }}
          />
        </div>
      </div>
    </>
  );
}

function ListPelatihan(props) {
  const [loading, setLoading] = useState(false);
  const [length, setLength] = useState(10);
  const [totalLength, setTotalLength] = useState(0);
  const [dataPelatihan, setDataPelatihan] = useState(null);
  const [skeletons, setSkeletons] = useState([1, 2, 3, 4]);
  const [metaData, setMetaData] = useState(null);
  const [namaMitra, setNamaMitra] = useState(null);
  const [logo, setLogo] = useState("");
  const [timeDiff, setTimeDiff] = useState(0);

  const getServerTime = async () => {
    const resp = await GeneralService.getServerTime();
    setTimeDiff(resp);
  };

  const router = useRouter();

  const page = router.query.page ? router.query.page : 1;
  useEffect(async () => {
    try {
      setLoading(true);
      if (router.isReady) {
        const idMitra = router.query?.mitraId;
        const mitraDetail = await PortalService.mitraById(idMitra);
        const listPelatihan = await PortalService.pelatihanFilter(
          length,
          page,
          idMitra,
        );
        setNamaMitra(mitraDetail.data?.result[0]?.nama_mitra);
        setLogo(mitraDetail.data?.result[0]?.agency_logo);
        setDataPelatihan(listPelatihan.data.result.data);
        setMetaData(listPelatihan.data.result.meta);
        setLoading(false);
        getServerTime();
      }
    } catch (err) {
      console.error(err);
    }
  }, [router]);

  return (
    <>
      <header
        className="py-6 bg-blue mb-6 z-index-0"
        style={{ backgroundImage: "none" }}
      >
        <div className="container">
          <div className="row align-items-end" data-aos="fade-up">
            {loading && (
              <div className="col-md mb-md-0">
                <div className="row align-items-center mb-0 py-2 ps-4">
                  <div className="col-1 px-0">
                    <Skeleton className="h-50p"></Skeleton>
                  </div>

                  <div className="col-6 px-4">
                    <div className="card-body mb-0 p-0">
                      <Skeleton></Skeleton>
                      <Skeleton></Skeleton>
                    </div>
                  </div>
                  <div className="col-auto d-block d-md-none d-lg-none d-xl-none">
                    <Skeleton className="h-50p"></Skeleton>
                  </div>
                </div>
              </div>
            )}
            {!loading && (
              <div className="col-md mb-md-0">
                <div className="row align-items-center mb-0 py-3 ps-4">
                  <div className="col-auto px-0">
                    <div className="icon-h-p secondary">
                      <img
                        className="img-fluid h-60p shadow-light-lg"
                        src={PortalHelper.urlMitraLogo(logo)}
                        alt="DTS"
                      />
                    </div>
                  </div>

                  <div className="col px-4">
                    <div className="card-body mb-0 p-0">
                      <p className="mb-0 text-white line-clamp-1">Mitra</p>
                      <h3 className="fw-bolder text-white mb-0">{namaMitra}</h3>
                    </div>
                  </div>
                </div>
              </div>
            )}
            {/* <div className="col-md-auto col-sm-12">
              <div className="d-lg-flex rounded mb-5">
                <div className="d-lg-flex flex-wrap">
                  <div>
                    <div className="d-flex align-items-center h-50p">
                      <div className="form-floating col-12">
                        <select className="form-select form-select-sm form-control-sm ps-5 shadow-none text-primary fw-medium rounded-xl">
                          <option value="Terbaru">Terbaru</option>
                          <option value="Terlama">Terlama</option>
                          <option value="Buka">Buka</option>
                          <option value="Tutup">Tutup</option>
                        </select>
                        <label className="ps-5">Urutkan</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> */}
          </div>
        </div>
      </header>

      <div className="container z-index-0">
        <div className="row mb-9">
          <div className="col-xl-9 pt-6 mb-9">
            {loading &&
              skeletons.map((index) => (
                <div className="col-lg mb-5" key={index}>
                  <div className="card border shadow p-3 rounded-5 lift">
                    <div className="row gx-0">
                      <div style={{ maxWidth: "80px" }}>
                        <Skeleton inline={true} height={80} />
                      </div>
                      <div className="col-12 colmd-9 col-lg-9 col-xl-9 d-md-block">
                        <div className="card-body p-2">
                          <Skeleton className="mb-0" width={200} />

                          <Skeleton className="mb-4" width={250} />

                          <ul className="nav mx-n3 d-block d-md-flex">
                            <li className="nav-item px-3 mb-3 mb-md-0">
                              <div className="d-flex align-items-center">
                                <Skeleton
                                  className="me-1 d-flex text-secondary icon-uxs"
                                  width={80}
                                />
                              </div>
                            </li>
                            <li className="nav-item px-3 mb-3 mb-md-0">
                              <div className="d-flex align-items-center">
                                <Skeleton
                                  className="me-1 d-flex text-secondary icon-uxs"
                                  width={80}
                                />
                              </div>
                            </li>
                            <li className="nav-item px-3 mb-3 mb-md-0">
                              <div className="d-flex align-items-center">
                                <Skeleton
                                  className="me-1 d-flex text-secondary icon-uxs"
                                  width={80}
                                />
                              </div>
                            </li>
                            <li className="nav-item px-3 mb-3 mb-md-0">
                              <div className="d-flex align-items-center">
                                <Skeleton
                                  className="me-1 d-flex text-secondary icon-uxs"
                                  width={80}
                                />
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="col-auto d-none pe-xl-2 d-lg-flex place-center">
                        <Skeleton />
                      </div>
                    </div>
                  </div>
                </div>
              ))}

            {!loading &&
              dataPelatihan &&
              dataPelatihan.map((pelatihan, i) => {
                pelatihan = {
                  ...pelatihan,
                  current_server_time: timeDiff.serverTime,
                  diff: timeDiff.diff,
                };
                return (
                  <div className="col-lg mb-5" key={i}>
                    <CardPelatihan
                      cardClassName="rounded-5 border p-3 lift"
                      {...pelatihan}
                    ></CardPelatihan>
                  </div>
                );
              })}

            {!loading &&
              (!dataPelatihan || dataPelatihan[0] == null) &&
              namaMitra && <TidakAdaData nama={namaMitra} />}

            {metaData && (
              <div className="d-flex justify-content-center align-items-center">
                <Pagination pageSize={metaData?.last_page} currentPage={page} />
              </div>
            )}
          </div>
          <div className="col-xl-3 pt-6 px-4 mb-5 mb-xl-0">
            <div className="col-lg-12 mb-5">
              <div className="card border rounded-5 px-5 pt-6 pb-8">
                <div className="d-inline-block rounded-circle mb-4">
                  <div
                    className="icon-circle"
                    style={{ backgroundColor: "#c2dffb", color: "#196ECD" }}
                  >
                    <i className="fa fa-book fa-2x"></i>
                  </div>
                </div>
                <h5>Tentang Program</h5>
                <p className="font-size-12 mb-5">
                  Pelajari mengenai program Digital Talent Scholarship.
                </p>
                <Link href="/program">
                  <a className="btn btn-outline-primary btn-xs btn-pill mb-1">
                    Pelajari
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
export default ListPelatihan;
