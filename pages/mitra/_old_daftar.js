// wizard resgitrasi
import React, { useState, useEffect, useRef } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import Link from "next/link";
import { useRouter } from "next/router";
import PortalService from "../../services/PortalService";
import ValidationMessage from "../../src/validation-message";
import SSOInfo from "../../components/SSOSInfo";
import Failed from "../../components/Modal/Failed";
import Success from "../../components/Modal/Success";
import PortalHelper from "../../src/helper/PortalHelper";
import FlipMove from "react-flip-move";
import CountryCode from "../../public/assets/json/phone_code.js";
import EncryptionHelper from "../../src/helper/EncryptionHelper";
import VerifikasiEmail from "../../components/Modal/VerifikasiEmailModal";
import GeneralService from "../../services/GeneralService";
// import eventBus
let regexValidation = {
  email:
    /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
  url: /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/,
  number: /^[0-9]+$/,
  date: /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/,
};

export default function MitraRegister({ showLoadingModal }) {
  const [listProv, setLisProv] = useState([]);
  const [listKab, setListKab] = useState([]);
  const [listNegara, setListNegara] = useState([]);

  const [listMitra, setListMitra] = useState([
    {
      id: 211,
      nama_mitra: "Lion Air",
      agency_logo:
        "https://s3d.dtsnet.net:9000/dts-partnership/logo/logo/d96d7f3a-b0c2-437f-8438-8e7143c22c23.png?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=TEST1%2F20220923%2Fus-west-0%2Fs3%2Faws4_request&X-Amz-Date=20220923T121307Z&X-Amz-SignedHeaders=host&X-Amz-Expires=60&X-Amz-Signature=4d0367e76650bde7d66598e675186428477a1a22fefc65cae7c627d171ae4eba",
      website: "LionAir.com",
      address: "jL aCEH bARAT",
      jml_pelatihan: 6,
      nama_akademi: "Akademi QC",
      slug_akademi: "AQC",
    },
  ]);

  const router = useRouter();

  const validationSchema = Yup.object({
    name: Yup.string().required(
      ValidationMessage.required.replace("__field", "Nama Lembaga/Perusahaan"),
    ),
    website: Yup.string()
      .matches(
        regexValidation.url,
        ValidationMessage.url.replace("__field", "Url Website"),
      )
      .nullable(true)
      .transform((value, originalValue) =>
        originalValue.trim() === "" ? null : value,
      ),
    email: Yup.string()
      .required(ValidationMessage.required.replace("__field", "Email Lembaga"))
      .email(ValidationMessage.invalid.replace("__field", "Email Lembaga")),
    agency_logo: Yup.object().shape({
      attachment: Yup.mixed().test(
        "fileSize",
        "Ukuran terlalu besar",
        (value) => {
          // if (!value.length) return true // attachment is optional
          return value[0].size <= 2000000;
        },
      ),
    }),
    origin_country: Yup.string().required(
      ValidationMessage.required.replace("__field", "Asal"),
    ),
    indonesia_provinces_id: Yup.string().when("origin_country", {
      is: (value) => value == "1",
      then: Yup.string().required(
        ValidationMessage.required.replace("__field", "Provinsi"),
      ),
    }),
    indonesia_cities_id: Yup.string().when("origin_country", {
      is: (value) => value == "1",
      then: Yup.string().required(
        ValidationMessage.required.replace("__field", "Kabupaten/Kota"),
      ),
    }),
    country_id: Yup.string().when("origin_country", {
      is: (value) => value == "2",
      then: Yup.string().required(
        ValidationMessage.required.replace("__field", "Negara"),
      ),
    }),
    address: Yup.string().required(
      ValidationMessage.required.replace("__field", "Alamat"),
    ),
    postal_code: Yup.string().required(
      ValidationMessage.required.replace("__field", "Kode Pos"),
    ),
    pic_name: Yup.string().required(
      ValidationMessage.required.replace("__field", "Nama Lembaga/Perusahaan"),
    ),
    // pic_contact_number: Yup.number()
    //   .typeError(
    //     ValidationMessage.required.replace("__field", "Nomor Handphone")
    //   )
    //   .test(
    //     "minLength",
    //     ValidationMessage.minLength
    //       .replace("__field", "Nomor Handphone")
    //       .replace("__length", 7),
    //     (val) => String(val).length >= 8
    //   )
    //   .test(
    //     "maxLength",
    //     ValidationMessage.maxLength
    //       .replace("__field", "Nomor Handphone")
    //       .replace("__length", 16 - phoneCode.phone_code.length + 1),
    //     (val) => String(val).length <= 16 - phoneCode.phone_code.length
    //   )
    //   .required(
    //     ValidationMessage.required.replace("__field", "Nomor Handphone")
    //   ),
    pic_email: Yup.string()
      .required(ValidationMessage.required.replace("__field", "Email"))
      .email(ValidationMessage.invalid.replace("__field", "Email")),
    nik: Yup.number()
      .typeError(ValidationMessage.required.replace("__field", "Nomor KTP"))
      .required(ValidationMessage.required.replace("__field", "Nomor KTP"))
      .test(
        "nik",
        ValidationMessage.fixLength
          .replace("__field", "Nomor KTP")
          .replace("__length", 16),
        (val) => String(val).length === 16,
      ),

    // password: Yup.string().matches(
    //   /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!#%*?&]{8,}$/,
    //   ValidationMessage.password.replace("__field", "Password")
    // ),
    // password_confirmation: Yup.string().oneOf(
    //   [Yup.ref("password"), null],
    //   ValidationMessage.match
    //     .replace("__field", "Konfirmasi Password")
    //     .replace("__ref", "Password")
    // ),
  });

  useEffect(async () => {
    if (router.isReady) {
      const provs = await GeneralService.provinsi();
      const resp = await PortalService.mitraList("", "Newest", 5, 1);
      if (resp.data.success) {
        setListMitra(resp.data.result.data);
      } else {
        throw Error(resp.data.message);
      }
      // const negaras = await GeneralService.
    }
  }, [router.isReady]);

  const getListKab = () => {};

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
    getValues,
    setError,
    setFocus,
  } = useForm({
    mode: "onBlur",
    // defaultValues: FormProfil,
    resolver: yupResolver(validationSchema),
  });

  return (
    <>
      <div className="container my-9">
        <div className="row mb-8">
          <div className="col-lg-7 mb-6 mb-lg-0 position-relative">
            <div>
              <h3 className="mb-1">Registrasi Mitra Pelatihan</h3>
              <p className="font-size-lg mb-0 text-capitalize">
                Ayo bergabung menjadi{" "}
                <span className="text-blue font-weight-medium">
                  #JagoanDigital
                </span>
              </p>
              <hr className="mb-5" />
              <ul id="mitra-regis-tab" className="nav nav-pills nav-fill">
                <li className="nav-item">
                  <a
                    className="nav-link active py-2 cursor-pointer"
                    data-bs-toggle="tab"
                    data-bs-target="#regis-mitra-1"
                    role="tab"
                  >
                    (1) Formulir Pendaftaran
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    disabled
                    className="nav-link py-2"
                    data-bs-toggle="tab"
                    data-bs-target="#regis-mitra-2"
                    role="tab"
                  >
                    (2) Pembuatan Akun
                  </a>
                </li>
              </ul>
              <div id="user-setting-tab-content" className="tab-content mt-4">
                <div
                  id="regis-mitra-1"
                  className="tab-pane fade show active"
                  role="tabpanel"
                  aria-labelledby="regis-mitra-1"
                >
                  <div className="row">
                    <div className="col-12">
                      <div className="form-group mb-4">
                        <div className="row align-items-end" data-aos="fade-up">
                          <div className="col-md mb-2 mb-md-0">
                            <label for="nama">
                              Nama Lembaga/Perusahaan
                              <span
                                className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                                title="required"
                              >
                                *
                              </span>
                            </label>
                          </div>
                        </div>
                        <input
                          type="text"
                          className={`form-control form-control-sm`}
                          id="name"
                          placeholder="Masukan nama lembaga/perusahaan anda"
                          name="name"
                        />
                        <span className="text-red"></span>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-md-6">
                      <div className="form-group mb-4">
                        <div className="row align-items-end" data-aos="fade-up">
                          <div className="col-md mb-2 mb-md-0">
                            <label for="nama">
                              Alamat Email
                              <span
                                className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                                title="required"
                              >
                                *
                              </span>
                            </label>
                          </div>
                        </div>
                        <input
                          type="text"
                          className={`form-control form-control-sm`}
                          id="name"
                          placeholder="Masukan alamat email perusahaan"
                          name="name"
                        />

                        <span className="text-red"></span>
                      </div>
                    </div>
                    <div className="col-12 col-md-6">
                      <div className="form-group mb-4">
                        <div className="row align-items-end" data-aos="fade-up">
                          <div className="col-md mb-2 mb-md-0">
                            <label for="nama">
                              Website Perusahaan
                              <span
                                className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                                title="required"
                              >
                                *
                              </span>
                            </label>
                          </div>
                        </div>
                        <input
                          type="text"
                          className={`form-control form-control-sm`}
                          id="name"
                          placeholder="Masukan link website perusahaan"
                          name="name"
                        />

                        <span className="text-red"></span>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-md-6">
                      <div className="form-group mb-4">
                        <div className="row align-items-end" data-aos="fade-up">
                          <div className="col-md mb-2 mb-md-0">
                            <label for="nama">
                              Logo
                              <span
                                className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                                title="required"
                              >
                                *
                              </span>
                            </label>
                          </div>
                        </div>
                        <input
                          type="text"
                          className={`form-control form-control-sm`}
                          id="name"
                          placeholder="Pilih logo perusahaan"
                          name="name"
                        />

                        <span className="text-red"></span>
                      </div>
                    </div>
                    <div className="col-12 col-md-6">
                      <div className="w-100">
                        <div className="w-200p mx-auto rounded h-200p bg-gray-100 border text-center">
                          <span className="align-self-center">Logo</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-md-6">
                      <div className="form-group mb-4">
                        <div className="row align-items-end" data-aos="fade-up">
                          <div className="col-md mb-2 mb-md-0">
                            <label for="nama">
                              Asal
                              <span
                                className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                                title="required"
                              >
                                *
                              </span>
                            </label>
                          </div>
                        </div>
                        <div className="form-check form-check-inline">
                          <input
                            className="form-check-input"
                            type="radio"
                            name="inlineRadioOptions"
                            id="inlineRadio1"
                            value="option1"
                          />
                          <label
                            className="form-check-label"
                            for="inlineRadio1"
                          >
                            Dalam Negeri
                          </label>
                        </div>
                        <div className="form-check form-check-inline">
                          <input
                            className="form-check-input"
                            type="radio"
                            name="inlineRadioOptions"
                            id="inlineRadio2"
                            value="option2"
                          />
                          <label
                            className="form-check-label"
                            for="inlineRadio2"
                          >
                            Luar Negeri
                          </label>
                        </div>

                        <span className="text-red"></span>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 ">
                      <div className="form-group mb-4">
                        <div className="row align-items-end" data-aos="fade-up">
                          <div className="col-md mb-2 mb-md-0">
                            <label for="nama">
                              Alamat
                              <span
                                className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                                title="required"
                              >
                                *
                              </span>
                            </label>
                          </div>
                        </div>
                        <textarea
                          type="text"
                          className={`form-control form-control-sm`}
                          id="name"
                          placeholder="Masukanalamat kantor"
                          name="name"
                          style={{ resize: "none" }}
                        />

                        <span className="text-red"></span>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 ">
                      <div className="form-group mb-4">
                        <div className="row align-items-end" data-aos="fade-up">
                          <div className="col-md mb-2 mb-md-0">
                            <label for="nama">
                              Kode Pos
                              <span
                                className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                                title="required"
                              >
                                *
                              </span>
                            </label>
                          </div>
                        </div>
                        <input
                          type="text"
                          className={`form-control form-control-sm`}
                          id="name"
                          placeholder="Masukan kode pos alamat kantor"
                          name="name"
                        />

                        <span className="text-red"></span>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-md-6">
                      <div className="form-group mb-4">
                        <div className="row align-items-end" data-aos="fade-up">
                          <div className="col-md mb-2 mb-md-0">
                            <label for="nama">
                              Nama Person In Charge (PIC)
                              <span
                                className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                                title="required"
                              >
                                *
                              </span>
                            </label>
                          </div>
                        </div>
                        <input
                          type="text"
                          className={`form-control form-control-sm`}
                          id="name"
                          placeholder="Masukan alamat email perusahaan"
                          name="name"
                        />

                        <span className="text-red"></span>
                      </div>
                    </div>
                    <div className="col-12 col-md-6">
                      <div className="form-group mb-4">
                        <div className="row align-items-end" data-aos="fade-up">
                          <div className="col-md mb-2 mb-md-0">
                            <label for="nama">
                              NIK Person In Charge (PIC)
                              <span
                                className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                                title="required"
                              >
                                *
                              </span>
                            </label>
                          </div>
                        </div>
                        <input
                          type="text"
                          className={`form-control form-control-sm`}
                          id="name"
                          placeholder="Masukan link website perusahaan"
                          name="name"
                        />

                        <span className="text-red"></span>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12 col-md-6">
                      <div className="form-group mb-4">
                        <div className="row align-items-end" data-aos="fade-up">
                          <div className="col-md mb-2 mb-md-0">
                            <label for="nama">
                              Nomor Handphone PIC
                              <span
                                className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                                title="required"
                              >
                                *
                              </span>
                            </label>
                          </div>
                        </div>
                        <input
                          type="text"
                          className={`form-control form-control-sm`}
                          id="name"
                          placeholder="Masukan alamat email perusahaan"
                          name="name"
                        />

                        <span className="text-red"></span>
                      </div>
                    </div>
                    <div className="col-12 col-md-6">
                      <div className="form-group mb-4">
                        <div className="row align-items-end" data-aos="fade-up">
                          <div className="col-md mb-2 mb-md-0">
                            <label for="nama">
                              Email PIC
                              <span
                                className="pl-5 required align-self-end text-red font-weight-medium font-size-lg"
                                title="required"
                              >
                                *
                              </span>
                            </label>
                          </div>
                        </div>
                        <input
                          type="text"
                          className={`form-control form-control-sm`}
                          id="name"
                          placeholder="Masukan link website perusahaan"
                          name="name"
                        />

                        <span className="text-red"></span>
                      </div>
                    </div>
                  </div>
                  <button className="btn btn-primary btn-sm btn-block mt-4">
                    Selanjutnya
                  </button>
                </div>
                <div
                  id="regis-mitra-2"
                  className="tab-pane fade"
                  role="tabpanel"
                  aria-labelledby="regis-mitra-2"
                >
                  2
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-5">
            <div className="d-block rounded p-2 mb-6 bg-light">
              <div className="pt-6 pb-5 px-5 px-lg-3 px-xl-5">
                <div className="d-flex align-items-center">
                  <div className="mb-4">
                    <h4 className="mb-4">Mitra Pelatihan</h4>
                    <p className="font-size-base mb-0 text-capitalize">
                      Ayo bergabung menjadi bagian dari digitalent kominfo.
                      Beberapa mitra aktif kami
                    </p>
                  </div>
                </div>
                <div className="row">
                  {listMitra.map((mitra, i) => (
                    <div className="col-12 mb-4 px-2" key={i}>
                      <a
                        href={PortalHelper.setUpUrl(mitra.website, true, true)}
                        target="__blank"
                      >
                        <div className="row align-items-center px-2">
                          <div className="col-auto">
                            <div className="avatar avatar-custom d-inline">
                              <img
                                src={mitra.agency_logo}
                                alt={mitra.nama_mitra}
                                className="avatar-img avatar-md img-fluid"
                              />
                            </div>
                          </div>

                          <div className="col px-3">
                            <div className="card-body p-0">
                              <h6 className="mb-0 line-clamp-1">
                                {mitra.nama_mitra}
                              </h6>
                              <p className="mb-0 line-clamp-1">
                                {mitra.website}
                              </p>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>
                  ))}
                </div>
              </div>
              {/* <div className="alert alert-info p-5 m-5 " role="alert">
                Jika kamu sudah pernah mengikuti program Digital Talent
                Scholarship sebelumnya (2019-2021), silahkan Login menggunakan
                akun terakhir yang kamu gunakan mengikuti program ini.
              </div> */}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
