import _ from "lodash";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Skeleton from "react-loading-skeleton";
import Moment from "react-moment";
import SimpleDateRange from "../../components/SimpleDateRange";
import GeneralService from "../../services/GeneralService";
import CacheHelper from "../../src/helper/CacheHelper";
import PortalHelper from "../../src/helper/PortalHelper";

import CardPelatihan from "../../components/Card/Pelatihan";
import NotificationHelper from "../../src/helper/NotificationHelper";

const Cari = () => {
  const router = useRouter();
  const [akademis, setAkademis] = useState([]);
  const [pelatihans, setPelatihans] = useState([]);
  const [temas, setTemas] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [count, setCount] = useState();

  const { s } = router.query;
  // const []

  useEffect(async () => {
    // console.log(s)
    if (router.isReady) {
      if (s?.length != 0) {
        setIsLoading(true);
        const result = await GeneralService.generalSearch(s, 1000);
        if (result.data.success) {
          let cacheHist = CacheHelper.GetFromCache({
            index: "searchHistory",
            force: true,
          });
          if (cacheHist == undefined) {
            cacheHist = [];
          }
          if (cacheHist.constructor == [].constructor) {
            cacheHist = [s, ...cacheHist];
          }
          cacheHist = _.uniq(cacheHist, function (o) {
            return o;
          });

          CacheHelper.CreateCache({
            index: "searchHistory",
            data: cacheHist.slice(0, 3),
            duration: 3,
          });
          // console.log(cacheHist.slice(0,3))
          if (result.data.result) {
            const { akademi, pelatihan, tema } = result.data.result;
            setAkademis(akademi);
            setPelatihans(pelatihan);
            setTemas(tema);
            setCount(akademi.length + pelatihan.length + tema.length);
          }
        }
        // console.log(result)
        setIsLoading(false);
      } else {
        NotificationHelper.Failed({
          message: "Kata kunci pencarian tidak boleh kosong",
          onClose: () => {
            router.push("/");
          },
          okText: "Kembali",
        });
      }
    }
    // console.log(s)
  }, [s, router.isReady]);

  return (
    <>
      <header className="py-6 bg-blue mb-6 z-index-0">
        <div className="container">
          <div className="row align-items-end" data-aos="fade-up">
            <div className="col-md mb-md-0">
              <h3 className="fw-bolder text-white mb-0">Hasil Pencarian</h3>
              <p className="text-white mb-0">
                Ditemukan <strong>{count}</strong> hasil pencarian
              </p>
            </div>
          </div>
        </div>
      </header>
      {isLoading ? (
        <>
          <div className="container pb-8">
            <div className="row">
              <div className="col">
                <h2 style={{ width: 300 }}>
                  <Skeleton className="h-40p"></Skeleton>
                </h2>
              </div>
            </div>
            <div className="row">
              <div className="col-12 mb-4">
                <Skeleton className="h-100p mb-5" count={3}></Skeleton>
              </div>
            </div>
            <div className="row">
              <div className="col">
                <h2 style={{ width: 300 }}>
                  <Skeleton className="h-40p"></Skeleton>
                </h2>
              </div>
            </div>
            <div className="row">
              <div className="col-12 mb-4">
                <Skeleton className="h-100p mb-5" count={3}></Skeleton>
              </div>
            </div>
          </div>
        </>
      ) : akademis.length == 0 &&
        pelatihans.length == 0 &&
        temas.length == 0 ? (
        <>
          <div className="container pb-8">
            <div className="col-12 mx-auto text-center pt-lg-6 pb-80">
              <h2 className="fw-bolder mb-0">Tidak Ditemukan</h2>
              <p className="font-size-14 mb-6">
                Pencarian dengan kata kunci{" "}
                <strong className="text-blue">{s}</strong> tidak ditemukan,{" "}
                <br />
                silahkan coba lagi menggunakan kata kunci lain
              </p>
              <img
                src="/assets/img/empty-state/no-data.png"
                alt="..."
                className="img-fluid align-self-center"
                style={{ width: "500px" }}
              />
            </div>
          </div>
        </>
      ) : (
        <div className="container pb-8">
          <div className="pt-lg-6">
            {pelatihans.length == 0 ? (
              <></>
            ) : (
              <div className="row mb-6">
                <div className="col-12">
                  <h3 className="fw-bold mb-6">Pelatihan</h3>
                </div>
                {pelatihans.map((Pelatihan, i) => (
                  <div className="col-12 mb-5" key={i}>
                    <CardPelatihan
                      cardClassName="border rounded-5 p-3 lift"
                      {...Pelatihan}
                    ></CardPelatihan>
                  </div>
                ))}
              </div>
            )}
            {temas.length == 0 ? (
              <></>
            ) : (
              <div className="row mb-6">
                <div className="col-12">
                  <h3 className="fw-bold mb-6">Tema</h3>
                </div>
                {temas.map((tema, i) => (
                  <div className="col-lg-4 mb-3" key={i}>
                    <div className="row">
                      <Link
                        href={`/akademi/${tema.slug_akademi}?tema=${tema.id}`}
                      >
                        <div className="card border p-2 lift rounded-pill">
                          <div className="row gx-0 align-items-center">
                            <div className="col">
                              <div className="card-body p-0 ms-5">
                                <h6 className="fw-semi-bold mb-n1">
                                  {tema.nama_tema}
                                </h6>
                                <span className="text-caption font-size-12 text-muted">
                                  {tema.nama_akademi}
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </Link>
                    </div>
                  </div>
                ))}
              </div>
            )}
            {akademis.length == 0 ? (
              <></>
            ) : (
              <div className="row mb-6">
                <div className="col-12">
                  <h3 className="fw-bold mb-6">Akademi</h3>
                </div>
                {akademis.map((Akademi, i) => (
                  <div className="col-12 mb-3 px-2" key={i}>
                    <Link href={`/akademi/${Akademi.slug}`}>
                      <a className="card icon-category border rounded-5 icon-category-sm rounded lift py-3 px-4 shadow">
                        <div className="row align-items-center mx-n3">
                          <div className="col-auto px-3">
                            <div className="icon-h-p secondary">
                              <img
                                src={PortalHelper.urlAkademiIcon(Akademi.logo)}
                                className="h-60p"
                              />
                            </div>
                          </div>
                          <div className="col px-3">
                            <div className="card-body p-0">
                              <h6 className="fw-semi-bold text-primary font-size-lg mb-0">
                                {Akademi.sub_nama}
                              </h6>
                              <p className="font-size-sm fw-semi-bold text-muted mb-0">
                                {Akademi.nama}
                              </p>
                            </div>
                          </div>
                        </div>
                      </a>
                    </Link>
                  </div>
                ))}
              </div>
            )}
          </div>
        </div>
      )}
    </>
  );
};

export default Cari;
