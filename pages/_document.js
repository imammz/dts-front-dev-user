import Document, { Html, Head, Main, NextScript } from "next/document";

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const originalRenderPage = ctx.renderPage;

    // Run the React rendering logic synchronously
    ctx.renderPage = () =>
      originalRenderPage({
        // Useful for wrapping the whole react tree
        enhanceApp: (App) => App,
        // Useful for wrapping in a per-page basis
        enhanceComponent: (Component) => Component,
      });

    // Run the parent `getInitialProps`, it now includes the custom `renderPage`
    const initialProps = await Document.getInitialProps(ctx);
    return initialProps;
  }

  render() {
    return (
      <Html>
        <Head>
          <meta charSet="utf-8" />
          <meta name="color-scheme" content="light only" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0, viewport-fit=cover"
          />
          <meta
            name="description"
            content="Website Digital Talent Scholarship Kementerian Komunikasi dan Informatika RI"
          />
          <meta
            name="keywords"
            content="Digital Talent Scholarship, Digital Talent, Digitalent, Kementerian, Kominfo, Informatika, Sertifikasi, Pelatihan, Bimtek, Bimbingan Teknis, SKKNI, Literasi, SDM, Indonesia, Proserti, Litprofkom, Litprofinformatika, DTS, Scholarship"
          />
          <meta name="author" content="Digital Talent Scholarship" />
          <meta name="color:Background" content="#f4fbfa" />
          <meta property="og:url" content="https://digitalent.kominfo.go.id" />
          <meta property="og:type" content="article" />
          <meta property="og:title" content="Digital Talent Scholarship" />
          <meta
            property="og:description"
            content="Digital Talent Scholarship Kementerian Komunikasi dan Informatika"
          />
          <meta
            property="og:site_name"
            content="Website Digital Talent Scholarship Kementerian Komunikasi dan Informatika RI"
          />
          <title>Digital Talent Scholarship</title>
          <link rel="shortcut icon" href="/favicon.ico" />
          {/* <link rel="shortcut icon" href="/assets/img/logo-dts.svg" /> */}
          <link
            rel="stylesheet"
            href="/assets/fonts/fontawesome/fontawesome.css"
          />
          <link
            rel="stylesheet"
            href="/assets/libs/@fancyapps/fancybox/dist/jquery.fancybox.min.css"
          />
          <link rel="stylesheet" href="/assets/libs/aos/dist/aos.css" />
          <link
            rel="stylesheet"
            href="/assets/libs/choices.js/public/assets/styles/choices.min.css"
          />
          <link
            rel="stylesheet"
            href="/assets/libs/flickity-fade/flickity-fade.css"
          />
          <link
            rel="stylesheet"
            href="/assets/libs/flickity/dist/flickity.min.css"
          />
          <link
            rel="stylesheet"
            href="/assets/libs/highlightjs/styles/vs2015.css"
          />
          <link
            rel="stylesheet"
            href="/assets/libs/jarallax/dist/jarallax.css"
          />
          <link
            rel="stylesheet"
            href="/assets/libs/quill/dist/quill.core.css"
          />
          <link
            href="https://api.mapbox.com/mapbox-gl-js/v0.53.0/mapbox-gl.css"
            rel="stylesheet"
          />
          <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css"
          />
          <script
            defer
            src="https://analytic.sdmdigital.id/js/script.js"
          ></script>
          <script
            src="https://website-widgets.pages.dev/dist/sienna.min.js"
            defer
          ></script>
          <link rel="stylesheet" href="/assets/css/theme.min.css" />
          {/* <link rel="stylesheet" href="/assets/css/reactflatpicker.css" /> */}
        </Head>
        <body className="bg-white">
          <Main />
          <NextScript />
          <script defer src="/assets/libs/jquery/dist/jquery.min.js"></script>
          <script
            defer
            src="/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"
          ></script>
          <script defer src="/assets/libs/aos/dist/aos.js"></script>
          <script
            defer
            src="/assets/libs/choices.js/public/assets/scripts/choices.min.js"
          ></script>
          <script
            defer
            src="/assets/libs/countup.js/dist/countUp.min.js"
          ></script>
          <script
            defer
            src="/assets/libs/dropzone/dist/min/dropzone.min.js"
          ></script>
          {/* <script defer src="/assets/libs/flickity/dist/flickity.pkgd.min.js"></script>
          <script defer src="/assets/libs/flickity-fade/flickity-fade.js"></script> */}
          <script
            defer
            src="/assets/libs/highlightjs/highlight.pack.min.js"
          ></script>
          <script
            defer
            src="/assets/libs/imagesloaded/imagesloaded.pkgd.min.js"
          ></script>
          <script
            defer
            src="/assets/libs/isotope-layout/dist/isotope.pkgd.min.js"
          ></script>
          <script
            defer
            src="/assets/libs/jarallax/dist/jarallax.min.js"
          ></script>
          <script
            defer
            src="/assets/libs/jarallax/dist/jarallax-video.min.js"
          ></script>
          <script
            defer
            src="/assets/libs/jarallax/dist/jarallax-element.min.js"
          ></script>
          <script
            defer
            src="/assets/libs/parallax-js/dist/parallax.min.js"
          ></script>
          <script defer src="/assets/libs/quill/dist/quill.min.js"></script>
          <script
            defer
            src="/assets/libs/smooth-scroll/dist/smooth-scroll.min.js"
          ></script>
          <script defer src="/assets/libs/typed.js/lib/typed.min.js"></script>
          <script
            defer
            src="/assets/libs/@fancyapps/fancybox/dist/jquery.fancybox.min.js"
          ></script>
          <script
            defer
            src="https://api.mapbox.com/mapbox-gl-js/v0.53.0/mapbox-gl.js"
          ></script>
          <script
            data-name="theme-script"
            defer
            src="/assets/js/theme.min.js"
          ></script>
          <script defer src="/firebase-messaging-sw.js"></script>
        </body>
      </Html>
    );
  }
}

export default MyDocument;
