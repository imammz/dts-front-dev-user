/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
};

const {
  PHASE_DEVELOPMENT_SERVER,
  PHASE_PRODUCTION_BUILD,
} = require("next/constants");

module.exports = (phase) => {
  // when started in development mode `next dev` or `npm run dev` regardless of the value of STAGING environmental variable
  const isDev = phase === PHASE_DEVELOPMENT_SERVER;
  // when `next build` or `npm run build` is used
  const isProd =
    phase === PHASE_PRODUCTION_BUILD && process.env.STAGING !== "1";
  // when `next build` or `npm run build` is used
  const isStaging =
    phase === PHASE_PRODUCTION_BUILD && process.env.STAGING === "1";

  const env = {
    poweredByHeader: (() => {
      return false;
    })(),
    RESTURL_SESSIONS: (() => {
      if (isDev) return "https://dtsapi.dtsnet.net/";
      //if (isDev) return "http://localhost:8080/dts-back-dev/public/api/";

      if (isProd) return "https://dtsapi.dtsnet.net/";
      if (isStaging) return "https://dtsapi.dtsnet.net/";
      return "RESTURL_SESSIONS:not (isDev,isProd && !isStaging,isProd && isStaging)";
    })(),
    URL_IMG: (() => {
      if (isDev) return "https://bucket.dev.sdmdigital.id/";
      if (isProd) return "https://bucket.sdmdigital.id/";
      if (isStaging) return "https://bucket.dev.sdmdigital.id/";
      return "RESTURL_SESSIONS:not (isDev,isProd && !isStaging,isProd && isStaging)";
    })(),
    STORAGE_URL: (() => {
      if (isDev) return "https://bucket.dev.sdmdigital.id/";
      if (isProd) return "https://bucket.sdmdigital.id/";
      if (isStaging) return "https://bucket.dev.sdmdigital.id/";
      return "RESTURL_SESSIONS:not (isDev,isProd && !isStaging,isProd && isStaging)";
    })(),
    BASE_URL: (() => {
      if (isStaging) return "https://front-user.dev.dtsnet.net/";
      // if (isDev) return 'https://localhost:3000/'
      if (isDev) return "https://front-user.dev.dtsnet.net/";
      if (isProd) return "https://front-user.dev.dtsnet.net/"; //ganti jadi domain server production
      return "RESTURL_SESSIONS:not (isDev,isProd && !isStaging,isProd && isStaging)";
    })(),
    ADMIN_BASE_URL: (() => {
      if (isStaging) return "https://front.dev.dtsnet.net/";
      // if (isDev) return 'https://localhost:3000/'
      if (isDev) return "https://front.dev.dtsnet.net/";
      if (isProd) return "https://front.dev.dtsnet.net/"; //ganti jadi domain server production
      return "RESTURL_SESSIONS:not (isDev,isProd && !isStaging,isProd && isStaging)";
    })(),
    SENTRY_DSN: (() => {
      if (isStaging)
        return "https://d3e593169b6e41a99f575e82f6823a11@sentry.sdmdigital.id//5";
      if (isDev)
        return "https://99ac5c78990542fab67e824df7e91115@sentry.sdmdigital.id//4";
      if (isProd)
        return "https://74dd4a0e82d5462c88ba59bafffb135d@sentry.sdmdigital.id//2";
      return "RESTURL_SESSIONS:not (isDev,isProd && !isStaging,isProd && isStaging)";
    })(),
    PADIUMKM_BASE_URL: (() => {
      if (isStaging) return "https://seller-stg.berasumkm.id/";
      if (isDev) return "https://seller-stg.berasumkm.id/";
      if (isProd) return "https://seller.padiumkm.id/";
      return "RESTURL_SESSIONS:not (isDev,isProd && !isStaging,isProd && isStaging)";
    })(),
    USER_TOKEN_KEY: (() => {
      return "token";
    })(),
    USER_DATA_KEY: (() => {
      return "user_data";
    })(),
    USER_REGISTRATION_EMAIL_KEY: (() => {
      return "user_regist_email";
    })(),
    CAPTCHA_SITEKEY: (() => {
      return "6LfiDbogAAAAAIg9C6ywnUTbOYbcwiFQKU5bYU2z";
    })(), //udah punya kominfo
    RECAPTCHAV3_SITEKEY: (() => {
      return "6LfiDbogAAAAAIg9C6ywnUTbOYbcwiFQKU5bYU2z";
    })(),
    RECAPTCHAV3_SITEKEY: (() => {
      return "6LfiDbogAAAAAIg9C6ywnUTbOYbcwiFQKU5bYU2z";
    })(),
    USER_EMAIL_RESET_PASSWORD: (() => {
      return "user_forgot_pass_email";
    })(),
    USER_EMAIL_REGISTER: (() => {
      return "user_email_register";
    })(),
    VERIFY: (() => {
      return "verify";
    })(),
    PELATIHAN_KEY: (() => {
      return "pelatihan_temp";
    })(),
    // mulai daru sini ada kondisi
    AVATAR_NAME: (() => {
      return "https://ui-avatars.com/api/";
    })(),
    WA_URL: (() => {
      return "https://wa.me/";
    })(),
    INSTAGRAM_MEDIA_URL: (() => {
      return "https://graph.instagram.com/me/media";
    })(),
    MAJAPAHIT_BEASISWA: (() => {
      return "https://api.dts.sdmdigital.id/beasiswa/api/get-scholarship-data";
    })(),
    CHECKTIMEZONE: (() => {
      return "http://worldtimeapi.org/api/timezone/";
    })(),
    SSO_FORGOT_PASSWORD: (() => {
      return "https://sso.sdm.dev.sdmdigital.id/realms/dignas.space/login-actions/reset-credentials?client_id=dtsapi.dtsnet.net&tab_id=a2kRySv00qw";
    })(),
    LMS_BASE_URL: (() => {
      if (isStaging) return "https://lms.sdm.dev.sdmdigital.id/";
      // if (isDev) return 'https://localhost:3000/'
      if (isDev) return "https://lms.sdm.dev.sdmdigital.id/";
      if (isProd) return "https://lms.sdm.sdmdigital.id/"; //ganti jadi domain server production
      return "LMS_BASE_URL:not (isDev,isProd && !isStaging,isProd && isStaging)";
    })(),
    PRESENSI_BASE_URL: (() => {
      if (isStaging) return "https://presensi.sdm.dev.sdmdigital.id/";
      // if (isDev) return 'https://localhost:3000/'
      if (isDev) return "https://presensi.sdm.dev.sdmdigital.id/";
      if (isProd) return "https://presensi.sdm.dev.sdmdigital.id/"; //ganti jadi domain server production
      return "PRESENSI_BASE_URL:not (isDev,isProd && !isStaging,isProd && isStaging)";
    })(),
  };

  // next.config.js object
  return {
    env,
    eslint: {
      // Warning: This allows production builds to successfully complete even if
      // your project has ESLint errors.
      ignoreDuringBuilds: true,
    },
  };
};
