// importScripts('https://www.gstatic.com/firebasejs/8.10.1/firebase-app.js')
// importScripts('https://www.gstatic.com/firebasejs/8.10.1/firebase-messaging.js')

if ("function" === typeof importScripts) {
  importScripts("https://www.gstatic.com/firebasejs/8.10.1/firebase-app.js");
  importScripts(
    "https://www.gstatic.com/firebasejs/8.10.1/firebase-messaging.js"
  );

  self.addEventListener("notificationclick", function (event) {
    console.debug("SW notification click event", event);
    const url = JSON.stringify(event);
    event.waitUntil(
      clients.matchAll({ type: "window" }).then((windowClients) => {
        // Check if there is already a window/tab open with the target URL
        for (var i = 0; i < windowClients.length; i++) {
          var client = windowClients[i];
          // If so, just focus it.
          if (client.url === url && "focus" in client) {
            return client.focus();
          }
        }
        // If not, then open the target URL in a new window/tab.
        if (clients.openWindow) {
          // console.log(client)
          return clients.openWindow("/");
        }
      })
    );
  });

  const firebaseConfig = {
    apiKey: "AIzaSyC2acfqAjlOXLgEtSrg69cMfmKn9nUtPaA",
    authDomain: "dts-dev-5cedf.firebaseapp.com",
    projectId: "dts-dev-5cedf",
    storageBucket: "dts-dev-5cedf.appspot.com",
    messagingSenderId: "939587175979",
    appId: "1:939587175979:web:a6b0541f1f6ff570a18c6e",
    measurementId: "G-XZPTK56BW9",
  };

  firebase.initializeApp(firebaseConfig);
  const messaging = firebase.messaging();

  messaging.setBackgroundMessageHandler(function (payload) {
    // console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    const notificationTitle = "Background Message Title";
    const notificationOptions = {
      icon: "/assets/img/dts-mono.png",
      body: "Background Message body.",
    };

    return self.registration.showNotification(
      notificationTitle,
      notificationOptions
    );
  });
}
