const data = [
   {
      "name":"Ascension Island",
      "code":"AC",
      "emoji":"🇦🇨",
      "unicode":"U+1F1E6 U+1F1E8",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AC.svg",
      "phone":"376"
   },
   {
      "name":"Andorra",
      "code":"AD",
      "emoji":"🇦🇩",
      "unicode":"U+1F1E6 U+1F1E9",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AD.svg",
      "phone":"376"
   },
   {
      "name":"United Arab Emirates",
      "code":"AE",
      "emoji":"🇦🇪",
      "unicode":"U+1F1E6 U+1F1EA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AE.svg",
      "phone":"971"
   },
   {
      "name":"Afghanistan",
      "code":"AF",
      "emoji":"🇦🇫",
      "unicode":"U+1F1E6 U+1F1EB",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AF.svg",
      "phone":"93"
   },
   {
      "name":"Antigua & Barbuda",
      "code":"AG",
      "emoji":"🇦🇬",
      "unicode":"U+1F1E6 U+1F1EC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AG.svg",
      "phone":"1268"
   },
   {
      "name":"Anguilla",
      "code":"AI",
      "emoji":"🇦🇮",
      "unicode":"U+1F1E6 U+1F1EE",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AI.svg",
      "phone":"1264"
   },
   {
      "name":"Albania",
      "code":"AL",
      "emoji":"🇦🇱",
      "unicode":"U+1F1E6 U+1F1F1",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AL.svg",
      "phone":"355"
   },
   {
      "name":"Armenia",
      "code":"AM",
      "emoji":"🇦🇲",
      "unicode":"U+1F1E6 U+1F1F2",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AM.svg",
      "phone":"374"
   },
   {
      "name":"Angola",
      "code":"AO",
      "emoji":"🇦🇴",
      "unicode":"U+1F1E6 U+1F1F4",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AO.svg",
      "phone":"244"
   },
   {
      "name":"Antarctica",
      "code":"AQ",
      "emoji":"🇦🇶",
      "unicode":"U+1F1E6 U+1F1F6",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AQ.svg",
      "phone":"672"
   },
   {
      "name":"Argentina",
      "code":"AR",
      "emoji":"🇦🇷",
      "unicode":"U+1F1E6 U+1F1F7",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AR.svg",
      "phone":"54"
   },
   {
      "name":"American Samoa",
      "code":"AS",
      "emoji":"🇦🇸",
      "unicode":"U+1F1E6 U+1F1F8",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AS.svg",
      "phone":"1684"
   },
   {
      "name":"Austria",
      "code":"AT",
      "emoji":"🇦🇹",
      "unicode":"U+1F1E6 U+1F1F9",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AT.svg",
      "phone":"43"
   },
   {
      "name":"Australia",
      "code":"AU",
      "emoji":"🇦🇺",
      "unicode":"U+1F1E6 U+1F1FA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AU.svg",
      "phone":"61"
   },
   {
      "name":"Aruba",
      "code":"AW",
      "emoji":"🇦🇼",
      "unicode":"U+1F1E6 U+1F1FC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AW.svg",
      "phone":"297"
   },
   {
      "name":"Åland Islands",
      "code":"AX",
      "emoji":"🇦🇽",
      "unicode":"U+1F1E6 U+1F1FD",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AX.svg",
      "phone":"35818"
   },
   {
      "name":"Azerbaijan",
      "code":"AZ",
      "emoji":"🇦🇿",
      "unicode":"U+1F1E6 U+1F1FF",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/AZ.svg",
      "phone":"994"
   },
   {
      "name":"Bosnia & Herzegovina",
      "code":"BA",
      "emoji":"🇧🇦",
      "unicode":"U+1F1E7 U+1F1E6",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BA.svg",
      "phone":"387"
   },
   {
      "name":"Barbados",
      "code":"BB",
      "emoji":"🇧🇧",
      "unicode":"U+1F1E7 U+1F1E7",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BB.svg",
      "phone":"1246"
   },
   {
      "name":"Bangladesh",
      "code":"BD",
      "emoji":"🇧🇩",
      "unicode":"U+1F1E7 U+1F1E9",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BD.svg",
      "phone":"880"
   },
   {
      "name":"Belgium",
      "code":"BE",
      "emoji":"🇧🇪",
      "unicode":"U+1F1E7 U+1F1EA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BE.svg",
      "phone":"32"
   },
   {
      "name":"Burkina Faso",
      "code":"BF",
      "emoji":"🇧🇫",
      "unicode":"U+1F1E7 U+1F1EB",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BF.svg",
      "phone":"226"
   },
   {
      "name":"Bulgaria",
      "code":"BG",
      "emoji":"🇧🇬",
      "unicode":"U+1F1E7 U+1F1EC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BG.svg",
      "phone":"359"
   },
   {
      "name":"Bahrain",
      "code":"BH",
      "emoji":"🇧🇭",
      "unicode":"U+1F1E7 U+1F1ED",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BH.svg",
      "phone":"973"
   },
   {
      "name":"Burundi",
      "code":"BI",
      "emoji":"🇧🇮",
      "unicode":"U+1F1E7 U+1F1EE",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BI.svg",
      "phone":"257"
   },
   {
      "name":"Benin",
      "code":"BJ",
      "emoji":"🇧🇯",
      "unicode":"U+1F1E7 U+1F1EF",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BJ.svg",
      "phone":"229"
   },
   {
      "name":"St. Barthélemy",
      "code":"BL",
      "emoji":"🇧🇱",
      "unicode":"U+1F1E7 U+1F1F1",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BL.svg",
      "phone":"590"
   },
   {
      "name":"Bermuda",
      "code":"BM",
      "emoji":"🇧🇲",
      "unicode":"U+1F1E7 U+1F1F2",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BM.svg",
      "phone":"1441"
   },
   {
      "name":"Brunei",
      "code":"BN",
      "emoji":"🇧🇳",
      "unicode":"U+1F1E7 U+1F1F3",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BN.svg",
      "phone":"673"
   },
   {
      "name":"Bolivia",
      "code":"BO",
      "emoji":"🇧🇴",
      "unicode":"U+1F1E7 U+1F1F4",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BO.svg",
      "phone":"591"
   },
   {
      "name":"Caribbean Netherlands",
      "code":"BQ",
      "emoji":"🇧🇶",
      "unicode":"U+1F1E7 U+1F1F6",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BQ.svg",
      "phone":"599"
   },
   {
      "name":"Brazil",
      "code":"BR",
      "emoji":"🇧🇷",
      "unicode":"U+1F1E7 U+1F1F7",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BR.svg",
      "phone":"55"
   },
   {
      "name":"Bahamas",
      "code":"BS",
      "emoji":"🇧🇸",
      "unicode":"U+1F1E7 U+1F1F8",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BS.svg",
      "phone":"1242"
   },
   {
      "name":"Bhutan",
      "code":"BT",
      "emoji":"🇧🇹",
      "unicode":"U+1F1E7 U+1F1F9",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BT.svg",
      "phone":"975"
   },
   {
      "name":"Bouvet Island",
      "code":"BV",
      "emoji":"🇧🇻",
      "unicode":"U+1F1E7 U+1F1FB",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BV.svg",
      "phone":"55"
   },
   {
      "name":"Botswana",
      "code":"BW",
      "emoji":"🇧🇼",
      "unicode":"U+1F1E7 U+1F1FC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BW.svg",
      "phone":"267"
   },
   {
      "name":"Belarus",
      "code":"BY",
      "emoji":"🇧🇾",
      "unicode":"U+1F1E7 U+1F1FE",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BY.svg",
      "phone":"375"
   },
   {
      "name":"Belize",
      "code":"BZ",
      "emoji":"🇧🇿",
      "unicode":"U+1F1E7 U+1F1FF",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/BZ.svg",
      "phone":"501"
   },
   {
      "name":"Canada",
      "code":"CA",
      "emoji":"🇨🇦",
      "unicode":"U+1F1E8 U+1F1E6",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CA.svg",
      "phone":"1"
   },
   {
      "name":"Cocos (Keeling) Islands",
      "code":"CC",
      "emoji":"🇨🇨",
      "unicode":"U+1F1E8 U+1F1E8",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CC.svg",
      "phone":"61"
   },
   {
      "name":"Congo - Kinshasa",
      "code":"CD",
      "emoji":"🇨🇩",
      "unicode":"U+1F1E8 U+1F1E9",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CD.svg",
      "phone":"243"
   },
   {
      "name":"Central African Republic",
      "code":"CF",
      "emoji":"🇨🇫",
      "unicode":"U+1F1E8 U+1F1EB",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CF.svg",
      "phone":"236"
   },
   {
      "name":"Congo - Brazzaville",
      "code":"CG",
      "emoji":"🇨🇬",
      "unicode":"U+1F1E8 U+1F1EC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CG.svg",
      "phone":"242"
   },
   {
      "name":"Switzerland",
      "code":"CH",
      "emoji":"🇨🇭",
      "unicode":"U+1F1E8 U+1F1ED",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CH.svg",
      "phone":"41"
   },
   {
      "name":"Côte d’Ivoire",
      "code":"CI",
      "emoji":"🇨🇮",
      "unicode":"U+1F1E8 U+1F1EE",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CI.svg",
      "phone":"225"
   },
   {
      "name":"Cook Islands",
      "code":"CK",
      "emoji":"🇨🇰",
      "unicode":"U+1F1E8 U+1F1F0",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CK.svg",
      "phone":"682"
   },
   {
      "name":"Chile",
      "code":"CL",
      "emoji":"🇨🇱",
      "unicode":"U+1F1E8 U+1F1F1",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CL.svg",
      "phone":"56"
   },
   {
      "name":"Cameroon",
      "code":"CM",
      "emoji":"🇨🇲",
      "unicode":"U+1F1E8 U+1F1F2",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CM.svg",
      "phone":"237"
   },
   {
      "name":"China",
      "code":"CN",
      "emoji":"🇨🇳",
      "unicode":"U+1F1E8 U+1F1F3",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CN.svg",
      "phone":"86"
   },
   {
      "name":"Colombia",
      "code":"CO",
      "emoji":"🇨🇴",
      "unicode":"U+1F1E8 U+1F1F4",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CO.svg",
      "phone":"57"
   },
   {
      "name":"Clipperton Island",
      "code":"CP",
      "emoji":"🇨🇵",
      "unicode":"U+1F1E8 U+1F1F5",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CP.svg",
      "phone":"689"
   },
   {
      "name":"Costa Rica",
      "code":"CR",
      "emoji":"🇨🇷",
      "unicode":"U+1F1E8 U+1F1F7",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CR.svg",
      "phone":"506"
   },
   {
      "name":"Cuba",
      "code":"CU",
      "emoji":"🇨🇺",
      "unicode":"U+1F1E8 U+1F1FA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CU.svg",
      "phone":"53"
   },
   {
      "name":"Cape Verde",
      "code":"CV",
      "emoji":"🇨🇻",
      "unicode":"U+1F1E8 U+1F1FB",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CV.svg",
      "phone":"238"
   },
   {
      "name":"Curaçao",
      "code":"CW",
      "emoji":"🇨🇼",
      "unicode":"U+1F1E8 U+1F1FC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CW.svg",
      "phone":"599"
   },
   {
      "name":"Christmas Island",
      "code":"CX",
      "emoji":"🇨🇽",
      "unicode":"U+1F1E8 U+1F1FD",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CX.svg",
      "phone":"61"
   },
   {
      "name":"Cyprus",
      "code":"CY",
      "emoji":"🇨🇾",
      "unicode":"U+1F1E8 U+1F1FE",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CY.svg",
      "phone":"357"
   },
   {
      "name":"Czechia",
      "code":"CZ",
      "emoji":"🇨🇿",
      "unicode":"U+1F1E8 U+1F1FF",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/CZ.svg",
      "phone":"420"
   },
   {
      "name":"Germany",
      "code":"DE",
      "emoji":"🇩🇪",
      "unicode":"U+1F1E9 U+1F1EA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/DE.svg",
      "phone":"49"
   },
   {
      "name":"Diego Garcia",
      "code":"DG",
      "emoji":"🇩🇬",
      "unicode":"U+1F1E9 U+1F1EC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/DG.svg",
      "phone":"246"
   },
   {
      "name":"Djibouti",
      "code":"DJ",
      "emoji":"🇩🇯",
      "unicode":"U+1F1E9 U+1F1EF",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/DJ.svg",
      "phone":"253"
   },
   {
      "name":"Denmark",
      "code":"DK",
      "emoji":"🇩🇰",
      "unicode":"U+1F1E9 U+1F1F0",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/DK.svg",
      "phone":"45"
   },
   {
      "name":"Dominica",
      "code":"DM",
      "emoji":"🇩🇲",
      "unicode":"U+1F1E9 U+1F1F2",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/DM.svg",
      "phone":"1767"
   },
   {
      "name":"Dominican Republic",
      "code":"DO",
      "emoji":"🇩🇴",
      "unicode":"U+1F1E9 U+1F1F4",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/DO.svg",
      "phone":"1809"
   },
   {
    "name":"Dominican Republic",
    "code":"DO",
    "emoji":"🇩🇴",
    "unicode":"U+1F1E9 U+1F1F4",
    "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/DO.svg",
    "phone":"1829"
    },
   {
      "name":"Algeria",
      "code":"DZ",
      "emoji":"🇩🇿",
      "unicode":"U+1F1E9 U+1F1FF",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/DZ.svg",
      "phone":"213"
   },
   {
      "name":"Ceuta & Melilla",
      "code":"EA",
      "emoji":"🇪🇦",
      "unicode":"U+1F1EA U+1F1E6",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/EA.svg",
      "phone":"952"
   },
   {
      "name":"Ecuador",
      "code":"EC",
      "emoji":"🇪🇨",
      "unicode":"U+1F1EA U+1F1E8",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/EC.svg",
      "phone":"593"
   },
   {
      "name":"Estonia",
      "code":"EE",
      "emoji":"🇪🇪",
      "unicode":"U+1F1EA U+1F1EA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/EE.svg",
      "phone":"372"
   },
   {
      "name":"Egypt",
      "code":"EG",
      "emoji":"🇪🇬",
      "unicode":"U+1F1EA U+1F1EC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/EG.svg",
      "phone":"20"
   },
   {
      "name":"Western Sahara",
      "code":"EH",
      "emoji":"🇪🇭",
      "unicode":"U+1F1EA U+1F1ED",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/EH.svg",
      "phone":"212"
   },
   {
      "name":"Eritrea",
      "code":"ER",
      "emoji":"🇪🇷",
      "unicode":"U+1F1EA U+1F1F7",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ER.svg",
      "phone":"291"
   },
   {
      "name":"Spain",
      "code":"ES",
      "emoji":"🇪🇸",
      "unicode":"U+1F1EA U+1F1F8",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ES.svg",
      "phone":"34"
   },
   {
      "name":"Ethiopia",
      "code":"ET",
      "emoji":"🇪🇹",
      "unicode":"U+1F1EA U+1F1F9",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ET.svg",
      "phone":"251"
   },
   {
      "name":"Finland",
      "code":"FI",
      "emoji":"🇫🇮",
      "unicode":"U+1F1EB U+1F1EE",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/FI.svg",
      "phone":"358"
   },
   {
      "name":"Fiji",
      "code":"FJ",
      "emoji":"🇫🇯",
      "unicode":"U+1F1EB U+1F1EF",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/FJ.svg",
      "phone":"679"
   },
   {
      "name":"Falkland Islands",
      "code":"FK",
      "emoji":"🇫🇰",
      "unicode":"U+1F1EB U+1F1F0",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/FK.svg",
      "phone":"500"
   },
   {
      "name":"Micronesia",
      "code":"FM",
      "emoji":"🇫🇲",
      "unicode":"U+1F1EB U+1F1F2",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/FM.svg",
      "phone":"691"
   },
   {
      "name":"Faroe Islands",
      "code":"FO",
      "emoji":"🇫🇴",
      "unicode":"U+1F1EB U+1F1F4",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/FO.svg",
      "phone":"298"
   },
   {
      "name":"France",
      "code":"FR",
      "emoji":"🇫🇷",
      "unicode":"U+1F1EB U+1F1F7",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/FR.svg",
      "phone":"33"
   },
   {
      "name":"Gabon",
      "code":"GA",
      "emoji":"🇬🇦",
      "unicode":"U+1F1EC U+1F1E6",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GA.svg",
      "phone":"241"
   },
   {
      "name":"United Kingdom",
      "code":"GB",
      "emoji":"🇬🇧",
      "unicode":"U+1F1EC U+1F1E7",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GB.svg",
      "phone":"44"
   },
   {
      "name":"Grenada",
      "code":"GD",
      "emoji":"🇬🇩",
      "unicode":"U+1F1EC U+1F1E9",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GD.svg",
      "phone":"1473"
   },
   {
      "name":"Georgia",
      "code":"GE",
      "emoji":"🇬🇪",
      "unicode":"U+1F1EC U+1F1EA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GE.svg",
      "phone":"995"
   },
   {
      "name":"French Guiana",
      "code":"GF",
      "emoji":"🇬🇫",
      "unicode":"U+1F1EC U+1F1EB",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GF.svg",
      "phone":"594"
   },
   {
      "name":"Guernsey",
      "code":"GG",
      "emoji":"🇬🇬",
      "unicode":"U+1F1EC U+1F1EC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GG.svg",
      "phone":"441481"
   },
   {
      "name":"Ghana",
      "code":"GH",
      "emoji":"🇬🇭",
      "unicode":"U+1F1EC U+1F1ED",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GH.svg",
      "phone":"233"
   },
   {
      "name":"Gibraltar",
      "code":"GI",
      "emoji":"🇬🇮",
      "unicode":"U+1F1EC U+1F1EE",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GI.svg",
      "phone":"350"
   },
   {
      "name":"Greenland",
      "code":"GL",
      "emoji":"🇬🇱",
      "unicode":"U+1F1EC U+1F1F1",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GL.svg",
      "phone":"299"
   },
   {
      "name":"Gambia",
      "code":"GM",
      "emoji":"🇬🇲",
      "unicode":"U+1F1EC U+1F1F2",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GM.svg",
      "phone":"220"
   },
   {
      "name":"Guinea",
      "code":"GN",
      "emoji":"🇬🇳",
      "unicode":"U+1F1EC U+1F1F3",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GN.svg",
      "phone":"224"
   },
   {
      "name":"Guadeloupe",
      "code":"GP",
      "emoji":"🇬🇵",
      "unicode":"U+1F1EC U+1F1F5",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GP.svg",
      "phone":"590"
   },
   {
      "name":"Equatorial Guinea",
      "code":"GQ",
      "emoji":"🇬🇶",
      "unicode":"U+1F1EC U+1F1F6",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GQ.svg",
      "phone":"240"
   },
   {
      "name":"Greece",
      "code":"GR",
      "emoji":"🇬🇷",
      "unicode":"U+1F1EC U+1F1F7",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GR.svg",
      "phone":"30"
   },
   {
      "name":"South Georgia & South Sandwich Islands",
      "code":"GS",
      "emoji":"🇬🇸",
      "unicode":"U+1F1EC U+1F1F8",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GS.svg",
      "phone":"500"
   },
   {
      "name":"Guatemala",
      "code":"GT",
      "emoji":"🇬🇹",
      "unicode":"U+1F1EC U+1F1F9",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GT.svg",
      "phone":"502"
   },
   {
      "name":"Guam",
      "code":"GU",
      "emoji":"🇬🇺",
      "unicode":"U+1F1EC U+1F1FA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GU.svg",
      "phone":"1671"
   },
   {
      "name":"Guinea-Bissau",
      "code":"GW",
      "emoji":"🇬🇼",
      "unicode":"U+1F1EC U+1F1FC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GW.svg",
      "phone":"245"
   },
   {
      "name":"Guyana",
      "code":"GY",
      "emoji":"🇬🇾",
      "unicode":"U+1F1EC U+1F1FE",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/GY.svg",
      "phone":"592"
   },
   {
      "name":"Hong Kong SAR China",
      "code":"HK",
      "emoji":"🇭🇰",
      "unicode":"U+1F1ED U+1F1F0",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/HK.svg",
      "phone":"852"
   },
   {
      "name":"Honduras",
      "code":"HN",
      "emoji":"🇭🇳",
      "unicode":"U+1F1ED U+1F1F3",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/HN.svg",
      "phone":"504"
   },
   {
      "name":"Croatia",
      "code":"HR",
      "emoji":"🇭🇷",
      "unicode":"U+1F1ED U+1F1F7",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/HR.svg",
      "phone":"385"
   },
   {
      "name":"Haiti",
      "code":"HT",
      "emoji":"🇭🇹",
      "unicode":"U+1F1ED U+1F1F9",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/HT.svg",
      "phone":"509"
   },
   {
      "name":"Hungary",
      "code":"HU",
      "emoji":"🇭🇺",
      "unicode":"U+1F1ED U+1F1FA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/HU.svg",
      "phone":"36"
   },
   {
      "name":"Canary Islands",
      "code":"IC",
      "emoji":"🇮🇨",
      "unicode":"U+1F1EE U+1F1E8",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IC.svg",
      "phone":"34"
   },
   {
      "name":"Indonesia",
      "code":"ID",
      "emoji":"🇮🇩",
      "unicode":"U+1F1EE U+1F1E9",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ID.svg",
      "phone":"62"
   },
   {
      "name":"Ireland",
      "code":"IE",
      "emoji":"🇮🇪",
      "unicode":"U+1F1EE U+1F1EA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IE.svg",
      "phone":"353"
   },
   {
      "name":"Israel",
      "code":"IL",
      "emoji":"🇮🇱",
      "unicode":"U+1F1EE U+1F1F1",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IL.svg",
      "phone":"972"
   },
   {
      "name":"Isle of Man",
      "code":"IM",
      "emoji":"🇮🇲",
      "unicode":"U+1F1EE U+1F1F2",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IM.svg",
      "phone":"441624"
   },
   {
      "name":"India",
      "code":"IN",
      "emoji":"🇮🇳",
      "unicode":"U+1F1EE U+1F1F3",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IN.svg",
      "phone":"91"
   },
   {
      "name":"British Indian Ocean Territory",
      "code":"IO",
      "emoji":"🇮🇴",
      "unicode":"U+1F1EE U+1F1F4",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IO.svg",
      "phone":"246"
   },
   {
      "name":"Iraq",
      "code":"IQ",
      "emoji":"🇮🇶",
      "unicode":"U+1F1EE U+1F1F6",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IQ.svg",
      "phone":"964"
   },
   {
      "name":"Iran",
      "code":"IR",
      "emoji":"🇮🇷",
      "unicode":"U+1F1EE U+1F1F7",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IR.svg",
      "phone":"98"
   },
   {
      "name":"Iceland",
      "code":"IS",
      "emoji":"🇮🇸",
      "unicode":"U+1F1EE U+1F1F8",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IS.svg",
      "phone":"354"
   },
   {
      "name":"Italy",
      "code":"IT",
      "emoji":"🇮🇹",
      "unicode":"U+1F1EE U+1F1F9",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/IT.svg",
      "phone":"39"
   },
   {
      "name":"Jersey",
      "code":"JE",
      "emoji":"🇯🇪",
      "unicode":"U+1F1EF U+1F1EA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/JE.svg",
      "phone":"441534"
   },
   {
      "name":"Jamaica",
      "code":"JM",
      "emoji":"🇯🇲",
      "unicode":"U+1F1EF U+1F1F2",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/JM.svg",
      "phone":"1876"
   },
   {
      "name":"Jordan",
      "code":"JO",
      "emoji":"🇯🇴",
      "unicode":"U+1F1EF U+1F1F4",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/JO.svg",
      "phone":"962"
   },
   {
      "name":"Japan",
      "code":"JP",
      "emoji":"🇯🇵",
      "unicode":"U+1F1EF U+1F1F5",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/JP.svg",
      "phone":"81"
   },
   {
      "name":"Kenya",
      "code":"KE",
      "emoji":"🇰🇪",
      "unicode":"U+1F1F0 U+1F1EA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KE.svg",
      "phone":"254"
   },
   {
      "name":"Kyrgyzstan",
      "code":"KG",
      "emoji":"🇰🇬",
      "unicode":"U+1F1F0 U+1F1EC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KG.svg",
      "phone":"996"
   },
   {
      "name":"Cambodia",
      "code":"KH",
      "emoji":"🇰🇭",
      "unicode":"U+1F1F0 U+1F1ED",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KH.svg",
      "phone":"855"
   },
   {
      "name":"Kiribati",
      "code":"KI",
      "emoji":"🇰🇮",
      "unicode":"U+1F1F0 U+1F1EE",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KI.svg",
      "phone":"686"
   },
   {
      "name":"Comoros",
      "code":"KM",
      "emoji":"🇰🇲",
      "unicode":"U+1F1F0 U+1F1F2",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KM.svg",
      "phone":"269"
   },
   {
      "name":"St. Kitts & Nevis",
      "code":"KN",
      "emoji":"🇰🇳",
      "unicode":"U+1F1F0 U+1F1F3",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KN.svg",
      "phone":"1869"
   },
   {
      "name":"North Korea",
      "code":"KP",
      "emoji":"🇰🇵",
      "unicode":"U+1F1F0 U+1F1F5",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KP.svg",
      "phone":"850"
   },
   {
      "name":"South Korea",
      "code":"KR",
      "emoji":"🇰🇷",
      "unicode":"U+1F1F0 U+1F1F7",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KR.svg",
      "phone":"82"
   },
   {
      "name":"Kuwait",
      "code":"KW",
      "emoji":"🇰🇼",
      "unicode":"U+1F1F0 U+1F1FC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KW.svg",
      "phone":"965"
   },
   {
      "name":"Cayman Islands",
      "code":"KY",
      "emoji":"🇰🇾",
      "unicode":"U+1F1F0 U+1F1FE",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KY.svg",
      "phone":"1345"
   },
   {
      "name":"Kazakhstan",
      "code":"KZ",
      "emoji":"🇰🇿",
      "unicode":"U+1F1F0 U+1F1FF",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/KZ.svg",
      "phone":"7"
   },
   {
      "name":"Laos",
      "code":"LA",
      "emoji":"🇱🇦",
      "unicode":"U+1F1F1 U+1F1E6",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LA.svg",
      "phone":"856"
   },
   {
      "name":"Lebanon",
      "code":"LB",
      "emoji":"🇱🇧",
      "unicode":"U+1F1F1 U+1F1E7",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LB.svg",
      "phone":"961"
   },
   {
      "name":"St. Lucia",
      "code":"LC",
      "emoji":"🇱🇨",
      "unicode":"U+1F1F1 U+1F1E8",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LC.svg",
      "phone":"1758"
   },
   {
      "name":"Liechtenstein",
      "code":"LI",
      "emoji":"🇱🇮",
      "unicode":"U+1F1F1 U+1F1EE",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LI.svg",
      "phone":"423"
   },
   {
      "name":"Sri Lanka",
      "code":"LK",
      "emoji":"🇱🇰",
      "unicode":"U+1F1F1 U+1F1F0",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LK.svg",
      "phone":"94"
   },
   {
      "name":"Liberia",
      "code":"LR",
      "emoji":"🇱🇷",
      "unicode":"U+1F1F1 U+1F1F7",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LR.svg",
      "phone":"231"
   },
   {
      "name":"Lesotho",
      "code":"LS",
      "emoji":"🇱🇸",
      "unicode":"U+1F1F1 U+1F1F8",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LS.svg",
      "phone":"266"
   },
   {
      "name":"Lithuania",
      "code":"LT",
      "emoji":"🇱🇹",
      "unicode":"U+1F1F1 U+1F1F9",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LT.svg",
      "phone":"370"
   },
   {
      "name":"Luxembourg",
      "code":"LU",
      "emoji":"🇱🇺",
      "unicode":"U+1F1F1 U+1F1FA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LU.svg",
      "phone":"352"
   },
   {
      "name":"Latvia",
      "code":"LV",
      "emoji":"🇱🇻",
      "unicode":"U+1F1F1 U+1F1FB",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LV.svg",
      "phone":"371"
   },
   {
      "name":"Libya",
      "code":"LY",
      "emoji":"🇱🇾",
      "unicode":"U+1F1F1 U+1F1FE",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/LY.svg",
      "phone":"218"
   },
   {
      "name":"Morocco",
      "code":"MA",
      "emoji":"🇲🇦",
      "unicode":"U+1F1F2 U+1F1E6",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MA.svg",
      "phone":"212"
   },
   {
      "name":"Monaco",
      "code":"MC",
      "emoji":"🇲🇨",
      "unicode":"U+1F1F2 U+1F1E8",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MC.svg",
      "phone":"377"
   },
   {
      "name":"Moldova",
      "code":"MD",
      "emoji":"🇲🇩",
      "unicode":"U+1F1F2 U+1F1E9",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MD.svg",
      "phone":"373"
   },
   {
      "name":"Montenegro",
      "code":"ME",
      "emoji":"🇲🇪",
      "unicode":"U+1F1F2 U+1F1EA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ME.svg",
      "phone":"382"
   },
   {
      "name":"St. Martin",
      "code":"MF",
      "emoji":"🇲🇫",
      "unicode":"U+1F1F2 U+1F1EB",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MF.svg",
      "phone":"590"
   },
   {
      "name":"Madagascar",
      "code":"MG",
      "emoji":"🇲🇬",
      "unicode":"U+1F1F2 U+1F1EC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MG.svg",
      "phone":"261"
   },
   {
      "name":"Marshall Islands",
      "code":"MH",
      "emoji":"🇲🇭",
      "unicode":"U+1F1F2 U+1F1ED",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MH.svg",
      "phone":"692"
   },
   {
      "name":"North Macedonia",
      "code":"MK",
      "emoji":"🇲🇰",
      "unicode":"U+1F1F2 U+1F1F0",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MK.svg",
      "phone":"389"
   },
   {
      "name":"Mali",
      "code":"ML",
      "emoji":"🇲🇱",
      "unicode":"U+1F1F2 U+1F1F1",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ML.svg",
      "phone":"223"
   },
   {
      "name":"Myanmar (Burma)",
      "code":"MM",
      "emoji":"🇲🇲",
      "unicode":"U+1F1F2 U+1F1F2",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MM.svg",
      "phone":"95"
   },
   {
      "name":"Mongolia",
      "code":"MN",
      "emoji":"🇲🇳",
      "unicode":"U+1F1F2 U+1F1F3",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MN.svg",
      "phone":"976"
   },
   {
      "name":"Macao SAR China",
      "code":"MO",
      "emoji":"🇲🇴",
      "unicode":"U+1F1F2 U+1F1F4",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MO.svg",
      "phone":"853"
   },
   {
      "name":"Northern Mariana Islands",
      "code":"MP",
      "emoji":"🇲🇵",
      "unicode":"U+1F1F2 U+1F1F5",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MP.svg",
      "phone":"1670"
   },
   {
      "name":"Martinique",
      "code":"MQ",
      "emoji":"🇲🇶",
      "unicode":"U+1F1F2 U+1F1F6",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MQ.svg",
      "phone":"596"
   },
   {
      "name":"Mauritania",
      "code":"MR",
      "emoji":"🇲🇷",
      "unicode":"U+1F1F2 U+1F1F7",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MR.svg",
      "phone":"222"
   },
   {
      "name":"Montserrat",
      "code":"MS",
      "emoji":"🇲🇸",
      "unicode":"U+1F1F2 U+1F1F8",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MS.svg",
      "phone":"1664"
   },
   {
      "name":"Malta",
      "code":"MT",
      "emoji":"🇲🇹",
      "unicode":"U+1F1F2 U+1F1F9",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MT.svg",
      "phone":"356"
   },
   {
      "name":"Mauritius",
      "code":"MU",
      "emoji":"🇲🇺",
      "unicode":"U+1F1F2 U+1F1FA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MU.svg",
      "phone":"230"
   },
   {
      "name":"Maldives",
      "code":"MV",
      "emoji":"🇲🇻",
      "unicode":"U+1F1F2 U+1F1FB",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MV.svg",
      "phone":"960"
   },
   {
      "name":"Malawi",
      "code":"MW",
      "emoji":"🇲🇼",
      "unicode":"U+1F1F2 U+1F1FC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MW.svg",
      "phone":"265"
   },
   {
      "name":"Mexico",
      "code":"MX",
      "emoji":"🇲🇽",
      "unicode":"U+1F1F2 U+1F1FD",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MX.svg",
      "phone":"52"
   },
   {
      "name":"Malaysia",
      "code":"MY",
      "emoji":"🇲🇾",
      "unicode":"U+1F1F2 U+1F1FE",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MY.svg",
      "phone":"60"
   },
   {
      "name":"Mozambique",
      "code":"MZ",
      "emoji":"🇲🇿",
      "unicode":"U+1F1F2 U+1F1FF",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/MZ.svg",
      "phone":"258"
   },
   {
      "name":"Namibia",
      "code":"NA",
      "emoji":"🇳🇦",
      "unicode":"U+1F1F3 U+1F1E6",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NA.svg",
      "phone":"264"
   },
   {
      "name":"New Caledonia",
      "code":"NC",
      "emoji":"🇳🇨",
      "unicode":"U+1F1F3 U+1F1E8",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NC.svg",
      "phone":"687"
   },
   {
      "name":"Niger",
      "code":"NE",
      "emoji":"🇳🇪",
      "unicode":"U+1F1F3 U+1F1EA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NE.svg",
      "phone":"227"
   },
   {
      "name":"Norfolk Island",
      "code":"NF",
      "emoji":"🇳🇫",
      "unicode":"U+1F1F3 U+1F1EB",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NF.svg",
      "phone":"672"
   },
   {
      "name":"Nigeria",
      "code":"NG",
      "emoji":"🇳🇬",
      "unicode":"U+1F1F3 U+1F1EC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NG.svg",
      "phone":"234"
   },
   {
      "name":"Nicaragua",
      "code":"NI",
      "emoji":"🇳🇮",
      "unicode":"U+1F1F3 U+1F1EE",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NI.svg",
      "phone":"505"
   },
   {
      "name":"Netherlands",
      "code":"NL",
      "emoji":"🇳🇱",
      "unicode":"U+1F1F3 U+1F1F1",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NL.svg",
      "phone":"31"
   },
   {
      "name":"Norway",
      "code":"NO",
      "emoji":"🇳🇴",
      "unicode":"U+1F1F3 U+1F1F4",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NO.svg",
      "phone":"47"
   },
   {
      "name":"Nepal",
      "code":"NP",
      "emoji":"🇳🇵",
      "unicode":"U+1F1F3 U+1F1F5",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NP.svg",
      "phone":"977"
   },
   {
      "name":"Nauru",
      "code":"NR",
      "emoji":"🇳🇷",
      "unicode":"U+1F1F3 U+1F1F7",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NR.svg",
      "phone":"674"
   },
   {
      "name":"Niue",
      "code":"NU",
      "emoji":"🇳🇺",
      "unicode":"U+1F1F3 U+1F1FA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NU.svg",
      "phone":"683"
   },
   {
      "name":"New Zealand",
      "code":"NZ",
      "emoji":"🇳🇿",
      "unicode":"U+1F1F3 U+1F1FF",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/NZ.svg",
      "phone":"64"
   },
   {
      "name":"Oman",
      "code":"OM",
      "emoji":"🇴🇲",
      "unicode":"U+1F1F4 U+1F1F2",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/OM.svg",
      "phone":"968"
   },
   {
      "name":"Panama",
      "code":"PA",
      "emoji":"🇵🇦",
      "unicode":"U+1F1F5 U+1F1E6",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PA.svg",
      "phone":"507"
   },
   {
      "name":"Peru",
      "code":"PE",
      "emoji":"🇵🇪",
      "unicode":"U+1F1F5 U+1F1EA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PE.svg",
      "phone":"51"
   },
   {
      "name":"French Polynesia",
      "code":"PF",
      "emoji":"🇵🇫",
      "unicode":"U+1F1F5 U+1F1EB",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PF.svg",
      "phone":"689"
   },
   {
      "name":"Papua New Guinea",
      "code":"PG",
      "emoji":"🇵🇬",
      "unicode":"U+1F1F5 U+1F1EC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PG.svg",
      "phone":"675"
   },
   {
      "name":"Philippines",
      "code":"PH",
      "emoji":"🇵🇭",
      "unicode":"U+1F1F5 U+1F1ED",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PH.svg",
      "phone":"63"
   },
   {
      "name":"Pakistan",
      "code":"PK",
      "emoji":"🇵🇰",
      "unicode":"U+1F1F5 U+1F1F0",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PK.svg",
      "phone":"92"
   },
   {
      "name":"Poland",
      "code":"PL",
      "emoji":"🇵🇱",
      "unicode":"U+1F1F5 U+1F1F1",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PL.svg",
      "phone":"48"
   },
   {
      "name":"St. Pierre & Miquelon",
      "code":"PM",
      "emoji":"🇵🇲",
      "unicode":"U+1F1F5 U+1F1F2",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PM.svg",
      "phone":"508"
   },
   {
      "name":"Pitcairn Islands",
      "code":"PN",
      "emoji":"🇵🇳",
      "unicode":"U+1F1F5 U+1F1F3",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PN.svg",
      "phone":"870"
   },
   {
      "name":"Puerto Rico",
      "code":"PR",
      "emoji":"🇵🇷",
      "unicode":"U+1F1F5 U+1F1F7",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PR.svg",
      "phone":"1787"
   },
   {
    "name":"Puerto Rico",
    "code":"PR",
    "emoji":"🇵🇷",
    "unicode":"U+1F1F5 U+1F1F7",
    "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PR.svg",
    "phone":"1939"
 },
   {
      "name":"Palestinian Territories",
      "code":"PS",
      "emoji":"🇵🇸",
      "unicode":"U+1F1F5 U+1F1F8",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PS.svg",
      "phone":"970"
   },
   {
      "name":"Portugal",
      "code":"PT",
      "emoji":"🇵🇹",
      "unicode":"U+1F1F5 U+1F1F9",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PT.svg",
      "phone":"351"
   },
   {
      "name":"Palau",
      "code":"PW",
      "emoji":"🇵🇼",
      "unicode":"U+1F1F5 U+1F1FC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PW.svg",
      "phone":"680"
   },
   {
      "name":"Paraguay",
      "code":"PY",
      "emoji":"🇵🇾",
      "unicode":"U+1F1F5 U+1F1FE",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/PY.svg",
      "phone":"595"
   },
   {
      "name":"Qatar",
      "code":"QA",
      "emoji":"🇶🇦",
      "unicode":"U+1F1F6 U+1F1E6",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/QA.svg",
      "phone":"974"
   },
   {
      "name":"Réunion",
      "code":"RE",
      "emoji":"🇷🇪",
      "unicode":"U+1F1F7 U+1F1EA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/RE.svg",
      "phone":"262"
   },
   {
      "name":"Romania",
      "code":"RO",
      "emoji":"🇷🇴",
      "unicode":"U+1F1F7 U+1F1F4",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/RO.svg",
      "phone":"40"
   },
   {
      "name":"Serbia",
      "code":"RS",
      "emoji":"🇷🇸",
      "unicode":"U+1F1F7 U+1F1F8",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/RS.svg",
      "phone":"381"
   },
   {
      "name":"Russia",
      "code":"RU",
      "emoji":"🇷🇺",
      "unicode":"U+1F1F7 U+1F1FA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/RU.svg",
      "phone":"7"
   },
   {
      "name":"Rwanda",
      "code":"RW",
      "emoji":"🇷🇼",
      "unicode":"U+1F1F7 U+1F1FC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/RW.svg",
      "phone":"250"
   },
   {
      "name":"Saudi Arabia",
      "code":"SA",
      "emoji":"🇸🇦",
      "unicode":"U+1F1F8 U+1F1E6",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SA.svg",
      "phone":"966"
   },
   {
      "name":"Solomon Islands",
      "code":"SB",
      "emoji":"🇸🇧",
      "unicode":"U+1F1F8 U+1F1E7",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SB.svg",
      "phone":"677"
   },
   {
      "name":"Seychelles",
      "code":"SC",
      "emoji":"🇸🇨",
      "unicode":"U+1F1F8 U+1F1E8",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SC.svg",
      "phone":"248"
   },
   {
      "name":"Sudan",
      "code":"SD",
      "emoji":"🇸🇩",
      "unicode":"U+1F1F8 U+1F1E9",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SD.svg",
      "phone":"249"
   },
   {
      "name":"Sweden",
      "code":"SE",
      "emoji":"🇸🇪",
      "unicode":"U+1F1F8 U+1F1EA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SE.svg",
      "phone":"46"
   },
   {
      "name":"Singapore",
      "code":"SG",
      "emoji":"🇸🇬",
      "unicode":"U+1F1F8 U+1F1EC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SG.svg",
      "phone":"65"
   },
   {
      "name":"St. Helena",
      "code":"SH",
      "emoji":"🇸🇭",
      "unicode":"U+1F1F8 U+1F1ED",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SH.svg",
      "phone":"290"
   },
   {
      "name":"Slovenia",
      "code":"SI",
      "emoji":"🇸🇮",
      "unicode":"U+1F1F8 U+1F1EE",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SI.svg",
      "phone":"386"
   },
   {
      "name":"Svalbard & Jan Mayen",
      "code":"SJ",
      "emoji":"🇸🇯",
      "unicode":"U+1F1F8 U+1F1EF",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SJ.svg",
      "phone":"47"
   },
   {
      "name":"Slovakia",
      "code":"SK",
      "emoji":"🇸🇰",
      "unicode":"U+1F1F8 U+1F1F0",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SK.svg",
      "phone":"421"
   },
   {
      "name":"Sierra Leone",
      "code":"SL",
      "emoji":"🇸🇱",
      "unicode":"U+1F1F8 U+1F1F1",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SL.svg",
      "phone":"232"
   },
   {
      "name":"San Marino",
      "code":"SM",
      "emoji":"🇸🇲",
      "unicode":"U+1F1F8 U+1F1F2",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SM.svg",
      "phone":"378"
   },
   {
      "name":"Senegal",
      "code":"SN",
      "emoji":"🇸🇳",
      "unicode":"U+1F1F8 U+1F1F3",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SN.svg",
      "phone":"221"
   },
   {
      "name":"Somalia",
      "code":"SO",
      "emoji":"🇸🇴",
      "unicode":"U+1F1F8 U+1F1F4",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SO.svg",
      "phone":"252"
   },
   {
      "name":"Suriname",
      "code":"SR",
      "emoji":"🇸🇷",
      "unicode":"U+1F1F8 U+1F1F7",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SR.svg",
      "phone":"597"
   },
   {
      "name":"South Sudan",
      "code":"SS",
      "emoji":"🇸🇸",
      "unicode":"U+1F1F8 U+1F1F8",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SS.svg",
      "phone":"211"
   },
   {
      "name":"São Tomé & Príncipe",
      "code":"ST",
      "emoji":"🇸🇹",
      "unicode":"U+1F1F8 U+1F1F9",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ST.svg",
      "phone":"239"
   },
   {
      "name":"El Salvador",
      "code":"SV",
      "emoji":"🇸🇻",
      "unicode":"U+1F1F8 U+1F1FB",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SV.svg",
      "phone":"503"
   },
   {
      "name":"Sint Maarten",
      "code":"SX",
      "emoji":"🇸🇽",
      "unicode":"U+1F1F8 U+1F1FD",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SX.svg",
      "phone":"599"
   },
   {
      "name":"Syria",
      "code":"SY",
      "emoji":"🇸🇾",
      "unicode":"U+1F1F8 U+1F1FE",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SY.svg",
      "phone":"963"
   },
   {
      "name":"Eswatini",
      "code":"SZ",
      "emoji":"🇸🇿",
      "unicode":"U+1F1F8 U+1F1FF",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SZ.svg",
      "phone":"268"
   },
   {
      "name":"Tristan da Cunha",
      "code":"TA",
      "emoji":"🇹🇦",
      "unicode":"U+1F1F9 U+1F1E6",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TA.svg",
      "phone":"290"
   },
   {
      "name":"Turks & Caicos Islands",
      "code":"TC",
      "emoji":"🇹🇨",
      "unicode":"U+1F1F9 U+1F1E8",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TC.svg",
      "phone":"1649"
   },
   {
      "name":"Chad",
      "code":"TD",
      "emoji":"🇹🇩",
      "unicode":"U+1F1F9 U+1F1E9",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TD.svg",
      "phone":"235"
   },
   {
      "name":"French Southern Territories",
      "code":"TF",
      "emoji":"🇹🇫",
      "unicode":"U+1F1F9 U+1F1EB",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TF.svg",
      "phone":"262"
   },
   {
      "name":"Togo",
      "code":"TG",
      "emoji":"🇹🇬",
      "unicode":"U+1F1F9 U+1F1EC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TG.svg",
      "phone":"228"
   },
   {
      "name":"Thailand",
      "code":"TH",
      "emoji":"🇹🇭",
      "unicode":"U+1F1F9 U+1F1ED",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TH.svg",
      "phone":"66"
   },
   {
      "name":"Tajikistan",
      "code":"TJ",
      "emoji":"🇹🇯",
      "unicode":"U+1F1F9 U+1F1EF",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TJ.svg",
      "phone":"992"
   },
   {
      "name":"Tokelau",
      "code":"TK",
      "emoji":"🇹🇰",
      "unicode":"U+1F1F9 U+1F1F0",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TK.svg",
      "phone":"690"
   },
   {
      "name":"Timor-Leste",
      "code":"TL",
      "emoji":"🇹🇱",
      "unicode":"U+1F1F9 U+1F1F1",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TL.svg",
      "phone":"670"
   },
   {
      "name":"Turkmenistan",
      "code":"TM",
      "emoji":"🇹🇲",
      "unicode":"U+1F1F9 U+1F1F2",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TM.svg",
      "phone":"993"
   },
   {
      "name":"Tunisia",
      "code":"TN",
      "emoji":"🇹🇳",
      "unicode":"U+1F1F9 U+1F1F3",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TN.svg",
      "phone":"216"
   },
   {
      "name":"Tonga",
      "code":"TO",
      "emoji":"🇹🇴",
      "unicode":"U+1F1F9 U+1F1F4",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TO.svg",
      "phone":"676"
   },
   {
      "name":"Turkey",
      "code":"TR",
      "emoji":"🇹🇷",
      "unicode":"U+1F1F9 U+1F1F7",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TR.svg",
      "phone":"90"
   },
   {
      "name":"Trinidad & Tobago",
      "code":"TT",
      "emoji":"🇹🇹",
      "unicode":"U+1F1F9 U+1F1F9",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TT.svg",
      "phone":"1868"
   },
   {
      "name":"Tuvalu",
      "code":"TV",
      "emoji":"🇹🇻",
      "unicode":"U+1F1F9 U+1F1FB",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TV.svg",
      "phone":"688"
   },
   {
      "name":"Taiwan",
      "code":"TW",
      "emoji":"🇹🇼",
      "unicode":"U+1F1F9 U+1F1FC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TW.svg",
      "phone":"886"
   },
   {
      "name":"Tanzania",
      "code":"TZ",
      "emoji":"🇹🇿",
      "unicode":"U+1F1F9 U+1F1FF",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/TZ.svg",
      "phone":"255"
   },
   {
      "name":"Ukraine",
      "code":"UA",
      "emoji":"🇺🇦",
      "unicode":"U+1F1FA U+1F1E6",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/UA.svg",
      "phone":"380"
   },
   {
      "name":"Uganda",
      "code":"UG",
      "emoji":"🇺🇬",
      "unicode":"U+1F1FA U+1F1EC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/UG.svg",
      "phone":"256"
   },
   {
      "name":"U.S. Outlying Islands",
      "code":"UM",
      "emoji":"🇺🇲",
      "unicode":"U+1F1FA U+1F1F2",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/UM.svg",
      "phone":"1"
   },
   {
      "name":"United States",
      "code":"US",
      "emoji":"🇺🇸",
      "unicode":"U+1F1FA U+1F1F8",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/US.svg",
      "phone":"1"
   },
   {
      "name":"Uruguay",
      "code":"UY",
      "emoji":"🇺🇾",
      "unicode":"U+1F1FA U+1F1FE",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/UY.svg",
      "phone":"598"
   },
   {
      "name":"Uzbekistan",
      "code":"UZ",
      "emoji":"🇺🇿",
      "unicode":"U+1F1FA U+1F1FF",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/UZ.svg",
      "phone":"998"
   },
   {
      "name":"Vatican City",
      "code":"VA",
      "emoji":"🇻🇦",
      "unicode":"U+1F1FB U+1F1E6",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/VA.svg",
      "phone":"379"
   },
   {
      "name":"St. Vincent & Grenadines",
      "code":"VC",
      "emoji":"🇻🇨",
      "unicode":"U+1F1FB U+1F1E8",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/VC.svg",
      "phone":"1784"
   },
   {
      "name":"Venezuela",
      "code":"VE",
      "emoji":"🇻🇪",
      "unicode":"U+1F1FB U+1F1EA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/VE.svg",
      "phone":"58"
   },
   {
      "name":"British Virgin Islands",
      "code":"VG",
      "emoji":"🇻🇬",
      "unicode":"U+1F1FB U+1F1EC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/VG.svg",
      "phone":"1284"
   },
   {
      "name":"U.S. Virgin Islands",
      "code":"VI",
      "emoji":"🇻🇮",
      "unicode":"U+1F1FB U+1F1EE",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/VI.svg",
      "phone":"1340"
   },
   {
      "name":"Vietnam",
      "code":"VN",
      "emoji":"🇻🇳",
      "unicode":"U+1F1FB U+1F1F3",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/VN.svg",
      "phone":"84"
   },
   {
      "name":"Vanuatu",
      "code":"VU",
      "emoji":"🇻🇺",
      "unicode":"U+1F1FB U+1F1FA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/VU.svg",
      "phone":"678"
   },
   {
      "name":"Wallis & Futuna",
      "code":"WF",
      "emoji":"🇼🇫",
      "unicode":"U+1F1FC U+1F1EB",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/WF.svg",
      "phone":"681"
   },
   {
      "name":"Samoa",
      "code":"WS",
      "emoji":"🇼🇸",
      "unicode":"U+1F1FC U+1F1F8",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/WS.svg",
      "phone":"685"
   },
   {
      "name":"Kosovo",
      "code":"XK",
      "emoji":"🇽🇰",
      "unicode":"U+1F1FD U+1F1F0",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/XK.svg",
      "phone":"383"
   },
   {
      "name":"Yemen",
      "code":"YE",
      "emoji":"🇾🇪",
      "unicode":"U+1F1FE U+1F1EA",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/YE.svg",
      "phone":"967"
   },
   {
      "name":"Mayotte",
      "code":"YT",
      "emoji":"🇾🇹",
      "unicode":"U+1F1FE U+1F1F9",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/YT.svg",
      "phone":"262"
   },
   {
      "name":"South Africa",
      "code":"ZA",
      "emoji":"🇿🇦",
      "unicode":"U+1F1FF U+1F1E6",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ZA.svg",
      "phone":"27"
   },
   {
      "name":"Zambia",
      "code":"ZM",
      "emoji":"🇿🇲",
      "unicode":"U+1F1FF U+1F1F2",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ZM.svg",
      "phone":"260"
   },
   {
      "name":"Zimbabwe",
      "code":"ZW",
      "emoji":"🇿🇼",
      "unicode":"U+1F1FF U+1F1FC",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ZW.svg",
      "phone":"263"
   },
   {
      "name":"England",
      "code":"ENGLAND",
      "emoji":"🏴󠁧󠁢󠁥󠁮󠁧󠁿",
      "unicode":"U+1F3F4 U+E0067 U+E0062 U+E0065 U+E006E U+E0067 U+E007F",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ENGLAND.svg",
      "phone":"44"
   },
   {
      "name":"Scotland",
      "code":"SCOTLAND",
      "emoji":"🏴󠁧󠁢󠁳󠁣󠁴󠁿",
      "unicode":"U+1F3F4 U+E0067 U+E0062 U+E0073 U+E0063 U+E0074 U+E007F",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/SCOTLAND.svg",
      "phone":"44"
   },
   {
      "name":"Wales",
      "code":"WALES",
      "emoji":"🏴󠁧󠁢󠁷󠁬󠁳󠁿",
      "unicode":"U+1F3F4 U+E0067 U+E0062 U+E0077 U+E006C U+E0073 U+E007F",
      "image":"https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/WALES.svg",
      "phone":"44"
   }
]
export default data