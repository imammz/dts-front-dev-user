import React from "react";

export default function RegistrasiWizPelatihan() {
  return (
    <div>
      <header
        className="py-8 mb-6 bg-white z-index-0"
        style={{ backgroundImage: "none" }}
      >
        <div className="container">
          <div className="alert bg-green" role="alert">
            <div className="row text-white mb-md-0">
              <div className="row align-items-center">
                <div className="col-auto px-3">
                  <i className="fa fa-check-circle img-fluid fa-2x" />
                </div>
                <div className="col px-3">
                  <h5 className="text-white mb-0">
                    Selamat, pendaftaranmu telah selesai. Silahkan untuk memilih
                    pelatihan yang kamu inginkan.
                  </h5>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* Img */}
      </header>

      <section className="py-6 mb-11">
        <div className="container">
          <div className="row align-items-end mb-md-7 mb-4">
            <div className="col-md mb-4 mb-md-0">
              <h1 className="mb-1">Pilih Pelatihan</h1>
              <p className="font-size-lg mb-0 text-capitalize">
                Hi Digiers, temukan berbagai pelatihan menarik yang sesuai
                dengan dirimu
              </p>
            </div>
            <div className="col-md-auto">
              <a
                href="all-pelatihan.html"
                className="d-flex align-items-center fw-medium"
              >
                Lihat Semua Pelatihan
                <div className="ms-2 d-flex">
                  {/* Icon */}
                  <svg
                    width={10}
                    height={10}
                    viewBox="0 0 10 10"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M7.7779 4.6098L3.32777 0.159755C3.22485 0.0567475 3.08745 0 2.94095 0C2.79445 0 2.65705 0.0567475 2.55412 0.159755L2.2264 0.487394C2.01315 0.700889 2.01315 1.04788 2.2264 1.26105L5.96328 4.99793L2.22225 8.73895C2.11933 8.84196 2.0625 8.97928 2.0625 9.1257C2.0625 9.27228 2.11933 9.4096 2.22225 9.51269L2.54998 9.84025C2.65298 9.94325 2.7903 10 2.9368 10C3.0833 10 3.2207 9.94325 3.32363 9.84025L7.7779 5.38614C7.88107 5.2828 7.93774 5.14484 7.93741 4.99817C7.93774 4.85094 7.88107 4.71305 7.7779 4.6098Z"
                      fill="currentColor"
                    />
                  </svg>
                </div>
              </a>
            </div>
          </div>
          <div className="row row-cols-2 row-cols-lg-3 row-cols-xl-4">
            <div
              className="col mb-md-6 mb-4 px-2 px-md-4"
              data-aos="fade-up"
              data-aos-delay={50}
            >
              {/* Card */}
              <a
                href="detail-akademi.html"
                className="card icon-category border shadow-dark p-md-5 p-3 text-center lift"
              >
                {/* Image */}
                <div className="position-relative text-light">
                  <img src="assets/img/akademi/fga.png" className="h-152" />
                </div>
                {/* Footer */}
                <div className="card-footer px-0 py-3">
                  <h5 className="mb-0 line-clamp-1">Lulusan Baru</h5>
                  <p className="mb-0 font-size-sm line-clamp-1">
                    Fresh Graduate Academy
                  </p>
                </div>
              </a>
            </div>
            <div
              className="col mb-md-6 mb-4 px-2 px-md-4"
              data-aos="fade-up"
              data-aos-delay={100}
            >
              {/* Card */}
              <a
                href="detail-akademi.html"
                className="card icon-category border shadow-dark p-md-5 p-3 text-center lift"
              >
                {/* Image */}
                <div className="position-relative text-light">
                  <img src="assets/img/akademi/vsga.png" className="h-152" />
                </div>
                {/* Footer */}
                <div className="card-footer px-0 py-3">
                  <h5 className="mb-0 line-clamp-1">Lulusan Vokasi</h5>
                  <p className="mb-0 font-size-sm line-clamp-1">
                    Vocational School Graduate Academy
                  </p>
                </div>
              </a>
            </div>
            <div
              className="col mb-md-6 mb-4 px-2 px-md-4"
              data-aos="fade-up"
              data-aos-delay={150}
            >
              {/* Card */}
              <a
                href="detail-akademi.html"
                className="card icon-category border shadow-dark p-md-5 p-3 text-center lift"
              >
                {/* Image */}
                <div className="position-relative text-light">
                  <img src="assets/img/akademi/proa.png" className="h-152" />
                </div>
                {/* Footer */}
                <div className="card-footer px-0 py-3">
                  <h5 className="mb-0 line-clamp-1">Profesional</h5>
                  <p className="mb-0 font-size-sm line-clamp-1">
                    Profesional Academy
                  </p>
                </div>
              </a>
            </div>
            <div
              className="col mb-md-6 mb-4 px-2 px-md-4"
              data-aos="fade-up"
              data-aos-delay={200}
            >
              {/* Card */}
              <a
                href="detail-akademi.html"
                className="card icon-category border shadow-dark p-md-5 p-3 text-center lift"
              >
                {/* Image */}
                <div className="position-relative text-light">
                  <img src="assets/img/akademi/gta.png" className="h-152" />
                </div>
                {/* Footer */}
                <div className="card-footer px-0 py-3">
                  <h5 className="mb-0 line-clamp-1">Aparatur Sipil Negara</h5>
                  <p className="mb-0 font-size-sm line-clamp-1">
                    Government Transformation Academy
                  </p>
                </div>
              </a>
            </div>
            <div
              className="col mb-md-6 mb-4 px-2 px-md-4"
              data-aos="fade-up"
              data-aos-delay={250}
            >
              {/* Card */}
              <a
                href="detail-akademi.html"
                className="card icon-category border shadow-dark p-md-5 p-3 text-center lift"
              >
                {/* Image */}
                <div className="position-relative text-light">
                  <img src="assets/img/akademi/dea.png" className="h-152" />
                </div>
                {/* Footer */}
                <div className="card-footer px-0 py-3">
                  <h5 className="mb-0 line-clamp-1">Wirausaha</h5>
                  <p className="mb-0 font-size-sm line-clamp-1">
                    Digital Enterpreneurship Academy
                  </p>
                </div>
              </a>
            </div>
            <div
              className="col mb-md-6 mb-4 px-2 px-md-4"
              data-aos="fade-up"
              data-aos-delay={300}
            >
              {/* Card */}
              <a
                href="detail-akademi.html"
                className="card icon-category border shadow-dark p-md-5 p-3 text-center lift"
              >
                {/* Image */}
                <div className="position-relative text-light">
                  <img src="assets/img/akademi/tsa.png" className="h-152" />
                </div>
                {/* Footer */}
                <div className="card-footer px-0 py-3">
                  <h5 className="mb-0 line-clamp-1">Mahasiswa</h5>
                  <p className="mb-0 font-size-sm line-clamp-1">
                    Talent Scouting Academy
                  </p>
                </div>
              </a>
            </div>
            <div
              className="col mb-md-6 mb-4 px-2 px-md-4"
              data-aos="fade-up"
              data-aos-delay={350}
            >
              {/* Card */}
              <a
                href="detail-akademi.html"
                className="card icon-category border shadow-dark p-md-5 p-3 text-center lift"
              >
                {/* Image */}
                {/* Image */}
                <div className="position-relative text-light">
                  <img src="assets/img/akademi/ta.png" className="h-152" />
                </div>
                {/* Footer */}
                <div className="card-footer px-0 py-3">
                  <h5 className="mb-0 line-clamp-1">Pelatihan Tematik</h5>
                  <p className="mb-0 font-size-sm line-clamp-1">
                    Thematic Academy
                  </p>
                </div>
              </a>
            </div>
            <div
              className="col mb-md-6 mb-4 px-2 px-md-4"
              data-aos="fade-up"
              data-aos-delay={400}
            >
              {/* Card */}
              <a
                href="detail-akademi.html"
                className="card icon-category border shadow-dark p-md-5 p-3 text-center lift"
              >
                {/* Image */}
                <div className="position-relative text-light">
                  <img src="assets/img/akademi/dla.png" className="h-152" />
                </div>
                {/* Footer */}
                <div className="card-footer px-0 py-3">
                  <h5 className="mb-0 line-clamp-1">Pimpinan</h5>
                  <p className="mb-0 font-size-sm line-clamp-1">
                    Digital Leadership Academy
                  </p>
                </div>
              </a>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
