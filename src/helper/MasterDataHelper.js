const jenisKelamin = { 1: "Pria", 2: "Wanita" };

const hubunganKontakDarurat = ["Orang Tua", "Saudara", "Pasangan", "Kerabat"];

const MasterDataHelper = {
  jenisKelamin,
  hubunganKontakDarurat,
};

export default MasterDataHelper;
