import localforage from "localforage";
import moment from "moment";

const CreateCache = ({
  index = "",
  data = "",
  duration = 24,
  durationType = "hours",
}) => {
  let stringData = JSON.stringify({
    data: data,
    exp: moment().add(durationType, duration).unix(),
  });

  localStorage.setItem(index, stringData);
};

const ClearCache = ({ index = "" }) => {
  localStorage.removeItem(index);
};

const GetFromCache = ({ index = "", force = false }) => {
  let tmpData = JSON.parse(localStorage.getItem(index));
  // console.log(moment().unix())
  if (moment().unix() > tmpData?.exp) {
    // window.alert("expired")
    if (force) {
      return tmpData?.data;
    } else {
      localStorage.removeItem(index);
      return null;
    }
  } else {
    return tmpData?.data;
  }
};

const CreateCacheV2 = async ({
  index = "",
  data = "",
  duration = 24,
  durationType = "hours",
}) => {
  let stringData = JSON.stringify({
    data: data,
    exp: moment().add(durationType, duration).unix(),
  });

  localforage.setItem(index, stringData);
};

const ClearCacheV2 = async ({ index = "" }) => {
  localforage.removeItem(index);
};

const GetFromCacheV2 = async ({ index = "", force = false }) => {
  try {
    let tmpData = JSON.parse(await localforage.getItem(index));
    // console.log(moment().unix())
    if (moment().unix() > tmpData?.exp) {
      // window.alert("expired")
      if (force) {
        return tmpData?.data;
      } else {
        ClearCacheV2(index);
        return null;
      }
    } else {
      return tmpData?.data;
    }
  } catch (error) {
    return null;
  }
};

const HttpCacheWrapper = ({
  func = async () => {},
  index = "",
  duration = 24,
  durationType = "hours",
  axiosResultDataTransform = (d) => d,
  useV2 = false,
}) => {
  return new Promise(async (resolve, reject) => {
    try {
      let dataCache = useV2
        ? await GetFromCacheV2({ index: index })
        : GetFromCache({ index: index });
      if (dataCache != null) {
        resolve(dataCache);
      } else {
        if (typeof func == "function") {
          let data = await func();
          if (typeof axiosResultDataTransform == "function") {
            let newResponsData = axiosResultDataTransform(data?.data);
            data.data = newResponsData;
          }
          if (useV2) {
            await CreateCacheV2({
              index: index,
              data: data,
              duration: duration,
              durationType: durationType,
            });
          } else {
            CreateCache({
              index: index,
              data: data,
              duration: duration,
              durationType: durationType,
            });
          }
          resolve(data);
        } else {
          reject(new Error("func is not http function"));
        }
      }
    } catch (error) {
      reject(error);
    }
  });
};

const ClearAllCache = async () => {
  Object.keys(localStorage).forEach((k) => {
    // console.log(k)
    localStorage.removeItem(k);
  });
  await localforage.clear();
};

const CacheHelper = {
  CreateCache,
  GetFromCache,
  HttpCacheWrapper,
  ClearCache,
  ClearAllCache,
};

export const CacheHelperV2 = {
  CreateCache: CreateCacheV2,
  GetFromCache: GetFromCacheV2,
  ClearCache: ClearCacheV2,
  HttpCacheWrapper: (
    args = {
      func: async () => {},
      index: "",
      duration: 24,
      durationType: "hours",
      axiosResultDataTransform: (d) => d,
    },
  ) => HttpCacheWrapper({ ...args, useV2: true }),
};

export default CacheHelper;
