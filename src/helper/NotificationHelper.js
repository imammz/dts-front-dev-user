import eventBus from "../../components/EventBus";

class NotificationHelper {
  constructor() {
    // this.id = moment().unix()
  }

  Failed({
    message = "",
    timeout = 0,
    onClose = () => {},
    imgUrl = "",
    okText = "Ok",
  }) {
    if (imgUrl.length > 0) {
      eventBus.dispatch("showModalNotificationSuccessImage", imgUrl);
    }
    if (okText.length > 0) {
      eventBus.dispatch("showModalNotificationFailedOkText", okText);
    }
    eventBus.dispatch("showModalNotificationFailedMessage", message);
    eventBus.dispatch("showModalNotificationFailedTimeOut", timeout);
    eventBus.dispatch("showModalNotificationFailedOnClose", { func: onClose });
    eventBus.dispatch("showModalNotificationFailed", true);
  }
  Success({
    title = "SUKSES",
    message = "",
    timeout = 0,
    onClose = () => {},
    imgUrl = "",
    okText = "Ok",
  }) {
    if (imgUrl.length > 0) {
      eventBus.dispatch("showModalNotificationSuccessImage", imgUrl);
    }
    if (okText.length > 0) {
      eventBus.dispatch("showModalNotificationSuccessOkText", okText);
    }
    eventBus.dispatch("showModalNotificationSuccessTitle", title);
    eventBus.dispatch("showModalNotificationSuccessMessage", message);
    eventBus.dispatch("showModalNotificationSuccessTimeOut", timeout);
    eventBus.dispatch("showModalNotificationSuccessOnClose", { func: onClose });
    eventBus.dispatch("showModalNotificationSuccess", true);
  }
}

export default new NotificationHelper();
