import { getCookie } from "cookies-next";
import EncryptionHelper from "/src/helper/EncryptionHelper";
import RouteSegment from "/src/route-segment";

const Next = {
  next: true,
  redirect: null,
};

const Middleware = async (route, nextUrl = null) => {
  const authCookie = getCookie(process.env.USER_TOKEN_KEY);
  const verifyCookie = getCookie(process.env.VERIFY);
  const forgotPassCookie = getCookie(process.env.USER_EMAIL_RESET_PASSWORD);
  let userData = getCookie(process.env.USER_DATA_KEY);
  userData = userData ? JSON.parse(userData) : null;

  // return NextResponse.next();
  let routerpath = nextUrl ? nextUrl : route.pathname;
  const userDataPageMatch = RouteSegment.completeUserData.find((element) =>
    routerpath.includes(element),
  );

  if (userDataPageMatch) {
    if (userData?.wizard == null || userData?.wizard == 0) {
      return Promise.resolve(Next);
    } else if (routerpath.includes("/nik") && userData?.flag_nik == 0) {
      return Promise.resolve(Next);
    } else {
      route.push("/");
      return Promise.resolve({ next: false, redirect: "/" });
    }
  }

  const forgotPassPageMatch = RouteSegment.forgotPass.find((element) =>
    routerpath.includes(element),
  );

  if (forgotPassPageMatch) {
    if (
      (routerpath.includes(RouteSegment.forgotPass[1]) ||
        routerpath.includes(RouteSegment.forgotPass[2])) &&
      !forgotPassCookie
    ) {
      route.push("/" + RouteSegment.LOGIN_PAGE);
      return Promise.resolve({
        next: false,
        redirect: "/" + RouteSegment.LOGIN_PAGE,
      });
    } else {
      return Promise.resolve(Next);
    }
  }

  const verifyPageMatch = RouteSegment.verifyUser.find((element) =>
    routerpath.includes(element),
  );

  if (verifyPageMatch) {
    if (routerpath.includes(RouteSegment.verifyUser[1]) && !authCookie) {
      return Promise.resolve(Next);
    }
  }
  // mendaftar ke pelatihan
  const pelatihandMatch = RouteSegment.pelatihan.find((element) =>
    routerpath.match(element),
  );

  if (pelatihandMatch) {
    // akses ke protected route
    if (authCookie) {
      //refresh local user data
      try {
        let resp = await fetch(
          process.env.RESTURL_SESSIONS + "/auth/login_refresh",
          {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            mode: "cors", // no-cors, *cors, same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            headers: {
              "Content-Type": "application/json",
              Authorization: "Bearer " + authCookie,
              // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: "follow", // manual, *follow, error
            referrerPolicy: "no-referrer",
          },
        );
        resp = await resp.json();
        if (resp && resp.success) {
          removeCookies(process.env.USER_DATA_KEY);
          setCookies(process.env.USER_DATA_KEY, resp.result.data.user);
        }
      } catch (err) {
        console.log(err);
      }

      let pelatihan = getCookie(process.env.PELATIHAN_KEY);
      pelatihan = pelatihan ? JSON.parse(pelatihan) : null;

      if (pelatihan) {
        //refresh local user data
        try {
          let resp = await fetch(
            process.env.RESTURL_SESSIONS +
              "/profil/pelatihan?pelatihan_id=" +
              pelatihan.id,
            {
              method: "GET", // *GET, POST, PUT, DELETE, etc.
              mode: "cors", // no-cors, *cors, same-origin
              cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
              credentials: "same-origin", // include, *same-origin, omit
              headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + authCookie,
                // 'Content-Type': 'application/x-www-form-urlencoded',
              },
              redirect: "follow", // manual, *follow, error
              referrerPolicy: "no-referrer",
            },
          );
          resp = await resp.json();
          if (
            resp &&
            resp.data.result.data &&
            resp.data.result.data.status_peserta == "1"
          ) {
            return Promise.resolve(Next);
          }
        } catch (err) {
          console.log(err);
        }

        if (pelatihan.status_kuota == 0 || pelatihan.sisa_kuota <= 0) {
          let dest =
            RouteSegment.EMPTY_STATE +
            "/?message=" +
            `<p>Maaf Digiers, saat ini Kamu tidak dapat mendaftar <br/><strong className="text-blue">${pelatihan?.nama_pelatihan}</strong><br/> karena kuota sudah penuh.
          Ikuti pelatihan menarik lainnya di DTS yaaa.</p>`;
          route.push(dest);
          return Promise.resolve({
            next: false,
            redirect: dest,
          });
        }
      }
      if (userData && userData.flag_nik == 0) {
        if (pelatihan && pelatihan.id) {
          let dest =
            RouteSegment.completeUserData[1] +
            "?data=" +
            `${EncryptionHelper.encrypt(
              "/pelatihan/" + pelatihan.id + "/daftar",
            )}`;
          route.push("/" + dest);
          return Promise.resolve({
            next: false,
            redirect: dest,
          });
        } else {
          route.push(RouteSegment.completeUserData[1]);
          return Promise.resolve({
            next: false,
            redirect: RouteSegment.completeUserData[1],
          });
        }
      }
      if (
        userData &&
        (userData.handphone_verifikasi == null ||
          userData.handphone_verifikasi == false ||
          userData.email_verifikasi == null ||
          userData.email_verifikasi == false)
      ) {
        if (pelatihan && pelatihan.id) {
          route.push(`/pelatihan/${pelatihan.id}`);
          return Promise.resolve({
            next: false,
            redirect: `/jadwal-pelatihan`,
          });
        } else {
          route.push(`/pelatihan/${pelatihan.id}`);
          return Promise.resolve({
            next: false,
            redirect: `/jadwal-pelatihan`,
          });
        }
      }

      if (
        userData.lengkapi_pendaftaran &&
        userData.lengkapi_pendaftaran.length > 0
      ) {
        let isLengkapiPendaftaran = userData.lengkapi_pendaftaran.find(
          (elem) => elem.pelatian_id == reqIdPelatihan,
        );
        if (isLengkapiPendaftaran) {
          return Promise.resolve(Next);
        }
      }
      // cek waktu pelatihan
      try {
        let currentTime = await fetch(
          process.env.RESTURL_SESSIONS + "/general/servertime/",
          {
            method: "GET",
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin",
            headers: {
              "Content-Type": "application/json",
              Authorization: "Bearer " + authCookie,
            },
            redirect: "follow",
            referrerPolicy: "no-referrer",
          },
        );
        let response = await currentTime.json();
        if (response) {
          const worldDate = new Date(response?.result[0].server_time);
          if (
            (pelatihan &&
              worldDate > new Date(pelatihan.pendaftaran_selesai)) ||
            !pelatihan.pendaftaran_selesai
          ) {
            let dest =
              RouteSegment.EMPTY_STATE +
              "/?message=" +
              `<p>Maaf Digiers, saat ini Kamu tidak dapat mendaftar <br/><strong className="text-blue">${pelatihan?.nama_pelatihan}</strong><br/> karena sudah melewati batas waktu pendaftaran.
                  Ikuti pelatihan menarik lainnya di DTS yaaa.</p>`;
            route.push(dest);
            return Promise.resolve({
              next: false,
              redirect: dest,
            });
          }
        }
      } catch (err) {
        console.log("error:", err);
      }

      // cek zonasi
      if (userData && userData.provinsi_id) {
        let pelatihan = getCookie(process.env.PELATIHAN_KEY);
        if (pelatihan) {
          pelatihan = JSON.parse(pelatihan);
          const pelatihanProvinsi = pelatihan?.pelatihan_provinsi;
          const provinsi = pelatihanProvinsi.find(
            (element) => element.provinsi_id == userData.provinsi_id,
          );
          if (!provinsi) {
            let dest =
              RouteSegment.EMPTY_STATE +
              "/?message=" +
              `<p>Maaf Digiers, saat ini Kamu tidak dapat mendaftar <br/><strong className="text-blue">${pelatihan?.nama_pelatihan}</strong><br/> karena domisili kamu tidak sesuai dengan zonasi pelatihan.</p>`;
            route.push(dest);
            return Promise.resolve({
              next: false,
              redirect: dest,
            });
          }
        }
      }

      // pelatihan aktif (dts)
      if (
        userData &&
        userData.pelatihan_aktif &&
        userData.pelatihan_aktif[0].jml > 0
      ) {
        // Pelatihan Aktif
        let pelatihan = getCookie(process.env.PELATIHAN_KEY);
        if (pelatihan) {
          pelatihan = JSON.parse(pelatihan);
          if (pelatihan.program_dts == "0") {
            return Promise.resolve(Next);
          }
        }

        let reqIdPelatihan = new RegExp("/pelatihan/(.*)/daftar").exec(
          routerpath,
        )[1];

        // pelatihan yang saat ini diikuti
        if (userData.pelatihan_aktif[0].detail?.length > 0) {
          let iscurrentPelatihan = userData.pelatihan_aktif[0].detail.find(
            (elem) => elem.pelatihan_id == reqIdPelatihan,
          );
          if (iscurrentPelatihan) {
            return Promise.resolve(Next);
          }

          let shouldCheckSurvey = userData.pelatihan_aktif[0].detail.filter(
            (elem) =>
              ["7", "8", "9", "10", "18", "19", "20", "21"].includes(
                elem.status_peserta,
              ) && elem.id_survey != null,
          );
          if (shouldCheckSurvey.length > 0) {
            route.push(
              `/auth/survey/${shouldCheckSurvey[0].pelatihan_id}/${shouldCheckSurvey[0]?.id_survey}`,
            );
            return Promise.resolve({
              next: false,
              redirect: `/auth/survey/${shouldCheckSurvey[0].pelatihan_id}/${shouldCheckSurvey[0]?.id_survey}`,
            });
          }
        }

        // status peserta pleatihan yang tidak kondisional dengan pengerjaan survey
        let pelatihanAktif = userData.pelatihan_aktif[0].detail?.filter(
          (elem) =>
            ["1", "3", "5", "6", "12", "15", "18"].includes(
              elem.status_peserta,
            ),
        );

        if (pelatihanAktif.length > 0) {
          let dest =
            RouteSegment.EMPTY_STATE +
            "/?message=" +
            `<p>Maaf Digiers, saat ini Kamu tidak dapat mendaftar <br/><strong className="text-blue">${pelatihan?.nama_pelatihan}
              </strong><br/>karena kamu masih memiliki pelatihan yang masih dalam proses atau belum diselesaikan.</p>`;
          route.push(dest);
          return Promise.resolve({
            next: false,
            redirect: dest,
          });
        }
      }

      // Cek eligible (level, tema, mitra)
      try {
        let eligible = await fetch(
          process.env.RESTURL_SESSIONS + "/auth/eligible",
          {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            mode: "cors", // no-cors, *cors, same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            headers: {
              "Content-Type": "application/json",
              Authorization: "Bearer " + authCookie,
              // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: "follow", // manual, *follow, error
            referrerPolicy: "no-referrer",
            body: JSON.stringify({ pelatihan_id: pelatihan.id }),
          },
        );
        let response = await eligible.json();
        if (response.success && response.result?.data?.user?.jml > 0) {
          let dest =
            RouteSegment.EMPTY_STATE +
            "/?message=" +
            `<p>Maaf Digiers, saat ini Kamu tidak dapat mendaftar <br/><strong className="text-blue">${pelatihan?.nama_pelatihan}</strong><br/> karena anda sudah mengikuti pelatihan serupa.
              Ikuti pelatihan menarik lainnya di DTS yaaa.</p>`;
          route.push(dest);
          return Promise.resolve({
            next: false,
            redirect: dest,
          });
        }
      } catch (err) {
        console.log("error:", err);
      }

      if (routerpath.includes(RouteSegment.verifyUser[0])) {
        //sudah verifikasi gaboleh masuk halaman verifikasi
        route.push("/");
        return Promise.resolve({
          next: false,
          redirect: "/",
        });
      }
      return Promise.resolve(Next);
    }
    route.push(RouteSegment.LOGIN_PAGE);
    return Promise.resolve({
      next: false,
      redirect: RouteSegment.LOGIN_PAGE,
    });
  }

  const match = RouteSegment.auth.find((element) =>
    routerpath.includes(element),
  );

  if (match) {
    // akses ke protected route
    if (authCookie) {
      if (userData?.wizard == null || userData?.wizard == 0) {
        try {
          window.location.href = RouteSegment.completeUserData[0];
        } catch (e) {
          console.log(e);
          route.replace(RouteSegment.completeUserData[0]);
        }
        return Promise.resolve({
          next: false,
          redirect: RouteSegment.completeUserData[0],
        });
      }

      // check jika punya last view pelatihan
      const lastViewPelatihanId = getCookie("LSPLID");
      if (lastViewPelatihanId) {
        route.push("/pelatihan/" + lastViewPelatihanId);
        return Promise.resolve({
          next: false,
          redirect: "/pelatihan/" + lastViewPelatihanId,
        });
      }
      return Promise.resolve(Next);
    } else {
      route.push(RouteSegment.LOGIN_PAGE);
      return Promise.resolve({
        next: false,
        redirect: RouteSegment.LOGIN_PAGE,
      });
    }
  } else {
    //validasi sebelum masuk ke halaman verifikasi
    // akses non login route
    const match = RouteSegment.nonAuth.find(
      (element) => element.includes(routerpath) || routerpath.includes(element),
    );
    if (match && authCookie) {
      if (routerpath !== "/") {
        route.push("/");
      }
      return Promise.resolve({
        next: false,
        redirect: "/",
      });
    } else {
      return Promise.resolve(Next);
    }
  }
};

export default Middleware;
