import axios from "axios";
import { setCookies, getCookie, removeCookies } from "cookies-next";
import _ from "lodash";
import moment from "moment";
import "moment/locale/id";
import eventBus from "../../components/EventBus";
import EncryptionHelper from "./EncryptionHelper";
import NotificationHelper from "./NotificationHelper";

const pathNoLayout = ["/login", "/register", "/mobile", "/mitra/daftar"];
const userDataBaseObject = {
  id: null,
  nama: null,
  nik: null,
  email: null,
  foto: null,
  nomor_handphone: null,
  nomor_handphone_darurat: null,
  email_verifikasi: false, // udah verif belum
  handphone_verifikasi: false, // udah verif belum
  status_verified: false,
  wizard: null, //udah isi wizard belum
  verifikasi_otp: null,
  flag_nik: 0, //verifikasi nik
  mandatory_survey: [],
  pelatihan_aktif: [],
  update_at_domisili: "", //tanggal terakhir update
  count_update_domisili: 0, //jumlah berapa kali update
  lengkapi_pendaftaran: [], // untuk user via import yg harus melengkapi form pendafatan
  rubah_domisili: null,
  provinsi_id: null,
};
const keyPressed = {};

const keyup = (evt) => {
  delete keyPressed[evt.key];
};

const handleErrorResponse = (error = Object) => {
  const msg = "";
  const keys = Object.keys(error);
  keys.forEach((key) => {
    error[key].forEach((err) => {
      msg += err + "\n";
    });
    msg += "\n";
  });
  return msg;
};

// Validation
const inputNumberOnly = (evt) => {
  keyPressed[evt.key] = true;
  if (
    (keyPressed["Meta"] || keyPressed["Control"]) &&
    evt.code?.includes("Key")
  ) {
    return;
  }

  if (
    evt.code &&
    !(
      evt.code.includes("Digit") ||
      evt.code.includes("Tab") ||
      evt.code.includes("Arrow") ||
      evt.code.includes("Backspace") ||
      evt.code.includes("Delete") ||
      evt.code.includes("Enter")
    )
  ) {
    evt.preventDefault();
    return;
  }
  if (
    isNaN(evt.key) &&
    !(
      evt.code?.includes("Backspace") ||
      evt.code?.includes("Arrow") ||
      evt.code?.includes("Delete") ||
      evt.code?.includes("Backspace")
    )
  ) {
    evt.preventDefault();
    return;
  }
};

const phoneNumberPreventZeroOnFirstInput = (evt) => {
  if (/^0/.test(evt.target.value)) {
    evt.target.value = evt.target.value.replace(/^0/, "");
  }
  evt.target.value = evt.target.value.replace(/[^\d]/, "");
};

const nameFirstInputValidation = (evt) => {
  let value = evt.target.value || "";
  while (value.substr(0, 1) == " ") {
    value = value.substr(1);
  }
  evt.target.value = value;
};

const inputPhoneNumber = (evt) => {
  keyPressed[evt.key] = true;
  if (
    (keyPressed["Meta"] || keyPressed["Control"]) &&
    evt.code?.includes("Key")
  ) {
    return;
  }

  if (
    evt.code &&
    !(
      evt.code.includes("Digit") ||
      evt.code.includes("Tab") ||
      evt.code.includes("Arrow") ||
      evt.code.includes("Backspace") ||
      evt.code.includes("Enter")
    )
  ) {
    evt.preventDefault();
    return;
  }
  // console.log(evt)
  if (
    isNaN(evt.key) &&
    !(
      evt.code?.includes("Backspace") ||
      evt.code?.includes("Arrow") ||
      evt.code?.includes("Backspace")
    )
  ) {
    evt.preventDefault();
    return;
  }
};

const inputNameValidation = (evt) => {
  keyPressed[evt.key] = true;
  if (
    (keyPressed["Meta"] || keyPressed["Control"] || keyPressed["Shift"]) &&
    evt.code?.includes("Key")
  ) {
    return;
  }
  // console.log(evt.code);
  if (
    evt.code &&
    !(
      evt.code.includes("Key") ||
      evt.code.includes("Tab") ||
      evt.code.includes("Arrow") ||
      evt.code.includes("Backspace") ||
      evt.code.includes("Enter") ||
      evt.code.includes("Quote") ||
      evt.code.includes("Space") ||
      evt.code.includes("Comma") ||
      evt.code.includes("Period") ||
      evt.code.includes("Minus")
    )
  ) {
    evt.preventDefault();
    return;
  }
};

const isRenderwithoutLayout = (pathname) => {
  if (typeof pathname == "string") {
    const currentPath = pathNoLayout.filter((path) => {
      return pathname.includes(path);
    });
    return currentPath.length > 0;
  } else {
    return false;
  }
};
const isOpenByRangeDate = (datetime) => {
  return new Date().toDateString() <= new Date(datetime).toDateString();
};

const dateDifference = (date, serverTime) => {
  var date = Math.ceil(
    (new Date(serverTime).getTime() - new Date(date)) / 86400000,
  );
  return date == 0 ? -1 : date;
};

const dateIsEqualToday = (date) => {
  return new Date().toDateString() == new Date(date).toDateString();
};

const timeDifference = (time) => {
  return (
    new Date(time) < new Date() &&
    new Date() <= new Date(time).setHours(23, 59, 0)
  );
};

const oneWeekNoticeBefore = (date, serverTime) => {
  var today = new Date(serverTime);
  var beforeDate = new Date(date);
  var beforeWeek = new Date(
    beforeDate.getFullYear(),
    beforeDate.getMonth(),
    beforeDate.getDate() - 7,
  );

  if (today > beforeWeek && today < beforeDate) {
    return true;
  }
};

const createDateFromMonth = (month) => {
  var date = new Date();
  date.setMonth(month);
  return date;
};

const isPelatihanOpen = (date_start, date_end, serverTime) => {
  var today = new Date(serverTime);
  var startDate = new Date(date_start);
  var endDate = new Date(date_end);

  if (today > startDate && today < endDate) {
    return 1;
  } else if (today > endDate) {
    return 2;
  } else {
    return 0;
  }
};

const isPelatihanOpenHTML = (date_start, date_end, serverTime) => {
  var today = new Date(serverTime);
  var startDate = new Date(date_start);
  var endDate = new Date(date_end);

  if (today > startDate && today < endDate) {
    return '<div className="badge badge-pill badge-lg bg-green text-white ms-auto fw-normal">Buka</div>';
  } else if (today > endDate) {
    return '<div className="badge badge-pill badge-lg bg-danger text-white ms-auto fw-normal">Tutup</div>';
  } else {
    return '<div className="badge badge-pill badge-lg bg-danger text-white ms-auto fw-normal">Belum Buka</div>';
  }
};

function stripHtml(html) {
  let tmp = document.createElement("DIV");
  tmp.innerHTML = html;
  return tmp.textContent || tmp.innerText || "";
}

const isEventOpen = (datetime, serverTime) => {
  return (
    moment
      .duration(
        moment(datetime).diff(moment(serverTime, "YYYY-MM-DD HH:mm:ss")),
      )
      .asSeconds() >= 0
    //     moment
    //         .duration(moment(serverTime, "YYYY-MM-DD HH:mm:ss").diff(moment(datetime)))
    //         .asDays() <= 0
  );
};

const isZoomURLOpen = (datetime) => {
  return dateIsEqualToday && timeDifference(datetime);
};

const countArticle = (articles) => {
  let count = 0;
  articles.forEach((article) => {
    if (article.kategori == category) {
      count = count + 1;
    }
  });
  return count;
};

const truncateString = (text = "", length) => {
  return text && text.length > length ? text.slice(0, length) + "..." : text;
};

const dateRange = (startDate, endDate) => {
  startDate = startDate.split(" ");
  endDate = endDate.split(" ");
  if (startDate[2] != endDate[2]) {
    startDate = `${startDate.join(" ")}`;
  } else if (startDate[1] != endDate[1]) {
    startDate = `${startDate.slice(0, 2).join(" ")}`;
  } else {
    startDate = `${startDate[0]}`;
  }
  return `${startDate} - ${endDate.join(" ")}`;
};

const generateListPelatihanDisplay = (items, keyMap = {}, page = "") => {
  const data = {
    items: [],
  };
  items.forEach((item, index) => {
    let tempObj = {
      dateRange: dateRange(item.pendaftaran_mulai, item.pelatihan_selesai),
      status:
        item.status_pelatihan == "Pendaftaran"
          ? 1
          : item.status_pelatihan == "Dibatalkan"
            ? 2
            : 0,
      gambar: page == "mitra" ? `${urlMitraLogo(item.logo)}` : "",
      url: `/pelatihan/${item.id}`,
    };
    Object.keys(keyMap).forEach((key) => {
      tempObj[key] = item[keyMap[key]];
    });
    data.items.push(tempObj);
  });
  return data;
};

const generateListMediaDisplay = (items, keyMap = {}) => {
  const data = {
    items: [],
  };
  items.forEach((item, index) => {
    let tempObj = {
      dateRange: moment(item.tanggal_publish)
        .locale("id")
        .format("DD MMMM YYYY")
        .toString(),
      url: `/${item.tabel}/${item.slug}`,
    };
    Object.keys(keyMap).forEach((key) => {
      tempObj[key] = item[keyMap[key]];
    });
    data.items.push(tempObj);
  });
  return data;
};

// auth user
const updateUserData = (key, value) => {
  const userData = getUserData();
  if (key in userData) {
    userData[key] = value;
  }
  // console.log("user data to update:", userData);
  saveUserData(userData);
  if (key == "foto") {
    eventBus.dispatch("updateFoto");
  }
};

const savePelatihanToCookie = (pelatihan = null) => {
  const {
    id,
    pelatihan_provinsi,
    nama_pelatihan,
    program_dts,
    pendaftaran_selesai,
    jml_peserta,
    sisa_kuota,
    status_kuota,
  } = pelatihan;
  const option = {
    path: "/",
  };
  setCookies(
    process.env.PELATIHAN_KEY,
    JSON.stringify({
      id: id,
      pelatihan_provinsi: pelatihan_provinsi,
      nama_pelatihan: nama_pelatihan,
      program_dts: program_dts,
      pendaftaran_selesai: pendaftaran_selesai,
      jml_peserta: jml_peserta,
      sisa_kuota: sisa_kuota,
      status_kuota: status_kuota,
    }),
    option,
  );
};

const saveUserEmailForgotPass = (email = null, pin = null) => {
  const option = {
    path: "/",
  };
  const payload = {
    email: email,
    pin: pin,
  };
  setCookies(
    process.env.USER_EMAIL_RESET_PASSWORD,
    JSON.stringify(payload),
    option,
  );
};
const getUserEmailForgotPass = () => {
  return getCookie(process.env.USER_EMAIL_RESET_PASSWORD)
    ? JSON.parse(getCookie(process.env.USER_EMAIL_RESET_PASSWORD))
    : null;
};
const getUserVerifyCookie = () => {
  return getCookie(process.env.VERIFY)
    ? JSON.parse(EncryptionHelper.decrypt(getCookie(process.env.VERIFY)))
    : null;
};

const removeUserEmailForgotPass = () => {
  return removeCookies(process.env.USER_EMAIL_RESET_PASSWORD, { path: "/" });
};

const saveUserData = (userData, remember = false) => {
  const option = {
    path: "/",
  };
  if (remember) {
    option["maxAge"] = 24 * 3600;
  }
  const userDataTemp = {};

  Object.keys(userDataBaseObject).forEach((key) => {
    userDataTemp[key] = userData[key];
  });
  // delete userData["pelatihan_aktif"];
  setCookies(process.env.USER_DATA_KEY, JSON.stringify(userDataTemp), option);
  // broadcast userdata udpate
  // eventBus.dispatch("userDataUpdated", userData);
};

const tokenGetFromCookie = () => {
  return `Bearer ${getCookie(process.env.USER_TOKEN_KEY)}`;
};

const saveUserEmailRegister = (userData) => {
  const option = {
    path: "/",
  };
  option["maxAge"] = 3600;
  setCookies(process.env.USER_EMAIL_REGISTER, userData);
};

const getUserEmailRegister = () => {
  return getCookie(process.env.USER_EMAIL_REGISTER);
};

const saveUserToken = (token, remember) => {
  // Cookies.set(process.env.USER_TOKEN_KEY, token, { path: '/'});
  const option = {
    path: "/",
  };
  if (remember) {
    option["maxAge"] = 24 * 3600;
  }
  setCookies(process.env.USER_TOKEN_KEY, token, option);
};

const saveLastViewPelatihanCookie = (pelatihanId) => {
  const option = {
    path: "/",
  };
  option["maxAge"] = 3600;
  setCookies("LSPLID", pelatihanId, option);
};

const getLastViewPelatihanCookie = () => {
  return getCookie("LSPLID");
};

const getUserToken = () => {
  return getCookie(process.env.USER_TOKEN_KEY);
};

const isLoggedin = () => {
  return !(getUserToken() == null);
};

const getUserData = () => {
  return getCookie(process.env.USER_DATA_KEY)
    ? JSON.parse(getCookie(process.env.USER_DATA_KEY))
    : null;
};

const refreshUserCookies = (userData) => {
  removeCookies(process.env.USER_DATA_KEY, { path: "/" });
  saveUserData(userData);
};

const destroyUserCookies = () => {
  removeCookies(process.env.USER_DATA_KEY, { path: "/" });
  removeCookies(process.env.USER_TOKEN_KEY, { path: "/" });
};

const makeDaysFromMonth = (month) => {
  var month = month ? month : moment().year() + "-" + month;
  var days = Array.from(Array(moment(month).daysInMonth()), (_, i) =>
    moment(month)
      .date(i + 1)
      .format("YYYY-MM-DD"),
  );
  return days;
};

const makeMonthFromYear = (year) => {
  var year = year ? year : moment().year();
  var months = Array.from(Array(12), (_, i) =>
    moment(year + "-" + (i + 1), "YYYY-MM").format("YYYY-MM"),
  );
  return months;
};

// mengubah undefined atau null value ke value yg diinginkan
const reValue = (v, replace = "") => {
  if (typeof v == "undefined") {
    return replace;
  }
  if (v == null) {
    return replace;
  }
  return v;
};

const stringToDate = (date) => {
  return new Date(moment(date, "DD/MM/YYYY").format("YYYY-MM-DD"));
};

const scrollSpy = (id_element, offset = 10, speed = 50) => {
  if ($(id_element).offset()) {
    $("html").animate(
      {
        scrollTop: $(id_element).offset().top - offset,
      },
      speed, //speed
    );
  }
};

const addNameValidation = () => {
  return {
    onInput: nameFirstInputValidation,
    onKeyUpCapture: keyup,
    onKeyDown: inputNameValidation,
  };
};

const addPhoneNumberValidation = () => {
  return {
    // onKeyDown: inputNumberOnly,
    onInput: phoneNumberPreventZeroOnFirstInput,
    onKeyUp: keyup,
  };
};

const titleCase = (str) => {
  return str
    .toLowerCase()
    .split(" ")
    .map(function (word) {
      return word.charAt(0).toUpperCase() + word.slice(1);
    })
    .join(" ");
};

const formatChars = () => {
  return {
    n: "[0-1]",
    m: "[0-9]",
    e: "[0-3]",
    d: "[0-9]",
    z: "[1-2]",
    y: "[0-9]",
  };
};

const getColorClassStatusPendaftaran = (status = "") => {
  let s = status?.toLowerCase() || "";
  let color = "bg-yellow";
  switch (s) {
    case "submit":
      color = "bg-blue";
      break;

    case "tidak lulus administrasi":
      color = "bg-red";
      break;

    case "tes substansi":
      color = "bg-gigas";
      break;

    case "tidak lulus tes substansi":
      color = "bg-red";
      break;

    case "seleksi akhir":
      color = "bg-gigas";
      break;

    case "ditolak":
      color = "bg-red";
      break;

    case "diterima":
      color = "bg-green";
      break;

    case "pelatihan":
      color = "bg-green";
      break;

    case "administrasi akhir":
      color = "bg-green";
      break;

    case "lulus pelatihan":
      color = "bg-green";
      break;

    case "tidak lulus pelatihan":
      color = "bg-red";
      break;

    case "cadangan":
      color = "bg-yellow";
      break;

    case "mengundurkan diri":
      color = "bg-red";
      break;

    case "pembatalan":
      color = "bg-red";
      break;
    case "tidak hadir":
      color = "bg-red";
      break;

    default:
      color = "bg-blue";
      break;
  }
  return color;
};

// cek base 64 from string
const isBase64Data = (str) => {
  // var base64regex = /^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/;

  try {
    window.atob(str.split(",")[1]);
    // window.alert("b64")
    // let matchs = base64regex.exec(str)
    // console.log(str,matchs)
    return true;
  } catch (error) {
    // console.log(error)
    // window.alert("nob64")
    return false;
  }
};

const formatPhoneNumber = (number) => {
  return number
    .split("")
    .reverse()
    .join("")
    .replace(/.{3}/g, "$& ")
    .split("")
    .reverse()
    .join("");
};

const refactorMenu = (
  menuList = [],
  id_parent = 0,
  parentProp = "id_parent",
) => {
  let parent = _.filter(menuList, (o) => {
    return o[parentProp] == id_parent;
  });
  // console.log(menuList)
  let np = parent.map((m) => {
    let childs = _.filter(menuList, (o) => {
      return o[parentProp] == m.id;
    });
    return { ...m, childs: childs || [] };
  });
  // console.log(np)
  return np;
};

function isValidHttpUrl(string) {
  let url;

  try {
    url = new URL(string);
  } catch (_) {
    return false;
  }

  return url.protocol === "http:" || url.protocol === "https:";
}

function setUpUrl(url = "", isHttps = true, addHttp = false) {
  if (isValidHttpUrl(url)) {
    return url;
  } else {
    // console.log(url,url.includes("www."))
    if (url.includes("www.")) {
      let newUrl = `${isHttps ? "https://" : "http://"}${url}`;
      // console.log(newUrl)
      return newUrl;
    } else {
      if (addHttp) {
        return "https://" + url;
      }
      return "";
    }
  }
}

const capitalizeFirstLetter = (sentence) => {
  const words = sentence.split(" ");

  return words
    .map((word) => {
      return word[0].toUpperCase() + word.substring(1);
    })
    .join(" ");
};

// storage
const storageUrl = "https://back.dev.dtsnet.net";

const urlBroadcastImage = (icon) =>
  process.env.STORAGE_URL + "dts-publikasi/" + icon;

const urlAkademiIcon = (icon) =>
  icon
    ? process.env.STORAGE_URL + "dts-pelatihan/" + icon
    : "/assets/img/akademi/fga.png";

const urlMitraLogo = (logo) =>
  logo
    ? process.env.STORAGE_URL + "dts-partnership/" + logo
    : "/assets/img/akademi/fga.png";

const urlPublikasiProfil = (logo) =>
  logo
    ? process.env.STORAGE_URL + "dts-dev/" + logo
    : "/assets/img/akademi/fga.png";

const urlBrosur = (file) =>
  file ? process.env.STORAGE_URL + "dts-pelatihan/" + file : "#";

const urlPublikasiLogo = (logo) =>
  process.env.STORAGE_URL + "dts-publikasi/" + logo;

const urlSertifikat = (certt) => {
  return process.env.STORAGE_URL + "dts-sertifikat/" + certt;
};

const isCancelPelatihanAvailabel = (
  pendaftaran_mulai,
  pendaftaran_selesai,
  status_peserta,
) => {
  if (
    status_peserta != "Submit" ||
    status_peserta?.toLowerCase() != "belum melengkapi"
  ) {
    return false;
  }
  return (
    moment().isSameOrAfter(moment(pendaftaran_mulai)) &&
    moment().isBefore(moment(pendaftaran_selesai))
  );
};

const mask = (str, start = null, end = null, sep = null) => {
  if (!str) return;
  end = end ?? str.length;
  if (sep) {
    str = str.split(sep);
    const maskchar = "*".repeat(3);
    str[0] = str[0].substring(0, str[0].length - 3) + maskchar;
    return str.join(sep);
  } else {
    const maskchar = "*".repeat(end - start);
    return str.substring(0, start) + maskchar + str.substr(end);
  }
};
const fromImageElToBase64 = (img) => {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
};

const axiosDownloadFile = (
  url,
  fileName,
  option = { responseType: "blob", exportData: "application/pdf" },
) => {
  let cfg = {
    url: url, //your url
    method: "GET",
    headers: {
      Authorization: tokenGetFromCookie(),
    },
  };
  if (option.responseType == "blob") {
    cfg["responseType"] = "blob";
  }
  return axios({
    ...cfg,
  })
    .then((response) => {
      // console.log(response)
      // create file link in browser's memory
      let href;
      if (option.responseType == "blob") {
        if (response.data.size != 0) {
          href = URL.createObjectURL(response.data);
        } else {
          NotificationHelper.Failed({
            message: "Gagal mengunduh. Data tidak ditemukan",
          });
          return;
        }
      } else {
        href = `data:${option.exportData};base64,${response.data}`;
      }

      // create "a" HTML element with href to file & click
      const link = document.createElement("a");
      link.href = href;
      link.setAttribute("download", fileName); //or any other extension
      document.body.appendChild(link);
      link.click();

      // clean up "a" element & remove ObjectURL
      document.body.removeChild(link);
      URL.revokeObjectURL(href);
    })
    .catch(() => {
      let msg = fileName.toLowerCase().includes("sertifikat")
        ? "Mohon tunggu, Sertifikat dalam proses generate. Mohon coba beberapa saat lagi"
        : `Terjadi kesalahan saat mengunduh file ${fileName}`;
      NotificationHelper.Failed({ message: msg });
    });
};

const noWaDigitalent = "6289633707000";

const linkIcon = (base64Data) =>
  base64Data ||
  `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAD9CAMAAAAxtrp5AAAC/VBMVEUAAAAwfvYAAABFjvQ2g/QygfY9i/JHR0czMzMFBQUHBwcGBgZGRkYxgPYAAAAAAAA8PDw/Pz8AAABwqOwAAACkpKQwfva/v786hPSZmZkwfvYJCQkcHBxFkfSqqqoAAAABAQFISEgKCgoAAAAAAAABAQEAAAAVFRUYGBgZGRkKCgoAAABgYGD///8BAQFxrfAygPYBAQEygPYBAQEGBgYODg4zgPYvf/UJCQkiIiICAgIPDw8mJiYxgPY4hPUUFBQjIyMTExMxgPY1gvUzf/YqKipAivQwMDASEhIzMzMwgPY7OzseHh4uLi48h/UXFxcICAgrKys3gvUcHBwtLS0MDAwMDAwpKSlHkfM0NDQ+i/UPDw8XFxdGRkYdHR03g/UNDQ0dHR05OTkcHBxFRUUEBARdofdSlfJ3d3djoPFAQEAnJyc3g/Vbm/FRUVE0gPZJSUk6OjqOjo5tbW1Ql+84hfQ1g/SS2/9mZmYzgfciIiJIjvKqqqpppfAmJiYlJSVNTU0AAACAgIAiIiLMzMyWtOE5OTkAAABTU1OZzO4GBgY2hPUrKysPDw9gYGBJSUmFhYU6OjpgYGBGRkY5OTlMk/NCjvVWmO41NTUFBQU3gPY/ifRZm/FRlfE0NDRJSUkuLi5AQEAvLy9AjvQ7iO4XFxcfHx8PDw+Av790uegNDQ0PDw9zq+6A//9Vqv9CQkJOTk4pKSlJku1Lj/MdHR2YuupJkvNqqv+qwvNYWFhRl/OPv+9Gi/N5qudppPGq1f8cHByAs/ITExM3g/R3ru9gYGCqqqoAAP+ZzMxAgP+fn59joe+A1f8SEhJineuCgoIrKytVVVWDsvBqamqqqv+Pj49mmf9ISEifv98VFRWvz+8lJSWRvellZWUjIyOAgICPj48+Pj5bpP8mJiZxcXEsLCwVFRUFBQWGtu2qqqoAgP9AgL8WFhZVVf8qKirMzP9VqqoeHh5UVFR0dHSAgP9VgNWbm5uMjIxNmf85jv9VVVV7e3tTmexwcHBOTk4vtLVIAAAA/3RSTlMA//9vz+RPXZHb1ix76tbqalH7KSYO8gTLBfwct0MD2uVDy83v/z1vsT5+9AgB0SKi4NjEy8Ort8Vb6LWVwJ/Cprjeg88MiyCuFMh9rJeVj71ftqY+u5FYVk57IaoWYK1MnQltP3geYA9LCCfEOBZUB3YJDkBxkAcKQWiOBhF5iVZZCh4FETECKw+FNC+IJSoXPhgLJH9oTVs3HKZZcYQ7Mgx+SB5kMmUEC9DTSQIDHxdFDik+JWkMFSAWEBYVWgZSFHthLzAJAQUIEDEGDg0rEk4hHQMiBVIIYBBtOj9RQhBKDjYiaRhdKgwCBHID/wUDfzosAgYpPAoSYxsoQmhIPMYRAAAOGUlEQVR4nO2deXwU1R3AZxcUsA0LoS4m4hJDaqJhI4QkxIQbwqGEUyAQDgG571PKKaKIWgW1IogWrUfrAahQRIuKCN71vq320Grtfd/tx9l5vzfzZjLzZt7u7M5m5vf9B97M/t6+980c75pZCUEQBEEQBAks+97e/1evy9AseOXout6hUKi8at6jXhcl23n6qZ6yKWDPbK+Lk9XcxqhKcMd+r0uUvSxKnIA6xrzvdZmylZVRo6tQqJvXhcpSZjc5rhIM9rpY2ckYM1eh/LO8Llc20mByEiYY5HXBshHTkzBxaN3udcmyjxMWB1YotNrromUfz1q5CkW9Llr2YX55V2Rd43XZso4zLGWFDntdtqyjpbWsFl6XLetAWQKgLAFQlgAoSwCUJQDKEgBlCYCyBEBZAqAsAVCWAChLAJQlAMoSAGUJgLIEQFkCoCwBUJYAKEsAlCUAyhIAZQmAsgRAWQKc38GSZ7wuG4L4lCMPHR64sPyU5Jm26lde1yEjdBg4Jiff+oLulOi0eV7XJM28cHtV1AVRoKvbY17XJ4081M01UYSqX3hdpTTx63Uum1J4xetqpYN/jrBcj5wSVT/wumau86/BOWlRJeO3x3se23lKulTJTxU84XX1XGWR9bptN/DTUwWvthiQVleh6B+8rqJrzF6YXlUyD3ldR7e4uzztrkIDva6kO+xbleZT0Eeynh+cAVXyew28rqcrjMiIK3+0tPpkxpUvTsMMHVehkA+edR2Rjm6zGdGrvK5qygzOlCsfXLJWZUpVKP8Or+uaKremsedsoNkfWK9moi1KiP7Q68qmyGWZajTITPK6sqli/d4Bt4k2e1cvZ+wkzJnjdV1T5f30D8qAqtXNf26nITOmWg483+uaps7RCx1Wd8CgPUuWnJokK5t/sz3BJEemBg0+C1+bIs12Mue18O77vS5nVuBgvK/3yheahO05w5Jmf8ez5KjVS8NUojt/ZBIXyGWSc+xcTbvHNC6Isu43vJa1Cd02mAcGUdZtNq76WC3jCKKsaTauLAMDKGsD/yzkDD0FUNYJrquq31hHBlAW9yyM8vpywZP1LreRxZ3eC56sF3muenJ/OiB4su7gybqVGxo8Wbyh995Pc0ODJ2sQR1YDPzR4sjiuBtgskw2crA85svbYxAZO1k84su62iQ2crKc4sux+QydwsrpZV7icfy9EWSwX2sWiLI11drGBk8VZvvYtu1iUhbKsQVkCoCwBUJYAnLvhTrtYlKWB7SwjPFlv28QGTtYt1hUOPWkTGzhZvFGH62xiAyfrCEeWXX8ncLJ4I6W9L+OHoiwWm/MweLKqOLLG8EODJ4v3OGb0Nm5o8GQ9ypEV6skNDZ6sv/FkhTrwQoMni3vRCvXkLecOoCze/I687u9568gAynqR/0KxW6wjAyiLfx6GQtavfwyirNv5svJXWgUGUdYGmwfJ862eLQmiLNtHd6KTzB8GC6Sse2wf+R1j2ksMpCwHb+uJTjIZCQymrIO2suRlIquavOP9fB0dDl7XcCFthvhYVtzmiRTCKX3m2QxwyY953kJWivtYlvRfh2/5k58In/MhP6sNytt//CyLO29hIBrNsWBaC+URzoPyw0C+lvWkOy+QjK5OPMb5RE9/y5KWuCJLbsEOjiceNPO3LPdeYFe1X37e0+eyrnLtTa5V+ySpub/zyY4lrr26p8rrqmSAFm698S/f7z/qkWC1W7b48xw+wenre+zI5z965w/e5D0iJoLN7Kw/WOSSrajXFckIL7tjK2r26hr/4dKxddjremSGDq5c5X3egld5xg1bQZElvbsuZVfRYPw0X4I/NqR6cAXjbgh0sJmltiPH6wpklN9x341hS7N/IbAgb6bwYupAnYWEW8uT7VgH7cBS2DkgKV3N/tX4STKnXPw3DnOu9LrUnjFvoKCuHL8PKXO5rGGEwIhzHz//mK8jrmpY1dLRAVb+rP9+bjUZrlm0ZMTClpw7ZM6ghafaLoYIFkdOLDrLlJXcNfMIgiAIgiAIgiAIgiAIgiAIgiAIgiAIEmziC9ote2SYUMiQ9ssmikX4hJvHhmXaXuo8IraxQI6ouSBtRcoi4ucS6khyU8JVOByZ4jiDCyDiztTKcYgU44bUckkzeaSu4a8pqTWJoyTBvU7j42UQMTy1crQhuXw9tVzSjF7WMEiFC53GL6YRnVMrR3OWNdZpvHosVsKGbSUK3xUsh/uy4tNJSeAC4wZ6WXkVkLzPcQYlEDEV0meTZCfBcrgvq+M3SZZnupelXpb0eHclNdJ5BvNJNScvh3SAZEmLexWEyybKr4hxTN3GtuG2E7rSZJBkyQ2njqJ5xNczcoMlK0VQlgA+k/VGbU1ubm6PoUrCIOtkawXDjf9kbQ85okvtP5QU+Ujrj8m+IZDMg13DSX6nka1Xs9nUjyqVsxnVS0nshTjYZyGr1yg5IrdonLElAsFKt+HvidJFRvViLx8HWreuh5rVK59cJuaIMH8Tud8l6PK/uFFWe6gqE3H1Mi0i96KYJHUm/29Hdn8Ddp2eSOSqnwTU67702Sht67j/qN2ksLWsGa2KmJx6/Fn+apW2ZOM5Uuz/2nf22Kbuv9hYELZGTvkUakpLfZetrO1QDaCiLilZSzdF2M1FxTFbWSdpm08tLDO0QWXNYP4EMr1muChrgjGP6ptsZJ2rtxsOl25JQlbdUOOO6+1k3auzq1Bwo1HWJdWGj4yFw88FWY80+f4wXAWtZG3VTkFKBXRwBGTFJjfJJVIJ/7GQNbWpq3C4u2oLZDXll27JmtK05irmsmYYjysGAVnF1rlYyLpTc1VaWqr+v4CeiZayug9xSVYP9fsfHD9z9Kz+7B/PXFYvdX+b8cWjZ9XWMBFmsoqLi0eSZKH8XxnlI0PUL4r0nTV67bUPsBduU1ldT4O9NeO/lBu9Hw+lvmgHX5NVM779a+/sGq+WjHziQHHxb2HDcaUgtOvqlBtofhUTyIbl59nImk/3Fq0l9+Wl52mCzWTJmLSzptOQyZ+RDZcylxpTWfTa2v9m2L0dZBR8xyBr6F1kw7Ba2FAwH0JSa2eNg+wqF6ubtCuDqSxoM4XbLFAj2qsRjmVNoSHXK22xBHO1C76ZrOVwYPWdq2ayAI6tyXpZlSvULOkfYLsrsqh6clYTjnNlwaW8iB0yvldY1mj4xGSmobSYnmemshpJolT7G0nSTLItslRJgazIt7UP0LvXvyGdkqx3ILPj7Mab6didmayOsO9aNqJjoais/lAxOIMIE3myNpLEg2zAz+DQIn0CkMUO6MbAf4kbsqD5VnOFbuv3OLJ+ShJFe3URx0RlQcX66nK5kv6VIK2TBeP6H+gi4K5AOj6QJ+k5AXBp7ALJlGTBvVD355Kk3RxZcC8s0Ec8LiirEQ6JS/TZ0JsLJFlZz0FEm0IWaJaQiRG1u8MA92FXZMGX6c4p+ejmyAK9/fURMWitOZV1JnzgXH0237eWdQXsMoWUxkwW6HdT1iz9Vnpdci6L1zeUcSqL091ZgbKcy5rLk0Wu3xmSdb1+63P2svRXZulKd2QlexpaX7PclAVVN3SSzuHIuogkatiBJK0j4FTWMPgrGS7wszgXeIgw6GVJtyzoD0Ru0m110HQIT9RFXJJk06Gzznmeg6aDQS9LumXFoXS683AFHYgwbcHDPjIADcTomJxjWf3gE1vZbNSRNUjrZLUjCcP5z5JuWbRVF2HWE8Vh1YyFLDhzI9DxVphKIxzLug8+UclMpL+ujr/CBp2s+0hDq4DpzHRddpFCO3J8pl0WHYGrUTuHsbW0yOayoJnH+j2mjonxZT3A1FMdX6FjCNJd0AWyGnWALlUhHUHQ7gfQZncgKw++ljEuwA5avshUMib3ljZcZS5rBh0viBSTkZAd0GvjyLoUIpgLHXUeLrxYSccPqd1oC1m051j7MOymwyNFnzuWRbOs0IyLoA3utpn+xeaZndiBU/PBP3r5l0cgh3+x+QNdhIUsmo707SejDMp8rg3V9R25efP0B5hczGWtp4POBX/a+9LpO471pZ+mizCcyKJHb5vhckE2icqaQe9AJpjLqjPOsNjLitHVbQrkEKaDNGaYDytvYQdTNSrIAI0zWfS2ndywsrS3qa3fw78WExY3NY2ohjPCQpb0kcnszvgmuRTRg8ViwuLnZrZqttDdTmStYCfxkpgK+8RY91Ff8mVJ24wRFTv4LXhpDTs9BbLWG20VzbSbCjOx1fmAuteJLGkik0Uyk6zbdedV7tDltpOsD9PLB4monWvTN5SkH9PhcGaSNW+mru5Fu+xnpIfQYXAgMvZ1bacjWdIu7UuTkSXVfaQdKmXyzcl++r7rJ9r1uSwxcWcnS1ozQb1uadP3jeoMTbh0aKOT6fvYxXRQNkGlrgngTJbUOJZ+aVKy5FN5a7/qsnBZZevGxPtU45e3UoB79Fsk9YYuouuC+spERH2jsrxPL2sxibhcnYxQ6jnlzBuVzWwXZ0qrkrKysoqS95R14zeQuFawE4rxF90X5w1pVZIoa/Xw94bp19fBx7WpF5ltUBLdB+duWaBsPduZHFN0VROMAFm7kvnemMiaQkJc349vXrwEslxb0eVn6Bg8ytLzGtHSXbcRWpi5670pU9ZyNRxE7FNMa+AsLPOqUNnK0i5EDDPqUEdbP7u9K1aWAgPL4chG8phY7FPaqO2sH3BF5Ma41gRuW792bT9tuRYsGkMY2pv3/sPj8PJuwrVmyxXD1exKHERlpomtat2CGERjqzpMCZSO1C/EQRg6Hiph1uF2no6HFZ+uu0fXd6os7FQ/erd4FxxBEARBEARBEARBspivAIGHVtmLTws1AAAAAElFTkSuQmCC`;

const getUrlLogoDts = (color = "default") => {
  let logo = "/assets/img/logo-dts.svg";
  switch (color) {
    case "white":
      logo = "/assets/img/logo-dts-white.png";
      break;
  }
  return logo;
};

const timezoneConverter = (date, tzString) => {
  return new Date(
    (typeof date === "string" ? new Date(date) : date).toLocaleString("en-US", {
      timeZone: tzString,
    }),
  );
};

const PortalHelper = {
  urlPublikasiProfil,
  userDataBaseObject,
  urlPublikasiLogo,
  inputPhoneNumber,
  handleErrorResponse,
  inputNumberOnly,
  inputNameValidation,
  addNameValidation,
  addPhoneNumberValidation,
  isPelatihanOpen,
  isPelatihanOpenHTML,
  isEventOpen,
  isZoomURLOpen,
  oneWeekNoticeBefore,
  countArticle,
  truncateString,
  stripHtml,
  generateListPelatihanDisplay,
  dateDifference,
  dateIsEqualToday,
  timeDifference,
  isOpenByRangeDate,
  getUserData,
  saveUserToken,
  saveUserData,
  generateListMediaDisplay,
  getUserToken,
  isLoggedin,
  destroyUserCookies,
  isRenderwithoutLayout,
  tokenGetFromCookie,
  updateUserData,
  keyup,
  makeDaysFromMonth,
  makeMonthFromYear,
  getUserEmailForgotPass,
  removeUserEmailForgotPass,
  saveUserEmailForgotPass,
  reValue,
  scrollSpy,
  stringToDate,
  phoneNumberPreventZeroOnFirstInput,
  nameFirstInputValidation,
  titleCase,
  formatChars,
  getColorClassStatusPendaftaran,
  refactorMenu,
  urlAkademiIcon,
  isBase64Data,
  saveUserEmailRegister,
  getUserEmailRegister,
  formatPhoneNumber,
  capitalizeFirstLetter,
  getUserVerifyCookie,
  setUpUrl,
  urlMitraLogo,
  fromImageElToBase64,
  savePelatihanToCookie,
  axiosDownloadFile,
  urlSertifikat,
  refreshUserCookies,
  isValidHttpUrl,
  urlBrosur,
  noWaDigitalent,
  linkIcon,
  getUrlLogoDts,
  timezoneConverter,
  isCancelPelatihanAvailabel,
  mask,
  saveLastViewPelatihanCookie,
  getLastViewPelatihanCookie,
  urlBroadcastImage,
};

export default PortalHelper;
