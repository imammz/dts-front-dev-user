// Import the functions you need from the SDKs you need
import firebase from "firebase/app";
import "firebase/messaging";
// import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyC2acfqAjlOXLgEtSrg69cMfmKn9nUtPaA",
  authDomain: "dts-dev-5cedf.firebaseapp.com",
  projectId: "dts-dev-5cedf",
  storageBucket: "dts-dev-5cedf.appspot.com",
  messagingSenderId: "939587175979",
  appId: "1:939587175979:web:a6b0541f1f6ff570a18c6e",
  measurementId: "G-XZPTK56BW9",
};

// Initialize Firebase
// console.log(firebase.app)
const app = !firebase.apps.length
  ? firebase.initializeApp(firebaseConfig)
  : firebase.app();
// const analytics = getAnalytics(app);

const vapidKey =
  "BNiF55m0Z6am0ed-LdH2BpyFlZMxhpRP2Z32JXGhC9hkCyIYOC0_BIctWa25y_n-sOQCDzD4KdkNonuhQ2-IYM4";

const messaging = {
  messaging: () => {
    return firebase.messaging();
  },
  get getToken() {
    return this.messaging().getToken({ vapidKey: vapidKey });
  },
  onMessage(func) {
    this.messaging().onMessage((payload) => {
      // console.log("payload", payload);
      if (typeof func == "function") {
        func(payload);
      }
    });
  },
  requestPermission: function () {
    return new Promise(async (resolve, reject) => {
      const a = await window?.Notification?.requestPermission();
      if (a) resolve(a);
      else {
        window?.Notification?.requestPermission(resolve);
      }
    });
  },
  // onBackgroundMessage (func) {
  //     this.messaging().onMessage()
  // },
};

const fbApp = {
  getApp: app,
  messaging: messaging,
};

export default fbApp;
