// memastikan user belum login/otentikasi
const nonAuth = ["/register", "/login"];
// memastikan sudah berstatus login tapi belum verifikasi (old)
const verifyUser = ["/verify-user/verifikasi", "/auth/verify"];
const surveys = [];
const completeUserData = [
  "/verify-user/complete-user-data",
  "/verify-user/nik",
];
// memastikan belum login
const forgotPass = [
  "/forgot-pass",
  "/forgot-pass/new-password",
  "/forgot-pass/verifikasi-email",
];
// memastikan sudah login
const auth = ["/auth"];

// memastikan sudah verifikasi email dan nik untuk mendaftar pelatihan
const pelatihan = [/\/pelatihan\/\d+\/daftar/, /\/test-substansi\/\d+\/d+/];
// const pelatihan = [];

const LOGIN_PAGE = "/login";
const HOME = "/";
const USER_PROFIL = "/user-profile";
const EMPTY_STATE = "/empty-state";
const AVATAR_NAME = "https://ui-avatars.com/api/";
const WA_URL = "https://wa.me/";
const INSTAGRAM_MEDIA_URL = "https://graph.instagram.com/me/media";
const MAJAPAHIT_BEASISWA =
  "https://api.dts.sdmdigital.id/beasiswa/api/get-scholarship-data";
const CHECKTIMEZONE = "http://worldtimeapi.org/api/timezone/";
const SSO_FORGOT_PASSWORD =
  "https://sso.sdm.dev.sdmdigital.id/realms/dignas.space/login-actions/reset-credentials?client_id=dtsapi.dtsnet.net&tab_id=a2kRySv00qw";
const PELATIHAN_DAFTAR = /\/pelatihan\/\d+\/daftar/;

export default {
  CHECKTIMEZONE,
  MAJAPAHIT_BEASISWA,
  INSTAGRAM_MEDIA_URL,
  WA_URL,
  AVATAR_NAME,
  nonAuth,
  forgotPass,
  auth,
  verifyUser,
  pelatihan,
  completeUserData,
  LOGIN_PAGE,
  HOME,
  USER_PROFIL,
  EMPTY_STATE,
  SSO_FORGOT_PASSWORD,
  PELATIHAN_DAFTAR,
};
