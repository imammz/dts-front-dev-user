import axios from "axios";
import eventBus from "../components/EventBus";
import NotificationHelper from "./helper/NotificationHelper";
import PortalHelper from "./helper/PortalHelper";
import validationMessage from "./validation-message";

class Ax {
  constructor() {
    const ax = axios.create({
      baseURL: `${process.env.RESTURL_SESSIONS}`,
    });
    ax.interceptors.request.use(
      (config) => {
        let { headers } = config;
        // console.log(headers)
        const authToken = PortalHelper.tokenGetFromCookie();
        if (!authToken.includes("undefined")) {
          headers.common = { ...headers.common, Authorization: authToken };
          config.headers = headers;
        }
        return config;
      },
      (err) => {
        return err;
      },
    );
    ax.interceptors.response.use(
      (v) => {
        // console.log("call inter")
        return v;
      },
      (err) => {
        // console.log(err.response)
        let msg = err.response?.data?.message;
        if (msg == "Error.") {
          msg = err.response?.data?.result;
        }
        NotificationHelper.Failed({
          message: msg || err.toString(),
          okText: "Tutup",
        });
        if (
          err.response?.status == 401 &&
          msg == "Sesi berakhir, silahkan login kembali"
        ) {
          eventBus.dispatch("logout");
          return;
        }
        if (
          msg?.includes(
            `duplicate key value violates unique constraint "user_email_idx"`,
          )
        ) {
          NotificationHelper.Failed({
            message: validationMessage.unique.replace("__field", "Email"),
          });
          return;
        } else if (
          msg
            ?.toLowerCase()
            .includes(
              "Koneksi gagal, mohon periksa koneksi Anda dan coba kembali beberapa saat lagi",
            ) ||
          err.message
            ?.toLowerCase()
            .includes(
              "Koneksi gagal, mohon periksa koneksi Anda dan coba kembali beberapa saat lagi",
            )
        ) {
          NotificationHelper.Failed({
            message:
              "Gagal memuat halaman, mohon periksa koneksi anda atau refresh halaman",
          });
          return;
        }
        return Promise.reject(err);
      },
    );
    this.http = ax;
  }
}

export const httpDummy = (jsonProp) => {
  const { protocol, host } = window.location;
  return new Ax().http({ url: `${protocol}//${host}/api/data/${jsonProp}` });
};

export default new Ax().http;
