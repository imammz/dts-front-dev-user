import axios from "axios";
import Configuration from "../config";
import NotificationHelper from "./helper/NotificationHelper";
import PortalHelper from "./helper/PortalHelper";

class Ax {
  constructor() {
    const ax = axios.create({
      baseURL: `https://dukapi.sdmdigital.id/`,
    });
    ax.interceptors.request.use(
      (config) => {
        let { headers } = config;
        // console.log(headers)
        const authToken = PortalHelper.tokenGetFromCookie();
        if (!authToken.includes("undefined")) {
          headers.common = { ...headers.common, Authorization: authToken };
          config.headers = headers;
        }
        return config;
      },
      (err) => {
        return err;
      },
    );
    ax.interceptors.response.use(
      (v) => {
        // console.log("call inter")
        return v;
      },
      (err) => {
        // console.log(err.response)
        let msg = err.response?.data?.message;
        if (msg == "Error.") {
          msg = err.response?.data?.result;
        }
        NotificationHelper.Failed({
          message: msg || err.toString(),
          okText: "Tutup",
        });
        return Promise.reject(err);
      },
    );
    this.httpDukcapil = ax;
  }
}

export const httpDummy = (jsonProp) => {
  const { protocol, host } = window.location;
  return new Ax().http({ url: `${protocol}//${host}/api/data/${jsonProp}` });
};

export default new Ax().httpDukcapil;
