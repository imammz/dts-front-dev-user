const apiUrl =
  process.env.NODE_ENV === "development"
    ? "https://dtsapi.dtsnet.net/" // development api
    : "https://dtsapi.dtsnet.net/"; // production api

const storageUrl = "https://back.dev.dtsnet.net";

/**
 * URL Storage - Pelatihan
 */

const Configuration = {
  apiUrl,
  storageUrl,
};

export default Configuration;

/**
 * DEL THIS SOON!
 */
