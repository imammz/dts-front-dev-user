import moment from "moment";

const TanggalUpdateDomisili = ({ tanggal, jumlahUpdate }) => {
  const d = moment(tanggal);
  return d.isValid()
    ? `diperbarui pada : ${d
        .locale("id")
        .format("D MMM YYYY, HH:mm")}; perubahan : ${jumlahUpdate}`
    : ``;
};

export default TanggalUpdateDomisili;
