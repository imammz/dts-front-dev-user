import React, { useEffect } from "react";
import Link from "next/link";
import { useRouter } from "next/router";

function Pagination({ pageSize, currentPage, onPageChange }) {
  const router = useRouter();
  const Pages = Array.from({ length: pageSize }, (_, i) => i + 1).slice(
    currentPage >= 3 ? parseInt(currentPage) - 1 : 0,
    parseInt(currentPage) <= 3 ||
      (parseInt(currentPage) > 3 && parseInt(currentPage) > pageSize - 2)
      ? parseInt(currentPage) + 1
      : parseInt(currentPage),
  );
  const PredPages = Array.from({ length: 2 }, (_, i) => i + 1);
  const LastPages = Array.from({ length: 2 }, (_, i) => i + pageSize - 1).slice(
    parseInt(currentPage) == pageSize - 2 && pageSize <= 4 ? 1 : 0,
  );

  return (
    <>
      <nav>
        <ul className="pagination">
          {currentPage > 1 && (
            <li className="page-item ms-0">
              <Link
                href={{
                  pathname: ``,
                  query: {
                    ...router.query,
                    page: 1,
                  },
                }}
                replace
              >
                <a className="page-link">
                  <i className="fas fa-angle-double-left"></i>
                </a>
              </Link>
            </li>
          )}
          <li className="page-item ms-0">
            {currentPage > 1 && (
              <Link
                href={{
                  pathname: ``,
                  query: {
                    ...router.query,
                    page: Number(currentPage) - 1,
                  },
                }}
                replace
              >
                <a className="page-link">
                  <i className="fas fa-angle-left"></i>
                </a>
              </Link>
            )}
          </li>

          {currentPage > 2 &&
            PredPages.map((page, idx) => (
              <li key={idx} className="page-item ms-0">
                <Link
                  href={{
                    pathname: ``,
                    query: {
                      ...router.query,
                      page: page,
                    },
                  }}
                  replace
                >
                  <a className="page-link">{page}</a>
                </Link>
              </li>
            ))}

          {currentPage == 4 && (
            <li className="page-item ms-0">
              <Link
                href={{
                  pathname: ``,
                  query: {
                    ...router.query,
                    page: 3,
                  },
                }}
                replace
              >
                <a className="page-link">3</a>
              </Link>
            </li>
          )}

          {currentPage > 4 && (
            <li className="page-item ms-0 link-disabled">
              <Link href="#">
                <a className="page-link">...</a>
              </Link>
            </li>
          )}

          {pageSize > 1 &&
            Pages.map((Page) => (
              <li
                key={Page}
                className={
                  Page == currentPage
                    ? "page-item active link-disabled"
                    : "page-item  ms-0"
                }
              >
                <Link
                  disabled
                  href={{
                    pathname: ``,
                    query: {
                      ...router.query,
                      page: Page,
                    },
                  }}
                  replace
                >
                  <a className="page-link">{Page}</a>
                </Link>
              </li>
            ))}

          {currentPage < pageSize - 3 && (
            <li className="page-item ms-0 link-disabled">
              <Link href="#">
                <a className="page-link">...</a>
              </Link>
            </li>
          )}

          {currentPage > 3 && currentPage == pageSize - 3 && (
            <li className="page-item ms-0">
              <Link
                href={{
                  pathname: ``,
                  query: {
                    ...router.query,
                    page: pageSize - 2,
                  },
                }}
                replace
              >
                <a className="page-link">{pageSize - 2}</a>
              </Link>
            </li>
          )}
          {currentPage < pageSize - 1 &&
            LastPages.map((page, idx) => (
              <li key={idx} className="page-item ms-0">
                <Link
                  href={{
                    pathname: ``,
                    query: {
                      ...router.query,
                      page: page,
                    },
                  }}
                  replace
                >
                  <a className="page-link">{page}</a>
                </Link>
              </li>
            ))}

          {currentPage < pageSize && (
            <li className="page-item ms-0">
              <Link
                href={{
                  pathname: ``,
                  query: {
                    ...router.query,
                    page: Number(currentPage) + 1,
                  },
                }}
                replace
              >
                <a className="page-link">
                  <i className="fas fa-angle-right"></i>
                </a>
              </Link>
            </li>
          )}

          {currentPage < pageSize && (
            <li className="page-item ms-0">
              {currentPage < pageSize && (
                <Link
                  href={{
                    pathname: ``,
                    query: {
                      ...router.query,
                      page: pageSize,
                    },
                  }}
                  replace
                >
                  <a className="page-link">
                    <i className="fas fa-angle-double-right"></i>
                  </a>
                </Link>
              )}
            </li>
          )}
        </ul>
      </nav>
    </>
  );
}

export default Pagination;
