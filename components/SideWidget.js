import React, { useEffect, useState } from "react";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

import PortalService from "../services/PortalService";

function SideWidget({ className, loading }) {
  const [sideWidgets, setSideWidgets] = useState(null);

  useEffect(async () => {
    try {
      const resp = await PortalService.widgetList();
      if (resp.data.success) {
        setSideWidgets(resp.data.result);
      } else throw Error(resp.message);
    } catch (err) {
      console.log(err);
    }
  }, []);

  return (
    <>
      {(loading || !sideWidgets) && (
        <div className={className}>
          <div className="col-lg-12 mb-5">
            <Skeleton href="#" className="h-200p">
              {/* <img src={sidebar01} className="img-responsive" width={'100%'}/> */}
            </Skeleton>
          </div>

          <div className="col-lg-12 mb-5">
            <Skeleton href="#" className="h-200p">
              {/* <img src="/assets/img/simonas-ads.jpg" className="img-responsive"/> */}
            </Skeleton>
          </div>
        </div>
      )}

      {!loading && sideWidgets && (
        <div className={className}>
          {sideWidgets.map((widget) => (
            <div className="col-lg-12 mb-5" key={widget.id}>
              <div className="card border rounded-5 px-5 pt-6 pb-6">
                <div className="d-inline-block rounded-circle mb-4">
                  <div
                    className="icon-circle"
                    style={{ backgroundColor: "#c2dffb", color: "#196ECD" }}
                  >
                    <i className={`${widget.icon} fa-2x`}></i>
                  </div>
                </div>
                <h5 className="fw-semi-bold mb-0">{widget.judul_widget}</h5>
                <div
                  className="mb-3 widget-container"
                  dangerouslySetInnerHTML={{ __html: widget.deskripsi }}
                ></div>
                <a
                  href={
                    widget.link.includes("http://") ||
                    widget.link.includes("https://")
                      ? widget.link
                      : `://${widget.link}`
                  }
                  target="blank"
                  className="btn btn-outline-primary btn-xs btn-pill mb-1"
                >
                  {widget.wording_button}
                </a>
              </div>
            </div>
          ))}
        </div>
      )}
    </>
  );
}

export default SideWidget;
