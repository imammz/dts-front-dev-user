import moment from "moment";
import { useState, useEffect } from "react";

export default function SimpleDateRange({ startDate, endDate, diff = 0 }) {
  const [rangeValid, setRangeValid] = useState(false);
  const [error, setError] = useState("");
  const [dateString, setDateString] = useState("");

  useEffect(() => {
    setRangeValid(false);
    setError("");
    // const
    let dt1 = moment(startDate).add(diff, "minutes").locale("id");
    let dt2 = moment(endDate).add(diff, "minutes").locale("id");

    if (dt1.isValid() && dt2.isValid()) {
      if (dt1.isSameOrBefore(dt2)) {
        setRangeValid(true);
        let f1 = "D MMMM YYYY";
        let f2 = "D MMMM YYYY";

        let dy = dt1.year() == dt2.year();
        let dm = dt1.month() == dt2.month();
        let dd = dt1.date() == dt2.date();

        if (dy) {
          f1 = f1.replace(" YYYY", "");
          if (dm) {
            f1 = f1.replace(" MMMM", "");
            if (dd) {
              setDateString(dt2.format(f2));
              return;
            }
          }
        }
        setDateString(dt1.format(f1) + " - " + dt2.format(f2));
      } else {
        setError("start date tidak <= end date");
      }
    } else {
      setError("terdapat tanggal yg tidak valid");
    }
  }, [startDate, endDate]);

  return rangeValid ? dateString : error;
}
