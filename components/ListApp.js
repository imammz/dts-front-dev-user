import React, { useEffect } from "react";
import Skeleton from "react-loading-skeleton";
import Link from "next/link";

function SSOInfo({ links = [], isLoading = false, isLoggedIn = false }) {
  return (
    <>
      <div className="container py-8">
        <div className="row align-items-end mb-md-7 mb-4">
          <div className="text-center mb-5">
            <h1 className="fw-bolder mb-1">Platform SDM Digital</h1>
            <h5 className="mb-0 text-muted">
              Hi Digiers, Sekarang akun-mu dapat digunakan untuk berbagai
              platform.
              <br />
              Nikmati berbagai kemudahan akses platform dalam satu akun.
            </h5>
          </div>
        </div>
        <div className="row">
          {isLoading ? (
            <>
              <div className="col-6 col-lg-3 mb-md-6 mb-4 px-2 px-md-4">
                <Skeleton height={207} />
              </div>
              <div className="col-6 col-lg-3 mb-md-6 mb-4 px-2 px-md-4">
                <Skeleton height={207} />
              </div>
              <div className="col-6 col-lg-3 mb-md-6 mb-4 px-2 px-md-4">
                <Skeleton height={207} />
              </div>
              <div className="col-6 col-lg-3 mb-md-6 mb-4 px-2 px-md-4">
                <Skeleton height={207} />
              </div>
              <div className="col-6 col-lg-3 mb-md-6 mb-4 px-2 px-md-4">
                <Skeleton height={207} />
              </div>
              <div className="col-6 col-lg-3 mb-md-6 mb-4 px-2 px-md-4">
                <Skeleton height={207} />
              </div>
              <div className="col-6 col-lg-3 mb-md-6 mb-4 px-2 px-md-4">
                <Skeleton height={207} />
              </div>
              <div className="col-6 col-lg-3 mb-md-6 mb-4 px-2 px-md-4">
                <Skeleton height={207} />
              </div>
            </>
          ) : (
            <>
              {links.map((link, i) => (
                <div
                  className="col-6 col-lg-3 mb-md-6 mb-4 px-2 px-md-4"
                  key={i}
                >
                  <Link href={isLoggedIn ? link.link_member : link.link_public}>
                    <a className="card icon-category border-1 rounded-5 p-md-5 p-3 text-center lift">
                      <div className="position-relative text-light">
                        <img
                          src={link.icon}
                          alt={link.nama_platform}
                          className="h-90p"
                        />
                      </div>

                      <div className="card-footer px-0 py-3">
                        <h5 className="mb-0 line-clamp-1">
                          {" "}
                          {link.nama_platform}
                        </h5>
                      </div>
                    </a>
                  </Link>
                </div>
              ))}
            </>
          )}
        </div>
      </div>
    </>
  );
}

export default SSOInfo;
