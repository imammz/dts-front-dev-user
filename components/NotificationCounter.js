import { useEffect } from "react";
import { useState } from "react";
import eventBus from "./EventBus";

export default function NotificationCounter({ withBg = false, number = 0 }) {
  const [count, setCount] = useState(0);
  useEffect(() => {
    if (!isNaN(number)) setCount(number);
  }, [number]);

  useEffect(() => {
    eventBus.on("setNotificationCounter", (v) => {
      setCount(v);
    });
  }, []);
  return count > 0 ? (
    withBg ? (
      <span className="notif-dot bg-red mt-n1 me-2">{count}</span>
    ) : (
      count
    )
  ) : (
    ""
  );
}
