// import { useEffect, useState } from "react"
const availabelLoading = ["border", "grow"];
export default function ButtonWithLoading({
  loading,
  className,
  loadingType = "border",
  disabled,
  children,
  ...other
}) {
  // const bProp = { ...props }
  // delete bProp["children"]

  // const loading = props?.loading || false
  // const className = props?.className || false
  // const disabled = props?.disabled || false
  let lt = loadingType;
  if (!availabelLoading.includes(loadingType)) {
    lt = availabelLoading[0];
  }

  return (
    <button
      {...other}
      className={`${className} ${loading ? "disabled" : ""}`}
      disabled={loading || disabled}
    >
      {loading && <i className={`spinner-${lt} spinner-${lt}-sm me-2`}></i>}
      {children}
    </button>
  );
}
