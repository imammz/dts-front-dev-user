import { useState } from "react";
import FlipMove from "react-flip-move";

export default function InputPassword({
  error = "",
  yupRegister = {},
  inputProps = {},
}) {
  const [showPassword, setShowPassword] = useState(false);

  return (
    <>
      <div className="input inner-addon right-addon mb-1">
        <span
          className="icon"
          onClick={() => {
            setShowPassword(!showPassword);
          }}
        >
          <FlipMove>
            {showPassword && <i className="fa fa-eye grey"></i>}
            {!showPassword && <i className="fa fa-eye-slash "></i>}
          </FlipMove>
        </span>
        <input
          placeholder="***********"
          autoComplete="off"
          type={showPassword ? "text" : "password"}
          className={`form-control form-control-sm ${
            error ? "is-invalid" : ""
          }`}
          {...inputProps}
          {...yupRegister}
        />
      </div>
      {error && (
        <div className="text-danger form-caption font-size-sm ">{error}</div>
      )}
    </>
  );
}
