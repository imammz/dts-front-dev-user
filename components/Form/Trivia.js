import { useEffect, useState } from "react";
import PortalHelper from "../../src/helper/PortalHelper";

export default function FormTrivia({
  trivia_question_bank_id,
  trivia_question_bank_detail_id,
  question,
  question_image,
  type,
  answer_key,
  answer,
  status,
  duration,
  value,
  nourut,
  no,
  onChange = () => {},
}) {
  const [optionList, setOptionList] = useState([]);

  useEffect(() => {
    try {
      if (answer) {
        // console.log(answer)
        const ops = JSON.parse(answer);
        setOptionList(ops);
      }
    } catch (error) {}
  }, [answer, nourut]);

  const onChangeValue = (e) => {
    // console.log(e.target.value)
    if (typeof onChange == "function") {
      onChange(e.target.value);
    }
  };

  return (
    <>
      <div className="mb-5">
        {question && <h4 className="mb-0">{question}</h4>}
        {/* {question_image} */}
        {PortalHelper.isBase64Data(question_image) && (
          <img src={question_image} alt="..." className="img-thumbnail my-5" />
        )}
      </div>
      <form>
        {optionList.map((opt, i) => {
          return (
            <div className="form-check" key={i}>
              <input
                className="form-check-input"
                onChange={onChangeValue}
                value={opt.key}
                type="radio"
                name={"soal-" + no}
                id={"opt-" + nourut + "-" + i}
                checked={opt.key == answer_key}
              />
              <label
                className="form-check-label mb-2 cursor-pointer"
                htmlFor={"opt-" + nourut + "-" + i}
              >
                {opt.option && (
                  <span className="mb-4">{`${opt.key}. ${opt.option}`}</span>
                )}
                <br />
                <br />
                {opt.image && PortalHelper.isBase64Data(opt.image) && (
                  <img
                    src={opt.image}
                    alt={"Gambar " + opt.key}
                    className="img-thumbnail mb-4"
                  />
                )}
              </label>
            </div>
          );
        })}
      </form>
    </>
  );
}
