import React, { useEffect } from "react";
import Link from "next/link";
import { useRouter } from "next/router";

function PrevNextagination({ handlePageChange, before, next }) {
  return (
    <>
      <nav>
        <ul className="pagination">
          <li className="page-item ms-0">
            {before && (
              <a
                href="#"
                className="page-link"
                onClick={() => handlePageChange(before)}
              >
                <i className="fas fa-arrow-left"></i>
              </a>
            )}
          </li>
          <li className="page-item">
            {next && (
              <a
                href="#"
                className="page-link"
                onClick={() => handlePageChange(next)}
              >
                <i className="fas fa-arrow-right"></i>
              </a>
            )}
          </li>
        </ul>
      </nav>
    </>
  );
}

export default PrevNextagination;
