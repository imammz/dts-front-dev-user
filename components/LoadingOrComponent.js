import { useEffect, useState } from "react";
import Skeleton from "react-loading-skeleton";

const LoadingOrComponent = ({
  isLoading = true,
  height = 10,
  Component = () => {},
  count = 1,
}) => {
  const [isReactComponent, setIsReactComponent] = useState(false);
  const [sHeight, setSHeight] = useState(10);

  useEffect(() => {
    if (!Number.isNaN(height)) {
      setSHeight(height);
    }
  }, [height]);

  useEffect(() => {
    if (typeof Component == "function") {
      setIsReactComponent(true);
    }
    if (!Number.isNaN(height)) {
      setSHeight(height);
    }
  }, []);

  return isLoading ? (
    <>
      <Skeleton height={height} count={count}>
        {" "}
      </Skeleton>{" "}
    </>
  ) : isReactComponent ? (
    <Component></Component>
  ) : (
    <>Bukan react componet</>
  );
};

export default LoadingOrComponent;
