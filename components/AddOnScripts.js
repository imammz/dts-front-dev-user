import { useState } from "react";
import { useEffect } from "react";
import HtmlParser from "react-html-parser";
import PortalService from "../services/PortalService";

const options = (key) => ({
  transform: function transform(node, index) {
    // console.log(node);
    if (node.type === "script") {
      // let urlSplitted = window.location.href.split("/");
      let id = "add-ons-" + key + "-" + index;
      let status = true;
      if (window?.document.getElementById(id) == null) {
        var script = document.createElement("script");
        script.onload = function () {
          //do stuff with the script
        };

        script.id = id;
        if (node?.attribs?.src) script.src = node?.attribs?.src;
        if (node?.attribs?.type) script.type = node?.attribs?.type;
        // console.log(script)
        if (node?.children?.length > 0) {
          let stx = "";
          node.children.forEach((el, i) => {
            stx += el.data;
            // console.log(el)
          });
          // console.log(stx)
          try {
            eval(stx);
          } catch (error) {
            status = false;
          }

          script.innerHTML = stx;
        }
        // console.log(script)
        script.async = true;
        if (status) window.document.head.appendChild(script);
      }
      // return <script key={index} src={node?.attribs?.src}>{node?.children[0]?.data}</script>;
      return "";
    }
  },
});

const AddOnScript = () => {
  const [listAdOn, setListAdOn] = useState([]);

  useEffect(() => {
    (async () => {
      const scripts = await PortalService.publikasiAOS();
      // console.log(scripts)
      if (scripts?.data?.result?.Data) {
        const list = scripts.data?.result?.Data;
        // console.log(list)
        setListAdOn(list);
      }
    })();
  }, []);

  return (
    <>
      {listAdOn.map((addons, key) => HtmlParser(addons.scripts, options(key)))}
    </>
  );
};

export default AddOnScript;
