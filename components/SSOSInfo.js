import React, { useEffect } from "react";
import Skeleton from "react-loading-skeleton";
import Link from "next/link";

function SSOInfo({
  background = "bg-secondary",
  links = [],
  isLoading = false,
  isLoggedIn = false,
  CustomFoot = null,
}) {
  return (
    <>
      <div className={`d-block rounded p-2 mb-6`}>
        <div className="pb-5 px-5 px-lg-3 px-xl-5">
          <div className="d-flex align-items-center">
            <div className="mb-4">
              <h4 className="fw-bolder mb-4">Single Sign On</h4>
              <p className="font-size-base mb-0 text-capitalize">
                Hi Digiers, Sekarang akun-mu dapat digunakan untuk berbagai
                platform. Nikmati berbagai kemudahan akses platform dalam satu
                akun.
              </p>
            </div>
          </div>
          <div className="row">
            {isLoading ? (
              <Skeleton className="h-70p mb-2" count={3}></Skeleton>
            ) : (
              <>
                {links.map((link, i) => (
                  <div className="col-12 mb-4 px-2" key={i}>
                    <a
                      href={isLoggedIn ? link.link_member : link.link_public}
                      target="__blank"
                    >
                      <div className="row align-items-center px-2">
                        <div className="col-auto">
                          <div className="avatar avatar-custom d-inline">
                            <img
                              src={link.icon}
                              alt={link.nama_platform}
                              className="avatar-img avatar-md img-fluid"
                            />
                          </div>
                        </div>

                        <div className="col px-3">
                          <div className="card-body p-0">
                            <h6 className="mb-0 fw-bolder line-clamp-1">
                              {link.nama_platform}
                            </h6>
                            <p className="mb-0 text-black fw-normal font-size-12 line-clamp-1">
                              {link.deskripsi}
                            </p>
                          </div>
                        </div>
                      </div>
                    </a>
                  </div>
                ))}
              </>
            )}
          </div>
        </div>
        {CustomFoot ? (
          <>
            <CustomFoot></CustomFoot>
          </>
        ) : (
          <></>
        )}
      </div>
    </>
  );
}

export default SSOInfo;
