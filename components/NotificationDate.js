import moment from "moment";
import { useEffect, useState } from "react";

export default function NotificationDate({ children }) {
  const [date, setDate] = useState("-");
  useEffect(() => {
    if (typeof children == "string") {
      const cur = moment();
      const d = moment(children);
      if (d.isValid()) {
        const dur = moment.duration(cur.diff(d));
        if (dur.days() >= 0 && dur.days() < 1) {
          setDate(`hari ini, ${d.format("HH:mm")}`);
        } else if (dur.days() >= 1 && dur.days() < 2) {
          setDate(`kemarin, ${d.format("HH:mm")}`);
        } else if (dur.years() == 0) {
          setDate(`${d.format("D MMM, HH:mm")}`);
        } else if (dur.years() > 0) {
          setDate(`${d.format("D/MM/YYYY, HH:mm")}`);
        }
      }
    }
  }, [children]);

  return date;
}
