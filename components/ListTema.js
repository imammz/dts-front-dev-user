import React, { useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import Skeleton from "react-loading-skeleton";

export default function ListTema({ data = null, loading, namaAkademi = null }) {
  const router = useRouter();
  const [skeletons, setSkeleton] = useState([...Array(6).keys()]);

  return (
    <>
      {loading && (
        <ul>
          <div className="row">
            {skeletons.map((index, idx) => (
              <li
                key={idx}
                className="nav-item col-lg-6 mb-4"
                role="presentation"
              >
                <div className="nav-link p-0">
                  <div className="card border p-2 lift rounded-5">
                    <div
                      href="#"
                      className="card icon-category icon-category-sm"
                    >
                      <div className="row align-items-center">
                        <div className="col">
                          <div className="card-body p-0 ms-5">
                            <Skeleton className="mb-0 line-clamp-1" />
                            <Skeleton className="mb-0 line-clamp-1" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
            ))}
          </div>
        </ul>
      )}
      {!loading && data && (
        <ul>
          <div className="row">
            {!data && (
              <div className="col-12 mx-auto text-center pt-8 pb-80">
                <h2 className="fw-bolder mb-0">Belum Ada Tema</h2>
                <p className="font-size-14 mb-6">
                  {namaAkademi && (
                    <>
                      Tema pada akademi{" "}
                      <span className="text-blue font-size-14 fw-bolder">
                        {namaAkademi}
                      </span>{" "}
                      belum tersedia
                    </>
                  )}
                  {!namaAkademi && <>Tema belum tersedia</>}
                </p>
                <img
                  src="/assets/img/empty-state/no-data.png"
                  alt="..."
                  className="img-fluid align-self-center"
                  style={{ width: "500px" }}
                />
              </div>
            )}
            {data &&
              data.map(
                (tema) =>
                  tema.id != null && (
                    <li
                      className="nav-item col-lg-6"
                      role="presentation"
                      key={tema.id}
                    >
                      <Link
                        href={{
                          pathname: `/akademi/[slug]`,
                          query: {
                            slug: tema.slug_akademi,
                            tema: tema.id,
                          },
                        }}
                      >
                        <a className="nav-link pt-0">
                          <div className="card border p-2 lift rounded-5">
                            <div className="row align-items-center row-cols-1">
                              <div className="col">
                                <div className="card-body p-0 mx-3">
                                  <h6
                                    className="fw-semi-bold mb-n1 me-3"
                                    style={{
                                      whiteSpace: "nowrap",
                                      textOverflow: "ellipsis",
                                      overflow: "hidden",
                                    }}
                                  >
                                    {tema.nama_tema}
                                  </h6>
                                  <span className="fw-semi-bold font-size-12 text-muted">
                                    {tema.nama_akademi}
                                    {/* {tema.jml_pelatihan} Pelatihan */}
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </Link>
                    </li>
                  ),
              )}
          </div>
        </ul>
      )}
    </>
  );
}
