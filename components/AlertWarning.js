import React, { useEffect, useState, useMemo } from "react";
import Skeleton from "react-loading-skeleton";
import Link from "next/link";
import EncryptionHelper from "../src/helper/EncryptionHelper";
import PortalHelper from "../src/helper/PortalHelper";
import eventBus from "./EventBus";
import { useRouter } from "next/router";

function AlertWarning({
  redirect,
  unverfiedMessage,
  onPhoneClick,
  onEmailClick,
}) {
  return (
    <>
      <div className="alert bg-red rounded-0 mb-0" role="alert">
        <div className="container">
          <div className="row text-white mb-md-0">
            <div className="row align-items-center">
              <div className="col-auto px-3">
                <i className="fa fa-exclamation-circle img-fluid fa-3x"></i>
              </div>
              <div className="col px-3">
                <h5 className="text-white mb-0">
                  Hai Digiers, untuk dapat melakukan pendaftaran pelatihan
                  silahakan lakukan verifikasi data berikut:
                  <br />{" "}
                  {unverfiedMessage.map((msg) => (
                    <>
                      {msg == "NIK" ? (
                        <Link
                          href={`/verify-user/nik?data=${EncryptionHelper.encrypt(
                            redirect,
                          )}`}
                        >
                          <a className="btn btn-sm btn-outline-light rounded-pill font-size-14 my-1 py-0 px-2">
                            <i className="fa fa-id-card me-1"></i>
                            Verifikasi {msg}
                          </a>
                        </Link>
                      ) : msg == "Nomor Handphone" ? (
                        <>
                          {unverfiedMessage.length > 1 && <span>{<br />}</span>}
                          <a
                            href="#"
                            className="btn btn-sm btn-outline-light rounded-pill font-size-14 my-1 py-0 px-2"
                            onClick={(e) => {
                              onPhoneClick(e);
                            }}
                          >
                            <i className="fa fa-phone-alt me-1"></i>
                            Verifikasi {msg}
                          </a>
                        </>
                      ) : (
                        <>
                          {unverfiedMessage.length > 1 && <span>{<br />}</span>}
                          <Link href={`/verify-user/complete-user-data`}>
                            <a className="btn btn-sm btn-outline-light rounded-pill font-size-14 my-1 py-0 px-2">
                              <i className="fa fa-id-card me-1"></i>
                              Lengkapi {msg}
                            </a>
                          </Link>
                        </>
                      )}
                    </>
                  ))}{" "}
                </h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export function AlertPelatihan({ redirect, unverfiedMessage, onPhoneClick }) {
  const [userData, setUserData] = useState({});
  const { asPath } = useRouter();
  // const [isPagePendaftaran] = useState((new RegExp('\/pelatihan\/[0-9]*\/daftar')).test(asPath))

  const isPagePendaftaran =
    new RegExp("/pelatihan/[0-9]*/daftar").test(asPath) ||
    new RegExp("/verify-user/complete-user-data").test(asPath);
  // useMemo(() => (new RegExp('\/pelatihan\/[0-9]*\/daftar')).test(asPath), [userData, asPath])

  const updateUserData = () => {
    const user = PortalHelper.getUserData();
    setUserData(user);
  };

  useEffect(() => {
    updateUserData();
    eventBus.on("profil-updated", () => {
      updateUserData();
    });
    return () => {
      eventBus.remove("profil-updated");
    };
  }, []);

  return userData?.wizard != null &&
    userData.lengkapi_pendaftaran?.length > 0 &&
    !isPagePendaftaran ? (
    <>
      <div className="alert bg-red rounded-0 mb-0" role="alert">
        <div className="container">
          <div className="row text-white mb-md-0">
            <div className="row align-items-center">
              <div className="col-auto px-3">
                <i className="fa fa-exclamation-circle img-fluid fa-3x"></i>
              </div>
              <div className="col px-3">
                <h5 className="text-white mb-0">
                  Hai Digiers, kamu memiliki pelatihan yang sedang aktif.
                  Silakan lengkapi formulir pendaftaran berikut
                  <br />
                  <Link
                    href={`/pelatihan/${userData.lengkapi_pendaftaran[0].pelatian_id}/daftar`}
                  >
                    <a className="btn btn-sm btn-outline-light rounded-pill font-size-14 my-1 py-0 px-2">
                      <i className="fa fa-file-contract me-1"></i>
                      Lengkapi Formulir Pendaftaran
                    </a>
                  </Link>
                  {/* {userData.handphone_verifikasi != true && (
                    <a
                      href="#"
                      className="btn btn-sm btn-outline-light rounded-pill font-size-14 my-1 py-0 px-2"
                      onClick={(e) => {
                        onPhoneClick(e);
                      }}
                    >
                      <i className="fa fa-phone-alt me-1"></i>
                      Verifikasi Nomor Handphone
                    </a>
                  )}
                  {userData.email_verifikasi != true && (
                    <a
                      href="#"
                      className="btn btn-sm btn-outline-light rounded-pill font-size-14 my-1 py-0 px-2"
                      onClick={(e) => {
                        onPhoneClick(e);
                      }}
                    >
                      <i className="fa fa-phone-alt me-1"></i>
                      Verifikasi Nomor Handphone
                    </a>
                  )} */}
                </h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  ) : (
    <></>
  );
}

export default AlertWarning;
