import React, { useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import Skeleton from "react-loading-skeleton";

// {
//     headerMessage:
//     headerStrongMessage:
//     showTotal:
//     items: [{
//         category,
//         title,
//         dateRange,
//         speaker: opt
//     }]
// }

function DaftarPelatihanView({ data = null, onItemClick }) {
  const router = useRouter();
  const [skeletons, setSkeleton] = useState([...Array(6).keys()]);

  return (
    <>
      {data &&
        data.items.map((item) => (
          <div className="col-lg mb-5" key={item.id}>
            <div className="card border shadow p-3 lift">
              <div className="row gx-0 row-cols-2">
                <div className="col-lg-2 p-3">
                  {item.url && (
                    <Link href={item.url}>
                      <a>
                        <img
                          className="img-fluid shadow-light-lg cover-img"
                          src={
                            item.gambar
                              ? item.gambar
                              : "/assets/img/microsoft.webp"
                          }
                          alt="Mitra"
                        />
                      </a>
                    </Link>
                  )}
                  {!item.url && (
                    <span>
                      <img
                        className="img-fluid shadow-light-lg cover-img"
                        src={
                          item.gambar
                            ? item.gambar
                            : "/assets/img/microsoft.webp"
                        }
                        alt="Mitra"
                      />
                    </span>
                  )}
                </div>

                {/* <div className="col-sm-12 col-md-9 col-lg-9 col-xl-9 d-md-block"> */}
                <div className="col-lg-8">
                  <div className="card-body p-2">
                    {item.category && (
                      <div className="mb-0">
                        <span className="text-dark font-size-sm fw-normal">
                          {item.category}
                        </span>
                      </div>
                    )}

                    {item.url && (
                      <Link href={item.url ? item.url : "#"}>
                        <a className="d-block mb-2">
                          <h6 className="line-clamp-2 mb-3">{item.title}</h6>
                        </a>
                      </Link>
                    )}
                    {!item.url && (
                      <span className="d-block mb-2">
                        <h6 className="line-clamp-2 mb-3">{item.title}</h6>
                      </span>
                    )}

                    <ul className="nav mx-n3 d-block d-md-flex">
                      <li className="nav-item px-3 mb-3 mb-md-0">
                        <div className="d-flex align-items-center">
                          <div className="me-2 d-flex text-secondary icon-uxs">
                            <i className="fa fa-calendar"></i>
                          </div>
                          <div className="font-size-sm">{item.dateRange}</div>
                        </div>
                      </li>
                      {item.location && (
                        <li className="nav-item px-3 mb-3 mb-md-0">
                          <div className="d-flex align-items-center">
                            <div className="me-2 d-flex text-secondary icon-uxs">
                              <i className="fa fa-map-marker-alt"></i>
                            </div>
                            <div className="font-size-sm">{item.location}</div>
                          </div>
                        </li>
                      )}
                      {item.speaker && (
                        <li className="nav-item px-3 mb-3 mb-md-0">
                          <div className="d-flex align-items-center">
                            <div className="me-2 d-flex text-secondary icon-uxs">
                              <i className="fa fa-microphone-alt"></i>
                            </div>
                            <div className="font-size-sm">{item.speaker}</div>
                          </div>
                        </li>
                      )}
                      {item.status !== null && (
                        <li className="nav-item px-3 mb-3 mb-md-0">
                          <div className="d-flex align-items-center">
                            {item.status == 1 && (
                              <div className="badge badge-sm bg-green badge-pill">
                                <span className="text-white font-size-sm fw-normal">
                                  Buka
                                </span>
                              </div>
                            )}
                            {item.status == 0 && (
                              <div className="badge badge-sm bg-red badge-pill">
                                <span className="text-white font-size-sm fw-normal">
                                  Tutup
                                </span>
                              </div>
                            )}
                            {item.status == 2 && (
                              <div className="badge badge-sm bg-red badge-pill">
                                <span className="text-white font-size-sm fw-normal">
                                  Batal
                                </span>
                              </div>
                            )}
                          </div>
                        </li>
                      )}
                    </ul>
                  </div>
                </div>
                <div className="col-lg-2 d-none pe-xl-2 d-lg-flex place-center">
                  {item.status == 1 && (
                    <Link href="#">
                      <a className="col-auto btn bg-blue rounded-pill font-size-14 text-white btn-xs mb-1">
                        DAFTAR SEKARANG
                      </a>
                    </Link>
                  )}
                </div>
              </div>
            </div>
          </div>
        ))}
    </>
  );
}

export default DaftarPelatihanView;
