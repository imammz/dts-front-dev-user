const ButtonPresensi = ({ status_peserta = "", id_pelatihan = 0 }) => {
  return `${status_peserta}`.toLowerCase() == "pelatihan" ? (
    <>
      <br className="d-block d-sm-none"></br>{" "}
      <a
        href={
          process.env.PRESENSI_BASE_URL +
          "pelatihan/kelola-sesi/5237" +
          id_pelatihan
        }
        className="btn bg-gradient-2 text-white rounded-pill btn-xs px-3 mb-3 font-size-15 me-2"
        target={"_blank"}
      >
        <i className="bi bi-person-check me-2"></i> Presensi
      </a>
    </>
  ) : (
    <></>
  );
};

export default ButtonPresensi;
