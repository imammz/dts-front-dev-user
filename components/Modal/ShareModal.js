import React, { useEffect, useRef, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import {
  FacebookShareButton,
  FacebookIcon,
  TwitterShareButton,
  TwitterIcon,
  EmailShareButton,
  EmailIcon,
} from "react-share";

export default function ShareModal({
  show,
  handleClose,
  title = "Bagikan kontent ini",
}) {
  const router = useRouter();
  const toggleOpenModal = useRef(null);
  const toggleCloseModal = useRef(null);
  const showSuccessModal = useRef(null);
  const [textCopied, setTextCopied] = useState(false);
  const [isWindowReady, setIsWindowReady] = useState(false);

  useEffect(() => {
    showSuccessModal.current.addEventListener("hidden.bs.modal", () => {
      handleClose();
    });
    if (show) {
      toggleOpenModal.current.click();
      setTextCopied(false);
    } else {
      toggleCloseModal.current.click();
    }
  }, [show]);

  useEffect(() => {
    if (window !== undefined) {
      setIsWindowReady(true);
    }
  }, []);

  return (
    <>
      <a
        href="#"
        ref={toggleOpenModal}
        data-bs-toggle="modal"
        data-bs-target="#successModal"
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseModal}
        data-bs-dismiss="modal"
        data-bs-target="#successModal"
        hidden
      ></a>

      <div
        className="modal fade"
        id="successModal"
        ref={showSuccessModal}
        tabIndex="-1"
        role="dialog"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header py-4">
              <div className="row align-items-end">
                <h4 className="fw-bolder fs-4 m-auto">{title}</h4>
              </div>
              <button
                type="button"
                className="close float-end text-primary"
                data-bs-dismiss="modal"
                aria-label="Close"
              >
                <i className="fa fa-times-circle text-muted"></i>
              </button>
            </div>
            <div className="modal-body py-5 border-top">
              {isWindowReady && (
                <div className="input-group flex-nowrap">
                  <input
                    type="text"
                    className="form-control py-2"
                    placeholder="URL page"
                    aria-describedby="button-copy"
                    value={
                      process.env.BASE_URL +
                      window?.location.pathname.replace(/\//, "")
                    }
                    readOnly
                  />
                  <button
                    className="btn btn-secondary py-2"
                    type="button"
                    id="button-copy"
                    onClick={() => {
                      setTextCopied(true);
                      navigator.clipboard.writeText(
                        process.env.BASE_URL +
                          window?.location.pathname.replace(/\//, ""),
                      );
                    }}
                  >
                    <span className="svg-icon svg-icon-primary svg-icon-2x">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24px"
                        height="24px"
                        viewBox="0 0 24 24"
                        version="1.1"
                      >
                        <g
                          stroke="none"
                          strokeWidth="1"
                          fill="none"
                          fillRule="evenodd"
                        >
                          <rect x="0" y="0" width="24" height="24" />
                          <path
                            d="M14,16 L12,16 L12,12.5 C12,11.6715729 11.3284271,11 10.5,11 C9.67157288,11 9,11.6715729 9,12.5 L9,17.5 C9,19.4329966 10.5670034,21 12.5,21 C14.4329966,21 16,19.4329966 16,17.5 L16,7.5 C16,5.56700338 14.4329966,4 12.5,4 L12,4 C10.3431458,4 9,5.34314575 9,7 L7,7 C7,4.23857625 9.23857625,2 12,2 L12.5,2 C15.5375661,2 18,4.46243388 18,7.5 L18,17.5 C18,20.5375661 15.5375661,23 12.5,23 C9.46243388,23 7,20.5375661 7,17.5 L7,12.5 C7,10.5670034 8.56700338,9 10.5,9 C12.4329966,9 14,10.5670034 14,12.5 L14,16 Z"
                            fill="#fff"
                            fillRule="nonzero"
                            transform="translate(12.500000, 12.500000) rotate(-315.000000) translate(-12.500000, -12.500000) "
                          />
                        </g>
                      </svg>
                    </span>
                  </button>
                </div>
              )}
              {textCopied && (
                <span style={{ color: "success" }}>URL telah tersalin!</span>
              )}
              {isWindowReady && (
                <div className="d-flex justify-content-center mt-4">
                  <FacebookShareButton
                    className="mx-1"
                    url={
                      process.env.BASE_URL +
                      window?.location.pathname.replace(/\//, "")
                    }
                    hashtag="#JagoanDigital #Digitalent #DTSKominfo"
                  >
                    <FacebookIcon size={40} round={true} />
                  </FacebookShareButton>
                  <TwitterShareButton
                    className="mx-1"
                    url={
                      process.env.BASE_URL +
                      window?.location.pathname.replace(/\//, "")
                    }
                    hashtags={["#JagoanDigital", "#Digitalent", "#DTSKominfo"]}
                  >
                    <TwitterIcon size={40} round={true} />
                  </TwitterShareButton>
                  <EmailShareButton
                    className="mx-1"
                    url={
                      process.env.BASE_URL +
                      window?.location.pathname.replace(/\//, "")
                    }
                  >
                    <EmailIcon size={40} round={true} />
                  </EmailShareButton>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
