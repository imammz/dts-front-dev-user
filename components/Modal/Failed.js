import React, { Children, useEffect, useRef, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import eventBus from "../EventBus";
import HtmlParser from "react-html-parser";

export const FailedNotification = () => {
  const toggleOpenModal = useRef(null);
  const toggleCloseModal = useRef(null);
  const showfailedModalNotificationFailed = useRef(null);

  const [message, setMessage] = useState("");
  const [show, setShow] = useState(false);
  const [timeout, setTimeOutModal] = useState(0);
  const [onClose, setOnClose] = useState(null);
  const [imgUrl, setImgUrl] = useState(
    "/assets/img/empty-state/field-warning.png",
  );
  const [okText, setOkText] = useState("TUTUP");

  useEffect(() => {
    eventBus.on("showModalNotificationFailed", (bool) => {
      if (bool != show) {
        setShow(bool);
      }
    });
    eventBus.on("showModalNotificationFailedMessage", (message) => {
      setMessage(message);
    });

    eventBus.on("showModalNotificationFailedTimeOut", (t) => {
      setTimeOutModal(t);
    });

    eventBus.on("showModalNotificationFailedOnClose", (obj) => {
      setOnClose(obj);
    });

    eventBus.on("showModalNotificationFailedImage", (url) => {
      setImgUrl(url);
    });

    eventBus.on("showModalNotificationFailedOkText", (text) => {
      setOkText(text);
    });
  }, []);

  useEffect(() => {
    showfailedModalNotificationFailed.current.addEventListener(
      "hidden.bs.modal",
      () => {
        handleClose();
      },
    );
    if (show) {
      toggleOpenModal.current.click();
      if (timeout != 0) {
        setTimeout(() => {
          toggleCloseModal.current.click();
        }, timeout * 1000);
      }
    } else {
      toggleCloseModal.current.click();
    }
  }, [show]);

  const resetState = () => {
    setImgUrl("/assets/img/empty-state/field-warning.png");
    setOkText("Tutup");
  };

  const handleClose = () => {
    if (typeof onClose?.func == "function") {
      onClose.func();
    }
    toggleCloseModal.current.click();
    if (show) {
      setShow(false);
    }
    resetState();
  };

  return (
    <>
      <a
        href="#"
        ref={toggleOpenModal}
        data-bs-toggle="modal"
        data-bs-target="#failedModalNotificationFailed"
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseModal}
        data-bs-dismiss="modal"
        data-bs-target="#failedModalNotificationFailed"
        hidden
      ></a>

      <div
        className="modal fade"
        id="failedModalNotificationFailed"
        ref={showfailedModalNotificationFailed}
        tabIndex="-1"
        role="dialog"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <button
                type="button"
                className="close float-end"
                data-bs-dismiss="modal"
                aria-label="Close"
                onClick={() => {
                  handleClose();
                }}
              >
                <span aria-hidden="true">
                  <i className="fa fa-times-circle text-muted fa-2x"></i>
                </span>
              </button>

              <h2
                className="fw-bold text-center mb-1 text-red"
                id="modalExampleTitle"
              >
                Ooppss
              </h2>

              <p className="font-size-16 fs-semi-bold text-center text-dark mb-0">
                {message}
              </p>
              <div className="mb-md-4 text-center">
                <img src={imgUrl} className="w-75 image-responsive" />
              </div>

              <div className="row">
                <div className="col-12">
                  <button
                    className="btn btn-block rounded-pill btn-primary font-size-14 mt-3 lift"
                    onClick={() => {
                      handleClose();
                    }}
                  >
                    TUTUP
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default function Failed({
  show,
  handleClose,
  children,
  useHtml = false,
}) {
  // const router = useRouter();
  const toggleOpenModal = useRef(null);
  const toggleCloseModal = useRef(null);
  const showFailModal = useRef(null);

  useEffect(() => {
    showFailModal.current.addEventListener("hidden.bs.modal", () => {
      handleClose();
    });
    if (show) {
      toggleOpenModal.current.click();
    } else {
      toggleCloseModal.current.click();
    }
  }, [show]);

  return (
    <>
      <a
        href="#"
        ref={toggleOpenModal}
        data-bs-toggle="modal"
        data-bs-target="#failedModal"
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseModal}
        data-bs-dismiss="modal"
        data-bs-target="#failedModal"
        hidden
      ></a>

      <div
        className="modal fade"
        id="failedModal"
        data-bs-keyboard="false"
        ref={showFailModal}
        tabIndex="-1"
        role="dialog"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-body">
              {/*<button type="button" className="close float-end" data-bs-dismiss="modal" aria-label="Close" onClick={() => {
                                handleClose();
                            }}>
                                <i className="fa fa-times-circle text-muted fa-1halfx"></i>
                            </button>*/}
              <h2
                className="fw-bold text-center mb-1 text-red"
                id="modalExampleTitle"
              >
                Oooppss
              </h2>

              <p className="font-size-16 fs-semi-bold text-center text-dark mb-0">
                {useHtml ? HtmlParser(children) : children}
                {/* {children} */}
              </p>
              <div className="mb-md-4 text-center">
                <img
                  src="/assets/img/empty-state/field-warning.png"
                  className="w-75 image-responsive"
                />
              </div>

              <div className="row">
                <div className="col-12">
                  <button
                    className="btn btn-block btn-blue rounded-pill font-size-14 mt-3 lift"
                    onClick={() => {
                      handleClose();
                    }}
                  >
                    TUTUP
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
