import React, { Children, useEffect, useRef } from "react";
import Link from "next/link";
import { useRouter } from "next/router";

export default function NeededLogin({ show, handleClose, children }) {
  const router = useRouter();
  const toggleOpenModal = useRef(null);
  const toggleCloseModal = useRef(null);
  const showFailModal = useRef(null);

  async function saveCurrentUrl() {
    const currentUrl = router.asPath;
    localStorage.setItem("targetAfterLogin", currentUrl);
    handleClose();
    router.push(`/login`);
  }

  useEffect(() => {
    showFailModal.current.addEventListener("hidden.bs.modal", () => {
      handleClose();
    });
    if (show) {
      toggleOpenModal.current.click();
    } else {
      toggleCloseModal.current.click();
    }
  }, [show]);

  return (
    <>
      <a
        href="#"
        ref={toggleOpenModal}
        data-bs-toggle="modal"
        data-bs-target="#failedModal"
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseModal}
        data-bs-dismiss="modal"
        data-bs-target="#failedModal"
        hidden
      ></a>

      <div
        className="modal fade"
        id="failedModal"
        ref={showFailModal}
        tabIndex="-1"
        role="dialog"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <button
                type="button"
                className="close float-end"
                data-bs-dismiss="modal"
                aria-label="Close"
                onClick={() => {
                  handleClose();
                }}
              >
                <span aria-hidden="true">
                  <i className="fa fa-times-circle text-muted fa-2x"></i>
                </span>
              </button>

              <h2
                className="fw-bold text-center mb-1 text-red"
                id="modalExampleTitle"
              >
                Ooppss
              </h2>

              <p className="font-size-16 fs-semi-bold text-center text-dark mb-0">
                {children}
              </p>

              <div className="mb-md-4 text-center">
                <img
                  src="/assets/img/empty-state/login.png"
                  className="w-50 image-responsive"
                />
              </div>

              <div className="row">
                <div className="col-12">
                  <button
                    type="button"
                    className="btn btn-block rounded-pill btn-primary mt-3 lift"
                    onClick={saveCurrentUrl}
                  >
                    KLIK DISINI UNTUK LOGIN
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
