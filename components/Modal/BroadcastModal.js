import React, { useEffect, useRef, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import ProfilService from "../../services/ProfileService";
import routeSegment from "../../src/route-segment";
import PortalHelper from "../../src/helper/PortalHelper";
import { auto } from "@popperjs/core";

export default function BroadcastModal() {
  const router = useRouter();
  const toggleOpenModal = useRef(null);
  const toggleCloseModal = useRef(null);
  const showBroadcastModalRef = useRef(null);

  const [showBroadcastModal, setShowBroadcastModal] = useState(false);
  const [broadcasts, setBroadcasts] = useState(null);

  useEffect(async () => {
    let resp = null;
    if (
      router.pathname != routeSegment.HOME &&
      !router.pathname.includes(routeSegment.USER_PROFIL)
    ) {
      return;
    }
    resp = await ProfilService.getBroadcast(
      router.pathname != routeSegment.HOME,
    );
    if (resp.data.result.Status && resp.data.result.Data[0] != null) {
      let bcs = resp.data.result.Data;
      setBroadcasts(bcs);
      let viewedBroadcast =
        JSON.parse(
          sessionStorage.getItem(ProfilService.VIEWED_BROADCAST_KEY),
        ) ?? [];
      if (
        bcs.length > 0 &&
        viewedBroadcast.length > 0 &&
        bcs.length != viewedBroadcast.length
      ) {
        let newBrod = false;
        bcs.forEach((elem) => {
          const searchbc = viewedBroadcast.filter((val) => elem.id == val.id);
          // jika ketemu skip
          if (searchbc.length == 0) {
            newBrod = true;
          }
        });
        setShowBroadcastModal(newBrod);
        if (newBrod) {
          toggleOpenModal.current.click();
        }
      } else {
        setShowBroadcastModal(true);
        toggleOpenModal.current.click();
      }
    }
  }, [router.pathname]);

  useEffect(() => {
    showBroadcastModalRef.current.addEventListener("hidden.bs.modal", () => {
      let viewedBcs =
        JSON.parse(
          sessionStorage.getItem(ProfilService.VIEWED_BROADCAST_KEY),
        ) ?? [];
      viewedBcs = broadcasts != null ? viewedBcs.concat(broadcasts) : viewedBcs;
      sessionStorage.setItem(
        ProfilService.VIEWED_BROADCAST_KEY,
        JSON.stringify(viewedBcs),
      );
    });
    // if (showBroadcastModal) {
    //   toggleOpenModal.current.click();
    // } else {
    //   toggleCloseModal.current.click();
    // }
  }, [showBroadcastModal]);

  return (
    <>
      <a
        href="#"
        ref={toggleOpenModal}
        data-bs-toggle="modal"
        data-bs-target="#broadcastModal"
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseModal}
        data-bs-dismiss="modal"
        data-bs-target="#broadcastModal"
        hidden
      ></a>

      <div
        className="modal fade"
        id="broadcastModal"
        ref={showBroadcastModalRef}
        tabIndex="-1"
        role="dialog"
      >
        <div
          className="modal-dialog modal-dialog-centered modal-lg"
          role="document"
        >
          <div className="modal-content modal-lg">
            {/* <div className="modal-header p-0 border-0"> */}
            <button
              type="button"
              className="close-broadcast"
              data-bs-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">
                {/* <i className="fa fa-times-circle text-muted fa-4x"></i> */}
                <i className="far fa-times-circle text-red fa-3x"></i>
              </span>
            </button>
            {/* </div> */}
            <div className="modal-body p-0">
              <div className="rounded" style={{ overflow: "hidden" }}>
                <div className="row">
                  <div className="col-12">
                    <div
                      id="bc-hero"
                      className="carousel slide bc-carousel"
                      data-bs-ride="carousel"
                      data-bs-pause="hover"
                      data-bs-interval="5000"
                    >
                      <div className="carousel-inner rounded-0">
                        {broadcasts?.map((broadcast, index) => (
                          <div
                            key={index}
                            className={`carousel-item 
                              ${index == 0 ? "active" : ""} ${
                                broadcast.jenis_content == "Video"
                                  ? " h-100"
                                  : ""
                              }
                            `}
                          >
                            {broadcast.jenis_content == "Image" && (
                              <img
                                src={PortalHelper.urlBroadcastImage(
                                  broadcast.gambar,
                                )}
                                // alt={broadcast.judul_imagetron}
                                className="d-block"
                                style={{ width: "100%", objectFit: "contain" }}
                              />
                            )}

                            {broadcast.jenis_content == "Video" && (
                              <iframe
                                src={broadcast.url}
                                frameBorder="0"
                                allow="autoplay; encrypted-media"
                                allowFullScreen
                                width="100%"
                                height="100%"
                                title="video"
                                className="video-item"
                              />
                            )}

                            {broadcast.jenis_content == "Rich Content" && (
                              <div
                                className="container bc-rich-content w-100 p-5"
                                dangerouslySetInnerHTML={{
                                  __html: broadcast.isi,
                                }}
                              ></div>
                            )}
                          </div>
                        ))}
                      </div>
                      <button
                        className="carousel-control-prev"
                        type="button"
                        data-bs-target="#bc-hero"
                        data-bs-slide="prev"
                      >
                        <span
                          className="carousel-control-prev-icon"
                          aria-hidden="true"
                        ></span>
                        <span className="visually-hidden">Previous</span>
                      </button>
                      <button
                        className="carousel-control-next text-white"
                        type="button"
                        data-bs-target="#bc-hero"
                        data-bs-slide="next"
                      >
                        <span
                          className="carousel-control-next-icon"
                          aria-hidden="true"
                        ></span>
                        <span className="visually-hidden">Next</span>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
