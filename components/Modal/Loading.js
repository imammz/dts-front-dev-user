import React, { useEffect, useRef, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import Lottie, { useLottie } from "lottie-react";
import * as animation from "./../../public/assets/json/loading-animation.json";

export default function Loading({
  show,
  handleClose,
  children,
  backdropOnly = false,
}) {
  const router = useRouter();
  const toggleOpenLoadignModal = useRef(null);
  const toggleCloseLoadingModal = useRef(null);
  const showLoadingModal = useRef(null);

  const options = {
    animationData: animation,
    loop: true,
  };
  const lottie = useLottie(options);
  lottie.setSpeed(2);
  const { View } = lottie;

  useEffect(() => {
    console.log("handle show loading: ");
    // console.log(show);
    showLoadingModal.current?.addEventListener("hidden.bs.modal", () => {
      handleClose();
    });
    if (show) {
      toggleOpenLoadignModal.current?.click();
    } else {
      // console.log("handle show loading: ", show);
      toggleCloseLoadingModal.current.click();
      // showLoadingModal.current.addEventListener('shown.bs.modal', () => {
      //     if (!show) {
      //     }
      // })
    }
  }, [show]);

  return (
    <>
      <a
        href="#"
        ref={toggleOpenLoadignModal}
        data-bs-toggle="modal"
        data-bs-target="#loadingModal"
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseLoadingModal}
        data-bs-dismiss="modal"
        data-bs-target="#loadingModal"
        hidden
      ></a>

      <div
        className="modal"
        id="loadingModal"
        ref={showLoadingModal}
        data-bs-backdrop="static"
        data-bs-keyboard="false"
        role="dialog"
      >
        {!backdropOnly && (
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">
              <div className="modal-body" style={{ textAlign: "center" }}>
                <div className="w-100 m-0">
                  {/* {View} */}
                  <img
                    src="/assets/img/GIF-DTS.gif"
                    alt="loading gif"
                    className="w-50"
                  />

                  <h2
                    className="fw-bold text-center mb-1 text-primary"
                    id="title"
                  >
                    Loading...
                  </h2>
                </div>
              </div>
            </div>
          </div>
        )}
        {backdropOnly && (
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-body" style={{ background: "transparent" }}>
              <div className="w-100 m-0" style={{ textAlign: "center" }}>
                {/* {View} */}
                <img
                  src="/assets/img/GIF-DTS.gif"
                  alt="loading gif"
                  className="w-50"
                />
                <h2 className="fw-bold text-center mb-1 text-white " id="title">
                  Loading...
                </h2>
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  );
}
