import React, { useEffect, useRef, useState } from "react";
import eventBus from "../EventBus";

export const SuccessNotification = () => {
  const toggleOpenModal = useRef(null);
  const toggleCloseModal = useRef(null);
  const showsuccessModalNotificationSuccess = useRef(null);

  const [message, setMessage] = useState("");
  const [show, setShow] = useState(false);
  const [timeout, setTimeOutModal] = useState(0);
  const [onClose, setOnClose] = useState(null);
  const [imgUrl, setImgUrl] = useState(
    "/assets/img/empty-state/success-acc.png",
  );
  const [okText, setOkText] = useState("Ok");
  const [title, setTitle] = useState("SUKSES");

  useEffect(() => {
    eventBus.on("showModalNotificationSuccess", (bool) => {
      if (bool != show) {
        setShow(bool);
      }
    });
    eventBus.on("showModalNotificationSuccessMessage", (message) => {
      setMessage(message);
    });

    eventBus.on("showModalNotificationSuccessTitle", (message) => {
      setTitle(message);
    });

    eventBus.on("showModalNotificationSuccessTimeOut", (t) => {
      setTimeOutModal(t);
    });

    eventBus.on("showModalNotificationSuccessOnClose", (obj) => {
      setOnClose(obj);
    });

    eventBus.on("showModalNotificationSuccessImage", (url) => {
      setImgUrl(url);
    });

    eventBus.on("showModalNotificationSuccessOkText", (text) => {
      setOkText(text);
    });
  }, []);

  useEffect(() => {
    showsuccessModalNotificationSuccess.current.addEventListener(
      "hidden.bs.modal",
      () => {
        handleClose();
      },
    );
    if (show) {
      toggleOpenModal.current.click();
      if (timeout != 0) {
        setTimeout(() => {
          toggleCloseModal.current.click();
        }, timeout * 1000);
      }
    } else {
      toggleCloseModal.current.click();
    }
  }, [show]);

  const resetState = () => {
    setImgUrl("/assets/img/empty-state/success-acc.png");
    setOkText("Ok");
  };

  const handleClose = () => {
    if (typeof onClose?.func == "function") {
      onClose.func();
    }
    toggleCloseModal.current.click();
    if (show) {
      setShow(false);
    }
    resetState();
  };

  return (
    <>
      <a
        href="#"
        ref={toggleOpenModal}
        data-bs-toggle="modal"
        data-bs-target="#successModalNotificationSuccess"
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseModal}
        data-bs-dismiss="modal"
        data-bs-target="#successModalNotificationSuccess"
        hidden
      ></a>

      <div
        className="modal fade"
        id="successModalNotificationSuccess"
        ref={showsuccessModalNotificationSuccess}
        tabIndex="-1"
        role="dialog"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <button
                type="button"
                className="close float-end"
                data-bs-dismiss="modal"
                aria-label="Close"
                onClick={() => {
                  handleClose();
                }}
              >
                <span aria-hidden="true">
                  <i className="fa fa-times-circle text-muted fa-2x"></i>
                </span>
              </button>

              <h2
                className="fw-bold text-center mb-1 text-green"
                id="modalExampleTitle"
              >
                {title}
              </h2>

              <p className="font-size-16 text-center text-muted mb-0">
                {message}
              </p>
              <div className="mb-md-4 text-center">
                <img src={imgUrl} className="w-50 image-responsive" />
              </div>

              <div className="row">
                <div className="col-12">
                  <button
                    className="btn btn-block rounded-pill font-size-14 btn-primary mt-3 lift"
                    onClick={() => {
                      handleClose();
                    }}
                  >
                    TUTUP
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default function Success({ show, handleClose, children }) {
  // const router = useRouter();
  const toggleOpenModal = useRef(null);
  const toggleCloseModal = useRef(null);
  const showSuccessModal = useRef(null);

  useEffect(() => {
    showSuccessModal.current.addEventListener("hidden.bs.modal", () => {
      handleClose();
    });
    if (show) {
      toggleOpenModal.current.click();
    } else {
      toggleCloseModal.current.click();
    }
  }, [show]);

  return (
    <>
      <a
        href="#"
        ref={toggleOpenModal}
        data-bs-toggle="modal"
        data-bs-target="#successModal"
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseModal}
        data-bs-dismiss="modal"
        data-bs-target="#successModal"
        hidden
      ></a>

      <div
        className="modal fade"
        id="successModal"
        ref={showSuccessModal}
        tabIndex="-1"
        role="dialog"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <button
                type="button"
                className="close float-end"
                data-bs-dismiss="modal"
                aria-label="Close"
                onClick={() => {
                  handleClose();
                }}
              >
                <span aria-hidden="true">
                  <i className="fa fa-times-circle text-muted fa-2x"></i>
                </span>
              </button>

              <p className="font-size-16 text-center text-dark mb-0">
                {children}
              </p>
              <div className="mb-md-4 text-center">
                <img
                  src="/assets/img/empty-state/success-acc.png"
                  className="w-50 image-responsive"
                />
              </div>

              <div className="row">
                <div className="col-12">
                  <button
                    className="btn btn-block btn-primary rounded-pill font-size-14 mt-3 lift"
                    onClick={() => {
                      handleClose();
                    }}
                  >
                    TUTUP
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
