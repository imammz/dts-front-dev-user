import React, { useEffect, useRef, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import * as animation from "../../public/assets/json/survey-lottie.json";
import Lottie, { useLottie } from "lottie-react";
import eventBus from "../EventBus";

export default function SurveyMandatoryModalDownloadSertifikat({
  // show,
  // handleClose,
  // surveys = [],
  body = "Hai Digiers, sebelum mengunduh sertifikat kamu diwajibkan untuk mengisi survei evaluasi berikut.",
  onClose = () => {},
}) {
  const [show, setShow] = useState(false);
  const [surveys, setSurveys] = useState([]);
  const [handleClose, setOnclose] = useState(() => {});

  const router = useRouter();
  const toggleOpenModal = useRef(null);
  const toggleCloseModal = useRef(null);
  const showSuccessModal = useRef(null);

  const options = {
    animationData: animation,
    loop: true,
  };

  useEffect(() => {
    const fnShow = (option) => {
      setShow((v) => option?.show || v);
      setSurveys((v) => option?.surveys || v);
      setOnclose((v) => option?.onClose || v);
    };
    const fnClear = () => {
      setShow(false);
      setSurveys([]);
      setOnclose(() => {});
    };

    const fnHide = () => {
      setShow((v) => false);
      onClose();
    };

    eventBus.on("showMandatoryDS", fnShow);
    eventBus.on("clearMandatoryDS", fnClear);
    eventBus.on("hideMandatoryDS", fnHide);

    return () => {
      eventBus.remove("showMandatoryDS", fnShow);
      eventBus.remove("clearMandatoryDS", fnClear);
      eventBus.remove("hideMandatoryDS", fnHide);
    };
  }, []);

  const lottie = useLottie(options);
  lottie.setSpeed(2);
  const { View } = lottie;

  useEffect(() => {
    // console.log("show",show)
    showSuccessModal.current.addEventListener("hidden.bs.modal", () => {
      eventBus.dispatch("hideMandatoryDS");
      if (typeof handleClose == "function") {
        fnHide();
      }
    });
    if (show) {
      toggleOpenModal.current.click();
    } else {
      toggleCloseModal.current.click();
    }
  }, [show]);

  useEffect(() => {
    if (surveys.length == 0) {
      toggleCloseModal.current.click();
    }
  }, [surveys]);

  return (
    <>
      <a
        href="#"
        ref={toggleOpenModal}
        data-bs-toggle="modal"
        data-bs-target="#surveyModalDS"
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseModal}
        data-bs-dismiss="modal"
        data-bs-target="#surveyModalDS"
        hidden
      ></a>

      <div
        className="modal fade"
        id="surveyModalDS"
        ref={showSuccessModal}
        data-bs-keyboard="false"
        // data-bs-backdrop="static"
        tabIndex="-1"
        role="dialog"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header py-4">
              <div className="row align-items-end">
                <h4 className="fw-bolder fs-4 m-auto">Perhatian!</h4>
              </div>
            </div>
            <div className="modal-body pt-5 border-top">
              <div className="input-group flex-nowrap">{View}</div>
              <p className="font-size-16 mb-2 text-center">{body}</p>

              <ul className="nav row" id="pills-tab" role="tablist">
                {surveys &&
                  surveys.map((survey) => (
                    <li
                      className="nav-item col-12 my-2"
                      role="presentation"
                      key={survey.id}
                    >
                      <Link
                        href={`/auth/${survey.type}/${survey.pelatian_id}/${survey.question_bank_id}`}
                      >
                        <a
                          className="nav-link p-0"
                          onClick={(e) => {
                            // handleClose();
                            toggleCloseModal.current.click();
                          }}
                        >
                          <div className="card border p-3 lift rounded-pill">
                            <div
                              href="#"
                              className="card icon-category icon-category-sm"
                            >
                              <div className="row align-items-center row-cols-2">
                                <div className="col-12 px-4 line-clamp-1">
                                  <h6 className="fw-semi-bold mb-0">
                                    {survey?.nama} (
                                    {survey.type == "survey"
                                      ? "Survei"
                                      : "Test Substansi"}
                                    )
                                  </h6>
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </Link>
                    </li>
                  ))}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
