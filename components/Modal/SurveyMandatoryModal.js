import React, { useEffect, useRef, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import * as animation from "./../../public/assets/json/survey-lottie.json";
import Lottie, { useLottie } from "lottie-react";

export default function SurveyMandatoryModal({
  show,
  handleClose,
  surveys = [],
}) {
  const router = useRouter();
  const toggleOpenModal = useRef(null);
  const toggleCloseModal = useRef(null);
  const showSuccessModal = useRef(null);
  const options = {
    animationData: animation,
    loop: true,
  };
  const lottie = useLottie(options);
  lottie.setSpeed(2);
  const { View } = lottie;

  useEffect(() => {
    showSuccessModal.current.addEventListener("hidden.bs.modal", () => {
      handleClose();
    });
    if (show) {
      toggleOpenModal.current.click();
    } else {
      toggleCloseModal.current.click();
    }
  }, [show]);

  useEffect(() => {
    if (surveys.length == 0) {
      toggleCloseModal.current.click();
    }
  }, [surveys]);

  return (
    <>
      <a
        href="#"
        ref={toggleOpenModal}
        data-bs-toggle="modal"
        data-bs-target="#surveyModal"
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseModal}
        data-bs-dismiss="modal"
        data-bs-target="#surveyModal"
        hidden
      ></a>

      <div
        className="modal fade"
        id="surveyModal"
        ref={showSuccessModal}
        data-bs-keyboard="false"
        // data-bs-backdrop="static"
        tabIndex="-1"
        role="dialog"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header py-4">
              <div className="row align-items-end">
                <h4 className="fw-bolder fs-4 m-auto">Perhatian!</h4>
              </div>
            </div>
            <div className="modal-body pt-5 border-top">
              <div className="input-group flex-nowrap">{View}</div>
              <p className="font-size-16 mb-2 text-center">
                Hai Digiers, yuk luangkan waktumu sebentar untuk mengerjakan
                assignment berikut
              </p>

              <ul className="nav row" id="pills-tab" role="tablist">
                {surveys &&
                  surveys.map((survey) => (
                    <li
                      className="nav-item col-12 my-2"
                      role="presentation"
                      key={survey.id}
                    >
                      <Link
                        href={`/auth/${survey.type}/${survey.pelatian_id}/${survey.question_bank_id}`}
                      >
                        <a
                          className="nav-link p-0"
                          onClick={(e) => {
                            // handleClose();
                            toggleCloseModal.current.click();
                          }}
                        >
                          <div className="card border p-3 lift rounded-pill">
                            <div
                              href="#"
                              className="card icon-category icon-category-sm"
                            >
                              <div className="row align-items-center row-cols-2">
                                <div className="col-12 px-4 line-clamp-1">
                                  <h6 className="fw-semi-bold mb-0">
                                    {survey?.nama} (
                                    {survey.type == "survey"
                                      ? "Survei"
                                      : "Test Substansi"}
                                    )
                                  </h6>
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </Link>
                    </li>
                  ))}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
