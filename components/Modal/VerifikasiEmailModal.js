import React, { useEffect, useRef, useState } from "react";
import ValidationMessage from "../../src/validation-message";
import BeatLoader from "react-spinners/BeatLoader";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import moment from "moment";

export default function VerifikasiEmail({
  show,
  handleClose,
  children,
  onVerify,
  onResend,
  errMessage = "",
}) {
  const toggleOpenModal = useRef(null);
  const toggleCloseModal = useRef(null);
  const showsuccessModalNotificationSuccess = useRef(null);

  const [timestampOtp, settimestampOtp] = useState(
    moment().format("DD/MM/YYYY, HH:mm:ss"),
  );
  const [timeout, setTimeOutModal] = useState(0);
  const submitButton = useRef(null);
  const [duration, setDuration] = useState("");
  const [timeInterval, setTimeInterval] = useState(null);
  const [isResendEmail, setIsResendEmail] = useState(false);
  const [imgUrl, setImgUrl] = useState(
    "/assets/img/illustrations/file-mail.png",
  );
  const [loading, setLoading] = useState(false);

  const validationSchema = Yup.object({
    "digit-1": Yup.string().required(ValidationMessage.otp),
    "digit-2": Yup.string().required(ValidationMessage.otp),
    "digit-3": Yup.string().required(ValidationMessage.otp),
    "digit-4": Yup.string().required(ValidationMessage.otp),
    "digit-5": Yup.string().required(ValidationMessage.otp),
    "digit-6": Yup.string().required(ValidationMessage.otp),
  });
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    mode: "onBlur",
    resolver: yupResolver(validationSchema),
  });

  useEffect(() => {
    if (timeInterval) {
      clearInterval(timeInterval);
    }
  }, [show]);

  const stopCountDown = () => {
    clearInterval(timeInterval);
  };

  const countDown = (isReset = false) => {
    if (isReset) {
      setDuration("");
      return;
    }
    setIsResendEmail(false);
    const upperBounddate = moment().add(5, "minutes");
    setDuration(
      moment.utc(moment(upperBounddate).diff(moment())).format("mm:ss"),
    );
    const localInterval = setInterval(() => {
      duration = moment.duration(
        moment(upperBounddate).diff(moment()),
        "milliseconds",
      );
      setDuration(moment.utc(duration.asMilliseconds()).format("mm:ss"));
      if (duration < 0) {
        setIsResendEmail(true);
        clearInterval(localInterval);
        return;
      }
    }, 1000);
    setTimeInterval(localInterval);
  };

  useEffect(() => {
    showsuccessModalNotificationSuccess.current.addEventListener(
      "hidden.bs.modal",
      () => {
        stopCountDown();
        handleClose();
      },
    );
    if (show) {
      reset();
      toggleOpenModal.current.click();
      countDown();
      if (timeout != 0) {
        setTimeout(() => {
          toggleCloseModal.current.click();
        }, timeout * 1000);
      }
    } else {
      toggleCloseModal.current.click();
      countDown(true);
    }
  }, [show]);

  const changeElementfocus = (idElement, e) => {
    if (e.code.includes("Digit") || e.code.includes("Key")) {
      document.getElementById(idElement).focus();
    }
    return;
  };

  async function onSubmit(data, event) {
    let pin = "";
    Object.keys(data).forEach((key) => {
      pin += data[key];
      delete data[key];
    });
    // const userData = PortalHelper.getUserData();
    // userdata == null -> lupa password
    // data["email"] = userData?.email;

    data["pin"] = pin;
    onVerify(data);
  }

  return (
    <>
      <a
        href="#"
        ref={toggleOpenModal}
        data-bs-toggle="modal"
        data-bs-target="#verifikasiEmailModal"
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseModal}
        data-bs-dismiss="modal"
        data-bs-target="#verifikasiEmailModal"
        hidden
      ></a>

      <div
        className="modal fade"
        id="verifikasiEmailModal"
        ref={showsuccessModalNotificationSuccess}
        data-bs-backdrop="static"
        data-bs-keyboard="false"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="staticBackdropLabel"
        aria-hidden="true"
      >
        <div
          className="modal-lg modal-dialog modal-dialog-centered modal-fullscreen-sm-down"
          role="document"
        >
          <div className="modal-content">
            <div className="modal-body">
              <form
                onSubmit={handleSubmit(onSubmit)}
                className="digit-group"
                data-group-name="digits"
                autoComplete="off"
              >
                <button
                  type="button"
                  className="close float-end"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                  onClick={() => {
                    handleClose();
                  }}
                >
                  <span aria-hidden="true">
                    <i className="fa fa-times-circle text-muted fa-2x"></i>
                  </span>
                </button>

                <h2
                  className="fw-bold text-center mb-6 text-primary"
                  id="modalExampleTitle"
                >
                  Verifikasi Email
                </h2>

                <div className="row">
                  <div className="col-lg-5">
                    <img
                      src="/assets/img/empty-state/otp-email.png"
                      className="img-responsive"
                    />
                  </div>
                  <div className="col-lg-7">
                    <div className="d-block rounded bg-light mb-6">
                      <div className="pt-6 pb-5 px-5 px-lg-3 px-xl-5">
                        <div className="d-flex align-items-center">
                          <div className="mb-4">
                            <p className="mb-0 text-capitalize">
                              Silahkan masukkan kode OTP yang dikirim ke
                              Email/Inbox mu. Jangan bagikan kode OTP kamu
                              kepada siapapun.<br></br>
                              <small>
                                * jika tidak masuk silakan periksa{" "}
                                <strong>folder spam</strong>
                              </small>
                            </p>
                          </div>
                        </div>
                        <div id="otp" className="my-3">
                          <div
                            className="digit-group"
                            data-group-name="digits"
                            autoComplete="off"
                          >
                            <input
                              type="text"
                              onKeyUp={(e) => {
                                changeElementfocus("digit-2", e);
                              }}
                              className="otp-input text-center m-1"
                              id="digit-1"
                              name="digit-1"
                              {...register("digit-1")}
                              maxLength="1"
                              disabled={loading}
                            />
                            <input
                              type="text"
                              onKeyUp={(e) => {
                                changeElementfocus("digit-3", e);
                              }}
                              className="otp-input text-center m-1"
                              id="digit-2"
                              name="digit-2"
                              {...register("digit-2")}
                              maxLength="1"
                              disabled={loading}
                            />
                            <input
                              type="text"
                              onKeyUp={(e) => {
                                changeElementfocus("digit-4", e);
                              }}
                              className="otp-input text-center m-1"
                              id="digit-3"
                              name="digit-3"
                              {...register("digit-3")}
                              maxLength="1"
                              disabled={loading}
                            />
                            <input
                              type="text"
                              onKeyUp={(e) => {
                                changeElementfocus("digit-5", e);
                              }}
                              className="otp-input text-center m-1"
                              id="digit-4"
                              name="digit-4"
                              {...register("digit-4")}
                              maxLength="1"
                              disabled={loading}
                            />
                            <input
                              type="text"
                              onKeyUp={(e) => {
                                changeElementfocus("digit-6", e);
                              }}
                              className="otp-input text-center m-1"
                              id="digit-5"
                              name="digit-5"
                              {...register("digit-5")}
                              maxLength="1"
                              disabled={loading}
                            />
                            <input
                              type="text"
                              onKeyUp={() => {
                                if (!loading) {
                                  submitButton.current.click();
                                }
                              }}
                              className="otp-input text-center m-1"
                              id="digit-6"
                              name="digit-6"
                              {...register("digit-6")}
                              maxLength="1"
                              disabled={loading}
                            />

                            <div className="text-red small">
                              {errors["digit-1"]
                                ? errors["digit-1"].message
                                : errors["digit-2"]
                                  ? errors["digit-2"].message
                                  : errors["digit-3"]
                                    ? errors["digit-3"].message
                                    : errors["digit-4"]
                                      ? errors["digit-4"].message
                                      : errors["digit-5"]
                                        ? errors["digit-5"].message
                                        : errors["digit-6"]
                                          ? errors["digit-6"].message
                                          : errMessage
                                            ? errMessage
                                            : ""}
                            </div>

                            {isResendEmail ? (
                              <p className="font-12 color-silver mt-3">
                                Tidak mendapatkan Kode Verifikasi?
                                <a
                                  href="#"
                                  onClick={async () => {
                                    countDown();
                                    onResend();
                                    settimestampOtp(
                                      moment().format("DD/MM/YYYY, HH:mm:ss"),
                                    );
                                  }}
                                  className="font-500 color-theme"
                                >
                                  &nbsp;Kirim Ulang
                                </a>
                              </p>
                            ) : (
                              <>
                                <p className="color-silver mt-3 fw-bolder text-center">
                                  <span className="fw-bolder italic">
                                    (OTP Terakhir {timestampOtp}){" "}
                                  </span>
                                  dapat melakukan request ulang setelah{" "}
                                  {duration}
                                </p>
                              </>
                            )}
                          </div>
                        </div>
                      </div>
                      {/* <div className="form-row place-order p-5">
                          {!loading && (
                            <button
                              className="btn btn-blue btn-block"
                              type="submit"
                              ref={submitButton}
                            >
                              SUBMIT
                            </button>
                          )}
                          {loading && (
                            <div className="btn btn-block btn-blue">
                              <BeatLoader
                                color="#fff"
                                loading={loading}
                                size={7}
                                margin={2}
                              />
                            </div>
                          )}
                        </div> */}
                    </div>
                  </div>
                </div>

                <div className="row">
                  {!loading && (
                    <button
                      href="#"
                      className="btn btn-blue rounded-pill font-size-14 btn-block"
                      type="submit"
                      ref={submitButton}
                    >
                      VERIFIKASI
                    </button>
                  )}
                  {loading && (
                    <div className="btn btn-blue rounded-pill font-size-14 btn-block">
                      <BeatLoader
                        color="#fff"
                        loading={loading}
                        size={7}
                        margin={2}
                      />
                    </div>
                  )}
                  <div className="col-12">
                    {/* <button
                    className="btn btn-block btn-primary mt-3 lift"
                    onClick={() => {
                      handleClose();
                    }}
                  >
                    SUBMIT
                  </button> */}
                  </div>
                </div>

                {/* <p className="font-size-lg text-center text-muted mb-0">
                {message}
              </p>
              <div className="mb-md-4 text-center">
                <img src={imgUrl} className="w-50 image-responsive" />
              </div>

              <div className="row">
                <div className="col-12">
                  <button
                    className="btn btn-block btn-primary mt-3 lift"
                    onClick={() => {
                      handleClose();
                    }}
                  >
                    {okText}
                  </button>
                </div>
              </div> */}
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
