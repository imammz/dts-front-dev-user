import React, { useEffect, useRef, useState } from "react";
import ValidationMessage from "../../src/validation-message";
import BeatLoader from "react-spinners/BeatLoader";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import PortalService from "../../services/PortalService";
import moment from "moment";
import PortalHelper from "../../src/helper/PortalHelper";
import CountryCode from "../../data/phone_code.json";
import ReCAPTCHA from "react-google-recaptcha";

export default function VerifikasiHandphoneModa({
  show,
  handleClose,
  children,
  onVerify,
  onResend,
  nomorHandphone = "",
}) {
  const reCaptchaRef = useRef(null);
  const toggleOpenModal = useRef(null);
  const toggleCloseModal = useRef(null);
  const formVerifikasi = useRef(null);
  const showsuccessModalNotificationSuccess = useRef(null);

  const [timestampOtp, settimestampOtp] = useState(
    moment().format("DD/MM/YYYY, HH:mm:ss"),
  );
  const [timeout, setTimeOutModal] = useState(0);
  const [timeInterval, setTimeInterval] = useState(null);
  const submitButton = useRef(null);
  const [duration, setDuration] = useState("");
  const [userData, setUserData] = useState({});
  const [otpSended, setOtpSended] = useState(false);
  const [phoneCheckResult, setPhoneCheckResult] = useState(false);
  const [phoneChecked, setPhoneChecked] = useState(false);
  const [phoneChanged, setPhoneChanged] = useState(false);
  const [isResendHandphone, setIsResendHandphone] = useState(false);
  const [phoneCheckLoading, setPhoneCheckLoading] = useState(true);
  const [lenNomorHandphone, setLenNomorHandphone] = useState(0);
  const [FNomorHandphone, setFNomorHandphone] = useState({
    name: "Indonesia",
    code: "ID",
    emoji: "🇮🇩",
    unicode: "U+1F1EE U+1F1E9",
    image:
      "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ID.svg",
    phone: "62",
  });
  const defaultSelection = {
    name: "Indonesia",
    code: "ID",
    emoji: "🇮🇩",
    unicode: "U+1F1EE U+1F1E9",
    image:
      "https://cdn.jsdelivr.net/npm/country-flag-emoji-json@2.0.0/dist/images/ID.svg",
    phone: "62",
  };
  const [loading, setLoading] = useState(false);
  const [captchaErrorMessage, setCaptchaErrorMessage] = useState();
  const [userCaptchaResponse, setUserCaptchaResponse] = useState(null);

  class CaptchaError extends Error {
    constructor(message) {
      super(message); // (1)
      this.name = "CaptchaError"; // (2)
    }
  }

  const validationSchema = Yup.object({
    "digit-1": Yup.string().required(ValidationMessage.otp),
    "digit-2": Yup.string().required(ValidationMessage.otp),
    "digit-3": Yup.string().required(ValidationMessage.otp),
    "digit-4": Yup.string().required(ValidationMessage.otp),
    "digit-5": Yup.string().required(ValidationMessage.otp),
    "digit-6": Yup.string().required(ValidationMessage.otp),
    nomor_handphone: Yup.number()
      .typeError(
        ValidationMessage.required.replace("__field", "Nomor Handphone"),
      )
      .required(ValidationMessage.required.replace("__field", "Nomor Kontak"))
      .test(
        "minLength",
        ValidationMessage.minLength
          .replace("__field", "Nomor Handphone")
          .replace("__length", 7),
        (val) => String(val).length >= 8,
      )
      .test(
        "maxLength",
        ValidationMessage.maxLength
          .replace("__field", "Nomor Handphone")
          .replace("__length", 16 - FNomorHandphone.phone?.length + 1),
        (val) => String(val).length <= 16 - FNomorHandphone?.phone.length,
      )
      .test("phone_code", "Nomor Handphone tidak valid", (value) => {
        if (value) {
          if (
            (value.toString().startsWith("62") == "62" &&
              value.toString().startsWith("6262")) ||
            value.toString().startsWith("0")
          ) {
            return false;
          } else {
            return true;
          }
        }
      })
      .test(
        "unique",
        ValidationMessage.unique.replace("__field", "Nomor Handphone"),
        async (val) => {
          if (!phoneChecked) {
            try {
              setPhoneChecked(true);
              const resp = await PortalService.checkNomorHpDuplicate(
                `${FNomorHandphone.phone}${val}`,
              );
              const res = resp.data;
              if (res.success) {
                const stat_notelp = res.result.data.filter(
                  (elem) => elem.stat_notelp == true,
                );
                setPhoneCheckResult(
                  res.result.data.length == 0 || stat_notelp.length == 0,
                );
                return res.result.data.length == 0 || stat_notelp.length == 0;
              } else {
                throw Error("Belum bisa memeriksa nomor hp.");
              }
            } catch (error) {
              console.log(error);
              setPhoneCheckResult(false);
              return false;
            }
          } else return phoneCheckResult;
        },
      )
      .label("Nomor Handphone"),
  });
  const {
    register,
    handleSubmit,
    reset,
    getValues,
    setError,
    watch,
    setValue,
    trigger,
    formState: { errors },
  } = useForm({
    mode: "onBlur",
    resolver: yupResolver(validationSchema),
  });

  const stopCountDown = () => {
    clearInterval(timeInterval);
  };

  const countDown = (isReset = false) => {
    if (isReset) {
      setDuration("");
      return;
    }
    const upperBounddate = moment().add(5, "minutes");
    setIsResendHandphone(false);
    setDuration(
      moment.utc(moment(upperBounddate).diff(moment())).format("mm:ss"),
    );
    const localInterval = setInterval(() => {
      duration = moment.duration(
        moment(upperBounddate).diff(moment()),
        "milliseconds",
      );
      setDuration(moment.utc(duration.asMilliseconds()).format("mm:ss"));
      if (duration < 0) {
        setIsResendHandphone(true);
        clearInterval(localInterval);
        return;
      }
    }, 1000);
    setTimeInterval(localInterval);
  };

  useEffect(() => {
    if (timeInterval) {
      clearInterval(timeInterval);
    }
    reCaptchaRef?.current?.reset();
    showsuccessModalNotificationSuccess.current.addEventListener(
      "hidden.bs.modal",
      () => {
        handleClose();
      },
    );
    if (show) {
      reset();
      toggleOpenModal.current.click();
      setOtpSended(false);
      setPhoneCheckResult(false);
      if (timeout != 0) {
        setTimeout(() => {
          toggleCloseModal.current.click();
        }, timeout * 1000);
      }
    } else {
      toggleCloseModal.current.click();
      countDown(true);
    }
    setUserData(PortalHelper.getUserData());
  }, [show]);

  useEffect(() => {
    setLenNomorHandphone(
      FNomorHandphone?.phone.length + getValues("nomor_handphone")?.length,
    );
  }, [FNomorHandphone]);
  useEffect(() => {
    if (userData?.nomor_handphone) {
      reset({ ...{ nomor_handphone: userData.nomor_handphone.split("-")[1] } });
      let kodeNegaraHandphone = CountryCode.filter((item) =>
        userData?.nomor_handphone?.startsWith(item.phone),
      );
      if (kodeNegaraHandphone.length > 0) {
        userData.nomor_handphone = userData?.nomor_handphone.slice(
          kodeNegaraHandphone[0].phone.length,
        );
        setValue("nomor_handphone", userData.nomor_handphone);
        setFNomorHandphone(kodeNegaraHandphone[0]);
      } else {
        setFNomorHandphone(defaultSelection);
      }
    } else {
      setFNomorHandphone(defaultSelection);
    }
  }, [userData]);

  const changeElementfocus = (idElement, e) => {
    if (e.code.includes("Digit") || e.code.includes("Key")) {
      document.getElementById(idElement).focus();
    }
    return;
  };

  async function onSubmit(data, event) {
    let pin = "";
    for (let i = 1; i <= 6; i++) {
      pin += data[`digit-${i}`];
      delete data[`digit-${i}`];
    }
    try {
      setLoading(true);
      const resp = await PortalService.verifyPhoneOTP(
        `${FNomorHandphone.phone}${data.nomor_handphone}`,
        pin,
      );
      setLoading(false);
      if (resp.data.success) {
        PortalHelper.updateUserData(
          "handphone_verifikasi",
          resp.data.result.data.handphone_verifikasi,
        );
        PortalHelper.updateUserData(
          "nomor_handphone",
          resp.data.result.data.nomor_handphone,
        );
        onVerify();
      } else {
        let message = "Terjadi kesalahan, silahkan hubungi admin!";
        if (resp.data.result) {
          Object.keys(resp.data.result).map((field) => {
            message += resp.data.result[field]?.join("\n");
          });
        } else if (resp.data?.message != "") {
          message = resp.data.message;
        }
        throw Error(message);
      }
    } catch (err) {
      setLoading(false);
      console.log(err);
      const message = err.response?.data?.message
        ? err.response?.data?.message
        : err.message;
      setError("digit-1", { type: "focus", message: message }, {});
    }
  }

  return (
    <>
      <a
        href="#"
        ref={toggleOpenModal}
        data-bs-toggle="modal"
        data-bs-target="#VerifikasiHandphoneModal"
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseModal}
        data-bs-dismiss="modal"
        data-bs-target="#VerifikasiHandphoneModal"
        hidden
      ></a>

      <div
        className="modal fade"
        id="VerifikasiHandphoneModal"
        ref={showsuccessModalNotificationSuccess}
        data-bs-backdrop="static"
        data-bs-keyboard="false"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="staticBackdropLabel"
        aria-hidden="true"
      >
        <div
          className="modal-lg modal-dialog modal-dialog-centered modal-fullscreen-sm-down"
          role="document"
        >
          <div className="modal-content">
            <div className="modal-body">
              <form
                onSubmit={handleSubmit(onSubmit)}
                className="digit-group"
                data-group-name="digits"
                autoComplete="off"
                ref={formVerifikasi}
              >
                <button
                  type="button"
                  className="close float-end"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                  onClick={() => {
                    stopCountDown();
                    handleClose();
                  }}
                >
                  <span aria-hidden="true">
                    <i className="fa fa-times-circle text-muted fa-2x"></i>
                  </span>
                </button>

                <h2
                  className="fw-bold text-center mb-6 text-primary"
                  id="modalExampleTitle"
                >
                  Verifikasi Nomor Handphone
                </h2>

                <div className="row">
                  <div className="col-lg-5">
                    <img
                      src="/assets/img/empty-state/otp.png"
                      className="img-responsive"
                    />
                  </div>
                  <div className="col-lg-7">
                    <div className="d-block rounded bg-light rounded-3 mb-6">
                      <div className="pt-6 pb-5 px-5 px-lg-3 px-xl-5">
                        <div className="d-flex align-items-center">
                          <div className="mb-4">
                            <p className="mb-0 text-capitalize">
                              Silahkan masukkan kode OTP yang dikirim ke Nomor
                              mu. Jangan bagikan kode OTP kamu kepada siapapun.
                              <br></br>
                            </p>
                          </div>
                        </div>
                        <div className="col-12 mb-5">
                          <div className="form-group">
                            <div className="form-group mb-3">
                              <div className="d-flex justify-content-between">
                                <label>
                                  Nomor Handphone&nbsp;
                                  <span className="text-red">*</span>
                                </label>
                                {lenNomorHandphone >
                                  FNomorHandphone.phone.length && (
                                  <a
                                    href="#"
                                    onClick={(e) => {
                                      e.preventDefault();
                                      setValue("nomor_handphone", "");
                                    }}
                                  >
                                    Clear
                                  </a>
                                )}
                              </div>
                              <div className="input-group">
                                <button
                                  type="button"
                                  className="btn btn-outline-secondary btn-sm dropdown-toggle"
                                  data-bs-toggle="dropdown"
                                  aria-haspopup="true"
                                  aria-expanded="false"
                                >
                                  <span style={{ fontWeight: "lighter" }}>
                                    {FNomorHandphone.emoji}{" "}
                                    {FNomorHandphone.phone}
                                  </span>
                                </button>
                                <div
                                  className="dropdown-menu h-200p"
                                  style={{ overflowY: "auto" }}
                                >
                                  {CountryCode &&
                                    CountryCode.map((Code, idx) => (
                                      <a
                                        key={idx}
                                        className="dropdown-item"
                                        onClick={() => {
                                          setFNomorHandphone(Code);
                                          setValue("nomor_handphone", "");
                                        }}
                                      >
                                        {Code.emoji} {Code.code}{" "}
                                        {`+${Code.phone}`}
                                      </a>
                                    ))}
                                </div>
                                <span
                                  style={{
                                    position: "absolute",
                                    top: 14,
                                    right: 10,
                                    zIndex: 999,
                                    fontSize: 12,
                                  }}
                                >
                                  {lenNomorHandphone}
                                </span>
                                <input
                                  type="tel"
                                  {...PortalHelper.addPhoneNumberValidation()}
                                  className="form-control form-control-sm"
                                  placeholder="Nomor Handphone"
                                  {...register("nomor_handphone")}
                                  maxLength={
                                    15 - FNomorHandphone?.phone?.length
                                  }
                                  onChange={(e) => {
                                    setPhoneChanged(true);
                                    setPhoneChecked(false);
                                    setPhoneCheckResult(false);
                                    if (FNomorHandphone?.code == "ID") {
                                      e.target.value = e.target.value.replace(
                                        /^62/,
                                        "",
                                      );
                                    }
                                    setLenNomorHandphone(
                                      FNomorHandphone?.phone?.length +
                                        e.target.value?.length,
                                    );
                                  }}
                                />
                              </div>
                              <span className="text-red">
                                {errors.nomor_handphone?.message}
                              </span>
                              {!errors.nomor_handphone && (
                                <>
                                  {!userData?.handphone_verifikasi ||
                                  phoneChanged ? (
                                    <span className="form-caption font-size-sm text-italic my-2 text-red">
                                      Belum Verifikasi
                                    </span>
                                  ) : (
                                    <span className="form-caption font-size-sm text-italic my-2 text-green">
                                      Validated
                                    </span>
                                  )}
                                </>
                              )}
                              <div className="clearfix"></div>
                              <span className="form-caption font-size-sm text-italic my-2">
                                Format penulisan tanpa kode negara, wajib
                                menggunakan Nomor Handphone Aktif/WhatsApp
                              </span>
                            </div>
                            {!otpSended && (
                              <div className="d-flex align-items-center mb-5 font-size-sm">
                                <div>
                                  <ReCAPTCHA
                                    ref={reCaptchaRef}
                                    sitekey={process.env.CAPTCHA_SITEKEY}
                                    onChange={(e) => {
                                      setUserCaptchaResponse(e);
                                      trigger(["nomor_handphone"]);
                                    }}
                                    onExpired={() => {
                                      setUserCaptchaResponse(null);
                                    }}
                                    onError={() => {
                                      setUserCaptchaResponse(null);
                                    }}
                                  ></ReCAPTCHA>
                                </div>
                                {!userCaptchaResponse &&
                                  captchaErrorMessage && (
                                    <span className="text-red">
                                      {captchaErrorMessage}
                                    </span>
                                  )}
                              </div>
                            )}
                          </div>
                        </div>
                        {otpSended && (
                          <div id="otp" className="my-3">
                            <div
                              className="digit-group text-center"
                              data-group-name="digits"
                              autoComplete="off"
                            >
                              <input
                                type="text"
                                onKeyUp={(e) => {
                                  changeElementfocus("digit-2", e);
                                }}
                                className="otp-input text-center m-1"
                                id="digit-1"
                                name="digit-1"
                                {...register("digit-1")}
                                maxLength="1"
                                disabled={loading}
                              />
                              <input
                                type="text"
                                onKeyUp={(e) => {
                                  changeElementfocus("digit-3", e);
                                }}
                                className="otp-input text-center m-1"
                                id="digit-2"
                                name="digit-2"
                                {...register("digit-2")}
                                maxLength="1"
                                disabled={loading}
                              />
                              <input
                                type="text"
                                onKeyUp={(e) => {
                                  changeElementfocus("digit-4", e);
                                }}
                                className="otp-input text-center m-1"
                                id="digit-3"
                                name="digit-3"
                                {...register("digit-3")}
                                maxLength="1"
                                disabled={loading}
                              />
                              <input
                                type="text"
                                onKeyUp={(e) => {
                                  changeElementfocus("digit-5", e);
                                }}
                                className="otp-input text-center m-1"
                                id="digit-4"
                                name="digit-4"
                                {...register("digit-4")}
                                maxLength="1"
                                disabled={loading}
                              />
                              <input
                                type="text"
                                onKeyUp={(e) => {
                                  changeElementfocus("digit-6", e);
                                }}
                                className="otp-input text-center m-1"
                                id="digit-5"
                                name="digit-5"
                                {...register("digit-5")}
                                maxLength="1"
                                disabled={loading}
                              />
                              <input
                                type="text"
                                onKeyUp={() => {
                                  if (!loading) {
                                    submitButton.current.click();
                                  }
                                }}
                                className="otp-input text-center m-1"
                                id="digit-6"
                                name="digit-6"
                                {...register("digit-6")}
                                maxLength="1"
                                disabled={loading}
                              />

                              <div className="form-caption font-size-sm text-italic my-2 text-red">
                                {errors["digit-1"]
                                  ? errors["digit-1"].message
                                  : errors["digit-2"]
                                    ? errors["digit-2"].message
                                    : errors["digit-3"]
                                      ? errors["digit-3"].message
                                      : errors["digit-4"]
                                        ? errors["digit-4"].message
                                        : errors["digit-5"]
                                          ? errors["digit-5"].message
                                          : errors["digit-6"]
                                            ? errors["digit-6"].message
                                            : errors["endpoint"]}
                              </div>

                              {isResendHandphone ? (
                                <p className="font-12 color-silver mt-3">
                                  Tidak mendapatkan Kode Verifikasi?
                                  <a
                                    href="#"
                                    // style={{ cursor: "pointer" }}
                                    onClick={async (e) => {
                                      e.preventDefault();
                                      countDown();
                                      settimestampOtp(
                                        moment().format("DD/MM/YYYY, HH:mm:ss"),
                                      );
                                      // PortalService.sendPhoneOTP(
                                      //   `${FNomorHandphone.phone}${getValues(
                                      //     "nomor_handphone"
                                      //   )}`
                                      // )
                                      //   .then((result) => {})
                                      //   .catch((err) => {
                                      //     console.log(err);
                                      //   });
                                    }}
                                    className="font-500 color-theme"
                                  >
                                    &nbsp;Kirim Ulang
                                  </a>
                                </p>
                              ) : (
                                <>
                                  <p className="color-silver mt-3 fw-bolder text-center">
                                    <span className="fw-bolder italic">
                                      (OTP Terakhir {timestampOtp}){" "}
                                    </span>
                                    {`Kirim ulang setelah ${duration}`}
                                  </p>
                                </>
                              )}
                            </div>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                </div>

                <div className="row">
                  {!loading && (
                    <button
                      // href="#"
                      className="btn btn-blue rounded-pill font-size-14 btn-block"
                      // type="submit"
                      disabled={
                        !phoneCheckResult || userCaptchaResponse == null
                      }
                      ref={submitButton}
                      onClick={async (e) => {
                        e.preventDefault();
                        if (otpSended) {
                          formVerifikasi.current?.dispatchEvent(
                            new Event("submit", {
                              cancelable: true,
                              bubbles: true,
                            }),
                          );
                          return;
                        } else {
                          trigger(["nomor_handphone"])
                            .then((valid) => {
                              if (valid) {
                                if (userCaptchaResponse == null) {
                                  throw new CaptchaError(
                                    "Captcha tidak boleh kosong",
                                  );
                                }
                                return PortalService.sendPhoneOTP(
                                  `${FNomorHandphone.phone}${getValues(
                                    "nomor_handphone",
                                  )}`,
                                );
                              } else {
                                return Promise.reject("validation fail");
                              }
                            })
                            .then((resp) => {
                              if (resp.data.success) {
                                setLoading(false);
                                setOtpSended(true);
                                countDown();
                              } else if (resp.data.success == false) {
                                const error = resp.data.result;
                                setError(error);
                                setLoading(false);
                                setOtpSended(false);
                              }
                            })
                            .catch((err) => {
                              console.log(err);
                              setOtpSended(false);
                              setLoading(false);
                            });
                        }
                      }}
                    >
                      {otpSended ? "VERIFIKASI" : "KIRIM OTP"}
                    </button>
                  )}
                  {loading && (
                    <div className="btn btn-blue rounded-pill font-size-14 btn-block">
                      <BeatLoader
                        color="#fff"
                        loading={loading}
                        size={7}
                        margin={2}
                      />
                    </div>
                  )}
                  <div className="col-12"></div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
