import React, { useCallback, useEffect, useRef, useState } from "react";
import { useRouter } from "next/router";
// import Cropper from 'react-easy-crop'
import eventBus from "../EventBus";
import Croppie from "croppie";
// import ""
// import Croppie from 'react-croppie'
// import 'react-easy-crop/'
// useCallback
class CropperHelperClass {
  constructor(onComplete, onModalClose) {
    // this.onComplete
    eventBus.remove("onCompleteModalCropper");
    eventBus.dispatch("onCompleteModalCropper", {
      func: (v) => {
        onComplete(v);
      },
    });
    eventBus.remove("onCloseModalCropper");
    eventBus.dispatch("onCloseModalCropper", {
      func: (v) => {
        onModalClose(v);
      },
    });
  }
  show() {
    // console.log("show clicked")
    eventBus.dispatch("showModalCropper", true);
  }
  hide() {
    eventBus.dispatch("showModalCropper", false);
  }
  setWidth(w) {
    eventBus.dispatch("modalCropperSetWidth", parseInt(w || 0));
  }
  setHeight(h) {
    eventBus.dispatch("modalCropperSetHeight", parseInt(h || 0));
  }
  setKeteranganForm(val) {
    eventBus.dispatch("modalCropperSetKeterangan", val);
  }
  setShape(shape) {
    const availableShape = ["square", "circle"];
    if (availableShape.includes(shape)) {
      eventBus.dispatch("modalCropperSetShape", shape);
    } else {
      eventBus.dispatch("modalCropperSetShape", availableShape[0]);
    }
  }

  setMaxFileSize(size) {
    eventBus.dispatch("modalCropperSetMaxSize", parseInt(size || 0));
  }
}

export const CropperHelper = (
  onComplete = () => {},
  onModalClose = () => {},
) => {
  const h = new CropperHelperClass(onComplete, onModalClose);
  return h;
};

export default function ImageCropperComponent() {
  const fileInput = useRef(null);

  const toggleOpenModal = useRef(null);
  const toggleCloseModal = useRef(null);
  const showimageCropperModal = useRef(null);
  const c = useRef(null);
  const acceptFileType = useRef("image/png, image/jpg, image/jpeg");

  const [callBackComplete, setCallBackComplete] = useState(() => {});
  const [callBackHide, setCallBackHide] = useState(() => {});

  const [errFoto, setErrorFoto] = useState("");
  const [file, setFile] = useState(undefined);

  const [show, setShow] = useState(false);

  const [width, setWidth] = useState(230);
  const [height, setHeight] = useState(230);
  const [shape, setShape] = useState("square");
  const [maxSize, setMaxSize] = useState(0);
  const [keteranganForm, setKeteranganForm] = useState("");

  const [minimumExportSize, setMinimumExportSize] = useState(650);
  const [progress, setProgress] = useState("");

  useEffect(() => {
    // console.log(c.current)
    // console.log()
    if (c.current == null) {
      c.current = new Croppie(
        window.document.getElementById("croppieElement"),
        {
          boundary: { width: width + 20, height: height + 20 },
          viewport: { width: width, height: height, type: shape },
        },
      );
    }

    eventBus.on("showModalCropper", (bool) => {
      // console.log("showModals")
      if (bool != show) {
        setShow(bool);
      }
    });
    eventBus.on("onCompleteModalCropper", (obj) => {
      // console.log("vobj:", obj);
      setCallBackComplete(obj);
    });
    eventBus.on("onCloseModalCropper", (obj) => {
      setCallBackHide(obj);
    });
    eventBus.on("modalCropperSetWidth", (width) => {
      setWidth(width);
    });
    eventBus.on("modalCropperSetKeterangan", (ket) => {
      setKeteranganForm(ket);
    });
    eventBus.on("modalCropperSetHeight", (height) => {
      setHeight(height);
    });
    eventBus.on("modalCropperSetShape", (shape) => {
      setShape(shape);
    });
    eventBus.on("modalCropperSetMaxSize", (size) => {
      // window.alert(size)
      setMaxSize(size);
    });
  }, []);

  useEffect(() => {
    // console.log(c)
    if (c.current != null) {
      c.current.destroy();

      c.current = new Croppie(
        window.document.getElementById("croppieElement"),
        {
          boundary: { width: width + 20, height: height + 20 },
          viewport: { width: width, height: height, type: shape },
        },
      );
    }
  }, [width, height, shape]);

  const onOk = async () => {
    setErrorFoto("");
    // console.log("callBackComplete", callBackComplete)
    if (typeof callBackComplete.func == "function") {
      try {
        setProgress("mengeksport gambar");
        if (file == undefined) {
          setErrorFoto("Harap pilih file");
          setProgress("");
          return;
        }

        let expWidth = width;
        let expHeight = height;
        if (width >= height) {
          if (width < minimumExportSize) {
            expWidth = minimumExportSize;
            expHeight = (expWidth * height) / width;
          }
        } else {
          if (height < minimumExportSize) {
            expHeight = minimumExportSize;
            expWidth = (expHeight * width) / height;
          }
        }
        let options = {
          type: "blob",
          size: { width: expWidth, height: expHeight },
          // size: "original",
          // size: "viewport",
          format: "png",
          // quality: 0
        };

        c.current.result(options).then((output) => {
          let tmpFoto = new File([output], "foto.png");
          // console.log(tmpFoto)
          // window.alert(tmpFoto.size, maxSize)
          const callComplete = () => {
            setTimeout(() => {
              callBackComplete.func(tmpFoto);
              onClose();
            }, 2000);
          };
          const sizeCheck = [
            { pembagi: Math.pow(2, 10), satuan: "KB" },
            { pembagi: Math.pow(2, 20), satuan: "MB" },
          ];
          let selected = {};
          for (let i = 0; i < sizeCheck.length; i++) {
            const v = maxSize / sizeCheck[i].pembagi;
            if (v > 0 && v < 1000) {
              selected = sizeCheck[i];
            }
          }
          if (selected.pembagi == undefined) {
            selected = sizeCheck[sizeCheck.length - 1];
          }
          if (maxSize != 0) {
            if (tmpFoto.size > maxSize) {
              setErrorFoto(
                `Ukuran foto setelah di krop lebih dari maksimal yg diizinkan (${(
                  tmpFoto.size / selected.pembagi
                ).toFixed(2)} ${selected.satuan} > ${(
                  maxSize / selected.pembagi
                ).toFixed(2)} ${selected.satuan})`,
              );
              setProgress("");
            } else {
              setProgress(
                "export size : " +
                  (tmpFoto.size / selected.pembagi).toFixed(2) +
                  " " +
                  selected.satuan,
              );
              callComplete();
            }
          } else {
            setProgress(
              "export size : " +
                (tmpFoto.size / selected.pembagi).toFixed(2) +
                " " +
                selected.satuan,
            );
            callBackComplete();
          }
        });
      } catch (error) {
        setProgress("");
        return error;
      }
    } else {
      setProgress("");
      console.warn("callback not defined");
      // onClose()
    }
  };

  const onClose = () => {
    fileInput.current.value = "";
    setFile(undefined);
    setShow(false);
    setErrorFoto("");
    setMaxSize(0);
    setProgress("");
    if (typeof callBackHide?.func == "function") {
      callBackHide.func();
    }
  };

  const setCroppieImage = (e) => {
    setErrorFoto("");
    // this.file = this.$refs.file.files[0];
    // console.log(e.target.files[0])
    setFile(e.target.files[0]);

    let files = e.target.files || e.dataTransfer.files;

    if (!files.length) return;
    // console.log(files)
    let reader = new FileReader();
    reader.onload = (e) => {
      // console.log(e.target);
      c.current.bind({
        url: e.target.result,
      });
    };
    reader.readAsDataURL(files[0]);
  };

  useEffect(() => {
    showimageCropperModal.current.addEventListener("hidden.bs.modal", () => {
      onClose();
    });
    if (show) {
      toggleOpenModal.current.click();
    } else {
      toggleCloseModal.current.click();
    }
  }, [show]);

  return (
    <>
      <a
        href="#"
        ref={toggleOpenModal}
        data-bs-toggle="modal"
        data-bs-target="#imageCropperModal"
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseModal}
        data-bs-dismiss="modal"
        data-bs-target="#imageCropperModal"
        hidden
      ></a>

      <div
        className="modal fade"
        id="imageCropperModal"
        ref={showimageCropperModal}
        tabIndex={"-1"}
        role="dialog"
      >
        <div
          className="modal-dialog modal-dialog-centered modal-fullscreen-sm-down"
          role="document"
        >
          <div className="modal-content">
            <div className="modal-header py-3 px-5">
              <h5 className="modal-title">Upload Foto Formal</h5>
              <button
                type="button"
                className="close float-end"
                data-bs-dismiss="modal"
                aria-label="Close"
                onClick={() => {
                  onClose();
                }}
              >
                <span aria-hidden="true">
                  <i className="fa fa-times-circle text-muted fa-2x"></i>
                </span>
              </button>
            </div>
            <div className="modal-body">
              <div
                className="container"
                style={{ display: file == undefined ? "none" : "block" }}
              >
                <div id="croppieElement"></div>
              </div>

              <div className="font-size-lg text-center text-muted mb-0">
                <div className="form-group mb-2">
                  {/* <label for="ipFileFotooo" class="form-label">{keteranganForm}</label> */}

                  <label className="text-left form-label w-100">
                    {keteranganForm}
                  </label>
                  <input
                    id="ipFileFotooo"
                    disabled={progress != ""}
                    ref={fileInput}
                    type="file"
                    className={`form-control form-control-sm ${
                      errFoto.length > 0 ? "is-invalid" : ""
                    } ${progress.length != "" ? "disabled" : ""}`}
                    accept={acceptFileType.current}
                    onChange={setCroppieImage}
                  />

                  <span className="text-red">{errFoto}</span>
                </div>
              </div>
              <p className="fw-bold">{progress}</p>

              <div className="row">
                <div className="col-12">
                  <button
                    className={`btn btn-block btn-primary rounded-pill font-size-14 mt-3 lift ${
                      progress != "" ? "disabled" : ""
                    }`}
                    disabled={progress != ""}
                    onClick={() => {
                      onOk();
                    }}
                  >
                    {progress == "" ? (
                      "SIMPAN"
                    ) : (
                      <>
                        {" "}
                        <span
                          className="spinner-grow spinner-grow-sm me-2"
                          role="status"
                          aria-hidden="true"
                        ></span>
                        Memproses...
                      </>
                    )}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
