import React, { useEffect, useRef, useState } from "react";
import { useRouter } from "next/router";

export default function Zonasi({ show, handleClose, zonasi }) {
  const toggleOpenModal = useRef(null);
  const toggleCloseModal = useRef(null);
  const showZonasiModal = useRef(null);

  useEffect(() => {}, [zonasi]);
  zonasi?.sort(
    (a, b) => {
      const nameA = a.nama_provinsi.toUpperCase(); // ignore upper and lowercase
      const nameB = b.nama_provinsi.toUpperCase(); // ignore upper and lowercase
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }

      // names must be equal
      return 0;
    },
    [zonasi],
  );
  useEffect(() => {
    showZonasiModal.current.addEventListener("hidden.bs.modal", () => {
      handleClose();
    });
    if (show) {
      toggleOpenModal.current.click();
    } else {
      toggleCloseModal.current.click();
    }
  }, [show]);

  return (
    <>
      <a
        href="#"
        ref={toggleOpenModal}
        data-bs-toggle="modal"
        data-bs-target="#zonasimodal"
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseModal}
        data-bs-dismiss="modal"
        data-bs-target="#zonasimodal"
        hidden
      ></a>

      <div
        className="modal fade"
        id="zonasimodal"
        ref={showZonasiModal}
        tabIndex="-1"
        role="dialog"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div class="modal-header pb-4">
              <h2
                className="fw-bold text-center my-auto text-primary"
                id="modalExampleTitle"
              >
                Zonasi
              </h2>
              <button
                type="button"
                class="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body mt-0">
              <h5 className="text-left">
                <ol>
                  {zonasi?.map((Provinsi) => (
                    <li class="">{Provinsi.nama_provinsi}</li>
                  ))}
                </ol>
              </h5>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
