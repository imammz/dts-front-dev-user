const ButtonLMS = ({ status_peserta = "", id_pelatihan = 0 }) => {
  return `${status_peserta}`.toLowerCase() == "pelatihan" ? (
    <>
      <br className="d-block d-sm-none"></br>{" "}
      <a
        href={process.env.LMS_BASE_URL + "course/view.php?id=" + id_pelatihan}
        className="btn bg-gradient-1 text-white rounded-pill btn-xs px-3 mb-3 font-size-15 me-2"
        target={"_blank"}
      >
        <i className="bi bi-journal-check me-2"></i> LMS
      </a>
    </>
  ) : (
    <></>
  );
};

export default ButtonLMS;
