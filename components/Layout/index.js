import footer from "./Footer";
import headerBottom from "./Header/HeaderBottom";
import headerMobile from "./Header/HeaderMobile";
import headerTop from "./Header/HeaderTop";
import sideBar from "./Header/SideBar";

export const Footer = footer;
export const HeaderTop = headerTop;
export const HeaderBottom = headerBottom;
export const HeaderMobile = headerMobile;
export const SideBar = sideBar;
