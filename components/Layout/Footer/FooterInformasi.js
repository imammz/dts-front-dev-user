import ReactHtmlParser from "react-html-parser";
import LoadingOrComponent from "../../LoadingOrComponent";

const FooterInformasi = ({ isLoading, value = "" }) => {
  return (
    <LoadingOrComponent
      count={4}
      isLoading={isLoading}
      Component={() => ReactHtmlParser(value)}
    ></LoadingOrComponent>
  );
};
export default FooterInformasi;
