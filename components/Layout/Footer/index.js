import Link from "next/link";
import { useEffect, useState } from "react";
import PortalService from "../../../services/PortalService";
import PortalHelper from "../../../src/helper/PortalHelper";
import FooterAlamat from "./FooterAlamat";
import FooterCopyRight from "./FooterCopyRight";
import FooterInformasi from "./FooterInformasi";
import FooterPranala from "./FooterPranala";
import RouteSegment from "../../../src/route-segment";

const Footer = ({
  pathname,
  alamat = "",
  email = "",
  footer_logo = "",
  // koordinat = [],
  link = [],
  logo_description = "",
  notelp = [],
  social_media = [],
  clusterLinks = [],
}) => {
  const [isLoading, setIsLoading] = useState(false);

  return pathname?.includes("/login") ||
    pathname?.includes("/register") ||
    pathname?.includes("/mitra/daftar") ? (
    <></>
  ) : !PortalHelper.isRenderwithoutLayout(pathname) ? (
    <>
      <div className="floating-help z-index-2 m-3">
        <a
          href={`${process.env.WA_URL ?? RouteSegment.WA_URL}${
            PortalHelper.noWaDigitalent
          }`}
          target="_blank"
        >
          <img src="/assets/img/ChatBot.png" />
        </a>
      </div>
      <footer
        className="pt-8 pt-md-11 shadow-dark"
        style={{ position: "relative", backgroundColor: "#f5f5f5" }}
      >
        <div className="container">
          <div className="row" id="accordionFooter">
            <FooterAlamat
              isLoading={isLoading}
              footer_logo={footer_logo}
              alamat={alamat}
              social_media={social_media}
              notelp={notelp}
            ></FooterAlamat>

            <div className="col-12 col-md-8">
              <div className="row">
                {clusterLinks.map((menu, i) => (
                  <div className="col-12 col-md-6 pb-6" key={i}>
                    <div className="mb-5 mb-xl-0 footer-accordion">
                      <div id="widgetTwo">
                        <Link href={`/additional/${menu.url}`}>
                          <h5 className="fw-bolder mb-5">{menu.nama}</h5>
                        </Link>
                      </div>
                      <div>
                        {menu.childs.length != 0 && (
                          <ul className="list-unstyled text-gray-800 font-size-base mb-6 mb-md-8 mb-lg-0">
                            {menu.childs.map((child, j) => (
                              <li className="mb-3" key={j}>
                                <Link
                                  href={
                                    child.is_url
                                      ? PortalHelper.setUpUrl(child.urlink)
                                      : `/additional/${child.url}`
                                  }
                                >
                                  <a
                                    target={`${
                                      child.target
                                        ?.toLowerCase()
                                        .includes("blank")
                                        ? "_blank"
                                        : ""
                                    }`}
                                    className="text-reset"
                                  >
                                    {child.nama}
                                  </a>
                                </Link>
                              </li>
                            ))}
                          </ul>
                        )}
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
        {/* <div className="wrapper-footer-image"></div> */}
        <div className="container">
          <FooterCopyRight
            footer_logo={footer_logo}
            isLoading={isLoading}
          ></FooterCopyRight>
        </div>
      </footer>
      {/* <div style={{
                backgroundImage: 'url(/assets/img/footer-city.png)', backgroundPosition: "center",
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
                height: 170,
                position: "relative",
                marginTop:-270
            }}></div> */}
      {/* <div style={{
                backgroundImage: 'url(/assets/img/footer-city.png)', backgroundPosition: "center",
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
                height: 383,
                position: "relative",
                marginTop:-383
            }}></div> */}
    </>
  ) : !pathname?.includes("/mobile") ? (
    <div className="container">
      <FooterCopyRight isLoading={isLoading}></FooterCopyRight>
    </div>
  ) : (
    <></>
  );
};

export default Footer;
