import Link from "next/link";
import ReactHtmlParser from "react-html-parser";
import AppVersion from "../../AppVersion";
import LoadingOrComponent from "../../LoadingOrComponent";

const FooterCopyRight = ({ isLoading, value = "", footer_logo }) => {
  return (
    <LoadingOrComponent
      isLoading={isLoading}
      Component={() => (
        <>
          {/*         
            <div style={{
                backgroundImage: 'url(/assets/img/footer-city.png)', backgroundPosition: "center",
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
                height: 170,
                position: "relative",
                marginTop: -270
            }}></div> */}
          <div className="col-12 mt-md-5">
            <div className=" pb-6 pt-6 py-md-4 text-center text-xl-start d-flex flex-column d-md-block d-xl-flex flex-xl-row align-items-center">
              <p className="text-grey-800 font-size-sm-alone fw-semi-bold d-block mb-0 mb-md-2 mb-xl-0 order-1 order-md-0 px-9 px-md-0">
                Copyright © {new Date().getFullYear()} Badan Pengembangan SDM
                Kominfo
              </p>
              <div className="ms-xl-auto d-flex flex-column flex-md-row align-items-stretch align-items-md-center justify-content-center">
                <ul className="navbar-nav flex-row flex-wrap font-size-sm-alone mb-3 mb-md-0 mx-n4 me-md-5 justify-content-center justify-content-lg-start order-1 order-md-0">
                  <li className="nav-item py-2 py-md-0 px-0 border-top-0 text-grey-800">
                    <Link href="/syarat-ketentuan" className="text-grey-800">
                      <a
                        className="px-4 fw-semi-bold font-size-12 text-decoration-none"
                        style={{ color: "rgb(var(--bs-grey-800-rgb))" }}
                      >
                        Syarat & Ketentuan
                      </a>
                    </Link>
                  </li>
                  <li className="nav-item py-2 py-md-0 px-0 border-top-0 text-grey-800">
                    <Link
                      href="/pemberitahuan-privasi"
                      className="text-grey-800"
                    >
                      <a
                        className="px-4 fw-semi-bold font-size-12 text-decoration-none"
                        style={{ color: "rgb(var(--bs-grey-800-rgb))" }}
                      >
                        Pemberitahuan Privasi
                      </a>
                    </Link>
                  </li>
                  <li className="nav-item py-2 py-md-0 px-0 border-top-0 text-grey-800">
                    <AppVersion logo={footer_logo}></AppVersion>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </>
      )}
    ></LoadingOrComponent>
  );
};
export default FooterCopyRight;
