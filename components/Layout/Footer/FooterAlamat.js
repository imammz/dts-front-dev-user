import ReactHtmlParser from "react-html-parser";
import LoadingOrComponent from "../../LoadingOrComponent";
import React from "react";

const parsingNoTel = (tel) => {
  return `(021) ` + `${tel}`.substr(3);
};

const FooterAlamat = ({
  isLoading,
  value = "",
  alamat = "",
  email = "",
  footer_logo = "",
  // koordinat = [],
  link = [],
  logo_description = "",
  notelp = [],
  social_media = [],
}) => {
  return (
    <LoadingOrComponent
      count={3}
      isLoading={isLoading}
      Component={() => (
        <div className="col-12 col-md-4 col-lg-4">
          <img
            src={footer_logo}
            alt="..."
            className="footer-brand img-fluid mb-4 h-40p"
          />
          <div id="widgetOne">
            {/* <h5 className="fw-bolder mb-2">Alamat</h5>*/}
          </div>
          <p className="text-gray-800 mb-4 font-size-base">
            Badan Pengembangan SDM Kominfo
            <br />
            {ReactHtmlParser(alamat)}
          </p>
          {notelp.map((tel, i) => (
            <div className="mb-0" key={i}>
              <a
                href={`tel:${tel.nomor}`}
                className="text-gray-800 font-size-base"
              >
                {" "}
                <i className="fa fa-phone-alt me-1"></i>{" "}
                {parsingNoTel(tel.nomor)}
              </a>
            </div>
          ))}
          {/* <a href="tel:1234567890" className="text-gray-800 font-size-base">{JSON.stringify(notelp)}</a> */}
          <div>{/* {JSON.stringify(social_media)} */}</div>
          <ul className="list-unstyled list-inline list-social mb-6 mb-md-0 mt-4">
            {social_media.map((sosmed, i) => (
              <li className="list-inline-item list-social-item" key={i}>
                <a
                  href={sosmed.link_social_media}
                  target="_blank"
                  rel="noreferrer"
                  className="text-blue font-size-22 w-36 h-36 d-flex align-items-center justify-content-center rounded-circle border-hover"
                >
                  <i className={sosmed.icon?.value}></i>
                </a>
              </li>
            ))}
            <li className="list-inline-item list-social-item">
              <a
                href="https://www.tiktok.com/@digitalent.kominfo"
                target="_blank"
                rel="noreferrer"
                className="text-blue font-size-22 w-36 h-36 d-flex align-items-center justify-content-center rounded-circle border-hover"
              >
                <i className="bi bi-tiktok"></i>
              </a>
            </li>
          </ul>
          <div className="mt-4 mb-4">
            <p>
              <a
                href="https://apps.apple.com/id/app/digital-talent-scholarship/id1669947124?l=id"
                target="_blank"
              >
                <img
                  src="/assets/img/icon-appstore.webp"
                  alt="App Store"
                  className="img-fluid align-self-center me-3 mb-3"
                  style={{ width: "140px" }}
                />
              </a>
              <a
                href="https://play.google.com/store/apps/details?id=id.go.kominfo.digitalent"
                target="_blank"
              >
                <img
                  src="/assets/img/icon-gplay.webp"
                  alt="Google Play"
                  className="img-fluid align-self-center mb-3"
                  style={{ width: "140px" }}
                />
              </a>
            </p>
          </div>
        </div>
        // <></>
        // ReactHtmlParser(value)
      )}
    ></LoadingOrComponent>
  );
};
export default FooterAlamat;
