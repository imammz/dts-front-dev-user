import moment from "moment";
import Link from "next/link";
import { useRef, useEffect, useState } from "react";
import PortalHelper from "../../../src/helper/PortalHelper";
import eventBus from "../../EventBus";
import NotificationCounter from "../../NotificationCounter";
import AvatarUser from "../AvatarUser";
import UserSetting from "./UserSetting";

export default function SideBar({
  isLoggedin,
  user,
  logout,
  siteManajemenData,
}) {
  const sideModal = useRef();
  const [tmpState, setTmpState] = useState(true);
  const [showUserSetting, setShowUserSetting] = useState(false);

  const dismissModal = () => {
    if ($) {
      $(".modal").modal("hide");
    }
  };

  useEffect(() => {
    sideModal.current.addEventListener("show.bs.modal", () => {
      // console.log("callde")
      setTmpState(false);
      eventBus.dispatch("updateFoto", null);
      setTmpState(true);

      eventBus.on("setShowUserSetting", (v) => {
        setShowUserSetting(v);
      });
    });
  }, []);

  return (
    <>
      <div
        className="modal modal-sidebar col-5 left fade-left fade"
        id="accountModal"
        ref={sideModal}
      >
        <div className="modal-dialog modal-dialog-scrollable">
          <div className="modal-content">
            <div className="modal-header">
              <div className="row align-items-end">
                <div className="col-auto d-md-block d-lg-block d-xl-block px-3">
                  <AvatarUser
                    width={50}
                    isLoggedin={isLoggedin || tmpState}
                    isLoading={user == null}
                  ></AvatarUser>
                </div>

                <div className="col">
                  <div className="card-body mb-1 p-0">
                    {user && (
                      <>
                        <p className="mb-0 line-clamp-1">Selamat Datang,</p>
                        <h6 className="mb-0 line-clamp-1">{user.nama}</h6>
                      </>
                    )}
                  </div>
                </div>
              </div>
              <button
                type="button"
                className="close text-primary"
                data-bs-dismiss="modal"
                aria-label="Close"
              >
                {/* Icon */}
                <svg
                  width={16}
                  height={17}
                  viewBox="0 0 16 17"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M0.142135 2.00015L1.55635 0.585938L15.6985 14.7281L14.2843 16.1423L0.142135 2.00015Z"
                    fill="currentColor"
                  />
                  <path
                    d="M14.1421 1.0001L15.5563 2.41431L1.41421 16.5564L0 15.1422L14.1421 1.0001Z"
                    fill="currentColor"
                  />
                </svg>
              </button>
            </div>
            <div className="modal-body py-5 border-top">
              <div style={{ overflowY: "auto" }}>
                <h6 className="fw-bolder text-uppercase">User Menu</h6>
                <ul className="list-unstyled text-gray-800 font-size-sm-alone mb-6 mb-md-8 mb-lg-">
                  <li className="mb-4">
                    <Link
                      href={{
                        pathname: "/auth/user-profile",
                        query: {
                          menu: "dashboard",
                        },
                      }}
                    >
                      <a onClick={dismissModal} className="list-link">
                        <i className="bi bi-bar-chart me-3" />
                        Dashboard
                      </a>
                    </Link>
                  </li>
                  <li className="mb-4">
                    <Link
                      href={{
                        pathname: "/auth/user-profile",
                        query: {
                          menu: "profile",
                        },
                      }}
                    >
                      <a onClick={dismissModal} className="list-link">
                        <i className="bi bi-person me-3" />
                        Data Diri
                      </a>
                    </Link>
                  </li>
                  <li className="mb-4">
                    <Link
                      href={{
                        pathname: "/auth/user-profile",
                        query: {
                          menu: "pelatihan",
                        },
                      }}
                    >
                      <a onClick={dismissModal} className="list-link">
                        <i className="bi bi-mortarboard me-3" />
                        Pelatihan
                      </a>
                    </Link>
                  </li>
                  <li className="mb-4">
                    <Link
                      href={{
                        pathname: "/auth/user-profile",
                        query: {
                          menu: "tes-substansi",
                        },
                      }}
                    >
                      <a onClick={dismissModal} className="list-link">
                        <i className="bi bi-book me-3" />
                        Test Substansi
                      </a>
                    </Link>
                  </li>
                  <li className="mb-4">
                    <Link
                      href={{
                        pathname: "/auth/user-profile",
                        query: {
                          menu: "survey",
                        },
                      }}
                    >
                      <a onClick={dismissModal} className="list-link">
                        <i className="bi bi-clipboard-check me-3" />
                        Survey
                      </a>
                    </Link>
                  </li>
                  <li className="mb-4">
                    <Link
                      href={{
                        pathname: "/auth/user-profile",
                        query: {
                          menu: "trivia",
                        },
                      }}
                    >
                      <a onClick={dismissModal} className="list-link">
                        <i className="bi bi-gem me-3" />
                        Trivia
                      </a>
                    </Link>
                  </li>
                  <li className="mb-4">
                    <Link
                      href={{
                        pathname: "/auth/user-profile",
                        query: {
                          menu: "bookmark",
                        },
                      }}
                    >
                      <a onClick={dismissModal} className="list-link">
                        <i className="bi bi-bookmark me-3" />
                        Bookmark
                      </a>
                    </Link>
                  </li>
                  <li className="mb-4">
                    <Link
                      href={{
                        pathname: "/auth/user-profile",
                        query: {
                          menu: "notifikasi",
                        },
                      }}
                    >
                      <a onClick={dismissModal} className="list-link">
                        <i className="bi bi-bell me-3" />
                        Notifikasi{" "}
                        <span className="badge bg-red badge-pill ms-2 text-white">
                          <NotificationCounter></NotificationCounter>
                        </span>
                      </a>
                    </Link>
                  </li>
                  <li className="mb-4">
                    <Link
                      href={{
                        pathname: "/auth/user-profile",
                        query: {
                          menu: "setting",
                        },
                      }}
                    >
                      <a onClick={dismissModal} className="list-link">
                        <i className="bi bi-gear me-3" />
                        Settings
                      </a>
                    </Link>
                  </li>
                  <li className="mb-4">
                    <a
                      className="list-link"
                      href="#"
                      data-bs-dismiss="modal"
                      data-bs-target="#accountModal"
                      onClick={() => {
                        logout();
                      }}
                    >
                      <i className="bi bi-box-arrow-right me-3" />
                      Logout
                    </a>
                  </li>
                </ul>
                <h6 className="fw-bolder text-uppercase">Platform</h6>
                <ul className="list-unstyled text-gray-800 font-size-sm-alone mb-6 mb-md-8 mb-lg-">
                  {siteManajemenData.link.map((link, i) => (
                    <li className="mb-5" key={i}>
                      <Link href={link.link_member}>
                        <a className="list-link">
                          <img
                            src={PortalHelper.linkIcon(link.icon)}
                            alt={link.nama_platform}
                            className="avatar-img shadow-0 avatar-platform img-thumbnail me-3"
                          />
                          {link.nama_platform}
                        </a>
                      </Link>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* {isLoggedin ?
            <UserSetting show={showUserSetting} handleClose={() => { setShowUserSetting(false) }}> </UserSetting>

            : <></>} */}
    </>
  );
}
