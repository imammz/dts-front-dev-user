import { AsyncTypeahead, Highlighter } from "react-bootstrap-typeahead";
import "react-bootstrap-typeahead/css/Typeahead.css";
const SEARCH_URI = "https://api.github.com/search/users";
import { Fragment, useEffect, useRef, useState } from "react";
import eventBus from "../../EventBus";
import { useRouter } from "next/router";
import GeneralService from "../../../services/GeneralService";
import _ from "lodash";
import Link from "next/link";
import CacheHelper from "../../../src/helper/CacheHelper";
import PortalHelper from "../../../src/helper/PortalHelper";

const style = {
  tdImageResult: { width: 55, height: 41 },
  imgResult: { width: 40, height: 40 },
  namaItem: {},
};

const MenuItem = (props) => {
  let content = <></>;

  const onClikcItem = (nama) => {
    // window.alert(nama)
    props.setQuery(nama);
    eventBus.dispatch("setOverlay", false);
  };

  const onRemoveHist = (e) => {
    e.stopPropagation();
    console.log("ok");
  };

  if (props.tipe == "header") {
    content = (
      <h6 className="dropdown-header pb-0 pt-3 pe-none">{props.nama}</h6>
    );
  } else if (props.tipe == "pelatihan") {
    content = (
      <Link href={`/pelatihan/${props.id}`} className="mb-2">
        <table
          className="megasearch-item my-1 w-100"
          onClick={() => {
            onClikcItem(props.nama);
          }}
        >
          <tbody>
            <tr>
              <td style={style.tdImageResult} rowSpan={2}>
                <div className="me-2">
                  <img
                    className="img-fluid"
                    src={
                      props.metode_pelaksanaan == "Swakelola"
                        ? "/assets/img/kominfo.png"
                        : PortalHelper.urlMitraLogo(props.logo)
                    }
                    // style={style.imgResult}
                  />
                </div>
              </td>
              <td>
                <span
                  className="fw-semi-bold text-dark line-clamp-1 mb-n3"
                  style={style.namaItem}
                >
                  {props.slug_akademi}
                  {props.id} - {props.nama} - Batch {props.batch}
                </span>
              </td>
            </tr>
            <tr>
              <td>
                <span className="text-muted line-clamp-1">
                  <span className="me-2">
                    <i className="bi bi-mic" /> {props.nama_penyelenggara}
                  </span>
                  <span>
                    <i className="bi bi-geo-alt" />{" "}
                    {props.metode_pelatihan == "Online"
                      ? "Online"
                      : "" + props.lokasi_pelatihan}
                  </span>
                </span>
              </td>
            </tr>
          </tbody>
        </table>
      </Link>
    );
  } else if (props.tipe == "akademi") {
    content = (
      <Link href={`/akademi/${props.slug}`}>
        <table
          className="megasearch-item my-1 w-100"
          onClick={() => {
            onClikcItem(props.nama);
          }}
        >
          <tbody>
            <tr>
              <td style={style.tdImageResult} rowSpan={2}>
                <div className="me-2">
                  <img
                    src={PortalHelper.urlAkademiIcon(props.logo)}
                    className="img-fluid"
                    // style={style.imgResult}
                  ></img>
                </div>
              </td>
              <td>
                <span
                  className="fw-semi-bold text-dark line-clamp-1 mb-n1"
                  style={style.namaItem}
                >
                  {props.nama}
                </span>
              </td>
            </tr>
            <tr>
              <td>
                <span className="text-muted line-clamp-1">
                  {props.sub_nama}
                </span>
              </td>
            </tr>
          </tbody>
        </table>
      </Link>
    );
  } else if (props.tipe == "tema") {
    content = (
      <Link
        href={`/akademi/${props.slug_akademi}?tema=${props.id}`}
        // href={`/pelatihan/${props.id}`}
        // onClick={() => { onClikcItem(props.nama) }}
        className="mb-2"
      >
        <table
          className="megasearch-item my-1 w-100"
          onClick={() => {
            onClikcItem(props.nama);
          }}
        >
          <tbody>
            <tr>
              <td style={style.tdImageResult} rowSpan={2}>
                <div className="me-2">
                  <img
                    src="/assets/img/kominfo.png"
                    className="img-fluid"
                    // style={style.imgResult}
                  ></img>
                </div>
              </td>
              <td>
                <span
                  className="fw-semi-bold text-dark line-clamp-1 mb-n3"
                  style={style.namaItem}
                >
                  {props.nama}
                </span>
              </td>
            </tr>
            <tr>
              <td>
                <span className="text-muted line-clamp-1">
                  {props.nama_akademi}
                </span>
              </td>
            </tr>
          </tbody>
        </table>
      </Link>
    );
  } else if (props.tipe == "history") {
    content = (
      <Link href={`/cari?s=${props.nama}`}>
        <table
          className="my-1 w-100"
          onClick={() => {
            onClikcItem(props.nama);
          }}
        >
          <tbody>
            <tr>
              <td style={style.tdImageResult}>
                <i className="fa fa-history"></i>
              </td>
              <td>
                <span className="fw-semi-bold line-clamp-1">{props.nama}</span>
              </td>
              <td width={10}>
                <a onClick={onRemoveHist}>
                  {" "}
                  <i className="fa fa-times"></i>
                </a>
              </td>
            </tr>
          </tbody>
        </table>
      </Link>
    );
  }
  return content;
};

const MegaSearch = ({ listClass = "" }) => {
  const router = useRouter();
  const asyncRef = useRef();

  const [isLoading, setIsLoading] = useState(false);
  const [options, setOptions] = useState([]);
  const [query, setQuery] = useState("");

  const handleSearch = _.debounce((q) => {
    if (q.length == 0) {
      setOptions([]);
      console.log("set opt   ");
      return;
    }
    setIsLoading(true);

    GeneralService.generalSearch(q, 6)
      .then((res) => {
        // console.log(res)
        if (res.data.success) {
          let opt = [];
          let { akademi, pelatihan, tema } = res.data.result;
          if (pelatihan.length > 0) {
            opt.push({ tipe: "header", nama: "Pelatihan", labelKey: "p" });
            for (let i = 0; i < pelatihan.length; i++) {
              opt.push({
                ...pelatihan[i],
                nama: pelatihan[i].nama_pelatihan,
                tipe: "pelatihan",
                labelKey: `p-${i}`,
              });
            }
          }
          if (akademi.length > 0) {
            opt.push({ tipe: "header", nama: "Akademi", labelKey: "a" });
            for (let i = 0; i < akademi.length; i++) {
              opt.push({ ...akademi[i], tipe: "akademi", labelKey: `a-${i}` });
            }
          }
          if (tema.length > 0) {
            opt.push({ tipe: "header", nama: "Tema", labelKey: "t" });
            for (let i = 0; i < tema.length; i++) {
              opt.push({
                ...tema[i],
                nama: tema[i].nama_tema,
                tipe: "tema",
                labelKey: `t-${i}`,
              });
            }
          }
          // console.log(opt)
          setOptions(opt);
        }
      })
      .then(() => {
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });
  }, 700);

  const filterBy = () => true;

  const queryDetail = () => {
    let searchValue = asyncRef.current.getInput().value;
    if (searchValue.length > 0) {
      asyncRef.current.hideMenu();
      router.replace({ pathname: `/cari`, query: { s: searchValue } });
    }
  };

  const getHistorySeach = () => {
    let searchValue = asyncRef.current.getInput().value;
    if (searchValue.length == 0) {
      let cacheData = CacheHelper.GetFromCache({
        index: "searchHistory",
        force: true,
      });
      if (cacheData != undefined) {
        let opt = [{ tipe: "header", nama: "Riwayat", labelKey: "r" }];
        let i = 0;
        for (let d of cacheData) {
          opt.push({ tipe: "history", nama: d, labelKey: "r", index: i++ });
        }
        // console.log(opt)
        setOptions(opt);
      }
    }
  };

  const onToggleSearch = (v) => {
    // jika dropdown muncul
    if (v) {
      // getHistorySeach()
      setOptions([]);
    }
    eventBus.dispatch("setOverlay", v);
  };

  return (
    <>
      <div className="categori-search-wrap categori-search-wrap-modify-2 rounded-pill">
        <div className="search-wrap-3">
          <AsyncTypeahead
            ref={asyncRef}
            emptyLabel="Pencarian tidak ditemukan"
            filterBy={filterBy}
            inputProps={{
              className: "px-3 bg-light py-2 rounded-pill",
              onKeyDownCapture: (k) => {
                // console.log("key",k)
                if (k.code == "Enter") {
                  queryDetail();
                }
              },
            }}
            onMenuToggle={(v) => {
              onToggleSearch(v);
            }}
            // onBlur={onBlurTextInput}
            id="mega-searcher"
            isLoading={isLoading}
            labelKey="nama"
            minLength={0}
            onInputChange={(v) => {
              if (v == "") {
                setOptions([]);
              }
            }}
            // style={{overflowX:"hidden"}}
            onSearch={handleSearch}
            options={options}
            useCache={false}
            placeholder="Pencarian Akademi, Pelatihan, Tema..."
            maxHeight="500px"
            renderMenuItemChildren={(option, props, index) => (
              <Highlighter>
                <MenuItem {...option} setQuery={setQuery}></MenuItem>
              </Highlighter>
            )}
          ></AsyncTypeahead>

          {/* <form >
                    <input placeholder="Pencarian Akademi, Pelatihan, Tema..." type="text" onFocus={onFocusTextInput} onBlur={onBlurTextInput} />
                    <button><i className="fa fa-search" ></i></button>

                </form> */}
        </div>
        {!isLoading ? (
          <>
            <i
              className="fa fa-search"
              style={{ position: "absolute", right: 10, top: 12 }}
            ></i>
          </>
        ) : (
          <></>
        )}
      </div>
    </>
  );
};
export default MegaSearch;
