import { yupResolver } from "@hookform/resolvers/yup";
import { useEffect, useRef, useState } from "react";
import { useForm } from "react-hook-form";
import InputPassword from "../../Form/InputPassword";
import * as Yup from "yup";
import ValidationMessage from "../../../src/validation-message";
import PortalHelper from "../../../src/helper/PortalHelper";
import CountryCode from "../../../public/assets/json/phone_code.js";
import ProfilService from "../../../services/ProfileService";
import NotificationHelper from "../../../src/helper/NotificationHelper";
import eventBus from "../../EventBus";
import { removeCookies } from "cookies-next";

const Otp = ({
  email = "",
  type = "email",
  onChange = () => {},
  setHideOtp = () => {},
  isLoading = false,
  onComplete = () => {},
  reqAgain = () => {},
}) => {
  const validationSchema = Yup.object({
    "digit-1": Yup.string().required(ValidationMessage.otp),
    "digit-2": Yup.string().required(ValidationMessage.otp),
    "digit-3": Yup.string().required(ValidationMessage.otp),
    "digit-4": Yup.string().required(ValidationMessage.otp),
    "digit-5": Yup.string().required(ValidationMessage.otp),
    "digit-6": Yup.string().required(ValidationMessage.otp),
  });

  useEffect(() => {
    reset();
  }, []);

  const {
    register,
    handleSubmit,
    watch,
    reset,
    formState: { errors },
  } = useForm({
    mode: "onBlur",
    resolver: yupResolver(validationSchema),
  });

  useEffect(() => {
    const subscription = watch((value, { name, type }) => {
      // console.log(value, name, type)
      let v = "";
      Object.keys(value).forEach((k) => {
        v += `${value[k]}`;
      });
      // console.log(v)
      onChange(v);
      if (v.length == "6") {
        onComplete({ pin: v, email: email });
      }
    });
    return () => subscription.unsubscribe();
  }, [watch]);

  const changeElementfocus = (idElement, e) => {
    // console.log(e.code)
    if (e.code.includes("Digit") || e.code.includes("Key")) {
      const el = document.getElementById(idElement);
      el?.focus();
      el?.setSelectionRange(0, 0);
    } else if (e.code.includes("Backspace")) {
      const arr = idElement.split("-");
      const num = parseInt(arr[arr.length - 1]) - 2;
      if (num > 0) {
        let id = arr[0] + "-" + arr[1] + "-" + num;
        // console.log(id)
        document.getElementById(id)?.focus();
      }
    }
    return;
  };

  return (
    <>
      <div id="otp" className="my-3">
        {type == "email" ? (
          <p>
            Masukan Kode OTP yg dikirim ke{" "}
            <strong>email baru anda ({email})</strong>
          </p>
        ) : (
          <p>
            Masukan Kode OTP yg dikirim ke{" "}
            <strong>email aktif anda ({email})</strong>
          </p>
        )}

        <div
          className="digit-group"
          data-group-name="digits"
          autoComplete="off"
        >
          <input
            type="text"
            disabled={isLoading}
            onKeyUp={(e) => {
              changeElementfocus(type + "-digit-2", e);
            }}
            className="otp-input text-center m-1"
            id={type + "-digit-1"}
            name="digit-1"
            {...register("digit-1")}
            maxLength="1"
          />
          <input
            type="text"
            disabled={isLoading}
            onKeyUp={(e) => {
              changeElementfocus(type + "-digit-3", e);
            }}
            className="otp-input text-center m-1"
            id={type + "-digit-2"}
            name="digit-2"
            {...register("digit-2")}
            maxLength="1"
          />
          <input
            type="text"
            disabled={isLoading}
            onKeyUp={(e) => {
              changeElementfocus(type + "-digit-4", e);
            }}
            className="otp-input text-center m-1"
            id={type + "-digit-3"}
            name="digit-3"
            {...register("digit-3")}
            maxLength="1"
          />
          <input
            type="text"
            disabled={isLoading}
            onKeyUp={(e) => {
              changeElementfocus(type + "-digit-5", e);
            }}
            className="otp-input text-center m-1"
            id={type + "-digit-4"}
            name="digit-4"
            {...register("digit-4")}
            maxLength="1"
          />
          <input
            type="text"
            disabled={isLoading}
            onKeyUp={(e) => {
              changeElementfocus(type + "-digit-6", e);
            }}
            className="otp-input text-center m-1"
            id={type + "-digit-5"}
            name="digit-5"
            {...register("digit-5")}
            maxLength="1"
          />
          <input
            type="text"
            disabled={isLoading}
            className="otp-input text-center m-1"
            id={type + "-digit-6"}
            name="digit-6"
            {...register("digit-6")}
            maxLength="1"
          />

          <div className="text-danger">
            {errors["digit-1"]
              ? errors["digit-1"].message
              : errors["digit-2"]
                ? errors["digit-2"].message
                : errors["digit-3"]
                  ? errors["digit-3"].message
                  : errors["digit-4"]
                    ? errors["digit-4"].message
                    : errors["digit-5"]
                      ? errors["digit-5"].message
                      : errors["digit-6"]
                        ? errors["digit-6"].message
                        : ""}
          </div>

          <p
            className={`font-12 color-silver mt-3 mb-0 ${
              isLoading ? "pe-none" : ""
            }`}
          >
            Tidak mendapatkan Kode Verifikasi ?
            <a
              onClick={() => {
                reqAgain();
              }}
              className="font-500 color-theme cursor-pointer"
            >
              &nbsp;Kirim Ulang
            </a>
          </p>
          <p>
            <strong>atau</strong> <br />
            <a
              onClick={() => {
                setHideOtp();
              }}
              className="font-500 text-red cursor-pointer"
            >
              Batalkan Pengajuan Perubahan
            </a>
          </p>
        </div>
      </div>
    </>
  );
};

const UbahPassword = ({ show }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");

  const yupSchemaValidation = Yup.object({
    password_active: Yup.string().matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!#%*?&]{8,}$/,
      ValidationMessage.password.replace("__field", "Password"),
    ),
    password: Yup.string().matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!#%*?&]{8,}$/,
      ValidationMessage.password.replace("__field", "Password"),
    ),
    password_confirmation: Yup.string().oneOf(
      [Yup.ref("password"), null],
      ValidationMessage.match
        .replace("__field", "Konfirmasi Password")
        .replace("__ref", "Password"),
    ),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    getValues,
  } = useForm({
    mode: "onBlur",
    resolver: yupResolver(yupSchemaValidation),
  });

  const submit = handleSubmit(async (data) => {
    // console.log(data)
    setError("");
    setSuccess("");
    setIsLoading(true);
    try {
      const resp = await ProfilService.ubahPassword(
        data.password_active,
        data.password,
        data.password_confirmation,
      );
      // console.log(resp)
      if (resp.data.success) {
        setSuccess("Berhasil memperbarui password");
        NotificationHelper.Success({
          message: "Berhasil memperbarui password",
          onClose: () => {
            eventBus.dispatch("setShowUserSetting", true);
          },
        });
      } else {
        setError(resp.data.result);
        NotificationHelper.Failed({
          message: resp.data.result,
          onClose: () => {
            eventBus.dispatch("setShowUserSetting", true);
          },
        });
      }
    } catch (error) {
      setError("Gagal memperbarui");
      NotificationHelper.Failed({
        message: "Gagal memperbarui password",
        onClose: () => {
          eventBus.dispatch("setShowUserSetting", true);
        },
      });
    }
    setIsLoading(false);
  });

  useEffect(() => {
    setIsLoading(false);
    if (show) {
      if (error?.length == 0) {
        reset({ password_active: "", password: "", password_confirmation: "" });
      }
      if (success) {
        setTimeout(() => {
          setSuccess("");
        }, 5000);
      }
      if (error) {
        setTimeout(() => {
          setError("");
        }, 5000);
      }
    }
  }, [show]);

  // return <></>
  return (
    <>
      <div className="px-5 py-3">
        <h4 className="my-3">Ubah Password</h4>
        <div className="form-group">
          <label>Password Aktif</label>
          <InputPassword
            inputProps={{
              placeholder: "Masukan password aktif",
              name: "password_active",
            }}
            yupRegister={{ ...register("password_active") }}
            error={errors.password_active?.message}
          >
            {" "}
          </InputPassword>
        </div>
        <div className="form-group">
          <label>Password Baru</label>
          <InputPassword
            inputProps={{
              placeholder: "Masukan password baru",
              name: "password",
            }}
            yupRegister={{ ...register("password") }}
            error={errors.password?.message}
          >
            {" "}
          </InputPassword>
        </div>
        <div className="form-group mb-4">
          <label>Konfirmasi Password Baru</label>
          <InputPassword
            inputProps={{
              placeholder: "Masukan kembali password baru",
              name: "password_confirmation",
            }}
            yupRegister={{ ...register("password_confirmation") }}
            error={errors.password_confirmation?.message}
          >
            {" "}
          </InputPassword>
        </div>

        {error && (
          <div key={"a"} className="alert alert-danger" role="alert">
            {error}
          </div>
        )}
        {success && (
          <div key={"b"} className="alert alert-success" role="alert">
            {success}
          </div>
        )}

        <button
          className={`mt-5 btn btn-block btn-primary btn-sm ${
            isLoading ? "disabled" : ""
          }`}
          disabled={isLoading}
          onClick={submit}
        >
          {isLoading ? (
            <div className="spinner-border spinner-border-sm" role="status">
              <span className="visually-hidden">Loading...</span>
            </div>
          ) : (
            <>Simpan Perubahan</>
          )}
        </button>
      </div>
    </>
  );
};

const UbahEmail = ({ show, user }) => {
  const [showOtp, setShowOtp] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [otpCode, setOtpCode] = useState("");

  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");

  const yupSchemaValidation = Yup.object({
    email: Yup.string()
      .required(ValidationMessage.required.replace("__field", "Email"))
      .email(ValidationMessage.invalid.replace("__field", "Email")),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    getValues,
  } = useForm({
    mode: "onBlur",
    resolver: yupResolver(yupSchemaValidation),
  });

  const reqOtp = async (data) => {
    setIsLoading(true);
    try {
      const resp = await ProfilService.reqOtpEmail(user?.email, data.email);
      if (resp.data.success) {
        setShowOtp(true);
      } else {
        NotificationHelper.Failed({
          message:
            resp.data.result?.newemail[0].replace(
              "Newemail",
              "Email yg anda masukan",
            ) || resp.data.message,
        });
      }
      // setShowOtp(true)
    } catch (error) {
      console.log(error);
      NotificationHelper.Failed({ message: "Terjadi kesalahan" });
    }
    setIsLoading(false);
  };

  const confirmChange = async (data) => {
    setIsLoading(true);
    try {
      // console.log(data)
      const resp = await ProfilService.verifyOtpEmail(data.pin, data.email);
      // console.log(resp)
      if (resp.data.success) {
        setSuccess(resp.data.message);
        NotificationHelper.Success({
          message: resp.data.message,
          onClose: () => {
            eventBus.dispatch("setShowUserSetting", true);
          },
        });
        removeCookies(process.env.USER_DATA_KEY);
        PortalHelper.saveUserData(resp.data.result.data);
      } else {
        setError("Gagal memperbarui");
        NotificationHelper.Failed({
          message:
            resp.data.result?.newemail[0].replace(
              "Newemail",
              "Email yg anda masukan",
            ) || resp.data.message,
          onClose: () => {
            eventBus.dispatch("setShowUserSetting", true);
          },
        });
      }
    } catch (error) {
      // console.log(error)
      setError("Gagal memperbarui");
      NotificationHelper.Failed({
        message: "Gagal memperbarui email",
        onClose: () => {
          eventBus.dispatch("setShowUserSetting", true);
        },
      });
    }
    setIsLoading(false);
  };

  useEffect(() => {
    reset({ email: "" });
    setShowOtp(false);
  }, [show]);

  return (
    <div className="px-5 py-3">
      {!showOtp ? (
        <>
          <h4 className="my-3">Ubah Email</h4>
          <p>
            Email Anda : <strong>{user?.email}</strong>
          </p>
          <div className="form-group mb-5">
            <input
              type="text"
              className={`form-control form-control-sm ${
                errors.email ? "is-invalid" : ""
              }`}
              id="email"
              placeholder="Alamat email baru"
              name="email"
              {...register("email")}
            />
            <span className="text-red">{errors.email?.message}</span>
          </div>
          <button
            onClick={handleSubmit(reqOtp)}
            className={`mt-5 btn btn-block btn-primary btn-sm ${
              isLoading ? "disabled" : ""
            }`}
            disabled={isLoading}
          >
            {" "}
            {isLoading && (
              <span
                className="spinner-border spinner-border-sm"
                role="status"
                aria-hidden="true"
              ></span>
            )}
            Kirim Kode Verifikasi OTP{" "}
          </button>
        </>
      ) : (
        <Otp
          email={getValues("email")}
          isLoading={isLoading}
          onChange={(v) => {
            // console.log(v)
            setOtpCode(v);
          }}
          setHideOtp={() => {
            setShowOtp(false);
          }}
          onComplete={(data) => {
            confirmChange(data);
          }}
          reqAgain={() => {
            reqOtp({ email: getValues("email") });
          }}
        ></Otp>
      )}
    </div>
  );
};

const UbahNoHp = ({ show, user }) => {
  const [phoneCode, setPhoneCode] = useState({
    country_name: "Indonesia",
    country_slug: "ID",
    emoji: "🇮🇩",
    phone_code: "62",
  });
  const [codes, setCodes] = useState(null);
  const [showOtp, setShowOtp] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [otpCode, setOtpCode] = useState("");

  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");

  const yupSchemaValidation = Yup.object({
    nomor_handphone: Yup.number()
      .typeError(
        ValidationMessage.required.replace("__field", "Nomor Handphone"),
      )
      .test(
        "minLength",
        ValidationMessage.minLength
          .replace("__field", "Nomor Handphone")
          .replace("__length", 7),
        (val) => String(val).length >= 8,
      )
      .test(
        "maxLength",
        ValidationMessage.maxLength
          .replace("__field", "Nomor Handphone")
          .replace("__length", 16 - phoneCode.phone_code.length + 1),
        (val) => String(val).length <= 16 - phoneCode.phone_code.length,
      )
      .test("phone_code", "Nomor Handphone tidak valid", (value) => {
        if (value) {
          if (
            (phoneCode.phone_code == "62" &&
              value.toString().startsWith(phoneCode.phone_code)) ||
            value.toString().startsWith("0")
          ) {
            return false;
          } else {
            return true;
          }
        }
      })
      .required(
        ValidationMessage.required.replace("__field", "Nomor Handphone"),
      ),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    getValues,
  } = useForm({
    mode: "onBlur",
    resolver: yupResolver(yupSchemaValidation),
  });

  const reqOtp = async (data) => {
    setIsLoading(true);
    try {
      const resp = await ProfilService.reqOtpEmailHp(
        user?.nomor_handphone,
        phoneCode.phone_code + data.nomor_handphone,
      );
      if (resp.data.success) {
        setShowOtp(true);
      } else {
        NotificationHelper.Failed({
          message:
            resp.data.result?.newhp[0].replace(
              "Newhp",
              "Nomor HP yg anda masukan",
            ) || resp.data.message,
        });
      }
      // setShowOtp(true)
    } catch (error) {
      console.log(error);
      NotificationHelper.Failed({ message: "Terjadi kesalahan" });
    }
    setIsLoading(false);
  };

  const confirmChange = async (data) => {
    setIsLoading(true);
    try {
      // console.log(data)
      const resp = await ProfilService.verifyOtpEmailHp(
        data.pin,
        data.nomor_handphone,
      );
      // console.log(resp)
      if (resp.data.success) {
        setSuccess(resp.data.message);
        NotificationHelper.Success({
          message: resp.data.message,
          onClose: () => {
            eventBus.dispatch("setShowUserSetting", true);
          },
        });
        removeCookies(process.env.USER_DATA_KEY);
        PortalHelper.saveUserData(resp.data.result.data);
      } else {
        setError("Gagal memperbarui");
        NotificationHelper.Failed({
          message:
            resp.data.result?.newhp[0].replace(
              "Newhp",
              "Nomor HP yg anda masukan",
            ) || resp.data.message,
          onClose: () => {
            eventBus.dispatch("setShowUserSetting", true);
          },
        });
      }
    } catch (error) {
      // console.log(error)
      setError("Gagal memperbarui");
      NotificationHelper.Failed({
        message: "Gagal memperbarui nomor hp",
        onClose: () => {
          eventBus.dispatch("setShowUserSetting", true);
        },
      });
    }
    setIsLoading(false);
  };

  useEffect(() => {
    // console.log(CountryCode)
    setCodes(CountryCode);
    setPhoneCode({
      country_name: "Indonesia",
      country_slug: "ID",
      emoji: "🇮🇩",
      phone_code: "62",
    });

    reset({ nomor_handphone: "" });
    setShowOtp(false);
  }, [show]);

  return (
    <div className="px-5 py-3">
      {!showOtp ? (
        <>
          <h4 className="my-3">Ubah Nomor Handphone</h4>
          <p>
            No HP Anda : <strong>{user?.nomor_handphone}</strong> &nbsp;
            {user?.handphone_verifikasi && (
              <i className="fa fa-check-circle text-green" title="Verified"></i>
            )}
          </p>
          <div className="form-group mb-5">
            <div className="input-group">
              {/* <div className='input-group-prepend'> */}
              <button
                type="button"
                className="btn btn-outline-secondary btn-sm dropdown-toggle"
                data-bs-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                <span style={{ fontWeight: "lighter" }}>
                  {phoneCode.emoji} {phoneCode.phone_code}
                </span>
              </button>
              {codes && (
                <div
                  className="dropdown-menu h-200p"
                  style={{ overflowY: "auto" }}
                >
                  {codes &&
                    codes.map((code, i) => (
                      <a
                        key={i}
                        className="dropdown-item"
                        onClick={() => {
                          setPhoneCode(code);
                        }}
                      >
                        {code.emoji} {code.name} {`+${code.phone}`}
                      </a>
                    ))}
                </div>
              )}
              <input
                type="tel"
                {...PortalHelper.addPhoneNumberValidation()}
                className={`form-control form-control-sm ${
                  errors.nomor_hp ? "is-invalid" : ""
                }`}
                placeholder=""
                name="nomor_hp"
                disabled={user?.handphone_verifikasi}
                {...register("nomor_handphone")}
              />
            </div>
            <span className="form-caption font-size-sm text-italic my-2">
              Nomor Handphone Aktif/WhatsApp
            </span>{" "}
            <br />
            <span className="text-red">{errors.nomor_handphone?.message}</span>
          </div>
          {user?.handphone_verifikasi && (
            <button
              onClick={handleSubmit(reqOtp)}
              className={`mt-5 btn btn-block btn-primary btn-sm ${
                isLoading ? "disabled" : ""
              }`}
              disabled={isLoading}
            >
              {" "}
              {isLoading && (
                <span
                  className="spinner-border spinner-border-sm"
                  role="status"
                  aria-hidden="true"
                ></span>
              )}
              Kirim Kode Verifikasi OTP{" "}
            </button>
          )}
        </>
      ) : (
        <Otp
          type="hp"
          email={user?.email}
          isLoading={isLoading}
          onChange={(v) => {
            // console.log(v)
            setOtpCode(v);
          }}
          setHideOtp={() => {
            setShowOtp(false);
          }}
          onComplete={(data) => {
            confirmChange({
              pin: data.pin,
              nomor_handphone:
                phoneCode.phone_code + getValues("nomor_handphone"),
            });
          }}
          reqAgain={() => {
            reqOtp({ nomor_handphone: getValues("nomor_handphone") });
          }}
        ></Otp>
      )}
    </div>
  );
};

export default function UserSetting({ show, handleClose, children }) {
  // const router = useRouter();
  const toggleOpenModal = useRef(null);
  const toggleCloseModal = useRef(null);
  const showUserSettingModal = useRef(null);
  const btnNavUbahPassword = useRef(null);

  const [user, setUser] = useState({});

  useEffect(() => {
    showUserSettingModal.current.addEventListener("hidden.bs.modal", () => {
      handleClose();
    });
    if (show) {
      const us = PortalHelper.getUserData();
      // console.log(us)
      setUser(us);
      toggleOpenModal.current.click();
      btnNavUbahPassword.current.click();
    } else {
      toggleCloseModal.current.click();
    }
  }, [show]);

  return (
    <>
      <a
        href="#"
        ref={toggleOpenModal}
        data-bs-toggle="modal"
        data-bs-target="#userSettingModal"
        hidden
      ></a>
      <a
        href="#"
        ref={toggleCloseModal}
        data-bs-dismiss="modal"
        data-bs-target="#userSettingModal"
        hidden
      ></a>

      <div
        className="modal fade"
        id="userSettingModal"
        data-bs-backdrop="static"
        data-bs-keyboard="false"
        ref={showUserSettingModal}
        tabIndex="-1"
        role="dialog"
      >
        <div
          className="modal-dialog  modal-fullscreen-sm-down "
          role="document"
        >
          <div className="modal-content">
            <div className="modal-header p-5">
              <h5 className="modal-title">Pengaturan</h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body px-0 pt-0">
              <ul
                id="user-setting-tab"
                className="nav nav-tabs nav-fill"
                role="tablist"
              >
                <li className="nav-item">
                  <a
                    ref={btnNavUbahPassword}
                    className="nav-link active py-2"
                    data-bs-toggle="tab"
                    data-bs-target="#nav-ubah-password"
                    role="tab"
                  >
                    Password
                  </a>
                </li>
                {/* <li className="nav-item">
                  <a
                    className="nav-link py-2"
                    data-bs-toggle="tab"
                    data-bs-target="#nav-ubah-email"
                    role="tab"
                  >
                    Email
                  </a>
                </li> */}
                <li className="nav-item">
                  <a
                    className="nav-link py-2"
                    data-bs-toggle="tab"
                    data-bs-target="#nav-ubah-nohp"
                    role="tab"
                  >
                    No HP
                  </a>
                </li>
              </ul>
              <div id="user-setting-tab-content" className="tab-content">
                <div
                  id="nav-ubah-password"
                  className="tab-pane fade show active"
                  role="tabpanel"
                  aria-labelledby="nav-ubah-password"
                >
                  {<UbahPassword show={show}></UbahPassword>}
                </div>
                {/* <div
                  id="nav-ubah-email"
                  className="tab-pane fade"
                  role="tabpanel"
                  aria-labelledby="nav-ubah-email"
                >
                  {<UbahEmail show={show} user={user}></UbahEmail>}
                </div> */}
                <div
                  id="nav-ubah-nohp"
                  className="tab-pane fade"
                  role="tabpanel"
                  aria-labelledby="nav-ubah-nohp"
                >
                  {<UbahNoHp show={show} user={user}></UbahNoHp>}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
