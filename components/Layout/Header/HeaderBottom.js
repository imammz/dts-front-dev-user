import Link from "next/link";
import PortalHelper from "../../../src/helper/PortalHelper";
import RouteSegment from "./../../../src/route-segment";

const HeaderBottom = ({ ListAkademi, isOverlayShown, menus = [] }) => {
  return (
    <>
      <div
        className="navbar shadow-sm navbar-expand-xl navbar-dark p-0"
        style={isOverlayShown ? { zIndex: 1 } : {}}
      >
        <div className="container">
          <div className="collapse navbar-collapse" id="navbarCollapse">
            <ul className="navbar-nav">
              <li className="nav-item">
                <Link href={`/`}>
                  <a className="nav-link ps-0">Home</a>
                </Link>
              </li>
              <li className="nav-item dropdown dropdown-full-width">
                <a
                  className="nav-link dropdown-toggle"
                  id="navbarDocumentation"
                  data-bs-toggle="dropdown"
                  href="#"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Akademi
                </a>
                <div
                  className="dropdown-menu border-xl shadow-none dropdown-full pt-xl-7 px-xl-8"
                  aria-labelledby="navbarLandings"
                >
                  <div className="row row-cols-1 row-cols-md-2 row-cols-lg-3 row-cols-xl-3">
                    {ListAkademi.map((Akademi, i) => (
                      <div className="col-md mb-md-3 mb-2 px-2" key={i}>
                        <Link href={`/akademi/${Akademi.slug}`}>
                          <a className="card icon-category icon-category-sm">
                            <div className="row align-items-center mx-n3">
                              <div className="col-auto px-3">
                                <div className="icon-h-p secondary">
                                  <img
                                    src={PortalHelper.urlAkademiIcon(
                                      Akademi.logo,
                                    )}
                                    className="h-60p"
                                  />
                                </div>
                              </div>
                              <div className="col px-3">
                                <div className="card-body p-0">
                                  <h6 className="text-primary fw-semi-bold mb-n1">
                                    {Akademi.sub_nama}
                                  </h6>
                                  <p className="font-size-12 fw-semi-bold text-muted mb-0">
                                    {Akademi.nama}
                                  </p>
                                </div>
                              </div>
                            </div>
                          </a>
                        </Link>
                      </div>
                    ))}
                  </div>
                </div>
              </li>
              <li className="nav-item dropdown">
                <a
                  className="nav-link dropdown-toggle cursor-pointer"
                  id="navbarJadwal"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Jadwal
                </a>
                <ul
                  className="dropdown-menu border-xl shadow-none"
                  aria-labelledby="navbarJadwal"
                >
                  <li className="dropdown-item">
                    <Link href={`/jadwal-pendaftaran`}>
                      <a className="dropdown-link">Pendaftaran</a>
                    </Link>
                  </li>
                  <li className="dropdown-item">
                    <Link href={`/jadwal-pelatihan`}>
                      <a className="dropdown-link">Pelatihan</a>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item dropdown">
                <a
                  className="nav-link dropdown-toggle cursor-pointer"
                  id="navbarShop"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Rilis Media
                </a>
                <ul
                  className="dropdown-menu border-xl shadow-none"
                  aria-labelledby="navbarShop"
                >
                  <li className="dropdown-item">
                    <Link href={`/informasi`}>
                      <a className="dropdown-link">Informasi</a>
                    </Link>
                  </li>
                  <li className="dropdown-item">
                    <Link href={`/artikel`}>
                      <a className="dropdown-link">Artikel</a>
                    </Link>
                  </li>
                  {/*<li className="dropdown-item">
                    <Link href={`/gallery`}>
                      <a className="dropdown-link">Galeri</a>
                    </Link>
                  </li>*/}
                  <li className="dropdown-item">
                    <Link href={`/video`}>
                      <a className="dropdown-link">Video</a>
                    </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item">
                <Link href={`/event`}>
                  <a className="nav-link">Event</a>
                </Link>
              </li>
              <li className="nav-item dropdown">
                <a
                  href="#"
                  className="nav-link dropdown-toggle"
                  id="navbarShop"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Tentang Kami
                </a>
                <ul
                  className="dropdown-menu border-xl shadow-none"
                  aria-labelledby="navbarShop"
                >
                  <li className="dropdown-item">
                    <Link href={`/program`}>
                      <a className="dropdown-link">Program</a>
                    </Link>
                  </li>
                  <li className="dropdown-item">
                    <Link href={`/data-capaian`}>
                      <a className="dropdown-link">Data Capaian</a>
                    </Link>
                  </li>
                  <li className="dropdown-item">
                    <Link href={`/app-portal`}>
                      <a className="dropdown-link">Platform SDM Digital</a>
                    </Link>
                  </li>
                  <li className="dropdown-item">
                    <Link href={`/faq`}>
                      <a className="dropdown-link">FAQ</a>
                    </Link>
                  </li>
                  <li className="dropdown-item">
                    <Link href={`/cek-sertifikat`}>
                      <a className="dropdown-link">Validasi Sertifikat</a>
                    </Link>
                  </li>
                  <li className="dropdown-item">
                    <a
                      href={`${process.env.WA_URL ?? RouteSegment.WA_URL}${
                        PortalHelper.noWaDigitalent
                      }`}
                      target="_blank"
                      className="dropdown-link"
                    >
                      Helpdesk
                    </a>
                  </li>
                </ul>
              </li>
              <li className="nav-item">
                <Link href={`/mitra/`}>
                  <a className="nav-link">Mitra Pelatihan</a>
                </Link>
              </li>

              {menus.map((menu, i) => {
                return menu.childs?.length == 0 ? (
                  <li className="nav-item text-center" key={i}>
                    <Link href={`/additional/${menu.url}`}>
                      <a
                        target={`${
                          menu.target?.toLowerCase().includes("blank")
                            ? "_blank"
                            : "current"
                        }`}
                        className="nav-link"
                      >
                        {menu.nama}
                      </a>
                    </Link>
                  </li>
                ) : (
                  <li
                    className="nav-item dropdown"
                    onMouseEnter={(e) => {
                      if (bootstrap) {
                        new bootstrap.Dropdown(
                          document.getElementById(`navbarAdd` + i),
                        ).show();
                      }
                    }}
                    onMouseLeave={(e) => {
                      if (bootstrap) {
                        new bootstrap.Dropdown(
                          document.getElementById(`navbarAdd` + i),
                        ).hide();
                      }
                    }}
                    key={i}
                  >
                    <a
                      className="nav-link dropdown-toggle"
                      id={`navbarAdd` + i}
                      data-bs-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      {menu.nama}
                    </a>
                    <ul
                      className="dropdown-menu border-xl shadow-none"
                      aria-labelledby={`navbarAdd` + i}
                    >
                      {menu.childs.map((m, j) => (
                        <li className="dropdown-item" key={j}>
                          <Link
                            href={
                              m.is_url == 1
                                ? PortalHelper.setUpUrl(m.urlink)
                                : `/additional/${m.url}`
                            }
                          >
                            <a
                              target={`${
                                m.target?.toLowerCase().includes("blank")
                                  ? "_blank"
                                  : ""
                              }`}
                              className="dropdown-link"
                            >
                              {m.nama}
                            </a>
                          </Link>
                        </li>
                      ))}
                    </ul>
                  </li>
                );
              })}
              <li className="nav-item">
                <Link href={`/kontak`}>
                  <a className="nav-link">Kontak</a>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </>
  );
};

export default HeaderBottom;
