import Link from "next/link";
import AvatarUser from "../AvatarUser";
import MegaSearch from "./MegaSearch";

const HeaderTop = ({
  isLoggedin,
  user,
  isOverlayShown,
  logoHeader,
  isLoading = false,
}) => {
  return (
    <>
      <div className="container" style={isOverlayShown ? { zIndex: 2 } : {}}>
        <div className="row align-items-center">
          <div className="col-xl-2 col-lg-2">
            <div className="logo">
              <Link href={`/`}>
                <a>
                  <img
                    src={`${
                      logoHeader ? logoHeader : "/assets/img/dts-mono.png"
                    }`}
                    width="60"
                    alt="logo"
                  />
                </a>
              </Link>
            </div>
          </div>
          <div className="col-xl-7 col-lg-7">
            <MegaSearch></MegaSearch>
          </div>
          <div className="col-xl-3 col-lg-3">
            {!isLoggedin && (
              <>
                <div className="header-action header-action-flex">
                  <div className="same-style-2 same-style-2-white same-style-2-font-inc">
                    <Link href={`/login`}>
                      <a className="btn btn-outline-blue btn-xs px-5 font-size-12 rounded-pill">
                        <i className="bi bi-person-circle pr-10"></i>Login
                      </a>
                    </Link>
                  </div>
                  <div className="same-style-2 same-style-2-white same-style-2-font-inc">
                    <Link href={`/register`}>
                      <a className="btn btn-blue btn-xs text-white font-size-12 px-5 rounded-pill">
                        <i className="bi bi-box-arrow-in-right pr-10"></i>Daftar
                      </a>
                    </Link>
                  </div>
                </div>
              </>
            )}
            {isLoggedin && (
              <Link href="/auth/user-profile">
                <a
                  className="rounded-pill py-2 px-3 font-size-14 text-dark float-end"
                  data-bs-toggle="modal"
                  data-bs-target="#accountModal"
                >
                  <div className="d-flex flex-row align-items-center">
                    <div className="">
                      <AvatarUser
                        width={38}
                        isLoggedin={isLoggedin}
                        isLoading={isLoading}
                      />
                    </div>
                    <div className="">
                      {user && (
                        <span className="d-inline-block fw-medium ms-2">
                          {user.nama}
                          <i className="fa fa-chevron-down ms-3" />
                        </span>
                      )}
                    </div>
                  </div>
                </a>
              </Link>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default HeaderTop;
