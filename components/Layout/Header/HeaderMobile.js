import Link from "next/link";
import PortalHelper from "../../../src/helper/PortalHelper";
import AvatarUser from "../AvatarUser";
import MegaSearch from "./MegaSearch";
import RouteSegment from "./../../../src/route-segment";

const HeaderMobile = ({
  ListAkademi,
  isLoggedin,
  logoHeader,
  menus = [],
  isLoading = false,
}) => {
  return (
    <>
      <div className="container">
        <button
          id="navbar-toggler-pc"
          className="navbar-toggler ms-0 shadow-none bg-gray-4 text-dark icon-xs p-0 outline-0 h-40p w-40p d-flex d-xl-none place-flex-center"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarCollapseMobile"
          aria-controls="navbarCollapseMobile"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <i className="fa fa-bars"></i>
        </button>
        <Link href={`/`}>
          <a className="navbar-brand ms-3 me-0">
            <img
              src={`${logoHeader ? logoHeader : "/assets/img/dts-mono.png"}`}
              className="navbar-brand-img"
              alt="..."
            />
          </a>
        </Link>
        <div
          className="collapse navbar-collapse z-index-lg"
          id="navbarCollapseMobile"
        >
          <button
            id="navbar-toggler-mobile"
            className="navbar-toggler bottom-left"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarCollapseMobile"
            aria-controls="navbarCollapseMobile"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            {" "}
            <svg
              width="16"
              height="17"
              viewBox="0 0 16 17"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M0.142135 2.00015L1.55635 0.585938L15.6985 14.7281L14.2843 16.1423L0.142135 2.00015Z"
                fill="currentColor"
              ></path>
              <path
                d="M14.1421 1.0001L15.5563 2.41431L1.41421 16.5564L0 15.1422L14.1421 1.0001Z"
                fill="currentColor"
              ></path>
            </svg>
          </button>
          <ul className="navbar-nav m-0">
            <li className="nav-item">
              <Link href={`/`}>
                <a className="nav-link fw-bolder text-dark px-xl-4">Home</a>
              </Link>
            </li>
            <li className="nav-item dropdown dropdown-full-width">
              <a
                className="nav-link fw-bolder text-dark dropdown-toggle px-xl-4"
                id="navbarDocumentation"
                data-bs-toggle="dropdown"
                href="#"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Akademi
              </a>
              <div
                className="dropdown-menu border-xl shadow-none dropdown-full pt-xl-7 px-xl-8"
                aria-labelledby="navbarLandings"
              >
                <div className="row row-cols-1 row-cols-md-2 row-cols-lg-2 row-cols-xl-2">
                  {ListAkademi.map((Akademi, i) => (
                    <div className="col-md mb-md-6 mb-4 px-2" key={i}>
                      <Link href={`/akademi/${Akademi.slug}`}>
                        <a className="card icon-category icon-category-sm">
                          <div className="row align-items-center mx-n3">
                            <div className="col-auto px-3">
                              <div className="icon-h-p secondary">
                                <img
                                  src={PortalHelper.urlAkademiIcon(
                                    Akademi.logo,
                                  )}
                                  className="h-60p"
                                />
                              </div>
                            </div>

                            <div className="col px-3">
                              <div className="card-body p-0">
                                <h6 className="text-primary fw-semi-bold mb-n1">
                                  {Akademi.sub_nama}
                                </h6>
                                <p className="font-size-12 fw-semi-bold text-muted mb-0">
                                  {Akademi.nama}
                                </p>
                              </div>
                            </div>
                          </div>
                        </a>
                      </Link>
                    </div>
                  ))}
                </div>
              </div>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link fw-bolder text-dark dropdown-toggle"
                id="navbarJadwal"
                data-bs-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Jadwal
              </a>
              <ul
                className="dropdown-menu border-xl shadow-none"
                aria-labelledby="navbarJadwal"
              >
                <li className="dropdown-item">
                  <Link href={`/jadwal-pendaftaran`}>
                    <a className="dropdown-link">Pendaftaran</a>
                  </Link>
                </li>
                <li className="dropdown-item">
                  <Link href={`/jadwal-pelatihan`}>
                    <a className="dropdown-link">Pelatihan</a>
                  </Link>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <Link href="/rilis-media">
                <a
                  className="nav-link fw-bolder text-dark dropdown-toggle"
                  id="navbarShop"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Rilis Media
                </a>
              </Link>
              <ul
                className="dropdown-menu border-xl shadow-none"
                aria-labelledby="navbarShop"
              >
                <li className="dropdown-item">
                  <Link href={`/informasi`}>
                    <a className="dropdown-link">Informasi</a>
                  </Link>
                </li>
                <li className="dropdown-item">
                  <Link href={`/artikel`}>
                    <a className="dropdown-link">Artikel</a>
                  </Link>
                </li>
                {/*<li className="dropdown-item">
                    <Link href={`/gallery`}>
                      <a className="dropdown-link">Galeri</a>
                    </Link>
                  </li>*/}
                <li className="dropdown-item">
                  <Link href={`/video`}>
                    <a className="dropdown-link">Video</a>
                  </Link>
                </li>
                {/* <li className="dropdown-item">
                                <a className="dropdown-link">
                                    Berita
                                </a>
                            </li>
                            <li className="dropdown-item">
                                <a href="kategori-rilis.html" className="dropdown-link">
                                    Pengumuman
                                </a>
                            </li>
                            <li className="dropdown-item">
                                <a href="kategori-rilis.html" className="dropdown-link">
                                    Artikel
                                </a>
                            </li>
                            <li className="dropdown-item">
                                <a href="gallery.html" className="dropdown-link">
                                    Galeri
                                </a>
                            </li>
                            <li className="dropdown-item">
                                <a href="video.html" className="dropdown-link">
                                    Video
                                </a>
                            </li> */}
              </ul>
            </li>
            <li className="nav-item">
              <Link href={"/event"}>
                <a className="nav-link fw-bolder text-dark px-xl-4">Event</a>
              </Link>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link fw-bolder text-dark dropdown-toggle"
                id="navbarShop"
                data-bs-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Tentang Kami
              </a>
              <ul
                className="dropdown-menu border-xl shadow-none"
                aria-labelledby="navbarShop"
              >
                <li className="dropdown-item">
                  <Link href={`/program`}>
                    <a className="dropdown-link">Program</a>
                  </Link>
                </li>
                <li className="dropdown-item">
                  <Link href={`/data-capaian`}>
                    <a className="dropdown-link">Data Capaian</a>
                  </Link>
                </li>
                <li className="dropdown-item">
                  <Link href={`/app-portal`}>
                    <a className="dropdown-link">Platform SDM Digital</a>
                  </Link>
                </li>
                <li className="dropdown-item">
                  <Link href={`/faq`}>
                    <a className="dropdown-link">FAQ</a>
                  </Link>
                </li>
                <li className="dropdown-item">
                  <Link href={`/cek-sertifikat`}>
                    <a className="dropdown-link">Validasi Sertifikat</a>
                  </Link>
                </li>
                <li className="dropdown-item">
                  <a
                    href={`${process.env.WA_URL ?? RouteSegment.WA_URL}${
                      PortalHelper.noWaDigitalent
                    }`}
                    target="_blank"
                    rel="noreferrer"
                    className="dropdown-link"
                  >
                    Helpdesk
                  </a>
                </li>
              </ul>
            </li>
            <li className="nav-item">
              <Link href={"/mitra/"}>
                <a className="nav-link fw-bolder text-dark px-xl-4">
                  Mitra Pelatihan
                </a>
              </Link>
            </li>

            {menus.map((menu, i) => {
              return menu.childs?.length == 0 ? (
                <li className="nav-item" key={i}>
                  <Link href={`/additional/${menu.url}`}>
                    <a
                      target={`${
                        menu.target?.toLowerCase().includes("blank")
                          ? "_blank"
                          : ""
                      }`}
                      className="nav-link fw-bolder text-dark px-xl-4"
                    >
                      {menu.nama}
                    </a>
                  </Link>
                </li>
              ) : (
                <li
                  className="nav-item dropdown"
                  onMouseEnter={(e) => {
                    e.target.click();
                  }}
                  onMouseLeave={(e) => {
                    e.target.click();
                  }}
                  key={i}
                >
                  <a
                    href="#"
                    className="nav-link fw-bolder text-dark dropdown-toggle"
                    id={`navbarAdd` + i}
                    data-bs-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    {menu.nama}
                  </a>
                  <ul
                    className="dropdown-menu border-xl shadow-none"
                    aria-labelledby={`navbarAdd` + i}
                  >
                    {menu.childs.map((m, j) => (
                      <li className="dropdown-item" key={j}>
                        <Link
                          href={
                            m.is_url
                              ? PortalHelper.setUpUrl(m.urlink)
                              : `/additional/${m.url}`
                          }
                        >
                          <a
                            target={`${
                              m.target?.toLowerCase().includes("blank")
                                ? "_blank"
                                : ""
                            }`}
                            className="dropdown-link"
                          >
                            {m.nama}
                          </a>
                        </Link>
                      </li>
                    ))}
                  </ul>
                </li>
              );
            })}
            <li className="nav-item">
              <Link href={"/kontak"}>
                <a className="nav-link fw-bolder text-dark px-xl-4">Kontak</a>
              </Link>
            </li>
          </ul>
        </div>
        <ul className="navbar-nav flex-row ms-auto ms-xl-0 me-n2 me-md-n4 align-items-center">
          <li className="nav-item border-0 px-0 d-sm-block d-lg-block d-xl-none">
            <a
              id="btnCollapseSearchMobile"
              className="nav-link d-flex px-3 px-md-4 search-mobile text-dark icon-xs"
              data-bs-toggle="collapse"
              href="#collapseSearchMobile"
              role="button"
              aria-expanded="false"
              aria-controls="collapseSearchMobile"
            >
              {" "}
              <svg
                width="20"
                height="20"
                viewBox="0 0 20 20"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M8.80758 0C3.95121 0 0 3.95121 0 8.80758C0 13.6642 3.95121 17.6152 8.80758 17.6152C13.6642 17.6152 17.6152 13.6642 17.6152 8.80758C17.6152 3.95121 13.6642 0 8.80758 0ZM8.80758 15.9892C4.8477 15.9892 1.62602 12.7675 1.62602 8.80762C1.62602 4.84773 4.8477 1.62602 8.80758 1.62602C12.7675 1.62602 15.9891 4.8477 15.9891 8.80758C15.9891 12.7675 12.7675 15.9892 8.80758 15.9892Z"
                  fill="currentColor"
                />
                <path
                  d="M19.762 18.6121L15.1007 13.9509C14.7831 13.6332 14.2687 13.6332 13.9511 13.9509C13.6335 14.2682 13.6335 14.7831 13.9511 15.1005L18.6124 19.7617C18.7712 19.9205 18.9791 19.9999 19.1872 19.9999C19.395 19.9999 19.6032 19.9205 19.762 19.7617C20.0796 19.4444 20.0796 18.9295 19.762 18.6121Z"
                  fill="currentColor"
                />
              </svg>
              <svg
                width="16"
                height="17"
                viewBox="0 0 16 17"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M0.142135 2.00015L1.55635 0.585938L15.6985 14.7281L14.2843 16.1423L0.142135 2.00015Z"
                  fill="currentColor"
                ></path>
                <path
                  d="M14.1421 1.0001L15.5563 2.41431L1.41421 16.5564L0 15.1422L14.1421 1.0001Z"
                  fill="currentColor"
                ></path>
              </svg>{" "}
            </a>
            <div
              className="collapse position-absolute right-0 left-0 mx-4"
              id="collapseSearchMobile"
            >
              <div className="card card-body p-4 mt-6 mt-xl-4 shadow-dark">
                <MegaSearch listClass="px-2"></MegaSearch>
              </div>
            </div>
          </li>
          <li className="nav-item border-0 px-0">
            {/* <Link href="/auth/user-profile"> */}
            {!isLoggedin ? (
              <Link href="/login">
                <a className="nav-link d-flex px-3 px-md-4 text-dark icon-xs">
                  <AvatarUser
                    width={38}
                    isLoggedin={isLoggedin}
                    isLoading={isLoading}
                  />
                </a>
              </Link>
            ) : (
              <>
                <a
                  className="nav-link d-flex px-3 px-md-4 text-white-all icon-xs"
                  data-bs-toggle={"modal"}
                  data-bs-target={"#accountModal"}
                >
                  <AvatarUser
                    width={38}
                    isLoggedin={isLoggedin}
                    isLoading={isLoading}
                  />
                </a>
              </>
            )}

            {/* </Link> */}
          </li>
        </ul>
      </div>
    </>
  );
};

export default HeaderMobile;
