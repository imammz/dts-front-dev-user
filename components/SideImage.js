import React, { useEffect, useState } from "react";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

import PortalService from "../services/PortalService";
import PortalHelper from "../src/helper/PortalHelper";

function SideImage({ className, loading }) {
  const [sideImages, setSideImages] = useState(null);

  useEffect(async () => {
    const resp = await PortalService.imagetronList();
    setSideImages(resp.data.result.slice(0, 2));
  }, []);
  //   col-xl-3 px-4 mb-5 mb-xl-0
  return (
    <>
      {loading && (
        <div className={className}>
          <div className="col-lg-12 mb-2">
            <Skeleton href="#" className="h-200p">
              {/* <img src={sidebar01} className="img-responsive" width={'100%'}/> */}
            </Skeleton>
          </div>

          <div className="col-lg-12 mb-2">
            <Skeleton href="#" className="h-200p">
              {/* <img src="/assets/img/simonas-ads.jpg" className="img-responsive"/> */}
            </Skeleton>
          </div>
        </div>
      )}

      {!loading && sideImages && (
        <div className="row gx-0">
          {sideImages.map((image, index) => (
            <div
              className={`col-6 col-lg-6 col-xl-12 ${
                index < sideImages.length - 1
              }`}
            >
              <a target={"_blank"} href={image.link_url} width={"100%"}>
                <img
                  src={PortalHelper.urlPublikasiLogo(image.gambar)}
                  className="img-responsive"
                  width={"100%"}
                />
              </a>
            </div>
          ))}
        </div>
      )}
    </>
  );
}

export default SideImage;
