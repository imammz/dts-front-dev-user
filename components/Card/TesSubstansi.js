import moment from "moment";
import Link from "next/link";
import { useEffect, useState } from "react";
import Moment from "react-moment";
import ProfilService from "../../services/ProfileService";
import { useRouter } from "next/router";
import NotificationHelper from "./../../src/helper/NotificationHelper";

export default function CardTesSubstansi({
  user_id,
  user_nama,
  user_email,
  user_nik,
  category,
  akademi_id,
  akademi_nama,
  pelatihan_id,
  pelatihan_nama,
  nilai,
  tgl_pengerjaan,
  menit,
  status,
  total_jawab,
  subtance_id,
  subtance_nama,
  duration,
  questions_to_share,
  subtansi_mulai,
  subtansi_selesai,
  midtest_mulai,
  midtest_selesai,

  start_datetime,
  finish_datetime,
  alur_pendaftaran,
}) {
  const router = useRouter();
  const [stat, setStat] = useState("");
  const [tglPengerjaan, setTanggalPengerjaan] = useState("-");
  const [loading, setLoading] = useState(false);

  const [tglMulai, setTglMulai] = useState("2000-01-01");
  const [tglSelesai, setTglSelesai] = useState("2000-01-01");
  const [eligible, setEligible] = useState(false);

  // tipe a, adm -> test sub
  // tipe b, test sub -> adm
  const [tipeAlurPendaftaran, setTipeAlurPendaftaran] = useState();

  useEffect(async () => {
    // if (alur_pendaftaran == "Administrasi - Test Substansi") {
    const eligible = await checkEligible();
    setEligible(eligible);
    // }
  }, []);

  useEffect(() => {
    if (status && tglPengerjaan) {
      setStat(status?.toLowerCase());
      if (moment(tgl_pengerjaan).isValid()) {
        setTanggalPengerjaan(tglPengerjaan);
      } else {
        setTanggalPengerjaan(
          moment(tgl_pengerjaan, "DD-MM-YYYY").format("YYYY-MM-DD"),
        );
      }
      // console.log(subtansi_mulai, subtansi_selesai, midtest_mulai, midtest_selesai)
      if (`${category}`.toLowerCase() == "mid test") {
        setTglMulai(midtest_mulai);
        setTglSelesai(midtest_selesai);
      } else {
        setTglMulai(subtansi_mulai);
        setTglSelesai(subtansi_selesai);
      }
      // if (midtest_mulai != null && midtest_selesai != null) {
      //   // console.log(subtance_nama , "mid")
      //   setTglMulai(midtest_mulai);
      //   setTglSelesai(midtest_selesai);
      // } else if (subtansi_mulai != null && subtansi_selesai != null) {
      //   // console.log(subtance_nama , "end")
      //   setTglMulai(subtansi_mulai);
      //   setTglSelesai(subtansi_selesai);
      // } else {
      //   setTglMulai("2000-01-02");
      //   setTglSelesai("2000-01-02");
      // }

      // set tipe alur tipe
      if (alur_pendaftaran == "Test Substansi - Administrasi") {
        setTipeAlurPendaftaran("b");
      } else {
        setTipeAlurPendaftaran("a");
      }
    }
  }, [status, tglPengerjaan, subtansi_mulai, midtest_mulai]);

  const checkEligible = async () => {
    setLoading(true);
    if (alur_pendaftaran == "Test Substansi - Administrasi") {
      return true;
    }
    try {
      const resp = await ProfilService.cekStatusPelatihanPeserta(pelatihan_id);
      setLoading(false);
      // console.log(resp)
      if (resp.data.success) {
        if (category == "Mid Test" && alur_pendaftaran == "Administrasi") {
          return resp.data.result.data[0].status_peserta == "Pelatihan";
        }
        return resp.data.result.data[0].status_peserta == "Tes Substansi";
      } else {
        throw Error(resp.data.message);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const isClickAvailable = () => {
    if (tipeAlurPendaftaran == "b") {
      return !moment().isSameOrAfter(moment(tglSelesai));
    }
    return (
      (stat == "belum mengerjakan" &&
        !moment().isSameOrAfter(moment(tglSelesai)) &&
        moment().isSameOrAfter(tglMulai)) ||
      stat == "sudah mengerjakan"
    );
  };

  return (
    <>
      <div className="bg-white border rounded-5 px-5 mb-5">
        <div className="card border-bottom pt-3 mb-3">
          <div className="row gx-0">
            <a className="col-auto p-3 d-block" style={{ maxWidth: 80 }}>
              <img
                className="img-fluid shadow-light-lg"
                src="/assets/img/exam.png"
                alt="Mitra"
              />
            </a>
            <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
              <div className="card-body p-2">
                <div className="mb-0">
                  <span className="text-dark font-size-sm fw-normal">
                    {category}
                  </span>
                </div>
                <div className="mb-0">
                  <span className="text-dark font-size-sm fw-normal">
                    {akademi_nama}
                  </span>
                </div>
                <a
                  className="d-block mb-2"
                  href="#"
                  disabled={!(isClickAvailable() && eligible)}
                  onClick={async (e) => {
                    e.preventDefault();
                    const isEligible = await checkEligible();
                    if (isEligible) {
                      router.push({
                        pathname: `/auth/test-substansi/${pelatihan_id}/${subtance_id}`,
                      });
                    } else {
                      NotificationHelper.Failed({
                        message:
                          "Maaf Digiers, kamu belum bisa mengerjakan Tes Substansi " +
                          subtance_nama +
                          " karena belum lolos seleksi Administrasi.",
                      });
                    }
                  }}
                >
                  <h6
                    className={`${
                      isClickAvailable() ? "cursor-pointer" : ""
                    } line-clamp-2 mb-3`}
                  >
                    {subtance_nama} :{" "}
                    <span className="text-dark">{pelatihan_nama}</span>{" "}
                  </h6>
                </a>
                <ul className="nav mx-n3 d-block d-md-flex">
                  <li className="nav-item px-3 mb-3 mb-md-0">
                    <div className="d-flex align-items-center">
                      <div className="me-2 d-flex text-secondary icon-uxs">
                        <i className="fa fa-edit"></i>
                      </div>
                      <div className="font-size-sm">
                        {questions_to_share} Soal
                      </div>
                    </div>
                  </li>
                  <li className="nav-item px-3 mb-3 mb-md-0">
                    <div className="d-flex align-items-center">
                      <div className="me-2 d-flex text-secondary icon-uxs">
                        <i className="fa fa-clock"></i>
                      </div>
                      <div className="font-size-sm">{duration} Menit</div>
                    </div>
                  </li>
                  {/* {!isNaN(nilai) && <li className="nav-item px-3 mb-3 mb-md-0">
                                    <div className="d-flex align-items-center">
                                        <div className="me-2 d-flex text-secondary icon-uxs"><i className="fa fa-bullseye"></i></div>
                                        <div className="font-size-sm">Nilai : {nilai}</div>
                                    </div>
                                </li>} */}
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="row gx-2 mb-3">
          {stat == "belum mengerjakan" ? (
            <>
              {/* cek alur pelatihan */}
              {tipeAlurPendaftaran == "a" ? (
                <>
                  {moment().isBefore(moment(tglMulai)) ? (
                    <>
                      <div className="col-md mb-md-0">
                        <div className="d-flex align-items-center pb-3">
                          <i className="fa fa-clock"></i>
                          <span className="ms-2">
                            Waktu pengerjaan tes substansi dimulai pada (
                            <Moment format="D MMMM YYYY, HH:mm" locale="id">
                              {tglMulai}
                            </Moment>
                            )
                          </span>
                        </div>
                      </div>
                    </>
                  ) : (
                    <>
                      {/* cek jika kadaluarsa */}
                      {moment().isSameOrAfter(moment(tglSelesai)) ? (
                        <>
                          <div className="col-md mb-md-0">
                            <div className="d-flex align-items-center pb-3">
                              <i className="fa fa-stopwatch"></i>
                              <span className="ms-2">
                                Waktu pengerjaan tes substansi telah berakhir (
                                <Moment format="D MMMM YYYY, HH:mm" locale="id">
                                  {tglSelesai}
                                </Moment>
                                )
                              </span>
                            </div>
                          </div>
                        </>
                      ) : (
                        <>
                          <div className="col-md mb-md-0">
                            {/* <div>{moment().format("YYYY-MM-DD HH:mm")}-{moment(tglSelesai).format("YYYY-MM-DD HH:mm")}</div> */}
                            <div className="d-flex align-items-center text-alizarin pb-3">
                              <i className="fa fa-bell"></i>
                              <span className="ms-2">
                                Deadline :{" "}
                                <Moment format="D MMMM YYYY, HH:mm" locale="id">
                                  {tglSelesai}
                                </Moment>
                              </span>
                            </div>
                          </div>
                          <div className="col-auto align-self-end pb-3">
                            {/* <Link
                              href={`/auth/test-substansi/${pelatihan_id}/${subtance_id}`}
                            > */}
                            {eligible ? (
                              <a
                                href="#"
                                className={`btn ${
                                  loading
                                    ? "btn-secondary text-white"
                                    : "btn-blue rounded-pill"
                                } btn-xs px-3 font-size-15`}
                                onClick={async () => {
                                  const isEligible = await checkEligible();
                                  if (isEligible) {
                                    router.push({
                                      pathname: `/auth/test-substansi/${pelatihan_id}/${subtance_id}`,
                                    });
                                  } else {
                                    NotificationHelper.Failed({
                                      message:
                                        "Maaf Digiers, kamu belum bisa mengerjakan Tes Substansi " +
                                        subtance_nama +
                                        " karena belum lolos seleksi Administrasi.",
                                    });
                                  }
                                }}
                              >
                                {loading ? (
                                  <>
                                    <span
                                      className="spinner-border spinner-border-sm mt-1"
                                      role="status"
                                      aria-hidden="true"
                                    ></span>
                                  </>
                                ) : (
                                  <>
                                    <i className="fa fa-play"></i>
                                  </>
                                )}
                                <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                                  Kerjakan Tes Substansi
                                </span>
                              </a>
                            ) : (
                              <div className="badge badge-sm bg-yellow badge-pill">
                                <span className="text-white font-size-sm fw-normal">
                                  Menunggu Seleksi Administrasi
                                </span>
                              </div>
                            )}
                            {/* </Link> */}
                          </div>
                        </>
                      )}
                    </>
                  )}
                </>
              ) : (
                <>
                  {moment().isBefore(moment(tglMulai)) ? (
                    <>
                      <div className="col-md mb-md-0">
                        <div className="d-flex align-items-center pb-3">
                          <i className="fa fa-clock"></i>
                          <span className="ms-2">
                            Waktu pengerjaan tes substansi dimulai pada (
                            <Moment format="D MMMM YYYY, HH:mm" locale="id">
                              {tglMulai}
                            </Moment>
                            )
                          </span>
                        </div>
                      </div>
                    </>
                  ) : (
                    <>
                      {moment().isSameOrAfter(moment(tglSelesai)) ? (
                        <>
                          <div className="col-md mb-md-0">
                            <div className="d-flex align-items-center pb-3">
                              <i className="fa fa-stopwatch"></i>
                              <span className="ms-2">
                                Waktu pengerjaan tes substansi telah berakhir (
                                <Moment format="D MMMM YYYY, HH:mm" locale="id">
                                  {tglSelesai}
                                </Moment>
                                )
                              </span>
                            </div>
                          </div>
                        </>
                      ) : (
                        <>
                          <div className="col-md mb-md-0">
                            {/* <div>{moment().format("YYYY-MM-DD HH:mm")}-{moment(tglSelesai).format("YYYY-MM-DD HH:mm")}</div> */}
                            <div className="d-flex align-items-center text-alizarin pb-3">
                              <i className="fa fa-bell"></i>
                              <span className="ms-2">
                                Deadline :{" "}
                                <Moment format="D MMMM YYYY, HH:mm" locale="id">
                                  {tglSelesai}
                                </Moment>
                              </span>
                            </div>
                          </div>
                          <div className="col-auto align-self-end pb-3">
                            <Link
                              href={`/auth/test-substansi/${pelatihan_id}/${subtance_id}`}
                            >
                              <a className="btn btn-blue rounded-pill btn-xs px-3 font-size-15">
                                <i className="fa fa-play"></i>
                                <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                                  Kerjakan Tes Substansi
                                </span>
                              </a>
                            </Link>
                          </div>
                        </>
                      )}
                    </>
                  )}
                </>
              )}
            </>
          ) : stat == "sudah mengerjakan" ? (
            <>
              <div className="col-md mb-md-0">
                <div className="d-flex align-items-center text-green pb-3">
                  <i className="fa fa-check-circle"></i>
                  <span className="ms-2">
                    Sudah Dikerjakan :{" "}
                    <Moment locale="id" format="D MMMM YYYY, HH:mm">
                      {finish_datetime}
                    </Moment>
                  </span>
                </div>
              </div>
            </>
          ) : tgl_pengerjaan ? (
            <>
              {moment(finish_datetime).isValid() &&
              moment()
                .subtract(1, "minutes")
                .isSameOrAfter(moment(finish_datetime)) ? (
                <>
                  <div className="col-md mb-md-0">
                    <div className="d-flex align-items-center text-green pb-3">
                      <i className="fa fa-check-circle"></i>
                      <span className="ms-2">
                        Sudah Dikerjakan :{" "}
                        <Moment locale="id" format="D MMMM YYYY, HH:mm">
                          {finish_datetime}
                        </Moment>
                      </span>
                    </div>
                  </div>
                </>
              ) : (
                <>
                  <div className="col-md mb-md-0">
                    <div className="d-flex align-items-center pb-3">
                      <i className="fa fa-stopwatch"></i>
                      <span className="ms-2">Sedang dikerjakan</span>
                    </div>
                  </div>
                  <div className="col-auto align-self-end pb-3">
                    <Link
                      href={`/auth/test-substansi/${pelatihan_id}/${subtance_id}`}
                    >
                      <a className="btn btn-blue rounded-pill btn-xs px-3 font-size-15">
                        <i className="fa fa-play"></i>
                        <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                          Lanjutkan Pengerjaan
                        </span>
                      </a>
                    </Link>
                  </div>
                </>
              )}
            </>
          ) : (
            <>
              <div className="col-md mb-md-0">
                <div className="d-flex align-items-center pb-3">
                  <i className="fa fa-clock"></i>
                  <span className="ms-2">Test Substansi Selesai</span>
                </div>
              </div>
            </>
          )}
        </div>
      </div>
    </>
  );
}
