import Link from "next/link";
import { useState } from "react";
import moment from "moment/moment";
import PortalHelper from "../../src/helper/PortalHelper";
import SimpleDateRange from "../SimpleDateRange";

export default function CardPelatihan({
  id = "",
  logo = "",
  nama_akademi = "",
  batch = "",
  nama_tema = "",
  namasub_akademi = "",
  nama_pelatihan = "",
  pendaftaran_mulai = "",
  pendaftaran_selesai = "",
  metode_pelatihan = "",
  lokasi_pelatihan = "",
  metode_pelaksanaan = "",
  nama_mitra = "",
  nama_penyelenggara = "",
  pelatihan_mulai = "",
  pelatihan_selesai = "",
  current_server_time = "",
  diff = "",

  cardClassName = "",
  showStatusPelatihan = true,
  showButtonDetail = true,
  customAction = null,
  useTanggalPelatihan = false,

  showButtonBatalPelatihan = false,
}) {
  const [showModal, setShowModal] = useState(false);
  let CustomAction = <></>;
  if (typeof customAction == "function") {
    CustomAction = customAction;
  }
  // console.log(customAction,CustomAction)
  return (
    <div className={`card ${cardClassName}`}>
      <div className="row gx-0">
        <Link href={`/pelatihan/${id}`}>
          <a className="col-auto p-3 d-block" style={{ maxWidth: "100px" }}>
            <div className="d-flex justify-content-center align-items-center h-100">
              <img
                className="img-fluid shadow-light-lg"
                src={
                  metode_pelaksanaan == "Swakelola"
                    ? "/assets/img/kominfo.png"
                    : PortalHelper.urlMitraLogo(logo)
                }
                alt={nama_mitra}
              />
            </div>
          </a>
        </Link>
        {/* Body */}
        <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
          <div className="card-body p-3">
            {showStatusPelatihan ? (
              <>
                {" "}
                {PortalHelper.isPelatihanOpen(
                  pendaftaran_mulai,
                  pendaftaran_selesai,
                  current_server_time,
                ) == 1 && (
                  <>
                    <span
                      className="badge badge-success font-size-sm fw-normal mb-1 rounded-5"
                      style={{ backgroundColor: "#11BB82" }}
                    >
                      Buka
                    </span>
                  </>
                )}
                {PortalHelper.isPelatihanOpen(
                  pendaftaran_mulai,
                  pendaftaran_selesai,
                  current_server_time,
                ) == 2 && (
                  <>
                    <span
                      className="badge badge-danger font-size-sm fw-normal mb-1 rounded-5"
                      style={{ backgroundColor: "#de4d6e" }}
                    >
                      Tutup
                    </span>
                  </>
                )}
              </>
            ) : (
              <></>
            )}
            <div className="mt-1 mb-0">
              <h5 className="fw-bolder text-muted mb-n1">Batch {batch}</h5>
            </div>
            <Link href={`/pelatihan/${id}`}>
              <a className="d-block mb-2">
                <h5 className="fw-bolder mb-n1">{nama_pelatihan}</h5>
                <span className="text-dark font-size-sm mt-n1">
                  {namasub_akademi} ({nama_akademi})
                </span>
              </a>
            </Link>
            <ul className="nav mx-n2 d-block d-md-flex">
              <li className="nav-item px-2 mb-1">
                <div className="d-flex align-items-center">
                  <div className="me-1 d-flex text-secondary icon-uxs">
                    {/* Icon */}
                    <i className="bi bi-calendar2-week" />
                  </div>
                  <div className="font-size-sm">
                    {useTanggalPelatihan ? (
                      <SimpleDateRange
                        startDate={pelatihan_mulai}
                        endDate={pelatihan_selesai}
                        diff={diff}
                      ></SimpleDateRange>
                    ) : (
                      <SimpleDateRange
                        startDate={pendaftaran_mulai}
                        endDate={pendaftaran_selesai}
                        diff={diff}
                      ></SimpleDateRange>
                    )}
                  </div>
                </div>
              </li>
              <li className="nav-item px-2 mb-1">
                <div className="d-flex align-items-center">
                  <div className="me-1 d-flex text-secondary icon-uxs">
                    {/* Icon */}
                    <i className="bi bi-pass" />
                  </div>
                  <div className="font-size-sm">{nama_tema}</div>
                </div>
              </li>
              <li className="nav-item px-2 mb-1">
                <div className="d-flex align-items-center">
                  <div className="me-1 d-flex text-secondary icon-uxs">
                    {/* Icon */}
                    <i className="bi bi-geo-alt" />
                  </div>
                  <div className="font-size-sm">
                    {metode_pelatihan == "Online"
                      ? "Online"
                      : "" + lokasi_pelatihan}
                  </div>
                </div>
              </li>
              <li className="nav-item px-2 mb-1">
                <div className="d-flex align-items-center">
                  <div className="me-1 d-flex text-secondary icon-uxs">
                    {/* Icon */}
                    <i className="bi bi-mic" />
                  </div>
                  <div className="font-size-sm">{nama_penyelenggara}</div>
                </div>
              </li>
              {metode_pelaksanaan == "Mitra" && (
                <li className="nav-item px-2 mb-1">
                  <div className="d-flex align-items-center">
                    <div className="me-1 d-flex text-secondary icon-uxs">
                      {/* Icon */}
                      <i className="bi bi-person-video3" />
                    </div>
                    <div className="font-size-sm">{nama_mitra}</div>
                  </div>
                </li>
              )}
            </ul>
          </div>
        </div>
        {customAction ? (
          <CustomAction></CustomAction>
        ) : showButtonDetail ? (
          <>
            <div className="col-auto pe-xl-2 d-lg-flex place-center">
              <Link href={`/pelatihan/${id}`}>
                <a className="col-auto btn bg-blue rounded-5 text-white btn-xs mb-5 mb-lg-0">
                  Lihat Detail
                </a>
              </Link>
            </div>
          </>
        ) : (
          <></>
        )}
      </div>
    </div>
  );
}
