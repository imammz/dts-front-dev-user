import { useEffect, useState, useMemo } from "react";
import moment from "moment";
import "moment/locale/id";
import Moment from "react-moment";
import Link from "next/link";

export default function CardSurvey({
  user_id = 0,
  user_nama = "",
  user_email = "",
  user_nik = "",
  akademi_id = 0,
  akademi_nama = "",
  pelatihan_id = 0,
  pelatihan_nama = "",
  nilai = "-",
  nama_level,
  jenis_survey,
  tgl_pengerjaan = null,
  menit = null,
  status = "",
  total_jawab = null,
  survey_id = "",
  survey_nama = "",
  survey_mulai = "",
  survey_selesai = "",
  status_peserta_survey = "TRUE",
}) {
  const [stat, setStat] = useState("");

  const availableByStatusPeserta = useMemo(() => {
    return (
      status_peserta_survey.toLowerCase() == "true" ||
      status_peserta_survey == true
    );
  }, [status_peserta_survey]);

  useEffect(() => {
    setStat(status.toLowerCase());
  }, []);

  const isClickAvailable = () => {
    // return true
    return (
      (stat == "belum mengerjakan" &&
        !moment().isSameOrAfter(moment(survey_selesai)) &&
        moment().isBefore(moment(survey_mulai)) &&
        availableByStatusPeserta) ||
      stat == "sudah mengerjakan"
    );
  };
  // useEffect
  return (
    <>
      <div className="bg-white border rounded-5 px-5 mb-5">
        <div className="card border-bottom pt-3 mb-3">
          <div className="row gx-0">
            <a className="col-auto p-3 d-block" style={{ maxWidth: 80 }}>
              <img
                className="img-fluid shadow-light-lg"
                src="/assets/img/survey.png"
                alt="Mitra"
              />
            </a>
            <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
              <div className="card-body p-2">
                <div className="mb-0">
                  <span className="text-dark font-size-sm fw-normal">
                    Survey {jenis_survey}
                    <i className="bi bi-chevron-right mx-1"></i>
                    {nama_level}
                  </span>
                </div>
                <div className="d-block mb-2">
                  <Link
                    href={`${
                      isClickAvailable()
                        ? `/auth/survey/${pelatihan_id}/${survey_id}`
                        : "javascript:void(0)"
                    }`}
                  >
                    <h5
                      className={`${
                        isClickAvailable() ? "cursor-pointer" : ""
                      } line-clamp-2 mb-0`}
                    >
                      {survey_nama}
                      {/* <span className="text-dark">: {pelatihan_nama}</span>*/}
                    </h5>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row gx-2 mb-3">
          {stat == "belum mengerjakan" ? (
            <>
              {moment().isBefore(moment(survey_mulai)) ? (
                <>
                  <div className="col-md mb-md-0">
                    <div className="d-flex align-items-center pb-3">
                      <i className="fa fa-stopwatch"></i>
                      <span className="ms-2">
                        Waktu pengerjaan survei dimulai pada (
                        <Moment format="D MMMM YYYY" locale="id">
                          {survey_mulai}
                        </Moment>
                        )
                      </span>
                    </div>
                  </div>
                </>
              ) : (
                <>
                  {moment().isSameOrAfter(
                    moment(survey_selesai, "YYYY-MM-DD HH:mm:ss").add(
                      1440,
                      "minutes",
                    ),
                  ) ? (
                    <>
                      <div className="col-md mb-md-0">
                        <div className="d-flex align-items-center pb-3">
                          <i className="fa fa-clock"></i>
                          <span className="ms-2">
                            {" "}
                            {}
                            Waktu pengerjaan survei telah berakhir (
                            <Moment format="D MMMM YYYY" locale="id">
                              {survey_selesai}
                            </Moment>
                            )
                          </span>
                        </div>
                      </div>
                    </>
                  ) : availableByStatusPeserta ? (
                    <>
                      <div className="col-md mb-md-0">
                        <div className="d-flex align-items-center text-alizarin pb-3">
                          <i className="fa fa-bell"></i>
                          <span className="ms-2">
                            Deadline :{" "}
                            <Moment format="D MMMM YYYY" locale="id">
                              {survey_selesai}
                            </Moment>
                          </span>
                        </div>
                      </div>
                      <div className="col-auto align-self-end pb-3">
                        <Link
                          href={`/auth/survey/${pelatihan_id}/${survey_id}`}
                        >
                          <a className="btn btn-blue rounded-pill btn-xs px-3 font-size-15">
                            <i className="fa fa-play"></i>
                            <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                              Kerjakan Survey
                            </span>
                          </a>
                        </Link>
                      </div>
                    </>
                  ) : (
                    <div className="col-md mb-md-0">
                      <div className="d-flex align-items-center pb-3">
                        <i className="fa fa-exclamation-circle"></i>
                        <span className="ms-2">
                          Anda belum dapat mengerjakan survey ini
                        </span>
                      </div>
                    </div>
                  )}
                </>
              )}
            </>
          ) : stat == "sudah mengerjakan" ? (
            <>
              <div className="col-md mb-md-0">
                <div className="d-flex align-items-center text-green pb-3">
                  <i className="fa fa-check-circle"></i>
                  <span className="ms-2">
                    Sudah Dikerjakan :{" "}
                    <Moment format="D MMMM YYYY, HH:mm" locale="id">
                      {tgl_pengerjaan}
                    </Moment>{" "}
                  </span>
                </div>
              </div>
            </>
          ) : tgl_pengerjaan ? (
            <>
              {stat == "sudah mengerjakan" ? (
                <>
                  <div className="col-md mb-md-0">
                    <div className="d-flex align-items-center text-green text-success-center pb-3">
                      <i className="fa fa-check-circle"></i>
                      <span className="ms-2">
                        Sudah Dikerjakan :{" "}
                        <Moment format="D MMMM YYYY, HH:mm" locale="id">
                          {tgl_pengerjaan}
                        </Moment>{" "}
                      </span>
                    </div>
                  </div>
                </>
              ) : (
                <>
                  {moment().isSameOrAfter(
                    moment(survey_selesai, "YYYY-MM-DD HH:mm:ss").add(
                      1440,
                      "minutes",
                    ),
                  ) ? (
                    <>
                      <div className="col-md mb-md-0">
                        <div className="d-flex align-items-center  pb-3">
                          <i className="fa fa-stopwatch"></i>
                          <span className="ms-2">
                            {" "}
                            {}
                            Waktu pengerjaan survei telah berakhir (
                            <Moment format="D MMMM YYYY" locale="id">
                              {survey_selesai}
                            </Moment>
                            )
                          </span>
                        </div>
                      </div>
                    </>
                  ) : (
                    <>
                      <div className="col-md mb-md-0">
                        <div className="d-flex align-items-center text-primary pb-3">
                          <i className="fa fa-stopwatch"></i>
                          <span className="ms-2">Sedang dikerjakan</span>
                        </div>
                      </div>
                      <div className="col-auto align-self-end pb-3">
                        <Link
                          href={`/auth/survey/${pelatihan_id}/${survey_id}`}
                        >
                          <a className="btn btn-blue rounded-pill btn-xs px-3 font-size-15">
                            <i className="fa fa-play"></i>
                            <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                              Lanjutkan Pengerjaan
                            </span>
                          </a>
                        </Link>
                      </div>
                    </>
                  )}
                </>
              )}
            </>
          ) : (
            <>
              <div className="col-md mb-md-0">
                <div className="d-flex align-items-center pb-3">
                  <i className="fa fa-stopwatch"></i>
                  <span className="ms-2">Survei selesai</span>
                </div>
              </div>
            </>
          )}
        </div>
      </div>
    </>
  );
}
