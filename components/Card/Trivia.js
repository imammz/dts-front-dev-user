import { useEffect, useState, useMemo } from "react";
import moment from "moment";
import "moment/locale/id";
import Moment from "react-moment";
import Link from "next/link";

export default function CardTrivia({
  user_id,
  user_nama,
  user_email,
  user_nik,
  akademi_id,
  akademi_nama,
  pelatihan_id,
  pelatihan_nama,
  nilai,
  nama_level,
  tgl_pengerjaan,
  status_peserta,
  menit,
  status,
  total_jawab,
  trivia_id,
  trivia_nama,
  trivia_mulai,
  trivia_selesai,
  finish_datetime,
  status_peserta_trivia = "TRUE",
}) {
  const [stat, setStat] = useState("");
  const [tglPengerjaan, setTanggalPengerjaan] = useState("2000-01-01");

  const [tglMulai, setTglMulai] = useState("2000-01-01");
  const [tglSelesai, setTglSelesai] = useState("2000-01-01");

  const availableByStatusPeserta = useMemo(() => {
    return (
      status_peserta_trivia.toLowerCase() == "true" ||
      status_peserta_trivia == true
    );
  }, [status_peserta_trivia]);

  useEffect(() => {
    if (status && tglPengerjaan) {
      setStat(status?.toLowerCase());
      if (moment(tgl_pengerjaan).isValid()) {
        setTanggalPengerjaan(tglPengerjaan);
      } else {
        setTanggalPengerjaan(
          moment(tgl_pengerjaan, "DD-MM-YYYY").format("YYYY-MM-DD"),
        );
      }
      if (trivia_mulai != null && trivia_selesai != null) {
        setTglMulai(trivia_mulai);
        setTglSelesai(trivia_selesai);
      } else {
        setTglMulai("2000-01-01");
        setTglSelesai("2000-01-01");
      }
    }
  }, [status, tgl_pengerjaan, trivia_mulai]);

  const isClickAvailable = () => {
    // return true
    return (
      (stat == "belum mengerjakan" &&
        !moment().isSameOrAfter(moment(tglSelesai)) &&
        moment().isBefore(moment(tglMulai)) &&
        availableByStatusPeserta) ||
      // && !["7", "8", "10"].includes(status_peserta)
      stat == "sudah mengerjakan"
    );
  };

  return (
    <>
      <div className="bg-white border rounded-5 px-5 mb-5">
        <div className="card border-bottom pt-3 mb-3">
          <div className="row gx-0">
            {/* Image */}
            <a className="col-auto p-3 d-block" style={{ maxWidth: 80 }}>
              <img
                className="img-fluid shadow-light-lg"
                src="/assets/img/trivia.png"
                alt="Trivia"
              />
            </a>
            {/* Body */}
            <div className="col-12 col-md-9 col-lg-9 col-xl-9 d-md-block">
              <div className="card-body p-2">
                <div className="mb-0">
                  <span className="text-dark font-size-sm fw-normal">
                    Trivia<i className="bi bi-chevron-right mx-1"></i>
                    {nama_level}
                  </span>
                </div>
                <a className="d-block mb-2">
                  <Link
                    href={`${
                      isClickAvailable()
                        ? `/auth/trivia/${pelatihan_id}/${trivia_id}`
                        : "javascript:void(0)"
                    }`}
                  >
                    <h5
                      className={`${
                        isClickAvailable() ? "cursor-pointer" : ""
                      } line-clamp-2 mb-0`}
                    >
                      {trivia_nama}{" "}
                      {/* <span className="text-dark">: {pelatihan_nama}</span>*/}{" "}
                    </h5>
                  </Link>
                  <div className="mb-0">
                    <span className="text-muted font-size-sm fw-semibold">
                      {" "}
                      <Moment format="D MMMM YYYY" locale="id">
                        {trivia_mulai}
                      </Moment>{" "}
                      -{" "}
                      <Moment format="D MMMM YYYY" locale="id">
                        {trivia_selesai}
                      </Moment>
                    </span>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className="row gx-2 mb-3">
          {stat == "belum mengerjakan" ? (
            <>
              {/* jika sudah lulu tidak bisa mengerjakan trivia */}
              {
                <>
                  {moment().isBefore(moment(tglMulai)) ? (
                    <>
                      <div className="col-md mb-md-0">
                        <div className="d-flex align-items-center pb-3">
                          <i className="fa fa-clock"></i>
                          <span className="ms-2">
                            Waktu pengerjaan trivia dimulai (
                            <Moment format="D MMMM YYYY" locale="id">
                              {tglMulai}
                            </Moment>
                            )
                          </span>
                        </div>
                      </div>
                    </>
                  ) : (
                    <>
                      {moment().isSameOrAfter(moment(tglSelesai)) ? (
                        <>
                          <div className="col-md mb-md-0">
                            <div className="d-flex align-items-center pb-3">
                              <i className="fa fa-clock"></i>
                              <span className="ms-2">
                                Waktu pengerjaan trivia telah berakhir (
                                <Moment format="D MMMM YYYY" locale="id">
                                  {tglSelesai}
                                </Moment>
                                )
                              </span>
                            </div>
                          </div>
                        </>
                      ) : availableByStatusPeserta ? (
                        <>
                          <div className="col-md mb-md-0">
                            <div className="d-flex align-items-center text-alizarin pb-3">
                              <i className="fa fa-bell"></i>
                              <span className="ms-2">
                                Deadline :{" "}
                                <Moment format="D MMMM YYYY" locale="id">
                                  {tglSelesai}
                                </Moment>
                              </span>
                            </div>
                          </div>
                          <div className="col-auto align-self-end pb-3">
                            <Link
                              href={`/auth/trivia/${pelatihan_id}/${trivia_id}`}
                            >
                              <a className="btn btn-blue rounded-pill btn-xs px-3 font-size-15">
                                <i className="fa fa-play"></i>
                                <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                                  Kerjakan Trivia
                                </span>
                              </a>
                            </Link>
                          </div>
                        </>
                      ) : (
                        <>
                          <div className="col-md mb-md-0">
                            <div className="d-flex align-items-center pb-3">
                              <i className="fa fa-exclamation-circle"></i>
                              <span className="ms-2">
                                Anda belum dapat mengerjakan trivia ini
                              </span>
                            </div>
                          </div>
                        </>
                      )}
                    </>
                  )}
                </>
              }
            </>
          ) : stat == "sudah mengerjakan" ? (
            <>
              <div className="col-md mb-md-0">
                <div className="d-flex align-items-center text-green pb-3">
                  <i className="fa fa-check-circle"></i>
                  <span className="ms-2">
                    Sudah Dikerjakan :{" "}
                    <Moment locale="id" format="D MMMM YYYY, HH:mm">
                      {finish_datetime}
                    </Moment>
                  </span>
                </div>
              </div>
            </>
          ) : tgl_pengerjaan ? (
            <>
              {moment(finish_datetime).isValid() ? (
                <>
                  <div className="col-md mb-md-0">
                    <div className="d-flex align-items-center text-green pb-3">
                      <i className="fa fa-check-circle"></i>
                      <span className="ms-2">
                        Sudah Dikerjakan :{" "}
                        <Moment locale="id" format="D MMMM YYYY, HH:mm">
                          {finish_datetime}
                        </Moment>
                      </span>
                    </div>
                  </div>
                </>
              ) : (
                <>
                  <div className="col-md mb-md-0">
                    <div className="d-flex align-items-center text-primary  pb-3">
                      <i className="fa fa-clock"></i>
                      <span className="ms-2">Sedang Dikerjakan</span>
                    </div>
                  </div>
                  <div className="col-auto align-self-end pb-3">
                    <Link href={`/auth/trivia/${pelatihan_id}/${trivia_id}`}>
                      <a className="btn btn-blue btn-xs px-3 font-size-15">
                        <i className="fa fa-play"></i>
                        <span className="ms-2 d-lg-inline-block d-md-inline-block d-xl-inline-block">
                          Lanjutkan Pengerjaan
                        </span>
                      </a>
                    </Link>
                  </div>
                </>
              )}
            </>
          ) : (
            <>
              <div className="col-md mb-md-0">
                <div className="d-flex align-items-center pb-3">
                  <i className="fa fa-clock"></i>
                  <span className="ms-2">Trivia Selesai</span>
                </div>
              </div>
            </>
          )}
        </div>
      </div>
    </>
  );
}
