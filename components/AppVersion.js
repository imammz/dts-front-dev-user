import { useEffect } from "react";

const appVersion = "1.0.0";
const availabelMode = ["text", "modal"];
export default function AppVersion({ mode = "modal", logo = "" }) {
  let activeMode = mode;
  if (!availabelMode.includes(activeMode)) {
    activeMode = availabelMode[0];
  }

  useEffect(() => {
    if (window.$) {
      window.$("#modalAppVersion").appendTo("body");
    }
  }, [window.$]);

  useEffect(() => {
    document.getElementById("logo-app-version")?.setAttribute("src", logo);
  }, [logo]);

  return activeMode == "text" ? (
    <>
      <span>DTS-NG {appVersion}</span>
    </>
  ) : (
    <>
      <header>
        <div
          className="modal fade"
          id="modalAppVersion"
          tabIndex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
              {/* <div className="modal-header">
                                <h1 className="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
                                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div> */}
              <div className="modal-body">
                <div className="d-flex py-5">
                  <div className="w-80p h-80p overflow-hidden me-5">
                    <img
                      id="logo-app-version"
                      src={logo}
                      width={150}
                      className="mb-5"
                    ></img>
                    <div className="mb-0">
                      <span className="d-block">
                        <h4 className="fw-bolder mb-0">
                          Versi : DTS-NG {appVersion}
                        </h4>
                      </span>
                      <span className="text-muted font-size-14">
                        Puslitbang APTIKA-IKP Kominfo
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              {/* <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="button" className="btn btn-primary">Save changes</button>
                            </div> */}
            </div>
          </div>
        </div>
      </header>
      <a
        data-bs-toggle="modal"
        data-bs-target="#modalAppVersion"
        className="text-decoration-none fw-semi-bold cursor-pointer"
        style={{ color: "rgb(var(--bs-grey-800))" }}
      >
        DTS-NG {appVersion}
      </a>
      {/* <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modalAppVersion">
                Launch demo modal
            </button> */}
    </>
  );
}
