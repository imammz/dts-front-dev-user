import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import PortalHelper from "../src/helper/PortalHelper";
import PortalService from "../services/PortalService";
import {
  HeaderTop,
  HeaderBottom,
  HeaderMobile,
  Footer,
  SideBar,
} from "../components/Layout";
import eventBus from "../components/EventBus";
import ImageCropperComponent from "../components/Modal/ImageCropper";
import { SuccessNotification } from "../components/Modal/Success";
import { FailedNotification } from "../components/Modal/Failed";
import _ from "lodash";
import ProfilService from "../services/ProfileService";
import { AlertPelatihan } from "../components/AlertWarning";

// function to set indenttaion sticky header
const setIndent = _.debounce((event) => {
  const headerMobile = document.getElementById("header-mobile");
  const headerDesktop = document.getElementById("header-desktop");
  const headerIndent = document.getElementById("header-indentation");

  let h = headerMobile?.offsetHeight;
  if (headerMobile?.offsetHeight < headerDesktop?.offsetHeight) {
    h = headerDesktop?.offsetHeight;
  }
  // console.log(h)
  if (headerIndent) {
    headerIndent.style.height = `${h - 5}px`;
  }
}, 100);

function Layout(props) {
  const router = useRouter();
  const [ListAkademi, setAkademi] = useState([]);
  const [siteManajemenData, setSiteManajemenData] = useState({
    alamat: "",
    color: [],
    created_at: "",
    created_by: "",
    email: "",
    footer_logo: "",
    header_logo: "",
    koordinat: [],
    link: [],
    logo_description: "",
    notelp: [],
    social_media: [],
  });

  const [isLoggedin, setIsLoggedin] = useState(false);

  const [showOverlay, setShowOverlay] = useState(false);

  const [user, setUser] = useState(null);

  const [dynamicMenus, setDynamicMenus] = useState({ header: [], footer: [] });

  useEffect(() => {
    // Register Event Bus
    eventBus.on("setOverlay", (bool) => {
      if (typeof bool == "boolean") {
        setShowOverlay(bool);
      }
    });
    eventBus.on("profil-updated", () => {
      setUser(PortalHelper.getUserData());
      eventBus.dispatch("updateFoto");
    });
    eventBus.on("logout", logoutFunc);
    // setIndent()
    window.addEventListener("resize", setIndent);

    const handleRouteChangeComplete = (url, { shallow }) => {
      if (localStorage.getItem("need-recall-script")) {
        const script = document.querySelector(
          'script[data-name="theme-script"]',
        );
        if (script) {
          script.remove();
          const ns = document.createElement("script");
          ns.src = script.getAttribute("src");
          ns.setAttribute("data-name", "theme-script");
          document.head.appendChild(ns);
          localStorage.removeItem("need-recall-script");
        }
      }
      if (PortalHelper.isRenderwithoutLayout(url)) {
        localStorage.setItem("need-recall-script", "ok");
      }
    };

    router.events.on("routeChangeComplete", handleRouteChangeComplete);

    return () => {
      window.removeEventListener("resize", setIndent);
      router.events.off("routeChangeComplete", handleRouteChangeComplete);
    };
  }, []);

  const logoutFunc = async () => {
    let logout = await ProfilService.logout();
    PortalHelper.destroyUserCookies();
    setUser(null);
    setIsLoggedin(false);
    router.push("/login");
  };

  useEffect(async () => {
    eventBus.on("updateMenuLink", async () => {
      console.log("updateMenuLink");
      let result = {};
      // const appProps = await App.getInitialProps(appContext);
      const resp = await PortalService.generalDataSiteManajamenen();
      // console.log("resp", resp)
      if (resp?.data?.success) {
        // console.log(settingData.data.result.Data)
        const {
          alamat,
          color,
          created_at,
          created_by,
          email,
          footer_logo,
          header_logo,
          koordinat,
          link,
          logo_description,
          notelp,
          social_media,
        } = resp.data.result[0];

        result = {
          alamat: alamat,
          color: JSON.parse(color),
          created_at: created_at,
          created_by: created_by,
          email: email,
          footer_logo: footer_logo,
          header_logo: header_logo,
          koordinat: koordinat?.split(","),
          logo_description: logo_description,
          notelp: JSON.parse(notelp),
          link: JSON.parse(link),
          social_media: JSON.parse(social_media),
        };
        setSiteManajemenData(result);
      }
    });
  });

  useEffect(async () => {
    // calls page's `getInitialProps` and fills `appProps.pageProps`
    let result = {};
    // const appProps = await App.getInitialProps(appContext);
    const resp = await PortalService.generalDataSiteManajamenen();
    // console.log("resp", resp)
    if (resp?.data?.success) {
      // console.log(settingData.data.result.Data)
      const {
        alamat,
        color,
        created_at,
        created_by,
        email,
        footer_logo,
        header_logo,
        koordinat,
        link,
        logo_description,
        notelp,
        social_media,
      } = resp.data.result[0];

      result = {
        alamat: alamat,
        color: JSON.parse(color),
        created_at: created_at,
        created_by: created_by,
        email: email,
        footer_logo: footer_logo,
        header_logo: header_logo,
        koordinat: koordinat?.split(","),
        logo_description: logo_description,
        notelp: JSON.parse(notelp),
        link: JSON.parse(link),
        social_media: JSON.parse(social_media),
      };
      setSiteManajemenData(result);
      // console.log(siteManajemenData)
    }
  }, []);

  useEffect(async () => {
    setIndent();
    setIsLoggedin(PortalHelper.isLoggedin());
    setUser(PortalHelper.getUserData());
  }, [router.pathname]);

  useEffect(async () => {
    try {
      if (router.isReady) {
        setIndent();
        const resAkademi = await PortalService.akademiList();
        setAkademi(resAkademi.data.result);

        // terkait alamat dsb

        // terkait menu
        const menus = await PortalService.generalMenuSiteManajamenen();
        // console.log(menus)
        if (menus.data.success) {
          const menuList = _.orderBy(menus.data.result, ["id"], ["asc"]);
          // console.log(menuList)
          const menuHeaderList = _.filter(menuList, (o) => {
            return o.jenis_menu?.toLowerCase().includes("header");
          });
          const menuFooterList = _.filter(menuList, (o) => {
            return o.jenis_menu?.toLowerCase().includes("footer");
          });
          // console.log(menuHeaderList)
          // console.log("footer pars", PortalHelper.refactorMenu(menuFooterList))
          setDynamicMenus({
            header: PortalHelper.refactorMenu(menuHeaderList),
            footer: PortalHelper.refactorMenu(menuFooterList),
          });
          // console.log(headMenu)
          // console.log(menuList)
          setIndent();
        }
      }
    } catch (error) {
      console.log(error);
    }
  }, [router.isReady]);

  useEffect(async () => {
    try {
      if (isLoggedin) {
        // notifikasi
        const notif = await ProfilService.unreadNotificationBadge();
        // console.log(notif)
        if (notif.data.success) {
          if (notif.data.result != null) {
            eventBus.dispatch(
              "setNotificationCounter",
              notif.data.result.data[0]?.jml,
            );
          } else {
            eventBus.dispatch("setNotificationCounter", 0);
          }
        }
      }
    } catch (error) {}
  }, [isLoggedin]);

  return (
    <>
      {PortalHelper.isRenderwithoutLayout(props.pathname) && (
        <>
          <div className="bg-white">{props.children}</div>
          <Footer pathname={props.pathname} {...siteManajemenData}></Footer>
        </>
      )}
      {!PortalHelper.isRenderwithoutLayout(props.pathname) && (
        <div
          className={props.bodyBackground ? props.bodyBackground : "bg-gray"}
        >
          <header
            id="header-desktop"
            className="header-area d-md-block d-xl-block d-lg-none fixed-top"
          >
            <div className="header-large-device">
              <div className="header-top bg-white header-top-ptb-7 border-bottom-8">
                <HeaderTop
                  user={user}
                  isLoggedin={isLoggedin}
                  isOverlayShown={showOverlay}
                  logoHeader={siteManajemenData?.header_logo}
                  isLoading={siteManajemenData?.header_logo == ""}
                ></HeaderTop>
              </div>
              <div className="header-bottom bg-white">
                <HeaderBottom
                  ListAkademi={ListAkademi}
                  isOverlayShown={showOverlay}
                  menus={dynamicMenus.header}
                ></HeaderBottom>
              </div>
            </div>
          </header>
          <header
            id="header-mobile"
            className="navbar shadow-sm d-sm-block d-lg-block d-xl-none navbar-expand-xl bg-white py-0 fixed-top"
          >
            <HeaderMobile
              menus={dynamicMenus.header}
              isLoggedin={isLoggedin}
              ListAkademi={ListAkademi}
              logoHeader={siteManajemenData?.header_logo}
              isLoading={siteManajemenData?.header_logo == ""}
            >
              {" "}
            </HeaderMobile>
          </header>
          <SideBar
            siteManajemenData={siteManajemenData}
            isLoggedin={isLoggedin}
            user={user}
            logout={logoutFunc}
          ></SideBar>

          {/* untuk indent dari sticky header */}
          <div id="header-indentation" className="d-block"></div>
          {/* <div className='d-none d-xl-block' style={{ height: 128, }}></div>
				<div className='d-none d-sm-block d-xl-none' style={{ height: 75, }}></div>
				<div className='d-block d-sm-none' style={{ height: 60 }}></div> */}

          <div
            id="body-page"
            style={{ position: "relative", zIndex: showOverlay ? 0 : "auto" }}
          >
            <AlertPelatihan />
            <div
              className="overlay-custom"
              style={{
                width: "100%",
                height: "100%",
                position: "absolute",
                zIndex: 10,
                pointerEvents: "none",
                background: "rgba(0,0,0,0.5)",
                display: showOverlay ? "block" : "none",
                // display: 'block'
              }}
            ></div>
            {props.children}
            {/* <div className="floating-help z-index-2 m-3">
						<a href={`${process.env.WA_URL ?? RouteSegment.WA_URL}${PortalHelper.noWaDigitalent}`} target='_blank'>
							<img src="/assets/img/ChatBot.webp" />
						</a>
					</div> */}
            <Footer
              pathname={props.pathname}
              {...siteManajemenData}
              clusterLinks={dynamicMenus.footer}
            ></Footer>
          </div>
          <ImageCropperComponent></ImageCropperComponent>
        </div>
      )}
      <SuccessNotification></SuccessNotification>
      <FailedNotification></FailedNotification>
    </>
  );
}

export default Layout;
